﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal class ConfDataServerWrapper : IConferenceDataServer
    {
        public delegate void OnPingResultDelegate(uint task_id, ref LMPingResult result, long context, int error);
        
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct ConfDataServerCallback
        {
            public OnPingResultDelegate OnPingResultDelegate_;
        }

        public void OnPingResult(uint task_id, ref LMPingResult result, long context, int error)
        {
            foreach (IConferenceDataServerObserver observer in observer_list_)
                observer.OnPingResult(task_id, result, context, error);
        }

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_set_callback")]
        private static extern void lm_conf_dataserver_set_callback(IntPtr instance, ref ConfDataServerCallback callback);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_GetClusterGroup_Count")]
        private static extern int lm_conf_dataserver_GetClusterGroup_Count(
            IntPtr instance,
            ref int count);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_GetClusterGroup")]
        private static extern int lm_conf_dataserver_GetClusterGroup(
	        IntPtr instance,
            ref LMDataServerInfo data_servers, 
	        ref int count);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_GetCurrentConnectDataServer")]
        private static extern int lm_conf_dataserver_GetCurrentConnectDataServer(
	        IntPtr instance, 
	        int module,
            ref LMDataServerInfo data_server);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_PingDataServer")]
        private static extern int lm_conf_dataserver_PingDataServer(
	        IntPtr instance, 
	        String address,
	        int port, 
	        long callback_time,
	        int is_use_udp,
	        ulong context, 
	        ref int task_id);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_StopPingDataServer")]
        private static extern int lm_conf_dataserver_StopPingDataServer(
	        IntPtr instance, 
	        int task_id);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_dataserver_SwitchDataServer")]
        private static extern int lm_conf_dataserver_SwitchDataServer(
	        IntPtr instance, 
	        int module,
            String address);

        private IntPtr c_instance_;
        private List<IConferenceDataServerObserver> observer_list_;
        private ConfDataServerCallback lm_conf_dataserver_callback_;

        public ConfDataServerWrapper(IntPtr c_instance)
        {
            c_instance_ = c_instance;
            observer_list_ = new List<IConferenceDataServerObserver>();
            lm_conf_dataserver_callback_ = new ConfDataServerCallback();
            lm_conf_dataserver_callback_.OnPingResultDelegate_ = new OnPingResultDelegate(OnPingResult);
            lm_conf_dataserver_set_callback(c_instance, ref lm_conf_dataserver_callback_);
        }


        #region IConferenceDataServer 成员

        public int AddObserver(IConferenceDataServerObserver observer)
        {
            if (observer_list_.Contains(observer))
                return -1;
            observer_list_.Add(observer);
            return 0;
        }

        public int RemoveObserver(IConferenceDataServerObserver observer)
        {
            observer_list_.Remove(observer);
            return 0;
        }

        public int GetClusterGroup(List<LMDataServerInfo> cluster_group_data_servers)
        {
            int count = 0;
            if (lm_conf_dataserver_GetClusterGroup_Count(c_instance_, ref count) != 0)
                return -1;

            LMDataServerInfo[] ds = new LMDataServerInfo[count];
            if (lm_conf_dataserver_GetClusterGroup(c_instance_, ref ds[0], ref count) == 0)
            {
                for (int i = 0; i < count; i++)
                {
                    cluster_group_data_servers.Add(ds[i]);
                }
                return 0;
            }
            return -1;
        }

        public int GetCurrentConnectDataServer(LMDataServerModule module, ref LMDataServerInfo data_server)
        {
            return lm_conf_dataserver_GetCurrentConnectDataServer(c_instance_, (int)module, ref data_server);
        }

        public int PingDataServer(String address, int port, 
            long callback_time, bool is_use_udp, ulong context, ref int task_id)
        {
            return lm_conf_dataserver_PingDataServer(c_instance_, address,
                port, callback_time, is_use_udp ? 1 : 0, context, ref task_id);
        }

        public int StopPingDataServer(int task_id)
        {
            return lm_conf_dataserver_StopPingDataServer(c_instance_, task_id);
        }

        public int SwitchDataServer(LMDataServerModule module, String address)
        {
            return lm_conf_dataserver_SwitchDataServer(c_instance_, (int)module, address);
        }

        #endregion
    }
}
