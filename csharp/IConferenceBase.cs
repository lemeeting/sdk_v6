﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceBase
    {
        bool Init();
        bool InitWithOptions(LMInitOptions options);
	    int Terminate();

        void SetUseTcp(bool is_use_tcp);
		bool IsUseTcp();

		void SetProxy(LMProxyOptions options);
		void GetProxy(ref LMProxyOptions options);

		//	default 6, min=1, max=10
		void SetScreenCopyFps(int fps);
		int GetScreenCopyFps();

		//	default false
		void EnableScreenCopyLayerWindow(bool enable);
		bool IsEnableScreenCopyLayerWindow();

		void EnableVirtualDesktopCamera(bool enable);
		bool IsEnableVirtualDesktopCamera();

		void SetMainVideoDevice(String guid);

	    int GetPerfMon(ref LMPerMonitor per_mon);

        ASConfAttributeWrapper ConferenceAttribute();
	    ASConfRealTimeInfoWrapper RealtimeConferenceInfo();
        ASConfSyncInfoWrapper GetConfSyncInfo();
        ASConfAttendeeWrapper GetAttendee(String account);
	    ASConfAttendeeWrapper SelfAttendee();
      
    }
}
