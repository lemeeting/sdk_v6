﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceVideoObserver
    {
        void OnDisconnectVideoServer(int result);

        void OnAddLocalPreview(
            int id_device, 
            int id_preview, 
            UInt64 context, 
            int error);

        void OnVideoDeviceChanged(
            int change_device,
            bool is_add,
            int dev_nums, 
            int main_dev_id);

        void OnLocalVideoResolutionChanged(
            int id_device, 
            int previewindex,
            int new_width,
            int new_height);

        void OnRemoteVideoResolutionChanged(
            String account,
            int id_device, 
            int previewindex,
            int new_width, 
            int new_height);
    }
}
