﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CSharpWrapper
{
    internal class ConferenceBase : IConferenceBase
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_init")]
        private static extern int lm_conf_base_init(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_init_with_options")]
        private static extern int lm_conf_base_init_with_options(IntPtr instance, ref LMInitOptions options);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_terminate")]
        private static extern void lm_conf_base_terminate(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_set_use_tcp")]
        private static extern int lm_conf_base_set_use_tcp(IntPtr instance, int is_use_tcp);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_is_use_tcp")]
        private static extern int lm_conf_base_is_use_tcp(IntPtr instance, ref int is_use_tcp);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_set_proxy")]
        private static extern void lm_conf_base_set_proxy(IntPtr instance, ref LMProxyOptions proxy_options);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_proxy")]
        private static extern void lm_conf_base_get_proxy(IntPtr instance, ref LMProxyOptions proxy_options);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_set_screen_copy_fps")]
        private static extern void lm_conf_base_set_screen_copy_fps(IntPtr instance, int fps);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_screen_copy_fps")]
        private static extern int lm_conf_base_get_screen_copy_fps(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_enable_screen_copy_layer_window")]
        private static extern void lm_conf_base_enable_screen_copy_layer_window(IntPtr instance, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_is_enable_screen_copy_layer_window")]
        private static extern int lm_conf_base_is_enable_screen_copy_layer_window(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_enable_virtual_desktop_camera")]
        private static extern void lm_conf_base_enable_virtual_desktop_camera(IntPtr instance, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_is_enable_virtual_desktop_camera")]
        private static extern int lm_conf_base_is_enable_virtual_desktop_camera(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_set_main_video_device")]
        private static extern void lm_conf_base_set_main_video_device(IntPtr instance, String guid);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_per_monitor")]
        private static extern int lm_conf_base_get_per_monitor(IntPtr instance, ref LMPerMonitor per_mon);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_conf_attribute")]
        private static extern int lm_conf_base_get_conf_attribute(IntPtr instance, IntPtr attribute);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_real_time_conf_info")]
        private static extern int lm_conf_base_get_real_time_conf_info(IntPtr instance, IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_conf_sync_info")]
        private static extern int lm_conf_base_get_conf_sync_info(IntPtr instance, IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_self_attendee_info")]
        private static extern int lm_conf_base_get_self_attendee_info(IntPtr instance, IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_base_get_attendee_info")]
        private static extern int lm_conf_base_get_attendee_info(IntPtr instance, String account, IntPtr info);

        private IntPtr c_instance_;
        public ConferenceBase(IntPtr c_instance)
        {
            c_instance_ = c_instance;
        }

        #region IConferenceBase 成员

        public bool Init()
        {
            return lm_conf_base_init(c_instance_) == 0;
        }

        public bool InitWithOptions(LMInitOptions options)
        {
            return lm_conf_base_init_with_options(c_instance_, ref options) == 0;
        }

        public int Terminate()
        {
            lm_conf_base_terminate(c_instance_);
            return 0;
        }

        public void SetUseTcp(bool is_use_tcp)
        {
            lm_conf_base_set_use_tcp(c_instance_, is_use_tcp ? 1 : 0);
        }

        public bool IsUseTcp()
        {
            int v = 0;
            lm_conf_base_is_use_tcp(c_instance_, ref v);
            return v == 1;
        }

        public void SetProxy(LMProxyOptions options)
        {
            lm_conf_base_set_proxy(c_instance_, ref options);
        }

        public void GetProxy(ref LMProxyOptions options)
        {
            lm_conf_base_get_proxy(c_instance_, ref options);
        }

        public void SetScreenCopyFps(int fps)
        {
            lm_conf_base_set_screen_copy_fps(c_instance_, fps);
        }

        public int GetScreenCopyFps()
        {
            return lm_conf_base_get_screen_copy_fps(c_instance_);
        }

        public void EnableScreenCopyLayerWindow(bool enable)
        {
            lm_conf_base_enable_screen_copy_layer_window(c_instance_, enable ? 1 : 0);
        }

        public bool IsEnableScreenCopyLayerWindow()
        {
            return lm_conf_base_is_enable_screen_copy_layer_window(c_instance_) != 0;
        }

        public void EnableVirtualDesktopCamera(bool enable)
        {
            lm_conf_base_enable_virtual_desktop_camera(c_instance_, enable ? 1 : 0);
        }

        public bool IsEnableVirtualDesktopCamera()
        {
            return lm_conf_base_is_enable_virtual_desktop_camera(c_instance_) != 0;
        }

        public void SetMainVideoDevice(String guid)
        {
            lm_conf_base_set_main_video_device(c_instance_, guid);
        }

        public int GetPerfMon(ref LMPerMonitor per_mon)
        {
            return lm_conf_base_get_per_monitor(c_instance_, ref per_mon);
        }

        public ASConfAttributeWrapper ConferenceAttribute()
        {
            ASConfAttributeWrapper attribute = new ASConfAttributeWrapper();
            lm_conf_base_get_conf_attribute(c_instance_, attribute.NativeInstance);
            return attribute;
        }

        public ASConfRealTimeInfoWrapper RealtimeConferenceInfo()
        {
            ASConfRealTimeInfoWrapper info = new ASConfRealTimeInfoWrapper();
            lm_conf_base_get_real_time_conf_info(c_instance_, info.NativeInstance);
            return info;
        }

        public ASConfSyncInfoWrapper GetConfSyncInfo()
        {
            ASConfSyncInfoWrapper info = new ASConfSyncInfoWrapper();
            lm_conf_base_get_conf_sync_info(c_instance_, info.NativeInstance);
            return info;
        }

        public ASConfAttendeeWrapper GetAttendee(string account)
        {
            ASConfAttendeeWrapper attendee = new ASConfAttendeeWrapper();
            lm_conf_base_get_attendee_info(c_instance_, account, attendee.NativeInstance);
            return attendee;
        }

        public ASConfAttendeeWrapper SelfAttendee()
        {
            ASConfAttendeeWrapper attendee = new ASConfAttendeeWrapper();
            lm_conf_base_get_self_attendee_info(c_instance_, attendee.NativeInstance);
            return attendee;
        }

        #endregion
    }
}
