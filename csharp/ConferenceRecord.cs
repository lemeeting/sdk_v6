﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal class ConfRecordWrapper : IConferenceRecord
    {
        public delegate void OnStartLocalRecordReadyDelegate();
        
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct ConfRecordCallback
        {
            public OnStartLocalRecordReadyDelegate OnStartLocalRecordReadyDelegate_;
        }

        public void OnStartLocalRecordReady()
        {
            foreach (IConferenceRecordObserver observer in observer_list_)
                observer.OnStartLocalRecordReady();
        }

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_record_set_callback")]
        private static extern void lm_conf_record_set_callback(
            IntPtr instance,
            ref ConfRecordCallback callback);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_record_StartLocalRecord")]
        private static extern int lm_conf_record_StartLocalRecord(
            IntPtr instance,
            String file_name,
            ref String accounts,
	        int count,
            int flag);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_record_StopLocalRecord")]
        private static extern int lm_conf_record_StopLocalRecord(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_record_StartScreenRecord")]
        private static extern int lm_conf_record_StartScreenRecord(
            IntPtr instance,
            String file_name,
            int format,
            int zoom,
            float frame_rate,
            int qp,
            int flags);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_record_StopScreenRecord")]
        private static extern int lm_conf_record_StopScreenRecord(IntPtr instance);


        private IntPtr c_instance_;
        private List<IConferenceRecordObserver> observer_list_;
        private ConfRecordCallback lm_conf_record_callback_;

        public ConfRecordWrapper(IntPtr c_instance)
        {
            c_instance_ = c_instance;
            observer_list_ = new List<IConferenceRecordObserver>();
            lm_conf_record_callback_ = new ConfRecordCallback();
            lm_conf_record_callback_.OnStartLocalRecordReadyDelegate_ = new OnStartLocalRecordReadyDelegate(OnStartLocalRecordReady);
            lm_conf_record_set_callback(c_instance_, ref lm_conf_record_callback_);
        }


        #region IConferenceRecord 成员

        public int AddObserver(IConferenceRecordObserver observer)
        {
            if (observer_list_.Contains(observer))
                return -1;
            observer_list_.Add(observer);
            return 0;
        }

        public int RemoveObserver(IConferenceRecordObserver observer)
        {
            observer_list_.Remove(observer);
            return 0;
        }

        public int StartLocalRecord(string file_name, String[] recordaccounts,
            int flag = (int)LMConferenceRecordFlags.kRecordVoice | (int)LMConferenceRecordFlags.kRecordVideo | (int)LMConferenceRecordFlags.kRecordWbd | (int)LMConferenceRecordFlags.kRecordAppShare | (int)LMConferenceRecordFlags.kRecordText)
        {
            return lm_conf_record_StartLocalRecord(c_instance_, file_name, ref recordaccounts[0], recordaccounts.Length, flag);
        }

        public int StopLocalRecord()
        {
            return lm_conf_record_StopLocalRecord(c_instance_);
        }

        public int StartScreenRecord(string file_name, LMScreenRecordFormats format,
            LMScreenRecordZoom zoom, float frame_rate, LMScreenRecordQP qp,
            int flags = (int)LMScreenRecordVoiceFlags.kScreenRecordInputVoice | (int)LMScreenRecordVoiceFlags.kScreenRecordOutputVoice)
        {
            return lm_conf_record_StartScreenRecord(c_instance_, file_name, (int)format,
                (int)zoom, frame_rate, (int)qp, flags);
        }

        public int StopScreenRecord()
        {
            return lm_conf_record_StopScreenRecord(c_instance_);
        }

        #endregion
    }
}
