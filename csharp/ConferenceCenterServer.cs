﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal class ConfCenterServerWrapper : IConferenceCenterServer
    {
        public delegate void OnDisconnectCenterDelegate(int result);

        public delegate void OnGetAuthCodeDelegate(int result, IntPtr param, String strCode);
        public delegate void OnRegUserInfoDelegate(int result, IntPtr param, IntPtr info);

        public delegate void OnLoginDelegate(int result, IntPtr param, IntPtr userInfo);
        public delegate void OnLogoutDelegate(int result, IntPtr param);

        public delegate void OnPrepareLoginConfDelegate(int result, IntPtr param, uint id_conf, String jsonAddress);
        public delegate void OnGetRealConfDelegate(int result, IntPtr param,
            IntPtr listInfo, int size);

        public delegate void OnGetUserInfoDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateUserInfoDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveUserInfoDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetUserInfoListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetOrgInfoDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateOrgInfoDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetOrgInfoListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetUserBindDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddUserBindDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateUserBindDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveUserBindDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetUserBindListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetOrgUserDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddOrgUserDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateOrgUserDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveOrgUserDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetOrgUserListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetConfRoomDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddConfRoomDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateConfRoomDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveConfRoomDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetConfRoomListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetConfCodeDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddConfCodeDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateConfCodeDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveConfCodeDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetConfCodeListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetApplyMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddApplyMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdateApplyMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemoveApplyMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetApplyMsgListDelegate(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size);

        public delegate void OnGetPushMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnAddPushMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnUpdatePushMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnRemovePushMsgDelegate(int result, IntPtr param, IntPtr info);
        public delegate void OnGetPushMsgListDelegate(int result, IntPtr param, IntPtr info, 
            IntPtr listInfo, int size);
        public delegate void OnNoticePushMsgDelegate(IntPtr param, IntPtr info);

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct ConfCenterServerCallback
        {
            public OnDisconnectCenterDelegate OnDisconnectCenterDelegate_;

            public OnGetAuthCodeDelegate OnGetAuthCodeDelegate_;
            public OnRegUserInfoDelegate OnRegUserInfoDelegate_;

            public OnLoginDelegate OnLoginDelegate_;
            public OnLogoutDelegate OnLogoutDelegate_;

            public OnPrepareLoginConfDelegate OnPrepareLoginConfDelegate_;
            public OnGetRealConfDelegate OnGetRealConfDelegate_;

            public OnGetUserInfoDelegate OnGetUserInfoDelegate_;
            public OnUpdateUserInfoDelegate OnUpdateUserInfoDelegate_;
            public OnRemoveUserInfoDelegate OnRemoveUserInfoDelegate_;
            public OnGetUserInfoListDelegate OnGetUserInfoListDelegate_;

            public OnGetOrgInfoDelegate OnGetOrgInfoDelegate_;
            public OnUpdateOrgInfoDelegate OnUpdateOrgInfoDelegate_;
            public OnGetOrgInfoListDelegate OnGetOrgInfoListDelegate_;

            public OnGetUserBindDelegate OnGetUserBindDelegate_;
            public OnAddUserBindDelegate OnAddUserBindDelegate_;
            public OnUpdateUserBindDelegate OnUpdateUserBindDelegate_;
            public OnRemoveUserBindDelegate OnRemoveUserBindDelegate_;
            public OnGetUserBindListDelegate OnGetUserBindListDelegate_;

            public OnGetOrgUserDelegate OnGetOrgUserDelegate_;
            public OnAddOrgUserDelegate OnAddOrgUserDelegate_;
            public OnUpdateOrgUserDelegate OnUpdateOrgUserDelegate_;
            public OnRemoveOrgUserDelegate OnRemoveOrgUserDelegate_;
            public OnGetOrgUserListDelegate OnGetOrgUserListDelegate_;

            public OnGetConfRoomDelegate OnGetConfRoomDelegate_;
            public OnAddConfRoomDelegate OnAddConfRoomDelegate_;
            public OnUpdateConfRoomDelegate OnUpdateConfRoomDelegate_;
            public OnRemoveConfRoomDelegate OnRemoveConfRoomDelegate_;
            public OnGetConfRoomListDelegate OnGetConfRoomListDelegate_;

            public OnGetConfCodeDelegate OnGetConfCodeDelegate_;
            public OnAddConfCodeDelegate OnAddConfCodeDelegate_;
            public OnUpdateConfCodeDelegate OnUpdateConfCodeDelegate_;
            public OnRemoveConfCodeDelegate OnRemoveConfCodeDelegate_;
            public OnGetConfCodeListDelegate OnGetConfCodeListDelegate_;

            public OnGetApplyMsgDelegate OnGetApplyMsgDelegate_;
            public OnAddApplyMsgDelegate OnAddApplyMsgDelegate_;
            public OnUpdateApplyMsgDelegate OnUpdateApplyMsgDelegate_;
            public OnRemoveApplyMsgDelegate OnRemoveApplyMsgDelegate_;
            public OnGetApplyMsgListDelegate OnGetApplyMsgListDelegate_;

            public OnGetPushMsgDelegate OnGetPushMsgDelegate_;
            public OnAddPushMsgDelegate OnAddPushMsgDelegate_;
            public OnUpdatePushMsgDelegate OnUpdatePushMsgDelegate_;
            public OnRemovePushMsgDelegate OnRemovePushMsgDelegate_;
            public OnGetPushMsgListDelegate OnGetPushMsgListDelegate_;
            public OnNoticePushMsgDelegate OnNoticePushMsgDelegate_;
        }

        public void OnDisconnectCenter(int result)
        {
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnDisconnectCenter(result);
        }

        public void OnGetAuthCode(int result, IntPtr param, String strCode)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetAuthCode(result, param_set, strCode);
        }

        public void OnRegUserInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRegUserInfo(result, param_set, user_info);
        }

        public void OnLogin(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnLogin(result, param_set, user_info);
        }

        public void OnLogout(int result, IntPtr param)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnLogout(result, param_set);
        }

        public void OnPrepareLoginConf(int result, IntPtr param, uint id_conf, String jsonAddress)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnPrepareLoginConf(result, param_set, id_conf, jsonAddress);
        }

        public void OnGetRealConf(int result, IntPtr param,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSRealTimeConfWrapper[] conf_infos = new CSRealTimeConfWrapper[size];
            for (int i = 0; i < size; i++)
                conf_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetRealConf(result, param_set, conf_infos);
        }

        public void OnGetUserInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetUserInfo(result, param_set, user_info);
        }

        public void OnUpdateUserInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateUserInfo(result, param_set, user_info);
        }

        public void OnRemoveUserInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveUserInfo(result, param_set, user_info);
        }

        public void OnGetUserInfoList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserInfoWrapper user_info = new CSUserInfoWrapper();
            CSUserInfoWrapper[] user_infos = new CSUserInfoWrapper[size];
            for (int i = 0; i < size; i++)
                user_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetUserInfoList(result, param_set, user_info, user_infos);
        }

        public void OnGetOrgInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgInfoWrapper org_info = new CSOrgInfoWrapper();
            org_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetOrgInfo(result, param_set, org_info);
        }

        public void OnUpdateOrgInfo(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgInfoWrapper org_info = new CSOrgInfoWrapper();
            org_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateOrgInfo(result, param_set, org_info);
        }

        public void OnGetOrgInfoList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgInfoWrapper org_info = new CSOrgInfoWrapper();
            org_info.CopyNativeInstance(info);
            CSOrgInfoWrapper[] org_infos = new CSOrgInfoWrapper[size];
            for (int i = 0; i < size; i++)
                org_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetOrgInfoList(result, param_set, org_info, org_infos);
        }

        public void OnGetUserBind(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserBindWrapper bind_info = new CSUserBindWrapper();
            bind_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetUserBind(result, param_set, bind_info);
        }

        public void OnAddUserBind(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserBindWrapper bind_info = new CSUserBindWrapper();
            bind_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnAddUserBind(result, param_set, bind_info);
        }

        public void OnUpdateUserBind(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserBindWrapper bind_info = new CSUserBindWrapper();
            bind_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateUserBind(result, param_set, bind_info);
        }

        public void OnRemoveUserBind(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserBindWrapper bind_info = new CSUserBindWrapper();
            bind_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveUserBind(result, param_set, bind_info);
        }

        public void OnGetUserBindList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSUserBindWrapper bind_info = new CSUserBindWrapper();
            bind_info.CopyNativeInstance(info);
            CSUserBindWrapper[] bind_infos = new CSUserBindWrapper[size];
            for (int i = 0; i < size; i++)
                bind_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetUserBindList(result, param_set, bind_info, bind_infos);
        }

        public void OnGetOrgUser(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgUserWrapper user_info = new CSOrgUserWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetOrgUser(result, param_set, user_info);
        }

        public void OnAddOrgUser(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgUserWrapper user_info = new CSOrgUserWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnAddOrgUser(result, param_set, user_info);
        }

        public void OnUpdateOrgUser(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgUserWrapper user_info = new CSOrgUserWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateOrgUser(result, param_set, user_info);
        }

        public void OnRemoveOrgUser(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgUserWrapper user_info = new CSOrgUserWrapper();
            user_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveOrgUser(result, param_set, user_info);
        }

        public void OnGetOrgUserList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSOrgUserWrapper user_info = new CSOrgUserWrapper();
            user_info.CopyNativeInstance(info);
            CSOrgUserWrapper[] user_infos = new CSOrgUserWrapper[size];
            for (int i = 0; i < size; i++)
                user_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetOrgUserList(result, param_set, user_info, user_infos);
        }

        public void OnGetConfRoom(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfRoomWrapper room_info = new CSConfRoomWrapper();
            room_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetConfRoom(result, param_set, room_info);
        }

        public void OnAddConfRoom(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfRoomWrapper room_info = new CSConfRoomWrapper();
            room_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnAddConfRoom(result, param_set, room_info);
        }

        public void OnUpdateConfRoom(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfRoomWrapper room_info = new CSConfRoomWrapper();
            room_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateConfRoom(result, param_set, room_info);
        }

        public void OnRemoveConfRoom(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfRoomWrapper room_info = new CSConfRoomWrapper();
            room_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveConfRoom(result, param_set, room_info);
        }

        public void OnGetConfRoomList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfRoomWrapper room_info = new CSConfRoomWrapper();
            room_info.CopyNativeInstance(info);
            CSConfRoomWrapper[] room_infos = new CSConfRoomWrapper[size];
            for (int i = 0; i < size; i++)
                room_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetConfRoomList(result, param_set, room_info, room_infos);
        }

        public void OnGetConfCode(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfCodeWrapper code_info = new CSConfCodeWrapper();
            code_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetConfCode(result, param_set, code_info);
        }

        public void OnAddConfCode(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfCodeWrapper code_info = new CSConfCodeWrapper();
            code_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnAddConfCode(result, param_set, code_info);
        }

        public void OnUpdateConfCode(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfCodeWrapper code_info = new CSConfCodeWrapper();
            code_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateConfCode(result, param_set, code_info);
        }

        public void OnRemoveConfCode(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfCodeWrapper code_info = new CSConfCodeWrapper();
            code_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveConfCode(result, param_set, code_info);
        }

        public void OnGetConfCodeList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSConfCodeWrapper code_info = new CSConfCodeWrapper();
            code_info.CopyNativeInstance(info);
            CSConfCodeWrapper[] code_infos = new CSConfCodeWrapper[size];
            for (int i = 0; i < size; i++)
                code_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetConfCodeList(result, param_set, code_info, code_infos);
        }

        public void OnGetApplyMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSApplyMsgWrapper msg_info = new CSApplyMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetApplyMsg(result, param_set, msg_info);
        }

        public void OnAddApplyMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSApplyMsgWrapper msg_info = new CSApplyMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnAddApplyMsg(result, param_set, msg_info);
        }

        public void OnUpdateApplyMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSApplyMsgWrapper msg_info = new CSApplyMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnUpdateApplyMsg(result, param_set, msg_info);
        }

        public void OnRemoveApplyMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSApplyMsgWrapper msg_info = new CSApplyMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnRemoveApplyMsg(result, param_set, msg_info);
        }

        public void OnGetApplyMsgList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSApplyMsgWrapper msg_info = new CSApplyMsgWrapper();
            msg_info.CopyNativeInstance(info);
            CSApplyMsgWrapper[] msg_infos = new CSApplyMsgWrapper[size];
            for (int i = 0; i < size; i++)
                msg_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetApplyMsgList(result, param_set, msg_info, msg_infos);
        }

        public void OnGetPushMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetPushMsg(result, param_set, msg_info);
        }

        public void OnAddPushMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetPushMsg(result, param_set, msg_info);
        }

        public void OnUpdatePushMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetPushMsg(result, param_set, msg_info);
        }

        public void OnRemovePushMsg(int result, IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetPushMsg(result, param_set, msg_info);
        }

        public void OnGetPushMsgList(int result, IntPtr param, IntPtr info,
            IntPtr listInfo, int size)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            CSPushMsgWrapper[] msg_infos = new CSPushMsgWrapper[size];
            for (int i = 0; i < size; i++)
                msg_infos[i].CopyNativeInstance(Marshal.ReadIntPtr(listInfo, i * 4));
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnGetPushMsg(result, param_set, msg_info);
        }

        public void OnNoticePushMsg(IntPtr param, IntPtr info)
        {
            CSParamWrapper param_set = new CSParamWrapper();
            param_set.CopyNativeInstance(param);
            CSPushMsgWrapper msg_info = new CSPushMsgWrapper();
            msg_info.CopyNativeInstance(info);
            foreach (IConferenceCenterServerObserver observer in observer_list_)
                observer.OnNoticePushMsg(param_set, msg_info);
        }

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_set_callback")]
        private static extern void lm_conf_centerserver_set_callback(
	        IntPtr instance,
	        ref ConfCenterServerCallback callback);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_SetCenterAddress")]
        private static extern int lm_conf_centerserver_SetCenterAddress(
	        IntPtr instance, 
	        String address);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetCenterAddress")]
        private static extern int lm_conf_centerserver_GetCenterAddress(
	        IntPtr instance, 
	        StringBuilder address, int size);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetAuthCode")]
        private static extern int lm_conf_centerserver_GetAuthCode(
	        IntPtr instance,
	        IntPtr param);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RegUserInfo")]
        private static extern int lm_conf_centerserver_RegUserInfo(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_Login")]
        private static extern int lm_conf_centerserver_Login(
	        IntPtr instance, 
	        IntPtr param, 
	        int id_org, 
	        String account,
	        String psw);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_Logout")]
        private static extern int lm_conf_centerserver_Logout(
	        IntPtr instance, 
	        IntPtr param);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_PrepareLoginConf")]
        private static extern int lm_conf_centerserver_PrepareLoginConf(
	        IntPtr instance, 
	        IntPtr param,
	        int id_conf);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetRealConf")]
        private static extern int lm_conf_centerserver_GetRealConf(
	        IntPtr instance, 
	        IntPtr param,
            IntPtr listConfID, int count);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetUserInfo")]
        private static extern int lm_conf_centerserver_GetUserInfo(
	        IntPtr instance, 
	        IntPtr param, 
	        String strAccount);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateUserInfo")]
        private static extern int lm_conf_centerserver_UpdateUserInfo(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveUserInfo")]
        private static extern int lm_conf_centerserver_RemoveUserInfo(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetUserInfoList")]
        private static extern int lm_conf_centerserver_GetUserInfoList(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetOrgInfo")]
        private static extern int lm_conf_centerserver_GetOrgInfo(
	        IntPtr instance, 
	        IntPtr param, 
	        int id_org);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateOrgInfo")]
        private static extern int lm_conf_centerserver_UpdateOrgInfo(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetOrgInfoList")]
        private static extern int lm_conf_centerserver_GetOrgInfoList(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetUserBind")]
        private static extern int lm_conf_centerserver_GetUserBind(
	        IntPtr instance, 
	        IntPtr param, 
	        String strName);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddUserBind")]
        private static extern int lm_conf_centerserver_AddUserBind(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateUserBind")]
        private static extern int lm_conf_centerserver_UpdateUserBind(
	        IntPtr instance,
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveUserBind")]
        private static extern int lm_conf_centerserver_RemoveUserBind(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetUserBindList")]
        private static extern int lm_conf_centerserver_GetUserBindList(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetOrgUser")]
        private static extern int lm_conf_centerserver_GetOrgUser(
	        IntPtr instance, 
	        IntPtr param, 
	        int id_org, 
	        String strAccount);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddOrgUser")]
        private static extern int lm_conf_centerserver_AddOrgUser(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateOrgUser")]
        private static extern int lm_conf_centerserver_UpdateOrgUser(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveOrgUser")]
        private static extern int lm_conf_centerserver_RemoveOrgUser(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetOrgUserList")]
        private static extern int lm_conf_centerserver_GetOrgUserList(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetConfRoom")]
        private static extern int lm_conf_centerserver_GetConfRoom(
	        IntPtr instance, 
	        IntPtr param,
	        int id_conf);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddConfRoom")]
        private static extern int lm_conf_centerserver_AddConfRoom(
	        IntPtr instance,
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateConfRoom")]
        private static extern int lm_conf_centerserver_UpdateConfRoom(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveConfRoom")]
        private static extern int lm_conf_centerserver_RemoveConfRoom(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetConfRoomList")]
        private static extern int lm_conf_centerserver_GetConfRoomList(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetConfCode")]
        private static extern int lm_conf_centerserver_GetConfCode(
	        IntPtr instance, 
	        IntPtr param, 
	        String strCodeid);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddConfCode")]
        private static extern int lm_conf_centerserver_AddConfCode(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateConfCode")]
        private static extern int lm_conf_centerserver_UpdateConfCode(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveConfCode")]
        private static extern int lm_conf_centerserver_RemoveConfCode(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetConfCodeList")]
        private static extern int lm_conf_centerserver_GetConfCodeList(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetApplyMsg")]
        private static extern int lm_conf_centerserver_GetApplyMsg(
	        IntPtr instance, 
	        IntPtr param, 
	        int id_req);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddApplyMsg")]
        private static extern int lm_conf_centerserver_AddApplyMsg(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdateApplyMsg")]
        private static extern int lm_conf_centerserver_UpdateApplyMsg(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemoveApplyMsg")]
        private static extern int lm_conf_centerserver_RemoveApplyMsg(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetApplyMsgList")]
        private static extern int lm_conf_centerserver_GetApplyMsgList(
	        IntPtr instance,
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetPushMsg")]
        private static extern int lm_conf_centerserver_GetPushMsg(
	        IntPtr instance, 
	        IntPtr param, 
	        int msg_id);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_AddPushMsg")]
        private static extern int lm_conf_centerserver_AddPushMsg(
	        IntPtr instance,
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_UpdatePushMsg")]
        private static extern int lm_conf_centerserver_UpdatePushMsg(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_RemovePushMsg")]
        private static extern int lm_conf_centerserver_RemovePushMsg(
	        IntPtr instance, 
	        IntPtr param, 
	        IntPtr info);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_centerserver_GetPushMsgList")]
        private static extern int lm_conf_centerserver_GetPushMsgList(
	        IntPtr instance, 
	        IntPtr param,
	        IntPtr info);


        [DllImport("c_wrapper.dll", EntryPoint = "lm_lm_conf_centerserver_GetConfCode_with_id")]
        private static extern int lm_lm_conf_centerserver_GetConfCode_with_id(
            IntPtr instance,
            IntPtr param,
            int id_conf);

        private IntPtr c_instance_;
        private List<IConferenceCenterServerObserver> observer_list_;
        private ConfCenterServerCallback conf_centerserver_callback_;

        public ConfCenterServerWrapper(IntPtr c_instance)
        {
            c_instance_ = c_instance;
            observer_list_ = new List<IConferenceCenterServerObserver>();
            conf_centerserver_callback_ = new ConfCenterServerCallback();
            conf_centerserver_callback_.OnAddApplyMsgDelegate_ = new OnAddApplyMsgDelegate(OnAddApplyMsg);
            conf_centerserver_callback_.OnAddConfCodeDelegate_ = new OnAddConfCodeDelegate(OnAddConfCode);
            conf_centerserver_callback_.OnAddConfRoomDelegate_ = new OnAddConfRoomDelegate(OnAddConfRoom);
            conf_centerserver_callback_.OnAddOrgUserDelegate_ = new OnAddOrgUserDelegate(OnAddOrgUser);
            conf_centerserver_callback_.OnAddPushMsgDelegate_ = new OnAddPushMsgDelegate(OnAddPushMsg);
            conf_centerserver_callback_.OnAddUserBindDelegate_ = new OnAddUserBindDelegate(OnAddUserBind);
            conf_centerserver_callback_.OnDisconnectCenterDelegate_ = new OnDisconnectCenterDelegate(OnDisconnectCenter);
            conf_centerserver_callback_.OnGetApplyMsgDelegate_ = new OnGetApplyMsgDelegate(OnGetApplyMsg);
            conf_centerserver_callback_.OnGetApplyMsgListDelegate_ = new OnGetApplyMsgListDelegate(OnGetApplyMsgList);
            conf_centerserver_callback_.OnGetAuthCodeDelegate_ = new OnGetAuthCodeDelegate(OnGetAuthCode);
            conf_centerserver_callback_.OnGetConfCodeDelegate_ = new OnGetConfCodeDelegate(OnGetConfCode);
            conf_centerserver_callback_.OnGetConfCodeListDelegate_ = new OnGetConfCodeListDelegate(OnGetConfCodeList);
            conf_centerserver_callback_.OnGetConfRoomDelegate_ = new OnGetConfRoomDelegate(OnGetConfRoom);
            conf_centerserver_callback_.OnGetConfRoomListDelegate_ = new OnGetConfRoomListDelegate(OnGetConfRoomList);
            conf_centerserver_callback_.OnGetOrgInfoDelegate_ = new OnGetOrgInfoDelegate(OnGetOrgInfo);
            conf_centerserver_callback_.OnGetOrgInfoListDelegate_ = new OnGetOrgInfoListDelegate(OnGetOrgInfoList);
            conf_centerserver_callback_.OnGetOrgUserDelegate_ = new OnGetOrgUserDelegate(OnGetOrgUser);
            conf_centerserver_callback_.OnGetOrgUserListDelegate_ = new OnGetOrgUserListDelegate(OnGetOrgUserList);
            conf_centerserver_callback_.OnGetPushMsgDelegate_ = new OnGetPushMsgDelegate(OnGetPushMsg);
            conf_centerserver_callback_.OnGetPushMsgListDelegate_ = new OnGetPushMsgListDelegate(OnGetPushMsgList);
            conf_centerserver_callback_.OnGetRealConfDelegate_ = new OnGetRealConfDelegate(OnGetRealConf);
            conf_centerserver_callback_.OnGetUserBindDelegate_ = new OnGetUserBindDelegate(OnGetUserBind);
            conf_centerserver_callback_.OnGetUserBindListDelegate_ = new OnGetUserBindListDelegate(OnGetUserBindList);
            conf_centerserver_callback_.OnGetUserInfoDelegate_ = new OnGetUserInfoDelegate(OnGetUserInfo);
            conf_centerserver_callback_.OnGetUserInfoListDelegate_ = new OnGetUserInfoListDelegate(OnGetUserInfoList);
            conf_centerserver_callback_.OnLoginDelegate_ = new OnLoginDelegate(OnLogin);
            conf_centerserver_callback_.OnLogoutDelegate_ = new OnLogoutDelegate(OnLogout);
            conf_centerserver_callback_.OnNoticePushMsgDelegate_ = new OnNoticePushMsgDelegate(OnNoticePushMsg);
            conf_centerserver_callback_.OnPrepareLoginConfDelegate_ = new OnPrepareLoginConfDelegate(OnPrepareLoginConf);
            conf_centerserver_callback_.OnRegUserInfoDelegate_ = new OnRegUserInfoDelegate(OnRegUserInfo);
            conf_centerserver_callback_.OnRemoveApplyMsgDelegate_ = new OnRemoveApplyMsgDelegate(OnRemoveApplyMsg);
            conf_centerserver_callback_.OnRemoveConfCodeDelegate_ = new OnRemoveConfCodeDelegate(OnRemoveConfCode);
            conf_centerserver_callback_.OnRemoveConfRoomDelegate_ = new OnRemoveConfRoomDelegate(OnRemoveConfRoom);
            conf_centerserver_callback_.OnRemoveOrgUserDelegate_ = new OnRemoveOrgUserDelegate(OnRemoveOrgUser);
            conf_centerserver_callback_.OnRemovePushMsgDelegate_ = new OnRemovePushMsgDelegate(OnRemovePushMsg);
            conf_centerserver_callback_.OnRemoveUserBindDelegate_ = new OnRemoveUserBindDelegate(OnRemoveUserBind);
            conf_centerserver_callback_.OnRemoveUserInfoDelegate_ = new OnRemoveUserInfoDelegate(OnRemoveUserInfo);
            conf_centerserver_callback_.OnUpdateApplyMsgDelegate_ = new OnUpdateApplyMsgDelegate(OnUpdateApplyMsg);
            conf_centerserver_callback_.OnUpdateConfCodeDelegate_ = new OnUpdateConfCodeDelegate(OnUpdateConfCode);
            conf_centerserver_callback_.OnUpdateConfRoomDelegate_ = new OnUpdateConfRoomDelegate(OnUpdateConfRoom);
            conf_centerserver_callback_.OnUpdateOrgInfoDelegate_ = new OnUpdateOrgInfoDelegate(OnUpdateOrgInfo);
            conf_centerserver_callback_.OnUpdateOrgUserDelegate_ = new OnUpdateOrgUserDelegate(OnUpdateOrgUser);
            conf_centerserver_callback_.OnUpdatePushMsgDelegate_ = new OnUpdatePushMsgDelegate(OnUpdatePushMsg);
            conf_centerserver_callback_.OnUpdateUserBindDelegate_ = new OnUpdateUserBindDelegate(OnUpdateUserBind);
            conf_centerserver_callback_.OnUpdateUserInfoDelegate_ = new OnUpdateUserInfoDelegate(OnUpdateUserInfo);

            lm_conf_centerserver_set_callback(c_instance, ref conf_centerserver_callback_);
        }

        #region IConferenceCenterServer 成员

        public int AddObserver(IConferenceCenterServerObserver observer)
        {
            if (observer_list_.Contains(observer))
                return -1;
            observer_list_.Add(observer);
            return 0;
        }

        public int RemoveObserver(IConferenceCenterServerObserver observer)
        {
            observer_list_.Remove(observer);
            return 0;
        }

        public int SetCenterAddress(string address)
        {
            return lm_conf_centerserver_SetCenterAddress(c_instance_, address);
        }

        public int GetCenterAddress(StringBuilder address)
        {
            if(address.Capacity < 512)
                address.Capacity = 512;

            return lm_conf_centerserver_GetCenterAddress(c_instance_, address, address.Capacity);
        }

        public int GetAuthCode(CSParamWrapper param)
        {
            return lm_conf_centerserver_GetAuthCode(c_instance_, param.NativeInstance);
        }

        public int RegUserInfo(CSParamWrapper param, CSUserInfoWrapper info)
        {
            return lm_conf_centerserver_RegUserInfo(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int Login(CSParamWrapper param, uint id_org, string account, string psw)
        {
            return lm_conf_centerserver_Login(c_instance_, param.NativeInstance, (int)id_org, account, psw);
        }

        public int Logout(CSParamWrapper param)
        {
            return lm_conf_centerserver_Logout(c_instance_, param.NativeInstance);
        }

        public int PrepareLoginConf(CSParamWrapper param, uint id_conf)
        {
            return lm_conf_centerserver_PrepareLoginConf(c_instance_, param.NativeInstance, (int)id_conf);
        }

        public int GetRealConf(CSParamWrapper param, int[] listConfID)
        {
            if (listConfID.Length == 0)
                return -1;

            IntPtr p = Marshal.AllocHGlobal(Marshal.SizeOf(listConfID[0]) * listConfID.Length);
            Marshal.Copy(listConfID, 0, p, listConfID.Length);
            int ret = lm_conf_centerserver_GetRealConf(c_instance_, param.NativeInstance, p, listConfID.Length);
            Marshal.FreeHGlobal(p);
            return ret;
        }

        public int GetUserInfo(CSParamWrapper param, string strAccount)
        {
            return lm_conf_centerserver_GetUserInfo(c_instance_, param.NativeInstance, strAccount);
        }

        public int UpdateUserInfo(CSParamWrapper param, CSUserInfoWrapper info)
        {
            return lm_conf_centerserver_UpdateUserInfo(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveUserInfo(CSParamWrapper param, CSUserInfoWrapper info)
        {
            return lm_conf_centerserver_RemoveUserInfo(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetUserInfoList(CSParamWrapper param, CSUserInfoWrapper info)
        {
            return lm_conf_centerserver_GetUserInfoList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetOrgInfo(CSParamWrapper param, uint id_org)
        {
            return lm_conf_centerserver_GetOrgInfo(c_instance_, param.NativeInstance, (int)id_org);
        }

        public int UpdateOrgInfo(CSParamWrapper param, CSOrgInfoWrapper info)
        {
            return lm_conf_centerserver_UpdateOrgInfo(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetOrgInfoList(CSParamWrapper param, CSOrgInfoWrapper info)
        {
            return lm_conf_centerserver_GetOrgInfoList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetUserBind(CSParamWrapper param, string strName)
        {
            return lm_conf_centerserver_GetUserBind(c_instance_, param.NativeInstance, strName);
        }

        public int AddUserBind(CSParamWrapper param, CSUserBindWrapper info)
        {
            return lm_conf_centerserver_AddUserBind(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdateUserBind(CSParamWrapper param, CSUserBindWrapper info)
        {
            return lm_conf_centerserver_UpdateUserBind(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveUserBind(CSParamWrapper param, CSUserBindWrapper info)
        {
            return lm_conf_centerserver_RemoveUserBind(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetUserBindList(CSParamWrapper param, CSUserBindWrapper info)
        {
            return lm_conf_centerserver_GetUserBindList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetOrgUser(CSParamWrapper param, uint id_org, string strAccount)
        {
            return lm_conf_centerserver_GetOrgUser(c_instance_, param.NativeInstance, (int)id_org, strAccount);
        }

        public int AddOrgUser(CSParamWrapper param, CSOrgUserWrapper info)
        {
            return lm_conf_centerserver_AddOrgUser(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdateOrgUser(CSParamWrapper param, CSOrgUserWrapper info)
        {
            return lm_conf_centerserver_UpdateOrgUser(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveOrgUser(CSParamWrapper param, CSOrgUserWrapper info)
        {
            return lm_conf_centerserver_RemoveOrgUser(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetOrgUserList(CSParamWrapper param, CSOrgUserWrapper info)
        {
            return lm_conf_centerserver_GetOrgUserList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetConfRoom(CSParamWrapper param, uint id_conf)
        {
            return lm_conf_centerserver_GetConfRoom(c_instance_, param.NativeInstance, (int)id_conf);
        }

        public int AddConfRoom(CSParamWrapper param, CSConfRoomWrapper info)
        {
            return lm_conf_centerserver_AddConfRoom(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdateConfRoom(CSParamWrapper param, CSConfRoomWrapper info)
        {
            return lm_conf_centerserver_UpdateConfRoom(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveConfRoom(CSParamWrapper param, CSConfRoomWrapper info)
        {
            return lm_conf_centerserver_RemoveConfRoom(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetConfRoomList(CSParamWrapper param, CSConfRoomWrapper info)
        {
            return lm_conf_centerserver_GetConfRoomList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetConfCode(CSParamWrapper param, string strCodeid)
        {
            return lm_conf_centerserver_GetConfCode(c_instance_, param.NativeInstance, strCodeid);
        }

        public int AddConfCode(CSParamWrapper param, CSConfCodeWrapper info)
        {
            return lm_conf_centerserver_AddConfCode(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdateConfCode(CSParamWrapper param, CSConfCodeWrapper info)
        {
            return lm_conf_centerserver_UpdateConfCode(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveConfCode(CSParamWrapper param, CSConfCodeWrapper info)
        {
            return lm_conf_centerserver_RemoveConfCode(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetConfCodeList(CSParamWrapper param, CSConfCodeWrapper info)
        {
            return lm_conf_centerserver_GetConfCodeList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetApplyMsg(CSParamWrapper param, uint id_req)
        {
            return lm_conf_centerserver_GetApplyMsg(c_instance_, param.NativeInstance, (int)id_req);
        }

        public int AddApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info)
        {
            return lm_conf_centerserver_AddApplyMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdateApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info)
        {
            return lm_conf_centerserver_UpdateApplyMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemoveApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info)
        {
            return lm_conf_centerserver_RemoveApplyMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetApplyMsgList(CSParamWrapper param, CSApplyMsgWrapper info)
        {
            return lm_conf_centerserver_GetApplyMsgList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetPushMsg(CSParamWrapper param, uint msg_id)
        {
            return lm_conf_centerserver_GetPushMsg(c_instance_, param.NativeInstance, (int)msg_id);
        }

        public int AddPushMsg(CSParamWrapper param, CSPushMsgWrapper info)
        {
            return lm_conf_centerserver_AddPushMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int UpdatePushMsg(CSParamWrapper param, CSPushMsgWrapper info)
        {
            return lm_conf_centerserver_UpdatePushMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int RemovePushMsg(CSParamWrapper param, CSPushMsgWrapper info)
        {
            return lm_conf_centerserver_RemovePushMsg(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetPushMsgList(CSParamWrapper param, CSPushMsgWrapper info)
        {
            return lm_conf_centerserver_GetPushMsgList(c_instance_, param.NativeInstance, info.NativeInstance);
        }

        public int GetConfCode(CSParamWrapper param, UInt32 id_conf)
        {
            return lm_lm_conf_centerserver_GetConfCode_with_id(c_instance_, param.NativeInstance, (int)id_conf);
        }

        #endregion
    }
}
