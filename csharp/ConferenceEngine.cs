﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CSharpWrapper
{
    internal class ConfEngineWrapper : IConferenceEngine
    {
        private IntPtr conf_engine_instance_ = IntPtr.Zero;
        private IntPtr conf_base_instance_ = IntPtr.Zero;
        private IntPtr conf_business_instance_ = IntPtr.Zero;
        private IntPtr conf_video_instance_ = IntPtr.Zero;
        private IntPtr conf_voice_instance_ = IntPtr.Zero;
        private IntPtr conf_sharedesktop_instance_ = IntPtr.Zero;
        private IntPtr conf_centerserver_instance_ = IntPtr.Zero;
        private IntPtr conf_dataserver_instance_ = IntPtr.Zero;
        private IntPtr conf_playback_instance_ = IntPtr.Zero;
        private IntPtr conf_record_instance_ = IntPtr.Zero;

        private ConferenceBase conf_base_ = null;
        private ConfBusinessWrapper conf_business_ = null;
        private ConfCenterServerWrapper conf_center_server_ = null;
        private ConfDataServerWrapper conf_data_server_ = null;
        private ConfPlaybackWrapper conf_playback_ = null;
        private ConfRecordWrapper conf_record_ = null;
        private ConfShareDesktopWrapper conf_sharedesktop_= null;
        private ConfVideoWrapper conf_video_ = null;
        private ConfVoiceWrapper conf_voice_ = null;

        [DllImport("c_wrapper.dll", EntryPoint = "create_conf_engine_instance")]
        private static extern void create_conf_engine_instance(ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "destroy_conf_engine_instance")]
        private static extern void destroy_conf_engine_instance(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_base_instance")]
        private static extern void get_conf_base_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_business_instance")]
        private static extern void get_conf_business_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_video_instance")]
        private static extern void get_conf_video_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_voice_instance")]
        private static extern void get_conf_voice_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_center_server_instance")]
        private static extern void get_conf_center_server_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_data_server_instance")]
        private static extern void get_conf_data_server_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_record_instance")]
        private static extern void get_conf_record_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_playback_instance")]
        private static extern void get_conf_playback_instance(IntPtr conf_engine, ref IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "get_conf_sharedesktop_instance")]
        private static extern void get_conf_sharedesktop_instance(IntPtr conf_engine, ref IntPtr instance);


        #region IConferenceEngine 成员

        public void Init()
        {
            if (conf_engine_instance_ != IntPtr.Zero)
                return;

            create_conf_engine_instance(ref conf_engine_instance_);

            IntPtr conf_engine = conf_engine_instance_;
            get_conf_base_instance(conf_engine, ref conf_base_instance_);
            get_conf_business_instance(conf_engine, ref conf_business_instance_);
            get_conf_video_instance(conf_engine, ref conf_video_instance_);
            get_conf_voice_instance(conf_engine, ref conf_voice_instance_);
            get_conf_sharedesktop_instance(conf_engine, ref conf_sharedesktop_instance_);
            get_conf_center_server_instance(conf_engine, ref conf_centerserver_instance_);
            get_conf_data_server_instance(conf_engine, ref conf_dataserver_instance_);
            get_conf_record_instance(conf_engine, ref conf_record_instance_);
            get_conf_playback_instance(conf_engine, ref conf_playback_instance_);

            conf_base_ = new ConferenceBase(conf_base_instance_);
            conf_business_ = new ConfBusinessWrapper(conf_business_instance_);
            conf_center_server_ = new ConfCenterServerWrapper(conf_centerserver_instance_);
            conf_data_server_ = new ConfDataServerWrapper(conf_dataserver_instance_);
            conf_playback_ = new ConfPlaybackWrapper(conf_playback_instance_);
            conf_record_ = new ConfRecordWrapper(conf_record_instance_);
            conf_sharedesktop_ = new ConfShareDesktopWrapper(conf_sharedesktop_instance_);
            conf_video_ = new ConfVideoWrapper(conf_video_instance_);
            conf_voice_ = new ConfVoiceWrapper(conf_voice_instance_);

        }

        public void Uninit()
        {
            if (conf_engine_instance_ != IntPtr.Zero)
            {
                destroy_conf_engine_instance(conf_engine_instance_);
                conf_engine_instance_ = IntPtr.Zero;
                conf_base_instance_ = IntPtr.Zero;
                conf_business_instance_ = IntPtr.Zero;
                conf_centerserver_instance_ = IntPtr.Zero;
                conf_dataserver_instance_ = IntPtr.Zero;
                conf_playback_instance_ = IntPtr.Zero;
                conf_record_instance_ = IntPtr.Zero;
                conf_sharedesktop_instance_ = IntPtr.Zero;
                conf_video_instance_ = IntPtr.Zero;
                conf_voice_instance_ = IntPtr.Zero;
            }
        }

        public IConferenceBase GetConfBase()
        {
            return conf_base_;
        }

        public IConferenceBusiness GetConfBusiness()
        {
            return conf_business_;
        }

        public IConferenceVoice GetConfVoice()
        {
            return conf_voice_;
        }

        public IConferenceVideo GetConfVideo()
        {
            return conf_video_;
        }

        public IConferenceSharedesktop GetConfSharedesktop()
        {
            return conf_sharedesktop_;
        }

        public IConferenceRecord GetConfRecord()
        {
            return conf_record_;
        }

        public IConferencePlayback GetConfPlayback()
        {
            return conf_playback_;
        }

        public IConferenceDataServer GetConfDataServer()
        {
            return conf_data_server_;
        }

        public IConferenceCenterServer GetConfCenterServer()
        {
            return conf_center_server_;
        }

        #endregion
    }
}
