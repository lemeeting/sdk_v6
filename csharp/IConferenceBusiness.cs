﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceBusiness
    {
        int AddObserver(IConferenceBusinessObserver observer);
	    int RemoveObserver(IConferenceBusinessObserver observer);

	    int EntryConference(String json_address, 
		    UInt32 id_conference, UInt32 conf_tag, String conf_psw, bool is_md5,
		    String account, UInt32 id_org, String name);

	    int LeaveConference();

	    int ApplySpeak(bool apply);

	    int AccreditSpeak(String account, bool accredit);

	    int ApplyDataOper(bool apply);

	    int AccreditDataOper(String account, bool accredit);

	    int ApplyDataSync(bool apply);

	    int AccreditDataSync(String account, bool accredit);

	    int DataSyncCommand(String command);

	    int ApplyTempAdmin(bool apply);

	    int AccreditTempAdmin(String account, bool accredit);

	    int AuthTempAdmin(String admin_psw);

	    int StartPreviewVideo(String shower, int index, UInt64 context);

	    int StopPreviewVideo(String shower, int index, UInt64 context);

	    int SwitchMainVideo(int index);

	    int EnableVideo(bool enabled);

	    int CaptureVideoState(int devindex, bool enabled);

	    int UpdateVideoShowName(int devindex, String name);

	    int KickOutAttendee(String account);

	    int UpdataAttendeeName(String name);

	    int RelayMsgToOne(String receiver, String msg);

	    int RelayMsgToAll(String msg);

	    int AdminOperConfSetting(int cmd, int cmd_value);

	    int SetConfPassword(String psw);

	    int StartDesktopShare();
	    int StopDesktopShare();

	    int StartPreviewDesktop(String sharer, UInt64 context);
	    int StopPreviewDesktop(String sharer, UInt64 context);

        int AskRemoteDesktopControl(String sharer);
		int AbstainRemoteDesktopControl(String sharer);

		int AnswerRemoteDesktopControl(String controller, bool allow);
        int AbortRemoteDesktopControl(String controller);

        int LaunchSignin(LMConferenceSigninType signin_type);
	    int Signin(bool is_signin);

	    int OperSubGroup(String account, UInt32 id_subgroup);
	    int CancelSubGroup();

	    int OpRemoteDesktopShared(String sharer, bool is_start);

	    int StartPlayback(String file_name);
	    int StopPlayback();

        int StartMediaPlay(String file_name, int media_flag);
		int StopMediaPlay();
		int PauseMediaPlay(bool paused);

    }
}
