﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceBusinessObserver
    {
        void OnDisconnect(int result);
		void OnEntryConference(int result);
		void OnLeaveConference(int result);

		void OnGetConference(ASConfAttributeWrapper attribute);
		void OnUpdateConerence(ASConfAttributeWrapper attribute);
		void OnRemoveConerence(UInt32 id_conference);

		void OnGetConferenceRealTimeInfo(ASConfRealTimeInfoWrapper info);
		void OnGetConferenceSyncInfo(ASConfSyncInfoWrapper info);

		void OnAddSelfAttendee(ASConfAttendeeWrapper attendee);
		void OnAttendeeOnline(ASConfAttendeeWrapper attendee);
		void OnAttendeeOffline(String account);
        void OnUpdateAttendee(ASConfAttendeeWrapper old_attendee, ASConfAttendeeWrapper attendee);
		void OnRemoveAttendee(String account);

		void OnAddConfAdmin(String admin);
		void OnRemoveConfAdmin(String account);
		void OnAddConfDefaultAttendee(String account);
		void OnRemoveConfDefaultAttendee(String account);

		void OnApplySpeakOper(int result, String account, UInt32 new_state, UInt32 old_state, bool apply);
		void OnAccreditSpeakOper(int result, String account, UInt32 new_state, UInt32 old_state, bool accredit);
		void OnSpeakNotify(String account, UInt32 new_state, UInt32 old_state, bool is_speak);
		void OnApplyDataOper(int result, String account, UInt32 new_state, UInt32 old_state, bool apply);
		void OnAccreditDataOper(int result, String account, UInt32 new_state, UInt32 old_state, bool accredit);
		void OnDataOpNotify(String account, UInt32 new_state, UInt32 old_state, bool is_data_op);
		void OnApplyDataSync(int result, String account, UInt32 new_state, UInt32 old_state, bool apply);
		void OnAccreditDataSync(int result, String account, UInt32 new_state, UInt32 old_state, bool accredit);
		void OnSyncOpNotify(String account, UInt32 new_state, UInt32 old_state, bool is_sync);
		void OnDataSyncCommand(int result);
		void OnDataSyncCommandNotify(String syncer, String sync_data);
		void OnApplyTempAdmin(int result, String account, UInt32 new_state, UInt32 old_state, bool apply);
		void OnAccreditTempAdmin(int result, String account, UInt32 new_state, UInt32 old_state, bool accredit);
		void OnAuthTempAdmin(int result, String account);
		void OnTempAdminNotify(String account, UInt32 new_state, UInt32 old_state, bool is_admin);

		void OnStartPreviewVideo(int result, String shower, int id_device, UInt64 context);
        void OnStopPreviewVideo(int result, String shower, int id_device, UInt64 context);
        void OnStartPreviewVideoNotify(String shower, int id_device);
        void OnStopPreviewVideoNotify(String shower, int id_device);

        void OnSwitchMainVideo(int result, int old_id_device, int id_device);
        void OnSwitchMainVideoNotify(String account, int old_id_device, int id_device);
		void OnEnableVideo(int result, bool enabled);
		void OnEnableVideoNotify(String account, bool enabled);
        void OnCaptureVideoState(int result, int id_device, bool enabled);
        void OnCaptureVideoStateNotify(String account, int id_device, bool enabled);
		void OnVideoDeviceChangedNotify(String account, UInt16 dev_nums, UInt16 dev_state, int dev_main_id);
        void OnVideoShowNameChangedNotify(String account, int id_device, String name);

		void OnKickoutAttendee(int result, String account);
		void OnKickoutAttendeeNotify(String oper_account, String account);
		void OnUpdateAttendeeName(int result, String new_name);
		void OnUpdateAttendeeNameNotify(String account, String new_name);

		void OnRelayMsgToOne(int result, String receiver);
		void OnRelayMsgToOneNotify(String sender, String receiver, String msg);
		void OnRelayMsgToAll(int result);
		void OnRelayMsgToAllNotify(String sender, String msg);

		void OnAdminOperConfSetting(int result, int cmd, int cmd_value);
		void OnAdminOperConfSettingNotify(String account, int cmd, int cmd_value);
		void OnSetConfPassword(int result);
		void OnSetConfPasswordNotify(String account);

		void OnStartDesktopShare(int result);
		void OnStartDesktopShareNotify(String account);

		void OnStopDesktopShare(int result);
		void OnStopDesktopShareNotify(String account);

		void OnStartPreviewDesktop(int result, String sharer, UInt64 context);
		void OnStopPreviewDesktop(int result, String sharer, UInt64 context);
	
		void OnStartPreviewDesktopNotify(String sharer);
		void OnStopPreviewDesktopNotify(String sharer);

        void OnAskRemoteDesktopControl(int result, String sharer);
	    void OnAskRemoteDesktopControlNotify(String controller, String sharer);
		void OnAbstainRemoteDesktopControl(int result, String sharer);
		void OnAbstainRemoteDesktopControlNotify(String controller, String sharer);
		void OnAnswerRemoteDesktopControl(int result, String controller, bool allow);
		void OnAnswerRemoteDesktopControlNotify(String controller, String sharer, bool allow);
		void OnAbortRemoteDesktopControl(int result, String controller);
        void OnAbortRemoteDesktopControlNotify(String controller, String sharer);

        void OnLaunchSignin(int result, LMConferenceSigninType signin_type, DateTime launch_or_stop_time);
        void OnLaunchSigninNotify(String launcher, LMConferenceSigninType signin_type, DateTime launch_or_stop_time);
		
		void OnSignin(int result, bool is_signin, DateTime launch_time, DateTime signin_or_cancel_time);
		void OnSigninNotify(String signiner, bool is_signin, DateTime launch_time, DateTime signin_or_cancel_time);

		void OnOperSubGroup(int result);
		void OnOperSubGroupNotify(String oper, UInt32 id_subgroup);
		void OnCancelSubGroup(int result);
		void OnCancelSubGroupNotify(String oper);

		void OnOpRemoteDesktopShared(int result, String sharer, bool is_start);
		void OnOpRemoteDesktopSharedNotify(String oper, String sharer, bool is_start);

		void OnStartPlayback(int result, String file_name);
		void OnStartPlaybackNotify(String oper);
		void OnStopPlayback(int result);
		void OnStopPlaybackNotify(String oper);

        void OnStartMediaPlay(int result, String file_name, int media_flag);
        void OnStartMediaPlayNotify(String oper, String file_name, int media_flag);
		void OnStopMediaPlay(int result);
		void OnStopMediaPlayNotify(String oper);
		void OnPauseMediaPlay(int result, bool paused);
		void OnPauseMediaPlayNotify(String oper, bool paused);

    }
}
