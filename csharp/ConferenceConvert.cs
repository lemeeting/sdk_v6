﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal static class ConferenceConvert
    {
        [DllImport("c_wrapper.dll", EntryPoint = "conf_malloc_and_zero")]
        private static extern IntPtr conf_malloc_and_zero(int size);
        [DllImport("c_wrapper.dll", EntryPoint = "conf_free")]
        private static extern void conf_free(IntPtr p);

        public static IntPtr NativeMalloc(int size)
        {
            return conf_malloc_and_zero(size);
        }

        public static void NativeFree(IntPtr p)
        {
            conf_free(p);
        }


        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_apply_msg_instance")]
        private static extern int create_center_server_apply_msg_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_apply_msg_instance")]
        private static extern void destroy_center_server_apply_msg_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_version_id")]
        private static extern int get_center_server_apply_msg_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_delete_flag")]
        private static extern int get_center_server_apply_msg_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_id")]
        private static extern int get_center_server_apply_msg_req_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_type")]
        private static extern int get_center_server_apply_msg_req_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_state")]
        private static extern int get_center_server_apply_msg_req_state(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_result")]
        private static extern int get_center_server_apply_msg_req_result(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_time")]
        private static extern long get_center_server_apply_msg_req_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_rsp_time")]
        private static extern long get_center_server_apply_msg_rsp_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_msg")]
        private static extern IntPtr get_center_server_apply_msg_req_msg(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_rsp_msg")]
        private static extern IntPtr get_center_server_apply_msg_rsp_msg(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_apply_msg_req_data")]
        private static extern IntPtr get_center_server_apply_msg_req_data(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_version_id")]
        private static extern void set_center_server_apply_msg_version_id(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_delete_flag")]
        private static extern void set_center_server_apply_msg_delete_flag(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_id")]
        private static extern void set_center_server_apply_msg_req_id(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_type")]
        private static extern void set_center_server_apply_msg_req_type(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_state")]
        private static extern void set_center_server_apply_msg_req_state(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_result")]
        private static extern void set_center_server_apply_msg_req_result(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_time")]
        private static extern void set_center_server_apply_msg_req_time(IntPtr instance, long v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_rsp_time")]
        private static extern void set_center_server_apply_msg_rsp_time(IntPtr instance, long v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_msg")]
        private static extern void set_center_server_apply_msg_req_msg(IntPtr instance, String v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_rsp_msg")]
        private static extern void set_center_server_apply_msg_rsp_msg(IntPtr instance, String v);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_apply_msg_req_data")]
        private static extern void set_center_server_apply_msg_req_data(IntPtr instance, String v);


        public static IntPtr CreateNativeInstanceForApplyMsg()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_apply_msg_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForApplyMsg(IntPtr instance)
        {
            destroy_center_server_apply_msg_instance(instance);
        }

        public static ACApplyMsg NativeInstanceToApplyMsg(IntPtr native_instance)
        {
            ACApplyMsg msg = new ACApplyMsg();
            msg.DeleteFlag = get_center_server_apply_msg_delete_flag(native_instance);
            msg.ReqData = Marshal.PtrToStringAnsi(get_center_server_apply_msg_req_data(native_instance));
            msg.ReqId = get_center_server_apply_msg_req_id(native_instance);
            msg.ReqMsg = Marshal.PtrToStringAnsi(get_center_server_apply_msg_req_msg(native_instance));
            msg.ReqResult = get_center_server_apply_msg_req_result(native_instance);
            msg.ReqState = get_center_server_apply_msg_req_state(native_instance);
            msg.ReqTime = DateTime.FromFileTime(get_center_server_apply_msg_req_time(native_instance));
            msg.ReqType = get_center_server_apply_msg_req_type(native_instance);
            msg.RspMsg = Marshal.PtrToStringAnsi(get_center_server_apply_msg_rsp_msg(native_instance));
            msg.RspTime = DateTime.FromFileTime(get_center_server_apply_msg_rsp_time(native_instance));
            msg.VersionId = get_center_server_apply_msg_version_id(native_instance);
            return msg;
        }

        public static IntPtr ApplyMsgToNativeInstance(ACApplyMsg info)
        {
            IntPtr native_instance = CreateNativeInstanceForApplyMsg();
            set_center_server_apply_msg_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_apply_msg_req_data(native_instance, info.ReqData);
            set_center_server_apply_msg_req_id(native_instance, info.ReqId);
            set_center_server_apply_msg_req_msg(native_instance, info.ReqMsg);
            set_center_server_apply_msg_req_result(native_instance, info.ReqResult);
            set_center_server_apply_msg_req_state(native_instance, info.ReqState);
            set_center_server_apply_msg_req_time(native_instance, info.ReqTime.ToFileTime());
            set_center_server_apply_msg_req_type(native_instance, info.ReqType);
            set_center_server_apply_msg_rsp_msg(native_instance, info.RspMsg);
            set_center_server_apply_msg_rsp_time(native_instance, info.RspTime.ToFileTime());
            set_center_server_apply_msg_version_id(native_instance, info.VersionId);
            return native_instance;
        }

        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_code_instance")]
        private static extern int create_center_server_code_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_code_instance")]
        private static extern void destroy_center_server_code_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_version_id")]
        private static extern int get_center_server_code_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_delete_flag")]
        private static extern int get_center_server_code_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_type")]
        private static extern int get_center_server_code_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_conference_id")]
        private static extern int get_center_server_code_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_reserve")]
        private static extern int get_center_server_code_reserve(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_code_id")]
        private static extern IntPtr get_center_server_code_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_version_id")]
        private static extern void set_center_server_code_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_delete_flag")]
        private static extern void set_center_server_code_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_type")]
        private static extern void set_center_server_code_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_conference_id")]
        private static extern void set_center_server_code_conference_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_reserve")]
        private static extern void set_center_server_code_reserve(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_code_id")]
        private static extern void set_center_server_code_id(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForConfCode()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_code_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForConfCode(IntPtr instance)
        {
            destroy_center_server_code_instance(instance);
        }

        public static ACConfCode NativeInstanceToConfCode(IntPtr native_instance)
        {
            ACConfCode conf_code = new ACConfCode();
            conf_code.CodeId = Marshal.PtrToStringAnsi(get_center_server_code_id(native_instance));
            conf_code.CodeType = get_center_server_code_type(native_instance);
            conf_code.ConferenceId = get_center_server_code_conference_id(native_instance);
            conf_code.DeleteFlag = get_center_server_code_delete_flag(native_instance);
            conf_code.Reserver = get_center_server_code_reserve(native_instance);
            conf_code.VersionId = get_center_server_code_version_id(native_instance);
            return conf_code;
        }

        public static IntPtr ConfCodeToNativeInstance(ACConfCode info)
        {
            IntPtr native_instance = CreateNativeInstanceForConfCode();
            set_center_server_code_conference_id(native_instance, info.ConferenceId);
            set_center_server_code_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_code_id(native_instance, info.CodeId);
            set_center_server_code_reserve(native_instance, info.Reserver);
            set_center_server_code_version_id(native_instance, info.VersionId);
            set_center_server_code_type(native_instance, info.CodeType);
            return native_instance;
        }
        
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_room_instance")]
        private static extern int create_center_server_room_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_room_instance")]
        private static extern void destroy_center_server_room_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_version_id")]
        private static extern int get_center_server_room_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_delete_flag")]
        private static extern int get_center_server_room_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_id")]
        private static extern int get_center_server_room_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_type")]
        private static extern int get_center_server_room_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_world_id")]
        private static extern int get_center_server_room_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_group_id")]
        private static extern int get_center_server_room_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_server_id")]
        private static extern int get_center_server_room_server_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_max_user_nums")]
        private static extern int get_center_server_room_max_user_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_max_speaker_nums")]
        private static extern int get_center_server_room_max_speaker_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_flag")]
        private static extern int get_center_server_room_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_start_time")]
        private static extern long get_center_server_room_start_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_end_time")]
        private static extern long get_center_server_room_end_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_create_time")]
        private static extern long get_center_server_room_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_modify_time")]
        private static extern long get_center_server_room_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_name")]
        private static extern IntPtr get_center_server_room_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_conf_password")]
        private static extern IntPtr get_center_server_room_conf_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_manager_password")]
        private static extern IntPtr get_center_server_room_manager_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_creator")]
        private static extern IntPtr get_center_server_room_creator(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_mender")]
        private static extern IntPtr get_center_server_room_mender(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_setting_json")]
        private static extern IntPtr get_center_server_room_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_room_extend_json")]
        private static extern IntPtr get_center_server_room_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_version_id")]
        private static extern void set_center_server_room_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_delete_flag")]
        private static extern void set_center_server_room_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_id")]
        private static extern void set_center_server_room_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_type")]
        private static extern void set_center_server_room_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_world_id")]
        private static extern void set_center_server_room_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_group_id")]
        private static extern void set_center_server_room_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_server_id")]
        private static extern void set_center_server_room_server_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_max_user_nums")]
        private static extern void set_center_server_room_max_user_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_max_speaker_nums")]
        private static extern void set_center_server_room_max_speaker_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_flag")]
        private static extern void set_center_server_room_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_start_time")]
        private static extern void set_center_server_room_start_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_end_time")]
        private static extern void set_center_server_room_end_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_create_time")]
        private static extern void set_center_server_room_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_modify_time")]
        private static extern void set_center_server_room_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_name")]
        private static extern void set_center_server_room_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_conf_password")]
        private static extern void set_center_server_room_conf_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_manager_password")]
        private static extern void set_center_server_room_manager_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_creator")]
        private static extern void set_center_server_room_creator(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_mender")]
        private static extern void set_center_server_room_mender(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_setting_json")]
        private static extern void set_center_server_room_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_room_extend_json")]
        private static extern void set_center_server_room_extend_json(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForConfRoom()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_room_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForConfRoom(IntPtr instance)
        {
            destroy_center_server_room_instance(instance);
        }

        public static ACConfRoom NativeInstanceToConfRoom(IntPtr native_instance)
        {
            ACConfRoom conf_room = new ACConfRoom();
            conf_room.ConferenceId = get_center_server_room_id(native_instance);
            conf_room.ConfFlag = get_center_server_room_flag(native_instance);
            conf_room.ConfName = Marshal.PtrToStringAnsi(get_center_server_room_name(native_instance));
            conf_room.ConfPSW = Marshal.PtrToStringAnsi(get_center_server_room_conf_password(native_instance));
            conf_room.ConfType = get_center_server_room_type(native_instance);
            conf_room.CreateTime = DateTime.FromFileTime(get_center_server_room_create_time(native_instance));
            conf_room.Creator = Marshal.PtrToStringAnsi(get_center_server_room_creator(native_instance));
            conf_room.DeleteFlag = get_center_server_room_delete_flag(native_instance);
            conf_room.EndTime = DateTime.FromFileTime(get_center_server_room_end_time(native_instance));
            conf_room.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_room_extend_json(native_instance));
            conf_room.GroupId = get_center_server_room_group_id(native_instance);
            conf_room.ManagerPSW = Marshal.PtrToStringAnsi(get_center_server_room_manager_password(native_instance));
            conf_room.MaxSpeakerCount = get_center_server_room_max_speaker_nums(native_instance);
            conf_room.MaxUserCount = get_center_server_room_max_user_nums(native_instance);
            conf_room.Mender = Marshal.PtrToStringAnsi(get_center_server_room_mender(native_instance));
            conf_room.ModifyTime = DateTime.FromFileTime(get_center_server_room_modify_time(native_instance));
            conf_room.ServerId = get_center_server_room_server_id(native_instance);
            conf_room.StartTime = DateTime.FromFileTime(get_center_server_room_start_time(native_instance));
            conf_room.SettingJson = Marshal.PtrToStringAnsi(get_center_server_room_setting_json(native_instance));
            conf_room.VersionId = get_center_server_room_version_id(native_instance);
            conf_room.WorldId = get_center_server_room_world_id(native_instance);
            return conf_room;
        }

        public static IntPtr ConfRoomToNativeInstance(ACConfRoom info)
        {
            IntPtr native_instance = CreateNativeInstanceForConfRoom();
            set_center_server_room_conf_password(native_instance, info.ConfPSW);
            set_center_server_room_create_time(native_instance, info.CreateTime.ToFileTime());
            set_center_server_room_creator(native_instance, info.Creator);
            set_center_server_room_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_room_end_time(native_instance, info.EndTime.ToFileTime());
            set_center_server_room_extend_json(native_instance, info.ExtendJson);
            set_center_server_room_flag(native_instance, info.ConfFlag);
            set_center_server_room_group_id(native_instance, info.GroupId);
            set_center_server_room_id(native_instance, info.ConferenceId);
            set_center_server_room_manager_password(native_instance, info.ManagerPSW);
            set_center_server_room_max_speaker_nums(native_instance, info.MaxSpeakerCount);
            set_center_server_room_max_user_nums(native_instance, info.MaxUserCount);
            set_center_server_room_mender(native_instance, info.Mender);
            set_center_server_room_modify_time(native_instance, info.ModifyTime.ToFileTime());
            set_center_server_room_name(native_instance, info.ConfName);
            set_center_server_room_server_id(native_instance, info.ServerId);
            set_center_server_room_setting_json(native_instance, info.SettingJson);
            set_center_server_room_start_time(native_instance, info.StartTime.ToFileTime());
            set_center_server_room_type(native_instance, info.ConfType);
            set_center_server_room_version_id(native_instance, info.VersionId);
            set_center_server_room_world_id(native_instance, info.WorldId);
            return native_instance;
        }
  
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_org_instance")]
        private static extern int create_center_server_org_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_org_instance")]
        private static extern void destroy_center_server_org_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_version_id")]
        private static extern int get_center_server_org_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_delete_flag")]
        private static extern int get_center_server_org_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_world_id")]
        private static extern int get_center_server_org_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_type")]
        private static extern int get_center_server_org_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_flag")]
        private static extern int get_center_server_org_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_status")]
        private static extern int get_center_server_org_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_group_id")]
        private static extern int get_center_server_org_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_max_user_nums")]
        private static extern int get_center_server_org_max_user_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_name")]
        private static extern IntPtr get_center_server_org_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_show_name")]
        private static extern IntPtr get_center_server_org_show_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_contacter")]
        private static extern IntPtr get_center_server_org_contacter(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_phone")]
        private static extern IntPtr get_center_server_org_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_email")]
        private static extern IntPtr get_center_server_org_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_website")]
        private static extern IntPtr get_center_server_org_website(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_birthday")]
        private static extern long get_center_server_org_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_create_time")]
        private static extern long get_center_server_org_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_modify_time")]
        private static extern long get_center_server_org_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_creator")]
        private static extern IntPtr get_center_server_org_creator(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_mender")]
        private static extern IntPtr get_center_server_org_mender(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_setting_json")]
        private static extern IntPtr get_center_server_org_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_extend_json")]
        private static extern IntPtr get_center_server_org_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_address")]
        private static extern IntPtr get_center_server_org_address(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_version_id")]
        private static extern void set_center_server_org_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_delete_flag")]
        private static extern void set_center_server_org_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_world_id")]
        private static extern void set_center_server_org_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_type")]
        private static extern void set_center_server_org_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_flag")]
        private static extern void set_center_server_org_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_status")]
        private static extern void set_center_server_org_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_group_id")]
        private static extern void set_center_server_org_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_max_user_nums")]
        private static extern void set_center_server_org_max_user_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_name")]
        private static extern void set_center_server_org_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_show_name")]
        private static extern void set_center_server_org_show_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_contacter")]
        private static extern void set_center_server_org_contacter(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_phone")]
        private static extern void set_center_server_org_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_email")]
        private static extern void set_center_server_org_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_website")]
        private static extern void set_center_server_org_website(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_birthday")]
        private static extern void set_center_server_org_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_create_time")]
        private static extern void set_center_server_org_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_modify_time")]
        private static extern void set_center_server_org_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_creator")]
        private static extern void set_center_server_org_creator(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_mender")]
        private static extern void set_center_server_org_mender(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_setting_json")]
        private static extern void set_center_server_org_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_extend_json")]
        private static extern void set_center_server_org_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_address")]
        private static extern void set_center_server_org_address(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForOrgInfo()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_org_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForOrgInfo(IntPtr instance)
        {
            destroy_center_server_org_instance(instance);
        }
        
        public static ACOrgInfo NativeInstanceToOrgInfo(IntPtr native_instance)
        {
            ACOrgInfo org_info = new ACOrgInfo();
            org_info.Address = Marshal.PtrToStringAnsi(get_center_server_org_address(native_instance));
            org_info.Birthday = DateTime.FromFileTime(get_center_server_org_birthday(native_instance));
            org_info.Contacter = Marshal.PtrToStringAnsi(get_center_server_org_contacter(native_instance));
            org_info.CreateTime = DateTime.FromFileTime(get_center_server_org_create_time(native_instance));
            org_info.Creator = Marshal.PtrToStringAnsi(get_center_server_org_creator(native_instance));
            org_info.DeleteFlag = get_center_server_org_delete_flag(native_instance);
            org_info.Email = Marshal.PtrToStringAnsi(get_center_server_org_email(native_instance));
            org_info.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_org_extend_json(native_instance));
            org_info.GroupId = get_center_server_org_group_id(native_instance);
            org_info.MaxUserCount = get_center_server_org_max_user_nums(native_instance);
            org_info.Mender = Marshal.PtrToStringAnsi(get_center_server_org_mender(native_instance));
            org_info.ModifyTime = DateTime.FromFileTime(get_center_server_org_modify_time(native_instance));
            org_info.OrgFlag = get_center_server_org_flag(native_instance);
            org_info.OrgName = Marshal.PtrToStringAnsi(get_center_server_org_name(native_instance));
            org_info.OrgStatus = get_center_server_org_status(native_instance);
            org_info.OrgType = get_center_server_org_type(native_instance);
            org_info.Phone = Marshal.PtrToStringAnsi(get_center_server_org_phone(native_instance));
            org_info.ShowName = Marshal.PtrToStringAnsi(get_center_server_org_show_name(native_instance));
            org_info.SettingJson = Marshal.PtrToStringAnsi(get_center_server_org_setting_json(native_instance));
            org_info.VersionId = get_center_server_org_version_id(native_instance);
            org_info.Website = Marshal.PtrToStringAnsi(get_center_server_org_website(native_instance));
            org_info.WorldId = get_center_server_org_world_id(native_instance);
            return org_info;
        }

        public static IntPtr OrgInfoToNativeInstance(ACOrgInfo info)
        {
            IntPtr native_instance = CreateNativeInstanceForOrgInfo();
            set_center_server_org_address(native_instance, info.Address);
            set_center_server_org_birthday(native_instance, info.Birthday.ToFileTime());
            set_center_server_org_contacter(native_instance, info.Contacter);
            set_center_server_org_create_time(native_instance, info.CreateTime.ToFileTime());
            set_center_server_org_creator(native_instance, info.Creator);
            set_center_server_org_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_org_email(native_instance, info.Email);
            set_center_server_org_extend_json(native_instance, info.ExtendJson);
            set_center_server_org_flag(native_instance, info.OrgFlag);
            set_center_server_org_group_id(native_instance, info.GroupId);
            set_center_server_org_max_user_nums(native_instance, info.MaxUserCount);
            set_center_server_org_mender(native_instance, info.Mender);
            set_center_server_org_modify_time(native_instance, info.ModifyTime.ToFileTime());
            set_center_server_org_name(native_instance, info.OrgName);
            set_center_server_org_phone(native_instance, info.Phone);
            set_center_server_org_setting_json(native_instance, info.SettingJson);
            set_center_server_org_show_name(native_instance, info.ShowName);
            set_center_server_org_status(native_instance, info.OrgStatus);
            set_center_server_org_type(native_instance, info.OrgType);
            set_center_server_org_version_id(native_instance, info.VersionId);
            set_center_server_org_website(native_instance, info.Website);
            set_center_server_org_world_id(native_instance, info.WorldId);
            return native_instance;
        }
  
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_org_user_instance")]
        private static extern int create_center_server_org_user_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_org_user_instance")]
        private static extern void destroy_center_server_org_user_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_version_id")]
        private static extern int get_center_server_org_user_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_delete_flag")]
        private static extern int get_center_server_org_user_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_org_id")]
        private static extern int get_center_server_org_user_org_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_type")]
        private static extern int get_center_server_org_user_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_status")]
        private static extern int get_center_server_org_user_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_bind_user")]
        private static extern int get_center_server_org_user_bind_user(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_birthday")]
        private static extern long get_center_server_org_user_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_create_time")]
        private static extern long get_center_server_org_user_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_perm")]
        private static extern long get_center_server_org_user_perm(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_account")]
        private static extern IntPtr get_center_server_org_user_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_password")]
        private static extern IntPtr get_center_server_org_user_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_phone")]
        private static extern IntPtr get_center_server_org_user_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_email")]
        private static extern IntPtr get_center_server_org_user_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_setting_json")]
        private static extern IntPtr get_center_server_org_user_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_extend_json")]
        private static extern IntPtr get_center_server_org_user_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_org_user_name")]
        private static extern IntPtr get_center_server_org_user_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_version_id")]
        private static extern void set_center_server_org_user_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_delete_flag")]
        private static extern void set_center_server_org_user_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_org_id")]
        private static extern void set_center_server_org_user_org_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_type")]
        private static extern void set_center_server_org_user_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_status")]
        private static extern void set_center_server_org_user_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_bind_user")]
        private static extern void set_center_server_org_user_bind_user(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_birthday")]
        private static extern void set_center_server_org_user_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_create_time")]
        private static extern void set_center_server_org_user_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_perm")]
        private static extern void set_center_server_org_user_perm(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_account")]
        private static extern void set_center_server_org_user_account(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_password")]
        private static extern void set_center_server_org_user_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_phone")]
        private static extern void set_center_server_org_user_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_email")]
        private static extern void set_center_server_org_user_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_setting_json")]
        private static extern void set_center_server_org_user_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_extend_json")]
        private static extern void set_center_server_org_user_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_org_user_name")]
        private static extern void set_center_server_org_user_name(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForOrgUser()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_org_user_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForOrgUser(IntPtr instance)
        {
            destroy_center_server_org_user_instance(instance);
        }

        public static ACOrgUser NativeInstanceToOrgUser(IntPtr native_instance)
        {
            ACOrgUser org_user = new ACOrgUser();
            org_user.BindUser = get_center_server_org_user_bind_user(native_instance);
            org_user.Birthday = DateTime.FromFileTime(get_center_server_org_user_birthday(native_instance));
            org_user.CreateTime = DateTime.FromFileTime(get_center_server_org_user_create_time(native_instance));
            org_user.DeleteFlag = get_center_server_org_user_delete_flag(native_instance);
            org_user.Email = Marshal.PtrToStringAnsi(get_center_server_org_user_email(native_instance));
            org_user.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_org_user_extend_json(native_instance));
            org_user.OrgId = get_center_server_org_user_org_id(native_instance);
            org_user.Phone = Marshal.PtrToStringAnsi(get_center_server_org_user_phone(native_instance));
            org_user.SettingJson = Marshal.PtrToStringAnsi(get_center_server_org_user_setting_json(native_instance));
            org_user.UserAccount = Marshal.PtrToStringAnsi(get_center_server_org_user_account(native_instance));
            org_user.UserName = Marshal.PtrToStringAnsi(get_center_server_org_user_name(native_instance));
            org_user.UserPerm = get_center_server_org_user_perm(native_instance);
            org_user.UserPSW = Marshal.PtrToStringAnsi(get_center_server_org_user_password(native_instance));
            org_user.UserStatus = get_center_server_org_user_status(native_instance);
            org_user.UserType = get_center_server_org_user_type(native_instance);
            org_user.VersionId = get_center_server_org_user_version_id(native_instance);
            return org_user;
        }

        public static IntPtr OrgUserToNativeInstance(ACOrgUser info)
        {
            IntPtr native_instance = CreateNativeInstanceForOrgUser();
            set_center_server_org_user_account(native_instance, info.UserAccount);
            set_center_server_org_user_bind_user(native_instance, info.BindUser);
            set_center_server_org_user_birthday(native_instance, info.Birthday.ToFileTime());
            set_center_server_org_user_create_time(native_instance, info.CreateTime.ToFileTime());
            set_center_server_org_user_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_org_user_email(native_instance, info.Email);
            set_center_server_org_user_extend_json(native_instance, info.ExtendJson);
            set_center_server_org_user_name(native_instance, info.UserName);
            set_center_server_org_user_org_id(native_instance, info.OrgId);
            set_center_server_org_user_password(native_instance, info.UserPSW);
            set_center_server_org_user_perm(native_instance, info.UserPerm);
            set_center_server_org_user_phone(native_instance, info.Phone);
            set_center_server_org_user_setting_json(native_instance, info.SettingJson);
            set_center_server_org_user_status(native_instance, info.UserStatus);
            set_center_server_org_user_type(native_instance, info.UserType);
            set_center_server_org_user_version_id(native_instance, info.VersionId);
            return native_instance;
        }
  
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_push_msg_instance")]
        private static extern int create_center_server_push_msg_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_push_msg_instance")]
        private static extern void destroy_center_server_push_msg_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_version_id")]
        private static extern int get_center_server_push_msg_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_delete_flag")]
        private static extern int get_center_server_push_msg_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_id")]
        private static extern int get_center_server_push_msg_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_type")]
        private static extern int get_center_server_push_msg_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_state")]
        private static extern int get_center_server_push_msg_state(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_result")]
        private static extern int get_center_server_push_msg_result(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_send_org_id")]
        private static extern int get_center_server_push_msg_send_org_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_send_time")]
        private static extern long get_center_server_push_msg_send_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_end_time")]
        private static extern long get_center_server_push_msg_end_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_topic")]
        private static extern IntPtr get_center_server_push_msg_topic(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_send_account")]
        private static extern IntPtr get_center_server_push_msg_send_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_data")]
        private static extern IntPtr get_center_server_push_msg_data(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_receiver")]
        private static extern IntPtr get_center_server_push_msg_receiver(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_push_msg_extend_json")]
        private static extern IntPtr get_center_server_push_msg_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_version_id")]
        private static extern void set_center_server_push_msg_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_delete_flag")]
        private static extern void set_center_server_push_msg_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_id")]
        private static extern void set_center_server_push_msg_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_type")]
        private static extern void set_center_server_push_msg_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_state")]
        private static extern void set_center_server_push_msg_state(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_result")]
        private static extern void set_center_server_push_msg_result(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_send_org_id")]
        private static extern void set_center_server_push_msg_send_org_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_send_time")]
        private static extern void set_center_server_push_msg_send_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_end_time")]
        private static extern void set_center_server_push_msg_end_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_topic")]
        private static extern void set_center_server_push_msg_topic(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_send_account")]
        private static extern void set_center_server_push_msg_send_account(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_data")]
        private static extern void set_center_server_push_msg_data(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_receiver")]
        private static extern void set_center_server_push_msg_receiver(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_push_msg_extend_json")]
        private static extern void set_center_server_push_msg_extend_json(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForPushMsg()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_push_msg_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForPushMsg(IntPtr instance)
        {
            destroy_center_server_push_msg_instance(instance);
        }

        public static ACPushMsg NativeInstanceToPushMsg(IntPtr native_instance)
        {
            ACPushMsg push_msg = new ACPushMsg();
            push_msg.DeleteFlag = get_center_server_push_msg_delete_flag(native_instance);
            push_msg.EndTime = DateTime.FromFileTime(get_center_server_push_msg_end_time(native_instance));
            push_msg.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_push_msg_extend_json(native_instance));
            push_msg.MsgData = Marshal.PtrToStringAnsi(get_center_server_push_msg_data(native_instance));
            push_msg.MsgId = get_center_server_push_msg_id(native_instance);
            push_msg.MsgReceiver = Marshal.PtrToStringAnsi(get_center_server_push_msg_receiver(native_instance));
            push_msg.MsgResult = get_center_server_push_msg_result(native_instance);
            push_msg.MsgState = get_center_server_push_msg_state(native_instance);
            push_msg.MsgTopic = Marshal.PtrToStringAnsi(get_center_server_push_msg_topic(native_instance));
            push_msg.MsgType = get_center_server_push_msg_type(native_instance);
            push_msg.SendAccount = Marshal.PtrToStringAnsi(get_center_server_push_msg_send_account(native_instance));
            push_msg.SendOrgId = get_center_server_push_msg_send_org_id(native_instance);
            push_msg.SendTime = DateTime.FromFileTime(get_center_server_push_msg_send_time(native_instance));
            push_msg.VersionId = get_center_server_push_msg_version_id(native_instance);
            return push_msg;
        }

        public static IntPtr PushMsgToNativeInstance(ACPushMsg info)
        {
            IntPtr native_instance = CreateNativeInstanceForPushMsg();
            set_center_server_push_msg_data(native_instance, info.MsgData);
            set_center_server_push_msg_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_push_msg_end_time(native_instance, info.EndTime.ToFileTime());
            set_center_server_push_msg_extend_json(native_instance, info.ExtendJson);
            set_center_server_push_msg_id(native_instance, info.MsgId);
            set_center_server_push_msg_receiver(native_instance, info.MsgReceiver);
            set_center_server_push_msg_result(native_instance, info.MsgResult);
            set_center_server_push_msg_send_account(native_instance, info.SendAccount);
            set_center_server_push_msg_send_org_id(native_instance, info.SendOrgId);
            set_center_server_push_msg_send_time(native_instance, info.SendTime.ToFileTime());
            set_center_server_push_msg_state(native_instance, info.MsgState);
            set_center_server_push_msg_topic(native_instance, info.MsgTopic);
            set_center_server_push_msg_type(native_instance, info.MsgType);
            set_center_server_push_msg_version_id(native_instance, info.VersionId);
            return native_instance;
        }
  
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_user_bind_instance")]
        private static extern int create_center_server_user_bind_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_user_bind_instance")]
        private static extern void destroy_center_server_user_bind_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_version_id")]
        private static extern int get_center_server_user_bind_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_delete_flag")]
        private static extern int get_center_server_user_bind_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_world_id")]
        private static extern int get_center_server_user_bind_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_type")]
        private static extern int get_center_server_user_bind_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_create_time")]
        private static extern long get_center_server_user_bind_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_name")]
        private static extern IntPtr get_center_server_user_bind_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_bind_extend_json")]
        private static extern IntPtr get_center_server_user_bind_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_version_id")]
        private static extern void set_center_server_user_bind_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_delete_flag")]
        private static extern void set_center_server_user_bind_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_world_id")]
        private static extern void set_center_server_user_bind_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_type")]
        private static extern void set_center_server_user_bind_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_create_time")]
        private static extern void set_center_server_user_bind_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_name")]
        private static extern void set_center_server_user_bind_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_bind_extend_json")]
        private static extern void set_center_server_user_bind_extend_json(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForUserBind()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_user_bind_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForUserBind(IntPtr instance)
        {
            destroy_center_server_user_bind_instance(instance);
        }

        public static ACUserBind NativeInstanceToUserBind(IntPtr native_instance)
        {
            ACUserBind user_bind = new ACUserBind();
            user_bind.CreateTime = DateTime.FromFileTime(get_center_server_user_bind_create_time(native_instance));
            user_bind.DeleteFlag = get_center_server_user_bind_delete_flag(native_instance);
            user_bind.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_user_bind_extend_json(native_instance));
            user_bind.Name = Marshal.PtrToStringAnsi(get_center_server_user_bind_name(native_instance));
            user_bind.Type = get_center_server_user_bind_type(native_instance);
            user_bind.VersionId = get_center_server_user_bind_version_id(native_instance);
            user_bind.WorldId = get_center_server_user_bind_world_id(native_instance);
            return user_bind;
        }

        public static IntPtr UserBindToNativeInstance(ACUserBind info)
        {
            IntPtr native_instance = CreateNativeInstanceForUserBind();
            set_center_server_user_bind_create_time(native_instance, info.CreateTime.ToFileTime());
            set_center_server_user_bind_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_user_bind_extend_json(native_instance, info.ExtendJson);
            set_center_server_user_bind_name(native_instance, info.Name);
            set_center_server_user_bind_type(native_instance, info.Type);
            set_center_server_user_bind_version_id(native_instance, info.VersionId);
            set_center_server_user_bind_world_id(native_instance, info.WorldId);
            return native_instance;
        }
  
        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_user_instance")]
        private static extern int create_center_server_user_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_user_instance")]
        private static extern void destroy_center_server_user_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_version_id")]
        private static extern int get_center_server_user_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_delete_flag")]
        private static extern int get_center_server_user_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_world_id")]
        private static extern int get_center_server_user_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_type")]
        private static extern int get_center_server_user_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_status")]
        private static extern int get_center_server_user_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_birthday")]
        private static extern long get_center_server_user_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_create_time")]
        private static extern long get_center_server_user_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_name")]
        private static extern IntPtr get_center_server_user_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_password")]
        private static extern IntPtr get_center_server_user_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_phone")]
        private static extern IntPtr get_center_server_user_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_email")]
        private static extern IntPtr get_center_server_user_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_setting_json")]
        private static extern IntPtr get_center_server_user_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_extend_json")]
        private static extern IntPtr get_center_server_user_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_user_account")]
        private static extern IntPtr get_center_server_user_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_version_id")]
        private static extern void set_center_server_user_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_delete_flag")]
        private static extern void set_center_server_user_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_world_id")]
        private static extern void set_center_server_user_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_type")]
        private static extern void set_center_server_user_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_status")]
        private static extern void set_center_server_user_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_birthday")]
        private static extern void set_center_server_user_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_create_time")]
        private static extern void set_center_server_user_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_name")]
        private static extern void set_center_server_user_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_password")]
        private static extern void set_center_server_user_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_phone")]
        private static extern void set_center_server_user_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_email")]
        private static extern void set_center_server_user_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_setting_json")]
        private static extern void set_center_server_user_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_extend_json")]
        private static extern void set_center_server_user_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_user_account")]
        private static extern void set_center_server_user_account(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForUserInfo()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_user_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForUserInfo(IntPtr instance)
        {
            destroy_center_server_user_instance(instance);
        }

        public static ACUserInfo NativeInstanceToUserInfo(IntPtr native_instance)
        {
            ACUserInfo user_info = new ACUserInfo();
            user_info.Birthday = DateTime.FromFileTime(get_center_server_user_birthday(native_instance));
            user_info.CreateTime = DateTime.FromFileTime(get_center_server_user_create_time(native_instance));
            user_info.DeleteFlag = get_center_server_user_delete_flag(native_instance);
            user_info.Email = Marshal.PtrToStringAnsi(get_center_server_user_email(native_instance));
            user_info.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_user_extend_json(native_instance));
            user_info.Phone = Marshal.PtrToStringAnsi(get_center_server_user_phone(native_instance));
            user_info.SettingJson = Marshal.PtrToStringAnsi(get_center_server_user_setting_json(native_instance));
            user_info.UserAccount = Marshal.PtrToStringAnsi(get_center_server_user_account(native_instance));
            user_info.UserName = Marshal.PtrToStringAnsi(get_center_server_user_name(native_instance));
            user_info.UserPSW = Marshal.PtrToStringAnsi(get_center_server_user_password(native_instance));
            user_info.UserStatus = get_center_server_user_status(native_instance);
            user_info.UserType = get_center_server_user_type(native_instance);
            user_info.VersionId = get_center_server_user_version_id(native_instance);
            user_info.WorldId = get_center_server_user_world_id(native_instance);
            return user_info;
        }

        public static IntPtr UserInfoToNativeInstance(ACUserInfo info)
        {
            IntPtr native_instance = CreateNativeInstanceForUserInfo();
            set_center_server_user_account(native_instance, info.UserAccount);
            set_center_server_user_birthday(native_instance, info.Birthday.ToFileTime());
            set_center_server_user_create_time(native_instance, info.CreateTime.ToFileTime());
            set_center_server_user_delete_flag(native_instance, info.DeleteFlag);
            set_center_server_user_email(native_instance, info.Email);
            set_center_server_user_extend_json(native_instance, info.ExtendJson);
            set_center_server_user_name(native_instance, info.UserName);
            set_center_server_user_password(native_instance, info.UserPSW);
            set_center_server_user_phone(native_instance, info.Phone);
            set_center_server_user_setting_json(native_instance, info.SettingJson);
            set_center_server_user_status(native_instance, info.UserStatus);
            set_center_server_user_type(native_instance, info.UserType);
            set_center_server_user_version_id(native_instance, info.VersionId);
            set_center_server_user_world_id(native_instance, info.WorldId);
            return native_instance;
        }

        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_real_conf_instance")]
        private static extern int create_center_server_real_conf_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_real_conf_instance")]
        private static extern void destroy_center_server_real_conf_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_version_id")]
        private static extern int get_center_server_real_conf_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_conference_id")]
        private static extern int get_center_server_real_conf_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_world_id")]
        private static extern int get_center_server_real_conf_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_group_id")]
        private static extern int get_center_server_real_conf_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_server_id")]
        private static extern int get_center_server_real_conf_server_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_online_users")]
        private static extern int get_center_server_real_conf_online_users(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_flag")]
        private static extern int get_center_server_real_conf_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_modify_time")]
        private static extern long get_center_server_real_conf_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_real_conf_extend_json")]
        private static extern IntPtr get_center_server_real_conf_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_version_id")]
        private static extern void set_center_server_real_conf_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_conference_id")]
        private static extern void set_center_server_real_conf_conference_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_world_id")]
        private static extern void set_center_server_real_conf_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_group_id")]
        private static extern void set_center_server_real_conf_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_server_id")]
        private static extern void set_center_server_real_conf_server_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_online_users")]
        private static extern void set_center_server_real_conf_online_users(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_flag")]
        private static extern void set_center_server_real_conf_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_modify_time")]
        private static extern void set_center_server_real_conf_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_real_conf_extend_json")]
        private static extern void set_center_server_real_conf_extend_json(IntPtr instance, String value);

        public static IntPtr CreateNativeInstanceForRealConfInfo()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_real_conf_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForRealConfInfo(IntPtr instance)
        {
            destroy_center_server_real_conf_instance(instance);
        }

        public static RealConfInfo NativeInstanceToRealConfInfo(IntPtr native_instance)
        {
            RealConfInfo conf_info = new RealConfInfo();
            conf_info.ConfId = get_center_server_real_conf_conference_id(native_instance);
            conf_info.ExtendJson = Marshal.PtrToStringAnsi(get_center_server_real_conf_extend_json(native_instance));
            conf_info.Flags = get_center_server_real_conf_flag(native_instance);
            conf_info.GroupId = get_center_server_real_conf_group_id(native_instance);
            conf_info.ModifyTime = DateTime.FromFileTime(get_center_server_real_conf_modify_time(native_instance));
            conf_info.OnlineNumsers = get_center_server_real_conf_online_users(native_instance);
            conf_info.ServerId = get_center_server_real_conf_server_id(native_instance);
            conf_info.VersionId = get_center_server_real_conf_version_id(native_instance);
            conf_info.WorldId = get_center_server_real_conf_world_id(native_instance);
            return conf_info;
        }

        public static IntPtr RealConfInfoToNativeInstance(RealConfInfo info)
        {
            IntPtr native_instance = CreateNativeInstanceForRealConfInfo();
            set_center_server_real_conf_conference_id(native_instance, info.ConfId);
            set_center_server_real_conf_extend_json(native_instance, info.ExtendJson);
            set_center_server_real_conf_flag(native_instance, info.Flags);
            set_center_server_real_conf_group_id(native_instance, info.GroupId);
            set_center_server_real_conf_modify_time(native_instance, info.ModifyTime.ToFileTime());
            set_center_server_real_conf_online_users(native_instance, info.OnlineNumsers);
            set_center_server_real_conf_server_id(native_instance, info.ServerId);
            set_center_server_real_conf_version_id(native_instance, info.VersionId);
            set_center_server_real_conf_world_id(native_instance, info.WorldId);
            return native_instance;
        }

        [DllImport("c_wrapper.dll", EntryPoint = "create_center_server_param_set_instance")]
        private static extern int create_center_server_param_set_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "destroy_center_server_param_set_instance")]
        private static extern void destroy_center_server_param_set_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_param_set_count")]
        private static extern int get_center_server_param_set_count(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_param_set_keys")]
        private static extern int get_center_server_param_set_keys(IntPtr instance, ref int keys, int count);
        [DllImport("c_wrapper.dll", EntryPoint = "get_center_server_param_set_value")]
        private static extern int get_center_server_param_set_value(IntPtr instance, int key, StringBuilder v, int size);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_param_set_int_value")]
        private static extern void set_center_server_param_set_int_value(IntPtr instance, int key, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_param_set_string_value")]
        private static extern void set_center_server_param_set_string_value(IntPtr instance, int key, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "set_center_server_param_set_time_value")]
        private static extern void set_center_server_param_set_time_value(IntPtr instance, int key, long value);

        public static IntPtr CreateNativeInstanceForParamSet()
        {
            IntPtr insatnce = IntPtr.Zero;
            create_center_server_param_set_instance(ref insatnce);
            return insatnce;
        }

        public static void DeleteNativeInstanceForParamSet(IntPtr instance)
        {
            destroy_center_server_param_set_instance(instance);
        }

        public static ParamSet NativeInstanceToParamSet(IntPtr native_instance)
        {
            ParamSet param_set = new ParamSet();
            int count = get_center_server_param_set_count(native_instance);
            if (count == 0)
                return param_set;

            int[] keys = new int[count];
            get_center_server_param_set_keys(native_instance, ref keys[0], count);
            StringBuilder s_builder = new StringBuilder(128);
            for (int i = 0; i < count; i++)
            {
                int ret = get_center_server_param_set_value(native_instance, keys[i], s_builder, 128);
                if(ret == 0)
                param_set.ParamMap.Add(keys[i], s_builder.ToString());
            }
            return param_set;
        }

        public static IntPtr ParamSetToNativeInstance(ParamSet param_set)
        {
            IntPtr native_instance = CreateNativeInstanceForParamSet();
            foreach (KeyValuePair<int, String> kv in param_set.ParamMap)
                set_center_server_param_set_string_value(native_instance, kv.Key, kv.Value);
            return native_instance;
        }


    }
}
