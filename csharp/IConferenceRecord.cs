﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceRecord
    {
        int AddObserver(IConferenceRecordObserver observer);
        int RemoveObserver(IConferenceRecordObserver observer);

	    int StartLocalRecord(String file_name, String[] recordaccounts,
            int flag = (int)LMConferenceRecordFlags.kRecordVoice | (int)LMConferenceRecordFlags.kRecordVideo | (int)LMConferenceRecordFlags.kRecordWbd | (int)LMConferenceRecordFlags.kRecordAppShare | (int)LMConferenceRecordFlags.kRecordText);

	    int StopLocalRecord();

        int StartScreenRecord(String file_name, LMScreenRecordFormats format, LMScreenRecordZoom zoom,
            float frame_rate, LMScreenRecordQP qp, int flags = (int)LMScreenRecordVoiceFlags.kScreenRecordInputVoice | (int)LMScreenRecordVoiceFlags.kScreenRecordOutputVoice);

	    int StopScreenRecord();

    }
}
