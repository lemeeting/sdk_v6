﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceEngine
    {
        void Init();

        void Uninit();

        IConferenceBase GetConfBase();

        IConferenceBusiness GetConfBusiness();

        IConferenceVoice GetConfVoice();

        IConferenceVideo GetConfVideo();

        IConferenceSharedesktop GetConfSharedesktop();

        IConferenceRecord GetConfRecord();

        IConferencePlayback GetConfPlayback();

        IConferenceDataServer GetConfDataServer();

        IConferenceCenterServer GetConfCenterServer();
    }


    public class ConferenceEngineFactory
    {
        private static IConferenceEngine instance = null;
        public static IConferenceEngine CreateEngine()
        {
            if (instance == null)
            {
                instance = new ConfEngineWrapper();
                instance.Init();
            }
            return instance;
        }
    }  
   
}
