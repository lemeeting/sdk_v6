﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferencePlayback
    {
        int AddObserver(IConferencePlaybackObserver observer);
	    int RemoveObserver(IConferencePlaybackObserver observer);

	    int StartPlayback(String file_name);
        int PlayPlayback(String[] play_accounts);
	    int PausePlayback();
	    int ContinuePlayback();
	    int StopPlayback();

        int StartRepair(String file_name);
        int StopRepair();

	    int AddPreview(
		    UInt32 rec_session,
            IntPtr windows,
		    ref int id_preview,
		    int z_order,
		    float left,
		    float top,
		    float right,
		    float bottom);

	    int RemovePreview(UInt32 rec_session, int id_preview);
	    int GetRenderSnapshot(UInt32 rec_session, int id_preview, String file_name);

    }
}
