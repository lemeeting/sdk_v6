﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal class ConfVideoWrapper : IConferenceVideo
    {
        public delegate void OnDisconnectVideoServerDelegate(int result);

        public delegate void OnAddLocalPreviewDelegate(
            int id_device,
            int id_preview,
            UInt64 context,
            int error);

        public delegate void OnVideoDeviceChangedDelegate(
            int change_device,
            bool is_add,
            int dev_nums,
            int main_dev_id);

        public delegate void OnLocalVideoResolutionChangedDelegate(
            int id_device,
            int previewindex,
            int new_width,
            int new_height);

        public delegate void OnRemoteVideoResolutionChangedDelegate(
            String account,
            int id_device,
            int previewindex,
            int new_width,
            int new_height);

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct ConfVideoCallback
        {
            public OnDisconnectVideoServerDelegate OnDisconnectVideoServerDelegate_;
            public OnAddLocalPreviewDelegate OnAddLocalPreviewDelegate_;
            public OnVideoDeviceChangedDelegate OnVideoDeviceChangedDelegate_;
            public OnLocalVideoResolutionChangedDelegate OnLocalVideoResolutionChangedDelegate_;
            public OnRemoteVideoResolutionChangedDelegate OnRemoteVideoResolutionChangedDelegate_;
        }

        public void OnDisconnectVideoServer(int result)
        {
            foreach (IConferenceVideoObserver observer in observer_list_)
                observer.OnDisconnectVideoServer(result);
        }

        public void OnAddLocalPreview(
            int id_device,
            int id_preview,
            UInt64 context,
            int error)
        {
            foreach (IConferenceVideoObserver observer in observer_list_)
                observer.OnAddLocalPreview(id_device, id_preview, context, error);
        }

        public void OnVideoDeviceChanged(
            int change_device,
            bool is_add,
            int dev_nums,
            int main_dev_id)
        {
            foreach (IConferenceVideoObserver observer in observer_list_)
                observer.OnVideoDeviceChanged(change_device, is_add, dev_nums, main_dev_id);
        }

        public void OnLocalVideoResolutionChanged(
            int id_device,
            int previewindex,
            int new_width,
            int new_height)
        {
            foreach (IConferenceVideoObserver observer in observer_list_)
                observer.OnLocalVideoResolutionChanged(id_device, previewindex, new_width, new_height);
        }

        public void OnRemoteVideoResolutionChanged(
            String account,
            int id_device,
            int previewindex,
            int new_width,
            int new_height)
        {
            foreach (IConferenceVideoObserver observer in observer_list_)
                observer.OnRemoteVideoResolutionChanged(account, id_device, previewindex, new_width, new_height);
        }

 
        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_set_callback")]
        private static extern void lm_conf_video_set_callback(
	        IntPtr instance,
	        ref ConfVideoCallback callback);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_AddLocalPreview")]
        private static extern int lm_conf_video_AddLocalPreview(
	        IntPtr instance,
	        int id_device,
	        IntPtr windows,
	        ulong context,
	        int z_order,
	        float left,
	        float top,
	        float right,
            float bottom,
            ref int id_preview);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_ConfigureLocalPreview")]
        private static extern int lm_conf_video_ConfigureLocalPreview(
	        IntPtr instance,
            int id_device,
	        int id_preview,
	        int z_order,
	        float left,
	        float top,
	        float right,
	        float bottom);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_ChangeLocalPreviewWindow")]
        private static extern int lm_conf_video_ChangeLocalPreviewWindow(
            IntPtr instance,
            int id_device, 
            int id_preview,
            IntPtr window,
            UInt32 z_order,
            float left,
            float top,
            float right,
            float bottom);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RemoveLocalPreview")]
        private static extern int lm_conf_video_RemoveLocalPreview(IntPtr instance, int id_device, int id_preview);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableMirrorLocalPreview")]
        private static extern int lm_conf_video_EnableMirrorLocalPreview(
            IntPtr instance, int id_device, int id_preview,
	        int enable, int mirror_xaxis, int mirror_yaxis);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_MirrorLocalPreviewState")]
        private static extern int lm_conf_video_MirrorLocalPreviewState(
            IntPtr instance, int id_device, int id_preview,
            ref int enable, ref int mirror_xaxis, ref int mirror_yaxis);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RotateLocalPreview")]
        private static extern int lm_conf_video_RotateLocalPreview(
            IntPtr instance, int id_device,
	        int id_preview, int rotation);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RotateLocalPreviewState")]
        private static extern int lm_conf_video_RotateLocalPreviewState(
            IntPtr instance, int id_device,
            int id_preview, ref int rotation);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetLocalPreviewResolution")]
        private static extern int lm_conf_video_GetLocalPreviewResolution(
            IntPtr instance, int id_device, ref int width, ref int height);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_AddRemotePreview")]
        private static extern int lm_conf_video_AddRemotePreview(
	        IntPtr instance, 
	        String account,
            int id_device,
	        IntPtr windows,
	        int z_order,
	        float left,
	        float top,
	        float right,
            float bottom,
            ref int id_preview);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_ConfigureRemotePreview")]
        private static extern int lm_conf_video_ConfigureRemotePreview(
	        IntPtr instance, 
	        String account,
            int id_device,
	        int id_preview,
	        int z_order,
	        float left,
	        float top,
	        float right,
	        float bottom);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_ChangeRemotePreviewWindow")]
        private static extern int lm_conf_video_ChangeRemotePreviewWindow(
            IntPtr instance,
            String account,
            int id_device, 
            int id_preview,
            IntPtr window,
            int z_order,
            float left,
            float top,
            float right,
            float bottom);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RemoveRemotePreview")]
        private static extern int lm_conf_video_RemoveRemotePreview(IntPtr instance, String account, int id_device, int id_preview);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableMirrorRemotePreview")]
        private static extern int lm_conf_video_EnableMirrorRemotePreview(
            IntPtr instance,
            String account,
            int id_device,
            int id_preview,
            int enable,
            int mirror_xaxis,
            int mirror_yaxis);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_MirrorRemotePreviewState")]
        private static extern int lm_conf_video_MirrorRemotePreviewState(
            IntPtr instance, String account,
            int id_device, int id_preview,
            ref int enable, ref int mirror_xaxis, ref int mirror_yaxis);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RotateRemotePreview")]
        private static extern int lm_conf_video_RotateRemotePreview(
             IntPtr instance, String account,
             int id_device, int id_preview, int rotation);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RotateRemotePreviewState")]
        private static extern int lm_conf_video_RotateRemotePreviewState(
             IntPtr instance, String account,
             int id_device, int id_preview, ref int rotation);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableRemotePreviewColorEnhancement")]
        private static extern int lm_conf_video_EnableRemotePreviewColorEnhancement(
            IntPtr instance, String account,
	        int id_device, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RemotePreviewColorEnhancementState")]
        private static extern int lm_conf_video_RemotePreviewColorEnhancementState(
	        IntPtr instance, String account,
	        int id_device, ref int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableRemotePreview")]
        private static extern int lm_conf_video_EnableRemotePreview(IntPtr instance, String account, int id_device, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetRemotePreviewState")]
        private static extern int lm_conf_video_GetRemotePreviewState(IntPtr instance, String account, int id_device, ref int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetRemotePreviewResolution")]
        private static extern int lm_conf_video_GetRemotePreviewResolution(
            IntPtr instance, String account,
	        int id_device, ref int width, ref int height);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_NumOfDevice")]
        private static extern int lm_conf_video_NumOfDevice(IntPtr instance, ref int nums);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetDeviceName")]
        private static extern int lm_conf_video_GetDeviceName(IntPtr instance, int id_device, 
            StringBuilder name, int name_size, StringBuilder guid, int guid_size);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetDeviceIdWithGuid")]
        private static extern int lm_conf_video_GetDeviceIdWithGuid(
	        IntPtr instance, String guid, ref int id_device);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetDeviceFormat")]
        private static extern int lm_conf_video_GetDeviceFormat(IntPtr instance, int id_device, ref int pixels, ref int count);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_OpenPropertypage")]
        private static extern int lm_conf_video_OpenPropertypage(IntPtr instance, int id_device, IntPtr window, String title);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SwitchMobiledevicesCamera")]
        private static extern int lm_conf_video_SwitchMobiledevicesCamera(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetMobiledevicesActiveCamera")]
        private static extern int lm_conf_video_GetMobiledevicesActiveCamera(IntPtr instance, ref int id_device);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_IsCaptureing")]
        private static extern int lm_conf_video_IsCaptureing(IntPtr instance, int id_device, ref int v);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetVideoConfig")]
        private static extern int lm_conf_video_SetVideoConfig(IntPtr instance, int id_device, ref LMVideoConfig config);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetVideoConfig")]
        private static extern int lm_conf_video_GetVideoConfig(IntPtr instance, int id_device, ref LMVideoConfig config);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableDeflickering")]
        private static extern int lm_conf_video_EnableDeflickering(
            IntPtr instance, int id_device, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_DeflickeringState")]
        private static extern int lm_conf_video_DeflickeringState(
            IntPtr instance, int id_device, ref int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetCaptureDeviceSnapshot")]
        private static extern int lm_conf_video_GetCaptureDeviceSnapshot(IntPtr instance, int id_device, String file_name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetRenderSnapshot")]
        private static extern int lm_conf_video_GetRenderSnapshot(IntPtr instance, String account, 
            int indexdevice, int id_preview, String file_name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetShowNameInVideo")]
        private static extern int lm_conf_video_SetShowNameInVideo(IntPtr instance, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetShowNameInVideoState")]
        private static extern int lm_conf_video_GetShowNameInVideoState(IntPtr instance, ref int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_EnableDebugVideo")]
        private static extern int lm_conf_video_EnableDebugVideo(IntPtr instance, int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetDebugVideoState")]
        private static extern int lm_conf_video_GetDebugVideoState(IntPtr instance, ref int enable);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_GetCameraType")]
        private static extern int lm_conf_video_GetCameraType(
	        IntPtr instance, int id_device, ref int type);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_AddIPCamera")]
        private static extern int lm_conf_video_AddIPCamera(
	        IntPtr instance, ref LMIPCameraInfo info,
	        int channel, int is_main_stream, String name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RemoveIPCamera")]
        private static extern int lm_conf_video_RemoveIPCamera(
	        IntPtr instance, int identity, int channel, int is_main_stream);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_AddRAWDataCamera")]
        private static extern int lm_conf_video_AddRAWDataCamera(
            IntPtr instance, int identity, String name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_RemoveRAWDataCamera")]
        private static extern int lm_conf_video_RemoveRAWDataCamera(
            IntPtr instance, int identity);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_IncomingRAWData")]
        private static extern int lm_conf_video_IncomingRAWData(
            IntPtr instance, int identity, ref byte data, int data_len, int width, int height, int type);

        private IntPtr c_instance_;
        private List<IConferenceVideoObserver> observer_list_;
        private ConfVideoCallback conf_video_callback_;

        public ConfVideoWrapper(IntPtr c_instance)
        {
            c_instance_ = c_instance;
            observer_list_ = new List<IConferenceVideoObserver>();
            conf_video_callback_ = new ConfVideoCallback();
            conf_video_callback_.OnDisconnectVideoServerDelegate_ = new OnDisconnectVideoServerDelegate(OnDisconnectVideoServer);
            conf_video_callback_.OnAddLocalPreviewDelegate_ = new OnAddLocalPreviewDelegate(OnAddLocalPreview);
            conf_video_callback_.OnLocalVideoResolutionChangedDelegate_ = new OnLocalVideoResolutionChangedDelegate(OnLocalVideoResolutionChanged);
            conf_video_callback_.OnRemoteVideoResolutionChangedDelegate_ = new OnRemoteVideoResolutionChangedDelegate(OnRemoteVideoResolutionChanged);
            conf_video_callback_.OnVideoDeviceChangedDelegate_ = new OnVideoDeviceChangedDelegate(OnVideoDeviceChanged);
            lm_conf_video_set_callback(c_instance, ref conf_video_callback_);
            List<Int32> ss = new List<Int32>(128);
            GetDeviceFormat(0, ss);
        }
   
        #region IConferenceVideo 成员

        public int AddObserver(IConferenceVideoObserver observer)
        {
            if (observer_list_.Contains(observer))
                return -1;
            observer_list_.Add(observer);
            return 0;
        }

        public int RemoveObserver(IConferenceVideoObserver observer)
        {
            observer_list_.Remove(observer);
            return 0;
        }

        public int AddLocalPreview(
            int id_device,
            IntPtr windows,
            UInt64 context,
            UInt32 z_order,
            float left,
            float top,
            float right,
            float bottom,
            ref int id_preview)
        {
            return lm_conf_video_AddLocalPreview(c_instance_, id_device, windows, context,
                (int)z_order, left, top, right, bottom, ref id_preview);
        }

        public int ConfigureLocalPreview(int id_device, int id_preview, uint z_order,
            float left, float top, float right, float bottom)
        {
            return lm_conf_video_ConfigureLocalPreview(c_instance_, id_device, 
                id_preview, (int)z_order, left, top, right, bottom);
        }

        public int ChangeLocalPreviewWindow(
            int id_device,
            int id_preview,
            IntPtr window,
            UInt32 z_order,
            float left,
            float top,
            float right,
            float bottom)
        {
            return lm_conf_video_ChangeLocalPreviewWindow(c_instance_, 
                id_device, id_preview, window, z_order, left, top, right, bottom);
        }

        public int RemoveLocalPreview(int id_device, int id_preview)
        {
            return lm_conf_video_RemoveLocalPreview(c_instance_, id_device, id_preview);
        }

        public int EnableMirrorLocalPreview(int id_device, int id_preview,
            bool enable, bool mirror_xaxis, bool mirror_yaxis)
        {
            return lm_conf_video_EnableMirrorLocalPreview(c_instance_, id_device, id_preview,
                enable ? 1 : 0, mirror_xaxis ? 1 : 0, mirror_yaxis ? 1 : 0);
        }

        public int MirrorLocalPreviewState(int id_device, int id_preview,
            ref bool enable, ref bool mirror_xaxis, ref bool mirror_yaxis)
        {
            int v1 = 0;
            int v2 = 0;
            int v3 = 0;
            int ret = lm_conf_video_MirrorLocalPreviewState(c_instance_, id_device, id_preview,
                ref v1, ref v2, ref v3);
            enable = v1 != 0;
            mirror_xaxis = v2 != 0;
            mirror_yaxis = v3 != 0;
            return ret;
        }

        public int RotateLocalPreview(int id_device,
            int id_preview, int rotation)
        {
            return lm_conf_video_RotateLocalPreview(c_instance_, id_device, id_preview, rotation);
        }

        public int RotateLocalPreviewState(int id_device,
            int id_preview, ref int rotation)
        {
            return lm_conf_video_RotateLocalPreviewState(c_instance_, id_device, id_preview, ref rotation);
        }

        public int GetLocalPreviewResolution(
            int id_device, ref int width, ref int height)
        {
            return lm_conf_video_GetLocalPreviewResolution(c_instance_, id_device,  ref width, ref height);
        }

        public int AddRemotePreview(string account, int id_device, IntPtr windows,
            uint z_order, float left, float top, float right, float bottom, ref int id_preview)
        {
            return lm_conf_video_AddRemotePreview(c_instance_, account, id_device, windows,
                (int)z_order, left, top, right, bottom, ref id_preview);
        }

        public int ConfigureRemotePreview(string account, int id_device, int id_preview, 
            uint z_order, float left, float top, float right, float bottom)
        {
            return lm_conf_video_ConfigureRemotePreview(c_instance_, account, id_device, id_preview,
                (int)z_order, left, top, right, bottom);
        }


        public int ChangeRemotePreviewWindow(string account, int id_device, int id_preview, IntPtr window,
            uint z_order, float left, float top, float right, float bottom)
        {
            return lm_conf_video_ChangeRemotePreviewWindow(
                c_instance_, account, id_device, id_preview, window,
                (int)z_order, left, top, right, bottom);
        }

        public int RemoveRemotePreview(string account, int id_device, int id_preview)
        {
            return lm_conf_video_RemoveRemotePreview(c_instance_, account, id_device, id_preview);
        }

        public int EnableRemotePreview(string account, int id_device, bool enable)
        {
            return lm_conf_video_EnableRemotePreview(c_instance_, account, id_device, enable ? 1 : 0);
        }

        public int GetRemotePreviewState(string account, int id_device, ref bool enable)
        {
            int v = 0;
            int ret = lm_conf_video_GetRemotePreviewState(c_instance_, account, id_device, ref v);
            enable = v != 0;
            return ret;
        }

        public int EnableMirrorRemotePreview(String account,
            int id_device, int id_preview,
            bool enable, bool mirror_xaxis, bool mirror_yaxis)
        {
            return lm_conf_video_EnableMirrorRemotePreview(c_instance_, account, id_device, id_preview, 
                enable ? 1 : 0, mirror_xaxis ? 1 : 0, mirror_yaxis ? 1 : 0);
        }

        public int MirrorRemotePreviewState(String account,
            int id_device, int id_preview,
            ref bool enable, ref bool mirror_xaxis, ref bool mirror_yaxis)
        {
            int v1 = 0;
            int v2 = 0;
            int v3 = 0;
            int ret = lm_conf_video_MirrorRemotePreviewState(c_instance_, account, id_device, id_preview,
                ref v1, ref v2, ref v3);
            enable = v1 != 0;
            mirror_xaxis = v2 != 0;
            mirror_yaxis = v3 != 0;
            return ret;

        }

        public int RotateRemotePreview(String account,
            int id_device, int id_preview, int rotation)
        {
            return lm_conf_video_RotateRemotePreview(c_instance_, account, id_device, id_preview, rotation);
        }

        public int RotateRemotePreviewState(String account,
            int id_device, int id_preview, ref int rotation)
        {
            return lm_conf_video_RotateRemotePreviewState(c_instance_, account, id_device, id_preview, ref rotation);
        }

        public int EnableRemotePreviewColorEnhancement(String account,
            int id_device, bool enable)
        {
            return lm_conf_video_EnableRemotePreviewColorEnhancement(c_instance_, account, id_device, enable ? 1 : 0);
        }

        public int RemotePreviewColorEnhancementState(String account,
            int id_device, ref bool enable)
        {
            int v = 0;
            int ret = lm_conf_video_RemotePreviewColorEnhancementState(c_instance_, account, id_device, ref v);
            enable = v != 0;
            return ret;
        }

        public int GetRemotePreviewResolution(String account,
            int id_device, ref int width, ref int height)
        {
            return lm_conf_video_GetRemotePreviewResolution(c_instance_, account, id_device, ref width, ref height);
        }

        public int NumOfDevice(ref int nums)
        {
            return lm_conf_video_NumOfDevice(c_instance_, ref nums);
        }

        public int GetDeviceName(int id_device, StringBuilder name, StringBuilder guid)
        {
            if (name.Capacity < 512)
                name.Capacity = 512;
            if (guid.Capacity < 512)
                guid.Capacity = 512;
            return lm_conf_video_GetDeviceName(c_instance_, id_device, name, 512, guid, 512);
        }

        public int GetDeviceIdWithGuid(String guid, ref int id_device)
        {
            return lm_conf_video_GetDeviceIdWithGuid(c_instance_, guid, ref id_device);
        }

        public int GetDeviceFormat(int id_device, List<Int32> pixels)
        {
            int count = 128;
            int[] int_list = new int[count];
            int ret = lm_conf_video_GetDeviceFormat(c_instance_, id_device, ref int_list[0], ref count);
            if (ret == 0)
            {
                for (int i = 0; i < count; i++)
                    pixels.Add(int_list[i]);
            }
            return ret;
        }

        public int OpenPropertypage(int id_device, IntPtr window, string title)
        {
            return lm_conf_video_OpenPropertypage(c_instance_, id_device, window, title);
        }

        public int SwitchMobiledevicesCamera()
        {
            return lm_conf_video_SwitchMobiledevicesCamera(c_instance_);
        }

        public int GetMobiledevicesActiveCameraId(ref int id_device)
        {
            return lm_conf_video_GetMobiledevicesActiveCamera(c_instance_, ref id_device);
        }

        public int IsCaptureing(int id_device, ref bool v)
        {
            int b = 0;
            int ret = lm_conf_video_IsCaptureing(c_instance_, id_device, ref b);
            v = b != 0;
            return ret;
        }

        public int SetVideoConfig(int id_device, LMVideoConfig config)
        {
            return lm_conf_video_SetVideoConfig(c_instance_, id_device, ref config);
        }

        public int GetVideoConfig(int id_device, ref LMVideoConfig config)
        {
            return lm_conf_video_GetVideoConfig(c_instance_, id_device, ref config);
        }

        public int EnableDeflickering(int id_device, bool enable)
        {
            return lm_conf_video_EnableDeflickering(c_instance_, id_device, enable ? 1 : 0);
        }

        public int DeflickeringState(int id_device, ref bool enable)
        {
            int b = 0;
            int ret = lm_conf_video_DeflickeringState(c_instance_, id_device, ref b);
            enable = b != 0;
            return ret;
        }

        public int GetCaptureDeviceSnapshot(int id_device, string file_name)
        {
            return lm_conf_video_GetCaptureDeviceSnapshot(c_instance_, id_device, file_name);
        }

        public int GetRenderSnapshot(string account, int id_device, int id_preview, string file_name)
        {
            return lm_conf_video_GetRenderSnapshot(c_instance_, account, id_device, id_preview, file_name);
        }

        public int SetShowNameInVideo(bool enable)
        {
            return lm_conf_video_SetShowNameInVideo(c_instance_, enable ? 1 : 0);
        }

        public int GetShowNameInVideoState(ref bool enable)
        {
            int v = 0;
            int ret = lm_conf_video_GetShowNameInVideoState(c_instance_, ref v);
            enable = v != 0;
            return ret;
        }

        public int EnableDebugVideo(bool enable)
        {
            return lm_conf_video_EnableDebugVideo(c_instance_, enable ? 1 : 0);
        }

        public int GetDebugVideoState(ref bool enable)
        {
            int v = 0;
            int ret = lm_conf_video_GetDebugVideoState(c_instance_, ref v);
            enable = v != 0;
            return ret;
        }

        public int GetCameraType(int id_device, ref LMCameraType type)
        {
            int v = 0;
            int ret = lm_conf_video_GetCameraType(c_instance_, id_device, ref v);
            type = (LMCameraType)v;
            return ret;
        }

        public int AddIPCamera(LMIPCameraInfo info, int channel, bool is_main_stream, String name)
        {
            return lm_conf_video_AddIPCamera(c_instance_, ref info, channel, is_main_stream ? 1 : 0, name);
        }

        public int RemoveIPCamera(int identity, int channel, bool is_main_stream)
        {
            return lm_conf_video_RemoveIPCamera(c_instance_, identity, channel, is_main_stream ? 1 : 0);
        }

        public int AddRAWDataCamera(int identity, String name)
        {
            return lm_conf_video_AddRAWDataCamera(c_instance_, identity, name);
        }

		public int RemoveRAWDataCamera(int identity)
        {
            return lm_conf_video_RemoveRAWDataCamera(c_instance_, identity);
        }

		public int IncomingRAWData(int identity, byte[] data,
			int width, int height, LMRawVideoType type)
        {
            return lm_conf_video_IncomingRAWData(c_instance_, identity, ref data[0], data.Length, width, height, (int)type);
        }

        #endregion
    }
}
