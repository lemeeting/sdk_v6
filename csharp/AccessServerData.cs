﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public class ASConfAttendeeWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_as_attendee_instance")]
        private static extern int lm_create_as_attendee_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_as_attendee_instance")]
        private static extern void lm_destroy_as_attendee_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_as_attendee_instance")]
        private static extern void lm_copy_as_attendee_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_type")]
        private static extern int lm_get_as_attendee_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_login_type")]
        private static extern int lm_get_as_attendee_login_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_op_status")]
        private static extern int lm_get_as_attendee_op_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_admin_privilege")]
        private static extern int lm_get_as_attendee_admin_privilege(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_video_device_nums")]
        private static extern int lm_get_as_attendee_video_device_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_enum_video_devices")]
        private static extern int lm_get_as_attendee_enum_video_devices(IntPtr instance, ref int id_dvices, ref int nums);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_video_device_type")]
        private static extern int lm_get_as_attendee_video_device_type(IntPtr instance, int id_dvices);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_video_device_sub_type")]
        private static extern int lm_get_as_attendee_video_device_sub_type(IntPtr instance, int id_dvices);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_video_device_state")]
        private static extern int lm_get_as_attendee_video_device_state(IntPtr instance, int id_dvices);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_video_device_show_name")]
        private static extern IntPtr lm_get_as_attendee_video_device_show_name(IntPtr instance, int id_dvices);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_attendee_is_valid_video_device")]
        private static extern int lm_as_attendee_is_valid_video_device(IntPtr instance, int id_dvices);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_main_video_device_id")]
        private static extern int lm_get_as_attendee_main_video_device_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_attendee_is_forbidden_video_device")]
        private static extern int lm_as_attendee_is_forbidden_video_device(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_audio_device_nums")]
        private static extern int lm_get_as_attendee_audio_device_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_account")]
        private static extern IntPtr lm_get_as_attendee_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_name")]
        private static extern IntPtr lm_get_as_attendee_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_enterprise_id")]
        private static extern int lm_get_as_attendee_enterprise_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_server_id")]
        private static extern int lm_get_as_attendee_server_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_conference_id")]
        private static extern int lm_get_as_attendee_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_mac_address")]
        private static extern IntPtr lm_get_as_attendee_mac_address(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_attendee_signin_time")]
        private static extern long lm_get_as_attendee_signin_time(IntPtr instance);

        private IntPtr native_instance_;

        public ASConfAttendeeWrapper()
        {
            lm_create_as_attendee_instance(ref native_instance_);
        }

        ~ASConfAttendeeWrapper()
        {
            lm_destroy_as_attendee_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_as_attendee_instance(native_instance, native_instance_);
        }

        public int Type() 
        { return lm_get_as_attendee_type(native_instance_); }

        public LMConferenceClientType LoginType()
        { return (LMConferenceClientType)lm_get_as_attendee_login_type(native_instance_); }
	    
        public UInt32 OpsStatus() 
        { return (UInt32)lm_get_as_attendee_op_status(native_instance_); }
        
        public LMConferenceAdminModes AdminPrivilege()
        { return (LMConferenceAdminModes)lm_get_as_attendee_admin_privilege(native_instance_); }

        bool IsAdmin()
        { return AdminPrivilege() != LMConferenceAdminModes.kConferenceNonAdmin; }

        public int NumOfVoiceDevices() 
        { return lm_get_as_attendee_audio_device_nums(native_instance_); }
       
        public int NumOfVideoDevices()
        { return lm_get_as_attendee_video_device_nums(native_instance_); }

        public void EnumVideoDeviceId(List<Int32> id_devices)
        {
            int count = 128;
            int[] int_list = new int[count];
            int ret = lm_get_as_attendee_enum_video_devices(native_instance_, ref int_list[0], ref count);
            if (ret == 0)
            {
                for (int i = 0; i < count; i++)
                    id_devices.Add(int_list[i]);
            }
        }

        public bool VideoCaptureState(int id_device)
        { return lm_get_as_attendee_video_device_state(native_instance_, id_device) != 0; }
	    
        public bool IsForbiddenVideo()
        { return lm_as_attendee_is_forbidden_video_device(native_instance_) != 0; }
	   
        public int MainVideoDevId()
        { return lm_get_as_attendee_main_video_device_id(native_instance_); }

        public String GetVideoShowName(int id_device)
        { return Marshal.PtrToStringAnsi(lm_get_as_attendee_video_device_show_name(native_instance_, id_device)); }
 
        public bool IsValidVideoDevice(int id_device)
        {
 		    return lm_as_attendee_is_valid_video_device(native_instance_, id_device) != 0;
        }
	
        public String Account()
        { return Marshal.PtrToStringAnsi(lm_get_as_attendee_account(native_instance_)); }
	    
        public String Name() 
        { return Marshal.PtrToStringAnsi(lm_get_as_attendee_name(native_instance_)); }
	    
        public UInt32 EnterpriseId()
        { return (UInt32)lm_get_as_attendee_enterprise_id(native_instance_); }
	    	    
        public UInt32 AccessServerId()
        { return (UInt32)lm_get_as_attendee_server_id(native_instance_); }
	    
        public UInt32 ConferenceId()
        { return (UInt32)lm_get_as_attendee_conference_id(native_instance_); }
	    
        public String MacAddress()
        { return Marshal.PtrToStringAnsi(lm_get_as_attendee_mac_address(native_instance_)); }
	    	    
        public DateTime SigninTime() 
        { return DateTime.FromFileTime(lm_get_as_attendee_signin_time(native_instance_)); }

        public bool ApplySpeakOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsApplySpeak) == (int)LMAttendeeOpsFlags.kOpsApplySpeak); }
		
        public bool SpeakOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsSpeak) == (int)LMAttendeeOpsFlags.kOpsSpeak); }
		
        public bool ApplyDataOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsApplyOperData) == (int)LMAttendeeOpsFlags.kOpsApplyOperData); }
		
        public bool DataOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsOperData) == (int)LMAttendeeOpsFlags.kOpsOperData); }
		
        public bool ApplySyncOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsApplySync) == (int)LMAttendeeOpsFlags.kOpsApplySync); }
		
        public bool SyncOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsSync) == (int)LMAttendeeOpsFlags.kOpsSync); }
		
        public bool ApplyAdminOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsApplyAdmin) == (int)LMAttendeeOpsFlags.kOpsApplyAdmin); }
		
        public bool SharedDesktopOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsSharedDesktop) == (int)LMAttendeeOpsFlags.kOpsSharedDesktop); }
		
        public bool PlaybackOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsPlayback) == (int)LMAttendeeOpsFlags.kOpsPlayback); }
		
        public bool MediaPlayOp()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsMediaPlay) == (int)LMAttendeeOpsFlags.kOpsMediaPlay); }
		
        public bool AskRemoteControlDesktopStatus()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsAskRemoteControlDesktop) == (int)LMAttendeeOpsFlags.kOpsAskRemoteControlDesktop); }
		
        public bool InRemoteControlDesktopStatus()
        { return ((OpsStatus() & (int)LMAttendeeOpsFlags.kOpsInRemoteControlDesktop) == (int)LMAttendeeOpsFlags.kOpsInRemoteControlDesktop); }
    }

    public class ASConfAttributeWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_as_conference_attribute_instance")]
        private static extern int lm_create_as_conference_attribute_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_as_conference_attribute_instance")]
        private static extern void lm_destroy_as_conference_attribute_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_as_conference_attribute_instance")]
        private static extern void lm_copy_as_conference_attribute_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_attribute_conference_id")]
        private static extern int lm_get_as_conference_attribute_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_attribute_enterprise_id")]
        private static extern int lm_get_as_conference_attribute_enterprise_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_attribute_max_attendees")]
        private static extern int lm_get_as_conference_attribute_max_attendees(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_attribute_tag")]
        private static extern int lm_get_as_conference_attribute_tag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_attribute_name")]
        private static extern IntPtr lm_get_as_conference_attribute_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_attribute_is_free")]
        private static extern int lm_as_conference_attribute_is_free(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_attribute_is_lock")]
        private static extern int lm_as_conference_attribute_is_lock(IntPtr instance);
         [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_attribute_is_disable_text")]
        private static extern int lm_as_conference_attribute_is_disable_text(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_attribute_is_disable_record")]
         private static extern int lm_as_conference_attribute_is_disable_record(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_attribute_is_disable_brower_video")]
        private static extern int lm_as_conference_attribute_is_disable_brower_video(IntPtr instance);

        private IntPtr native_instance_;
        public ASConfAttributeWrapper()
        {
            lm_create_as_conference_attribute_instance(ref native_instance_);
        }

        ~ASConfAttributeWrapper()
        {
            lm_destroy_as_conference_attribute_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_as_conference_attribute_instance(native_instance, native_instance_);
        }

        public UInt32 ConferenceId
        { get { return (UInt32)lm_get_as_conference_attribute_conference_id(native_instance_); } }

        public UInt32 OrgId
        { get { return (UInt32)lm_get_as_conference_attribute_enterprise_id(native_instance_); } }

        public int MaxAttendees
        { get { return lm_get_as_conference_attribute_max_attendees(native_instance_); }}

        public UInt32 Tag
        { get { return (UInt32)lm_get_as_conference_attribute_tag(native_instance_); }}

        public String Name
        { get { return Marshal.PtrToStringAnsi(lm_get_as_conference_attribute_name(native_instance_)); }}

        public bool IsFree()
        { return lm_as_conference_attribute_is_free(native_instance_) != 0; }

        public bool IsLock()
        { return lm_as_conference_attribute_is_lock(native_instance_) != 0; }

        public bool IsDisableText()
        { return lm_as_conference_attribute_is_disable_text(native_instance_) != 0; }

        public bool IsDisableRecord()
        { return lm_as_conference_attribute_is_disable_record(native_instance_) != 0; }

        public bool IsDisableBrowVideo()
        { return lm_as_conference_attribute_is_disable_brower_video(native_instance_) != 0; }
    }

    public class ASConfRealTimeInfoWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_as_real_time_conference_info_instance")]
        private static extern int lm_create_as_real_time_conference_info_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_as_real_time_conference_info_instance")]
        private static extern void lm_destroy_as_real_time_conference_info_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_as_real_time_conference_info_instance")]
        private static extern void lm_copy_as_real_time_conference_info_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_flags")]
        private static extern int lm_get_as_real_time_conference_info_flags(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_conference_id")]
        private static extern int lm_get_as_real_time_conference_info_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_online_attendees")]
        private static extern int lm_get_as_real_time_conference_info_online_attendees(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_signin_type")]
        private static extern int lm_get_as_real_time_conference_info_signin_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_lanunch_signin_time")]
        private static extern long lm_get_as_real_time_conference_info_lanunch_signin_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_real_time_conference_info_stop_signin_time")]
        private static extern long lm_get_as_real_time_conference_info_stop_signin_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_real_time_conference_info_is_lanunch_signin")]
        private static extern int lm_as_real_time_conference_info_is_lanunch_signin(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_real_time_conference_info_is_stop_signin")]
        private static extern int lm_as_real_time_conference_info_is_stop_signin(IntPtr instance);
	
        private IntPtr native_instance_;
        public ASConfRealTimeInfoWrapper()
        {
            lm_create_as_real_time_conference_info_instance(ref native_instance_);
        }

        ~ASConfRealTimeInfoWrapper()
        {
            lm_destroy_as_real_time_conference_info_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_as_real_time_conference_info_instance(native_instance, native_instance_);
        }

        public UInt32 Flags
        { get { return (UInt32)lm_get_as_real_time_conference_info_flags(native_instance_); }}

        public UInt32 ConferenceId
        { get { return (uint)lm_get_as_real_time_conference_info_conference_id(native_instance_); }}

        public int OnlineAttendee
        { get { return lm_get_as_real_time_conference_info_online_attendees(native_instance_); }}

  	    public bool IsLaunchSignin
        { get { return lm_as_real_time_conference_info_is_lanunch_signin(native_instance_) != 0; } }

	    public bool IsStopSignin
        { get { return lm_as_real_time_conference_info_is_stop_signin(native_instance_) != 0; } }

        public LMConferenceSigninType SigninType
        { get { return (LMConferenceSigninType)lm_get_as_real_time_conference_info_signin_type(native_instance_); } }

	    public DateTime LanunchSigninTime
        { get { return DateTime.FromFileTime(lm_get_as_real_time_conference_info_lanunch_signin_time(native_instance_)); }}

        public DateTime StopSigninTime
        { get { return DateTime.FromFileTime(lm_get_as_real_time_conference_info_stop_signin_time(native_instance_)); } }

        public bool IsFree()
        { return ((Flags & (int)LMConerenceFlags.kConferenceFree) == (int)LMConerenceFlags.kConferenceFree); }
        
        public bool IsLock()
        { return ((Flags & (int)LMConerenceFlags.kConferenceLock) == (int)LMConerenceFlags.kConferenceLock); }
        
        public bool IsOpen()
        { return ((Flags & (int)LMConerenceFlags.kConferenceOpen) == (int)LMConerenceFlags.kConferenceOpen); }
        
        public bool IsHide()
        { return ((Flags & (int)LMConerenceFlags.kConferenceHide) == (int)LMConerenceFlags.kConferenceHide); }
        
        public bool IsForceSync()
        { return ((Flags & (int)LMConerenceFlags.kConferenceForceSync) == (int)LMConerenceFlags.kConferenceForceSync); }
        
        public bool IsDisableText()
        { return ((Flags & (int)LMConerenceFlags.kConferenceDisableText) == (int)LMConerenceFlags.kConferenceDisableText); }
        
        public bool IsDisableRecord()
        { return ((Flags & (int)LMConerenceFlags.kConferenceDisableRecord) == (int)LMConerenceFlags.kConferenceDisableRecord); }
        
        public bool IsDisableBrowVideo()
        { return ((Flags & (int)LMConerenceFlags.kConferenceDisableBrowVideo) == (int)LMConerenceFlags.kConferenceDisableBrowVideo); }
        
        public bool IsPlayback()
        { return ((Flags & (int)LMConerenceFlags.kConferencePlayback) == (int)LMConerenceFlags.kConferencePlayback); }
        
        public bool IsDisableAllMicrophone()
        { return ((Flags & (int)LMConerenceFlags.kConferenceDisableMicrophone) == (int)LMConerenceFlags.kConferenceDisableMicrophone); }
        
        public bool IsAllMuted()
        { return ((Flags & (int)LMConerenceFlags.kConferenceMuted) == (int)LMConerenceFlags.kConferenceMuted); }
       
        public bool IsLockScreen()
        { return ((Flags & (int)LMConerenceFlags.kConferenceLockScreen) == (int)LMConerenceFlags.kConferenceLockScreen); }
        
        public bool IsMediaPlay()
        { return ((Flags & (int)LMConerenceFlags.kConferenceMediaPlay) == (int)LMConerenceFlags.kConferenceMediaPlay); }
        
        public bool IsMediaPause()
        { return ((Flags & (int)LMConerenceFlags.kConferenceMediaPause) == (int)LMConerenceFlags.kConferenceMediaPause); }

    }

    public class ASConfSyncInfoWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_as_conference_sync_info_instance")]
        private static extern int lm_create_as_conference_sync_info_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_as_conference_sync_info_instance")]
        private static extern void lm_destroy_as_conference_sync_info_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_as_conference_sync_info_instance")]
        private static extern void lm_copy_as_conference_sync_info_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_sync_info_syncer")]
        private static extern IntPtr lm_get_as_conference_sync_info_syncer(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_as_conference_sync_info_sync_data")]
        private static extern IntPtr lm_get_as_conference_sync_info_sync_data(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_as_conference_sync_info_is_sync")]
        private static extern int lm_as_conference_sync_info_is_sync(IntPtr instance);

        private IntPtr native_instance_;
        public ASConfSyncInfoWrapper()
        {
            lm_create_as_conference_sync_info_instance(ref native_instance_);
        }

        ~ASConfSyncInfoWrapper()
        {
            lm_destroy_as_conference_sync_info_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_as_conference_sync_info_instance(native_instance, native_instance_);
        }

        public bool IsSync
        {
            get { return lm_as_conference_sync_info_is_sync(native_instance_) != 0; }
        }

        public String Syncer
        {
            get { return Marshal.PtrToStringAnsi(lm_get_as_conference_sync_info_syncer(native_instance_)); }
        }

        public String SyncData
        {
            get { return Marshal.PtrToStringAnsi(lm_get_as_conference_sync_info_sync_data(native_instance_)); }
        }
    }

}
