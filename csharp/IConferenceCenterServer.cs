﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceCenterServer
    {
        int AddObserver(IConferenceCenterServerObserver observer);
	    int RemoveObserver(IConferenceCenterServerObserver observer);

	    int SetCenterAddress(String address);
	    int GetCenterAddress(StringBuilder address);

	    //以下有回调
	    int GetAuthCode(CSParamWrapper param);
	    int RegUserInfo(CSParamWrapper param, CSUserInfoWrapper info);

	    int Login(CSParamWrapper param, UInt32 id_org, String account, String psw);
	    int Logout(CSParamWrapper param);

	    int PrepareLoginConf(CSParamWrapper param, UInt32 id_conf);
	    int GetRealConf(CSParamWrapper param, int[] listConfID);

	    //用户信息处理（登录帐号后操作）
	    int GetUserInfo(CSParamWrapper param, String strAccount);
	    int UpdateUserInfo(CSParamWrapper param, CSUserInfoWrapper info);
	    int RemoveUserInfo(CSParamWrapper param, CSUserInfoWrapper info);
	    int GetUserInfoList(CSParamWrapper param, CSUserInfoWrapper info);

	    //与注册帐号绑定，删除帐号自动删除对应企业
	    int GetOrgInfo(CSParamWrapper param, UInt32 id_org);
	    int UpdateOrgInfo(CSParamWrapper param, CSOrgInfoWrapper info);
	    int GetOrgInfoList(CSParamWrapper param, CSOrgInfoWrapper info);

	    //用户登录唯一性绑定（可以设置手机号，邮箱等与用户ID关联）
	    int GetUserBind(CSParamWrapper param, String strName);
	    int AddUserBind(CSParamWrapper param, CSUserBindWrapper info);
	    int UpdateUserBind(CSParamWrapper param, CSUserBindWrapper info);
	    int RemoveUserBind(CSParamWrapper param, CSUserBindWrapper info);
	    int GetUserBindList(CSParamWrapper param, CSUserBindWrapper info);

	    //企业人员（或者用户好友）
	    int GetOrgUser(CSParamWrapper param, UInt32 id_org, String strAccount);
	    int AddOrgUser(CSParamWrapper param, CSOrgUserWrapper info);
	    int UpdateOrgUser(CSParamWrapper param, CSOrgUserWrapper info);
	    int RemoveOrgUser(CSParamWrapper param, CSOrgUserWrapper info);
	    int GetOrgUserList(CSParamWrapper param, CSOrgUserWrapper info);

	    //会议室信息管理
	    int GetConfRoom(CSParamWrapper param, UInt32 id_conf);
	    int AddConfRoom(CSParamWrapper param, CSConfRoomWrapper info);
	    int UpdateConfRoom(CSParamWrapper param, CSConfRoomWrapper info);
	    int RemoveConfRoom(CSParamWrapper param, CSConfRoomWrapper info);
	    int GetConfRoomList(CSParamWrapper param, CSConfRoomWrapper info);

	    //会议室验证码登录
	    int GetConfCode(CSParamWrapper param, String strCodeid);
	    int AddConfCode(CSParamWrapper param, CSConfCodeWrapper info);
	    int UpdateConfCode(CSParamWrapper param, CSConfCodeWrapper info);
	    int RemoveConfCode(CSParamWrapper param, CSConfCodeWrapper info);
	    int GetConfCodeList(CSParamWrapper param, CSConfCodeWrapper info);

	    //申请消息管理（申请加好友等）
	    int GetApplyMsg(CSParamWrapper param, UInt32 id_req);
	    int AddApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info);
	    int UpdateApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info);
	    int RemoveApplyMsg(CSParamWrapper param, CSApplyMsgWrapper info);
	    int GetApplyMsgList(CSParamWrapper param, CSApplyMsgWrapper info);

	    //会议邀请（推送消息等）
	    int GetPushMsg(CSParamWrapper param, UInt32 msg_id);
	    int AddPushMsg(CSParamWrapper param, CSPushMsgWrapper info);
	    int UpdatePushMsg(CSParamWrapper param, CSPushMsgWrapper info);
	    int RemovePushMsg(CSParamWrapper param, CSPushMsgWrapper info);
	    int GetPushMsgList(CSParamWrapper param, CSPushMsgWrapper info);

        int GetConfCode(CSParamWrapper param, UInt32 id_conf);

    }
}
