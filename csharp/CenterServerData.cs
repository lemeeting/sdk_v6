﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public class CSApplyMsgWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_apply_msg_instance")]
        private static extern int lm_create_cs_apply_msg_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_apply_msg_instance")]
        private static extern void lm_destroy_cs_apply_msg_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_apply_msg_instance")]
        private static extern void lm_copy_cs_apply_msg_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_version_id")]
        private static extern int lm_get_cs_apply_msg_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_delete_flag")]
        private static extern int lm_get_cs_apply_msg_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_id")]
        private static extern int lm_get_cs_apply_msg_req_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_type")]
        private static extern int lm_get_cs_apply_msg_req_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_state")]
        private static extern int lm_get_cs_apply_msg_req_state(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_result")]
        private static extern int lm_get_cs_apply_msg_req_result(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_time")]
        private static extern long lm_get_cs_apply_msg_req_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_rsp_time")]
        private static extern long lm_get_cs_apply_msg_rsp_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_msg")]
        private static extern IntPtr lm_get_cs_apply_msg_req_msg(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_rsp_msg")]
        private static extern IntPtr lm_get_cs_apply_msg_rsp_msg(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_apply_msg_req_data")]
        private static extern IntPtr lm_get_cs_apply_msg_req_data(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_version_id")]
        private static extern void lm_set_cs_apply_msg_version_id(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_delete_flag")]
        private static extern void lm_set_cs_apply_msg_delete_flag(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_id")]
        private static extern void lm_set_cs_apply_msg_req_id(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_type")]
        private static extern void lm_set_cs_apply_msg_req_type(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_state")]
        private static extern void lm_set_cs_apply_msg_req_state(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_result")]
        private static extern void lm_set_cs_apply_msg_req_result(IntPtr instance, int v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_time")]
        private static extern void lm_set_cs_apply_msg_req_time(IntPtr instance, long v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_rsp_time")]
        private static extern void lm_set_cs_apply_msg_rsp_time(IntPtr instance, long v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_msg")]
        private static extern void lm_set_cs_apply_msg_req_msg(IntPtr instance, String v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_rsp_msg")]
        private static extern void lm_set_cs_apply_msg_rsp_msg(IntPtr instance, String v);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_apply_msg_req_data")]
        private static extern void lm_set_cs_apply_msg_req_data(IntPtr instance, String v);

        private IntPtr native_instance_;
        public CSApplyMsgWrapper()
        {
            lm_create_cs_apply_msg_instance(ref native_instance_);
        }

        ~CSApplyMsgWrapper()
        {
            lm_destroy_cs_apply_msg_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_apply_msg_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_apply_msg_version_id(native_instance_); }
            set { lm_set_cs_apply_msg_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_apply_msg_delete_flag(native_instance_); }
            set { lm_set_cs_apply_msg_delete_flag(native_instance_, value); }
        }

        public int ReqId
        {
            get { return lm_get_cs_apply_msg_req_id(native_instance_); }
            set { lm_set_cs_apply_msg_req_id(native_instance_, value); }
        }

        public int ReqType
        {
            get { return lm_get_cs_apply_msg_req_type(native_instance_); }
            set { lm_set_cs_apply_msg_req_type(native_instance_, value); }
        }

        public int ReqState
        {
            get { return lm_get_cs_apply_msg_req_state(native_instance_); }
            set { lm_set_cs_apply_msg_req_state(native_instance_, value); }
        }

        public int ReqResult
        {
            get { return lm_get_cs_apply_msg_req_result(native_instance_); }
            set { lm_set_cs_apply_msg_req_result(native_instance_, value); }
        }

        public DateTime ReqTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_apply_msg_req_time(native_instance_)); }
            set { lm_set_cs_apply_msg_req_time(native_instance_, value.ToFileTime()); }
        }

        public DateTime RspTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_apply_msg_rsp_time(native_instance_)); }
            set { lm_set_cs_apply_msg_rsp_time(native_instance_, value.ToFileTime()); }
        }

        public String ReqMsg
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_apply_msg_req_msg(native_instance_)); }
            set { lm_set_cs_apply_msg_req_msg(native_instance_, value); }
        }

        public String RspMsg
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_apply_msg_rsp_msg(native_instance_)); }
            set { lm_set_cs_apply_msg_rsp_msg(native_instance_, value); }
        }

        public String ReqData
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_apply_msg_req_data(native_instance_)); }
            set { lm_set_cs_apply_msg_req_data(native_instance_, value); }
        }
    }

    public class CSConfCodeWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_code_instance")]
        private static extern int lm_create_cs_code_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_code_instance")]
        private static extern void lm_destroy_cs_code_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_code_instance")]
        private static extern void lm_copy_cs_code_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_version_id")]
        private static extern int lm_get_cs_code_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_delete_flag")]
        private static extern int lm_get_cs_code_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_type")]
        private static extern int lm_get_cs_code_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_conference_id")]
        private static extern int lm_get_cs_code_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_reserve")]
        private static extern int lm_get_cs_code_reserve(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_code_id")]
        private static extern IntPtr lm_get_cs_code_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_version_id")]
        private static extern void lm_set_cs_code_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_delete_flag")]
        private static extern void lm_set_cs_code_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_type")]
        private static extern void lm_set_cs_code_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_conference_id")]
        private static extern void lm_set_cs_code_conference_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_reserve")]
        private static extern void lm_set_cs_code_reserve(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_code_id")]
        private static extern void lm_set_cs_code_id(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSConfCodeWrapper()
        {
            lm_create_cs_code_instance(ref native_instance_);
        }

        ~CSConfCodeWrapper()
        {
            lm_destroy_cs_code_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_code_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_code_version_id(native_instance_); }
            set { lm_set_cs_code_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_code_delete_flag(native_instance_); }
            set { lm_set_cs_code_delete_flag(native_instance_, value); }
        }

        public int CodeType
        {
            get { return lm_get_cs_code_type(native_instance_); }
            set { lm_set_cs_code_type(native_instance_, value); }
        }

        public int ConferenceId
        {
            get { return lm_get_cs_code_conference_id(native_instance_); }
            set { lm_set_cs_code_conference_id(native_instance_, value); }
        }

        public int Reserver
        {
            get { return lm_get_cs_code_reserve(native_instance_); }
            set { lm_set_cs_code_reserve(native_instance_, value); }
        }

        public String CodeId
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_code_id(native_instance_)); }
            set { lm_set_cs_code_id(native_instance_, value); }
        }

    }

    public class CSConfRoomWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_room_instance")]
        private static extern int lm_create_cs_room_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_room_instance")]
        private static extern void lm_destroy_cs_room_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_room_instance")]
        private static extern void lm_copy_cs_room_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_version_id")]
        private static extern int lm_get_cs_room_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_delete_flag")]
        private static extern int lm_get_cs_room_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_id")]
        private static extern int lm_get_cs_room_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_type")]
        private static extern int lm_get_cs_room_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_world_id")]
        private static extern int lm_get_cs_room_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_group_id")]
        private static extern int lm_get_cs_room_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_server_id")]
        private static extern int lm_get_cs_room_server_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_max_user_nums")]
        private static extern int lm_get_cs_room_max_user_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_max_speaker_nums")]
        private static extern int lm_get_cs_room_max_speaker_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_flag")]
        private static extern int lm_get_cs_room_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_start_time")]
        private static extern long lm_get_cs_room_start_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_end_time")]
        private static extern long lm_get_cs_room_end_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_create_time")]
        private static extern long lm_get_cs_room_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_modify_time")]
        private static extern long lm_get_cs_room_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_name")]
        private static extern IntPtr lm_get_cs_room_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_conf_password")]
        private static extern IntPtr lm_get_cs_room_conf_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_manager_password")]
        private static extern IntPtr lm_get_cs_room_manager_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_creator")]
        private static extern IntPtr lm_get_cs_room_creator(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_mender")]
        private static extern IntPtr lm_get_cs_room_mender(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_setting_json")]
        private static extern IntPtr lm_get_cs_room_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_room_extend_json")]
        private static extern IntPtr lm_get_cs_room_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_version_id")]
        private static extern void lm_set_cs_room_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_delete_flag")]
        private static extern void lm_set_cs_room_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_id")]
        private static extern void lm_set_cs_room_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_type")]
        private static extern void lm_set_cs_room_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_world_id")]
        private static extern void lm_set_cs_room_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_group_id")]
        private static extern void lm_set_cs_room_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_server_id")]
        private static extern void lm_set_cs_room_server_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_max_user_nums")]
        private static extern void lm_set_cs_room_max_user_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_max_speaker_nums")]
        private static extern void lm_set_cs_room_max_speaker_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_flag")]
        private static extern void lm_set_cs_room_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_start_time")]
        private static extern void lm_set_cs_room_start_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_end_time")]
        private static extern void lm_set_cs_room_end_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_create_time")]
        private static extern void lm_set_cs_room_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_modify_time")]
        private static extern void lm_set_cs_room_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_name")]
        private static extern void lm_set_cs_room_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_conf_password")]
        private static extern void lm_set_cs_room_conf_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_manager_password")]
        private static extern void lm_set_cs_room_manager_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_creator")]
        private static extern void lm_set_cs_room_creator(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_mender")]
        private static extern void lm_set_cs_room_mender(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_setting_json")]
        private static extern void lm_set_cs_room_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_room_extend_json")]
        private static extern void lm_set_cs_room_extend_json(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSConfRoomWrapper()
        {
            lm_create_cs_room_instance(ref native_instance_);
        }

        ~CSConfRoomWrapper()
        {
            lm_destroy_cs_room_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_room_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_room_version_id(native_instance_); }
            set { lm_set_cs_room_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_room_delete_flag(native_instance_); }
            set { lm_set_cs_room_delete_flag(native_instance_, value); }
        }

        public int ConferenceId
        {
            get { return lm_get_cs_room_id(native_instance_); }
            set { lm_set_cs_room_id(native_instance_, value); }
        }

        public int ConfType
        {
            get { return lm_get_cs_room_type(native_instance_); }
            set { lm_set_cs_room_type(native_instance_, value); }
        }

        public int WorldId
        {
            get { return lm_get_cs_room_world_id(native_instance_); }
            set { lm_set_cs_room_world_id(native_instance_, value); }
        }

        public int GroupId
        {
            get { return lm_get_cs_room_group_id(native_instance_); }
            set { lm_set_cs_room_group_id(native_instance_, value); }
        }

        public int ServerId
        {
            get { return lm_get_cs_room_server_id(native_instance_); }
            set { lm_set_cs_room_server_id(native_instance_, value); }
        }

        public String ConfName
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_name(native_instance_)); }
            set { lm_set_cs_room_name(native_instance_, value); }
        }

        public String ConfPSW
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_conf_password(native_instance_)); }
            set { lm_set_cs_room_conf_password(native_instance_, value); }
        }

        public String ManagerPSW
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_manager_password(native_instance_)); }
            set { lm_set_cs_room_manager_password(native_instance_, value); }
        }

        public DateTime StartTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_room_start_time(native_instance_)); }
            set { lm_set_cs_room_start_time(native_instance_, value.ToFileTime()); }
        }

        public DateTime EndTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_room_end_time(native_instance_)); }
            set { lm_set_cs_room_end_time(native_instance_, value.ToFileTime()); }
        }

        public int MaxUserCount
        {
            get { return lm_get_cs_room_max_user_nums(native_instance_); }
            set { lm_set_cs_room_max_user_nums(native_instance_, value); }
        }

        public int MaxSpeakerCount
        {
            get { return lm_get_cs_room_max_speaker_nums(native_instance_); }
            set { lm_set_cs_room_max_speaker_nums(native_instance_, value); }
        }

        public String Creator
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_creator(native_instance_)); }
            set { lm_set_cs_room_creator(native_instance_, value); }
        }

        public String Mender
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_mender(native_instance_)); }
            set { lm_set_cs_room_mender(native_instance_, value); }
        }

        public DateTime CreateTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_room_create_time(native_instance_)); }
            set { lm_set_cs_room_create_time(native_instance_, value.ToFileTime()); }
        }

        public DateTime ModifyTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_room_modify_time(native_instance_)); }
            set { lm_set_cs_room_modify_time(native_instance_, value.ToFileTime()); }
        }

        public int ConfFlag
        {
            get { return lm_get_cs_room_flag(native_instance_); }
            set { lm_set_cs_room_flag(native_instance_, value); }
        }

        public String SettingJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_setting_json(native_instance_)); }
            set { lm_set_cs_room_setting_json(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_room_extend_json(native_instance_)); }
            set { lm_set_cs_room_extend_json(native_instance_, value); }
        }
    }

    public class CSOrgInfoWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_org_instance")]
        private static extern int lm_create_cs_org_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_org_instance")]
        private static extern void lm_destroy_cs_org_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_org_instance")]
        private static extern void lm_copy_cs_org_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_version_id")]
        private static extern int lm_get_cs_org_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_delete_flag")]
        private static extern int lm_get_cs_org_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_world_id")]
        private static extern int lm_get_cs_org_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_type")]
        private static extern int lm_get_cs_org_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_flag")]
        private static extern int lm_get_cs_org_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_status")]
        private static extern int lm_get_cs_org_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_group_id")]
        private static extern int lm_get_cs_org_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_max_user_nums")]
        private static extern int lm_get_cs_org_max_user_nums(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_name")]
        private static extern IntPtr lm_get_cs_org_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_show_name")]
        private static extern IntPtr lm_get_cs_org_show_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_contacter")]
        private static extern IntPtr lm_get_cs_org_contacter(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_phone")]
        private static extern IntPtr lm_get_cs_org_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_email")]
        private static extern IntPtr lm_get_cs_org_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_website")]
        private static extern IntPtr lm_get_cs_org_website(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_birthday")]
        private static extern long lm_get_cs_org_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_create_time")]
        private static extern long lm_get_cs_org_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_modify_time")]
        private static extern long lm_get_cs_org_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_creator")]
        private static extern IntPtr lm_get_cs_org_creator(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_mender")]
        private static extern IntPtr lm_get_cs_org_mender(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_setting_json")]
        private static extern IntPtr lm_get_cs_org_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_extend_json")]
        private static extern IntPtr lm_get_cs_org_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_address")]
        private static extern IntPtr lm_get_cs_org_address(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_version_id")]
        private static extern void lm_set_cs_org_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_delete_flag")]
        private static extern void lm_set_cs_org_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_world_id")]
        private static extern void lm_set_cs_org_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_type")]
        private static extern void lm_set_cs_org_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_flag")]
        private static extern void lm_set_cs_org_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_status")]
        private static extern void lm_set_cs_org_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_group_id")]
        private static extern void lm_set_cs_org_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_max_user_nums")]
        private static extern void lm_set_cs_org_max_user_nums(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_name")]
        private static extern void lm_set_cs_org_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_show_name")]
        private static extern void lm_set_cs_org_show_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_contacter")]
        private static extern void lm_set_cs_org_contacter(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_phone")]
        private static extern void lm_set_cs_org_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_email")]
        private static extern void lm_set_cs_org_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_website")]
        private static extern void lm_set_cs_org_website(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_birthday")]
        private static extern void lm_set_cs_org_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_create_time")]
        private static extern void lm_set_cs_org_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_modify_time")]
        private static extern void lm_set_cs_org_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_creator")]
        private static extern void lm_set_cs_org_creator(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_mender")]
        private static extern void lm_set_cs_org_mender(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_setting_json")]
        private static extern void lm_set_cs_org_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_extend_json")]
        private static extern void lm_set_cs_org_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_address")]
        private static extern void lm_set_cs_org_address(IntPtr instance, String value);


        private IntPtr native_instance_;
        public CSOrgInfoWrapper()
        {
            lm_create_cs_org_instance(ref native_instance_);
        }

        ~CSOrgInfoWrapper()
        {
            lm_destroy_cs_org_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_org_instance(native_instance, native_instance_);
        }

 
        public int VersionId
        {
            get { return lm_get_cs_org_version_id(native_instance_); }
            set { lm_set_cs_org_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_org_delete_flag(native_instance_); }
            set { lm_set_cs_org_delete_flag(native_instance_, value); }
        }

        public int WorldId
        {
            get { return lm_get_cs_org_world_id(native_instance_); }
            set { lm_set_cs_org_world_id(native_instance_, value); }
        }

        public int OrgType
        {
            get { return lm_get_cs_org_type(native_instance_); }
            set { lm_set_cs_org_type(native_instance_, value); }
        }

        public int OrgStatus
        {
            get { return lm_get_cs_org_status(native_instance_); }
            set { lm_set_cs_org_status(native_instance_, value); }
        }

        public int GroupId
        {
            get { return lm_get_cs_org_group_id(native_instance_); }
            set { lm_set_cs_org_group_id(native_instance_, value); }
        }

        public int MaxUserCount
        {
            get { return lm_get_cs_org_max_user_nums(native_instance_); }
            set { lm_set_cs_org_max_user_nums(native_instance_, value); }
        }

        public String OrgName
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_name(native_instance_)); }
            set { lm_set_cs_org_name(native_instance_, value); }
        }

        public String ShowName
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_show_name(native_instance_)); }
            set { lm_set_cs_org_show_name(native_instance_, value); }
        }

        public String Address
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_address(native_instance_)); }
            set { lm_set_cs_org_address(native_instance_, value); }
        }

        public String Contacter
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_contacter(native_instance_)); }
            set { lm_set_cs_org_contacter(native_instance_, value); }
        }

        public String Phone
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_phone(native_instance_)); }
            set { lm_set_cs_org_phone(native_instance_, value); }
        }

        public String Email
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_email(native_instance_)); }
            set { lm_set_cs_org_email(native_instance_, value); }
        }

        public String Website
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_website(native_instance_)); }
            set { lm_set_cs_org_website(native_instance_, value); }
        }

        public DateTime Birthday
        {
            get { return DateTime.FromFileTime(lm_get_cs_org_birthday(native_instance_)); }
            set { lm_set_cs_org_birthday(native_instance_, value.ToFileTime()); }
        }

        public DateTime CreateTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_org_create_time(native_instance_)); }
            set { lm_set_cs_org_create_time(native_instance_, value.ToFileTime()); }
        }

        public DateTime ModifyTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_org_modify_time(native_instance_)); }
            set { lm_set_cs_org_modify_time(native_instance_, value.ToFileTime()); }
        }

        public String Creator
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_creator(native_instance_)); }
            set { lm_set_cs_org_creator(native_instance_, value); }
        }

        public String Mender
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_mender(native_instance_)); }
            set { lm_set_cs_org_mender(native_instance_, value); }
        }

        public int OrgFlag
        {
            get { return lm_get_cs_org_flag(native_instance_); }
            set { lm_set_cs_org_flag(native_instance_, value); }
        }

        public String SettingJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_setting_json(native_instance_)); }
            set { lm_set_cs_org_setting_json(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_setting_json(native_instance_)); }
            set { lm_set_cs_org_extend_json(native_instance_, value); }
        }
    }

    public class CSOrgUserWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_org_user_instance")]
        private static extern int lm_create_cs_org_user_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_org_user_instance")]
        private static extern void lm_destroy_cs_org_user_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_org_user_instance")]
        private static extern void lm_copy_cs_org_user_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_version_id")]
        private static extern int lm_get_cs_org_user_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_delete_flag")]
        private static extern int lm_get_cs_org_user_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_org_id")]
        private static extern int lm_get_cs_org_user_org_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_type")]
        private static extern int lm_get_cs_org_user_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_status")]
        private static extern int lm_get_cs_org_user_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_bind_user")]
        private static extern int lm_get_cs_org_user_bind_user(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_birthday")]
        private static extern long lm_get_cs_org_user_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_create_time")]
        private static extern long lm_get_cs_org_user_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_perm")]
        private static extern long lm_get_cs_org_user_perm(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_account")]
        private static extern IntPtr lm_get_cs_org_user_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_password")]
        private static extern IntPtr lm_get_cs_org_user_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_phone")]
        private static extern IntPtr lm_get_cs_org_user_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_email")]
        private static extern IntPtr lm_get_cs_org_user_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_setting_json")]
        private static extern IntPtr lm_get_cs_org_user_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_extend_json")]
        private static extern IntPtr lm_get_cs_org_user_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_org_user_name")]
        private static extern IntPtr lm_get_cs_org_user_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_version_id")]
        private static extern void lm_set_cs_org_user_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_delete_flag")]
        private static extern void lm_set_cs_org_user_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_org_id")]
        private static extern void lm_set_cs_org_user_org_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_type")]
        private static extern void lm_set_cs_org_user_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_status")]
        private static extern void lm_set_cs_org_user_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_bind_user")]
        private static extern void lm_set_cs_org_user_bind_user(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_birthday")]
        private static extern void lm_set_cs_org_user_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_create_time")]
        private static extern void lm_set_cs_org_user_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_perm")]
        private static extern void lm_set_cs_org_user_perm(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_account")]
        private static extern void lm_set_cs_org_user_account(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_password")]
        private static extern void lm_set_cs_org_user_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_phone")]
        private static extern void lm_set_cs_org_user_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_email")]
        private static extern void lm_set_cs_org_user_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_setting_json")]
        private static extern void lm_set_cs_org_user_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_extend_json")]
        private static extern void lm_set_cs_org_user_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_org_user_name")]
        private static extern void lm_set_cs_org_user_name(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSOrgUserWrapper()
        {
            lm_create_cs_org_user_instance(ref native_instance_);
        }

        ~CSOrgUserWrapper()
        {
            lm_destroy_cs_org_user_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_org_user_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_org_user_version_id(native_instance_); }
            set { lm_set_cs_org_user_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_org_user_delete_flag(native_instance_); }
            set { lm_set_cs_org_user_delete_flag(native_instance_, value); }
        }

        public int OrgId
        {
            get { return lm_get_cs_org_user_org_id(native_instance_); }
            set { lm_set_cs_org_user_org_id(native_instance_, value); }
        }

        public String UserAccount
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_account(native_instance_)); }
            set { lm_set_cs_org_user_account(native_instance_, value); }
        }

        public int UserType
        {
            get { return lm_get_cs_org_user_type(native_instance_); }
            set { lm_set_cs_org_user_type(native_instance_, value); }
        }

        public int UserStatus
        {
            get { return lm_get_cs_org_user_status(native_instance_); }
            set { lm_set_cs_org_user_status(native_instance_, value); }
        }

        public int BindUser
        {
            get { return lm_get_cs_org_user_bind_user(native_instance_); }
            set { lm_set_cs_org_user_bind_user(native_instance_, value); }
        }

        public String UserName
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_name(native_instance_)); }
            set { lm_set_cs_org_user_name(native_instance_, value); }
        }

        public String UserPSW
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_password(native_instance_)); }
            set { lm_set_cs_org_user_password(native_instance_, value); }
        }

        public String Phone
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_phone(native_instance_)); }
            set { lm_set_cs_org_user_phone(native_instance_, value); }
        }

        public String Email
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_email(native_instance_)); }
            set { lm_set_cs_org_user_email(native_instance_, value); }
        }

        public DateTime Birthday
        {
            get { return DateTime.FromFileTime(lm_get_cs_org_user_birthday(native_instance_)); }
            set { lm_set_cs_org_user_birthday(native_instance_, value.ToFileTime()); }
        }

        public DateTime CreateTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_org_user_create_time(native_instance_)); }
            set { lm_set_cs_org_user_create_time(native_instance_, value.ToFileTime()); }
        }

        public String SettingJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_setting_json(native_instance_)); }
            set { lm_set_cs_org_user_setting_json(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_org_user_extend_json(native_instance_)); }
            set { lm_set_cs_org_user_extend_json(native_instance_, value); }
        }

        public long UserPerm
        {
            get { return lm_get_cs_org_user_perm(native_instance_); }
            set { lm_set_cs_org_user_perm(native_instance_, value); }
        }
    }

    public class CSPushMsgWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_push_msg_instance")]
        private static extern int lm_create_cs_push_msg_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_push_msg_instance")]
        private static extern void lm_destroy_cs_push_msg_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_push_msg_instance")]
        private static extern void lm_copy_cs_push_msg_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_version_id")]
        private static extern int lm_get_cs_push_msg_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_delete_flag")]
        private static extern int lm_get_cs_push_msg_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_id")]
        private static extern int lm_get_cs_push_msg_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_type")]
        private static extern int lm_get_cs_push_msg_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_state")]
        private static extern int lm_get_cs_push_msg_state(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_result")]
        private static extern int lm_get_cs_push_msg_result(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_send_org_id")]
        private static extern int lm_get_cs_push_msg_send_org_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_send_time")]
        private static extern long lm_get_cs_push_msg_send_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_end_time")]
        private static extern long lm_get_cs_push_msg_end_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_topic")]
        private static extern IntPtr lm_get_cs_push_msg_topic(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_send_account")]
        private static extern IntPtr lm_get_cs_push_msg_send_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_data")]
        private static extern IntPtr lm_get_cs_push_msg_data(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_receiver")]
        private static extern IntPtr lm_get_cs_push_msg_receiver(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_push_msg_extend_json")]
        private static extern IntPtr lm_get_cs_push_msg_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_version_id")]
        private static extern void lm_set_cs_push_msg_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_delete_flag")]
        private static extern void lm_set_cs_push_msg_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_id")]
        private static extern void lm_set_cs_push_msg_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_type")]
        private static extern void lm_set_cs_push_msg_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_state")]
        private static extern void lm_set_cs_push_msg_state(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_result")]
        private static extern void lm_set_cs_push_msg_result(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_send_org_id")]
        private static extern void lm_set_cs_push_msg_send_org_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_send_time")]
        private static extern void lm_set_cs_push_msg_send_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_end_time")]
        private static extern void lm_set_cs_push_msg_end_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_topic")]
        private static extern void lm_set_cs_push_msg_topic(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_send_account")]
        private static extern void lm_set_cs_push_msg_send_account(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_data")]
        private static extern void lm_set_cs_push_msg_data(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_receiver")]
        private static extern void lm_set_cs_push_msg_receiver(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_push_msg_extend_json")]
        private static extern void lm_set_cs_push_msg_extend_json(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSPushMsgWrapper()
        {
            lm_create_cs_push_msg_instance(ref native_instance_);
        }

        ~CSPushMsgWrapper()
        {
            lm_destroy_cs_push_msg_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_push_msg_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_push_msg_version_id(native_instance_); }
            set { lm_set_cs_push_msg_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_push_msg_delete_flag(native_instance_); }
            set { lm_set_cs_push_msg_delete_flag(native_instance_, value); }
        }

        public int MsgId
        {
            get { return lm_get_cs_push_msg_id(native_instance_); }
            set { lm_set_cs_push_msg_id(native_instance_, value); }
        }

        public int MsgType
        {
            get { return lm_get_cs_push_msg_type(native_instance_); }
            set { lm_set_cs_push_msg_type(native_instance_, value); }
        }

        public int MsgState
        {
            get { return lm_get_cs_push_msg_state(native_instance_); }
            set { lm_set_cs_push_msg_state(native_instance_, value); }
        }

        public int MsgResult
        {
            get { return lm_get_cs_push_msg_result(native_instance_); }
            set { lm_set_cs_push_msg_result(native_instance_, value); }
        }

        public int SendOrgId
        {
            get { return lm_get_cs_push_msg_send_org_id(native_instance_); }
            set { lm_set_cs_push_msg_send_org_id(native_instance_, value); }
        }

        public String SendAccount
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_push_msg_send_account(native_instance_)); }
            set { lm_set_cs_push_msg_send_account(native_instance_, value); }
        }

        public DateTime SendTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_push_msg_send_time(native_instance_)); }
            set { lm_set_cs_push_msg_send_time(native_instance_, value.ToFileTime()); }
        }

        public DateTime EndTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_push_msg_end_time(native_instance_)); }
            set { lm_set_cs_push_msg_end_time(native_instance_, value.ToFileTime()); }
        }

        public String MsgTopic
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_push_msg_topic(native_instance_)); }
            set { lm_set_cs_push_msg_topic(native_instance_, value); }
        }
        
        public String MsgData
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_push_msg_data(native_instance_)); }
            set { lm_set_cs_push_msg_data(native_instance_, value); }
        }

        public String MsgReceiver
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_push_msg_receiver(native_instance_)); }
            set { lm_set_cs_push_msg_receiver(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_push_msg_extend_json(native_instance_)); }
            set { lm_set_cs_push_msg_extend_json(native_instance_, value); }
        }
    }

    public class CSUserBindWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_user_bind_instance")]
        private static extern int lm_create_cs_user_bind_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_user_bind_instance")]
        private static extern void lm_destroy_cs_user_bind_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_user_bind_instance")]
        private static extern void lm_copy_cs_user_bind_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_version_id")]
        private static extern int lm_get_cs_user_bind_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_delete_flag")]
        private static extern int lm_get_cs_user_bind_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_world_id")]
        private static extern int lm_get_cs_user_bind_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_type")]
        private static extern int lm_get_cs_user_bind_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_create_time")]
        private static extern long lm_get_cs_user_bind_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_name")]
        private static extern IntPtr lm_get_cs_user_bind_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_bind_extend_json")]
        private static extern IntPtr lm_get_cs_user_bind_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_version_id")]
        private static extern void lm_set_cs_user_bind_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_delete_flag")]
        private static extern void lm_set_cs_user_bind_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_world_id")]
        private static extern void lm_set_cs_user_bind_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_type")]
        private static extern void lm_set_cs_user_bind_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_create_time")]
        private static extern void lm_set_cs_user_bind_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_name")]
        private static extern void lm_set_cs_user_bind_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_bind_extend_json")]
        private static extern void lm_set_cs_user_bind_extend_json(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSUserBindWrapper()
        {
            lm_create_cs_user_bind_instance(ref native_instance_);
        }

        ~CSUserBindWrapper()
        {
            lm_destroy_cs_user_bind_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_user_bind_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_user_bind_version_id(native_instance_); }
            set { lm_set_cs_user_bind_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_user_bind_delete_flag(native_instance_); }
            set { lm_set_cs_user_bind_delete_flag(native_instance_, value); }
        }

        public String Name
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_bind_name(native_instance_)); }
            set { lm_set_cs_user_bind_name(native_instance_, value); }
        }

        public int WorldId
        {
            get { return lm_get_cs_user_bind_world_id(native_instance_); }
            set { lm_set_cs_user_bind_world_id(native_instance_, value); }
        }

        public int Type
        {
            get { return lm_get_cs_user_bind_type(native_instance_); }
            set { lm_set_cs_user_bind_type(native_instance_, value); }
        }

        public DateTime CreateTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_user_bind_create_time(native_instance_)); }
            set { lm_set_cs_user_bind_create_time(native_instance_, value.ToFileTime()); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_bind_extend_json(native_instance_)); }
            set { lm_set_cs_user_bind_extend_json(native_instance_, value); }
        }
    }

    public class CSUserInfoWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_user_instance")]
        private static extern int lm_create_cs_user_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_user_instance")]
        private static extern void lm_destroy_cs_user_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_user_instance")]
        private static extern void lm_copy_cs_user_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_version_id")]
        private static extern int lm_get_cs_user_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_delete_flag")]
        private static extern int lm_get_cs_user_delete_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_world_id")]
        private static extern int lm_get_cs_user_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_type")]
        private static extern int lm_get_cs_user_type(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_status")]
        private static extern int lm_get_cs_user_status(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_birthday")]
        private static extern long lm_get_cs_user_birthday(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_create_time")]
        private static extern long lm_get_cs_user_create_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_name")]
        private static extern IntPtr lm_get_cs_user_name(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_password")]
        private static extern IntPtr lm_get_cs_user_password(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_phone")]
        private static extern IntPtr lm_get_cs_user_phone(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_email")]
        private static extern IntPtr lm_get_cs_user_email(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_setting_json")]
        private static extern IntPtr lm_get_cs_user_setting_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_extend_json")]
        private static extern IntPtr lm_get_cs_user_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_user_account")]
        private static extern IntPtr lm_get_cs_user_account(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_version_id")]
        private static extern void lm_set_cs_user_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_delete_flag")]
        private static extern void lm_set_cs_user_delete_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_world_id")]
        private static extern void lm_set_cs_user_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_type")]
        private static extern void lm_set_cs_user_type(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_status")]
        private static extern void lm_set_cs_user_status(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_birthday")]
        private static extern void lm_set_cs_user_birthday(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_create_time")]
        private static extern void lm_set_cs_user_create_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_name")]
        private static extern void lm_set_cs_user_name(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_password")]
        private static extern void lm_set_cs_user_password(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_phone")]
        private static extern void lm_set_cs_user_phone(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_email")]
        private static extern void lm_set_cs_user_email(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_setting_json")]
        private static extern void lm_set_cs_user_setting_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_extend_json")]
        private static extern void lm_set_cs_user_extend_json(IntPtr instance, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_user_account")]
        private static extern void lm_set_cs_user_account(IntPtr instance, String value);


        private IntPtr native_instance_;
        public CSUserInfoWrapper()
        {
            lm_create_cs_user_instance(ref native_instance_);
        }

        ~CSUserInfoWrapper()
        {
            lm_destroy_cs_user_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_user_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_user_version_id(native_instance_); }
            set { lm_set_cs_user_version_id(native_instance_, value); }
        }

        public int DeleteFlag
        {
            get { return lm_get_cs_user_delete_flag(native_instance_); }
            set { lm_set_cs_user_delete_flag(native_instance_, value); }
        }

        public int WorldId
        {
            get { return lm_get_cs_user_world_id(native_instance_); }
            set { lm_set_cs_user_world_id(native_instance_, value); }
        }

        public String UserAccount
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_account(native_instance_)); }
            set { lm_set_cs_user_account(native_instance_, value); }
        }

        public int UserType
        {
            get { return lm_get_cs_user_type(native_instance_); }
            set { lm_set_cs_user_type(native_instance_, value); }
        }

        public int UserStatus
        {
            get { return lm_get_cs_user_status(native_instance_); }
            set { lm_set_cs_user_status(native_instance_, value); }
        }

        public String UserName
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_name(native_instance_)); }
            set { lm_set_cs_user_name(native_instance_, value); }
        }

        public String UserPSW
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_password(native_instance_)); }
            set { lm_set_cs_user_password(native_instance_, value); }
        }

        public String Phone
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_phone(native_instance_)); }
            set { lm_set_cs_user_phone(native_instance_, value); }
        }

        public String Email
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_email(native_instance_)); }
            set { lm_set_cs_user_email(native_instance_, value); }
        }

        public DateTime Birthday
        {
            get { return DateTime.FromFileTime(lm_get_cs_user_birthday(native_instance_)); }
            set { lm_set_cs_user_birthday(native_instance_, value.ToFileTime()); }
        }

        public DateTime CreateTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_user_create_time(native_instance_)); }
            set { lm_set_cs_user_create_time(native_instance_, value.ToFileTime()); }
        }

        public String SettingJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_setting_json(native_instance_)); }
            set { lm_set_cs_user_setting_json(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_user_extend_json(native_instance_)); }
            set { lm_set_cs_user_extend_json(native_instance_, value); }
        }
    }

    public class CSRealTimeConfWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_real_conf_instance")]
        private static extern int lm_create_cs_real_conf_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_real_conf_instance")]
        private static extern void lm_destroy_cs_real_conf_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_real_conf_instance")]
        private static extern void lm_copy_cs_real_conf_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_version_id")]
        private static extern int lm_get_cs_real_conf_version_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_conference_id")]
        private static extern int lm_get_cs_real_conf_conference_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_world_id")]
        private static extern int lm_get_cs_real_conf_world_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_group_id")]
        private static extern int lm_get_cs_real_conf_group_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_server_id")]
        private static extern int lm_get_cs_real_conf_server_id(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_online_users")]
        private static extern int lm_get_cs_real_conf_online_users(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_flag")]
        private static extern int lm_get_cs_real_conf_flag(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_modify_time")]
        private static extern long lm_get_cs_real_conf_modify_time(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_real_conf_extend_json")]
        private static extern IntPtr lm_get_cs_real_conf_extend_json(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_version_id")]
        private static extern void lm_set_cs_real_conf_version_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_conference_id")]
        private static extern void lm_set_cs_real_conf_conference_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_world_id")]
        private static extern void lm_set_cs_real_conf_world_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_group_id")]
        private static extern void lm_set_cs_real_conf_group_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_server_id")]
        private static extern void lm_set_cs_real_conf_server_id(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_online_users")]
        private static extern void lm_set_cs_real_conf_online_users(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_flag")]
        private static extern void lm_set_cs_real_conf_flag(IntPtr instance, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_modify_time")]
        private static extern void lm_set_cs_real_conf_modify_time(IntPtr instance, long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_real_conf_extend_json")]
        private static extern void lm_set_cs_real_conf_extend_json(IntPtr instance, String value);

        private IntPtr native_instance_;
        public CSRealTimeConfWrapper()
        {
            lm_create_cs_real_conf_instance(ref native_instance_);
        }

        ~CSRealTimeConfWrapper()
        {
            lm_destroy_cs_real_conf_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_real_conf_instance(native_instance, native_instance_);
        }

        public int VersionId
        {
            get { return lm_get_cs_real_conf_version_id(native_instance_); }
            set { lm_set_cs_real_conf_version_id(native_instance_, value); }
        }

        public int ConfId
        {
            get { return lm_get_cs_real_conf_conference_id(native_instance_); }
            set { lm_set_cs_real_conf_conference_id(native_instance_, value); }
        }

        public int WorldId
        {
            get { return lm_get_cs_real_conf_world_id(native_instance_); }
            set { lm_set_cs_real_conf_world_id(native_instance_, value); }
        }

        public int GroupId
        {
            get { return lm_get_cs_real_conf_group_id(native_instance_); }
            set { lm_set_cs_real_conf_group_id(native_instance_, value); }
        }

        public int ServerId
        {
            get { return lm_get_cs_real_conf_server_id(native_instance_); }
            set { lm_set_cs_real_conf_server_id(native_instance_, value); }
        }

        public int OnlineNumsers
        {
            get { return lm_get_cs_real_conf_online_users(native_instance_); }
            set { lm_set_cs_real_conf_online_users(native_instance_, value); }
        }

        public DateTime ModifyTime
        {
            get { return DateTime.FromFileTime(lm_get_cs_real_conf_modify_time(native_instance_)); }
            set { lm_set_cs_real_conf_modify_time(native_instance_, value.ToFileTime()); }
        }

        public int Flags
        {
            get { return lm_get_cs_real_conf_flag(native_instance_); }
            set { lm_set_cs_real_conf_flag(native_instance_, value); }
        }

        public String ExtendJson
        {
            get { return Marshal.PtrToStringAnsi(lm_get_cs_real_conf_extend_json(native_instance_)); }
            set { lm_set_cs_real_conf_extend_json(native_instance_, value); }
        }
    }

    public class CSParamWrapper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_create_cs_param_instance")]
        private static extern int lm_create_cs_param_instance(ref IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_destroy_cs_param_instance")]
        private static extern void lm_destroy_cs_param_instance(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_copy_cs_param_instance")]
        private static extern void lm_copy_cs_param_instance(IntPtr srcinstance, IntPtr dstinstance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_count")]
        private static extern int lm_get_cs_param_count(IntPtr instance);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_contains_key")]
        private static extern int lm_get_cs_param_contains_key(IntPtr instance, int key);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_keys")]
        private static extern int lm_get_cs_param_keys(IntPtr instance, ref int keys, int count);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_int_value")]
        private static extern int lm_get_cs_param_int_value(IntPtr instance, int key, ref int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_uint_value")]
        private static extern int lm_get_cs_param_uint_value(IntPtr instance, int key, ref uint value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_string_value")]
        private static extern int lm_get_cs_param_string_value(IntPtr instance, int key, StringBuilder value, int size);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_cs_param_time_value")]
        private static extern int lm_get_cs_param_time_value(IntPtr instance, int key, ref long value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_param_uint_value")]
        private static extern void lm_set_cs_param_uint_value(IntPtr instance, int key, uint value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_param_int_value")]
        private static extern void lm_set_cs_param_int_value(IntPtr instance, int key, int value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_param_string_value")]
        private static extern void lm_set_cs_param_string_value(IntPtr instance, int key, String value);
        [DllImport("c_wrapper.dll", EntryPoint = "lm_set_cs_param_time_value")]
        private static extern void lm_set_cs_param_time_value(IntPtr instance, int key, long value);

        private IntPtr native_instance_;
        public CSParamWrapper()
        {
            lm_create_cs_param_instance(ref native_instance_);
        }

        ~CSParamWrapper()
        {
            lm_destroy_cs_param_instance(native_instance_);
        }

        internal IntPtr NativeInstance
        {
            get { return native_instance_; }
            set { native_instance_ = value; }
        }

        internal void CopyNativeInstance(IntPtr native_instance)
        {
            lm_copy_cs_param_instance(native_instance, native_instance_);
        }

        public int KeyCount()
        {
            return lm_get_cs_param_count(native_instance_);
        }

        public bool ContainsKey(int key) {
            return lm_get_cs_param_contains_key(native_instance_, key) != 0;
        }
        
        int GetIntValue(int key)
        {
            int v = -1;
            if (lm_get_cs_param_int_value(native_instance_, key, ref v) == 0)
                return v;
            else return -1;
        }

	    public void SetIntValue(int key, int value)
	    {
            lm_set_cs_param_int_value(native_instance_, key, value);
	    }

	    uint GetUIntValue(int key)
	    {
            uint v = 0xffffffff;
            if (lm_get_cs_param_uint_value(native_instance_, key, ref v) == 0)
                return v;
            else return 0xffffffff;
        }

	    void SetUIntValue(int key, uint value)
	    {
            lm_set_cs_param_uint_value(native_instance_, key, value);
        }

	    String GetStringValue(int key) 
	    {
            StringBuilder s = new StringBuilder(512);
            if (lm_get_cs_param_string_value(native_instance_, key, s, 512) == 0)
                return s.ToString();
            else return "";
	    }

	    void SetStringValue(int key, String value)
	    {
            lm_set_cs_param_string_value(native_instance_, key, value);
        }

	    DateTime GetDateTimeValue(int key)
	    {
            long ll = 0;
            if (lm_get_cs_param_time_value(native_instance_, key, ref ll) == 0)
                return DateTime.FromFileTime(ll);
            else return new DateTime();
	    }

        void SetDateTimeValue(int key, DateTime value)
	    {
            lm_set_cs_param_time_value(native_instance_, key, value.ToFileTime());
	    }
    }
}
