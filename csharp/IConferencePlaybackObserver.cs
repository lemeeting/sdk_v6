﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferencePlaybackObserver
    {
        void OnAddSession(UInt32 rec_session, LMRecSessionType rec_type,
			String account, String name);

        void OnRemoveSession(UInt32 rec_session, LMRecSessionType rec_type);

		void OnSessionVideoResolutionChanged(UInt32 rec_session,
			int old_width, int old_height, int new_width, int new_height);

		void OnPlaybackOpen(int error);
		void OnPlaybackPlay(int error);
		void OnPlaybackPause(int error);
		void OnPlaybackContinue(int error);
		void OnPlaybackComplete();
    }
}
