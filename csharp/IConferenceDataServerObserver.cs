﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceDataServerObserver
    {
        void OnPingResult(UInt32 task_id, LMPingResult result, long context, int error);
    }
}
