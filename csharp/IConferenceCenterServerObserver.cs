﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceCenterServerObserver
    {
        void OnDisconnectCenter(int result);

		void OnGetAuthCode(int result, CSParamWrapper param, String strCode);
		void OnRegUserInfo(int result, CSParamWrapper param, CSUserInfoWrapper info);

		void OnLogin(int result, CSParamWrapper param, CSUserInfoWrapper userInfo);
		void OnLogout(int result, CSParamWrapper param);

		void OnPrepareLoginConf(int result, CSParamWrapper param, UInt32 id_conf, String jsonAddress);
        void OnGetRealConf(int result, CSParamWrapper param, CSRealTimeConfWrapper[] listInfo);

		void OnGetUserInfo(int result, CSParamWrapper param, CSUserInfoWrapper info);
		void OnUpdateUserInfo(int result, CSParamWrapper param, CSUserInfoWrapper info);
		void OnRemoveUserInfo(int result, CSParamWrapper param, CSUserInfoWrapper info);
		void OnGetUserInfoList(int result, CSParamWrapper param, CSUserInfoWrapper info, CSUserInfoWrapper[] listInfo);

		void OnGetOrgInfo(int result, CSParamWrapper param, CSOrgInfoWrapper info);
		void OnUpdateOrgInfo(int result, CSParamWrapper param, CSOrgInfoWrapper info);
		void OnGetOrgInfoList(int result, CSParamWrapper param, CSOrgInfoWrapper info, CSOrgInfoWrapper[] listInfo);

		void OnGetUserBind(int result, CSParamWrapper param, CSUserBindWrapper info);
		void OnAddUserBind(int result, CSParamWrapper param, CSUserBindWrapper info);
		void OnUpdateUserBind(int result, CSParamWrapper param, CSUserBindWrapper info);
		void OnRemoveUserBind(int result, CSParamWrapper param, CSUserBindWrapper info);
		void OnGetUserBindList(int result, CSParamWrapper param, CSUserBindWrapper info, CSUserBindWrapper[] listInfo);
		
		void OnGetOrgUser(int result, CSParamWrapper param, CSOrgUserWrapper info);
		void OnAddOrgUser(int result, CSParamWrapper param, CSOrgUserWrapper info);
		void OnUpdateOrgUser(int result, CSParamWrapper param, CSOrgUserWrapper info);
		void OnRemoveOrgUser(int result, CSParamWrapper param, CSOrgUserWrapper info);
		void OnGetOrgUserList(int result, CSParamWrapper param, CSOrgUserWrapper info, CSOrgUserWrapper[] listInfo);

		void OnGetConfRoom(int result, CSParamWrapper param, CSConfRoomWrapper info);
		void OnAddConfRoom(int result, CSParamWrapper param, CSConfRoomWrapper info);
		void OnUpdateConfRoom(int result, CSParamWrapper param, CSConfRoomWrapper info);
		void OnRemoveConfRoom(int result, CSParamWrapper param, CSConfRoomWrapper info);
		void OnGetConfRoomList(int result, CSParamWrapper param, CSConfRoomWrapper info, CSConfRoomWrapper[] listInfo);

		void OnGetConfCode(int result, CSParamWrapper param, CSConfCodeWrapper info);
		void OnAddConfCode(int result, CSParamWrapper param, CSConfCodeWrapper info);
		void OnUpdateConfCode(int result, CSParamWrapper param, CSConfCodeWrapper info);
		void OnRemoveConfCode(int result, CSParamWrapper param, CSConfCodeWrapper info);
		void OnGetConfCodeList(int result, CSParamWrapper param, CSConfCodeWrapper info, CSConfCodeWrapper[] listInfo);

		void OnGetApplyMsg(int result, CSParamWrapper param, CSApplyMsgWrapper info);
		void OnAddApplyMsg(int result, CSParamWrapper param, CSApplyMsgWrapper info);
		void OnUpdateApplyMsg(int result, CSParamWrapper param, CSApplyMsgWrapper info);
		void OnRemoveApplyMsg(int result, CSParamWrapper param, CSApplyMsgWrapper info);
		void OnGetApplyMsgList(int result, CSParamWrapper param, CSApplyMsgWrapper info, CSApplyMsgWrapper[] listInfo);

		void OnGetPushMsg(int result, CSParamWrapper param, CSPushMsgWrapper info);
		void OnAddPushMsg(int result, CSParamWrapper param, CSPushMsgWrapper info);
		void OnUpdatePushMsg(int result, CSParamWrapper param, CSPushMsgWrapper info);
		void OnRemovePushMsg(int result, CSParamWrapper param, CSPushMsgWrapper info);
		void OnGetPushMsgList(int result, CSParamWrapper param, CSPushMsgWrapper info, CSPushMsgWrapper[] listInfo);
		void OnNoticePushMsg(CSParamWrapper param, CSPushMsgWrapper info);

    }
}
