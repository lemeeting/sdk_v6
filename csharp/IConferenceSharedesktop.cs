﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceSharedesktop
    {
 	    int AddObserver(IConferenceSharedesktopObserver observer);
	    int RemoveObserver(IConferenceSharedesktopObserver observer);

        int CreateView(String strSharer, IntPtr hParent, System.Drawing.Color crBkg, ref IntPtr hChildWnd);
	    int DestroyView(String strSharer);

        int SetDisplayMode(String strSharer, LMShareDesktopDisplayModes nMode);
	    //int GetSharerList(List<String> listSharer);

	    int GetScreenResolution(ref int nWidth, ref int nHeight);
        int GetRemoteResolution(ref int nWidth, ref int nHeight);
	    int SetRemoteResolution(int nWidth, int nHeight);
   }
}
