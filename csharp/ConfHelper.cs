﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public static class ConfHelper
    {
        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_version")]
        private static extern void lm_get_version(StringBuilder version, int size);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_error_string")]
        private static extern IntPtr lm_get_error_string(int error, StringBuilder s_error, int size);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_get_default_init_options")]
        private static extern IntPtr lm_get_default_init_options(ref LMInitOptions options);
        
        [DllImport("c_wrapper.dll", EntryPoint = "conf_malloc_and_zero")]
        private static extern IntPtr conf_malloc_and_zero(int size);
        
        [DllImport("c_wrapper.dll", EntryPoint = "conf_free")]
        private static extern void conf_free(IntPtr p);
        
        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_playback_IsVaildPlaybackFile")]
        private static extern int lm_conf_playback_IsVaildPlaybackFile(String file_name, ref int needRepairIfValid);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetStartImage")]
        private static extern void  lm_conf_video_SetStartImage(String jpg_file);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetTimeoutImage")]
        private static extern void  lm_conf_video_SetTimeoutImage(String jpg_file);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetRemoteDisableRemoteVideoImage")]
        private static extern void  lm_conf_video_SetRemoteDisableRemoteVideoImage(String jpg_file);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetLocalDisableRemoteVideoImage")]
        private static extern void  lm_conf_video_SetLocalDisableRemoteVideoImage(String jpg_file);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_video_SetLocalDesktopCamearImage")]
        private static extern void  lm_conf_video_SetLocalDesktopCamearImage(String jpg_file);

        public static String ErrorToString(int error)
        {
            StringBuilder sss = new StringBuilder(128);
            lm_get_error_string(error, sss, sss.Capacity);
            return sss.ToString();
        }

        public static LMInitOptions DefaultInitOptions()
        {
            LMInitOptions options = new LMInitOptions();
            lm_get_default_init_options(ref options);
            return options;
        }


        public static IntPtr NativeMalloc(int size)
        {
            return conf_malloc_and_zero(size);
        }

        public static void NativeFree(IntPtr p)
        {
            conf_free(p);
        }

        public static bool IsVaildPlaybackFile(String filename, ref bool needRepairIfValid)
        {
            int vv = 0;
            int ret = lm_conf_playback_IsVaildPlaybackFile(filename, ref vv);
            needRepairIfValid = vv != 0;
            return ret != 0;
        }

        public static void SetStartImage(String jpg_file)
        {
            lm_conf_video_SetStartImage(jpg_file);
        }

		public static void SetTimeoutImage(String jpg_file)
        {
            lm_conf_video_SetTimeoutImage(jpg_file);
        }

		public static void SetRemoteDisableRemoteVideoImage(String jpg_file)
        {
            lm_conf_video_SetRemoteDisableRemoteVideoImage(jpg_file);
        }

		public static void SetLocalDisableRemoteVideoImage(String jpg_file)
        {
            lm_conf_video_SetLocalDisableRemoteVideoImage(jpg_file);
        }

        public static void SetLocalDesktopCamearImage(String jpg_file)
        {
            lm_conf_video_SetLocalDesktopCamearImage(jpg_file);
        }


    }
}
