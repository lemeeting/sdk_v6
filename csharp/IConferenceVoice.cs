﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceVoice
    {
        int AddObserver(IConferenceVoiceObserver observer);
	    int RemoveObserver(IConferenceVoiceObserver observer);

        int SetCodecType(LMVoiceCodecModes codec_type, LMVoiceDelayModes delay_mode);
        int GetCodecType(ref LMVoiceCodecModes codec_type, ref LMVoiceDelayModes delay_mode);

        int SetCodecTypeWithInputDevice(LMVoiceDeviceType input_type,
			LMVoiceCodecModes codec_type, LMVoiceDelayModes delay_mode, int rate);
		int GetCodecTypeWithInputDevice(LMVoiceDeviceType input_type,
			ref LMVoiceCodecModes codec_type, ref LMVoiceDelayModes delay_mode, ref int rate);

        bool IsValidDelayMode(LMVoiceCodecModes codec_type, LMVoiceDelayModes delay_mode);

	    int GetNumOfInputDevices(ref int nums);
	    int GetNumOfOutputDevices(ref int nums);
        int GetInputDeviceName(int index, StringBuilder name, StringBuilder guid);
        int GetOutputDeviceName(int index, StringBuilder name, StringBuilder guid);
        int GetDefaultInputDeviceName(StringBuilder name, StringBuilder guid);
        int GetDefaultOutputDeviceName(StringBuilder name, StringBuilder guid);
        int GetCurrentInputDeviceName(StringBuilder name, StringBuilder guid);
        int GetCurrentOutputDeviceName(StringBuilder name, StringBuilder guid);

        int GetDefaultInputDeviceIndex(ref int index);
        int GetDefaultOutputDeviceIndex(ref int index);
        int GetCurrentInputDeviceIndex(ref int index);
        int GetCurrentOutputDeviceIndex(ref int index);
 
	    int SetInputDevice(int index);
	    int SetOutputDevice(int index);

	    int SetInputVolume(int volume);
	    int GetInputVolume(ref int volume);
	    int SetOutputVolume(int volume);
	    int GetOutputVolume(ref int volume);

	    int GetInputVoiceLevel(ref int level);
	    int GetOutputVoiceLevel(String account, ref int level);
	    int GetOutputVoiceLevel(int node, ref int level);

	    int SetLoudspeakerStatus(bool enabled);
        int SetEcStatus(bool enabled, LMEcModes mode);
        int SetNsStatus(bool enabled, LMNsModes mode);
        int SetAgcStatus(bool enabled, LMAgcModes mode);
        int SetAecmMode(LMAecmModes mode, bool enableCNG);
	    int SetVADStatus(bool enable, LMVadModes mode, bool disableDTX);

	    int GetLoudspeakerStatus(ref bool enabled);
        int GetEcStatus(ref bool enabled, ref LMEcModes mode);
        int GetNsStatus(ref bool enabled, ref LMNsModes mode);
        int GetAgcStatus(ref bool enabled, ref LMAgcModes mode);
        int GetAecmMode(ref LMAecmModes mode, ref bool enabledCNG);
        int GetVADStatus(ref bool enabled, ref LMVadModes mode, ref bool disabledDTX);

	    int EnableDriftCompensation(bool enable);
	    int DriftCompensationEnabled(ref bool enable);

	    int SetDelayOffsetMs(int offset);
	    int DelayOffsetMs(ref int offset);

	    int EnableHighPassFilter(bool enable);
	    int IsHighPassFilterEnabled(ref bool enable);

	    int EnableInput(bool enabled);
	    int EnableOutput(bool enabled);

	    int GetInputState(ref bool enabled);
	    int GetOutputState(ref bool enabled);

        int DisablePlaybackInput(bool disabled);
		int DisableIPCameraInput(bool disabled);
		int DisableMediaPlayerInput(bool disabled);
		int DisableExternalDeviceInput(bool disabled);

		int DisablePlaybackInputLocalOutput(bool disabled);
		int DisableIPCameraInputLocalOutput(bool disabled);
		int DisableMediaPlayerInputLocalOutput(bool disabled);
		int DisableExternalDeviceInputLocalOutput(bool disabled);

		int GetDisablePlaybackInputState(ref bool disabled);
		int GetDisableIPCameraInputState(ref bool disabled);
		int GetDisableMediaPlayerInputState(ref bool disabled);
		int GetDisableExternalDeviceInputState(ref bool disabled);

		int GetDisablePlaybackInputLocalOutputState(ref bool disabled);
		int GetDisableIPCameraInputLocalOutputState(ref bool disabled);
		int GetDisableMediaPlayerInputLocalOutputState(ref bool disabled);
        int GetDisableExternalDeviceInputLocalOutputState(ref bool disabled);

	    int StartPlayingFileLocally(
		    ref int node,
		    String fileName,
		    bool loop,
            LMVoiceFileFormats format,
		    float volumeScaling,
		    int startPointMs,
		    int stopPointMs);

	    int StartPlayingFileLocally(
		    ref int node,
		    byte[] stream,
            LMVoiceFileFormats format,
		    float volumeScaling,
		    int startPointMs, int stopPointMs);

	    int StopPlayingFileLocally(int node);

	    int IsPlayingFileLocally(int node);

	    int StartPlayingFileAsMicrophone(
		    String fileName,
		    bool loop,
		    bool mixWithMicrophone,
            LMVoiceFileFormats format,
		    float volumeScaling);

	    int StartPlayingFileAsMicrophone(
		    byte[] stream,
		    bool mixWithMicrophone,
            LMVoiceFileFormats format,
		    float volumeScaling);

	    int StopPlayingFileAsMicrophone();

	    int IsPlayingFileAsMicrophone();

	    int StartRecordingPlayout(
		    String account,
		    String fileName,
		    LMVoiceCodecModes codec,
		    int maxSizeBytes = -1);

	    int StartRecordingPlayout(
		    String account,
		    byte[] stream,
            LMVoiceCodecModes codec);

	    int StopRecordingPlayout(String account);

	    int StartRecordingPlayout(
		    String fileName,
            LMVoiceCodecModes codec,
		    int maxSizeBytes);

	    int StartRecordingPlayout(
		    byte[] stream,
		    LMVoiceCodecModes codec);

	    int StopRecordingPlayout();

	    int StartRecordingMicrophone(String fileName,
            LMVoiceCodecModes codec, int maxSizeBytes);

	    int StartRecordingMicrophone(byte[] stream,
            LMVoiceCodecModes codec);

	    int StopRecordingMicrophone();

        int AddIPCamera(LMIPCameraInfo info, int camera_channel);
		int RemoveIPCamera(int identity, int camera_channel);

		int AddExternalRAWDataInputDevice(int sampleRate, int channel, int identity);
		int RemoveExternalRAWDataInputDevice(int identity);
        int IncomingExternalInputRAWData(int identity, byte[] data);

		int AddExternalRTPDataInputDevice(int identity);
		int RemoveExternalRTPDataInputDevice(int identity);
        int IncomingExternalInputRTPData(int identity, byte[] data, LMVoiceDelayModes delay_mode);

	    int SetVoiceIOMode(LMVoiceIOModes mode);
	    int GetVoiceIOMode(ref LMVoiceIOModes mode);
    }
}
