﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceVoiceObserver
    {
        void OnDeviceChange();
        void OnDisconnectVoiceServer(int result);
    }
}
