﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    public interface IConferenceDataServer
    {
        int AddObserver(IConferenceDataServerObserver observer);
	    int RemoveObserver(IConferenceDataServerObserver observer);

	    int GetClusterGroup(List<LMDataServerInfo> cluster_group_data_servers);

        int GetCurrentConnectDataServer(LMDataServerModule module, ref LMDataServerInfo data_server);

	    int PingDataServer(String address, int port, long callback_time,
		    bool is_use_udp, ulong context, ref int task_id);

        int StopPingDataServer(int task_id);

        int SwitchDataServer(LMDataServerModule module, String address);

    }
}
