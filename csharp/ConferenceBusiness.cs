﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    internal class ConfBusinessWrapper : IConferenceBusiness
    {
        public delegate void OnDisconnectDelegate(int result);
        public delegate void OnEntryConferenceDelegate(int result);
        public delegate void OnLeaveConferenceDelegate(int result);

        public delegate void OnGetConferenceDelegate(IntPtr attribute);
        public delegate void OnUpdateConerenceDelegate(IntPtr attribute);
        public delegate void OnRemoveConerenceDelegate(uint id_conference);

        public delegate void OnGetConferenceRealTimeInfoDelegate(IntPtr info);
        public delegate void OnGetConferenceSyncInfoDelegate(IntPtr info);

        public delegate void OnAddSelfAttendeeDelegate(IntPtr attendee);
        public delegate void OnAttendeeOnlineDelegate(IntPtr attendee);
        public delegate void OnAttendeeOfflineDelegate(String account);
        public delegate void OnUpdateAttendeeDelegate(IntPtr old_attendee, IntPtr attendee);
        public delegate void OnRemoveAttendeeDelegate(String account);

        public delegate void OnAddConfAdminDelegate(String admin);
        public delegate void OnRemoveConfAdminDelegate(String account);
        public delegate void OnAddConfDefaultAttendeeDelegate(String account);
        public delegate void OnRemoveConfDefaultAttendeeDelegate(String account);

        public delegate void OnApplySpeakOperDelegate(int result, String account, uint new_state, uint old_state, int apply);
        public delegate void OnAccreditSpeakOperDelegate(int result, String account, uint new_state, uint old_state, int accredit);
        public delegate void OnSpeakNotifyDelegate(String account, uint new_state, uint old_state, int is_speak);
        public delegate void OnApplyDataOperDelegate(int result, String account, uint new_state, uint old_state, int apply);
        public delegate void OnAccreditDataOperDelegate(int result, String account, uint new_state, uint old_state, int accredit);
        public delegate void OnDataOpNotifyDelegate(String account, uint new_state, uint old_state, int is_data_op);
        public delegate void OnApplyDataSyncDelegate(int result, String account, uint new_state, uint old_state, int apply);
        public delegate void OnAccreditDataSyncDelegate(int result, String account, uint new_state, uint old_state, int accredit);
        public delegate void OnSyncOpNotifyDelegate(String account, uint new_state, uint old_state, int is_sync);
        public delegate void OnDataSyncCommandDelegate(int result);
        public delegate void OnDataSyncCommandNotifyDelegate(String syncer, String sync_data);
        public delegate void OnApplyTempAdminDelegate(int result, String account, uint new_state, uint old_state, int apply);
        public delegate void OnAccreditTempAdminDelegate(int result, String account, uint new_state, uint old_state, int accredit);
        public delegate void OnAuthTempAdminDelegate(int result, String account);
        public delegate void OnTempAdminNotifyDelegate(String account, uint new_state, uint old_state, int is_admin);

        public delegate void OnStartPreviewVideoDelegate(int result, String shower, int id_device, ulong context);
        public delegate void OnStopPreviewVideoDelegate(int result, String shower, int id_device, ulong context);
        public delegate void OnStartPreviewVideoNotifyDelegate(String shower, int id_device);
        public delegate void OnStopPreviewVideoNotifyDelegate(String shower, int id_device);

        public delegate void OnSwitchMainVideoDelegate(int result, int old_id_device, int id_device);
        public delegate void OnSwitchMainVideoNotifyDelegate(String account, int old_id_device, int id_device);
        public delegate void OnEnableVideoDelegate(int result, int enabled);
        public delegate void OnEnableVideoNotifyDelegate(String account, int enabled);
        public delegate void OnCaptureVideoStateDelegate(int result, int id_device, int enabled);
        public delegate void OnCaptureVideoStateNotifyDelegate(String account, int id_device, int enabled);
        public delegate void OnVideoDeviceChangedNotifyDelegate(String account, UInt16 dev_nums, UInt16 dev_state, int dev_main_id);
        public delegate void OnVideoShowNameChangedNotifyDelegate(String account, int id_device, String name_utf8);

        public delegate void OnKickoutAttendeeDelegate(int result, String account);
        public delegate void OnKickoutAttendeeNotifyDelegate(String oper_account, String account);
        public delegate void OnUpdateAttendeeNameDelegate(int result, String new_name);
        public delegate void OnUpdateAttendeeNameNotifyDelegate(String account, String new_name);

        public delegate void OnRelayMsgToOneDelegate(int result, String receiver);
        public delegate void OnRelayMsgToOneNotifyDelegate(String sender, String receiver, String msg);
        public delegate void OnRelayMsgToAllDelegate(int result);
        public delegate void OnRelayMsgToAllNotifyDelegate(String sender, String msg);

        public delegate void OnAdminOperConfSettingDelegate(int result, int cmd, int cmd_value);
        public delegate void OnAdminOperConfSettingNotifyDelegate(String account, int cmd, int cmd_value);
        public delegate void OnSetConfPasswordDelegate(int result);
        public delegate void OnSetConfPasswordNotifyDelegate(String account);

        public delegate void OnStartDesktopShareDelegate(int result);
        public delegate void OnStartDesktopShareNotifyDelegate(String account);

        public delegate void OnStopDesktopShareDelegate(int result);
        public delegate void OnStopDesktopShareNotifyDelegate(String account);

        public delegate void OnStartPreviewDesktopDelegate(int result, String sharer, ulong context);
        public delegate void OnStopPreviewDesktopDelegate(int result, String sharer, ulong context);

        public delegate void OnStartPreviewDesktopNotifyDelegate(String sharer);
        public delegate void OnStopPreviewDesktopNotifyDelegate(String sharer);

        public delegate void OnAskRemoteDesktopControlDelegate(int result, String sharer);
        public delegate void OnAskRemoteDesktopControlNotifyDelegate(String controller, String sharer);
        public delegate void OnAbstainRemoteDesktopControlDelegate(int result, String sharer);
        public delegate void OnAbstainRemoteDesktopControlNotifyDelegate(String controller, String sharer);
        public delegate void OnAnswerRemoteDesktopControlDelegate(int result, String controller, bool allow);
        public delegate void OnAnswerRemoteDesktopControlNotifyDelegate(String controller, String sharer, bool allow);
        public delegate void OnAbortRemoteDesktopControlDelegate(int result, String controller);
        public delegate void OnAbortRemoteDesktopControlNotifyDelegate(String controller, String sharer);

        public delegate void OnLaunchSigninDelegate(int result, int signin_type, long launch_or_stop_time);
        public delegate void OnLaunchSigninNotifyDelegate(String launcher, int signin_type, long launch_or_stop_time);
        public delegate void OnSigninDelegate(int result, int is_signin, long launch_time, long signin_or_cancel_time);
        public delegate void OnSigninNotifyDelegate(String signiner, int is_signin, long launch_time, long signin_or_cancel_time);
        public delegate void OnOperSubGroupDelegate(int result);
        public delegate void OnOperSubGroupNotifyDelegate(String oper, uint id_subgroup);
        public delegate void OnCancelSubGroupDelegate(int result);
        public delegate void OnCancelSubGroupNotifyDelegate(String oper);

        public delegate void OnOpRemoteDesktopSharedDelegate(int result, String sharer, int is_start);
        public delegate void OnOpRemoteDesktopSharedNotifyDelegate(String oper, String sharer, int is_start);

        public delegate void OnStartPlaybackDelegate(int result, String file_name);
        public delegate void OnStartPlaybackNotifyDelegate(String oper);
        public delegate void OnStopPlaybackDelegate(int result);
        public delegate void OnStopPlaybackNotifyDelegate(String oper);

        public delegate void OnStartMediaPlayDelegate(int result, String file_name, int media_flag);
        public delegate void OnStartMediaPlayNotifyDelegate(String oper, String file_name, int media_flag);
        public delegate void OnStopMediaPlayDelegate(int result);
        public delegate void OnStopMediaPlayNotifyDelegate(String oper);
        public delegate void OnPauseMediaPlayDelegate(int result, bool paused);
        public delegate void OnPauseMediaPlayNotifyDelegate(String oper, bool paused);

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct ConfBusinessCallback
        {
            public OnDisconnectDelegate OnDisconnectDelegate_;
            public OnEntryConferenceDelegate OnEntryConferenceDelegate_;
            public OnLeaveConferenceDelegate OnLeaveConferenceDelegate_;

            public OnGetConferenceDelegate OnGetConferenceDelegate_;
            public OnUpdateConerenceDelegate OnUpdateConerenceDelegate_;
            public OnRemoveConerenceDelegate OnRemoveConerenceDelegate_;

            public OnGetConferenceRealTimeInfoDelegate OnGetConferenceRealTimeInfoDelegate_;
            public OnGetConferenceSyncInfoDelegate OnGetConferenceSyncInfoDelegate_;

            public OnAddSelfAttendeeDelegate OnAddSelfAttendeeDelegate_;
            public OnAttendeeOnlineDelegate OnAttendeeOnlineDelegate_;
            public OnAttendeeOfflineDelegate OnAttendeeOfflineDelegate_;
            public OnUpdateAttendeeDelegate OnUpdateAttendeeDelegate_;
            public OnRemoveAttendeeDelegate OnRemoveAttendeeDelegate_;

            public OnAddConfAdminDelegate OnAddConfAdminDelegate_;
            public OnRemoveConfAdminDelegate OnRemoveConfAdminDelegate_;
            public OnAddConfDefaultAttendeeDelegate OnAddConfDefaultAttendeeDelegate_;
            public OnRemoveConfDefaultAttendeeDelegate OnRemoveConfDefaultAttendeeDelegate_;

            public OnApplySpeakOperDelegate OnApplySpeakOperDelegate_;
            public OnAccreditSpeakOperDelegate OnAccreditSpeakOperDelegate_;
            public OnSpeakNotifyDelegate OnSpeakNotifyDelegate_;
            public OnApplyDataOperDelegate OnApplyDataOperDelegate_;
            public OnAccreditDataOperDelegate OnAccreditDataOperDelegate_;
            public OnDataOpNotifyDelegate OnDataOpNotifyDelegate_;
            public OnApplyDataSyncDelegate OnApplyDataSyncDelegate_;
            public OnAccreditDataSyncDelegate OnAccreditDataSyncDelegate_;
            public OnSyncOpNotifyDelegate OnSyncOpNotifyDelegate_;
            public OnDataSyncCommandDelegate OnDataSyncCommandDelegate_;
            public OnDataSyncCommandNotifyDelegate OnDataSyncCommandNotifyDelegate_;
            public OnApplyTempAdminDelegate OnApplyTempAdminDelegate_;
            public OnAccreditTempAdminDelegate OnAccreditTempAdminDelegate_;
            public OnAuthTempAdminDelegate OnAuthTempAdminDelegate_;
            public OnTempAdminNotifyDelegate OnTempAdminNotifyDelegate_;

            public OnStartPreviewVideoDelegate OnStartPreviewVideoDelegate_;
            public OnStopPreviewVideoDelegate OnStopPreviewVideoDelegate_;
            public OnStartPreviewVideoNotifyDelegate OnStartPreviewVideoNotifyDelegate_;
            public OnStopPreviewVideoNotifyDelegate OnStopPreviewVideoNotifyDelegate_;

            public OnSwitchMainVideoDelegate OnSwitchMainVideoDelegate_;
            public OnSwitchMainVideoNotifyDelegate OnSwitchMainVideoNotifyDelegate_;
            public OnEnableVideoDelegate OnEnableVideoDelegate_;
            public OnEnableVideoNotifyDelegate OnEnableVideoNotifyDelegate_;
            public OnCaptureVideoStateDelegate OnCaptureVideoStateDelegate_;
            public OnCaptureVideoStateNotifyDelegate OnCaptureVideoStateNotifyDelegate_;
            public OnVideoDeviceChangedNotifyDelegate OnVideoDeviceChangedNotifyDelegate_;
            public OnVideoShowNameChangedNotifyDelegate OnVideoShowNameChangedNotifyDelegate_;

            public OnKickoutAttendeeDelegate OnKickoutAttendeeDelegate_;
            public OnKickoutAttendeeNotifyDelegate OnKickoutAttendeeNotifyDelegate_;
            public OnUpdateAttendeeNameDelegate OnUpdateAttendeeNameDelegate_;
            public OnUpdateAttendeeNameNotifyDelegate OnUpdateAttendeeNameNotifyDelegate_;

            public OnRelayMsgToOneDelegate OnRelayMsgToOneDelegate_;
            public OnRelayMsgToOneNotifyDelegate OnRelayMsgToOneNotifyDelegate_;
            public OnRelayMsgToAllDelegate OnRelayMsgToAllDelegate_;
            public OnRelayMsgToAllNotifyDelegate OnRelayMsgToAllNotifyDelegate_;

            public OnAdminOperConfSettingDelegate OnAdminOperConfSettingDelegate_;
            public OnAdminOperConfSettingNotifyDelegate OnAdminOperConfSettingNotifyDelegate_;
            public OnSetConfPasswordDelegate OnSetConfPasswordDelegate_;
            public OnSetConfPasswordNotifyDelegate OnSetConfPasswordNotifyDelegate_;

            public OnStartDesktopShareDelegate OnStartDesktopShareDelegate_;
            public OnStartDesktopShareNotifyDelegate OnStartDesktopShareNotifyDelegate_;

            public OnStopDesktopShareDelegate OnStopDesktopShareDelegate_;
            public OnStopDesktopShareNotifyDelegate OnStopDesktopShareNotifyDelegate_;

            public OnStartPreviewDesktopDelegate OnStartPreviewDesktopDelegate_;
            public OnStopPreviewDesktopDelegate OnStopPreviewDesktopDelegate_;

            public OnStartPreviewDesktopNotifyDelegate OnStartPreviewDesktopNotifyDelegate_;
            public OnStopPreviewDesktopNotifyDelegate OnStopPreviewDesktopNotifyDelegate_;

            public OnAskRemoteDesktopControlDelegate OnAskRemoteDesktopControlDelegate_;
            public OnAskRemoteDesktopControlNotifyDelegate OnAskRemoteDesktopControlNotifyDelegate_;
            public OnAbstainRemoteDesktopControlDelegate OnAbstainRemoteDesktopControlDelegate_;
            public OnAbstainRemoteDesktopControlNotifyDelegate OnAbstainRemoteDesktopControlNotifyDelegate_;
            public OnAnswerRemoteDesktopControlDelegate OnAnswerRemoteDesktopControlDelegate_;
            public OnAnswerRemoteDesktopControlNotifyDelegate OnAnswerRemoteDesktopControlNotifyDelegate_;
            public OnAbortRemoteDesktopControlDelegate OnAbortRemoteDesktopControlDelegate_;
            public OnAbortRemoteDesktopControlNotifyDelegate OnAbortRemoteDesktopControlNotifyDelegate_;

            public OnLaunchSigninDelegate OnLaunchSigninDelegate_;
            public OnLaunchSigninNotifyDelegate OnLaunchSigninNotifyDelegate_;
            public OnSigninDelegate OnSigninDelegate_;
            public OnSigninNotifyDelegate OnSigninNotifyDelegate_;
            public OnOperSubGroupDelegate OnOperSubGroupDelegate_;
            public OnOperSubGroupNotifyDelegate OnOperSubGroupNotifyDelegate_;
            public OnCancelSubGroupDelegate OnCancelSubGroupDelegate_;
            public OnCancelSubGroupNotifyDelegate OnCancelSubGroupNotifyDelegate_;

            public OnOpRemoteDesktopSharedDelegate OnOpRemoteDesktopSharedDelegate_;
            public OnOpRemoteDesktopSharedNotifyDelegate OnOpRemoteDesktopSharedNotifyDelegate_;

            public OnStartPlaybackDelegate OnStartPlaybackDelegate_;
            public OnStartPlaybackNotifyDelegate OnStartPlaybackNotifyDelegate_;
            public OnStopPlaybackDelegate OnStopPlaybackDelegate_;
            public OnStopPlaybackNotifyDelegate OnStopPlaybackNotifyDelegate_;

            public OnStartMediaPlayDelegate OnStartMediaPlayDelegate_;
            public OnStartMediaPlayNotifyDelegate OnStartMediaPlayNotifyDelegate_;
            public OnStopMediaPlayDelegate OnStopMediaPlayDelegate_;
            public OnStopMediaPlayNotifyDelegate OnStopMediaPlayNotifyDelegate_;
            public OnPauseMediaPlayDelegate OnPauseMediaPlayDelegate_;
            public OnPauseMediaPlayNotifyDelegate OnPauseMediaPlayNotifyDelegate_;

        }

        public void OnDisconnect(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnDisconnect(result);
        }

        public void OnEntryConference(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnEntryConference(result);
        }

        public void OnLeaveConference(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnLeaveConference(result);
        }

        public void OnGetConference(IntPtr attribute)
        {
            ASConfAttributeWrapper ca = new ASConfAttributeWrapper();
            ca.CopyNativeInstance(attribute);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnGetConference(ca);
        }

        public void OnUpdateConerence(IntPtr attribute)
        {
            ASConfAttributeWrapper ca = new ASConfAttributeWrapper();
            ca.CopyNativeInstance(attribute);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnUpdateConerence(ca);
        }

        public void OnRemoveConerence(uint id_conference)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRemoveConerence(id_conference);
        }

        public void OnGetConferenceRealTimeInfo(IntPtr info)
        {
            ASConfRealTimeInfoWrapper rtci = new ASConfRealTimeInfoWrapper();
            rtci.CopyNativeInstance(info);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnGetConferenceRealTimeInfo(rtci);
        }

        public void OnGetConferenceSyncInfo(IntPtr info)
        {
            ASConfSyncInfoWrapper csi = new ASConfSyncInfoWrapper();
            csi.CopyNativeInstance(info);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnGetConferenceSyncInfo(csi);
        }

        public void OnAddSelfAttendee(IntPtr attendee)
        {
            ASConfAttendeeWrapper ca = new ASConfAttendeeWrapper();
            ca.CopyNativeInstance(attendee);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAddSelfAttendee(ca);
        }

        public void OnAttendeeOnline(IntPtr attendee)
        {
            ASConfAttendeeWrapper ca = new ASConfAttendeeWrapper();
            ca.CopyNativeInstance(attendee);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAttendeeOnline(ca);
        }

        public void OnAttendeeOffline(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAttendeeOffline(account);
        }

        public void OnUpdateAttendee(IntPtr old_attendee, IntPtr attendee)
        {
            ASConfAttendeeWrapper ca_old = new ASConfAttendeeWrapper();
            ca_old.CopyNativeInstance(old_attendee);
            ASConfAttendeeWrapper ca_new = new ASConfAttendeeWrapper();
            ca_new.CopyNativeInstance(attendee);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnUpdateAttendee(ca_old, ca_new);
        }

        public void OnRemoveAttendee(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRemoveAttendee(account);
        }

        public void OnAddConfAdmin(String admin)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAddConfAdmin(admin);
        }

        public void OnRemoveConfAdmin(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRemoveConfAdmin(account);
        }

        public void OnAddConfDefaultAttendee(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAddConfDefaultAttendee(account);
        }

        public void OnRemoveConfDefaultAttendee(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRemoveConfDefaultAttendee(account);
        }

        public void OnApplySpeakOper(int result, String account, uint new_state, uint old_state, int apply)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnApplySpeakOper(result, account, new_state, old_state, apply != 0);
        }

        public void OnAccreditSpeakOper(int result, String account, uint new_state, uint old_state, int accredit)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAccreditSpeakOper(result, account, new_state, old_state, accredit != 0);
        }

        public void OnSpeakNotify(String account, uint new_state, uint old_state, int is_speak)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSpeakNotify(account, new_state, old_state, is_speak != 0);
        }

        public void OnApplyDataOper(int result, String account, uint new_state, uint old_state, int apply)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnApplyDataOper(result, account, new_state, old_state, apply != 0);
        }

        public void OnAccreditDataOper(int result, String account, uint new_state, uint old_state, int accredit)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAccreditDataOper(result, account, new_state, old_state, accredit != 0);
        }

        public void OnDataOpNotify(String account, uint new_state, uint old_state, int is_data_op)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnDataOpNotify(account, new_state, old_state, is_data_op != 0);
        }

        public void OnApplyDataSync(int result, String account, uint new_state, uint old_state, int apply)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnApplyDataSync(result, account, new_state, old_state, apply != 0);
        }

        public void OnAccreditDataSync(int result, String account, uint new_state, uint old_state, int accredit)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAccreditDataSync(result, 
                    account, new_state, old_state, accredit != 0);
        }

        public void OnSyncOpNotify(String account, uint new_state, uint old_state, int is_sync)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSyncOpNotify(account, 
                    new_state, old_state, is_sync != 0);
        }

        public void OnDataSyncCommand(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnDataSyncCommand(result);
        }

        public void OnDataSyncCommandNotify(String syncer, String sync_data)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnDataSyncCommandNotify(syncer,
                    sync_data);
        }

        public void OnApplyTempAdmin(int result, String account, uint new_state, uint old_state, int apply)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnApplyTempAdmin(result, account,
                    new_state, old_state, apply != 0);
        }

        public void OnAccreditTempAdmin(int result, String account, uint new_state, uint old_state, int accredit)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAccreditTempAdmin(result, account, 
                    new_state, old_state, accredit != 0);
        }

        public void OnAuthTempAdmin(int result, String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAuthTempAdmin(result, account);
        }

        public void OnTempAdminNotify(String account, uint new_state, uint old_state, int is_admin)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnTempAdminNotify(account,
                    new_state, old_state, is_admin != 0);
        }

        public void OnStartPreviewVideo(int result, String shower, int id_device, ulong context)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPreviewVideo(result, shower, id_device, context);
        }

        public void OnStopPreviewVideo(int result, String shower, int id_device, ulong context)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPreviewVideo(result, shower, id_device, context);
        }

        public void OnStartPreviewVideoNotify(String shower, int id_device)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPreviewVideoNotify(shower, id_device);
        }

        public void OnStopPreviewVideoNotify(String shower, int id_device)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPreviewVideoNotify(shower, id_device);
        }

        public void OnSwitchMainVideo(int result, int old_id_device, int id_device)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSwitchMainVideo(result, old_id_device, id_device);
        }

        public void OnSwitchMainVideoNotify(String account, int old_id_device, int id_device)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSwitchMainVideoNotify(account, old_id_device, id_device);
        }

        public void OnEnableVideo(int result, int enabled)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnEnableVideo(result, enabled != 0);
        }

        public void OnEnableVideoNotify(String account, int enabled)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnEnableVideoNotify(account, enabled != 0);
        }

        public void OnCaptureVideoState(int result, int id_device, int enabled)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnCaptureVideoState(result, id_device, enabled != 0);
        }

        public void OnCaptureVideoStateNotify(String account, int id_device, int enabled)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnCaptureVideoStateNotify(account, id_device, enabled != 0);
        }

        public void OnVideoDeviceChangedNotify(String account, UInt16 dev_nums, UInt16 dev_state, int dev_main_id)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnVideoDeviceChangedNotify(account, dev_nums, dev_state, dev_main_id);
        }

        public void OnVideoShowNameChangedNotify(String account, int id_device, String name)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnVideoShowNameChangedNotify(account,
                    id_device, name);
        }

        public void OnKickoutAttendee(int result, String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnKickoutAttendee(result, account);
        }

        public void OnKickoutAttendeeNotify(String oper_account, String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnKickoutAttendeeNotify(oper_account, 
                    account);
        }

        public void OnUpdateAttendeeName(int result, String new_name)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnUpdateAttendeeName(result,
                    Encoding.Default.GetString(Encoding.Unicode.GetBytes(new_name)));
        }

        public void OnUpdateAttendeeNameNotify(String account, String new_name)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnUpdateAttendeeNameNotify(account,
                    new_name);
        }

        public void OnRelayMsgToOne(int result, String receiver)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRelayMsgToOne(result,
                    receiver);
        }

        public void OnRelayMsgToOneNotify(String sender, String receiver, String msg)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRelayMsgToOneNotify(sender,
                    receiver,
                    msg);
        }

        public void OnRelayMsgToAll(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRelayMsgToAll(result);
        }

        public void OnRelayMsgToAllNotify(String sender, String msg)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnRelayMsgToAllNotify(sender,
                    msg);
        }


        public void OnAdminOperConfSetting(int result, int cmd, int cmd_value)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAdminOperConfSetting(result, cmd, cmd_value);
        }

        public void OnAdminOperConfSettingNotify(String account, int cmd, int cmd_value)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAdminOperConfSettingNotify(account, cmd, cmd_value);
        }

        public void OnSetConfPassword(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSetConfPassword(result);
        }

        public void OnSetConfPasswordNotify(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSetConfPasswordNotify(account);
        }


        public void OnStartDesktopShare(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartDesktopShare(result);
        }

        public void OnStartDesktopShareNotify(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartDesktopShareNotify(account);
        }


        public void OnStopDesktopShare(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopDesktopShare(result);
        }

        public void OnStopDesktopShareNotify(String account)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopDesktopShareNotify(account);
        }


        public void OnStartPreviewDesktop(int result, String sharer, ulong context)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPreviewDesktop(result, sharer, context);
        }

        public void OnStopPreviewDesktop(int result, String sharer, ulong context)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPreviewDesktop(result, sharer, context);
        }


        public void OnStartPreviewDesktopNotify(String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPreviewDesktopNotify(sharer);
        }

        public void OnStopPreviewDesktopNotify(String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPreviewDesktopNotify(sharer);
        }

        public void OnAskRemoteDesktopControl(int result, String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAskRemoteDesktopControl(result, sharer);
        }

        public void OnAskRemoteDesktopControlNotify(String controller, String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAskRemoteDesktopControlNotify(controller, sharer);
        }

        public void OnAbstainRemoteDesktopControl(int result, String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAbstainRemoteDesktopControl(result, sharer);
        }

        public void OnAbstainRemoteDesktopControlNotify(String controller, String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAbstainRemoteDesktopControlNotify(controller, sharer);
        }

        public void OnAnswerRemoteDesktopControl(int result, String controller, bool allow)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAnswerRemoteDesktopControl(result, controller, allow);
        }

        public void OnAnswerRemoteDesktopControlNotify(String controller, String sharer, bool allow)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAnswerRemoteDesktopControlNotify(controller, sharer, allow);
        }

        public void OnAbortRemoteDesktopControl(int result, String controller)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAbortRemoteDesktopControl(result, controller);
        }

        public void OnAbortRemoteDesktopControlNotify(String controller, String sharer)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnAbortRemoteDesktopControlNotify(controller, sharer);
        }

        public void OnLaunchSignin(int result, int signin_type, long launch_or_stop_time)
        {
            DateTime dt = DateTime.FromFileTime(launch_or_stop_time);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnLaunchSignin(result, (LMConferenceSigninType)signin_type, dt);
        }

        public void OnLaunchSigninNotify(String launcher, int signin_type, long launch_or_stop_time)
        {
            DateTime dt = DateTime.FromFileTime(launch_or_stop_time);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnLaunchSigninNotify(launcher,
                    (LMConferenceSigninType)signin_type, dt);
        }

        public void OnSignin(int result, int is_signin, long launch_time, long signin_or_cancel_time)
        {
            DateTime dt = DateTime.FromFileTime(signin_or_cancel_time);
            DateTime dt1 = DateTime.FromFileTime(launch_time);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSignin(result, is_signin != 0, dt1, dt);
        }

        public void OnSigninNotify(String signiner, int is_signin, long launch_time, long signin_or_cancel_time)
        {
            DateTime dt = DateTime.FromFileTime(signin_or_cancel_time);
            DateTime dt1 = DateTime.FromFileTime(launch_time);
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnSigninNotify(signiner, is_signin != 0, dt1, dt);
        }


        public void OnOperSubGroup(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnOperSubGroup(result);
        }

        public void OnOperSubGroupNotify(String oper, uint id_subgroup)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnOperSubGroupNotify(oper, id_subgroup);
        }

        public void OnCancelSubGroup(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnCancelSubGroup(result);
        }

        public void OnCancelSubGroupNotify(String oper)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnCancelSubGroupNotify(oper);
        }

        public void OnOpRemoteDesktopShared(int result, String sharer, int is_start)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnOpRemoteDesktopShared(result, sharer, is_start != 0);
        }

        public void OnOpRemoteDesktopSharedNotify(String oper, String sharer, int is_start)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnOpRemoteDesktopSharedNotify(oper,
                    sharer, is_start != 0);
        }


        public void OnStartPlayback(int result, String file_name)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPlayback(result,
                    file_name);
        }

        public void OnStartPlaybackNotify(String oper)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartPlaybackNotify(oper);
        }

        public void OnStopPlayback(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPlayback(result);
        }

        public void OnStopPlaybackNotify(String oper)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopPlaybackNotify(oper);
        }

        public void OnStartMediaPlay(int result, String file_name, int media_flag)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartMediaPlay(result, file_name, media_flag);
        }

        public void OnStartMediaPlayNotify(String oper, String file_name, int media_flag)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStartMediaPlayNotify(oper, file_name, media_flag);
        }

        public void OnStopMediaPlay(int result)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopMediaPlay(result);
        }

        public void OnStopMediaPlayNotify(String oper)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnStopMediaPlayNotify(oper);
        }

        public void OnPauseMediaPlay(int result, bool paused)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnPauseMediaPlay(result, paused);
        }

        public void OnPauseMediaPlayNotify(String oper, bool paused)
        {
            foreach (IConferenceBusinessObserver observer in observer_list_)
                observer.OnPauseMediaPlayNotify(oper, paused);
        }

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_set_observer")]
        private static extern void lm_conf_business_set_observer(IntPtr instance, ref ConfBusinessCallback callback);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_EntryConference")]
        private static extern int lm_conf_business_EntryConference(IntPtr instance, String json_address,
	        int id_conference, int conf_tag, String conf_psw, int is_md5, String account, int id_org, String name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_LeaveConference")]
        private static extern int  lm_conf_business_LeaveConference(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_ApplySpeak")]
        private static extern int lm_conf_business_ApplySpeak(IntPtr instance, int apply);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AccreditSpeak")]
        private static extern int lm_conf_business_AccreditSpeak(IntPtr instance, String account, int accredit);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_ApplyDataOper")]
        private static extern int lm_conf_business_ApplyDataOper(IntPtr instance, int apply);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AccreditDataOper")]
        private static extern int lm_conf_business_AccreditDataOper(IntPtr instance, String account, int accredit);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_ApplyDataSync")]
        private static extern int lm_conf_business_ApplyDataSync(IntPtr instance, int apply);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AccreditDataSync")]
        private static extern int lm_conf_business_AccreditDataSync(IntPtr instance, String account, int accredit);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_DataSyncCommand")]
        private static extern int lm_conf_business_DataSyncCommand(IntPtr instance, String command);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_ApplyTempAdmin")]
        private static extern int lm_conf_business_ApplyTempAdmin(IntPtr instance, int apply);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AccreditTempAdmin")]
        private static extern int lm_conf_business_AccreditTempAdmin(IntPtr instance, String account, int accredit);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AuthTempAdmin")]
        private static extern int lm_conf_business_AuthTempAdmin(IntPtr instance, String admin_psw);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StartPreviewVideo")]
        private static extern int lm_conf_business_StartPreviewVideo(IntPtr instance, String shower, int index, ulong context);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StopPreviewVideo")]
        private static extern int lm_conf_business_StopPreviewVideo(IntPtr instance, String shower, int index, ulong context);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_SwitchMainVideo")]
        private static extern int lm_conf_business_SwitchMainVideo(IntPtr instance, int index);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_EnableVideo")]
        private static extern int lm_conf_business_EnableVideo(IntPtr instance, int enabled);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_CaptureVideoState")]
        private static extern int lm_conf_business_CaptureVideoState(IntPtr instance, int devindex, int enabled);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_UpdateVideoShowName")]
        private static extern int lm_conf_business_UpdateVideoShowName(IntPtr instance, int devindex, String name_utf8);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_KickOutAttendee")]
        private static extern int lm_conf_business_KickOutAttendee(IntPtr instance, String account);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_UpdataAttendeeName")]
        private static extern int lm_conf_business_UpdataAttendeeName(IntPtr instance, String name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_RelayMsgToOne")]
        private static extern int lm_conf_business_RelayMsgToOne(IntPtr instance, String receiver, String msg);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_RelayMsgToAll")]
        private static extern int lm_conf_business_RelayMsgToAll(IntPtr instance, String msg);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AdminOperConfSetting")]
        private static extern int lm_conf_business_AdminOperConfSetting(IntPtr instance, int cmd, int cmd_value);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_SetConfPassword")]
        private static extern int lm_conf_business_SetConfPassword(IntPtr instance, String psw);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StartDesktopShare")]
        private static extern int lm_conf_business_StartDesktopShare(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StopDesktopShare")]
        private static extern int lm_conf_business_StopDesktopShare(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StartPreviewDesktop")]
        private static extern int lm_conf_business_StartPreviewDesktop(IntPtr instance, String sharer, ulong context);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StopPreviewDesktop")]
        private static extern int lm_conf_business_StopPreviewDesktop(IntPtr instance, String sharer, ulong context);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AskRemoteDesktopControl")]
        private static extern int lm_conf_business_AskRemoteDesktopControl(IntPtr instance, String sharer);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AbstainRemoteDesktopControl")]
        private static extern int lm_conf_business_AbstainRemoteDesktopControl(IntPtr instance, String sharer);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AnswerRemoteDesktopControl")]
        private static extern int lm_conf_business_AnswerRemoteDesktopControl(IntPtr instance, String sharer, int allow);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_AbortRemoteDesktopControl")]
        private static extern int lm_conf_business_AbortRemoteDesktopControl(IntPtr instance, String controller);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_LaunchSignin")]
        private static extern int lm_conf_business_LaunchSignin(IntPtr instance, int signin_type);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_Signin")]
        private static extern int lm_conf_business_Signin(IntPtr instance, int is_signin);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_OperSubGroup")]
        private static extern int lm_conf_business_OperSubGroup(IntPtr instance, String account, int id_subgroup);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_CancelSubGroup")]
        private static extern int lm_conf_business_CancelSubGroup(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_OpRemoteDesktopShared")]
        private static extern int lm_conf_business_OpRemoteDesktopShared(IntPtr instance, String sharer, int is_start);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StartPlayback")]
        private static extern int lm_conf_business_StartPlayback(IntPtr instance, String file_name);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StopPlayback")]
        private static extern int lm_conf_business_StopPlayback(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StartMediaPlay")]
        private static extern int lm_conf_business_StartMediaPlay(IntPtr instance, String file_name, int media_flag);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_StopMediaPlay")]
        private static extern int lm_conf_business_StopMediaPlay(IntPtr instance);

        [DllImport("c_wrapper.dll", EntryPoint = "lm_conf_business_PauseMediaPlay")]
        private static extern int lm_conf_business_PauseMediaPlay(IntPtr instance, int paused);


        private IntPtr c_instance_;
        private List<IConferenceBusinessObserver> observer_list_;
        private ConfBusinessCallback conf_business_callback_;

        public ConfBusinessWrapper(IntPtr c_instance)
        {
            c_instance_ = c_instance;
            observer_list_ = new List<IConferenceBusinessObserver>();

            conf_business_callback_ = new ConfBusinessCallback();
            conf_business_callback_.OnAccreditDataOperDelegate_ = new OnAccreditDataOperDelegate(OnAccreditDataOper);
            conf_business_callback_.OnAccreditDataSyncDelegate_ = new OnAccreditDataSyncDelegate(OnAccreditDataSync);
            conf_business_callback_.OnAccreditSpeakOperDelegate_ = new OnAccreditSpeakOperDelegate(OnAccreditSpeakOper);
            conf_business_callback_.OnAccreditTempAdminDelegate_ = new OnAccreditTempAdminDelegate(OnAccreditTempAdmin);
            conf_business_callback_.OnAddConfAdminDelegate_ = new OnAddConfAdminDelegate(OnAddConfAdmin);
            conf_business_callback_.OnAddConfDefaultAttendeeDelegate_ = new OnAddConfDefaultAttendeeDelegate(OnAddConfDefaultAttendee);
            conf_business_callback_.OnAddSelfAttendeeDelegate_ = new OnAddSelfAttendeeDelegate(OnAddSelfAttendee);
            conf_business_callback_.OnAdminOperConfSettingDelegate_ = new OnAdminOperConfSettingDelegate(OnAdminOperConfSetting);
            conf_business_callback_.OnAdminOperConfSettingNotifyDelegate_ = new OnAdminOperConfSettingNotifyDelegate(OnAdminOperConfSettingNotify);
            conf_business_callback_.OnApplyDataOperDelegate_ = new OnApplyDataOperDelegate(OnApplyDataOper);
            conf_business_callback_.OnApplyDataSyncDelegate_ = new OnApplyDataSyncDelegate(OnApplyDataSync);
            conf_business_callback_.OnApplySpeakOperDelegate_ = new OnApplySpeakOperDelegate(OnApplySpeakOper);
            conf_business_callback_.OnApplyTempAdminDelegate_ = new OnApplyTempAdminDelegate(OnApplyTempAdmin);
            conf_business_callback_.OnAttendeeOfflineDelegate_ = new OnAttendeeOfflineDelegate(OnAttendeeOffline);
            conf_business_callback_.OnAttendeeOnlineDelegate_ = new OnAttendeeOnlineDelegate(OnAttendeeOnline);
            conf_business_callback_.OnAuthTempAdminDelegate_ = new OnAuthTempAdminDelegate(OnAuthTempAdmin);
            conf_business_callback_.OnCancelSubGroupDelegate_ = new OnCancelSubGroupDelegate(OnCancelSubGroup);
            conf_business_callback_.OnCancelSubGroupNotifyDelegate_ = new OnCancelSubGroupNotifyDelegate(OnCancelSubGroupNotify);
            conf_business_callback_.OnCaptureVideoStateDelegate_ = new OnCaptureVideoStateDelegate(OnCaptureVideoState);
            conf_business_callback_.OnCaptureVideoStateNotifyDelegate_ = new OnCaptureVideoStateNotifyDelegate(OnCaptureVideoStateNotify);
            conf_business_callback_.OnDataOpNotifyDelegate_ = new OnDataOpNotifyDelegate(OnDataOpNotify);
            conf_business_callback_.OnDataSyncCommandDelegate_ = new OnDataSyncCommandDelegate(OnDataSyncCommand);
            conf_business_callback_.OnDataSyncCommandNotifyDelegate_ = new OnDataSyncCommandNotifyDelegate(OnDataSyncCommandNotify);
            conf_business_callback_.OnDisconnectDelegate_ = new OnDisconnectDelegate(OnDisconnect);
            conf_business_callback_.OnEnableVideoDelegate_ = new OnEnableVideoDelegate(OnEnableVideo);
            conf_business_callback_.OnEnableVideoNotifyDelegate_ = new OnEnableVideoNotifyDelegate(OnEnableVideoNotify);
            conf_business_callback_.OnEntryConferenceDelegate_ = new OnEntryConferenceDelegate(OnEntryConference);
            conf_business_callback_.OnGetConferenceDelegate_ = new OnGetConferenceDelegate(OnGetConference);
            conf_business_callback_.OnGetConferenceRealTimeInfoDelegate_ = new OnGetConferenceRealTimeInfoDelegate(OnGetConferenceRealTimeInfo);
            conf_business_callback_.OnGetConferenceSyncInfoDelegate_ = new OnGetConferenceSyncInfoDelegate(OnGetConferenceSyncInfo);
            conf_business_callback_.OnKickoutAttendeeDelegate_ = new OnKickoutAttendeeDelegate(OnKickoutAttendee);
            conf_business_callback_.OnKickoutAttendeeNotifyDelegate_ = new OnKickoutAttendeeNotifyDelegate(OnKickoutAttendeeNotify);
            conf_business_callback_.OnLaunchSigninDelegate_ = new OnLaunchSigninDelegate(OnLaunchSignin);
            conf_business_callback_.OnLaunchSigninNotifyDelegate_ = new OnLaunchSigninNotifyDelegate(OnLaunchSigninNotify);
            conf_business_callback_.OnLeaveConferenceDelegate_ = new OnLeaveConferenceDelegate(OnLeaveConference);
            conf_business_callback_.OnOperSubGroupDelegate_ = new OnOperSubGroupDelegate(OnOperSubGroup);
            conf_business_callback_.OnOperSubGroupNotifyDelegate_ = new OnOperSubGroupNotifyDelegate(OnOperSubGroupNotify);
            conf_business_callback_.OnOpRemoteDesktopSharedDelegate_ = new OnOpRemoteDesktopSharedDelegate(OnOpRemoteDesktopShared);
            conf_business_callback_.OnOpRemoteDesktopSharedNotifyDelegate_ = new OnOpRemoteDesktopSharedNotifyDelegate(OnOpRemoteDesktopSharedNotify);
            conf_business_callback_.OnRelayMsgToAllDelegate_ = new OnRelayMsgToAllDelegate(OnRelayMsgToAll);
            conf_business_callback_.OnRelayMsgToAllNotifyDelegate_ = new OnRelayMsgToAllNotifyDelegate(OnRelayMsgToAllNotify);
            conf_business_callback_.OnRelayMsgToOneDelegate_ = new OnRelayMsgToOneDelegate(OnRelayMsgToOne);
            conf_business_callback_.OnRelayMsgToOneNotifyDelegate_ = new OnRelayMsgToOneNotifyDelegate(OnRelayMsgToOneNotify);
            conf_business_callback_.OnRemoveAttendeeDelegate_ = new OnRemoveAttendeeDelegate(OnRemoveAttendee);
            conf_business_callback_.OnRemoveConerenceDelegate_ = new OnRemoveConerenceDelegate(OnRemoveConerence);
            conf_business_callback_.OnRemoveConfAdminDelegate_ = new OnRemoveConfAdminDelegate(OnRemoveConfAdmin);
            conf_business_callback_.OnRemoveConfDefaultAttendeeDelegate_ = new OnRemoveConfDefaultAttendeeDelegate(OnRemoveConfDefaultAttendee);
            conf_business_callback_.OnSetConfPasswordDelegate_ = new OnSetConfPasswordDelegate(OnSetConfPassword);
            conf_business_callback_.OnSetConfPasswordNotifyDelegate_ = new OnSetConfPasswordNotifyDelegate(OnSetConfPasswordNotify);
            conf_business_callback_.OnSigninDelegate_ = new OnSigninDelegate(OnSignin);
            conf_business_callback_.OnSigninNotifyDelegate_ = new OnSigninNotifyDelegate(OnSigninNotify);
            conf_business_callback_.OnSpeakNotifyDelegate_ = new OnSpeakNotifyDelegate(OnSpeakNotify);
            conf_business_callback_.OnStartDesktopShareDelegate_ = new OnStartDesktopShareDelegate(OnStartDesktopShare);
            conf_business_callback_.OnStartDesktopShareNotifyDelegate_ = new OnStartDesktopShareNotifyDelegate(OnStartDesktopShareNotify);
            conf_business_callback_.OnStartPlaybackDelegate_ = new OnStartPlaybackDelegate(OnStartPlayback);
            conf_business_callback_.OnStartPlaybackNotifyDelegate_ = new OnStartPlaybackNotifyDelegate(OnStartPlaybackNotify);
            conf_business_callback_.OnStartPreviewDesktopDelegate_ = new OnStartPreviewDesktopDelegate(OnStartPreviewDesktop);
            conf_business_callback_.OnStartPreviewDesktopNotifyDelegate_ = new OnStartPreviewDesktopNotifyDelegate(OnStartPreviewDesktopNotify);
            conf_business_callback_.OnStartPreviewVideoDelegate_ = new OnStartPreviewVideoDelegate(OnStartPreviewVideo);
            conf_business_callback_.OnStartPreviewVideoNotifyDelegate_ = new OnStartPreviewVideoNotifyDelegate(OnStartPreviewVideoNotify);
            conf_business_callback_.OnStopDesktopShareDelegate_ = new OnStopDesktopShareDelegate(OnStopDesktopShare);
            conf_business_callback_.OnStopDesktopShareNotifyDelegate_ = new OnStopDesktopShareNotifyDelegate(OnStopDesktopShareNotify);
            conf_business_callback_.OnStopPlaybackDelegate_ = new OnStopPlaybackDelegate(OnStopPlayback);
            conf_business_callback_.OnStopPlaybackNotifyDelegate_ = new OnStopPlaybackNotifyDelegate(OnStopPlaybackNotify);
            conf_business_callback_.OnStopPreviewDesktopDelegate_ = new OnStopPreviewDesktopDelegate(OnStopPreviewDesktop);
            conf_business_callback_.OnStopPreviewDesktopNotifyDelegate_ = new OnStopPreviewDesktopNotifyDelegate(OnStopPreviewDesktopNotify);
            conf_business_callback_.OnStopPreviewVideoDelegate_ = new OnStopPreviewVideoDelegate(OnStopPreviewVideo);
            conf_business_callback_.OnStopPreviewVideoNotifyDelegate_ = new OnStopPreviewVideoNotifyDelegate(OnStopPreviewVideoNotify);
            conf_business_callback_.OnSwitchMainVideoDelegate_ = new OnSwitchMainVideoDelegate(OnSwitchMainVideo);
            conf_business_callback_.OnSwitchMainVideoNotifyDelegate_ = new OnSwitchMainVideoNotifyDelegate(OnSwitchMainVideoNotify);
            conf_business_callback_.OnSyncOpNotifyDelegate_ = new OnSyncOpNotifyDelegate(OnSyncOpNotify);
            conf_business_callback_.OnTempAdminNotifyDelegate_ = new OnTempAdminNotifyDelegate(OnTempAdminNotify);
            conf_business_callback_.OnUpdateAttendeeDelegate_ = new OnUpdateAttendeeDelegate(OnUpdateAttendee);
            conf_business_callback_.OnUpdateAttendeeNameDelegate_ = new OnUpdateAttendeeNameDelegate(OnUpdateAttendeeName);
            conf_business_callback_.OnUpdateAttendeeNameNotifyDelegate_ = new OnUpdateAttendeeNameNotifyDelegate(OnUpdateAttendeeNameNotify);
            conf_business_callback_.OnUpdateConerenceDelegate_ = new OnUpdateConerenceDelegate(OnUpdateConerence);
            conf_business_callback_.OnVideoDeviceChangedNotifyDelegate_ = new OnVideoDeviceChangedNotifyDelegate(OnVideoDeviceChangedNotify);
            conf_business_callback_.OnVideoShowNameChangedNotifyDelegate_ = new OnVideoShowNameChangedNotifyDelegate(OnVideoShowNameChangedNotify);

            conf_business_callback_.OnAskRemoteDesktopControlDelegate_ = new OnAskRemoteDesktopControlDelegate(OnAskRemoteDesktopControl);
            conf_business_callback_.OnAskRemoteDesktopControlNotifyDelegate_ = new OnAskRemoteDesktopControlNotifyDelegate(OnAskRemoteDesktopControlNotify);
            conf_business_callback_.OnAbstainRemoteDesktopControlDelegate_ = new OnAbstainRemoteDesktopControlDelegate(OnAbstainRemoteDesktopControl);
            conf_business_callback_.OnAbstainRemoteDesktopControlNotifyDelegate_ = new OnAbstainRemoteDesktopControlNotifyDelegate(OnAbstainRemoteDesktopControlNotify);
            conf_business_callback_.OnAnswerRemoteDesktopControlDelegate_ = new OnAnswerRemoteDesktopControlDelegate(OnAnswerRemoteDesktopControl);
            conf_business_callback_.OnAnswerRemoteDesktopControlNotifyDelegate_ = new OnAnswerRemoteDesktopControlNotifyDelegate(OnAnswerRemoteDesktopControlNotify);
            conf_business_callback_.OnAbortRemoteDesktopControlDelegate_ = new OnAbortRemoteDesktopControlDelegate(OnAbortRemoteDesktopControl);
            conf_business_callback_.OnAbortRemoteDesktopControlNotifyDelegate_ = new OnAbortRemoteDesktopControlNotifyDelegate(OnAbortRemoteDesktopControlNotify);

            conf_business_callback_.OnStartMediaPlayDelegate_ = new OnStartMediaPlayDelegate(OnStartMediaPlay);
            conf_business_callback_.OnStartMediaPlayNotifyDelegate_ = new OnStartMediaPlayNotifyDelegate(OnStartMediaPlayNotify);
            conf_business_callback_.OnStopMediaPlayDelegate_ = new OnStopMediaPlayDelegate(OnStopMediaPlay);
            conf_business_callback_.OnStopMediaPlayNotifyDelegate_ = new OnStopMediaPlayNotifyDelegate(OnStopMediaPlayNotify);
            conf_business_callback_.OnPauseMediaPlayDelegate_ = new OnPauseMediaPlayDelegate(OnPauseMediaPlay);
            conf_business_callback_.OnPauseMediaPlayNotifyDelegate_ = new OnPauseMediaPlayNotifyDelegate(OnPauseMediaPlayNotify);

            lm_conf_business_set_observer(c_instance, ref conf_business_callback_);
        }

        #region IConferenceBusiness 成员

        public int AddObserver(IConferenceBusinessObserver observer)
        {
            if (observer_list_.Contains(observer))
                return -1;
            observer_list_.Add(observer);
            return 0;
        }

        public int RemoveObserver(IConferenceBusinessObserver observer)
        {
            observer_list_.Remove(observer);
            return 0;
        }

        public int EntryConference(String json_address, uint id_conference, uint conf_tag,
            String conf_psw, bool is_md5, String account, uint id_org, String name)
        {
            return lm_conf_business_EntryConference(c_instance_,
                json_address, 
                (int)id_conference,
                (int)conf_tag, 
                conf_psw, 
                is_md5 ? 1 : 0,
                account,
                (int)id_org, 
                name);
        }

        public int LeaveConference()
        {
            return lm_conf_business_LeaveConference(c_instance_);
        }

        public int ApplySpeak(bool apply)
        {
            return lm_conf_business_ApplySpeak(c_instance_, apply ? 1 : 0);
        }

        public int AccreditSpeak(String account, bool accredit)
        {
            return lm_conf_business_AccreditSpeak(c_instance_, account, accredit ? 1 : 0);
        }

        public int ApplyDataOper(bool apply)
        {
            return lm_conf_business_ApplyDataOper(c_instance_, apply ? 1 : 0);
        }

        public int AccreditDataOper(String account, bool accredit)
        {
            return lm_conf_business_AccreditDataOper(c_instance_, account, accredit ? 1 : 0);
        }

        public int ApplyDataSync(bool apply)
        {
            return lm_conf_business_ApplyDataSync(c_instance_, apply ? 1 : 0);
        }

        public int AccreditDataSync(String account, bool accredit)
        {
            return lm_conf_business_AccreditDataSync(c_instance_, account, accredit ? 1 : 0);
        }

        public int DataSyncCommand(String command)
        {
            return lm_conf_business_DataSyncCommand(c_instance_, command);
        }

        public int ApplyTempAdmin(bool apply)
        {
            return lm_conf_business_ApplyTempAdmin(c_instance_, apply ? 1 : 0);
        }

        public int AccreditTempAdmin(String account, bool accredit)
        {
            return lm_conf_business_AccreditTempAdmin(c_instance_, account, accredit ? 1 : 0);
        }

        public int AuthTempAdmin(String admin_psw)
        {
            return lm_conf_business_AuthTempAdmin(c_instance_, admin_psw);
        }

        public int StartPreviewVideo(String shower, int index, ulong context)
        {
            return lm_conf_business_StartPreviewVideo(c_instance_, shower, index, context);
        }

        public int StopPreviewVideo(String shower, int index, ulong context)
        {
            return lm_conf_business_StopPreviewVideo(c_instance_, shower, index, context);
        }

        public int SwitchMainVideo(int index)
        {
            return lm_conf_business_SwitchMainVideo(c_instance_, index);
        }

        public int EnableVideo(bool enabled)
        {
            return lm_conf_business_EnableVideo(c_instance_, enabled　? 1 : 0);
        }

        public int CaptureVideoState(int devindex, bool enabled)
        {
            return lm_conf_business_CaptureVideoState(c_instance_, devindex, enabled ? 1 : 0);
        }

        public int UpdateVideoShowName(int devindex, String name)
        {
            return lm_conf_business_UpdateVideoShowName(c_instance_, devindex, name);
        }

        public int KickOutAttendee(String account)
        {
            return lm_conf_business_KickOutAttendee(c_instance_, account);
        }

        public int UpdataAttendeeName(String name)
        {
            return lm_conf_business_UpdataAttendeeName(c_instance_, name);
        }

        public int RelayMsgToOne(String receiver, String msg)
        {
            return lm_conf_business_RelayMsgToOne(c_instance_, receiver, msg);
        }

        public int RelayMsgToAll(String msg)
        {
            return lm_conf_business_RelayMsgToAll(c_instance_, msg);
        }

        public int AdminOperConfSetting(int cmd, int cmd_value)
        {
            return lm_conf_business_AdminOperConfSetting(c_instance_, cmd, cmd_value);
        }

        public int SetConfPassword(String psw)
        {
            return lm_conf_business_SetConfPassword(c_instance_, psw);
        }

        public int StartDesktopShare()
        {
            return lm_conf_business_StartDesktopShare(c_instance_);
        }

        public int StopDesktopShare()
        {
            return lm_conf_business_StopDesktopShare(c_instance_);
        }

        public int StartPreviewDesktop(String sharer, ulong context)
        {
            return lm_conf_business_StartPreviewDesktop(c_instance_, sharer, context);
        }

        public int StopPreviewDesktop(String sharer, ulong context)
        {
            return lm_conf_business_StopPreviewDesktop(c_instance_, sharer, context);
        }

        public int AskRemoteDesktopControl(String sharer)
        {
            return lm_conf_business_AskRemoteDesktopControl(c_instance_, sharer);
        }

        public int AbstainRemoteDesktopControl(String sharer)
        {
            return lm_conf_business_AbstainRemoteDesktopControl(c_instance_, sharer);
        }

        public int AnswerRemoteDesktopControl(String controller, bool allow)
        {
            return lm_conf_business_AnswerRemoteDesktopControl(c_instance_, controller, allow ? 1 : 0);
        }

        public int AbortRemoteDesktopControl(String controller)
        {
            return lm_conf_business_AbortRemoteDesktopControl(c_instance_, controller);
        }

        public int LaunchSignin(LMConferenceSigninType signin_type)
        {
            return lm_conf_business_LaunchSignin(c_instance_, (int)signin_type);
        }

        public int Signin(bool is_signin)
        {
            return lm_conf_business_Signin(c_instance_, is_signin ?　1 : 0);
        }

        public int OperSubGroup(String account, uint id_subgroup)
        {
            return lm_conf_business_OperSubGroup(c_instance_, account, (int)id_subgroup);
        }

        public int CancelSubGroup()
        {
            return lm_conf_business_CancelSubGroup(c_instance_);
        }

        public int OpRemoteDesktopShared(String sharer, bool is_start)
        {
            return lm_conf_business_OpRemoteDesktopShared(c_instance_, sharer, is_start ? 1 : 0);
        }

        public int StartPlayback(String file_name)
        {
            return lm_conf_business_StartPlayback(c_instance_, file_name);
        }

        public int StopPlayback()
        {
            return lm_conf_business_StopPlayback(c_instance_);
        }

        public int StartMediaPlay(String file_name, int media_flag)
        {
            return lm_conf_business_StartMediaPlay(c_instance_, file_name, media_flag);
        }

        public int StopMediaPlay()
        {
            return lm_conf_business_StopMediaPlay(c_instance_);
        }

        public int PauseMediaPlay(bool paused)
        {
            return lm_conf_business_PauseMediaPlay(c_instance_, paused ? 1 : 0);
        }

        #endregion
    }
}
