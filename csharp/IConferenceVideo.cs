﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CSharpWrapper
{
    public interface IConferenceVideo
    {
	    int AddObserver(IConferenceVideoObserver observer);
	    int RemoveObserver(IConferenceVideoObserver observer);

	    int AddLocalPreview(
		    int id_device, 
		    IntPtr windows, 
		    UInt64 context,
		    UInt32 z_order,
		    float left,
		    float top,
		    float right,
            float bottom,
            ref int id_preview);

	    int ConfigureLocalPreview(
            int id_device,
		    int id_preview,
		    UInt32 z_order,
		    float left,
		    float top,
		    float right,
		    float bottom);

        int ChangeLocalPreviewWindow(
            int id_device, 
            int id_preview,
            IntPtr window,
            UInt32 z_order,
            float left,
            float top,
            float right,
            float bottom);

	    int RemoveLocalPreview(
            int id_device, 
            int id_preview);

        int EnableMirrorLocalPreview(int id_device, int id_preview,
			bool enable, bool mirror_xaxis, bool mirror_yaxis);

		int MirrorLocalPreviewState(int id_device, int id_preview,
			ref bool enable, ref bool mirror_xaxis, ref bool mirror_yaxis);

		int RotateLocalPreview(int id_device,
			int id_preview, int rotation);

		int RotateLocalPreviewState(int id_device,
			int id_preview, ref int rotation);

		int GetLocalPreviewResolution(
			int id_device, ref int width, ref int height);

	    int AddRemotePreview(
		    String account,
            int id_device,
            IntPtr windows,
		    UInt32 z_order,
		    float left,
		    float top,
		    float right,
            float bottom,
            ref int id_preview);

	    int ConfigureRemotePreview(
		    String account,
            int id_device,
		    int id_preview,
		    UInt32 z_order,
		    float left,
		    float top,
		    float right,
		    float bottom);

        int ChangeRemotePreviewWindow(
            String account,
            int id_device,
            int id_preview,
            IntPtr window,
            UInt32 z_order,
            float left,
            float top,
            float right,
            float bottom);

	    int RemoveRemotePreview(
            String account,
            int id_device, 
            int id_preview);

        int EnableMirrorRemotePreview(String account,
			int id_device, int id_preview,
			bool enable, bool mirror_xaxis, bool mirror_yaxis);

		int MirrorRemotePreviewState(String account,
			int id_device, int id_preview,
			ref bool enable, ref bool mirror_xaxis, ref bool mirror_yaxis);

		int RotateRemotePreview(String account,
			int id_device, int id_preview, int rotation);

		int RotateRemotePreviewState(String account,
			int id_device, int id_preview, ref int rotation);

		int EnableRemotePreviewColorEnhancement(String account,
			int id_device, bool enable);

		int RemotePreviewColorEnhancementState(String account,
			int id_device, ref bool enable);

        int EnableRemotePreview(String account, int id_device, bool enable);
        int GetRemotePreviewState(String account, int id_device, ref bool enable);

        int GetRemotePreviewResolution(String account,
            int id_device, ref int width, ref int height);

	    int NumOfDevice(ref int nums);
        int GetDeviceName(int id_device, StringBuilder name, StringBuilder guid);
        int GetDeviceIdWithGuid(String guid, ref int id_device);
        int GetDeviceFormat(int id_device, List<Int32> pixels);
        int OpenPropertypage(int id_device, IntPtr window, String title);

	    int SwitchMobiledevicesCamera();
        int GetMobiledevicesActiveCameraId(ref int id_device);

        int IsCaptureing(int id_device, ref bool v);

        int SetVideoConfig(int id_device, LMVideoConfig config);
        int GetVideoConfig(int id_device, ref LMVideoConfig config);

        int EnableDeflickering(int id_device, bool enable);
		int DeflickeringState(int id_device, ref bool enable);

        int GetCaptureDeviceSnapshot(
            int id_device, 
            String file_name);

        int GetRenderSnapshot(
            String account,
            int id_device, 
            int id_preview,
            String file_name);

	    int SetShowNameInVideo(bool enable);
	    int GetShowNameInVideoState(ref bool enable);

	    int EnableDebugVideo(bool enable);
        int GetDebugVideoState(ref bool enable);

        int GetCameraType(int id_device, ref LMCameraType type);

        int AddIPCamera(LMIPCameraInfo info, int channel, bool is_main_stream, String name);
        int RemoveIPCamera(int identity, int channel, bool is_main_stream);

        int AddRAWDataCamera(int identity, String name);
        int RemoveRAWDataCamera(int identity);
        int IncomingRAWData(int identity, byte[] data,
            int width, int height, LMRawVideoType type);
    }

    public static class StaticVideo
    {
        [System.Runtime.InteropServices.DllImport("c_wrapper.dll")]
        public static extern void conf_video_SetStartImage(String jpg_file);

        [System.Runtime.InteropServices.DllImport("c_wrapper.dll")]
        public static extern void conf_video_SetTimeoutImage(String jpg_file);

        [System.Runtime.InteropServices.DllImport("c_wrapper.dll")]
        public static extern void conf_video_SetRemoteDisableRemoteVideoImage(String jpg_file);

        [System.Runtime.InteropServices.DllImport("c_wrapper.dll")]
        public static extern void conf_video_SetLocalDisableRemoteVideoImage(String jpg_file);

        public static void SetStartImage(String jpg_file)
        {
            conf_video_SetStartImage(jpg_file);
        }

        public static void SetTimeoutImage(String jpg_file)
        {
            conf_video_SetTimeoutImage(jpg_file);
        }

        public static void SetRemoteDisableRemoteVideoImage(String jpg_file)
        {
            conf_video_SetRemoteDisableRemoteVideoImage(jpg_file);
        }

        public static void SetLocalDisableRemoteVideoImage(String jpg_file)
        {
            conf_video_SetLocalDisableRemoteVideoImage(jpg_file);
        }

    }
}
