﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWrapper
{
    /**
   *	调用会议中间层模式
   *	kCallHall,只开启会议大厅
   *	kCallConference,只开启会议
   *	k2In1,二者同时开启
   */
    public enum LMCallCenterMode
    {
        kCallHall,
        kCallConference,
        k2In1,
    };

    /**
    *	Windows平台应用层字符集,其他平台UTF8
    */
    public enum LMWindowsCharacterSet
    {
        kCharSetUTF8 = 0,
        kCharSetMultiByte,
    };

    /**
    *	参会者终端系统
    */
    public enum LMConferenceClientType
    {
        kUnknowClient = -1,
        kPCClient,
        kMACClient,
        kLinuxClient,
        kAndroidClient,
        kIPhoneClient,
        kIPadClient,
        kWinPhoneClient,
        kH323Client,
        kBoxClient,
    };

    /**
    *	参会者签到类型
    *	kImmediatelySignin,立即
    *	kTimingSignin,延时
    *	当前版本只支持kImmediatelySignin类型
    */
    public enum LMConferenceSigninType
    {
        kNoSignin = -1,
        kImmediatelySignin = 0,
        kTimingSignin,
    };

    /**
    *	摄像头类型
    *	kLocalCamera,本地摄像头
    *	kDesktopCamera,虚拟桌面摄像头
    *	kIPCamera,网络摄像头式
    *	kPlayBackCamera,录制回放摄像头
    *	kMediaFileCamera,媒体文件播放摄像头
    */
    public enum LMCameraType
    {
        kUnknowCamera = -1,
        kLocalCamera,
        kDesktopCamera,
        kIPCamera,
        kPlayBackCamera,
        kMediaFileCamera,
        kMixerCamera,
        kRAWDataCamera,
        kMaxCameraType = kRAWDataCamera + 1,
    };

    public enum LMCameraSubType
    {
        kUnknowSubType = -1,
        kLocalCameraSubType,
        kDesktopSubType,
        kIPCameraSubType,
        kPlayBackSubType,
        kMediaPlayerSubType,
        kMixerSubType,
        kRAWDataSubType,
    };

    public enum LMRawVideoType
    {
        kVideoI420 = 0,
        kVideoYV12 = 1,
        kVideoYUY2 = 2,
        kVideoUYVY = 3,
        kVideoIYUV = 4,
        kVideoARGB = 5,
        kVideoRGB24 = 6,
        kVideoRGB565 = 7,
        kVideoARGB4444 = 8,
        kVideoARGB1555 = 9,
        kVideoMJPEG = 10,
        kVideoNV12 = 11,
        kVideoNV21 = 12,
        kVideoBGRA = 13,
        kVideoUnknown = 99
    };

    /**
    *	与会者当前操作
    *	kOpsNormal,无
    *	kOpsApplySpeak,申请发言状态(会议室为集中管理模式)
    *	kOpsSpeak,发言状态
    *	kOpsApplyOperData,申请数据操作状态(会议室为集中管理模式)
    *	kOpsOperData,数据操作状态
    *	kOpsApplySync,申请同步状态(会议室为集中管理模式)
    *	kOpsSync,同步状态(会议室只有一个同步操作者)
    *	kOpsApplyAdmin,申请为会议室管理员状态
    *	kOpsRecord,录制状态
    *	kOpsRecordPause,录制暂停状态(当前版本不支持)
    *	kOpsMediaPlay,媒体播放状态(会议室只有一个媒体播放者)
    *	kOpsMediaPlayPause,媒体播放暂停状态(当前版本不支持)
    *	kOpsSharedDesktop,桌面共享状态(会议室只有一个桌面共享者)
    *	kOpsOperWhiteboard,白板操作状态(当前版本不支持)
    *	kOpsPlayback,回放录制文件状态(会议室只有一个回放录制文件者)
    *	kOpsAskRemoteControlDesktop远程控制被询问状态
    *	kOpsInRemoteControlDesktop桌面被远程控制状态
    */
    public enum LMAttendeeOpsFlags
    {
        kOpsNormal = 0,
        kOpsApplySpeak = 1 << 0,
        kOpsSpeak = 1 << 1,
        kOpsApplyOperData = 1 << 2,
        kOpsOperData = 1 << 3,
        kOpsApplySync = 1 << 4,
        kOpsSync = 1 << 5,
        kOpsApplyAdmin = 1 << 6,
        kOpsRecord = 1 << 7,
        kOpsRecordPause = 1 << 8,
        kOpsMediaPlay = 1 << 9,
        kOpsMediaPlayPause = 1 << 10,
        kOpsSharedDesktop = 1 << 11,
        kOpsOperWhiteboard = 1 << 12,
        kOpsPlayback = 1 << 13,
        kOpsAskRemoteControlDesktop = 1 << 14,
        kOpsInRemoteControlDesktop = 1 << 15,
    };

    /**
    *	会议室状态
    *	kConferenceFree,是否为自由模式标记
    *	kConferenceLock,是否为锁定状态标记
    *	kConferenceOpen,是否为开放状态标记(当前版本不支持)
    *	kConferenceHide,是否为隐藏状态标记(当前版本不支持)
    *	kConferenceForceSync是否为强制同步状态标记(当前版本不支持)
    *	kConferenceDisableText是否为禁止文字消息状态标记(当前版本不支持)
    *	kConferenceDisableRecord是否为禁止录制状态标记
    *	kConferenceDisableBrowVideo是否为禁止视频共享状态标记
    *	kConferencePlayback是否有回放者状态标记
    *	kConferenceDisableMicrophone是否为全体禁言状态标记
    *	kConferenceMuted是否为全体静音状态标记
    *	kConferenceLockScreen是否为全体锁屏状态标记
    *	kConferenceMediaPlay是否有媒体播放者标记
    *	kConferenceMediaPause是否有媒体播放暂停标记
    */
    public enum LMConerenceFlags
    {
        kConferenceFree = 1 << 0,
        kConferenceLock = 1 << 1,
        kConferenceOpen = 1 << 2,
        kConferenceHide = 1 << 3,
        kConferenceForceSync = 1 << 4,
        kConferenceDisableText = 1 << 5,
        kConferenceDisableRecord = 1 << 6,
        kConferenceDisableBrowVideo = 1 << 7,
        kConferencePlayback = 1 << 8,
        kConferenceDisableMicrophone = 1 << 9,
        kConferenceMuted = 1 << 10,
        kConferenceLockScreen = 1 << 11,
        kConferenceMediaPlay = 1 << 12,
        kConferenceMediaPause = 1 << 13,
    };

    /**
    *	会议室状态操作命令字
    *	非0值为置状态,0取消状态
    *	kSetLockCommand修改会议室锁定		
    *	kSetFreeCommand修改会议管理模式(集中管理与自由发言)
    *	kSetForceSyncCommand会议室是否为强制同步设置(当前版本不支持)
    *	kSetHideCommand修改会议室隐藏状态(当前版本不支持)	
    *	kSetDisableTextCommand修改文字消息状态(当前版本不支持)	
    *	kSetDisableRecordCommand修改录制状态
    *	kSetDisableBrowVideoCommand修改视频共享状态
    *	kSetLockScreenCommand修改会议室锁屏状态
    *	kSetDisableMicrophoneCommand修改会议室禁言状态
    *	kSetMutedCommand修改会议室静音状态
    */
    public enum LMConferenceCommandModes
    {
        kSetLockCommand,
        kSetFreeCommand,
        kSetForceSyncCommand,
        kSetHideCommand,
        kSetDisableTextCommand,
        kSetDisableRecordCommand,
        kSetDisableBrowVideoCommand,
        kSetLockScreenCommand,
        kSetDisableMicrophoneCommand,
        kSetMutedCommand,
    };

    /**
    *	会议管理员类型
    *	kConferenceNonAdmin一般与会者
    *	kConferenceSystemAdmin系统管理员(一般为企业管理员)
    *	kConferenceTimelessAdmin默认管理员(由系统管理员在建立会议室指定)
    *	kConferenceTempAdmin临时管理员(整个会议期间有效，由系统管理员或默认管理员授权或认证申请)
    *	kConferenceGroupAdmin会议分组管理员(当前版本不支持)
    */
    public enum LMConferenceAdminModes
    {
        kConferenceNonAdmin,
        kConferenceSystemAdmin,
        kConferenceTimelessAdmin,
        kConferenceTempAdmin,
        kConferenceGroupAdmin,
    };

    /**
    *	会议录制模块标记
    *	kRecordVoice录制音频模块
    *	kRecordVideo录制视频模块
    *	kRecordVideo录制白板模块
    *	kRecordAppShare录制桌面共享模块
    *	kRecordFileShare录制文件共享模块(当前版本不支持)
    *	kRecordWeb录制协同浏览模块(当前版本不支持)
    *	kRecordVote录制投票模块(当前版本不支持)
    *	kRecordMedia录制媒体播放模块
    *	kRecordText录制文字消息(当前版本不支持)
    *	kRecordServer服务器录制(当前版本不支持)
    */
    public enum LMConferenceRecordFlags
    {
        kRecordNull = 0,
        kRecordVoice = 1 << 0,
        kRecordVideo = 1 << 1,
        kRecordWbd = 1 << 2,
        kRecordAppShare = 1 << 3,
        kRecordFileShare = 1 << 4,
        kRecordWeb = 1 << 5,
        kRecordVote = 1 << 6,
        kRecordMedia = 1 << 7,
        kRecordText = 1 << 8,
        kRecordServer = 1 << 31,
    };

    /**
    *	代理类型,当前只支持Socks4与Socks5
    */
    public enum LMProxyType
    {
        kPTInvalid = 0,
        kPTSocks4,
        kPTSocks5,
        kPTHttpConnect,
        kPTHttpTunnel,
    };

    /**
    *	会议室类型
    *	kConferenceNormal单位会议室
    *	kConferencePublic公共会议室
    *	kConferenceFriend好友会议室(当前版本不支持)
    */
    public enum LMConferenceType
    {
        kConferenceNormal = 0,
        kConferencePublic,
        kConferenceFriend,
    };

    /**
    *	数据服务器模块类型，用于切换服务器与测速.
    */
    public enum LMDataServerModule
    {
        kUnknowDataModule = -1,
        kVideoDataModule = 0,
        kVoiceDataModule,
        kWhiteboardDataModule,
        kDesktopSharedDataModule,
    };

    /**
    *	音频编码方式
    */
    public enum LMVoiceCodecModes
    {
        kUnknowVoiceCodec = -1,
        kOpusCodec = 0,
        kISACCodec,
        kSpeexCodec,
        kILBCCodec,
        kG7211Codec,
        kPCMUCodec,
        kPCMACodec,
        kL16Codec,
    };

    /**
    *	音频编码延时包大小
    */
    public enum LMVoiceDelayModes
    {
        k30MSDelay = 0,
        k60MSDelay,
        k90MSDelay,
        k120MSDelay,
    };

    public enum LMVoiceDeviceType
    {
        kUnknowInputVoiceDevice = -1,
        kMediaFileInputVoiceDevice = 0,
        kIPCameraInputVoiceDevice = 1,
        kPlaybackInputVoiceDevice = 2,
        kExternalRAWDataInputVoiceDevice = 3,
        kExternalRTPDataInputVoiceDevice = 4,
        kLocalInputVoiceDevice = 5,
        kMaxInputVoiceDevice = kLocalInputVoiceDevice + 1,
    };

    /**
    *	屏幕录制音频编码格式
    */
    public enum LMRecordSoundFormats
    {
        kRsfAAC,
        kRsfMP3,
        kRsfPCMU,
        kRsfPCMA,
        kRsfL16
    };

    /**
    *	屏幕录制格式
    */
    public enum LMScreenRecordFormats
    {
        kRecordMP4 = 0,
        kRecordAVI,
        kRecordFLV,
        kRecordMKV,
    };

    /**
    *	屏幕录制音频选择
    *	kScreenRecordInputVoice会议室输入声音
    *	kScreenRecordOutputVoice会议室输出声音
    *	kScreenRecordPlatformMicrophoneVoice(当前版本不支持)
    *	kScreenRecordPlatformSoundCardVoice(当前版本不支持)
    */
    public enum LMScreenRecordVoiceFlags
    {
        kScreenRecordInputVoice = 1 << 0,
        kScreenRecordOutputVoice = 1 << 1,
        kScreenRecordPlatformMicrophoneVoice = 1 << 2,	//for windows
        kScreenRecordPlatformSoundCardVoice = 1 << 3,	//for windows and xp up
    };

    /**
    *	屏幕录制视频编码质量
    */
    public enum LMScreenRecordQP
    {
        kQPOrdinary = 0,	//普通
        kQPClear,			//清晰
        kQPVeryClear,		//非常清晰
    };

    /**
    *	屏幕录制视频编码缩放率
    */
    public enum LMScreenRecordZoom
    {
        kZoom100 = 0,
        kZoom75,
        kZoom50,
    };

    /**
    *	会议录制类型
    */
    public enum LMConfRecordType
    {
        kConfNoRecord = -1,		//没有录制
        kConfModuleRecord = 0,	//模块录制
        kConfScreenRecord = 1,	//屏幕录制
    };
    /**
    *	视频编码缩放算法,kBox质量最好
    */
    public enum LMVideoFrameResampling
    {
        kNoRescaling,         // Disables rescaling.
        kFastRescaling,       // Point filter.
        kBiLinear,            // Bi-linear interpolation.
        kBox,                 // Box inteprolation.
    };

    /**
    *	音频降噪模式
    */
    public enum LMNsModes    // type of Noise Suppression
    {
        kNsUnchanged = 0,   // previously set mode
        kNsDefault,         // platform default
        kNsConference,      // conferencing default
        kNsLowSuppression,  // lowest suppression
        kNsModerateSuppression,
        kNsHighSuppression,
        kNsVeryHighSuppression,     // highest suppression
    };

    /**
    *	音频自动增益模式
    */
    public enum LMAgcModes                  // type of Automatic Gain Control
    {
        kAgcUnchanged = 0,        // previously set mode
        kAgcDefault,              // platform default
        // adaptive mode for use when analog volume control exists (e.g. for
        // PC softphone)
        kAgcAdaptiveAnalog,
        // scaling takes place in the digital domain (e.g. for conference servers
        // and embedded devices)
        kAgcAdaptiveDigital,
        // can be used on embedded devices where the capture signal level
        // is predictable
        kAgcFixedDigital
    };

    /**
    *	音频回音消除模式
    */
    public enum LMEcModes                   // type of Echo Control
    {
        kEcUnchanged = 0,          // previously set mode
        kEcDefault,                // platform default
        kEcConference,             // conferencing default (aggressive AEC)
        kEcAec,                    // Acoustic Echo Cancellation
        kEcAecm,                   // AEC mobile
    };

    /**
    *	移动设备音频回音消除级别
    */
    public enum LMAecmModes                 // mode of AECM
    {
        kAecmQuietEarpieceOrHeadset = 0,
        // Quiet earpiece or headset use
        kAecmEarpiece,             // most earpiece use
        kAecmLoudEarpiece,         // Loud earpiece or quiet speakerphone use
        kAecmSpeakerphone,         // most speakerphone use (default)
        kAecmLoudSpeakerphone      // Loud speakerphone
    };

    /**
    *	音频静音检测模式
    */
    public enum LMVadModes                 // degree of bandwidth reduction
    {
        kVadConventional = 0,      // lowest reduction
        kVadAggressiveLow,
        kVadAggressiveMid,
        kVadAggressiveHigh         // highest reduction
    };

    /**
    *	音频录制或从文件播放格式
    */
    public enum LMVoiceFileFormats
    {
        kFileFormatWavFile = 1,
        kFileFormatCompressedFile = 2,
        kFileFormatAviFile = 3,
        kFileFormatPreencodedFile = 4,
        kFileFormatPcm16kHzFile = 7,
        kFileFormatPcm8kHzFile = 8,
        kFileFormatPcm32kHzFile = 9
    };

    /**
    *	音频传输模式
    *	kVoiceDefaultIO默认传输，udp双倍包
    *	kVoiceAdaptiveIO根据网络情况自动决策是否发双倍包
    *	kVoiceReliableIO可靠通道传送
    */
    public enum LMVoiceIOModes
    {
        kVoiceDefaultIO = 0,
        kVoiceAdaptiveIO = 1,
        kVoiceReliableIO = 2,
    };

    /**
    *	会议录制数据类型
    */
    public enum LMRecSessionType
    {
        kRec,
        kRecVoice,
        kRecVideo,
        kRecWhiteboard,
        kRecDesktopShare,
        kRecTxt,
    };

    public enum LMShareDesktopDisplayModes
    {
        kDMNormal = 0,	//原始尺寸
        kDMFitWindow,	//按比例缩放适合窗口
        kDMFillWindow,	//填充窗口
    };

    public enum LMParamCommonKey
    {
        PCK_START = 1,
        PCK_COUNT,
        PCK_TOTAL,
        PCK_OPERATE_TYPE,
        PCK_SERVER_ID,
        PCK_TAG,
        PCK_TYPE,
        PCK_WORLD_ID,
        PCK_CONNECT_TYPE,
        PCK_CONNECT_ID,
        PCK_CONNECT_ACCOUNT,
        PCK_HTTP_TYPE,
        PCK_CONF_CODE,
        PCK_SOURCE_VERSION,
        PCK_CLIENT_TYPE,
        PCK_CLIENT_VERSION,
        PCK_CLIENT_MIN_VERSION,
        PCK_CLIENT_URL,

        PCK_CONDITION = 20,
        PCK_ORDERBY,
        PCK_ACCOUNT,
        PCK_NAME,
        PCK_PARAM,
        PCK_MULTI_ACCOUNT,
        PCK_CONF_PASSWORD,
        PCK_IS_CREATE_CODE,
        PCK_CONF_GUEST_USER,
        PCK_SESSION_ID,
        PCK_CONF_NAME,
        PCK_QUERY_TIME,
        PCK_USER_PERM,
        PCK_SUCCESS_COUNT,
        PCK_ORG_ACCOUNT,
        PCK_TOKEN,
        PCK_VERSION_NAME,
        PCK_VERSION_INFO,
        PCK_MAC_ADDRESS,
        PCK_ORG_SETTING,
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMRecSessionInfo
    {
        public int rec_session_id;
        public LMRecSessionType rec_session_type;
        public LMCameraSubType camera_sub_type;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string account;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string name;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMProxyOptions 
    {
        public LMProxyType proxyType;
        public UInt16 proxyPort;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
	    public string sProxyAddr;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
	    public string sProxyUserName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
	    public string sProxyPwd;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMInitOptions
    {
	    public LMCallCenterMode callMode;
	    public LMWindowsCharacterSet char_set;
        public LMProxyOptions proxy_options;
        public int auto_connect;
        public int screen_copy_fps;
        public int screen_copy_layer_window;
        public int enable_desktop_camera;
        public int use_tcp;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMPerMonitor
    {
        public Int64 msStartTime;
        public Int64 bytesSentTotal;
        public Int64 bytesRecvTotal;

        public double mbpsSendRate;			// sending rate in Mb/s
        public double mbpsRecvRate;			// receiving rate in Mb/s

        public double avgmbpsSendRate;
        public double avgmbpsRecvRate;

        public Int64 bytesAllSentTotal;
        public Int64 bytesAllRecvTotal;

        public double mbpsAllSendRate;		// sending rate in Mb/s
        public double mbpsAllRecvRate;		// receiving rate in Mb/s

        public double avgmbpsAllSendRate;
        public double avgmbpsAllRecvRate;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMDataServerInfo
    {
	    public LMDataServerModule type;
	    public int id_server;
	    public int id_access_server;
	    public int is_online;
	    public int max_attendees;
	    public int current_online_attendees;
	    public int port;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
	    public string address;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
	    public string name;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMPingResult
    {
        public int all_packet;			//所有发送包个数
        public int lost_packet;		    //丢失包个数
        public double max_delaytime;	//最大延时值
        public double min_delaytime;	//最小延时值
        public double avg_delaytime;	//平均延时值
        public double eval_value;		//结合丢包率与延时计算的最优值
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMVideoConfig
    {
	    public int capture_width;
	    public int capture_height;
	    public int encode_width;
	    public int encode_height;
	    public int capture_rotation;
	    public int capture_fps;
	    public int encode_fps;
	    public int encode_bitrate;	//150-1200
	    public int encode_qp;	    //26-51
	    public int encode_frame_interval;
        public LMVideoFrameResampling encode_resampling;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMIPCameraAddress 
    {
        public int port;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
	    public string address;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMIPCameraAuthInfo
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
	    public string name;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
	    public string psw;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMIPCameraChannel
    {
	    public IntPtr main_stream_capture_handler;	//底层使用
	    public IntPtr pay_stream_capture_handler;	//底层使用
	    public IntPtr main_stream_call_back;		//底层使用
	    public IntPtr pay_stream_call_back;			//底层使用
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
	    public string strRtspUri_main_stream;	    //主码流rtsp地址
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
	    public string strRtspUri_pay_stream;	    //付码流rtsp地址
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMIPCameraOptions
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
	    public string sn;
        public int width;	//采集分辨率
        public int height;
        public int fps;	    //采集帧率
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct LMIPCameraInfo
    {
	    public int identity;	//自定义唯一标识符 0-65535
	    public int channel_nums;
	    public int valid;	
	    public LMIPCameraAddress address;
	    public LMIPCameraAuthInfo auth_info;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public LMIPCameraChannel[] channels;
	    public LMIPCameraOptions options;
    };

}
