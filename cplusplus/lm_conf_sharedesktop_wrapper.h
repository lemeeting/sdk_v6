#ifndef __lm_conf_sharedesktop_wrapper_h__
#define __lm_conf_sharedesktop_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfShareDesktopWrapper {
	public:
		class Observer {
		public:
			virtual void OnDisconnectShareServer(int result) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int SelfStartShare();
		int SelfStopShare();

		int CreateView(const char* sharer, void* parent_wnd, int crBkg, void*& wnd);
		int DestroyView(const char* sharer);

		int SetDisplayMode(const char* sharer, LMShareDesktopDisplayModes mode);

		int GetScreenResolution(int& width, int& height);
		int GetRemoteResolution(int& width, int& height);
		int SetRemoteResolution(int width, int height);

	private:
		friend void CALLBACK FuncShareDesktopOnDisconnectShareServerCB(int result, void* clientdata);

		void OnDisconnectShareServerCB(int result);

	private:
		friend class ConfEngineWrapper;
		explicit ConfShareDesktopWrapper(native_object_t c_object);
		~ConfShareDesktopWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_sharedesktop_wrapper_h__