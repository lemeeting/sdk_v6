#include "lm_conf_centerserver_wrapper.h"
#include "lm_conf_cs_param_wrapper.h"
#include "lm_conf_cs_applymsg_wrapper.h"
#include "lm_conf_cs_confcode_wrapper.h"
#include "lm_conf_cs_confroom_wrapper.h"
#include "lm_conf_cs_orginfo_wrapper.h"
#include "lm_conf_cs_orguser_wrapper.h"
#include "lm_conf_cs_pushmsg_wrapper.h"
#include "lm_conf_cs_userbind_wrapper.h"
#include "lm_conf_cs_userinfo_wrapper.h"
#include "lm_conf_cs_realtime_confinfo_wrapper.h"

namespace lm {
	void CALLBACK FuncToCenterOnDisconnectCenterCB(int result, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnDisconnectCenterCB(result);
	}

	void CALLBACK FuncToCenterOnGetAuthCodeCB(int result, native_object_t param, const char* code, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetAuthCodeCB(result, param, code);
	}

	void CALLBACK FuncToCenterOnRegUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRegUserInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnLoginCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnLoginCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnLogoutCB(int result, native_object_t param, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnLogoutCB(result, param);
	}

	void CALLBACK FuncToCenterOnPrepareLoginConfCB(int result, native_object_t param,
		int id_conf, const char* json_address, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnPrepareLoginConfCB(result, param, id_conf, json_address);
	}

	void CALLBACK FuncToCenterOnGetRealConfCB(int result, native_object_t param, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetRealConfCB(result, param, infos, count);
	}

	void CALLBACK FuncToCenterOnGetUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetUserInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateUserInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveUserInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetUserInfoListCB(int result, native_object_t param, 
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetUserInfoListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetOrgInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateOrgInfoCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetOrgInfoListCB(int result, native_object_t param,
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetOrgInfoListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetUserBindCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddUserBindCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateUserBindCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveUserBindCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetUserBindListCB(int result, native_object_t param,
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetUserBindListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetOrgUserCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddOrgUserCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateOrgUserCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveOrgUserCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetOrgUserListCB(int result, native_object_t param, 
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetOrgUserListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetConfRoomCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddConfRoomCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateConfRoomCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveConfRoomCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetConfRoomListCB(int result, native_object_t param,
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetConfRoomListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetConfCodeCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddConfCodeCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateConfCodeCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveConfCodeCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetConfCodeListCB(int result, native_object_t param, 
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetConfCodeListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetApplyMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddApplyMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdateApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdateApplyMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemoveApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemoveApplyMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetApplyMsgListCB(int result, native_object_t param, 
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetApplyMsgListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnGetPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetPushMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnAddPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnAddPushMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnUpdatePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnUpdatePushMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnRemovePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnRemovePushMsgCB(result, param, info);
	}

	void CALLBACK FuncToCenterOnGetPushMsgListCB(int result, native_object_t param, 
		native_object_t info, native_object_t* infos, int count, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnGetPushMsgListCB(result, param, info, infos, count);
	}

	void CALLBACK FuncToCenterOnNoticePushMsgCB(native_object_t param, native_object_t info, void* clientdata) {
		ConfCenterServerWrapper* wrapper = reinterpret_cast<ConfCenterServerWrapper*>(clientdata);
		wrapper->OnNoticePushMsgCB(param, info);
	}

	void ConfCenterServerWrapper::OnDisconnectCenterCB(int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDisconnectCenter(result);
		}
	}

	void ConfCenterServerWrapper::OnGetAuthCodeCB(int result,
		native_object_t param, const char* code) {
		CSParamWrapper param_wrapper(param);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetAuthCode(result, &param_wrapper, code);
		}
	}

	void ConfCenterServerWrapper::OnRegUserInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRegUserInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnLoginCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnLogin(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnLogoutCB(int result,
		native_object_t param) {
		CSParamWrapper param_wrapper(param);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnLogout(result, &param_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnPrepareLoginConfCB(int result,
		native_object_t param, int id_conf, const char* json_address) {
		CSParamWrapper param_wrapper(param);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnPrepareLoginConf(result, &param_wrapper, id_conf, json_address);
		}
	}

	void ConfCenterServerWrapper::OnGetRealConfCB(int result,
		native_object_t param, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		const CSRealTimeConfWrapper** info_wrappers = new const CSRealTimeConfWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSRealTimeConfWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetRealConf(result, &param_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetUserInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetUserInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateUserInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateUserInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveUserInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveUserInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetUserInfoListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSUserInfoWrapper info_wrapper(info);
		const CSUserInfoWrapper** info_wrappers = new const CSUserInfoWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSUserInfoWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetUserInfoList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetOrgInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetOrgInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateOrgInfoCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgInfoWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateOrgInfo(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetOrgInfoListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSOrgInfoWrapper info_wrapper(info);
		const CSOrgInfoWrapper** info_wrappers = new const CSOrgInfoWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSOrgInfoWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetOrgInfoList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetUserBindCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserBindWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetUserBind(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddUserBindCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserBindWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddUserBind(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateUserBindCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserBindWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateUserBind(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveUserBindCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSUserBindWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveUserBind(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetUserBindListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSUserBindWrapper info_wrapper(info);
		const CSUserBindWrapper** info_wrappers = new const CSUserBindWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSUserBindWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetUserBindList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetOrgUserCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgUserWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetOrgUser(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddOrgUserCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgUserWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddOrgUser(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateOrgUserCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgUserWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateOrgUser(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveOrgUserCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSOrgUserWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveOrgUser(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetOrgUserListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSOrgUserWrapper info_wrapper(info);
		const CSOrgUserWrapper** info_wrappers = new const CSOrgUserWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSOrgUserWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetOrgUserList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetConfRoomCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfRoomWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConfRoom(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddConfRoomCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfRoomWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddConfRoom(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateConfRoomCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfRoomWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateConfRoom(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveConfRoomCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfRoomWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveConfRoom(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetConfRoomListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSConfRoomWrapper info_wrapper(info);
		const CSConfRoomWrapper** info_wrappers = new const CSConfRoomWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSConfRoomWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConfRoomList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetConfCodeCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfCodeWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConfCode(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddConfCodeCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfCodeWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddConfCode(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateConfCodeCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfCodeWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateConfCode(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveConfCodeCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSConfCodeWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveConfCode(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetConfCodeListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSConfCodeWrapper info_wrapper(info);
		const CSConfCodeWrapper** info_wrappers = new const CSConfCodeWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSConfCodeWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConfCodeList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetApplyMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSApplyMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetApplyMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddApplyMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSApplyMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddApplyMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdateApplyMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSApplyMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateApplyMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemoveApplyMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSApplyMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveApplyMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetApplyMsgListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSApplyMsgWrapper info_wrapper(info);
		const CSApplyMsgWrapper** info_wrappers = new const CSApplyMsgWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSApplyMsgWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetApplyMsgList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnGetPushMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetPushMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnAddPushMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddPushMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnUpdatePushMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdatePushMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnRemovePushMsgCB(int result,
		native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemovePushMsg(result, &param_wrapper, &info_wrapper);
		}
	}

	void ConfCenterServerWrapper::OnGetPushMsgListCB(int result,
		native_object_t param, native_object_t info, native_object_t* infos, int count) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		const CSPushMsgWrapper** info_wrappers = new const CSPushMsgWrapper*[count];
		for (int i = 0; i < count; i++)
			info_wrappers[i] = new CSPushMsgWrapper(infos[i]);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetPushMsgList(result, &param_wrapper, &info_wrapper, info_wrappers, count);
		}
		for (int i = 0; i < count; i++)
			delete info_wrappers[i];
		delete[]info_wrappers;
	}

	void ConfCenterServerWrapper::OnNoticePushMsgCB(native_object_t param, native_object_t info) {
		CSParamWrapper param_wrapper(param);
		CSPushMsgWrapper info_wrapper(info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnNoticePushMsg(&param_wrapper, &info_wrapper);
		}
	}

	ConfCenterServerWrapper::ConfCenterServerWrapper(native_object_t c_object)
		: c_object_(c_object) {
		ConfToCenterCallback cb;
		memset(&cb, 0, sizeof(cb));
		cb.client_data_ = this;
		cb.OnAddApplyMsg_ = FuncToCenterOnAddApplyMsgCB;
		cb.OnAddConfCode_ = FuncToCenterOnAddConfCodeCB;
		cb.OnAddConfRoom_ = FuncToCenterOnAddConfRoomCB;
		cb.OnAddOrgUser_ = FuncToCenterOnAddOrgUserCB;
		cb.OnAddPushMsg_ = FuncToCenterOnAddPushMsgCB;
		cb.OnAddUserBind_ = FuncToCenterOnAddUserBindCB;
		cb.OnDisconnectCenter_ = FuncToCenterOnDisconnectCenterCB;
		cb.OnGetApplyMsg_ = FuncToCenterOnGetApplyMsgCB;
		cb.OnGetApplyMsgList_ = FuncToCenterOnGetApplyMsgListCB;
		cb.OnGetAuthCode_ = FuncToCenterOnGetAuthCodeCB;
		cb.OnGetConfCode_ = FuncToCenterOnGetConfCodeCB;
		cb.OnGetConfCodeList_ = FuncToCenterOnGetConfCodeListCB;
		cb.OnGetConfRoom_ = FuncToCenterOnGetConfRoomCB;
		cb.OnGetConfRoomList_ = FuncToCenterOnGetConfRoomListCB;
		cb.OnGetOrgInfo_ = FuncToCenterOnGetOrgInfoCB;
		cb.OnGetOrgInfoList_ = FuncToCenterOnGetOrgInfoListCB;
		cb.OnGetOrgUser_ = FuncToCenterOnGetOrgUserCB;
		cb.OnGetOrgUserList_ = FuncToCenterOnGetOrgUserListCB;
		cb.OnGetPushMsg_ = FuncToCenterOnGetPushMsgCB;
		cb.OnGetPushMsgList_ = FuncToCenterOnGetPushMsgListCB;
		cb.OnGetRealConf_ = FuncToCenterOnGetRealConfCB;
		cb.OnGetUserBind_ = FuncToCenterOnGetUserBindCB;
		cb.OnGetUserBindList_ = FuncToCenterOnGetUserBindListCB;
		cb.OnGetUserInfo_ = FuncToCenterOnGetUserInfoCB;
		cb.OnGetUserInfoList_ = FuncToCenterOnGetUserInfoListCB;
		cb.OnLogin_ = FuncToCenterOnLoginCB;
		cb.OnLogout_ = FuncToCenterOnLogoutCB;
		cb.OnNoticePushMsg_ = FuncToCenterOnNoticePushMsgCB;
		cb.OnPrepareLoginConf_ = FuncToCenterOnPrepareLoginConfCB;
		cb.OnRegUserInfo_ = FuncToCenterOnRegUserInfoCB;
		cb.OnRemoveApplyMsg_ = FuncToCenterOnRemoveApplyMsgCB;
		cb.OnRemoveConfCode_ = FuncToCenterOnRemoveConfCodeCB;
		cb.OnRemoveConfRoom_ = FuncToCenterOnRemoveConfRoomCB;
		cb.OnRemoveOrgUser_ = FuncToCenterOnRemoveOrgUserCB;
		cb.OnRemovePushMsg_ = FuncToCenterOnRemovePushMsgCB;
		cb.OnRemoveUserBind_ = FuncToCenterOnRemoveUserBindCB;
		cb.OnRemoveUserInfo_ = FuncToCenterOnRemoveUserInfoCB;
		cb.OnUpdateApplyMsg_ = FuncToCenterOnUpdateApplyMsgCB;
		cb.OnUpdateConfCode_ = FuncToCenterOnUpdateConfCodeCB;
		cb.OnUpdateConfRoom_ = FuncToCenterOnUpdateConfRoomCB;
		cb.OnUpdateOrgInfo_ = FuncToCenterOnUpdateOrgInfoCB;
		cb.OnUpdateOrgUser_ = FuncToCenterOnUpdateOrgUserCB;
		cb.OnUpdatePushMsg_ = FuncToCenterOnUpdatePushMsgCB;
		cb.OnUpdateUserBind_ = FuncToCenterOnUpdateUserBindCB;
		cb.OnUpdateUserInfo_ = FuncToCenterOnUpdateUserInfoCB;

		lm_conf_centerserver_set_callback(c_object_, &cb);
	}

	ConfCenterServerWrapper::~ConfCenterServerWrapper() {
	}

	void ConfCenterServerWrapper::AddObserver(Observer* observer) {
		if (std::find(observer_list_.begin(), observer_list_.end(), observer) == observer_list_.end())
			observer_list_.push_back(observer);
	}

	void ConfCenterServerWrapper::RemoveObserver(Observer* observer) {
		ObserverList::iterator it = std::find(observer_list_.begin(), observer_list_.end(), observer);
		if (it != observer_list_.end())
			observer_list_.erase(it);
	}

	int ConfCenterServerWrapper::SetCenterAddress(const char* address) {
		return lm_conf_centerserver_SetCenterAddress(c_object_, address);
	}

	int ConfCenterServerWrapper::GetCenterAddress(char* address, int count) {
		return lm_conf_centerserver_GetCenterAddress(c_object_, address, count);
	}

	int ConfCenterServerWrapper::GetAuthCode(const CSParamWrapper* param) {
		return lm_conf_centerserver_GetAuthCode(c_object_, param->cobject());
	}

	int ConfCenterServerWrapper::RegUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info) {
		return lm_conf_centerserver_RegUserInfo(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::Login(const CSParamWrapper* param, int id_org, const char* account, const char* psw) {
		return lm_conf_centerserver_Login(c_object_, param->cobject(), id_org, account, psw);
	}

	int ConfCenterServerWrapper::Logout(const CSParamWrapper* param) {
		return lm_conf_centerserver_Logout(c_object_, param->cobject());
	}

	int ConfCenterServerWrapper::PrepareLoginConf(const CSParamWrapper* param, int id_conf) {
		return lm_conf_centerserver_PrepareLoginConf(c_object_, param->cobject(), id_conf);
	}

	int ConfCenterServerWrapper::GetRealConf(const CSParamWrapper* param, int* id_confs, int count) {
		return lm_conf_centerserver_GetRealConf(c_object_, param->cobject(), id_confs, count);
	}

	int ConfCenterServerWrapper::GetUserInfo(const CSParamWrapper* param, const char* account) {
		return lm_conf_centerserver_GetUserInfo(c_object_, param->cobject(), account);
	}

	int ConfCenterServerWrapper::UpdateUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info) {
		return lm_conf_centerserver_UpdateUserInfo(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info) {
		return lm_conf_centerserver_RemoveUserInfo(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetUserInfoList(const CSParamWrapper* param, const CSUserInfoWrapper* info) {
		return lm_conf_centerserver_GetUserInfoList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetOrgInfo(const CSParamWrapper* param, int id_org) {
		return lm_conf_centerserver_GetOrgInfo(c_object_, param->cobject(), id_org);
	}

	int ConfCenterServerWrapper::UpdateOrgInfo(const CSParamWrapper* param, const CSOrgInfoWrapper* info) {
		return lm_conf_centerserver_UpdateOrgInfo(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetOrgInfoList(const CSParamWrapper* param, const CSOrgInfoWrapper* info) {
		return lm_conf_centerserver_UpdateOrgInfo(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetUserBind(const CSParamWrapper* param, const char* name) {
		return lm_conf_centerserver_GetUserBind(c_object_, param->cobject(), name);
	}

	int ConfCenterServerWrapper::AddUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info) {
		return lm_conf_centerserver_AddUserBind(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdateUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info) {
		return lm_conf_centerserver_UpdateUserBind(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info) {
		return lm_conf_centerserver_RemoveUserBind(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetUserBindList(const CSParamWrapper* param, const CSUserBindWrapper* info) {
		return lm_conf_centerserver_GetUserBindList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetOrgUser(const CSParamWrapper* param, int id_org, const char* account) {
		return lm_conf_centerserver_GetOrgUser(c_object_, param->cobject(), id_org, account);
	}

	int ConfCenterServerWrapper::AddOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info) {
		return lm_conf_centerserver_AddOrgUser(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdateOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info) {
		return lm_conf_centerserver_UpdateOrgUser(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info) {
		return lm_conf_centerserver_RemoveOrgUser(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetOrgUserList(const CSParamWrapper* param, const CSOrgUserWrapper* info) {
		return lm_conf_centerserver_GetOrgUserList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetConfRoom(const CSParamWrapper* param, int id_conf) {
		return lm_conf_centerserver_GetConfRoom(c_object_, param->cobject(), id_conf);
	}

	int ConfCenterServerWrapper::AddConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info) {
		return lm_conf_centerserver_AddConfRoom(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdateConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info) {
		return lm_conf_centerserver_UpdateConfRoom(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info) {
		return lm_conf_centerserver_RemoveConfRoom(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetConfRoomList(const CSParamWrapper* param, const CSConfRoomWrapper* info) {
		return lm_conf_centerserver_GetConfRoomList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetConfCode(const CSParamWrapper* param, const char* strCodeid) {
		return lm_conf_centerserver_GetConfCode(c_object_, param->cobject(), strCodeid);
	}

	int ConfCenterServerWrapper::AddConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info) {
		return lm_conf_centerserver_AddConfCode(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdateConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info) {
		return lm_conf_centerserver_UpdateConfCode(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info) {
		return lm_conf_centerserver_RemoveConfCode(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetConfCodeList(const CSParamWrapper* param, const CSConfCodeWrapper* info) {
		return lm_conf_centerserver_GetConfCodeList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetApplyMsg(const CSParamWrapper* param, int id_req) {
		return lm_conf_centerserver_GetApplyMsg(c_object_, param->cobject(), id_req);
	}

	int ConfCenterServerWrapper::AddApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info) {
		return lm_conf_centerserver_AddApplyMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdateApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info) {
		return lm_conf_centerserver_UpdateApplyMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemoveApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info) {
		return lm_conf_centerserver_RemoveApplyMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetApplyMsgList(const CSParamWrapper* param, const CSApplyMsgWrapper* info) {
		return lm_conf_centerserver_GetApplyMsgList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetPushMsg(const CSParamWrapper* param, int msg_id) {
		return lm_conf_centerserver_GetPushMsg(c_object_, param->cobject(), msg_id);
	}

	int ConfCenterServerWrapper::AddPushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info) {
		return lm_conf_centerserver_AddPushMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::UpdatePushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info) {
		return lm_conf_centerserver_UpdatePushMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::RemovePushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info) {
		return lm_conf_centerserver_RemovePushMsg(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetPushMsgList(const CSParamWrapper* param, const CSPushMsgWrapper* info) {
		return lm_conf_centerserver_GetPushMsgList(c_object_, param->cobject(), info->cobject());
	}

	int ConfCenterServerWrapper::GetConfCode(const CSParamWrapper* param, int id_conf) {
		return lm_conf_centerserver_GetConfCode_with_id(c_object_, param->cobject(), id_conf);
	}

}