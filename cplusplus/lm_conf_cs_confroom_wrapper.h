#ifndef __lm_conf_cs_confroom_wrapper_h__
#define __lm_conf_cs_confroom_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSConfRoomWrapper {
	public:
		CSConfRoomWrapper();
		explicit CSConfRoomWrapper(native_object_t c_object);
		~CSConfRoomWrapper();

		CSConfRoomWrapper& operator = (const CSConfRoomWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

		int GetVersionid() const;
		int GetDeleteflag() const;
		int GetConfid() const;
		int GetConftype() const;
		int GetWorldid() const;
		int GetGroupid() const;
		int GetServerid() const;

		const char* GetConfname() const;
		const char* GetConfpassword() const;
		const char* GetManagepassword() const;

		int64_t GetStarttime() const;
		int64_t GetEndtime() const;

		int GetMaxusercount() const;
		int GetMaxspeakercount() const;

		const char* GetCreator() const;
		const char* GetMender() const;

		int64_t GetCreatetime() const;
		int64_t GetModifytime() const;

		int GetConfflag() const;
		const char* GetSettingjson() const;
		const char* GetExtendjson() const;


		void SetConfid(int value);
		void SetConftype(int value);
		void SetWorldid(int value);
		void SetGroupid(int value);
		void SetServerid(int value);

		void SetConfname(const char* value);
		void SetConfpassword(const char* value);
		void SetManagepassword(const char* value);

		void SetStarttime(int64_t value);
		void SetEndtime(int64_t value);

		void SetMaxusercount(int value);
		void SetMaxspeakercount(int value);
		void SetConfflag(int value);
		void SetExtendjson(const char* value);

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_confroom_wrapper_h__