#ifndef __lm_conf_record_wrapper_h__
#define __lm_conf_record_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfRecordWrapper {
	public:
		class Observer {
		public:
			virtual void OnStartLocalRecordReady() {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int StartLocalRecord(const char* file_name, const char** recordaccounts, int count,
			int flags = kRecordVoice | kRecordVideo | kRecordWbd | kRecordAppShare | kRecordText);

		int StopLocalRecord();

		int StartScreenRecord(const char* file_name, LMScreenRecordFormats format, LMScreenRecordZoom zoom,
			float frame_rate, LMScreenRecordQP qp, int flags = kScreenRecordInputVoice | kScreenRecordOutputVoice);

		int StopScreenRecord();

		int StopRecord();

		LMConfRecordType CurrentRecordType();

	private:
		friend void CALLBACK FuncRecordOnStartLocalRecordReadyCB(void* clientdata);
		void OnStartLocalRecordReadyCB();

	private:
		friend class ConfEngineWrapper;
		explicit ConfRecordWrapper(native_object_t c_object);
		~ConfRecordWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_record_wrapper_h__