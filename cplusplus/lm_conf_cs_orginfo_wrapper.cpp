#include "lm_conf_cs_orginfo_wrapper.h"

namespace lm {
	CSOrgInfoWrapper::CSOrgInfoWrapper() : owner_(true) {
		lm_create_cs_org_instance(&c_object_);
	}

	CSOrgInfoWrapper::CSOrgInfoWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSOrgInfoWrapper::~CSOrgInfoWrapper() {
		if (owner_)
			lm_destroy_cs_org_instance(c_object_);
	}

	CSOrgInfoWrapper& CSOrgInfoWrapper::operator = (const CSOrgInfoWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_org_instance(&c_object_);
		}
		lm_copy_cs_org_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSOrgInfoWrapper::GetVersionid() const
	{
		return lm_get_cs_org_version_id(c_object_);
	}

	int CSOrgInfoWrapper::GetDeleteflag() const
	{
		return lm_get_cs_org_delete_flag(c_object_);
	}

	int CSOrgInfoWrapper::GetOrgid() const
	{
		return lm_get_cs_org_world_id(c_object_);
	}

	int CSOrgInfoWrapper::GetOrgtype() const
	{
		return lm_get_cs_org_type(c_object_);
	}

	int CSOrgInfoWrapper::GetOrgstatus() const
	{
		return lm_get_cs_org_status(c_object_);
	}

	int CSOrgInfoWrapper::GetGroupid() const
	{
		return lm_get_cs_org_group_id(c_object_);
	}

	int CSOrgInfoWrapper::GetMaxusercount() const
	{
		return lm_get_cs_org_max_user_nums(c_object_);
	}

	const char* CSOrgInfoWrapper::GetOrgname() const
	{
		return lm_get_cs_org_name(c_object_);
	}

	const char* CSOrgInfoWrapper::GetShowname() const
	{
		return lm_get_cs_org_show_name(c_object_);
	}

	const char* CSOrgInfoWrapper::GetAddress() const
	{
		return lm_get_cs_org_address(c_object_);
	}

	const char* CSOrgInfoWrapper::GetContacter() const
	{
		return lm_get_cs_org_contacter(c_object_);
	}

	const char* CSOrgInfoWrapper::GetPhone() const
	{
		return lm_get_cs_org_phone(c_object_);
	}

	const char* CSOrgInfoWrapper::GetEmail() const
	{
		return lm_get_cs_org_email(c_object_);
	}

	const char* CSOrgInfoWrapper::GetWebsite() const
	{
		return lm_get_cs_org_website(c_object_);
	}

	int64_t CSOrgInfoWrapper::GetBirthday() const
	{
		return lm_get_cs_org_birthday(c_object_);
	}

	int64_t CSOrgInfoWrapper::GetCreatetime() const
	{
		return lm_get_cs_org_create_time(c_object_);
	}

	int64_t CSOrgInfoWrapper::GetModifytime() const
	{
		return lm_get_cs_org_modify_time(c_object_);
	}

	const char* CSOrgInfoWrapper::GetCreator() const
	{
		return lm_get_cs_org_creator(c_object_);
	}

	const char* CSOrgInfoWrapper::GetMender() const
	{
		return lm_get_cs_org_mender(c_object_);
	}

	const char* CSOrgInfoWrapper::GetSettingjson() const
	{
		return lm_get_cs_org_setting_json(c_object_);
	}

	const char* CSOrgInfoWrapper::GetExtendjson() const
	{
		return lm_get_cs_org_extend_json(c_object_);
	}

	int CSOrgInfoWrapper::GetOrgflag() const
	{
		return lm_get_cs_org_flag(c_object_);
	}

	void CSOrgInfoWrapper::SetOrgid(int value)
	{
		lm_set_cs_org_world_id(c_object_, value);
	}

	void CSOrgInfoWrapper::SetOrgtype(int value)
	{
		lm_set_cs_org_type(c_object_, value);
	}

	void CSOrgInfoWrapper::SetOrgstatus(int value)
	{
		lm_set_cs_org_status(c_object_, value);
	}

	void CSOrgInfoWrapper::SetGroupid(int value)
	{
		lm_set_cs_org_group_id(c_object_, value);
	}

	void CSOrgInfoWrapper::SetMaxusercount(int value)
	{
		lm_set_cs_org_max_user_nums(c_object_, value);
	}

	void CSOrgInfoWrapper::SetOrgname(const char* value)
	{
		lm_set_cs_org_name(c_object_, value);
	}

	void CSOrgInfoWrapper::SetShowname(const char* value)
	{
		lm_set_cs_org_show_name(c_object_, value);
	}

	void CSOrgInfoWrapper::SetAddress(const char* value)
	{
		lm_set_cs_org_address(c_object_, value);
	}

	void CSOrgInfoWrapper::SetContacter(const char* value)
	{
		lm_set_cs_org_contacter(c_object_, value);
	}

	void CSOrgInfoWrapper::SetPhone(const char* value)
	{
		lm_set_cs_org_phone(c_object_, value);
	}

	void CSOrgInfoWrapper::SetEmail(const char* value)
	{
		lm_set_cs_org_email(c_object_, value);
	}

	void CSOrgInfoWrapper::SetWebsite(const char* value)
	{
		lm_set_cs_org_website(c_object_, value);
	}

	void CSOrgInfoWrapper::SetBirthday(int64_t value)
	{
		lm_set_cs_org_birthday(c_object_, value);
	}

	void CSOrgInfoWrapper::SetOrgflag(int value)
	{
		lm_set_cs_org_flag(c_object_, value);
	}

	void CSOrgInfoWrapper::SetExtendjson(const char* value)
	{
		lm_set_cs_org_extend_json(c_object_, value);
	}
}