#include "lm_conf_cs_userinfo_wrapper.h"

namespace lm {
	CSUserInfoWrapper::CSUserInfoWrapper() : owner_(true) {
		lm_create_cs_user_instance(&c_object_);
	}

	CSUserInfoWrapper::CSUserInfoWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSUserInfoWrapper::~CSUserInfoWrapper() {
		if (owner_)
			lm_destroy_cs_user_instance(c_object_);
	}

	CSUserInfoWrapper& CSUserInfoWrapper::operator = (const CSUserInfoWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_user_instance(&c_object_);
		}
		lm_copy_cs_user_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSUserInfoWrapper::GetVersionid() const
	{
		return lm_get_cs_user_version_id(c_object_);
	}

	int CSUserInfoWrapper::GetDeleteflag() const
	{
		return lm_get_cs_user_delete_flag(c_object_);
	}

	int CSUserInfoWrapper::GetOrgid() const
	{
		return lm_get_cs_user_world_id(c_object_);
	}

	const char* CSUserInfoWrapper::GetUseraccount() const
	{
		return lm_get_cs_user_account(c_object_);
	}

	int CSUserInfoWrapper::GetUsertype() const
	{
		return lm_get_cs_user_type(c_object_);
	}

	int CSUserInfoWrapper::GetUserstatus() const
	{
		return lm_get_cs_user_status(c_object_);
	}

	const char* CSUserInfoWrapper::GetUsername() const
	{
		return lm_get_cs_user_name(c_object_);
	}

	const char* CSUserInfoWrapper::GetPassword() const
	{
		return lm_get_cs_user_password(c_object_);
	}

	const char* CSUserInfoWrapper::GetPhone() const
	{
		return lm_get_cs_user_phone(c_object_);
	}

	const char* CSUserInfoWrapper::GetEmail() const
	{
		return lm_get_cs_user_email(c_object_);
	}

	int64_t CSUserInfoWrapper::GetBirthday() const
	{
		return lm_get_cs_user_birthday(c_object_);
	}

	int64_t CSUserInfoWrapper::GetCreatetime() const
	{
		return lm_get_cs_user_create_time(c_object_);
	}

	const char* CSUserInfoWrapper::GetSettingjson() const
	{
		return lm_get_cs_user_setting_json(c_object_);
	}

	const char* CSUserInfoWrapper::GetExtendjson() const
	{
		return lm_get_cs_user_extend_json(c_object_);
	}

	void CSUserInfoWrapper::SetOrgid(int value)
	{
		lm_set_cs_org_user_org_id(c_object_, value);
	}

	void CSUserInfoWrapper::SetUseraccount(const char* value)
	{
		lm_set_cs_org_user_account(c_object_, value);
	}

	void CSUserInfoWrapper::SetUsertype(int value)
	{
		lm_set_cs_org_user_type(c_object_, value);
	}

	void CSUserInfoWrapper::SetUserstatus(int value)
	{
		lm_set_cs_org_user_status(c_object_, value);
	}

	void CSUserInfoWrapper::SetUsername(const char* value)
	{
		lm_set_cs_org_user_name(c_object_, value);
	}

	void CSUserInfoWrapper::SetPassword(const char* value)
	{
		lm_set_cs_org_user_password(c_object_, value);
	}

	void CSUserInfoWrapper::SetPhone(const char* value)
	{
		lm_set_cs_org_user_phone(c_object_, value);
	}

	void CSUserInfoWrapper::SetEmail(const char* value)
	{
		lm_set_cs_org_user_email(c_object_, value);
	}

	void CSUserInfoWrapper::SetBirthday(int64_t value)
	{
		lm_set_cs_org_user_birthday(c_object_, value);
	}

	void CSUserInfoWrapper::SetCreatetime(int64_t value)
	{
		lm_set_cs_org_user_create_time(c_object_, value);
	}

	void CSUserInfoWrapper::SetSettingjson(const char* value)
	{
		lm_set_cs_org_user_setting_json(c_object_, value);
	}

	void CSUserInfoWrapper::SetExtendjson(const char* value)
	{
		lm_set_cs_org_user_extend_json(c_object_, value);
	}

}