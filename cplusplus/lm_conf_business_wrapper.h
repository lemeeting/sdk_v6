#ifndef __lm_conf_business_wrapper_h__
#define __lm_conf_business_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ASConfAttributeWrapper;
	class ASConfRealTimeInfoWrapper;
	class ASConfAttendeeWrapper;
	class ASConfSyncInfoWrapper;
	class ConfBusinessWrapper {
	public:
		class Observer {
		public:
			virtual void OnDisconnect(
				int result) {}

			virtual void OnEntryConference(
				int result) {}

			virtual void OnLeaveConference(
				int result) {}

			virtual void OnGetConference(
				const lm::ASConfAttributeWrapper* conference_info) {}

			void OnUpdateConerence(
				const lm::ASConfAttributeWrapper* conference_info) {}

			virtual void OnRemoveConerence(
				int id_conference) {}

			virtual void OnGetConferenceRealTimeInfo(
				const lm::ASConfRealTimeInfoWrapper* real_time_conference_info) {}

			virtual void OnGetConferenceSyncInfo(
				const lm::ASConfSyncInfoWrapper* conference_sync_info) {}

			virtual void OnAddSelfAttendee(
				const lm::ASConfAttendeeWrapper* slef_attendee_info) {}

			virtual void OnAttendeeOnline(
				const lm::ASConfAttendeeWrapper* attendee_info) {}

			virtual void OnAttendeeOffline(
				const char* account) {}

			virtual void OnUpdateAttendee(
				const lm::ASConfAttendeeWrapper* old_attendee_info,
				const lm::ASConfAttendeeWrapper* new_attendee_info) {}

			virtual void OnRemoveAttendee(
				const char* account) {}

			virtual void OnAddConfAdmin(
				const char* account) {}

			virtual void OnRemoveConfAdmin(
				const char* account) {}

			virtual void OnAddConfDefaultAttendee(
				const char* account) {}

			virtual void OnRemoveConfDefaultAttendee(
				const char* account) {}

			virtual void OnApplySpeakOper(
				int result, const char* account, int new_state, int old_state, bool apply) {}

			virtual void OnAccreditSpeakOper(
				int result, const char* account, int new_state, int old_state, bool accredit) {}

			virtual void OnSpeakNotify(
				const char* account, int new_state, int old_state, bool is_speak) {}

			virtual void OnApplyDataOper(
				int result, const char* account, int new_state, int old_state, bool apply) {}

			virtual void OnAccreditDataOper(
				int result, const char* account, int new_state, int old_state, bool accredit) {}

			virtual void OnDataOpNotify(
				const char* account, int new_state, int old_state, bool is_data_op) {}

			virtual void OnApplyDataSync(
				int result, const char* account, int new_state, int old_state, bool apply) {}

			virtual void OnAccreditDataSync(
				int result, const char* account, int new_state, int old_state, bool accredit) {}

			virtual void OnSyncOpNotify(
				const char* account, int new_state, int old_state, bool is_sync) {}

			virtual void OnDataSyncCommand(
				int result) {}

			virtual void OnDataSyncCommandNotify(
				const char* syncer, const char* sync_data) {}

			virtual void OnApplyTempAdmin(
				int result, const char* account,
				int new_state, int old_state, bool apply) {}

			virtual void OnAccreditTempAdmin(
				int result, const char* account,
				int new_state, int old_state, bool accredit) {}

			virtual void OnAuthTempAdmin(
				int result, const char* admin_psw) {}

			virtual void OnTempAdminNotify(const char* account,
				uint32_t new_state, uint32_t old_state, bool apply) {}

			virtual void OnStartPreviewVideo(
				int result, const char* shower,
				DeviceIdentity id_device, uint64_t context) {}

			virtual void OnStopPreviewVideo(
				int result, const char* shower,
				DeviceIdentity id_device, uint64_t context) {}

			virtual void OnStartPreviewVideoNotify(
				const char* shower, DeviceIdentity id_device) {}

			virtual void OnStopPreviewVideoNotify(
				const char* shower, DeviceIdentity id_device) {}

			virtual void OnSwitchMainVideo(
				int result, DeviceIdentity old_dev_id, DeviceIdentity new_dev_id) {}

			virtual void OnSwitchMainVideoNotify(
				const char* account, DeviceIdentity old_dev_id, DeviceIdentity new_dev_id) {}

			virtual void OnEnableVideo(
				int result, bool enabled) {}

			virtual void OnEnableVideoNotify(
				const char* account, bool enabled) {}

			virtual void OnCaptureVideoState(
				int result, DeviceIdentity id_device, bool enabled) {}

			virtual void OnCaptureVideoStateNotify(
				const char* account, DeviceIdentity id_device, bool enabled) {}

			virtual void OnVideoShowNameChangedNotify(
				const char* account, DeviceIdentity id_device, const char* new_show_name) {}

			virtual void OnVideoDeviceChangedNotify(
				const char* account, int dev_nums, int dev_state, DeviceIdentity dev_main_id) {}

			virtual void OnKickoutAttendee(
				int result, const char* account) {}

			virtual void OnKickoutAttendeeNotify(
				const char* oper, const char* account) {}

			virtual void OnUpdateAttendeeName(
				int result, const char* account) {}

			virtual void OnUpdateAttendeeNameNotify(
				const char* account, const char* new_name) {}

			virtual void OnRelayMsgToOne(
				int result, const char* receiver) {}

			virtual void OnRelayMsgToOneNotify(
				const char* sneder, const char* receiver, const char* msg) {}

			virtual void OnRelayMsgToAll(
				int result) {}

			virtual void OnRelayMsgToAllNotify(
				const char* sneder, const char* msg) {}

			virtual void OnAdminOperConfSetting(
				int result, int cmd, int cmd_value) {}

			virtual void OnAdminOperConfSettingNotify(
				const char* oper, int cmd, int cmd_value) {}

			virtual void OnSetConfPassword(int result) {}

			virtual void OnSetConfPasswordNotify(
				const char* oper) {}

			virtual void OnStartDesktopShare(
				int result) {}

			virtual void OnStartDesktopShareNotify(
				const char* sharer) {}

			virtual void OnStopDesktopShare(
				int result) {}

			virtual void OnStopDesktopShareNotify(
				const char* sharer) {}

			virtual void OnStartPreviewDesktop(
				int result, const char* sharer, uint64_t context) {}

			virtual void OnStopPreviewDesktop(
				int result, const char* sharer, uint64_t context) {}

			virtual void OnStartPreviewDesktopNotify(
				const char* sharer) {}

			virtual void OnStopPreviewDesktopNotify(
				const char* sharer) {}

			virtual void OnAskRemoteDesktopControl(
				int result, const char* sharer) {}

			virtual void OnAskRemoteDesktopControlNotify(
				const char* controller, const char* sharer) {}

			virtual void OnAbstainRemoteDesktopControl(
				int result, const char* sharer) {}

			virtual void OnAbstainRemoteDesktopControlNotify(
				const char* controller, const char* sharer) {}

			virtual void OnAnswerRemoteDesktopControl(
				int result, const char* controller, bool allow) {}

			virtual void OnAnswerRemoteDesktopControlNotify(
				const char* controller, const char* sharer, bool allow) {}

			virtual void OnAbortRemoteDesktopControl(
				int result, const char* controller) {}

			virtual void OnAbortRemoteDesktopControlNotify(
				const char* controller, const char* sharer) {}

			virtual void OnLaunchSignin(
				int result, LMConferenceSigninType type, int64_t launch_or_stop_time) {}

			virtual void OnLaunchSigninNotify(
				const char* oper, LMConferenceSigninType type, int64_t launch_or_stop_time) {}

			virtual void OnSignin(
				int result, bool is_signin, int64_t launch_time,
				int64_t signin_or_cancel_time) {}

			virtual void OnSigninNotify(
				const char* account, bool is_signin, int64_t launch_time,
				int64_t signin_or_cancel_time) {}

			virtual void OnOperSubGroup(
				int result) {}

			virtual void OnOperSubGroupNotify(
				const char* oper, int id_sub_group) {}

			virtual void OnCancelSubGroup(
				int result) {}

			virtual void OnCancelSubGroupNotify(
				const char* oper) {}

			virtual void OnOpRemoteDesktopShared(
				int result, const char* sharer, bool is_start) {}

			virtual void OnOpRemoteDesktopSharedNotify(
				const char* oper, const char* sharer, bool is_start) {}

			virtual void OnStartPlayback(
				int result, const char* file_name) {}

			virtual void OnStartPlaybackNotify(
				const char* account) {}

			virtual void OnStopPlayback(
				int result) {}

			virtual void OnStopPlaybackNotify(
				const char* account) {}

			virtual void OnStartMediaPlay(
				int result, const char* file_name, int media_flag) {}

			virtual void OnStartMediaPlayNotify(
				const char* oper, const char* file_name, int media_flag) {}

			virtual void OnStopMediaPlay(
				int result) {}

			virtual void OnStopMediaPlayNotify(
				const char* oper) {}

			virtual void OnPauseMediaPlay(
				int result, bool paused) {}

			virtual void OnPauseMediaPlayNotify(
				const char* oper, bool paused) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int EntryConference(const char* json_address,
			int id_conference, uint32_t conf_tag, const char* conf_psw, bool is_md5,
			const char* account, int id_org, const char* name);

		int LeaveConference();

		int ApplySpeak(bool apply);

		int AccreditSpeak(const char* account, bool accredit);

		int ApplyDataOper(bool apply);

		int AccreditDataOper(const char* account, bool accredit);

		int ApplyDataSync(bool apply);

		int AccreditDataSync(const char* account, bool accredit);

		int DataSyncCommand(const char* command);

		int ApplyTempAdmin(bool apply);

		int AccreditTempAdmin(const char* account, bool accredit);

		int AuthTempAdmin(const char* admin_psw);

		int StartPreviewVideo(const char* shower, DeviceIdentity id_device, uint64_t context);

		int StopPreviewVideo(const char* shower, DeviceIdentity id_device, uint64_t context);

		int SwitchMainVideo(DeviceIdentity id_device);

		int EnableVideo(bool enabled);

		int CaptureVideoState(DeviceIdentity id_device, bool enabled);

		int UpdateVideoShowName(DeviceIdentity id_device, const char* name);

		int KickOutAttendee(const char* account);

		int UpdataAttendeeName(const char* name);

		int RelayMsgToOne(const char* receiver, const char* msg);

		int RelayMsgToAll(const char* msg);

		int AdminOperConfSetting(int cmd, int cmd_value);

		int SetConfPassword(const char* psw);

		int StartDesktopShare();
		int StopDesktopShare();

		int StartPreviewDesktop(const char* sharer, uint64_t context);
		int StopPreviewDesktop(const char* sharer, uint64_t context);

		int AskRemoteDesktopControl(const char* sharer);
		int AbstainRemoteDesktopControl(const char* sharer);

		int AnswerRemoteDesktopControl(const char* controller, bool allow);
		int AbortRemoteDesktopControl(const char* controller);

		int LaunchSignin(LMConferenceSigninType signin_type);
		int Signin(bool is_signin);

		int OperSubGroup(const char** accounts, int count, int id_subgroup);
		int CancelSubGroup();

		int OpRemoteDesktopShared(const char* sharer, bool is_start);

		int StartPlayback(const char* file_name);
		int StopPlayback();

		int StartMediaPlay(const char* file_name, int media_flag);
		int StopMediaPlay();
		int PauseMediaPlay(bool paused);

	private:
		friend void CALLBACK FuncBusinessOnDisconnectCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnEntryConferenceCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnLeaveConferenceCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnGetConferenceCB(
			native_object_t conference_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnUpdateConerenceCB(
			native_object_t conference_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnRemoveConerenceCB(
			int id_conference, void* clientdata);
		friend void CALLBACK FuncBusinessOnGetConferenceRealTimeInfoCB(
			native_object_t real_time_conference_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnGetConferenceSyncInfoCB(
			native_object_t conference_sync_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnAddSelfAttendeeCB(
			native_object_t slef_attendee_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnAttendeeOnlineCB(
			native_object_t attendee_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnAttendeeOfflineCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnUpdateAttendeeCB(
			native_object_t old_attendee_info, native_object_t new_attendee_info, void* clientdata);
		friend void CALLBACK FuncBusinessOnRemoveAttendeeCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnAddConfAdminCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnRemoveConfAdminCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnAddConfDefaultAttendeeCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnRemoveConfDefaultAttendeeCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnApplySpeakOperCB(
			int result, const char* account, int new_state, int old_state, int apply, void* clientdata);
		friend void CALLBACK FuncBusinessOnAccreditSpeakOperCB(
			int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);
		friend void CALLBACK FuncBusinessOnSpeakNotifyCB(
			const char* account, int new_state, int old_state, int is_speak, void* clientdata);
		friend void CALLBACK FuncBusinessOnApplyDataOperCB(
			int result, const char* account, int new_state, int old_state, int apply, void* clientdata);
		friend void CALLBACK FuncBusinessOnAccreditDataOperCB(
			int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);
		friend void CALLBACK FuncBusinessOnDataOpNotifyCB(
			const char* account, int new_state, int old_state, int is_data_op, void* clientdata);
		friend void CALLBACK FuncBusinessOnApplyDataSyncCB(
			int result, const char* account, int new_state, int old_state, int apply, void* clientdata);
		friend void CALLBACK FuncBusinessOnAccreditDataSyncCB(
			int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);
		friend void CALLBACK FuncBusinessOnSyncOpNotifyCB(
			const char* account, int new_state, int old_state, int is_sync, void* clientdata);
		friend void CALLBACK FuncBusinessOnDataSyncCommandCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnDataSyncCommandNotifyCB(
			const char* syncer, const char* sync_data, void* clientdata);
		friend void CALLBACK FuncBusinessOnApplyTempAdminCB(
			int result, const char* account,
			int new_state, int old_state, int apply, void* clientdata);
		friend void CALLBACK FuncBusinessOnAccreditTempAdminCB(
			int result, const char* account,
			int new_state, int old_state, int accredit, void* clientdata);
		friend void CALLBACK FuncBusinessOnAuthTempAdminCB(
			int result, const char* admin_psw, void* clientdata);
		friend void CALLBACK FuncBusinessOnTempAdminNotifyCB(const char* account,
			uint32_t new_state, uint32_t old_state, int apply, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPreviewVideoCB(
			int result, const char* shower,
			int id_device, uint64_t context, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPreviewVideoCB(
			int result, const char* shower,
			int id_device, uint64_t context, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPreviewVideoNotifyCB(
			const char* shower, int id_device, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPreviewVideoNotifyCB(
			const char* shower, int id_device, void* clientdata);
		friend void CALLBACK FuncBusinessOnSwitchMainVideoCB(
			int result, int old_dev_id, int new_dev_id, void* clientdata);
		friend void CALLBACK FuncBusinessOnSwitchMainVideoNotifyCB(
			const char* account, int old_dev_id, int new_dev_id, void* clientdata);
		friend void CALLBACK FuncBusinessOnEnableVideoCB(
			int result, int enabled, void* clientdata);
		friend void CALLBACK FuncBusinessOnEnableVideoNotifyCB(
			const char* account, int enabled, void* clientdata);
		friend void CALLBACK FuncBusinessOnCaptureVideoStateCB(
			int result, int, int, void* clientdata);
		friend void CALLBACK FuncBusinessOnCaptureVideoStateNotifyCB(
			const char* account, int id_device, int enabled, void* clientdata);
		friend void CALLBACK FuncBusinessOnVideoShowNameChangedNotifyCB(
			const char* account, int i_device, const char* new_show_name, void* clientdata);
		friend void CALLBACK FuncBusinessOnVideoDeviceChangedNotifyCB(
			const char* account, int dev_nums, int dev_state, int dev_main_id, void* clientdata);
		friend void CALLBACK FuncBusinessOnKickoutAttendeeCB(
			int result, const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnKickoutAttendeeNotifyCB(
			const char* oper, const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnUpdateAttendeeNameCB(
			int result, const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnUpdateAttendeeNameNotifyCB(
			const char* account, const char* new_name, void* clientdata);
		friend void CALLBACK FuncBusinessOnRelayMsgToOneCB(
			int result, const char* receiver, void* clientdata);
		friend void CALLBACK FuncBusinessOnRelayMsgToOneNotifyCB(
			const char* sneder, const char* receiver, const char* msg, void* clientdata);
		friend void CALLBACK FuncBusinessOnRelayMsgToAllCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnRelayMsgToAllNotifyCB(
			const char* sneder, const char* msg, void* clientdata);
		friend void CALLBACK FuncBusinessOnAdminOperConfSettingCB(
			int result, int cmd, int cmd_value, void* clientdata);
		friend void CALLBACK FuncBusinessOnAdminOperConfSettingNotifyCB(
			const char* oper, int cmd, int cmd_value, void* clientdata);
		friend void CALLBACK FuncBusinessOnSetConfPasswordCB(int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnSetConfPasswordNotifyCB(
			const char* oper, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartDesktopShareCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartDesktopShareNotifyCB(
			const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopDesktopShareCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopDesktopShareNotifyCB(
			const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPreviewDesktopCB(
			int result, const char* sharer, uint64_t context, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPreviewDesktopCB(
			int result, const char* sharer, uint64_t, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPreviewDesktopNotifyCB(
			const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPreviewDesktopNotifyCB(
			const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnAskRemoteDesktopControlCB(
			int result, const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnAskRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlCB(
			int result, const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlCB(
			int result, const char* controller, int allow, void* clientdata);
		friend void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer, int allow, void* clientdata);
		friend void CALLBACK FuncBusinessOnAbortRemoteDesktopControlCB(
			int result, const char* controller, void* clientdata);
		friend void CALLBACK FuncBusinessOnAbortRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer, void* clientdata);
		friend void CALLBACK FuncBusinessOnLaunchSigninCB(
			int result, int type, int64_t launch_or_stop_time, void* clientdata);
		friend void CALLBACK FuncBusinessOnLaunchSigninNotifyCB(
			const char*, int type, int64_t launch_or_stop_time, void* clientdata);
		friend void CALLBACK FuncBusinessOnSigninCB(
			int result, int is_signin, int64_t launch_time,
			int64_t signin_or_cancel_time, void* clientdata);
		friend void CALLBACK FuncBusinessOnSigninNotifyCB(
			const char* account, int is_signin, int64_t launch_time,
			int64_t signin_or_cancel_time, void* clientdata);
		friend void CALLBACK FuncBusinessOnOperSubGroupCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnOperSubGroupNotifyCB(
			const char* oper, int id_sub_group, void* clientdata);
		friend void CALLBACK FuncBusinessOnCancelSubGroupCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnCancelSubGroupNotifyCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnOpRemoteDesktopSharedCB(
			int result, const char* sharer, int is_start, void* clientdata);
		friend void CALLBACK FuncBusinessOnOpRemoteDesktopSharedNotifyCB(
			const char* oper, const char* sharer, int is_start, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPlaybackCB(
			int result, const char* file_name, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartPlaybackNotifyCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPlaybackCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopPlaybackNotifyCB(
			const char* account, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartMediaPlayCB(
			int result, const char* file_name, int media_flag, void* clientdata);
		friend void CALLBACK FuncBusinessOnStartMediaPlayNotifyCB(
			const char* oper, const char* file_name, int media_flag, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopMediaPlayCB(
			int result, void* clientdata);
		friend void CALLBACK FuncBusinessOnStopMediaPlayNotifyCB(
			const char* oper, void* clientdata);
		friend void CALLBACK FuncBusinessOnPauseMediaPlayCB(
			int result, int paused, void* clientdata);
		friend void CALLBACK FuncBusinessOnPauseMediaPlayNotifyCB(
			const char* oper, int paused, void* clientdata);

		void OnDisconnectCB(
			int result);

		void OnEntryConferenceCB(
			int result);

		void OnLeaveConferenceCB(
			int result);

		void OnGetConferenceCB(
			native_object_t conference_info);

		void OnUpdateConerenceCB(
			native_object_t conference_info);

		void OnRemoveConerenceCB(
			int id_conference);

		void OnGetConferenceRealTimeInfoCB(
			native_object_t real_time_conference_info);

		void OnGetConferenceSyncInfoCB(
			native_object_t conference_sync_info);

		void OnAddSelfAttendeeCB(
			native_object_t slef_attendee_info);

		void OnAttendeeOnlineCB(
			native_object_t attendee_info);

		void OnAttendeeOfflineCB(
			const char* account);

		void OnUpdateAttendeeCB(
			native_object_t old_attendee_info, native_object_t new_attendee_info);

		void OnRemoveAttendeeCB(
			const char* account);

		void OnAddConfAdminCB(
			const char* account);

		void OnRemoveConfAdminCB(
			const char* account);

		void OnAddConfDefaultAttendeeCB(
			const char* account);

		void OnRemoveConfDefaultAttendeeCB(
			const char* account);

		void OnApplySpeakOperCB(
			int result, const char* account, int new_state, int old_state, int apply);

		void OnAccreditSpeakOperCB(
			int result, const char* account, int new_state, int old_state, int accredit);

		void OnSpeakNotifyCB(
			const char* account, int new_state, int old_state, int is_speak);

		void OnApplyDataOperCB(
			int result, const char* account, int new_state, int old_state, int apply);

		void OnAccreditDataOperCB(
			int result, const char* account, int new_state, int old_state, int accredit);

		void OnDataOpNotifyCB(
			const char* account, int new_state, int old_state, int is_data_op);

		void OnApplyDataSyncCB(
			int result, const char* account, int new_state, int old_state, int apply);

		void OnAccreditDataSyncCB(
			int result, const char* account, int new_state, int old_state, int accredit);

		void OnSyncOpNotifyCB(
			const char* account, int new_state, int old_state, int is_sync);

		void OnDataSyncCommandCB(
			int result);

		void OnDataSyncCommandNotifyCB(
			const char* syncer, const char* sync_data);

		void OnApplyTempAdminCB(
			int result, const char* account,
			int new_state, int old_state, int apply);

		void OnAccreditTempAdminCB(
			int result, const char* account,
			int new_state, int old_state, int accredit);

		void OnAuthTempAdminCB(
			int result, const char* admin_psw);

		void OnTempAdminNotifyCB(const char* account,
			uint32_t new_state, uint32_t old_state, int apply);

		void OnStartPreviewVideoCB(
			int result, const char* shower,
			int id_device, uint64_t context);

		void OnStopPreviewVideoCB(
			int result, const char* shower,
			int id_device, uint64_t context);

		void OnStartPreviewVideoNotifyCB(
			const char* shower, int id_device);

		void OnStopPreviewVideoNotifyCB(
			const char* shower, int id_device);

		void OnSwitchMainVideoCB(
			int result, int old_dev_id, int new_dev_id);

		void OnSwitchMainVideoNotifyCB(
			const char* account, int old_dev_id, int new_dev_id);

		void OnEnableVideoCB(
			int result, int enabled);

		void OnEnableVideoNotifyCB(
			const char* account, int enabled);

		void OnCaptureVideoStateCB(
			int result, int id_device, int enabled);

		void OnCaptureVideoStateNotifyCB(
			const char* account, int id_device, int enabled);

		void OnVideoShowNameChangedNotifyCB(
			const char* account, int i_device, const char* new_show_name);

		void OnVideoDeviceChangedNotifyCB(
			const char* account, int dev_nums, int dev_state, int dev_main_id);

		void OnKickoutAttendeeCB(
			int result, const char* account);

		void OnKickoutAttendeeNotifyCB(
			const char* oper, const char* account);

		void OnUpdateAttendeeNameCB(
			int result, const char* account);

		void OnUpdateAttendeeNameNotifyCB(
			const char* account, const char* new_name);

		void OnRelayMsgToOneCB(
			int result, const char* receiver);

		void OnRelayMsgToOneNotifyCB(
			const char* sneder, const char* receiver, const char* msg);

		void OnRelayMsgToAllCB(
			int result);

		void OnRelayMsgToAllNotifyCB(
			const char* sneder, const char* msg);

		void OnAdminOperConfSettingCB(
			int result, int cmd, int cmd_value);

		void OnAdminOperConfSettingNotifyCB(
			const char* oper, int cmd, int cmd_value);

		void OnSetConfPasswordCB(int result);

		void OnSetConfPasswordNotifyCB(
			const char* oper);

		void OnStartDesktopShareCB(
			int result);

		void OnStartDesktopShareNotifyCB(
			const char* sharer);

		void OnStopDesktopShareCB(
			int result);

		void OnStopDesktopShareNotifyCB(
			const char* sharer);

		void OnStartPreviewDesktopCB(
			int result, const char* sharer, uint64_t context);

		void OnStopPreviewDesktopCB(
			int result, const char* sharer, uint64_t context);

		void OnStartPreviewDesktopNotifyCB(
			const char* sharer);

		void OnStopPreviewDesktopNotifyCB(
			const char* sharer);

		void OnAskRemoteDesktopControlCB(
			int result, const char* sharer);

		void OnAskRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer);

		void OnAbstainRemoteDesktopControlCB(
			int result, const char* sharer);

		void OnAbstainRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer);

		void OnAnswerRemoteDesktopControlCB(
			int result, const char* controller, int allow);

		void OnAnswerRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer, int allow);

		void OnAbortRemoteDesktopControlCB(
			int result, const char* controller);

		void OnAbortRemoteDesktopControlNotifyCB(
			const char* controller, const char* sharer);

		void OnLaunchSigninCB(
			int result, int type, int64_t launch_or_stop_time);

		void OnLaunchSigninNotifyCB(
			const char*, int type, int64_t launch_or_stop_time);

		void OnSigninCB(
			int result, int is_signin, int64_t launch_time,
			int64_t signin_or_cancel_time);

		void OnSigninNotifyCB(
			const char* account, int is_signin, int64_t launch_time,
			int64_t signin_or_cancel_time);

		void OnOperSubGroupCB(
			int result);

		void OnOperSubGroupNotifyCB(
			const char* oper, int id_sub_group);

		void OnCancelSubGroupCB(
			int result);

		void OnCancelSubGroupNotifyCB(
			const char*);

		void OnOpRemoteDesktopSharedCB(
			int result, const char* sharer, int is_start);

		void OnOpRemoteDesktopSharedNotifyCB(
			const char* oper, const char* sharer, int is_start);

		void OnStartPlaybackCB(
			int result, const char* file_name);

		void OnStartPlaybackNotifyCB(
			const char* account);

		void OnStopPlaybackCB(
			int result);

		void OnStopPlaybackNotifyCB(
			const char* account);

		void OnStartMediaPlayCB(
			int result, const char* file_name, int media_flag);
		
		void OnStartMediaPlayNotifyCB(
			const char* oper, const char* file_name, int media_flag);

		void OnStopMediaPlayCB(
			int result);

		void OnStopMediaPlayNotifyCB(
			const char* oper);

		void OnPauseMediaPlayCB(
			int result, int paused);

		void OnPauseMediaPlayNotifyCB(
			const char* oper, int paused);

	private:
		friend class ConfEngineWrapper;
		explicit ConfBusinessWrapper(native_object_t c_object);
		~ConfBusinessWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_business_wrapper_h__