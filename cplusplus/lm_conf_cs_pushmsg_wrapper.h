#ifndef __lm_conf_cs_pushmsg_wrapper_h__
#define __lm_conf_cs_pushmsg_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSPushMsgWrapper {
	public:
		CSPushMsgWrapper();
		explicit CSPushMsgWrapper(native_object_t c_object);
		~CSPushMsgWrapper();

		CSPushMsgWrapper& operator = (const CSPushMsgWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

		int GetVersionid() const;
		int GetDeleteflag() const;
		int GetMsgid() const;
		int GetMsgtype() const;
		int GetMsgstate() const;
		int GetMsgresult() const;
		int GetSendorgid() const;

		const char* GetSendaccount() const;

		int64_t GetSendtime() const;
		int64_t GetEndtime() const;

		const char* GetMsgtopic() const;
		const char* GetMsgdata() const;
		const char* GetMsgreceiver() const;
		const char* GetExtendjson() const;


		void SetMsgid(int value);
		void SetMsgtype(int value);
		void SetMsgstate(int value);
		void SetMsgresult(int value);
		void SetSendorgid(int value);

		void SetSendaccount(const char* value);

		void SetSendtime(int64_t value);
		void SetEndtime(int64_t value);

		void SetMsgtopic(const char* value);
		void SetMsgdata(const char* value);
		void SetMsgreceiver(const char* value);
		void SetExtendjson(const char* value);

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_pushmsg_wrapper_h__