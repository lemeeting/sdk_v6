#ifndef __lm_conf_engine_wrapper_h__
#define __lm_conf_engine_wrapper_h__

#include "conf_wrapper_interface.h"

namespace lm {
	class ConfBaseWrapper;
	class ConfBusinessWrapper;
	class ConfVideoWrapper;
	class ConfVoiceWrapper;
	class ConfShareDesktopWrapper;
	class ConfRecordWrapper;
	class ConfPlaybackWrapper;
	class ConfMediaPlayerWrapper;
	class ConfDataServerWrapper;
	class ConfCenterServerWrapper;
	class ConfIPCameraWrapper;

	class ConfEngineWrapper {
	public:
		static ConfEngineWrapper* Create();
		static void Delete(ConfEngineWrapper*& conf_engine);
		static ConfEngineWrapper* get();

		ConfBaseWrapper* base_wrapper() const { return base_wrapper_; }
		ConfBusinessWrapper* business_wrapper() const { return business_wrapper_; }
		ConfVideoWrapper* video_wrapper() const { return video_wrapper_; }
		ConfVoiceWrapper* voice_wrapper() const { return voice_wrapper_; }
		ConfShareDesktopWrapper* sharedeskop_wrapper() const { return sharedeskop_wrapper_; }
		ConfRecordWrapper* record_wrapper() const { return record_wrapper_; }
		ConfPlaybackWrapper* playback_wrapper() const { return playback_wrapper_; }
		ConfMediaPlayerWrapper* mediaplayer_wrapper() const { return mediaplayer_wrapper_; }
		ConfDataServerWrapper* dataserver_wrapper() const { return dataserver_wrapper_; }
		ConfCenterServerWrapper* centerserver_wrapper() const { return centerserver_wrapper_; }
		ConfIPCameraWrapper* ipcamera_wrapper() const { return ipcamera_wrapper_; }

	private:
		ConfEngineWrapper();
		~ConfEngineWrapper();

		native_object_t native_conf_engine_;
		native_object_t native_conf_base_;
		native_object_t native_conf_business_;
		native_object_t native_conf_video_;
		native_object_t native_conf_voice_;
		native_object_t native_conf_sharedesktop_;
		native_object_t native_conf_record_;
		native_object_t native_conf_playback_;
		native_object_t native_conf_mediaplayer_;
		native_object_t native_conf_ipcamera_;
		native_object_t native_conf_dataserver_;
		native_object_t native_conf_centerserver_;

		ConfBaseWrapper* base_wrapper_;
		ConfBusinessWrapper* business_wrapper_;
		ConfVideoWrapper* video_wrapper_;
		ConfVoiceWrapper* voice_wrapper_;
		ConfShareDesktopWrapper* sharedeskop_wrapper_;
		ConfRecordWrapper* record_wrapper_;
		ConfPlaybackWrapper* playback_wrapper_;
		ConfMediaPlayerWrapper* mediaplayer_wrapper_;
		ConfDataServerWrapper* dataserver_wrapper_;
		ConfCenterServerWrapper* centerserver_wrapper_;
		ConfIPCameraWrapper* ipcamera_wrapper_;

	};
}

#endif //__lm_conf_engine_wrapper_h__