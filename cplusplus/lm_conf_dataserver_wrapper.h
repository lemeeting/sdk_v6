#ifndef __lm_conf_dataserver_wrapper_h__
#define __lm_conf_dataserver_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfDataServerWrapper {
	public:
		class Observer {
		public:
			virtual void OnPingResult(
				uint32_t task_id, uint64_t context, 
				int error, const LMPingResult* result) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int GetDataServerCount();

		int EnumDataServers(LMDataServerInfo* data_Servers, int& count);

		int GetCurrentConnectDataServer(LMDataServerModule module,
			LMDataServerInfo* data_server);

		int PingDataServer(const char* address, uint16_t port, 
			int64_t callback_time, bool is_use_udp, uint64_t context, uint32_t* task_id);

		int StopPingDataServer(uint32_t task_id);

		int SwitchDataServer(LMDataServerModule module, const char* address);

	private:
		friend void CALLBACK FuncDataServerOnPingResultCB(
			uint32_t task_id, uint64_t context, int error,
			const LMPingResult* result, void* clientdata);

		void OnPingResultCB(uint32_t task_id, uint64_t context, 
			int error, const LMPingResult* result);

	private:
		friend class ConfEngineWrapper;
		explicit ConfDataServerWrapper(native_object_t c_object);
		~ConfDataServerWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_dataserver_wrapper_h__