#ifndef __lm_conf_as_realtimeconfinfo_wrapper_h__
#define __lm_conf_as_realtimeconfinfo_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class ASConfRealTimeInfoWrapper {
	public:
		ASConfRealTimeInfoWrapper();
		explicit ASConfRealTimeInfoWrapper(native_object_t c_object);
		~ASConfRealTimeInfoWrapper();

		ASConfRealTimeInfoWrapper& operator = (const ASConfRealTimeInfoWrapper& rhs);

		static ASConfRealTimeInfoWrapper* from_conf_base();

		uint32_t Flags() const;
		int ConferenceId() const;
		int OnlineAttendee() const;
		bool IsLaunchSignin() const;
		bool IsStopSignin() const;

		LMConferenceSigninType SigninType() const;
		int64_t LanunchSigninTime() const;
		int64_t StopSigninTime() const;

		bool IsFree() const { return LM_CHECK_OPTION(Flags(), kConferenceFree); }
		bool IsLock() const { return LM_CHECK_OPTION(Flags(), kConferenceLock); }
		bool IsOpen() const { return LM_CHECK_OPTION(Flags(), kConferenceOpen); }
		bool IsHide() const { return LM_CHECK_OPTION(Flags(), kConferenceHide); }
		bool IsForceSync() const { return LM_CHECK_OPTION(Flags(), kConferenceForceSync); }
		bool IsDisableText() const { return LM_CHECK_OPTION(Flags(), kConferenceDisableText); }
		bool IsDisableRecord() const { return LM_CHECK_OPTION(Flags(), kConferenceDisableRecord); }
		bool IsDisableBrowVideo() const { return LM_CHECK_OPTION(Flags(), kConferenceDisableBrowVideo); }
		bool IsPlayback() const { return LM_CHECK_OPTION(Flags(), kConferencePlayback); }
		bool IsDisableAllMicrophone() const { return LM_CHECK_OPTION(Flags(), kConferenceDisableMicrophone); }
		bool IsAllMuted() const { return LM_CHECK_OPTION(Flags(), kConferenceMuted); }
		bool IsLockScreen() const { return LM_CHECK_OPTION(Flags(), kConferenceLockScreen); }
		bool IsMediaPlay() const { return LM_CHECK_OPTION(Flags(), kConferenceMediaPlay); }
		bool IsMediaPause() const { return LM_CHECK_OPTION(Flags(), kConferenceMediaPause); }

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif	//__lm_conf_as_realtimeconfinfo_wrapper_h__