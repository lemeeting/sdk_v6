#include "lm_conf_base_wrapper.h"

namespace lm {

	std::string ConfBaseWrapper::ErrorToString(int error) {
		char serror[256] = { 0 };
		lm_get_error_string(error, serror, 256);
		return std::string(serror);
	}

	void ConfBaseWrapper::DefaultInitOptions(LMInitOptions* options) {
		lm_get_default_init_options(options);
	}

	ConfBaseWrapper::ConfBaseWrapper(native_object_t c_object)
		: c_object_(c_object) {
	}

	ConfBaseWrapper::~ConfBaseWrapper() {
	}

	bool ConfBaseWrapper::Init() {
		return lm_conf_base_init(c_object_) == 0;
	}

	bool ConfBaseWrapper::InitWithOptions(const LMInitOptions& options) {
		return lm_conf_base_init_with_options(c_object_, &options) == 0;
	}

	int ConfBaseWrapper::Terminate() {
		lm_conf_base_terminate(c_object_);
		return 0;
	}

	void ConfBaseWrapper::SetUseTcp(bool is_use_tcp) {
		lm_conf_base_set_use_tcp(c_object_, is_use_tcp ? 1 : 0);
	}

	bool ConfBaseWrapper::IsUseTcp() {
		int v = 0;
		lm_conf_base_is_use_tcp(c_object_, &v);
		return !!v;
	}

	void ConfBaseWrapper::SetProxy(const LMProxyOptions& options) {
		lm_conf_base_set_proxy(c_object_, &options);
	}

	void ConfBaseWrapper::GetProxy(LMProxyOptions* options) {
		lm_conf_base_get_proxy(c_object_, options);
	}

	void ConfBaseWrapper::SetScreenCopyFps(int fps) {
		lm_conf_base_set_screen_copy_fps(c_object_, fps);
	}

	int ConfBaseWrapper::GetScreenCopyFps() {
		return lm_conf_base_get_screen_copy_fps(c_object_);
	}

	void ConfBaseWrapper::EnableScreenCopyLayerWindow(bool enable) {
		lm_conf_base_enable_screen_copy_layer_window(c_object_, enable ? 1 : 0);
	}

	bool ConfBaseWrapper::IsEnableScreenCopyLayerWindow() {
		return !!lm_conf_base_is_enable_screen_copy_layer_window(c_object_);
	}

	void ConfBaseWrapper::EnableVirtualDesktopCamera(bool enable) {
		lm_conf_base_enable_virtual_desktop_camera(c_object_, enable ? 1 : 0);
	}

	bool ConfBaseWrapper::IsEnableVirtualDesktopCamera() {
		return !!lm_conf_base_is_enable_virtual_desktop_camera(c_object_);
	}

	void ConfBaseWrapper::SetMainVideoDevice(const char* guid_utf8) {
		lm_conf_base_set_main_video_device(c_object_, guid_utf8);
	}

	int ConfBaseWrapper::GetPerfMon(LMPerMonitor* perfmon) {
		return lm_conf_base_get_per_monitor(c_object_, perfmon);
	}

	void* ConfBaseWrapper::CreateVideoPreviewWindows(
		float originX, float originY, float width, float height) {
		return lm_conf_base_CreateVideoPreviewWindows(c_object_,
			originX, originY, width, height);
	}

	void ConfBaseWrapper::DeleteVideoPreviewWindows(void* windows) {
		lm_conf_base_DeleteVideoPreviewWindows(c_object_, windows);
	}

}