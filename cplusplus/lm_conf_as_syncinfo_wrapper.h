#ifndef __lm_conf_as_syncinfo_wrapper_h__
#define __lm_conf_as_syncinfo_wrapper_h__

#include "conf_wrapper_interface.h"

namespace lm {
	class ASConfSyncInfoWrapper {
	public:
		ASConfSyncInfoWrapper();
		explicit ASConfSyncInfoWrapper(native_object_t c_object);
		~ASConfSyncInfoWrapper();

		ASConfSyncInfoWrapper& operator = (const ASConfSyncInfoWrapper& rhs);

		static ASConfSyncInfoWrapper* from_conf_base();

		bool IsSync() const;
		const char* Syncer() const;
		const char* SyncData() const;

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_as_syncinfo_wrapper_h__