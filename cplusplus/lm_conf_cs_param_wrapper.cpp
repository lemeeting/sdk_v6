#include "lm_conf_cs_param_wrapper.h"

namespace lm {
	CSParamWrapper::CSParamWrapper() : owner_(true) {
		lm_create_cs_param_instance(&c_object_);
	}

	CSParamWrapper::CSParamWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSParamWrapper::~CSParamWrapper() {
		if (owner_)
			lm_destroy_cs_param_instance(c_object_);
	}

	CSParamWrapper& CSParamWrapper::operator = (const CSParamWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_param_instance(&c_object_);
		}
		lm_copy_cs_param_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSParamWrapper::GetKeyCount(int* count) const
	{
        return lm_get_cs_param_count(c_object_);
	}

	int CSParamWrapper::GetKeyList(int* keys, int count) const
	{
		return lm_get_cs_param_keys(c_object_, keys, count);
	}

	int CSParamWrapper::GetKeyValue(int key, int* value) const
	{
		return lm_get_cs_param_int_value(c_object_, key, value);
	}

	int CSParamWrapper::GetKeyValue(int key, unsigned int* value) const
	{
		return lm_get_cs_param_uint_value(c_object_, key, value);
	}

	int CSParamWrapper::GetKeyValue(int key, char* value, int size) const
	{
		return lm_get_cs_param_string_value(c_object_, key, value, size);
	}

	int CSParamWrapper::GetKeyValue(int key, int64_t* value) const
	{
		return lm_get_cs_param_time_value(c_object_, key, value);
	}

	void CSParamWrapper::SetKeyValue(int key, int value)
	{
		lm_set_cs_param_int_value(c_object_, key, value);
	}

	void CSParamWrapper::SetKeyValue(int key, unsigned int value)
	{
		lm_set_cs_param_uint_value(c_object_, key, value);
	}

	void CSParamWrapper::SetKeyValue(int key, const char* value)
	{
		lm_set_cs_param_string_value(c_object_, key, value);
	}

	void CSParamWrapper::SetKeyValue(int key, int64_t value)
	{
		lm_set_cs_param_time_value(c_object_, key, value);
	}


}