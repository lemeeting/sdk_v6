#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"
#include "lm_conf_business_wrapper.h"
#include "lm_conf_centerserver_wrapper.h"
#include "lm_conf_dataserver_wrapper.h"
#include "lm_conf_ipcamera_wrapper.h"
#include "lm_conf_mediaplayer_wrapper.h"
#include "lm_conf_playback_wrapper.h"
#include "lm_conf_record_wrapper.h"
#include "lm_conf_sharedesktop_wrapper.h"
#include "lm_conf_video_wrapper.h"
#include "lm_conf_voice_wrapper.h"

#include <assert.h>

namespace lm {

	static ConfEngineWrapper* s_conf_engine = NULL;

	ConfEngineWrapper* ConfEngineWrapper::Create() {
		if (!s_conf_engine) {
			s_conf_engine = new ConfEngineWrapper;
		}
		return s_conf_engine;
	}

	void ConfEngineWrapper::Delete(ConfEngineWrapper*& conf_engine) {
		if (conf_engine == s_conf_engine) {
			delete s_conf_engine;
			s_conf_engine = NULL;
			conf_engine = NULL;
		}
	}

	ConfEngineWrapper* ConfEngineWrapper::get() {
		assert(s_conf_engine);
		return s_conf_engine;
	}

	ConfEngineWrapper::ConfEngineWrapper() 
		: native_conf_engine_(NULL),
		native_conf_base_(NULL),
		native_conf_business_(NULL),
		native_conf_video_(NULL),
		native_conf_voice_(NULL),
		native_conf_sharedesktop_(NULL),
		native_conf_record_(NULL),
		native_conf_playback_(NULL),
		native_conf_mediaplayer_(NULL),
		native_conf_ipcamera_(NULL),
		native_conf_dataserver_(NULL),
		native_conf_centerserver_(NULL),
		base_wrapper_(NULL),
		business_wrapper_(NULL),
		video_wrapper_(NULL),
		voice_wrapper_(NULL),
		sharedeskop_wrapper_(NULL),
		record_wrapper_(NULL),
		playback_wrapper_(NULL),
		mediaplayer_wrapper_(NULL),
		dataserver_wrapper_(NULL),
		centerserver_wrapper_(NULL),
		ipcamera_wrapper_(NULL) {
		
		lm_create_conf_engine_instance(&native_conf_engine_);
		if (native_conf_engine_) {
			lm_get_conf_base_instance_with_engine(native_conf_engine_, &native_conf_base_);
			lm_get_conf_business_instance_with_engine(native_conf_engine_, &native_conf_business_);
			lm_get_conf_center_server_instance_with_engine(native_conf_engine_, &native_conf_centerserver_);
			lm_get_conf_data_server_instance_with_engine(native_conf_engine_, &native_conf_dataserver_);
			lm_get_conf_record_instance_with_engine(native_conf_engine_, &native_conf_record_);
			lm_get_conf_playback_instance_with_engine(native_conf_engine_, &native_conf_playback_);
			lm_get_conf_mediaplayer_instance_with_engine(native_conf_engine_, &native_conf_mediaplayer_);
			lm_get_conf_video_instance_with_engine(native_conf_engine_, &native_conf_video_);
			lm_get_conf_voice_instance_with_engine(native_conf_engine_, &native_conf_voice_);
			lm_get_conf_sharedesktop_instance_with_engine(native_conf_engine_, &native_conf_sharedesktop_);
			lm_get_conf_ipcamera_instance_with_engine(native_conf_engine_, &native_conf_ipcamera_);

		}

		if (native_conf_base_)
			base_wrapper_ = new ConfBaseWrapper(native_conf_base_);
		if (native_conf_business_)
			business_wrapper_ = new ConfBusinessWrapper(native_conf_business_);
		if (native_conf_centerserver_)
			centerserver_wrapper_ = new ConfCenterServerWrapper(native_conf_centerserver_);
		if (native_conf_dataserver_)
			dataserver_wrapper_ = new ConfDataServerWrapper(native_conf_dataserver_);
		if (native_conf_ipcamera_)
			ipcamera_wrapper_ = new ConfIPCameraWrapper(native_conf_ipcamera_);
		if (native_conf_mediaplayer_)
			mediaplayer_wrapper_ = new ConfMediaPlayerWrapper(native_conf_mediaplayer_);
		if (native_conf_playback_)
			playback_wrapper_ = new ConfPlaybackWrapper(native_conf_playback_);
		if (native_conf_record_)
			record_wrapper_ = new ConfRecordWrapper(native_conf_record_);
		if (native_conf_sharedesktop_)
			sharedeskop_wrapper_ = new ConfShareDesktopWrapper(native_conf_sharedesktop_);
		if (native_conf_video_)
			video_wrapper_ = new ConfVideoWrapper(native_conf_video_);
		if (native_conf_voice_)
			voice_wrapper_ = new ConfVoiceWrapper(native_conf_voice_);
	}

	ConfEngineWrapper::~ConfEngineWrapper() {
		delete voice_wrapper_;
		delete video_wrapper_;
		delete sharedeskop_wrapper_;
		delete record_wrapper_;
		delete playback_wrapper_;
		delete mediaplayer_wrapper_;
		delete ipcamera_wrapper_;
		delete dataserver_wrapper_;
		delete centerserver_wrapper_;
		delete business_wrapper_;
		delete base_wrapper_;

		if (native_conf_engine_)
			lm_destroy_conf_engine_instance(native_conf_engine_);
	}

}