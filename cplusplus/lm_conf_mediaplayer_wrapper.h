#ifndef __lm_conf_mediaplayer_wrapper_h__
#define __lm_conf_mediaplayer_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfMediaPlayerWrapper {
	public:
		class Observer {
		public:
			virtual void OnMediaPlayOpen(int error, int identity) {}
			virtual void OnMediaPlayError(int error, int identity) {}
			virtual void OnMediaPlayComplete(int identity) {}
			virtual void OnMediaPlayDurationChanged(int identity, int64_t durationMS) {}
			virtual void OnMediaPlayPositionChanged(int identity, int64_t positionMS) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};
	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int OpenURL(const char* url, int* identity);
		int Open(const char* filename, int* identity);
		int Close(int identity);

		int Play(int identity, int timeoutS = 10);
		int Stop(int identity);
		int Pause(int identity);
		int PlayStatus(int identity, bool& running);
		int StopStatus(int identity, bool& stopped);
		int PauseStatus(int identity, bool& paused);

		int GetCurrentPosition(int identity, int64_t& positionMS);
		int SetCurrentPosition(int identity, int64_t positionMS);
		int GetDuration(int identity, int64_t& durationMS);

	private:
		friend void CALLBACK FuncMediaPlayOnMediaPlayOpenCB(
			int error, int identity, void* clientdata);
		friend void CALLBACK FuncMediaPlayOnMediaPlayErrorCB(
			int error, int identity, void* clientdata);
		friend void CALLBACK FuncMediaPlayOnMediaPlayCompleteCB(
			int identity, void* clientdata);
		friend void CALLBACK FuncMediaPlayOnMediaPlayDurationChangedCB(
			int identity, int64_t duration, void* clientdata);
		friend void CALLBACK FuncMediaPlayOnMediaPlayPositionChangedCB(
			int identity, int64_t position, void* clientdata);

		void OnMediaPlayOpenCB(int error, int identity);
		void OnMediaPlayErrorCB(int error, int identity);
		void OnMediaPlayCompleteCB(int identity);
		void OnMediaPlayDurationChangedCB(int identity, int64_t duration);
		void OnMediaPlayPositionChangedCB(int identity, int64_t position);

	private:
		friend class ConfEngineWrapper;
		explicit ConfMediaPlayerWrapper(native_object_t c_object);
		~ConfMediaPlayerWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_mediaplayer_wrapper_h__