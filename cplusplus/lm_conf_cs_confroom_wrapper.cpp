#include "lm_conf_cs_confroom_wrapper.h"

namespace lm {
	CSConfRoomWrapper::CSConfRoomWrapper() : owner_(true) {
		lm_create_cs_room_instance(&c_object_);
	}

	CSConfRoomWrapper::CSConfRoomWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSConfRoomWrapper::~CSConfRoomWrapper() {
		if (owner_)
			lm_destroy_cs_room_instance(c_object_);
	}

	CSConfRoomWrapper& CSConfRoomWrapper::operator = (const CSConfRoomWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_room_instance(&c_object_);
		}
		lm_copy_cs_room_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSConfRoomWrapper::GetVersionid() const
	{
		return lm_get_cs_room_version_id(c_object_);
	}

	int CSConfRoomWrapper::GetDeleteflag() const
	{
		return lm_get_cs_room_delete_flag(c_object_);
	}

	int CSConfRoomWrapper::GetConfid() const
	{
		return lm_get_cs_room_id(c_object_);
	}

	int CSConfRoomWrapper::GetConftype() const
	{
		return lm_get_cs_room_type(c_object_);
	}

	int CSConfRoomWrapper::GetWorldid() const
	{
		return lm_get_cs_room_world_id(c_object_);
	}

	int CSConfRoomWrapper::GetGroupid() const
	{
		return lm_get_cs_room_group_id(c_object_);
	}

	int CSConfRoomWrapper::GetServerid() const
	{
		return lm_get_cs_room_server_id(c_object_);
	}

	const char* CSConfRoomWrapper::GetConfname() const
	{
		return lm_get_cs_room_name(c_object_);
	}

	const char* CSConfRoomWrapper::GetConfpassword() const
	{
		return lm_get_cs_room_conf_password(c_object_);
	}

	const char* CSConfRoomWrapper::GetManagepassword() const
	{
		return lm_get_cs_room_manager_password(c_object_);
	}

	int64_t CSConfRoomWrapper::GetStarttime() const
	{
		return lm_get_cs_room_start_time(c_object_);
	}

	int64_t CSConfRoomWrapper::GetEndtime() const
	{
		return lm_get_cs_room_end_time(c_object_);
	}

	int CSConfRoomWrapper::GetMaxusercount() const
	{
		return lm_get_cs_room_max_user_nums(c_object_);
	}

	int CSConfRoomWrapper::GetMaxspeakercount() const
	{
		return lm_get_cs_room_max_speaker_nums(c_object_);
	}

	const char* CSConfRoomWrapper::GetCreator() const
	{
		return lm_get_cs_room_creator(c_object_);
	}

	const char* CSConfRoomWrapper::GetMender() const
	{
		return lm_get_cs_room_mender(c_object_);
	}

	int64_t CSConfRoomWrapper::GetCreatetime() const
	{
		return lm_get_cs_room_create_time(c_object_);
	}

	int64_t CSConfRoomWrapper::GetModifytime() const
	{
		return lm_get_cs_room_modify_time(c_object_);
	}

	int CSConfRoomWrapper::GetConfflag() const
	{
		return lm_get_cs_room_flag(c_object_);
	}

	const char* CSConfRoomWrapper::GetSettingjson() const
	{
		return lm_get_cs_room_setting_json(c_object_);
	}

	const char* CSConfRoomWrapper::GetExtendjson() const
	{
		return lm_get_cs_room_extend_json(c_object_);
	}


	void CSConfRoomWrapper::SetConfid(int value)
	{
		lm_set_cs_room_id(c_object_, value);
	}

	void CSConfRoomWrapper::SetConftype(int value)
	{
		lm_set_cs_room_type(c_object_, value);
	}

	void CSConfRoomWrapper::SetWorldid(int value)
	{
		lm_set_cs_room_world_id(c_object_, value);
	}

	void CSConfRoomWrapper::SetGroupid(int value)
	{
		lm_set_cs_room_group_id(c_object_, value);
	}

	void CSConfRoomWrapper::SetServerid(int value)
	{
		lm_set_cs_room_server_id(c_object_, value);
	}

	void CSConfRoomWrapper::SetConfname(const char* value)
	{
		lm_set_cs_room_name(c_object_, value);
	}

	void CSConfRoomWrapper::SetConfpassword(const char* value)
	{
		lm_set_cs_room_conf_password(c_object_, value);
	}

	void CSConfRoomWrapper::SetManagepassword(const char* value)
	{
		lm_set_cs_room_manager_password(c_object_, value);
	}

	void CSConfRoomWrapper::SetStarttime(int64_t value)
	{
		lm_set_cs_room_start_time(c_object_, value);
	}

    void CSConfRoomWrapper::SetEndtime(int64_t value)
    {
        lm_set_cs_room_end_time(c_object_, value);
    }
    
	void CSConfRoomWrapper::SetMaxusercount(int value)
	{
		lm_set_cs_room_max_user_nums(c_object_, value);
	}

	void CSConfRoomWrapper::SetMaxspeakercount(int value)
	{
		lm_set_cs_room_max_speaker_nums(c_object_, value);
	}

	void CSConfRoomWrapper::SetConfflag(int value)
	{
		lm_set_cs_room_flag(c_object_, value);
	}

	void CSConfRoomWrapper::SetExtendjson(const char* value)
	{
		lm_set_cs_room_extend_json(c_object_, value);
	}
}