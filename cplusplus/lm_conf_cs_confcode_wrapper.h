#ifndef __lm_conf_cs_confcode_wrapper_h__
#define __lm_conf_cs_confcode_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSConfCodeWrapper {
	public:
		CSConfCodeWrapper();
		explicit CSConfCodeWrapper(native_object_t c_object);
		~CSConfCodeWrapper();

		CSConfCodeWrapper& operator = (const CSConfCodeWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_confcode_wrapper_h__