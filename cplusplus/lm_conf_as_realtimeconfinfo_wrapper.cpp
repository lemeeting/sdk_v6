#include "lm_conf_as_realtimeconfinfo_wrapper.h"
#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"

namespace lm {
	ASConfRealTimeInfoWrapper* ASConfRealTimeInfoWrapper::from_conf_base() {
		native_object_t c_object;
		if (lm_conf_base_get_real_time_conf_info(ConfEngineWrapper::get()->base_wrapper()->cobject(), &c_object) != 0) {
			return NULL;
		}
		return new ASConfRealTimeInfoWrapper(c_object);
	}

	ASConfRealTimeInfoWrapper::ASConfRealTimeInfoWrapper() : owner_(true) {
		lm_create_as_real_time_conference_info_instance(&c_object_);
	}

	ASConfRealTimeInfoWrapper::ASConfRealTimeInfoWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	ASConfRealTimeInfoWrapper::~ASConfRealTimeInfoWrapper() {
		if (owner_)
			lm_destroy_as_real_time_conference_info_instance(c_object_);
	}

	ASConfRealTimeInfoWrapper& ASConfRealTimeInfoWrapper::operator = (const ASConfRealTimeInfoWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_as_real_time_conference_info_instance(&c_object_);
		}
		lm_copy_as_real_time_conference_info_instance(rhs.c_object_, c_object_);
		return *this;
	}

	uint32_t ASConfRealTimeInfoWrapper::Flags() const {
		return lm_get_as_real_time_conference_info_flags(c_object_);
	}

	int ASConfRealTimeInfoWrapper::ConferenceId() const {
		return lm_get_as_real_time_conference_info_conference_id(c_object_);
	}

	int ASConfRealTimeInfoWrapper::OnlineAttendee() const {
		return lm_get_as_real_time_conference_info_online_attendees(c_object_);
	}

	bool ASConfRealTimeInfoWrapper::IsLaunchSignin() const {
		return !!lm_as_real_time_conference_info_is_lanunch_signin(c_object_);
	}

	bool ASConfRealTimeInfoWrapper::IsStopSignin() const {
		return !!lm_as_real_time_conference_info_is_stop_signin(c_object_);
	}

	LMConferenceSigninType ASConfRealTimeInfoWrapper::SigninType() const {
		return LMConferenceSigninType(lm_get_as_real_time_conference_info_signin_type(c_object_));
	}

	int64_t ASConfRealTimeInfoWrapper::LanunchSigninTime() const {
		return lm_get_as_real_time_conference_info_lanunch_signin_time(c_object_);
	}

	int64_t ASConfRealTimeInfoWrapper::StopSigninTime() const {
		return lm_get_as_real_time_conference_info_stop_signin_time(c_object_);
	}
}