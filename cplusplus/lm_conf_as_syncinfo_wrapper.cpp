#include "lm_conf_as_syncinfo_wrapper.h"
#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"

namespace lm {

	ASConfSyncInfoWrapper* ASConfSyncInfoWrapper::from_conf_base() {
		native_object_t c_object;
		if (lm_conf_base_get_conf_sync_info(ConfEngineWrapper::get()->base_wrapper()->cobject(), &c_object) != 0) {
			return NULL;
		}
		return new ASConfSyncInfoWrapper(c_object);
	}

	ASConfSyncInfoWrapper::ASConfSyncInfoWrapper() : owner_(true) {
		lm_create_as_conference_sync_info_instance(&c_object_);
	}

	ASConfSyncInfoWrapper::ASConfSyncInfoWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	ASConfSyncInfoWrapper::~ASConfSyncInfoWrapper() {
		if (owner_)
			lm_destroy_as_conference_sync_info_instance(c_object_);
	}

	ASConfSyncInfoWrapper& ASConfSyncInfoWrapper::operator = (const ASConfSyncInfoWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_as_conference_sync_info_instance(&c_object_);
		}
		lm_copy_as_conference_sync_info_instance(rhs.c_object_, c_object_);
		return *this;
	}

	bool ASConfSyncInfoWrapper::IsSync() const {
		return !!lm_as_conference_sync_info_is_sync(c_object_);
	}

	const char* ASConfSyncInfoWrapper::Syncer() const {
		return lm_get_as_conference_sync_info_syncer(c_object_);
	}

	const char* ASConfSyncInfoWrapper::SyncData() const {
		return lm_get_as_conference_sync_info_sync_data(c_object_);
	}

}