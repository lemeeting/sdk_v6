#include "lm_conf_cs_pushmsg_wrapper.h"

namespace lm {
	CSPushMsgWrapper::CSPushMsgWrapper() : owner_(true) {
		lm_create_cs_push_msg_instance(&c_object_);
	}

	CSPushMsgWrapper::CSPushMsgWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSPushMsgWrapper::~CSPushMsgWrapper() {
		if (owner_)
			lm_destroy_cs_push_msg_instance(c_object_);
	}

	CSPushMsgWrapper& CSPushMsgWrapper::operator = (const CSPushMsgWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_push_msg_instance(&c_object_);
		}
		lm_copy_cs_user_bind_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSPushMsgWrapper::GetVersionid() const
	{
		return lm_get_cs_push_msg_version_id(c_object_);
	}

	int CSPushMsgWrapper::GetDeleteflag() const
	{
		return lm_get_cs_push_msg_delete_flag(c_object_);
	}

	int CSPushMsgWrapper::GetMsgid() const
	{
		return lm_get_cs_push_msg_id(c_object_);
	}

	int CSPushMsgWrapper::GetMsgtype() const
	{
		return lm_get_cs_push_msg_type(c_object_);
	}

	int CSPushMsgWrapper::GetMsgstate() const
	{
		return lm_get_cs_push_msg_state(c_object_);
	}

	int CSPushMsgWrapper::GetMsgresult() const
	{
		return lm_get_cs_push_msg_result(c_object_);
	}

	int CSPushMsgWrapper::GetSendorgid() const
	{
		return lm_get_cs_push_msg_send_org_id(c_object_);
	}

	const char* CSPushMsgWrapper::GetSendaccount() const
	{
		return lm_get_cs_push_msg_send_account(c_object_);
	}

	int64_t CSPushMsgWrapper::GetSendtime() const
	{
		return lm_get_cs_push_msg_send_time(c_object_);
	}

	int64_t CSPushMsgWrapper::GetEndtime() const
	{
		return lm_get_cs_push_msg_end_time(c_object_);
	}

	const char* CSPushMsgWrapper::GetMsgtopic() const
	{
		return lm_get_cs_push_msg_topic(c_object_);
	}

	const char* CSPushMsgWrapper::GetMsgdata() const
	{
		return lm_get_cs_push_msg_data(c_object_);
	}

	const char* CSPushMsgWrapper::GetMsgreceiver() const
	{
		return lm_get_cs_push_msg_receiver(c_object_);
	}

	const char* CSPushMsgWrapper::GetExtendjson() const
	{
		return lm_get_cs_push_msg_extend_json(c_object_);
	}

	void CSPushMsgWrapper::SetMsgid(int value)
	{
		lm_set_cs_push_msg_id(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgtype(int value)
	{
		lm_set_cs_push_msg_type(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgstate(int value)
	{
		lm_set_cs_push_msg_state(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgresult(int value)
	{
		lm_set_cs_push_msg_result(c_object_, value);
	}

	void CSPushMsgWrapper::SetSendorgid(int value)
	{
		lm_set_cs_push_msg_send_org_id(c_object_, value);
	}

	void CSPushMsgWrapper::SetSendaccount(const char* value)
	{
		lm_set_cs_push_msg_send_account(c_object_, value);
	}

	void CSPushMsgWrapper::SetSendtime(int64_t value)
	{
		lm_set_cs_push_msg_send_time(c_object_, value);
	}

	void CSPushMsgWrapper::SetEndtime(int64_t value)
	{
		lm_set_cs_push_msg_end_time(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgtopic(const char* value)
	{
		lm_set_cs_push_msg_topic(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgdata(const char* value)
	{
		lm_set_cs_push_msg_data(c_object_, value);
	}

	void CSPushMsgWrapper::SetMsgreceiver(const char* value)
	{
		lm_set_cs_push_msg_receiver(c_object_, value);
	}

	void CSPushMsgWrapper::SetExtendjson(const char* value)
	{
		lm_set_cs_push_msg_extend_json(c_object_, value);
	}
}