#include "lm_conf_cs_applymsg_wrapper.h"

namespace lm {
	CSApplyMsgWrapper::CSApplyMsgWrapper() : owner_(true) {
		lm_create_cs_apply_msg_instance(&c_object_);
	}

	CSApplyMsgWrapper::CSApplyMsgWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSApplyMsgWrapper::~CSApplyMsgWrapper() {
		if (owner_)
			lm_destroy_cs_apply_msg_instance(c_object_);
	}

	CSApplyMsgWrapper& CSApplyMsgWrapper::operator = (const CSApplyMsgWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_apply_msg_instance(&c_object_);
		}
		lm_copy_cs_apply_msg_instance(rhs.c_object_, c_object_);
		return *this;
	}

}