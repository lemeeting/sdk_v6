#include "lm_conf_record_wrapper.h"

namespace lm {
	void CALLBACK FuncRecordOnStartLocalRecordReadyCB(void* clientdata) {
		ConfRecordWrapper* wrapper = reinterpret_cast<ConfRecordWrapper*>(clientdata);
		wrapper->OnStartLocalRecordReadyCB();
	}

	void ConfRecordWrapper::OnStartLocalRecordReadyCB() {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartLocalRecordReady();
		}
	}

	ConfRecordWrapper::ConfRecordWrapper(native_object_t c_object)
		: c_object_(c_object) {
		ConfRecordCallback cb;
		memset(&cb, 0, sizeof(cb));
		cb.client_data_ = this;
		cb.OnStartLocalRecordReady_ = FuncRecordOnStartLocalRecordReadyCB;
		lm_conf_record_set_callback(c_object_, &cb);
	}

	ConfRecordWrapper::~ConfRecordWrapper() {

	}

	void ConfRecordWrapper::AddObserver(Observer* observer) {
		if (std::find(observer_list_.begin(), observer_list_.end(), observer) == observer_list_.end())
			observer_list_.push_back(observer);
	}

	void ConfRecordWrapper::RemoveObserver(Observer* observer) {
		ObserverList::iterator it = std::find(observer_list_.begin(), observer_list_.end(), observer);
		if (it != observer_list_.end())
			observer_list_.erase(it);
	}

	int ConfRecordWrapper::StartLocalRecord(const char* file_name, const char** recordaccounts, int count,
		int flags/* = kRecordVoice | kRecordVideo | kRecordWbd | kRecordAppShare | kRecordText*/) {
		return lm_conf_record_StartLocalRecord(c_object_, file_name, recordaccounts, count, flags);
	}

	int ConfRecordWrapper::StopLocalRecord() {
		return lm_conf_record_StopLocalRecord(c_object_);
	}

	int ConfRecordWrapper::StartScreenRecord(const char* file_name, LMScreenRecordFormats format, LMScreenRecordZoom zoom,
		float frame_rate, LMScreenRecordQP qp, int flags/* = kScreenRecordInputVoice | kScreenRecordOutputVoice*/) {
		return lm_conf_record_StartScreenRecord(c_object_, file_name, format, zoom, frame_rate, qp, flags);
	}

	int ConfRecordWrapper::StopScreenRecord() {
		return lm_conf_record_StopScreenRecord(c_object_);
	}

	int ConfRecordWrapper::StopRecord() {
		return lm_conf_record_StopRecord(c_object_);
	}

	LMConfRecordType ConfRecordWrapper::CurrentRecordType() {
		return LMConfRecordType(lm_conf_record_CurrentRecordType(c_object_));
	}
}