#ifndef __lm_conf_centerserver_wrapper_h__
#define __lm_conf_centerserver_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class CSParamWrapper;
	class CSApplyMsgWrapper;
	class CSConfCodeWrapper;
	class CSConfRoomWrapper;
	class CSOrgInfoWrapper;
	class CSOrgUserWrapper;
	class CSPushMsgWrapper;
	class CSUserBindWrapper;
	class CSUserInfoWrapper;
	class CSRealTimeConfWrapper;

	class ConfCenterServerWrapper {

	public:
		class Observer {
		public:
			virtual void OnDisconnectCenter(int result) {}

			virtual void OnGetAuthCode(int result,
				const lm::CSParamWrapper* param, const char* code) {}

			virtual void OnRegUserInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSUserInfoWrapper* info) {}

			virtual void OnLogin(int result,
				const lm::CSParamWrapper* param, const lm::CSUserInfoWrapper* info) {}

			virtual void OnLogout(int result,
				const lm::CSParamWrapper* param) {}

			virtual void OnPrepareLoginConf(int result,
				const lm::CSParamWrapper* param, int id_conf, const char* jsonAddress) {}

			virtual void OnGetRealConf(int result,
				const lm::CSParamWrapper* param,
				const lm::CSRealTimeConfWrapper** infos, int count) {}

			virtual void OnGetUserInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSUserInfoWrapper* info) {}

			virtual void OnUpdateUserInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSUserInfoWrapper* info) {}

			virtual void OnRemoveUserInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSUserInfoWrapper* info) {}

			virtual void OnGetUserInfoList(int result, const lm::CSParamWrapper* param,
				const lm::CSUserInfoWrapper* info,
				const lm::CSUserInfoWrapper** infos, int count) {}

			virtual void OnGetOrgInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgInfoWrapper* info) {}

			virtual void OnUpdateOrgInfo(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgInfoWrapper* info) {}

			virtual void OnGetOrgInfoList(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgInfoWrapper* info,
				const lm::CSOrgInfoWrapper** infos, int count) {}

			virtual void OnGetUserBind(int result,
				const lm::CSParamWrapper* param, const lm::CSUserBindWrapper* info) {}

			virtual void OnAddUserBind(int result,
				const lm::CSParamWrapper* param, const lm::CSUserBindWrapper* info) {}

			virtual void OnUpdateUserBind(int result,
				const lm::CSParamWrapper* param, const lm::CSUserBindWrapper* info) {}

			virtual void OnRemoveUserBind(int result,
				const lm::CSParamWrapper* param, const lm::CSUserBindWrapper* info) {}

			virtual void OnGetUserBindList(int result,
				const lm::CSParamWrapper* param, const lm::CSUserBindWrapper* info,
				const lm::CSUserBindWrapper** infos, int count) {}

			virtual void OnGetOrgUser(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgUserWrapper* info) {}

			virtual void OnAddOrgUser(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgUserWrapper* info) {}

			virtual void OnUpdateOrgUser(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgUserWrapper* info) {}

			virtual void OnRemoveOrgUser(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgUserWrapper* info) {}

			virtual void OnGetOrgUserList(int result,
				const lm::CSParamWrapper* param, const lm::CSOrgUserWrapper* info,
				const lm::CSOrgUserWrapper** infos, int count) {}

			virtual void OnGetConfRoom(int result,
				const lm::CSParamWrapper* param, const lm::CSConfRoomWrapper* info) {}

			virtual void OnAddConfRoom(int result,
				const lm::CSParamWrapper* param, const lm::CSConfRoomWrapper* info) {}

			virtual void OnUpdateConfRoom(int result,
				const lm::CSParamWrapper* param, const lm::CSConfRoomWrapper* info) {}

			virtual void OnRemoveConfRoom(int result,
				const lm::CSParamWrapper* param, const lm::CSConfRoomWrapper* info) {}

			virtual void OnGetConfRoomList(int result,
				const lm::CSParamWrapper* param, const lm::CSConfRoomWrapper* info,
				const lm::CSConfRoomWrapper** infos, int count) {}

			virtual void OnGetConfCode(int result,
				const lm::CSParamWrapper* param, const lm::CSConfCodeWrapper* info) {}

			virtual void OnAddConfCode(int result,
				const lm::CSParamWrapper* param, const lm::CSConfCodeWrapper* info) {}

			virtual void OnUpdateConfCode(int result,
				const lm::CSParamWrapper* param, const lm::CSConfCodeWrapper* info) {}

			virtual void OnRemoveConfCode(int result,
				const lm::CSParamWrapper* param, const lm::CSConfCodeWrapper* info) {}

			virtual void OnGetConfCodeList(int result,
				const lm::CSParamWrapper* param, const lm::CSConfCodeWrapper* info,
				const lm::CSConfCodeWrapper** infos, int count) {}

			virtual void OnGetApplyMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSApplyMsgWrapper* info) {}

			virtual void OnAddApplyMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSApplyMsgWrapper* info) {}

			virtual void OnUpdateApplyMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSApplyMsgWrapper* info) {}

			virtual void OnRemoveApplyMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSApplyMsgWrapper* info) {}

			virtual void OnGetApplyMsgList(int result,
				const lm::CSParamWrapper* param, const lm::CSApplyMsgWrapper* info,
				const lm::CSApplyMsgWrapper** infos, int count) {}

			virtual void OnGetPushMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info) {}

			virtual void OnAddPushMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info) {}

			virtual void OnUpdatePushMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info) {}

			virtual void OnRemovePushMsg(int result,
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info) {}

			virtual void OnGetPushMsgList(int result,
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info,
				const lm::CSPushMsgWrapper** infos, int count) {}

			virtual void OnNoticePushMsg(
				const lm::CSParamWrapper* param, const lm::CSPushMsgWrapper* info) {}

		protected:
			Observer() {}
			virtual ~Observer() {}

		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int SetCenterAddress(const char* address);
		int GetCenterAddress(char* address, int count);

		int GetAuthCode(const CSParamWrapper* param);
		int RegUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info);

		int Login(const CSParamWrapper* param, int id_org, const char* account, const char* psw);
		int Logout(const CSParamWrapper* param);

		int PrepareLoginConf(const CSParamWrapper* param, int id_conf);
		int GetRealConf(const CSParamWrapper* param, int* id_confs, int count);

		int GetUserInfo(const CSParamWrapper* param, const char* account);
		int UpdateUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info);
		int RemoveUserInfo(const CSParamWrapper* param, const CSUserInfoWrapper* info);
		int GetUserInfoList(const CSParamWrapper* param, const CSUserInfoWrapper* info);

		int GetOrgInfo(const CSParamWrapper* param, int id_org);
		int UpdateOrgInfo(const CSParamWrapper* param, const CSOrgInfoWrapper* info);
		int GetOrgInfoList(const CSParamWrapper* param, const CSOrgInfoWrapper* info);

		int GetUserBind(const CSParamWrapper* param, const char* name);
		int AddUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info);
		int UpdateUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info);
		int RemoveUserBind(const CSParamWrapper* param, const CSUserBindWrapper* info);
		int GetUserBindList(const CSParamWrapper* param, const CSUserBindWrapper* info);

		int GetOrgUser(const CSParamWrapper* param, int id_org, const char* account);
		int AddOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info);
		int UpdateOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info);
		int RemoveOrgUser(const CSParamWrapper* param, const CSOrgUserWrapper* info);
		int GetOrgUserList(const CSParamWrapper* param, const CSOrgUserWrapper* info);

		int GetConfRoom(const CSParamWrapper* param, int id_conf);
		int AddConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info);
		int UpdateConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info);
		int RemoveConfRoom(const CSParamWrapper* param, const CSConfRoomWrapper* info);
		int GetConfRoomList(const CSParamWrapper* param, const CSConfRoomWrapper* info);

		int GetConfCode(const CSParamWrapper* param, const char* strCodeid);
		int AddConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info);
		int UpdateConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info);
		int RemoveConfCode(const CSParamWrapper* param, const CSConfCodeWrapper* info);
		int GetConfCodeList(const CSParamWrapper* param, const CSConfCodeWrapper* info);

		int GetApplyMsg(const CSParamWrapper* param, int id_req);
		int AddApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info);
		int UpdateApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info);
		int RemoveApplyMsg(const CSParamWrapper* param, const CSApplyMsgWrapper* info);
		int GetApplyMsgList(const CSParamWrapper* param, const CSApplyMsgWrapper* info);

		int GetPushMsg(const CSParamWrapper* param, int msg_id);
		int AddPushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info);
		int UpdatePushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info);
		int RemovePushMsg(const CSParamWrapper* param, const CSPushMsgWrapper* info);
		int GetPushMsgList(const CSParamWrapper* param, const CSPushMsgWrapper* info);

		int GetConfCode(const CSParamWrapper* param, int id_conf);

	private:
		friend void CALLBACK FuncToCenterOnDisconnectCenterCB(int result, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetAuthCodeCB(int result, native_object_t param, const char* code, void* clientdata);
		friend void CALLBACK FuncToCenterOnRegUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnLoginCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnLogoutCB(int result, native_object_t param, void* clientdata);
		friend void CALLBACK FuncToCenterOnPrepareLoginConfCB(int result, native_object_t param, int id_conf, const char* json_address, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetRealConfCB(int result, native_object_t param, native_object_t* info, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetUserInfoListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetOrgInfoListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetUserBindListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetOrgUserListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetConfRoomListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetConfCodeListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdateApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemoveApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetApplyMsgListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnAddPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnUpdatePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnRemovePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata);
		friend void CALLBACK FuncToCenterOnGetPushMsgListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count, void* clientdata);
		friend void CALLBACK FuncToCenterOnNoticePushMsgCB(native_object_t, native_object_t param, void* clientdata);

		void OnDisconnectCenterCB(int result);
		void OnGetAuthCodeCB(int result, native_object_t param, const char* code);
		void OnRegUserInfoCB(int result, native_object_t param, native_object_t info);
		void OnLoginCB(int result, native_object_t param, native_object_t info);
		void OnLogoutCB(int result, native_object_t param);
		void OnPrepareLoginConfCB(int result, native_object_t param, int id_conf, const char* json_address);
		void OnGetRealConfCB(int result, native_object_t param, native_object_t* infos, int count);
		void OnGetUserInfoCB(int result, native_object_t param, native_object_t info);
		void OnUpdateUserInfoCB(int result, native_object_t param, native_object_t info);
		void OnRemoveUserInfoCB(int result, native_object_t param, native_object_t info);
		void OnGetUserInfoListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetOrgInfoCB(int result, native_object_t param, native_object_t info);
		void OnUpdateOrgInfoCB(int result, native_object_t param, native_object_t info);
		void OnGetOrgInfoListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetUserBindCB(int result, native_object_t param, native_object_t info);
		void OnAddUserBindCB(int result, native_object_t param, native_object_t info);
		void OnUpdateUserBindCB(int result, native_object_t param, native_object_t info);
		void OnRemoveUserBindCB(int result, native_object_t param, native_object_t info);
		void OnGetUserBindListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetOrgUserCB(int result, native_object_t param, native_object_t info);
		void OnAddOrgUserCB(int result, native_object_t param, native_object_t info);
		void OnUpdateOrgUserCB(int result, native_object_t param, native_object_t info);
		void OnRemoveOrgUserCB(int result, native_object_t param, native_object_t info);
		void OnGetOrgUserListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetConfRoomCB(int result, native_object_t param, native_object_t info);
		void OnAddConfRoomCB(int result, native_object_t param, native_object_t info);
		void OnUpdateConfRoomCB(int result, native_object_t param, native_object_t info);
		void OnRemoveConfRoomCB(int result, native_object_t param, native_object_t info);
		void OnGetConfRoomListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetConfCodeCB(int result, native_object_t param, native_object_t info);
		void OnAddConfCodeCB(int result, native_object_t param, native_object_t info);
		void OnUpdateConfCodeCB(int result, native_object_t param, native_object_t info);
		void OnRemoveConfCodeCB(int result, native_object_t param, native_object_t info);
		void OnGetConfCodeListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetApplyMsgCB(int result, native_object_t param, native_object_t info);
		void OnAddApplyMsgCB(int result, native_object_t param, native_object_t info);
		void OnUpdateApplyMsgCB(int result, native_object_t param, native_object_t info);
		void OnRemoveApplyMsgCB(int result, native_object_t param, native_object_t info);
		void OnGetApplyMsgListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnGetPushMsgCB(int result, native_object_t param, native_object_t info);
		void OnAddPushMsgCB(int result, native_object_t param, native_object_t info);
		void OnUpdatePushMsgCB(int result, native_object_t param, native_object_t info);
		void OnRemovePushMsgCB(int result, native_object_t param, native_object_t info);
		void OnGetPushMsgListCB(int result, native_object_t param, native_object_t info, native_object_t* infos, int count);
		void OnNoticePushMsgCB(native_object_t param, native_object_t info);

	private:
		friend class ConfEngineWrapper;
		explicit ConfCenterServerWrapper(native_object_t c_object);
		~ConfCenterServerWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_centerserver_wrapper_h__