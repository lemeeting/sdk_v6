#include "lm_conf_cs_orguser_wrapper.h"

namespace lm {
	CSOrgUserWrapper::CSOrgUserWrapper() : owner_(true) {
		lm_create_cs_org_user_instance(&c_object_);
	}

	CSOrgUserWrapper::CSOrgUserWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSOrgUserWrapper::~CSOrgUserWrapper() {
		if (owner_)
			lm_destroy_cs_org_user_instance(c_object_);
	}

	CSOrgUserWrapper& CSOrgUserWrapper::operator = (const CSOrgUserWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_org_user_instance(&c_object_);
		}
		lm_copy_cs_org_user_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int CSOrgUserWrapper::GetVersionid() const
	{
		return lm_get_cs_org_user_version_id(c_object_);
	}

	int CSOrgUserWrapper::GetDeleteflag() const
	{
		return lm_get_cs_org_user_delete_flag(c_object_);
	}

	int CSOrgUserWrapper::GetOrgid() const
	{
		return lm_get_cs_org_user_org_id(c_object_);
	}


	const char* CSOrgUserWrapper::GetUseraccount() const
	{
		return lm_get_cs_org_user_account(c_object_);
	}

	int CSOrgUserWrapper::GetUsertype() const
	{
		return lm_get_cs_org_user_type(c_object_);
	}

	int CSOrgUserWrapper::GetUserstatus() const
	{
		return lm_get_cs_org_user_status(c_object_);
	}

	int CSOrgUserWrapper::GetBinduser() const
	{
		return lm_get_cs_org_user_bind_user(c_object_);
	}

	const char* CSOrgUserWrapper::GetUsername() const
	{
		return lm_get_cs_org_user_name(c_object_);
	}

	const char* CSOrgUserWrapper::GetPassword() const
	{
		return lm_get_cs_org_user_password(c_object_);
	}

	const char* CSOrgUserWrapper::GetPhone() const
	{
		return lm_get_cs_org_user_phone(c_object_);
	}

	const char* CSOrgUserWrapper::GetEmail() const
	{
		return lm_get_cs_org_user_email(c_object_);
	}

	int64_t CSOrgUserWrapper::GetBirthday() const
	{
		return lm_get_cs_org_user_birthday(c_object_);
	}

	int64_t CSOrgUserWrapper::GetCreatetime() const
	{
		return lm_get_cs_org_user_create_time(c_object_);
	}

	const char* CSOrgUserWrapper::GetSettingjson() const
	{
		return lm_get_cs_org_user_setting_json(c_object_);
	}

	const char* CSOrgUserWrapper::GetExtendjson() const
	{
		return lm_get_cs_org_user_extend_json(c_object_);
	}

	int64_t CSOrgUserWrapper::GetUserperm() const
	{
		return lm_get_cs_org_user_perm(c_object_);
	}

	void CSOrgUserWrapper::SetOrgid(int value)
	{
		lm_set_cs_org_user_org_id(c_object_, value);
	}

	void CSOrgUserWrapper::SetUseraccount(const char* value)
	{
		lm_set_cs_org_user_account(c_object_, value);
	}

	void CSOrgUserWrapper::SetUsertype(int value)
	{
		lm_set_cs_org_user_type(c_object_, value);
	}

	void CSOrgUserWrapper::SetUserstatus(int value)
	{
		lm_set_cs_org_user_status(c_object_, value);
	}

	void CSOrgUserWrapper::SetBinduser(int value)
	{
		lm_set_cs_org_user_bind_user(c_object_, value);
	}

	void CSOrgUserWrapper::SetUsername(const char* value)
	{
		lm_set_cs_org_user_name(c_object_, value);
	}

	void CSOrgUserWrapper::SetPassword(const char* value)
	{
		lm_set_cs_org_user_password(c_object_, value);
	}

	void CSOrgUserWrapper::SetPhone(const char* value)
	{
		lm_set_cs_org_user_phone(c_object_, value);
	}

	void CSOrgUserWrapper::SetEmail(const char* value)
	{
		lm_set_cs_org_user_email(c_object_, value);
	}

	void CSOrgUserWrapper::SetBirthday(int64_t value)
	{
		lm_set_cs_org_user_birthday(c_object_, value);
	}

	void CSOrgUserWrapper::SetCreatetime(int64_t value)
	{
		lm_set_cs_org_user_create_time(c_object_, value);
	}

	void CSOrgUserWrapper::SetSettingjson(const char* value)
	{
		lm_set_cs_org_user_setting_json(c_object_, value);
	}

	void CSOrgUserWrapper::SetExtendjson(const char* value)
	{
		lm_set_cs_org_user_extend_json(c_object_, value);
	}

	void CSOrgUserWrapper::SetUserperm(int64_t value)
	{
		lm_set_cs_org_user_perm(c_object_, value);
	}

}