#ifndef __lm_conf_ipcamera_wrapper_h__
#define __lm_conf_ipcamera_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfIPCameraWrapper {
	public:
		class Observer {
		public:
			virtual void OnSearchWithLAN(int error, const LMIPCameraAddress* results, int count) {}
			virtual void OnAddIPCamera(int error, int identity, const LMIPCameraInfo* info) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int SearchWithLAN(const char* multicast_address = "239.255.255.250:3702");

		int AddIPCamera(
			const LMIPCameraAddress* address,
			const LMIPCameraAuthInfo* auth,
			int* identity);

		int RemoveIPCamera(int identity);

	private:
		friend void CALLBACK FuncIPCameraOnSearchWithLANCB(
			int error, const LMIPCameraAddress* results, int count, void* clientdata);
		friend void CALLBACK FuncIPCameraOnAddIPCameraCB(
			int error, int identity, const LMIPCameraInfo* info, void* clientdata);

		void OnSearchWithLANCB(int error, const LMIPCameraAddress* results, int count);
		void OnAddIPCameraCB(int error, int identity, const LMIPCameraInfo* info);

	private:
		friend class ConfEngineWrapper;
		explicit ConfIPCameraWrapper(native_object_t c_object);
		~ConfIPCameraWrapper();

		native_object_t c_object_;
		
		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_ipcamera_wrapper_h__