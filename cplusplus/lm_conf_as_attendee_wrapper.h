#ifndef __lm_conf_as_attendee_wrapper_h__
#define __lm_conf_as_attendee_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class ASConfAttendeeWrapper {
	public:
		ASConfAttendeeWrapper();
		explicit ASConfAttendeeWrapper(native_object_t c_object);
		~ASConfAttendeeWrapper();

		ASConfAttendeeWrapper& operator = (const ASConfAttendeeWrapper& rhs);

		static ASConfAttendeeWrapper* from_conf_base(const char* account);
		static ASConfAttendeeWrapper* self_from_conf_base();

		int Type() const;
		LMConferenceClientType LoginType() const;
		uint32_t OpsStatus() const;
		LMConferenceAdminModes AdminPrivilege() const;
		int NumOfVoiceDevices() const;
		int NumOfVideoDevices() const;
		void EnumVideoDevices(DeviceIdentity* id_devices, int* nums) const;
		bool IsEnableVideoDevice(DeviceIdentity id_device) const;
		LMCameraType GetVideoDeviceType(DeviceIdentity id_device) const;
		LMCameraSubType GetVideoDeviceSubType(DeviceIdentity id_device) const;
		const char* GetVideoDeviceShowName(DeviceIdentity id_device) const;
		DeviceIdentity MainVideoDeviceId() const;
		bool IsForbiddenVideoDevice() const;
		const char* Account() const;
		const char* Name() const;
		int EnterpriseId() const;
		int AccessServerId() const;
		int ConferenceId() const;
		const char* MacAddress() const;
		int64_t SigninTime() const;

		bool IsValidVideoDevice(DeviceIdentity id_device) const;
		
		bool IsAdmin() const { return AdminPrivilege() != kConferenceNonAdmin; }

		bool ApplySpeakOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsApplySpeak); }
		bool SpeakOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsSpeak); }
		bool ApplyDataOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsApplyOperData); }
		bool DataOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsOperData); }
		bool ApplySyncOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsApplySync); }
		bool SyncOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsSync); }
		bool ApplyAdminOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsApplyAdmin); }
		bool SharedDesktopOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsSharedDesktop); }
		bool PlaybackOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsPlayback); }
		bool MediaPlayOp() const { return LM_CHECK_OPTION(OpsStatus(), kOpsMediaPlay); }
		bool AskRemoteControlDesktopStatus() const { return LM_CHECK_OPTION(OpsStatus(), kOpsAskRemoteControlDesktop); }
		bool InRemoteControlDesktopStatus() const { return LM_CHECK_OPTION(OpsStatus(), kOpsInRemoteControlDesktop); }

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_as_attendee_wrapper_h__