#include "lm_conf_as_confattribute_wrapper.h"
#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"

namespace lm {
	ASConfAttributeWrapper* ASConfAttributeWrapper::from_conf_base() {
		native_object_t c_object;
		if (lm_conf_base_get_conf_attribute(ConfEngineWrapper::get()->base_wrapper()->cobject(), &c_object) != 0) {
			return NULL;
		}
		return new ASConfAttributeWrapper(c_object);
	}

	ASConfAttributeWrapper::ASConfAttributeWrapper() : owner_(true) {
		lm_create_as_conference_attribute_instance(&c_object_);
	}

	ASConfAttributeWrapper::ASConfAttributeWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	ASConfAttributeWrapper::~ASConfAttributeWrapper() {
		if (owner_)
			lm_destroy_as_conference_attribute_instance(c_object_);
	}

	ASConfAttributeWrapper& ASConfAttributeWrapper::operator = (const ASConfAttributeWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_as_conference_attribute_instance(&c_object_);
		}
		lm_copy_as_conference_attribute_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int ASConfAttributeWrapper::OrgId() const {
		return lm_get_as_conference_attribute_enterprise_id(c_object_);
	}

	int ASConfAttributeWrapper::ConferenceId() const {
		return lm_get_as_conference_attribute_conference_id(c_object_);
	}

	uint32_t ASConfAttributeWrapper::Tag() const {
		return lm_get_as_conference_attribute_tag(c_object_);
	}

	int ASConfAttributeWrapper::MaxAttendees() const {
		return lm_get_as_conference_attribute_max_attendees(c_object_);
	}

	const char* ASConfAttributeWrapper::Name() const {
		return lm_get_as_conference_attribute_name(c_object_);
	}

	bool ASConfAttributeWrapper::IsFree() const {
		return !!lm_as_conference_attribute_is_free(c_object_);
	}

	bool ASConfAttributeWrapper::IsLock() const {
		return !!lm_as_conference_attribute_is_lock(c_object_);
	}

	bool ASConfAttributeWrapper::IsDisableText() const {
		return !!lm_as_conference_attribute_is_disable_text(c_object_);
	}

	bool ASConfAttributeWrapper::IsDisableRecord() const {
		return !!lm_as_conference_attribute_is_disable_record(c_object_);
	}

	bool ASConfAttributeWrapper::IsDisableBrowVideo() const {
		return !!lm_as_conference_attribute_is_disable_brower_video(c_object_);
	}
}