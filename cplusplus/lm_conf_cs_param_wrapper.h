#ifndef __lm_conf_cs_param_wrapper_h__
#define __lm_conf_cs_param_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSParamWrapper {
	public:
		CSParamWrapper();
		explicit CSParamWrapper(native_object_t c_object);
		~CSParamWrapper();

		CSParamWrapper& operator = (const CSParamWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

		int GetKeyCount(int* count) const;
		int GetKeyList(int* keys, int count) const;

		int GetKeyValue(int key, int* value) const;
		int GetKeyValue(int key, unsigned int* value) const;
		int GetKeyValue(int key, char* value, int size) const;
		int GetKeyValue(int key, int64_t* value) const;

		void SetKeyValue(int key, int value);
		void SetKeyValue(int key, unsigned int value);
		void SetKeyValue(int key, const char* value);
		void SetKeyValue(int key, int64_t value);

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_param_wrapper_h__