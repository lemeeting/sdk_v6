#ifndef __lm_conf_cs_orginfo_wrapper_h__
#define __lm_conf_cs_orginfo_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSOrgInfoWrapper {
	public:
		CSOrgInfoWrapper();
		explicit CSOrgInfoWrapper(native_object_t c_object);
		~CSOrgInfoWrapper();

		CSOrgInfoWrapper& operator = (const CSOrgInfoWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

		int GetVersionid() const;
		int GetDeleteflag() const;
		int GetOrgid() const;
		int GetOrgtype() const;
		int GetOrgstatus() const;
		int GetGroupid() const;
		int GetMaxusercount() const;

		const char* GetOrgname() const;
		const char* GetShowname() const;
		const char* GetAddress() const;
		const char* GetContacter() const;
		const char* GetPhone() const;
		const char* GetEmail() const;
		const char* GetWebsite() const;

		int64_t GetBirthday() const;
		int64_t GetCreatetime() const;
		int64_t GetModifytime() const;

		const char* GetCreator() const;
		const char* GetMender() const;

		int GetOrgflag() const;
		const char* GetSettingjson() const;
		const char* GetExtendjson() const;


		void SetOrgid(int value);
		void SetOrgtype(int value);
		void SetOrgstatus(int value);
		void SetGroupid(int value);
		void SetMaxusercount(int value);

		void SetOrgname(const char* value);
		void SetShowname(const char* value);
		void SetAddress(const char* value);
		void SetContacter(const char* value);
		void SetPhone(const char* value);
		void SetEmail(const char* value);
		void SetWebsite(const char* value);

		void SetBirthday(int64_t value);
		void SetOrgflag(int value);
		void SetExtendjson(const char* value);

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_orginfo_wrapper_h__