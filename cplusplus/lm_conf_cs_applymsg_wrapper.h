#ifndef __lm_conf_cs_applymsg_wrapper_h__
#define __lm_conf_cs_applymsg_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSApplyMsgWrapper {
	public:
		CSApplyMsgWrapper();
		explicit CSApplyMsgWrapper(native_object_t c_object);
		~CSApplyMsgWrapper();

		CSApplyMsgWrapper& operator = (const CSApplyMsgWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_applymsg_wrapper_h__