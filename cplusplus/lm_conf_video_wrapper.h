#ifndef __lm_conf_video_wrapper_h__
#define __lm_conf_video_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfVideoWrapper {
	public:
		class Observer {
		public:
			virtual void OnDisconnectVideoServer(int result) {}

			virtual void OnAddLocalPreview(DeviceIdentity id_device,
				int id_preview, uint64_t context, int error) {}

			virtual void OnVideoDeviceChanged(DeviceIdentity change_device,
				bool is_add, int devnums, DeviceIdentity main_dev_id) {}

			virtual void OnLocalVideoResolutionChanged(
				DeviceIdentity id_device, int previewindex, int width, int height) {}

			virtual void OnRemoteVideoResolutionChanged(
				const char* account, DeviceIdentity id_device,
				int previewindex, int width, int height) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		static void SetStartImage(const char* jpg_file);
		static void SetTimeoutImage(const char* jpg_file);
		static void SetRemoteDisableRemoteVideoImage(const char* jpg_file);
		static void SetLocalDisableRemoteVideoImage(const char* jpg_file);
		static void SetLocalDesktopCamearImage(const char* jpg_file);

		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int AddLocalPreview(
			DeviceIdentity id_device,
			void* windows,
			uint64_t context,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom,
			int& id_preview);

		int ConfigureLocalPreview(
			DeviceIdentity id_device,
			int id_preview,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom);

		int ChangeLocalPreviewWindow(
			DeviceIdentity id_device,
			int id_preview, void* window,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom);

		int RemoveLocalPreview(DeviceIdentity id_device, int id_preview);

		int EnableMirrorLocalPreview(DeviceIdentity id_device, int id_preview,
			bool enable, bool mirror_xaxis, bool mirror_yaxis);

		int MirrorLocalPreviewState(DeviceIdentity id_device, int id_preview,
			bool& enable, bool& mirror_xaxis, bool& mirror_yaxis);

		int RotateLocalPreview(DeviceIdentity id_device,
			int id_preview, int rotation);

		int RotateLocalPreviewState(DeviceIdentity id_device,
			int id_preview, int& rotation);

		int GetLocalPreviewResolution(
			DeviceIdentity id_device, int& width, int& height);

		int AddRemotePreview(
			const char* account,
			DeviceIdentity id_device,
			void* windows,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom,
			int& id_preview);

		int ConfigureRemotePreview(
			const char* account,
			DeviceIdentity id_device,
			int id_preview,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom);

		int ChangeRemotePreviewWindow(
			const char* account,
			DeviceIdentity id_device,
			int id_preview,
			void* window,
			const unsigned int z_order,
			const float left,
			const float top,
			const float right,
			const float bottom);

		int RemoveRemotePreview(const char* account,
			DeviceIdentity id_device, int id_preview);

		int EnableMirrorRemotePreview(const char* account,
			DeviceIdentity id_device, int id_preview,
			bool enable, bool mirror_xaxis, bool mirror_yaxis);

		int MirrorRemotePreviewState(const char* account,
			DeviceIdentity id_device, int id_preview,
			bool& enable, bool& mirror_xaxis, bool& mirror_yaxis);

		int RotateRemotePreview(const char* account,
			DeviceIdentity id_device, int id_preview, int rotation);

		int RotateRemotePreviewState(const char* account,
			DeviceIdentity id_device, int id_preview, int& rotation);

		int EnableRemotePreviewColorEnhancement(const char* account,
			DeviceIdentity id_device, bool enable);

		int RemotePreviewColorEnhancementState(const char* account,
			DeviceIdentity id_device, bool& enable);

		int EnableRemotePreview(const char* account,
			DeviceIdentity id_device, bool enable);

		int GetRemotePreviewState(const char* account,
			DeviceIdentity id_device, bool& enable);

		int GetRemotePreviewResolution(const char* account,
			DeviceIdentity id_device, int& width, int& height);

		int NumOfDevice(int& nums);
		int EnumDevices(DeviceIdentity* id_devices, int& nums);

		int GetDeviceName(DeviceIdentity id_device,
			char* name, int name_size, char* guid, int guid_size);

		int GetDeviceIdWithGuid(const char* guid,
			DeviceIdentity& id_device);

		int GetDeviceFormat(DeviceIdentity id_device,
			unsigned int *pixels, int& count);

		int OpenPropertypage(DeviceIdentity id_device,
			void* window, const char* title);

		int SwitchMobiledevicesCamera();
		int GetMobiledevicesActiveCameraId(DeviceIdentity& id_device);

		int IsCaptureing(DeviceIdentity id_device, bool& value);

		int SetVideoConfig(DeviceIdentity id_device, const LMVideoConfig& config);
		int GetVideoConfig(DeviceIdentity id_device, LMVideoConfig& config);

		int EnableDeflickering(DeviceIdentity id_device, bool enable);
		int DeflickeringState(DeviceIdentity id_device, bool& enable);

		int GetCaptureDeviceSnapshot(DeviceIdentity id_device,
			const char* file_name);

		int GetRenderSnapshot(const char* account,
			DeviceIdentity id_device, int id_preview,
			const char* file_name);

		int SetShowNameInVideo(bool enable);
		int GetShowNameInVideoState(bool& enable);

		int EnableDebugVideo(bool enable);
		int GetDebugVideoState(bool& enable);

		int GetCameraType(DeviceIdentity id_device, LMCameraType& type);

		int AddIPCamera(const LMIPCameraInfo* info,
			int channel, bool is_main_stream, const char* name);

		int RemoveIPCamera(int identity,
			int channel, bool is_main_stream);

		int AddRAWDataCamera(int identity, const char* name);
		int RemoveRAWDataCamera(int identity);
		int IncomingRAWData(
			int identity, const void* data, size_t data_len,
			int width, int height, LMRawVideoType type);

		private:
			friend void CALLBACK FuncVideoOnDisconnectVideoServerCB(int result, void* clientdata);
			friend void CALLBACK FuncVideoOnAddLocalPreviewCB(
				int id_device, int id_preview, uint64_t context, int error, void* clientdata);
			friend void CALLBACK FuncVideoOnVideoDeviceChangedCB(
				int change_device, int is_add, int devnums, int main_dev_id, void* clientdata);
			friend void CALLBACK FuncVideoOnLocalVideoResolutionChangedCB(
				int id_device, int id_preview, int width, int height, void* clientdata);
			friend void CALLBACK FuncVideoOnRemoteVideoResolutionChangedCB(
				const char* account, int id_device, int id_preview, int width, int height, void* clientdata);

			void OnDisconnectVideoServerCB(int result);
			void OnAddLocalPreviewCB(
				int id_device, int id_preview, uint64_t context, int error);
			void OnVideoDeviceChangedCB(
				int change_device, int is_add, int devnums, int main_dev_id);
			void OnLocalVideoResolutionChangedCB(
				int id_device, int id_preview, int width, int height);
			void OnRemoteVideoResolutionChangedCB(
				const char* account, int id_device, int id_preview, int width, int height);

	private:
		friend class ConfEngineWrapper;
		explicit ConfVideoWrapper(native_object_t c_object);
		~ConfVideoWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_video_wrapper_h__