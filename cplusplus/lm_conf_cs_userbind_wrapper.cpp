#include "lm_conf_cs_userbind_wrapper.h"

namespace lm {
	CSUserBindWrapper::CSUserBindWrapper() : owner_(true) {
		lm_create_cs_user_bind_instance(&c_object_);
	}

	CSUserBindWrapper::CSUserBindWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSUserBindWrapper::~CSUserBindWrapper() {
		if (owner_)
			lm_destroy_cs_user_bind_instance(c_object_);
	}

	CSUserBindWrapper& CSUserBindWrapper::operator = (const CSUserBindWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_user_bind_instance(&c_object_);
		}
		lm_copy_cs_user_bind_instance(rhs.c_object_, c_object_);
		return *this;
	}

}