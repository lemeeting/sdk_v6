#ifndef __lm_conf_cs_userinfo_wrapper_h__
#define __lm_conf_cs_userinfo_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSUserInfoWrapper {
	public:
		CSUserInfoWrapper();
		explicit CSUserInfoWrapper(native_object_t c_object);
		~CSUserInfoWrapper();

		CSUserInfoWrapper& operator = (const CSUserInfoWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

		int GetVersionid() const;
		int GetDeleteflag() const;
		int GetOrgid() const;
		const char* GetUseraccount() const;
		int GetUsertype() const;
		int GetUserstatus() const;

		const char* GetUsername() const;
		const char* GetPassword() const;
		const char* GetPhone() const;
		const char* GetEmail() const;

		int64_t GetBirthday() const;
		int64_t GetCreatetime() const;
		const char* GetSettingjson() const;
		const char* GetExtendjson() const;


		void SetOrgid(int value);
		void SetUseraccount(const char* value);
		void SetUsertype(int value);
		void SetUserstatus(int value);

		void SetUsername(const char* value);
		void SetPassword(const char* value);
		void SetPhone(const char* value);
		void SetEmail(const char* value);

		void SetBirthday(int64_t value);
		void SetCreatetime(int64_t value);

		void SetSettingjson(const char* value);
		void SetExtendjson(const char* value);

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_userinfo_wrapper_h__