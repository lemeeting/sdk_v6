#include "lm_conf_cs_confcode_wrapper.h"

namespace lm {
	CSConfCodeWrapper::CSConfCodeWrapper() : owner_(true) {
		lm_create_cs_code_instance(&c_object_);
	}

	CSConfCodeWrapper::CSConfCodeWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSConfCodeWrapper::~CSConfCodeWrapper() {
		if (owner_)
			lm_destroy_cs_code_instance(c_object_);
	}

	CSConfCodeWrapper& CSConfCodeWrapper::operator = (const CSConfCodeWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_code_instance(&c_object_);
		}
		lm_copy_cs_code_instance(rhs.c_object_, c_object_);
		return *this;
	}

}