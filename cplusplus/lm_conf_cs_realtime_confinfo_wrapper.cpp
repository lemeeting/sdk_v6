#include "lm_conf_cs_realtime_confinfo_wrapper.h"


namespace lm {
	CSRealTimeConfWrapper::CSRealTimeConfWrapper() : owner_(true) {
		lm_create_cs_real_conf_instance(&c_object_);
	}

	CSRealTimeConfWrapper::CSRealTimeConfWrapper(native_object_t c_object)
		: owner_(false), c_object_(c_object) {
	}

	CSRealTimeConfWrapper::~CSRealTimeConfWrapper() {
		if (owner_)
			lm_destroy_cs_real_conf_instance(c_object_);
	}

	CSRealTimeConfWrapper& CSRealTimeConfWrapper::operator = (const CSRealTimeConfWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_cs_real_conf_instance(&c_object_);
		}
		lm_copy_cs_real_conf_instance(rhs.c_object_, c_object_);
		return *this;
	}

}