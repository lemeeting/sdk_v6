#include "lm_conf_as_attendee_wrapper.h"
#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"

namespace lm {
	ASConfAttendeeWrapper* ASConfAttendeeWrapper::from_conf_base(const char* account) {
		native_object_t c_object;
		if (lm_conf_base_get_attendee_info(ConfEngineWrapper::get()->base_wrapper()->cobject(), account, &c_object) != 0) {
			return NULL;
		}
		return new ASConfAttendeeWrapper(c_object);
	}

	ASConfAttendeeWrapper* ASConfAttendeeWrapper::self_from_conf_base() {
		native_object_t c_object;
		if (lm_conf_base_get_self_attendee_info(ConfEngineWrapper::get()->base_wrapper()->cobject(), &c_object) != 0) {
			return NULL;
		}
		return new ASConfAttendeeWrapper(c_object);
	}

	ASConfAttendeeWrapper::ASConfAttendeeWrapper() : owner_(true) {
		lm_create_as_attendee_instance(&c_object_);
	}

	ASConfAttendeeWrapper::ASConfAttendeeWrapper(native_object_t c_object) : owner_(false),
		c_object_(c_object) {
	}

	ASConfAttendeeWrapper::~ASConfAttendeeWrapper() {
		if (owner_)
			lm_destroy_as_attendee_instance(c_object_);
	}

	ASConfAttendeeWrapper& ASConfAttendeeWrapper::operator = (const ASConfAttendeeWrapper& rhs) {
		if (this == &rhs)
			return *this;
		if (!owner_) {
			owner_ = true;
			lm_create_as_attendee_instance(&c_object_);
		}
		lm_copy_as_attendee_instance(rhs.c_object_, c_object_);
		return *this;
	}

	int ASConfAttendeeWrapper::Type() const {
		return lm_get_as_attendee_type(c_object_);
	}

	LMConferenceClientType ASConfAttendeeWrapper::LoginType() const {
		return LMConferenceClientType(lm_get_as_attendee_login_type(c_object_));
	}

	uint32_t ASConfAttendeeWrapper::OpsStatus() const {
		return static_cast<uint32_t>(lm_get_as_attendee_op_status(c_object_));
	}

	LMConferenceAdminModes ASConfAttendeeWrapper::AdminPrivilege() const {
		return LMConferenceAdminModes(lm_get_as_attendee_admin_privilege(c_object_));
	}

	int ASConfAttendeeWrapper::NumOfVoiceDevices() const {
		return lm_get_as_attendee_audio_device_nums(c_object_);
	}

	int ASConfAttendeeWrapper::NumOfVideoDevices() const {
		return lm_get_as_attendee_video_device_nums(c_object_);
	}

	void ASConfAttendeeWrapper::EnumVideoDevices(DeviceIdentity* id_devices, int* nums) const {
		lm_get_as_attendee_enum_video_devices(c_object_, reinterpret_cast<int*>(id_devices), nums);
	}

	bool ASConfAttendeeWrapper::IsEnableVideoDevice(DeviceIdentity id_device) const {
		return !!lm_get_as_attendee_video_device_state(c_object_, static_cast<int>(id_device));
	}

	LMCameraType ASConfAttendeeWrapper::GetVideoDeviceType(DeviceIdentity id_device) const {
		return LMCameraType(lm_get_as_attendee_video_device_type(c_object_, static_cast<int>(id_device)));
	}

	LMCameraSubType ASConfAttendeeWrapper::GetVideoDeviceSubType(DeviceIdentity id_device) const {
		return LMCameraSubType(lm_get_as_attendee_video_device_sub_type(c_object_, static_cast<int>(id_device)));
	}

	const char* ASConfAttendeeWrapper::GetVideoDeviceShowName(DeviceIdentity id_device) const {
		return lm_get_as_attendee_video_device_show_name(c_object_, static_cast<int>(id_device));
	}

	DeviceIdentity ASConfAttendeeWrapper::MainVideoDeviceId() const {
		return DeviceIdentity(lm_get_as_attendee_main_video_device_id(c_object_));
	}

	bool ASConfAttendeeWrapper::IsForbiddenVideoDevice() const {
		return !!lm_as_attendee_is_forbidden_video_device(c_object_);
	}

	const char* ASConfAttendeeWrapper::Account() const {
		return lm_get_as_attendee_account(c_object_);
	}

	const char* ASConfAttendeeWrapper::Name() const {
		return lm_get_as_attendee_name(c_object_);
	}

	int ASConfAttendeeWrapper::EnterpriseId() const {
		return lm_get_as_attendee_enterprise_id(c_object_);
	}

	int ASConfAttendeeWrapper::AccessServerId() const {
		return lm_get_as_attendee_server_id(c_object_);
	}

	int ASConfAttendeeWrapper::ConferenceId() const {
		return lm_get_as_attendee_conference_id(c_object_);
	}

	const char* ASConfAttendeeWrapper::MacAddress() const {
		return lm_get_as_attendee_mac_address(c_object_);
	}

	int64_t ASConfAttendeeWrapper::SigninTime() const {
		return lm_get_as_attendee_signin_time(c_object_);
	}

	bool ASConfAttendeeWrapper::IsValidVideoDevice(DeviceIdentity id_device) const {
		return !!lm_as_attendee_is_valid_video_device(c_object_, static_cast<int>(id_device));
	}

}