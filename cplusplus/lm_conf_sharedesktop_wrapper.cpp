#include "lm_conf_sharedesktop_wrapper.h"

namespace lm {
	void CALLBACK FuncShareDesktopOnDisconnectShareServerCB(int result, void* clientdata) {
		ConfShareDesktopWrapper* wrapper = reinterpret_cast<ConfShareDesktopWrapper*>(clientdata);
		wrapper->OnDisconnectShareServerCB(result);
	}

	void ConfShareDesktopWrapper::OnDisconnectShareServerCB(int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDisconnectShareServer(result);
		}
	}

	ConfShareDesktopWrapper::ConfShareDesktopWrapper(native_object_t c_object)
		: c_object_(c_object) {
		ConfShareDesktopCallback cb;
		memset(&cb, 0, sizeof(cb));
		cb.client_data_ = this;
		cb.OnDisconnectShareServer_ = FuncShareDesktopOnDisconnectShareServerCB;
		lm_conf_sharedesktop_set_callback(c_object_, &cb);
	}

	ConfShareDesktopWrapper::~ConfShareDesktopWrapper() {
	}

	void ConfShareDesktopWrapper::AddObserver(Observer* observer) {
		if (std::find(observer_list_.begin(), observer_list_.end(), observer) == observer_list_.end())
			observer_list_.push_back(observer);
	}

	void ConfShareDesktopWrapper::RemoveObserver(Observer* observer) {
		ObserverList::iterator it = std::find(observer_list_.begin(), observer_list_.end(), observer);
		if (it != observer_list_.end())
			observer_list_.erase(it);
	}

	int ConfShareDesktopWrapper::SelfStartShare() {
		return lm_conf_sharedesktop_SelfStartShare(c_object_);
	}

	int ConfShareDesktopWrapper::SelfStopShare() {
		return lm_conf_sharedesktop_SelfStopShare(c_object_);
	}

	int ConfShareDesktopWrapper::CreateView(const char* sharer, void* parent_wnd, int crBkg, void*& wnd) {
		return lm_conf_sharedesktop_CreateView(c_object_, sharer, parent_wnd, crBkg, &wnd);
	}

	int ConfShareDesktopWrapper::DestroyView(const char* sharer) {
		return lm_conf_sharedesktop_DestroyView(c_object_, sharer);
	}

	int ConfShareDesktopWrapper::SetDisplayMode(const char* sharer, LMShareDesktopDisplayModes mode) {
		return lm_conf_sharedesktop_SetDisplayMode(c_object_, sharer, mode);
	}

	int ConfShareDesktopWrapper::GetScreenResolution(int& width, int& height) {
		return lm_conf_sharedesktop_GetScreenResolution(c_object_, &width, &height);
	}

	int ConfShareDesktopWrapper::GetRemoteResolution(int& width, int& height) {
		return lm_conf_sharedesktop_GetRemoteResolution(c_object_, &width, &height);
	}

	int ConfShareDesktopWrapper::SetRemoteResolution(int width, int height) {
		return lm_conf_sharedesktop_SetRemoteResolution(c_object_, width, height);
	}
}