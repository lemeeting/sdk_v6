#ifndef __lm_conf_as_confattribute_wrapper_h__
#define __lm_conf_as_confattribute_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class ASConfAttributeWrapper {
	public:
		ASConfAttributeWrapper();
		explicit ASConfAttributeWrapper(native_object_t c_object);
		~ASConfAttributeWrapper();

		ASConfAttributeWrapper& operator = (const ASConfAttributeWrapper& rhs);

		static ASConfAttributeWrapper* from_conf_base();

		int OrgId() const;
		int ConferenceId() const;
		uint32_t Tag() const;
		int MaxAttendees() const;
		const char* Name() const;

		bool IsFree() const;
		bool IsLock() const;
		bool IsDisableText() const;
		bool IsDisableRecord() const;
		bool IsDisableBrowVideo() const;

	private:
		bool owner_;
		native_object_t c_object_;
	};
}
#endif //__lm_conf_as_confattribute_wrapper_h__