#include "lm_conf_business_wrapper.h"
#include "lm_conf_as_confattribute_wrapper.h"
#include "lm_conf_as_realtimeconfinfo_wrapper.h"
#include "lm_conf_as_attendee_wrapper.h"
#include "lm_conf_as_syncinfo_wrapper.h"

namespace lm {
	void CALLBACK FuncBusinessOnDisconnectCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnDisconnectCB(result);
	}

	void CALLBACK FuncBusinessOnEntryConferenceCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnEntryConferenceCB(result);
	}

	void CALLBACK FuncBusinessOnLeaveConferenceCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnLeaveConferenceCB(result);
	}

	void CALLBACK FuncBusinessOnGetConferenceCB(
		native_object_t conference_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnGetConferenceCB(conference_info);
	}

	void CALLBACK FuncBusinessOnUpdateConerenceCB(
		native_object_t conference_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnUpdateConerenceCB(conference_info);
	}

	void CALLBACK FuncBusinessOnRemoveConerenceCB(
		int id_conference, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRemoveConerenceCB(id_conference);
	}

	void CALLBACK FuncBusinessOnGetConferenceRealTimeInfoCB(
		native_object_t real_time_conference_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnGetConferenceRealTimeInfoCB(real_time_conference_info);
	}

	void CALLBACK FuncBusinessOnGetConferenceSyncInfoCB(
		native_object_t conference_sync_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnGetConferenceSyncInfoCB(conference_sync_info);
	}

	void CALLBACK FuncBusinessOnAddSelfAttendeeCB(
		native_object_t slef_attendee_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAddSelfAttendeeCB(slef_attendee_info);
	}

	void CALLBACK FuncBusinessOnAttendeeOnlineCB(
		native_object_t attendee_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAttendeeOnlineCB(attendee_info);
	}

	void CALLBACK FuncBusinessOnAttendeeOfflineCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAttendeeOfflineCB(account);
	}

	void CALLBACK FuncBusinessOnUpdateAttendeeCB(
		native_object_t old_attendee_info, native_object_t new_attendee_info, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnUpdateAttendeeCB(old_attendee_info, new_attendee_info);
	}

	void CALLBACK FuncBusinessOnRemoveAttendeeCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRemoveAttendeeCB(account);
	}

	void CALLBACK FuncBusinessOnAddConfAdminCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAddConfAdminCB(account);
	}

	void CALLBACK FuncBusinessOnRemoveConfAdminCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRemoveConfAdminCB(account);
	}

	void CALLBACK FuncBusinessOnAddConfDefaultAttendeeCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAddConfDefaultAttendeeCB(account);
	}

	void CALLBACK FuncBusinessOnRemoveConfDefaultAttendeeCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRemoveConfDefaultAttendeeCB(account);
	}

	void CALLBACK FuncBusinessOnApplySpeakOperCB(
		int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnApplySpeakOperCB(result, account, new_state, old_state, apply);
	}

	void CALLBACK FuncBusinessOnAccreditSpeakOperCB(
		int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAccreditSpeakOperCB(result, account, new_state, old_state, accredit);
	}

	void CALLBACK FuncBusinessOnSpeakNotifyCB(
		const char* account, int new_state, int old_state, int is_speak, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSpeakNotifyCB( account, new_state, old_state, is_speak);
	}

	void CALLBACK FuncBusinessOnApplyDataOperCB(
		int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnApplyDataOperCB(result, account, new_state, old_state, apply);
	}

	void CALLBACK FuncBusinessOnAccreditDataOperCB(
		int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAccreditDataOperCB(result, account, new_state, old_state, accredit);
	}

	void CALLBACK FuncBusinessOnDataOpNotifyCB(
		const char* account, int new_state, int old_state, int is_data_op, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnDataOpNotifyCB(account, new_state, old_state, is_data_op);
	}

	void CALLBACK FuncBusinessOnApplyDataSyncCB(
		int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnApplyDataSyncCB(result, account, new_state, old_state, apply);
	}

	void CALLBACK FuncBusinessOnAccreditDataSyncCB(
		int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAccreditDataSyncCB(result, account, new_state, old_state, accredit);
	}

	void CALLBACK FuncBusinessOnSyncOpNotifyCB(
		const char* account, int new_state, int old_state, int is_sync, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSyncOpNotifyCB(account, new_state, old_state, is_sync);
	}

	void CALLBACK FuncBusinessOnDataSyncCommandCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnDataSyncCommandCB(result);
	}
	
	void CALLBACK FuncBusinessOnDataSyncCommandNotifyCB(
		const char* syncer, const char* sync_data, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnDataSyncCommandNotifyCB(syncer, sync_data);
	}

	void CALLBACK FuncBusinessOnApplyTempAdminCB(
		int result, const char* account,
		int new_state, int old_state, int apply, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnApplyTempAdminCB(result, account, new_state, old_state, apply);
	}

	void CALLBACK FuncBusinessOnAccreditTempAdminCB(
		int result, const char* account,
		int new_state, int old_state, int accredit, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAccreditTempAdminCB(result, account, new_state, old_state, accredit);
	}

	void CALLBACK FuncBusinessOnAuthTempAdminCB(
		int result, const char* admin_psw, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAuthTempAdminCB(result, admin_psw);
	}

	void CALLBACK FuncBusinessOnTempAdminNotifyCB(const char* account,
		uint32_t new_state, uint32_t old_state, int apply, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnTempAdminNotifyCB(account, new_state, old_state, apply);
	}

	void CALLBACK FuncBusinessOnStartPreviewVideoCB(
		int result, const char* shower,
		int id_device, uint64_t context, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPreviewVideoCB(result, shower, id_device, context);
	}

	void CALLBACK FuncBusinessOnStopPreviewVideoCB(
		int result, const char* shower,
		int id_device, uint64_t context, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPreviewVideoCB(result, shower, id_device, context);
	}

	void CALLBACK FuncBusinessOnStartPreviewVideoNotifyCB(
		const char* shower, int id_device, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPreviewVideoNotifyCB(shower, id_device);
	}

	void CALLBACK FuncBusinessOnStopPreviewVideoNotifyCB(
		const char* shower, int id_device, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPreviewVideoNotifyCB(shower, id_device);
	}

	void CALLBACK FuncBusinessOnSwitchMainVideoCB(
		int result, int old_dev_id, int new_dev_id, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSwitchMainVideoCB(result, old_dev_id, new_dev_id);
	}

	void CALLBACK FuncBusinessOnSwitchMainVideoNotifyCB(
		const char* account, int old_dev_id, int new_dev_id, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSwitchMainVideoNotifyCB(account, old_dev_id, new_dev_id);
	}

	void CALLBACK FuncBusinessOnEnableVideoCB(
		int result, int enabled, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnEnableVideoCB(result, enabled);
	}

	void CALLBACK FuncBusinessOnEnableVideoNotifyCB(
		const char* account, int enabled, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnEnableVideoNotifyCB(account, enabled);
	}

	void CALLBACK FuncBusinessOnCaptureVideoStateCB(
		int result, int id_device, int enabled, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnCaptureVideoStateCB(result, id_device, enabled);
	}

	void CALLBACK FuncBusinessOnCaptureVideoStateNotifyCB(
		const char* account, int id_device, int enabled, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnCaptureVideoStateNotifyCB(account, id_device, enabled);
	}

	void CALLBACK FuncBusinessOnVideoShowNameChangedNotifyCB(
		const char* account, int id_device, const char* new_show_name, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnVideoShowNameChangedNotifyCB(account, id_device, new_show_name);
	}

	void CALLBACK FuncBusinessOnVideoDeviceChangedNotifyCB(
		const char* account, int dev_nums, int dev_state, int dev_main_id, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnVideoDeviceChangedNotifyCB(account, dev_nums, dev_state, dev_main_id);
	}

	void CALLBACK FuncBusinessOnKickoutAttendeeCB(
		int result, const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnKickoutAttendeeCB(result, account);
	}

	void CALLBACK FuncBusinessOnKickoutAttendeeNotifyCB(
		const char* oper, const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnKickoutAttendeeNotifyCB(oper, account);
	}

	void CALLBACK FuncBusinessOnUpdateAttendeeNameCB(
		int result, const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnUpdateAttendeeNameCB(result, account);
	}

	void CALLBACK FuncBusinessOnUpdateAttendeeNameNotifyCB(
		const char* account, const char* new_name, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnUpdateAttendeeNameNotifyCB(account, new_name);
	}

	void CALLBACK FuncBusinessOnRelayMsgToOneCB(
		int result, const char* receiver, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRelayMsgToOneCB(result, receiver);
	}

	void CALLBACK FuncBusinessOnRelayMsgToOneNotifyCB(
		const char* sneder, const char* receiver, const char* msg, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRelayMsgToOneNotifyCB(sneder, receiver, msg);
	}

	void CALLBACK FuncBusinessOnRelayMsgToAllCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRelayMsgToAllCB(result);
	}

	void CALLBACK FuncBusinessOnRelayMsgToAllNotifyCB(
		const char* sneder, const char* msg, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnRelayMsgToAllNotifyCB(sneder, msg);
	}

	void CALLBACK FuncBusinessOnAdminOperConfSettingCB(
		int result, int cmd, int cmd_value, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAdminOperConfSettingCB(result, cmd, cmd_value);
	}

	void CALLBACK FuncBusinessOnAdminOperConfSettingNotifyCB(
		const char* oper, int cmd, int cmd_value, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAdminOperConfSettingNotifyCB(oper, cmd, cmd_value);
	}

	void CALLBACK FuncBusinessOnSetConfPasswordCB(int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSetConfPasswordCB(result);
	}

	void CALLBACK FuncBusinessOnSetConfPasswordNotifyCB(
		const char* oper, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSetConfPasswordNotifyCB(oper);
	}

	void CALLBACK FuncBusinessOnStartDesktopShareCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartDesktopShareCB(result);
	}

	void CALLBACK FuncBusinessOnStartDesktopShareNotifyCB(
		const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartDesktopShareNotifyCB(sharer);
	}

	void CALLBACK FuncBusinessOnStopDesktopShareCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopDesktopShareCB(result);
	}

	void CALLBACK FuncBusinessOnStopDesktopShareNotifyCB(
		const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopDesktopShareNotifyCB(sharer);
	}

	void CALLBACK FuncBusinessOnStartPreviewDesktopCB(
		int result, const char* sharer, uint64_t context, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPreviewDesktopCB(result, sharer, context);
	}

	void CALLBACK FuncBusinessOnStopPreviewDesktopCB(
		int result, const char* sharer, uint64_t context, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPreviewDesktopCB(result, sharer, context);
	}

	void CALLBACK FuncBusinessOnStartPreviewDesktopNotifyCB(
		const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPreviewDesktopNotifyCB(sharer);
	}

	void CALLBACK FuncBusinessOnStopPreviewDesktopNotifyCB(
		const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPreviewDesktopNotifyCB(sharer);
	}

	void CALLBACK FuncBusinessOnAskRemoteDesktopControlCB(
		int result, const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAskRemoteDesktopControlCB(result, sharer);
	}

	void CALLBACK FuncBusinessOnAskRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAskRemoteDesktopControlNotifyCB(controller, sharer);
	}

	void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlCB(
		int result, const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAbstainRemoteDesktopControlCB(result, sharer);
	}

	void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAbstainRemoteDesktopControlNotifyCB(controller, sharer);
	}

	void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlCB(
		int result, const char* controller, int allow, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAnswerRemoteDesktopControlCB(result, controller, allow);
	}

	void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer, int allow, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAnswerRemoteDesktopControlNotifyCB(controller, sharer, allow);
	}

	void CALLBACK FuncBusinessOnAbortRemoteDesktopControlCB(
		int result, const char* controller, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAbortRemoteDesktopControlCB(result, controller);
	}

	void CALLBACK FuncBusinessOnAbortRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnAbortRemoteDesktopControlNotifyCB(controller, sharer);
	}

	void CALLBACK FuncBusinessOnLaunchSigninCB(
		int result, int type, int64_t launch_or_stop_time, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnLaunchSigninCB(result, type, launch_or_stop_time);
	}

	void CALLBACK FuncBusinessOnLaunchSigninNotifyCB(
		const char* account, int type, int64_t launch_or_stop_time, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnLaunchSigninNotifyCB(account, type, launch_or_stop_time);
	}

	void CALLBACK FuncBusinessOnSigninCB(
		int result, int is_signin, int64_t launch_time,
		int64_t signin_or_cancel_time, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSigninCB(result, is_signin, launch_time, signin_or_cancel_time);
	}

	void CALLBACK FuncBusinessOnSigninNotifyCB(
		const char* account, int is_signin, int64_t launch_time,
		int64_t signin_or_cancel_time, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnSigninNotifyCB(account, is_signin, launch_time, signin_or_cancel_time);
	}

	void CALLBACK FuncBusinessOnOperSubGroupCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnOperSubGroupCB(result);
	}

	void CALLBACK FuncBusinessOnOperSubGroupNotifyCB(
		const char* oper, int id_sub_group, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnOperSubGroupNotifyCB(oper, id_sub_group);
	}

	void CALLBACK FuncBusinessOnCancelSubGroupCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnCancelSubGroupCB(result);
	}

	void CALLBACK FuncBusinessOnCancelSubGroupNotifyCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnCancelSubGroupNotifyCB(account);
	}

	void CALLBACK FuncBusinessOnOpRemoteDesktopSharedCB(
		int result, const char* sharer, int is_start, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnOpRemoteDesktopSharedCB(result, sharer, is_start);
	}

	void CALLBACK FuncBusinessOnOpRemoteDesktopSharedNotifyCB(
		const char* oper, const char* sharer, int is_start, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnOpRemoteDesktopSharedNotifyCB(oper, sharer, is_start);
	}

	void CALLBACK FuncBusinessOnStartPlaybackCB(
		int result, const char* file_name, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPlaybackCB(result, file_name);
	}

	void CALLBACK FuncBusinessOnStartPlaybackNotifyCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartPlaybackNotifyCB(account);
	}

	void CALLBACK FuncBusinessOnStopPlaybackCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPlaybackCB(result);
	}

	void CALLBACK FuncBusinessOnStopPlaybackNotifyCB(
		const char* account, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopPlaybackNotifyCB(account);
	}

	void CALLBACK FuncBusinessOnStartMediaPlayCB(
		int result, const char* file_name, int media_flag, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartMediaPlayCB(result, file_name, media_flag);
	}

	void CALLBACK FuncBusinessOnStartMediaPlayNotifyCB(
		const char* oper, const char* file_name, int media_flag, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStartMediaPlayNotifyCB(oper, file_name, media_flag);
	}

	void CALLBACK FuncBusinessOnStopMediaPlayCB(
		int result, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopMediaPlayCB(result);
	}

	void CALLBACK FuncBusinessOnStopMediaPlayNotifyCB(
		const char* oper, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnStopMediaPlayNotifyCB(oper);
	}

	void CALLBACK FuncBusinessOnPauseMediaPlayCB(
		int result, int paused, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnPauseMediaPlayCB(result, paused);
	}

	void CALLBACK FuncBusinessOnPauseMediaPlayNotifyCB(
		const char* oper, int paused, void* clientdata) {
		ConfBusinessWrapper* wrapper = reinterpret_cast<ConfBusinessWrapper*>(clientdata);
		wrapper->OnPauseMediaPlayNotifyCB(oper, paused);
	}

	void ConfBusinessWrapper::OnDisconnectCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDisconnect(result);
		}
	}

	void ConfBusinessWrapper::OnEntryConferenceCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnEntryConference(result);
		}
	}

	void ConfBusinessWrapper::OnLeaveConferenceCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnLeaveConference(result);
		}
	}

	void ConfBusinessWrapper::OnGetConferenceCB(
		native_object_t conference_info) {
		ASConfAttributeWrapper attr(conference_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConference(&attr);
		}
	}

	void ConfBusinessWrapper::OnUpdateConerenceCB(
		native_object_t conference_info) {
		ASConfAttributeWrapper attr(conference_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateConerence(&attr);
		}
	}

	void ConfBusinessWrapper::OnRemoveConerenceCB(
		int id_conference) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveConerence(id_conference);
		}
	}

	void ConfBusinessWrapper::OnGetConferenceRealTimeInfoCB(
		native_object_t real_time_conference_info) {
		ASConfRealTimeInfoWrapper info(real_time_conference_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConferenceRealTimeInfo(&info);
		}
	}

	void ConfBusinessWrapper::OnGetConferenceSyncInfoCB(
		native_object_t conference_sync_info) {
		ASConfSyncInfoWrapper info(conference_sync_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnGetConferenceSyncInfo(&info);
		}
	}

	void ConfBusinessWrapper::OnAddSelfAttendeeCB(
		native_object_t slef_attendee_info) {
		ASConfAttendeeWrapper info(slef_attendee_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddSelfAttendee(&info);
		}
	}

	void ConfBusinessWrapper::OnAttendeeOnlineCB(
		native_object_t attendee_info) {
		ASConfAttendeeWrapper info(attendee_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAttendeeOnline(&info);
		}
	}

	void ConfBusinessWrapper::OnAttendeeOfflineCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAttendeeOffline(account);
		}
	}

	void ConfBusinessWrapper::OnUpdateAttendeeCB(
		native_object_t old_attendee_info, native_object_t new_attendee_info) {
		ASConfAttendeeWrapper old_info(old_attendee_info);
		ASConfAttendeeWrapper new_info(new_attendee_info);
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateAttendee(&old_info, &new_info);
		}
	}

	void ConfBusinessWrapper::OnRemoveAttendeeCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveAttendee(account);
		}
	}

	void ConfBusinessWrapper::OnAddConfAdminCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddConfAdmin(account);
		}
	}

	void ConfBusinessWrapper::OnRemoveConfAdminCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveConfAdmin(account);
		}
	}

	void ConfBusinessWrapper::OnAddConfDefaultAttendeeCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAddConfDefaultAttendee(account);
		}
	}

	void ConfBusinessWrapper::OnRemoveConfDefaultAttendeeCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRemoveConfDefaultAttendee(account);
		}
	}

	void ConfBusinessWrapper::OnApplySpeakOperCB(
		int result, const char* account, int new_state, int old_state, int apply) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnApplySpeakOper(result, account, new_state, old_state, !!apply);
		}
	}

	void ConfBusinessWrapper::OnAccreditSpeakOperCB(
		int result, const char* account, int new_state, int old_state, int accredit) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAccreditSpeakOper(result, account, new_state, old_state, !!accredit);
		}
	}

	void ConfBusinessWrapper::OnSpeakNotifyCB(
		const char* account, int new_state, int old_state, int is_speak) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSpeakNotify(account, new_state, old_state, !!is_speak);
		}
	}

	void ConfBusinessWrapper::OnApplyDataOperCB(
		int result, const char* account, int new_state, int old_state, int apply) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnApplyDataOper(result, account, new_state, old_state, !!apply);
		}
	}

	void ConfBusinessWrapper::OnAccreditDataOperCB(
		int result, const char* account, int new_state, int old_state, int accredit) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAccreditDataOper(result, account, new_state, old_state, !!accredit);
		}
	}

	void ConfBusinessWrapper::OnDataOpNotifyCB(
		const char* account, int new_state, int old_state, int is_data_op) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDataOpNotify(account, new_state, old_state, !!is_data_op);
		}
	}

	void ConfBusinessWrapper::OnApplyDataSyncCB(
		int result, const char* account, int new_state, int old_state, int apply) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnApplyDataSync(result, account, new_state, old_state, !!apply);
		}
	}

	void ConfBusinessWrapper::OnAccreditDataSyncCB(
		int result, const char* account, int new_state, int old_state, int accredit) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAccreditDataSync(result, account, new_state, old_state, !!accredit);
		}
	}

	void ConfBusinessWrapper::OnSyncOpNotifyCB(
		const char* account, int new_state, int old_state, int is_sync) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSyncOpNotify(account, new_state, old_state, !!is_sync);
		}
	}

	void ConfBusinessWrapper::OnDataSyncCommandCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDataSyncCommand(result);
		}
	}

	void ConfBusinessWrapper::OnDataSyncCommandNotifyCB(
		const char* syncer, const char* sync_data) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnDataSyncCommandNotify(syncer, sync_data);
		}
	}

	void ConfBusinessWrapper::OnApplyTempAdminCB(
		int result, const char* account,
		int new_state, int old_state, int apply) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnApplyTempAdmin(result, account, new_state, old_state, !!apply);
		}
	}

	void ConfBusinessWrapper::OnAccreditTempAdminCB(
		int result, const char* account,
		int new_state, int old_state, int accredit) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAccreditTempAdmin(result, account, new_state, old_state, !!accredit);
		}
	}

	void ConfBusinessWrapper::OnAuthTempAdminCB(
		int result, const char* admin_psw) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAuthTempAdmin(result, admin_psw);
		}
	}

	void ConfBusinessWrapper::OnTempAdminNotifyCB(const char* account,
		uint32_t new_state, uint32_t old_state, int apply) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnTempAdminNotify(account, new_state, old_state, !!apply);
		}
	}

	void ConfBusinessWrapper::OnStartPreviewVideoCB(
		int result, const char* shower,
		int id_device, uint64_t context) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPreviewVideo(result, shower, id_device, context);
		}
	}

	void ConfBusinessWrapper::OnStopPreviewVideoCB(
		int result, const char* shower,
		int id_device, uint64_t context) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPreviewVideo(result, shower, id_device, context);
		}
	}

	void ConfBusinessWrapper::OnStartPreviewVideoNotifyCB(
		const char* shower, int id_device) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPreviewVideoNotify(shower, id_device);
		}
	}

	void ConfBusinessWrapper::OnStopPreviewVideoNotifyCB(
		const char* shower, int id_device) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPreviewVideoNotify(shower, id_device);
		}
	}

	void ConfBusinessWrapper::OnSwitchMainVideoCB(
		int result, int old_dev_id, int new_dev_id) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSwitchMainVideo(result, old_dev_id, new_dev_id);
		}
	}

	void ConfBusinessWrapper::OnSwitchMainVideoNotifyCB(
		const char* account, int old_dev_id, int new_dev_id) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSwitchMainVideoNotify(account, old_dev_id, new_dev_id);
		}
	}

	void ConfBusinessWrapper::OnEnableVideoCB(
		int result, int enabled) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnEnableVideo(result, !!enabled);
		}
	}

	void ConfBusinessWrapper::OnEnableVideoNotifyCB(
		const char* account, int enabled) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnEnableVideoNotify(account, !!enabled);
		}
	}

	void ConfBusinessWrapper::OnCaptureVideoStateCB(
		int result, int id_device, int enabled) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnCaptureVideoState(result, id_device, !!enabled);
		}
	}

	void ConfBusinessWrapper::OnCaptureVideoStateNotifyCB(
		const char* account, int id_device, int enabled) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnCaptureVideoStateNotify(account, id_device, !!enabled);
		}
	}

	void ConfBusinessWrapper::OnVideoShowNameChangedNotifyCB(
		const char* account, int id_device, const char* new_show_name) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnVideoShowNameChangedNotify(account, id_device, new_show_name);
		}
	}

	void ConfBusinessWrapper::OnVideoDeviceChangedNotifyCB(
		const char* account, int dev_nums, int dev_state, int dev_main_id) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnVideoDeviceChangedNotify(account, dev_nums, dev_state, dev_main_id);
		}
	}

	void ConfBusinessWrapper::OnKickoutAttendeeCB(
		int result, const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnKickoutAttendee(result, account);
		}
	}

	void ConfBusinessWrapper::OnKickoutAttendeeNotifyCB(
		const char* oper, const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnKickoutAttendeeNotify(oper, account);
		}
	}

	void ConfBusinessWrapper::OnUpdateAttendeeNameCB(
		int result, const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateAttendeeName(result, account);
		}
	}

	void ConfBusinessWrapper::OnUpdateAttendeeNameNotifyCB(
		const char* account, const char* new_name) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnUpdateAttendeeNameNotify(account, new_name);
		}
	}

	void ConfBusinessWrapper::OnRelayMsgToOneCB(
		int result, const char* receiver) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRelayMsgToOne(result, receiver);
		}
	}

	void ConfBusinessWrapper::OnRelayMsgToOneNotifyCB(
		const char* sneder, const char* receiver, const char* msg) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRelayMsgToOneNotify(sneder, receiver, msg);
		}
	}

	void ConfBusinessWrapper::OnRelayMsgToAllCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRelayMsgToAll(result);
		}
	}

	void ConfBusinessWrapper::OnRelayMsgToAllNotifyCB(
		const char* sneder, const char* msg) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnRelayMsgToAllNotify(sneder, msg);
		}
	}

	void ConfBusinessWrapper::OnAdminOperConfSettingCB(
		int result, int cmd, int cmd_value) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAdminOperConfSetting(result, cmd, cmd_value);
		}
	}

	void ConfBusinessWrapper::OnAdminOperConfSettingNotifyCB(
		const char* oper, int cmd, int cmd_value) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAdminOperConfSettingNotify(oper, cmd, cmd_value);
		}
	}

	void ConfBusinessWrapper::OnSetConfPasswordCB(int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSetConfPassword(result);
		}
	}

	void ConfBusinessWrapper::OnSetConfPasswordNotifyCB(
		const char* oper) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSetConfPasswordNotify(oper);
		}
	}

	void ConfBusinessWrapper::OnStartDesktopShareCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartDesktopShare(result);
		}
	}

	void ConfBusinessWrapper::OnStartDesktopShareNotifyCB(
		const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartDesktopShareNotify(sharer);
		}
	}

	void ConfBusinessWrapper::OnStopDesktopShareCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopDesktopShare(result);
		}
	}

	void ConfBusinessWrapper::OnStopDesktopShareNotifyCB(
		const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopDesktopShareNotify(sharer);
		}
	}

	void ConfBusinessWrapper::OnStartPreviewDesktopCB(
		int result, const char* sharer, uint64_t context) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPreviewDesktop(result, sharer, context);
		}
	}

	void ConfBusinessWrapper::OnStopPreviewDesktopCB(
		int result, const char* sharer, uint64_t context) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPreviewDesktop(result, sharer, context);
		}
	}

	void ConfBusinessWrapper::OnStartPreviewDesktopNotifyCB(
		const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPreviewDesktopNotify(sharer);
		}
	}

	void ConfBusinessWrapper::OnStopPreviewDesktopNotifyCB(
		const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPreviewDesktopNotify(sharer);
		}
	}

	void ConfBusinessWrapper::OnAskRemoteDesktopControlCB(
		int result, const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAskRemoteDesktopControl(result, sharer);
		}
	}

	void ConfBusinessWrapper::OnAskRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAskRemoteDesktopControlNotify(controller, sharer);
		}
	}

	void ConfBusinessWrapper::OnAbstainRemoteDesktopControlCB(
		int result, const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAbstainRemoteDesktopControl(result, sharer);
		}
	}

	void ConfBusinessWrapper::OnAbstainRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAbstainRemoteDesktopControlNotify(controller, sharer);
		}
	}

	void ConfBusinessWrapper::OnAnswerRemoteDesktopControlCB(
		int result, const char* controller, int allow) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAnswerRemoteDesktopControl(result, controller, !!allow);
		}
	}

	void ConfBusinessWrapper::OnAnswerRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer, int allow) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAnswerRemoteDesktopControlNotify(controller, sharer, !!allow);
		}
	}

	void ConfBusinessWrapper::OnAbortRemoteDesktopControlCB(
		int result, const char* controller) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAbortRemoteDesktopControl(result, controller);
		}
	}

	void ConfBusinessWrapper::OnAbortRemoteDesktopControlNotifyCB(
		const char* controller, const char* sharer) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnAbortRemoteDesktopControlNotify(controller, sharer);
		}
	}

	void ConfBusinessWrapper::OnLaunchSigninCB(
		int result, int type, int64_t launch_or_stop_time) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnLaunchSignin(result, LMConferenceSigninType(type), launch_or_stop_time);
		}
	}

	void ConfBusinessWrapper::OnLaunchSigninNotifyCB(
		const char* oper, int type, int64_t launch_or_stop_time) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnLaunchSigninNotify(oper, LMConferenceSigninType(type), launch_or_stop_time);
		}
	}

	void ConfBusinessWrapper::OnSigninCB(
		int result, int is_signin, int64_t launch_time,
		int64_t signin_or_cancel_time) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSignin(result, !!is_signin, launch_time, signin_or_cancel_time);
		}
	}

	void ConfBusinessWrapper::OnSigninNotifyCB(
		const char* account, int is_signin, int64_t launch_time,
		int64_t signin_or_cancel_time) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnSigninNotify(account, !!is_signin, launch_time, signin_or_cancel_time);
		}
	}

	void ConfBusinessWrapper::OnOperSubGroupCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnOperSubGroup(result);
		}
	}

	void ConfBusinessWrapper::OnOperSubGroupNotifyCB(
		const char* oper, int id_sub_group) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnOperSubGroupNotify(oper, id_sub_group);
		}
	}

	void ConfBusinessWrapper::OnCancelSubGroupCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnCancelSubGroup(result);
		}
	}

	void ConfBusinessWrapper::OnCancelSubGroupNotifyCB(
		const char* oper) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnCancelSubGroupNotify(oper);
		}
	}

	void ConfBusinessWrapper::OnOpRemoteDesktopSharedCB(
		int result, const char* sharer, int is_start) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnOpRemoteDesktopShared(result, sharer, !!is_start);
		}
	}

	void ConfBusinessWrapper::OnOpRemoteDesktopSharedNotifyCB(
		const char* oper, const char* sharer, int is_start) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnOpRemoteDesktopSharedNotify(oper, sharer, !!is_start);
		}
	}

	void ConfBusinessWrapper::OnStartPlaybackCB(
		int result, const char* file_name) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPlayback(result, file_name);
		}
	}

	void ConfBusinessWrapper::OnStartPlaybackNotifyCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartPlaybackNotify(account);
		}
	}

	void ConfBusinessWrapper::OnStopPlaybackCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPlayback(result);
		}
	}

	void ConfBusinessWrapper::OnStopPlaybackNotifyCB(
		const char* account) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopPlaybackNotify(account);
		}
	}

	void ConfBusinessWrapper::OnStartMediaPlayCB(
		int result, const char* file_name, int media_flag) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartMediaPlay(result, file_name, media_flag);
		}
	}

	void ConfBusinessWrapper::OnStartMediaPlayNotifyCB(
		const char* oper, const char* file_name, int media_flag) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStartMediaPlayNotify(oper, file_name, media_flag);
		}
	}

	void ConfBusinessWrapper::OnStopMediaPlayCB(
		int result) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopMediaPlay(result);
		}
	}

	void ConfBusinessWrapper::OnStopMediaPlayNotifyCB(
		const char* oper) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnStopMediaPlayNotify(oper);
		}
	}

	void ConfBusinessWrapper::OnPauseMediaPlayCB(
		int result, int paused) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnPauseMediaPlay(result, !!paused);
		}
	}

	void ConfBusinessWrapper::OnPauseMediaPlayNotifyCB(
		const char* oper, int paused) {
		for (ObserverList::iterator it = observer_list_.begin(); it != observer_list_.end(); it++) {
			(*it)->OnPauseMediaPlayNotify(oper, !!paused);
		}
	}

	ConfBusinessWrapper::ConfBusinessWrapper(native_object_t c_object)
		: c_object_(c_object) {
		ConfBusinessCallback cb;
		memset(&cb, 0, sizeof(cb));
		cb.client_data_ = this;
		cb.OnDisconnect_ = FuncBusinessOnDisconnectCB;
		cb.OnEntryConference_ = FuncBusinessOnEntryConferenceCB;
		cb.OnLeaveConference_ = FuncBusinessOnLeaveConferenceCB;
		cb.OnGetConference_ = FuncBusinessOnGetConferenceCB;
		cb.OnUpdateConerence_ = FuncBusinessOnUpdateConerenceCB;
		cb.OnRemoveConerence_ = FuncBusinessOnRemoveConerenceCB;
		cb.OnGetConferenceRealTimeInfo_ = FuncBusinessOnGetConferenceRealTimeInfoCB;
		cb.OnGetConferenceSyncInfo_ = FuncBusinessOnGetConferenceSyncInfoCB;
		cb.OnAddSelfAttendee_ = FuncBusinessOnAddSelfAttendeeCB;
		cb.OnAttendeeOnline_ = FuncBusinessOnAttendeeOnlineCB;
		cb.OnAttendeeOffline_ = FuncBusinessOnAttendeeOfflineCB;
		cb.OnUpdateAttendee_ = FuncBusinessOnUpdateAttendeeCB;
		cb.OnRemoveAttendee_ = FuncBusinessOnRemoveAttendeeCB;
		cb.OnAddConfAdmin_ = FuncBusinessOnAddConfAdminCB;
		cb.OnRemoveConfAdmin_ = FuncBusinessOnRemoveConfAdminCB;
		cb.OnAddConfDefaultAttendee_ = FuncBusinessOnAddConfDefaultAttendeeCB;
		cb.OnRemoveConfDefaultAttendee_ = FuncBusinessOnRemoveConfDefaultAttendeeCB;
		cb.OnApplySpeakOper_ = FuncBusinessOnApplySpeakOperCB;
		cb.OnAccreditSpeakOper_ = FuncBusinessOnAccreditSpeakOperCB;
		cb.OnSpeakNotify_ = FuncBusinessOnSpeakNotifyCB;
		cb.OnApplyDataOper_ = FuncBusinessOnApplyDataOperCB;
		cb.OnAccreditDataOper_ = FuncBusinessOnAccreditDataOperCB;
		cb.OnDataOpNotify_ = FuncBusinessOnDataOpNotifyCB;
		cb.OnApplyDataSync_ = FuncBusinessOnApplyDataSyncCB;
		cb.OnAccreditDataSync_ = FuncBusinessOnAccreditDataSyncCB;
		cb.OnSyncOpNotify_ = FuncBusinessOnSyncOpNotifyCB;
		cb.OnDataSyncCommand_ = FuncBusinessOnDataSyncCommandCB;
		cb.OnDataSyncCommandNotify_ = FuncBusinessOnDataSyncCommandNotifyCB;
		cb.OnApplyTempAdmin_ = FuncBusinessOnApplyTempAdminCB;
		cb.OnAccreditTempAdmin_ = FuncBusinessOnAccreditTempAdminCB;
		cb.OnAuthTempAdmin_ = FuncBusinessOnAuthTempAdminCB;
		cb.OnTempAdminNotify_ = FuncBusinessOnTempAdminNotifyCB;
		cb.OnStartPreviewVideo_ = FuncBusinessOnStartPreviewVideoCB;
		cb.OnStopPreviewVideo_ = FuncBusinessOnStopPreviewVideoCB;
		cb.OnStartPreviewVideoNotify_ = FuncBusinessOnStartPreviewVideoNotifyCB;
		cb.OnStopPreviewVideoNotify_ = FuncBusinessOnStopPreviewVideoNotifyCB;
		cb.OnSwitchMainVideo_ = FuncBusinessOnSwitchMainVideoCB;
		cb.OnSwitchMainVideoNotify_ = FuncBusinessOnSwitchMainVideoNotifyCB;
		cb.OnEnableVideo_ = FuncBusinessOnEnableVideoCB;
		cb.OnEnableVideoNotify_ = FuncBusinessOnEnableVideoNotifyCB;
		cb.OnCaptureVideoState_ = FuncBusinessOnCaptureVideoStateCB;
		cb.OnCaptureVideoStateNotify_ = FuncBusinessOnCaptureVideoStateNotifyCB;
		cb.OnVideoDeviceChangedNotify_ = FuncBusinessOnVideoDeviceChangedNotifyCB;
		cb.OnVideoShowNameChangedNotify_ = FuncBusinessOnVideoShowNameChangedNotifyCB;
		cb.OnKickoutAttendee_ = FuncBusinessOnKickoutAttendeeCB;
		cb.OnKickoutAttendeeNotify_ = FuncBusinessOnKickoutAttendeeNotifyCB;
		cb.OnUpdateAttendeeName_ = FuncBusinessOnUpdateAttendeeNameCB;
		cb.OnUpdateAttendeeNameNotify_ = FuncBusinessOnUpdateAttendeeNameNotifyCB;
		cb.OnRelayMsgToOne_ = FuncBusinessOnRelayMsgToOneCB;
		cb.OnRelayMsgToOneNotify_ = FuncBusinessOnRelayMsgToOneNotifyCB;
		cb.OnRelayMsgToAll_ = FuncBusinessOnRelayMsgToAllCB;
		cb.OnRelayMsgToAllNotify_ = FuncBusinessOnRelayMsgToAllNotifyCB;
		cb.OnAdminOperConfSetting_ = FuncBusinessOnAdminOperConfSettingCB;
		cb.OnAdminOperConfSettingNotify_ = FuncBusinessOnAdminOperConfSettingNotifyCB;
		cb.OnSetConfPassword_ = FuncBusinessOnSetConfPasswordCB;
		cb.OnSetConfPasswordNotify_ = FuncBusinessOnSetConfPasswordNotifyCB;
		cb.OnStartDesktopShare_ = FuncBusinessOnStartDesktopShareCB;
		cb.OnStartDesktopShareNotify_ = FuncBusinessOnStartDesktopShareNotifyCB;
		cb.OnStopDesktopShare_ = FuncBusinessOnStopDesktopShareCB;
		cb.OnStopDesktopShareNotify_ = FuncBusinessOnStopDesktopShareNotifyCB;
		cb.OnStartPreviewDesktop_ = FuncBusinessOnStartPreviewDesktopCB;
		cb.OnStopPreviewDesktop_ = FuncBusinessOnStopPreviewDesktopCB;
		cb.OnStartPreviewDesktopNotify_ = FuncBusinessOnStartPreviewDesktopNotifyCB;
		cb.OnStopPreviewDesktopNotify_ = FuncBusinessOnStopPreviewDesktopNotifyCB;
		cb.OnAskRemoteDesktopControl_ = FuncBusinessOnAskRemoteDesktopControlCB;
		cb.OnAskRemoteDesktopControlNotify_ = FuncBusinessOnAskRemoteDesktopControlNotifyCB;
		cb.OnAbstainRemoteDesktopControl_ = FuncBusinessOnAbstainRemoteDesktopControlCB;
		cb.OnAbstainRemoteDesktopControlNotify_ = FuncBusinessOnAbstainRemoteDesktopControlNotifyCB;
		cb.OnAnswerRemoteDesktopControl_ = FuncBusinessOnAnswerRemoteDesktopControlCB;
		cb.OnAnswerRemoteDesktopControlNotify_ = FuncBusinessOnAnswerRemoteDesktopControlNotifyCB;
		cb.OnAbortRemoteDesktopControl_ = FuncBusinessOnAbortRemoteDesktopControlCB;
		cb.OnAbortRemoteDesktopControlNotify_ = FuncBusinessOnAbortRemoteDesktopControlNotifyCB;
		cb.OnLaunchSignin_ = FuncBusinessOnLaunchSigninCB;
		cb.OnLaunchSigninNotify_ = FuncBusinessOnLaunchSigninNotifyCB;
		cb.OnSignin_ = FuncBusinessOnSigninCB;
		cb.OnSigninNotify_ = FuncBusinessOnSigninNotifyCB;
		cb.OnOperSubGroup_ = FuncBusinessOnOperSubGroupCB;
		cb.OnOperSubGroupNotify_ = FuncBusinessOnOperSubGroupNotifyCB;
		cb.OnCancelSubGroup_ = FuncBusinessOnCancelSubGroupCB;
		cb.OnCancelSubGroupNotify_ = FuncBusinessOnCancelSubGroupNotifyCB;
		cb.OnOpRemoteDesktopShared_ = FuncBusinessOnOpRemoteDesktopSharedCB;
		cb.OnOpRemoteDesktopSharedNotify_ = FuncBusinessOnOpRemoteDesktopSharedNotifyCB;
		cb.OnStartPlayback_ = FuncBusinessOnStartPlaybackCB;
		cb.OnStartPlaybackNotify_ = FuncBusinessOnStartPlaybackNotifyCB;
		cb.OnStopPlayback_ = FuncBusinessOnStopPlaybackCB;
		cb.OnStopPlaybackNotify_ = FuncBusinessOnStopPlaybackNotifyCB;
		cb.OnStartMediaPlay_ = FuncBusinessOnStartMediaPlayCB;
		cb.OnStartMediaPlayNotify_ = FuncBusinessOnStartMediaPlayNotifyCB;
		cb.OnStopMediaPlay_ = FuncBusinessOnStopMediaPlayCB;
		cb.OnStopMediaPlayNotify_ = FuncBusinessOnStopMediaPlayNotifyCB;
		cb.OnPauseMediaPlay_ = FuncBusinessOnPauseMediaPlayCB;
		cb.OnPauseMediaPlayNotify_ = FuncBusinessOnPauseMediaPlayNotifyCB;

		lm_conf_business_set_observer(c_object_, &cb);
	}

	ConfBusinessWrapper::~ConfBusinessWrapper() {
	}

	void ConfBusinessWrapper::AddObserver(Observer* observer) {
		if (std::find(observer_list_.begin(), observer_list_.end(), observer) == observer_list_.end())
			observer_list_.push_back(observer);
	}

	void ConfBusinessWrapper::RemoveObserver(Observer* observer) {
		ObserverList::iterator it = std::find(observer_list_.begin(), observer_list_.end(), observer);
		if (it != observer_list_.end())
			observer_list_.erase(it);
	}

	int ConfBusinessWrapper::EntryConference(const char* json_address,
		int id_conference, uint32_t conf_tag, const char* conf_psw, bool is_md5,
		const char* account, int id_org, const char* name) {
		return lm_conf_business_EntryConference(c_object_,
			json_address, id_conference, conf_tag, conf_psw, is_md5 ? 1 : 0, account, id_org, name);
	}

	int ConfBusinessWrapper::LeaveConference() {
		return lm_conf_business_LeaveConference(c_object_);
	}

	int ConfBusinessWrapper::ApplySpeak(bool apply) {
		return lm_conf_business_ApplySpeak(c_object_, apply ? 1 : 0);
	}

	int ConfBusinessWrapper::AccreditSpeak(const char* account, bool accredit) {
		return lm_conf_business_AccreditSpeak(c_object_, account, accredit ? 1 : 0);
	}

	int ConfBusinessWrapper::ApplyDataOper(bool apply) {
		return lm_conf_business_ApplyDataOper(c_object_, apply ? 1 : 0);
	}

	int ConfBusinessWrapper::AccreditDataOper(const char* account, bool accredit) {
		return lm_conf_business_AccreditDataOper(c_object_, account, accredit ? 1 : 0);
	}

	int ConfBusinessWrapper::ApplyDataSync(bool apply) {
		return lm_conf_business_ApplyDataSync(c_object_, apply ? 1 : 0);
	}

	int ConfBusinessWrapper::AccreditDataSync(const char* account, bool accredit) {
		return lm_conf_business_AccreditDataSync(c_object_, account, accredit ? 1 : 0);
	}

	int ConfBusinessWrapper::DataSyncCommand(const char* command) {
		return lm_conf_business_DataSyncCommand(c_object_, command);
	}

	int ConfBusinessWrapper::ApplyTempAdmin(bool apply) {
		return lm_conf_business_ApplyTempAdmin(c_object_, apply ? 1 : 0);
	}

	int ConfBusinessWrapper::AccreditTempAdmin(const char* account, bool accredit) {
		return lm_conf_business_AccreditTempAdmin(c_object_, account, accredit ? 1 : 0);
	}

	int ConfBusinessWrapper::AuthTempAdmin(const char* admin_psw) {
		return lm_conf_business_AuthTempAdmin(c_object_, admin_psw);
	}

	int ConfBusinessWrapper::StartPreviewVideo(const char* shower, DeviceIdentity id_device, uint64_t context) {
		return lm_conf_business_StartPreviewVideo(c_object_, shower, id_device, context);
	}

	int ConfBusinessWrapper::StopPreviewVideo(const char* shower, DeviceIdentity id_device, uint64_t context) {
		return lm_conf_business_StopPreviewVideo(c_object_, shower, id_device, context);
	}

	int ConfBusinessWrapper::SwitchMainVideo(DeviceIdentity id_device) {
		return lm_conf_business_SwitchMainVideo(c_object_, id_device);
	}

	int ConfBusinessWrapper::EnableVideo(bool enabled) {
		return lm_conf_business_EnableVideo(c_object_, enabled ? 1 : 0);
	}

	int ConfBusinessWrapper::CaptureVideoState(DeviceIdentity id_device, bool enabled) {
		return lm_conf_business_CaptureVideoState(c_object_, id_device, enabled ? 1 : 0);
	}

	int ConfBusinessWrapper::UpdateVideoShowName(DeviceIdentity id_device, const char* name) {
		return lm_conf_business_UpdateVideoShowName(c_object_, id_device, name);
	}

	int ConfBusinessWrapper::KickOutAttendee(const char* account) {
		return lm_conf_business_KickOutAttendee(c_object_, account);
	}

	int ConfBusinessWrapper::UpdataAttendeeName(const char* name) {
		return lm_conf_business_UpdataAttendeeName(c_object_, name);
	}

	int ConfBusinessWrapper::RelayMsgToOne(const char* receiver, const char* msg) {
		return lm_conf_business_RelayMsgToOne(c_object_, receiver, msg);
	}

	int ConfBusinessWrapper::RelayMsgToAll(const char* msg) {
		return lm_conf_business_RelayMsgToAll(c_object_, msg);
	}

	int ConfBusinessWrapper::AdminOperConfSetting(int cmd, int cmd_value) {
		return lm_conf_business_AdminOperConfSetting(c_object_, cmd, cmd_value);
	}

	int ConfBusinessWrapper::SetConfPassword(const char* psw) {
		return lm_conf_business_SetConfPassword(c_object_, psw);
	}

	int ConfBusinessWrapper::StartDesktopShare() {
		return lm_conf_business_StartDesktopShare(c_object_);
	}

	int ConfBusinessWrapper::StopDesktopShare() {
		return lm_conf_business_StopDesktopShare(c_object_);
	}

	int ConfBusinessWrapper::StartPreviewDesktop(const char* sharer, uint64_t context) {
		return lm_conf_business_StartPreviewDesktop(c_object_, sharer, context);
	}

	int ConfBusinessWrapper::StopPreviewDesktop(const char* sharer, uint64_t context) {
		return lm_conf_business_StopPreviewDesktop(c_object_, sharer, context);
	}

	int ConfBusinessWrapper::AskRemoteDesktopControl(const char* sharer) {
		return lm_conf_business_AskRemoteDesktopControl(c_object_, sharer);
	}

	int ConfBusinessWrapper::AbstainRemoteDesktopControl(const char* sharer) {
		return lm_conf_business_AbstainRemoteDesktopControl(c_object_, sharer);
	}

	int ConfBusinessWrapper::AnswerRemoteDesktopControl(const char* controller, bool allow) {
		return lm_conf_business_AnswerRemoteDesktopControl(c_object_, controller, allow ? 1 : 0);
	}

	int ConfBusinessWrapper::AbortRemoteDesktopControl(const char* controller) {
		return lm_conf_business_AbortRemoteDesktopControl(c_object_, controller);
	}

	int ConfBusinessWrapper::LaunchSignin(LMConferenceSigninType signin_type) {
		return lm_conf_business_LaunchSignin(c_object_, signin_type);
	}

	int ConfBusinessWrapper::Signin(bool is_signin) {
		return lm_conf_business_Signin(c_object_, is_signin ? 1 : 0);
	}

	int ConfBusinessWrapper::OperSubGroup(const char** accounts, int count, int id_subgroup) {
		return lm_conf_business_OperSubGroup(c_object_, accounts, count, id_subgroup);
	}

	int ConfBusinessWrapper::CancelSubGroup() {
		return lm_conf_business_CancelSubGroup(c_object_);
	}

	int ConfBusinessWrapper::OpRemoteDesktopShared(const char* sharer, bool is_start) {
		return lm_conf_business_OpRemoteDesktopShared(c_object_, sharer, is_start ? 1 : 0);
	}

	int ConfBusinessWrapper::StartPlayback(const char* file_name) {
		return lm_conf_business_StartPlayback(c_object_, file_name);
	}

	int ConfBusinessWrapper::StopPlayback() {
		return lm_conf_business_StopPlayback(c_object_);
	}

	int ConfBusinessWrapper::StartMediaPlay(const char* file_name, int media_flag) {
		return lm_conf_business_StartMediaPlay(c_object_, file_name, media_flag);
	}

	int ConfBusinessWrapper::StopMediaPlay() {
		return lm_conf_business_StopMediaPlay(c_object_);
	}

	int ConfBusinessWrapper::PauseMediaPlay(bool paused) {
		return lm_conf_business_PauseMediaPlay(c_object_, paused ? 1 : 0);
	}

}