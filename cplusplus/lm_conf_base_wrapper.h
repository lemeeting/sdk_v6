#ifndef __lm_conf_base_wrapper_h__
#define __lm_conf_base_wrapper_h__
#include "conf_wrapper_interface.h"
#include <string>

namespace lm {
	class ConfBaseWrapper {
	public:
		static std::string ErrorToString(int error);
		static void DefaultInitOptions(LMInitOptions* options);

		bool Init();
		bool InitWithOptions(const LMInitOptions& options);
		int Terminate();

		void SetUseTcp(bool is_use_tcp);
		bool IsUseTcp();

		void SetProxy(const LMProxyOptions& options);
		void GetProxy(LMProxyOptions* options);

		//	default 6, min=1, max=10
		void SetScreenCopyFps(int fps);
		int GetScreenCopyFps();

		//	default false
		void EnableScreenCopyLayerWindow(bool enable);
		bool IsEnableScreenCopyLayerWindow();

		void EnableVirtualDesktopCamera(bool enable);
		bool IsEnableVirtualDesktopCamera();

		void SetMainVideoDevice(const char* guid_utf8);

		int GetPerfMon(LMPerMonitor* perfmon);

		void* CreateVideoPreviewWindows(float originX, float originY, float width, float height);

		void DeleteVideoPreviewWindows(void* windows);

		native_object_t cobject() const { return c_object_; }

	private:
		friend class ConfEngineWrapper;
		explicit ConfBaseWrapper(native_object_t c_object);
		~ConfBaseWrapper();

		native_object_t c_object_;
	};
}

#endif //__lm_conf_base_wrapper_h__