#ifndef __lm_conf_playback_wrapper_h__
#define __lm_conf_playback_wrapper_h__
#include "conf_wrapper_interface.h"
#include <list>

namespace lm {
	class ConfPlaybackWrapper {
	public:
		class Observer {
		public:
			virtual void OnPlaybackOpen(int error) {}
			virtual void OnPlaybackPlay(int error) {}
			virtual void OnPlaybackPause(int error) {}
			virtual void OnPlaybackContinue(int error) {};
			virtual void OnPlaybackComplete() {}

			virtual void OnRepairBegin(int error) {}
			virtual void OnRepairEnd(int error) {}
			virtual void OnRepairProgress(int64_t currpos, int64_t duration) {}

		protected:
			Observer() {}
			virtual ~Observer() {}
		};

	public:
		static bool IsVaildPlaybackFile(const char* file_name, bool& needRepairIfValid);

		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

		int StartPlayback(const char* file_name);
		int PlayPlayback(const char** playaccounts, int count);
		int PausePlayback();
		int ContinuePlayback();
		int StopPlayback();

		int StartRepair(const char* file_name);
		int StopRepair();

		bool IsPlaybacking();
		bool IsRepairing();

		int GetRecSessionCount(LMRecSessionType type);
		int EnumRecSessionInfo(LMRecSessionType type, LMRecSessionInfo* info, int& count);

	private:
		friend void CALLBACK FuncPlaybackOnPlaybackOpenCB(
			int result, void* clientdata);
		friend void CALLBACK FuncPlaybackOnPlaybackPlayCB(
			int result, void* clientdata);
		friend void CALLBACK FuncPlaybackOnPlaybackPauseCB(
			int result, void* clientdata);
		friend void CALLBACK FuncPlaybackOnPlaybackContinueCB(
			int result, void* clientdata);
		friend void CALLBACK FuncPlaybackOnPlaybackCompleteCB(
			void* clientdata);
		friend void CALLBACK FuncPlaybackOnRepairBeginCB(
			int error, void* clientdata);
		friend void CALLBACK FuncPlaybackOnRepairEndCB(
			int error, void* clientdata);
		friend void CALLBACK FuncPlaybackOnRepairProgressCB(
			int64_t currpos, int64_t duration, void* clientdata);

		void OnPlaybackOpenCB(int result);
		void OnPlaybackPlayCB(int result);
		void OnPlaybackPauseCB(int result);
		void OnPlaybackContinueCB(int result);
		void OnPlaybackCompleteCB();
		void OnRepairBeginCB(int error);
		void OnRepairEndCB(int error);
		void OnRepairProgressCB(int64_t currpos, int64_t duration);

	private:
		friend class ConfEngineWrapper;
		explicit ConfPlaybackWrapper(native_object_t c_object);
		~ConfPlaybackWrapper();

		native_object_t c_object_;

		typedef std::list<Observer*> ObserverList;
		ObserverList observer_list_;
	};
}

#endif //__lm_conf_playback_wrapper_h__