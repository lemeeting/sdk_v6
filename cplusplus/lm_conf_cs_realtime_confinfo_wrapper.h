#ifndef __lm_conf_cs_realtime_confinfo_wrapper_h__
#define __lm_conf_cs_realtime_confinfo_wrapper_h__
#include "conf_wrapper_interface.h"

namespace lm {
	class CSRealTimeConfWrapper {
	public:
		CSRealTimeConfWrapper();
		explicit CSRealTimeConfWrapper(native_object_t c_object);
		~CSRealTimeConfWrapper();

		CSRealTimeConfWrapper& operator = (const CSRealTimeConfWrapper& rhs);

		native_object_t cobject() const { return c_object_; }

	private:
		bool owner_;
		native_object_t c_object_;
	};
}

#endif //__lm_conf_cs_realtime_confinfo_wrapper_h__