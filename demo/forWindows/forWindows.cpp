// forWindows.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "forWindows.h"
#include "lm_conf_engine_wrapper.h"
#include "lm_conf_base_wrapper.h"
#include "lm_conf_business_wrapper.h"
#include "lm_conf_video_wrapper.h"
#include "lm_conf_voice_wrapper.h"
#include "lm_conf_sharedesktop_wrapper.h"
#include "lm_conf_dataserver_wrapper.h"
#include "lm_conf_centerserver_wrapper.h"
#include "lm_conf_record_wrapper.h"
#include "lm_conf_playback_wrapper.h"
#include "lm_conf_mediaplayer_wrapper.h"
#include "lm_conf_ipcamera_wrapper.h"
#include "lm_conf_as_attendee_wrapper.h"
#include "lm_conf_cs_param_wrapper.h"

template<class T>
class ScopedConfPtr {
public:
	ScopedConfPtr(T* ptr) : conf_object_(ptr) {
	}

	~ScopedConfPtr() {
		delete conf_object_;
	}

	T& operator*() const {
		return *conf_object_;
	}

	T* operator->() const  {
		return conf_object_;
	}

	operator T*() const { return conf_object_; }

	T* get() const { return conf_object_; }

private:
	T* conf_object_;
};

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;								// 当前实例
TCHAR szTitle[MAX_LOADSTRING];					// 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];			// 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

void TestInitConfEngine();
void TestUninitConfEngine();
void TestAddObserver();
void TestRemoveObserver();

void TestPrepareLoginConf();
void TestEntryConference();
void TestLeaveConference();
void TestUpMic();
void TestDownMic();

void Start();
void Stop();
void UpMic();
void DownMic();

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO:  在此放置代码。
	MSG msg;
	HACCEL hAccelTable;

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_FORWINDOWS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 执行应用程序初始化: 
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FORWINDOWS));
	
	// 主消息循环: 
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  函数:  MyRegisterClass()
//
//  目的:  注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FORWINDOWS));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_FORWINDOWS);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   函数:  InitInstance(HINSTANCE, int)
//
//   目的:  保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // 将实例句柄存储在全局变量中

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数:  WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND	- 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY	- 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_START:
			Start();
			break;
		case ID_STOP:
			Stop();
			break;
		case ID_UPMIC:
			UpMic();
			break;
		case ID_DOWNMIC:
			DownMic();
			break;
		case ID_PREPARE_LOGIN:
			TestPrepareLoginConf();
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO:  在此添加任意绘图代码...
		EndPaint(hWnd, &ps);
		break;
	case WM_CREATE:
		break;

	case WM_DESTROY: {
		Stop();
		PostQuitMessage(0);
	}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

bool s_starting = false;
void Start() {
	if (s_starting)
		return;
	s_starting = true;
	TestInitConfEngine();
	TestAddObserver();
// 	TestPrepareLoginConf();
}

void Stop() {
	if (!s_starting)
		return;
	s_starting = false;
	TestLeaveConference();
	TestRemoveObserver();
	TestUninitConfEngine();
}

void UpMic() {
	if (!s_starting)
		return;
	TestUpMic();
}

void DownMic() {
	if (!s_starting)
		return;
	TestDownMic();
}

class ConfVoiceCB : public lm::ConfVoiceWrapper::Observer {
public:
	ConfVoiceCB() {}
	virtual ~ConfVoiceCB() {}
protected:
	virtual void OnDeviceChange() override {}
	virtual void OnDisconnectVoiceServer(int result) override {}
};

class ConfBussinessCB : public lm::ConfBusinessWrapper::Observer {
public:
	ConfBussinessCB() {}
	virtual ~ConfBussinessCB() {}

protected:
	virtual void OnEntryConference(int result) override {
		if (result != 0)
			::MessageBoxA(NULL, lm::ConfBaseWrapper::ErrorToString(result).c_str(), "ConfError", MB_OK);
	}

	virtual void OnLeaveConference(int result) override {
		TestUninitConfEngine();
	}

	virtual void OnApplySpeakOper(int result, const char* account, int new_state, int old_state, bool apply) override {
		if (apply) {
			if (result != 0)
				::MessageBoxA(NULL, lm::ConfBaseWrapper::ErrorToString(result).c_str(), "ConfError", MB_OK);
			ScopedConfPtr<lm::ASConfAttendeeWrapper> self(lm::ASConfAttendeeWrapper::self_from_conf_base());
			if (strcmp(self->Account(), account) == 0) {
				if (self->SpeakOp()) {
					//实际应用中不要阻塞
					::MessageBoxA(NULL, "音频数据正在发往音频服务器", "ConfError", MB_OK);
				}
				else if (self->ApplySpeakOp()) {
					//实际应用中不要阻塞
					::MessageBoxA(NULL, "等待管理员授权", "ConfError", MB_OK);
				}
			}
		}
	}

	virtual void OnSpeakNotify(const char* account, int new_state, int old_state, bool is_speak) override {
		ScopedConfPtr<lm::ASConfAttendeeWrapper> self(lm::ASConfAttendeeWrapper::self_from_conf_base());
		if (strcmp(self->Account(), account) == 0) {
			if (is_speak) {
				//实际应用中不要阻塞
				::MessageBoxA(NULL, "音频数据正在发往音频服务器", "ConfError", MB_OK);
			}
			else {
				//实际应用中不要阻塞
				::MessageBoxA(NULL, "音频数据已停止发往音频服务器", "ConfError", MB_OK);
			}
		}
		else {
			if (is_speak) {
				//实际应用中不要阻塞
				::MessageBoxA(NULL, "您将听到其他与会者的声音", "ConfError", MB_OK);
			}
			else {
				//实际应用中不要阻塞
				::MessageBoxA(NULL, "与会者停止声音", "ConfError", MB_OK);
			}
		}
	}

	virtual void OnAddSelfAttendee(
		const lm::ASConfAttendeeWrapper* slef_attendee_info) {
		//进入会议时或断开重连后,自己的信息
	}

	virtual void OnAttendeeOnline(
		const lm::ASConfAttendeeWrapper* attendee_info) {
		//有人员上线
	}

	virtual void OnAttendeeOffline(
		const char* account) {
		//有人员下线
	}

	virtual void OnUpdateAttendee(
		const lm::ASConfAttendeeWrapper* old_attendee_info,
		const lm::ASConfAttendeeWrapper* new_attendee_info) {
		//在断开重连的过程中,与会者的信息有改变
	}

};

lm::ConfEngineWrapper* engine_wrapper = NULL;

class ConfCenterServerCB : public lm::ConfCenterServerWrapper::Observer {
public:
	ConfCenterServerCB() {}
	virtual ~ConfCenterServerCB() {}

protected:
	virtual void OnPrepareLoginConf(int result,
		const lm::CSParamWrapper* param, int id_conf, const char* jsonAddress)
	{
		if (result == 0)//成功
		{
			char szServerAllocName[128] = "test001";
			param->GetKeyValue(PCK_CONF_GUEST_USER, szServerAllocName, 128);

			if (engine_wrapper != NULL)
			{
				engine_wrapper->business_wrapper()->EntryConference(
					jsonAddress, id_conf, 0, "", false, szServerAllocName, 0, "My Name");
			}
		}
		else
		{
			char szMsg[1024] = { 0 };
			sprintf_s(szMsg, "登录中心获取入会参数出错,错误号(%ld)", result);
			::MessageBoxA(NULL, szMsg, "ConfError", MB_OK);

		}
	}
};


ConfBussinessCB conf_business_cb;
ConfVoiceCB conf_voice_cb;
ConfCenterServerCB conf_center_cb;

const char* center_address = "127.0.0.1:2810";
void TestPrepareLoginConf() {

	if (engine_wrapper == NULL)
		return;

	engine_wrapper->centerserver_wrapper()->SetCenterAddress(center_address);

	int id_conf = 0;
	lm::CSParamWrapper paramSet;
	paramSet.SetKeyValue(PCK_CONF_CODE, "0");

	engine_wrapper->centerserver_wrapper()->PrepareLoginConf(&paramSet, id_conf);
}

void TestInitConfEngine() {
	if (engine_wrapper)
		return;

	engine_wrapper = lm::ConfEngineWrapper::Create();

	LMInitOptions options;
	lm::ConfBaseWrapper::DefaultInitOptions(&options);

	options.callMode = k2In1;
	options.char_set = kCharSetUTF8;
	options.proxy_options.proxyType = kPTInvalid;
	options.screen_copy_fps = 6;
	options.enable_desktop_camera = 0;
	options.screen_copy_layer_window = 0;
	options.use_tcp = 0;
	options.auto_connect = 1;
	engine_wrapper->base_wrapper()->InitWithOptions(options);
}

void TestUninitConfEngine() {
	if (!engine_wrapper)
		return;

	engine_wrapper->base_wrapper()->Terminate();
	lm::ConfEngineWrapper::Delete(engine_wrapper);
	engine_wrapper = NULL;
}

void TestAddObserver() {
	engine_wrapper->business_wrapper()->AddObserver(&conf_business_cb);
	engine_wrapper->voice_wrapper()->AddObserver(&conf_voice_cb);
	engine_wrapper->centerserver_wrapper()->AddObserver(&conf_center_cb);
}

void TestRemoveObserver() {
	engine_wrapper->business_wrapper()->RemoveObserver(&conf_business_cb);
	engine_wrapper->voice_wrapper()->RemoveObserver(&conf_voice_cb);
	engine_wrapper->centerserver_wrapper()->RemoveObserver(&conf_center_cb);
}

const char* server_address_json = "{\"Address\":\"103.56.62.174;222.186.136.174\",\"Port\":2811}";
void TestEntryConference() {
	engine_wrapper->business_wrapper()->EntryConference(
		server_address_json, 1130, 0, "", false, "test001", 0, "My Name");
}

void TestLeaveConference() {
	engine_wrapper->business_wrapper()->LeaveConference();
}

void TestUpMic() {
	engine_wrapper->business_wrapper()->ApplySpeak(true);
}

void TestDownMic() {
	engine_wrapper->business_wrapper()->ApplySpeak(false);
}

void TestVoiceManager() {
	//mic声音禁止
	engine_wrapper->voice_wrapper()->EnableInput(false);
	
	//speaker禁止
	engine_wrapper->voice_wrapper()->EnableOutput(false);

	//设置音频编码方式
	engine_wrapper->voice_wrapper()->SetCodecType(kISACCodec, k120MSDelay);
	//或	 rate OPUS支持48k,32k,16k,其他默认-1
	engine_wrapper->voice_wrapper()->SetCodecTypeWithInputDevice(kLocalInputVoiceDevice,
		kOpusCodec, k120MSDelay, 48000);
	engine_wrapper->voice_wrapper()->SetCodecTypeWithInputDevice(kLocalInputVoiceDevice,
		kISACCodec, k120MSDelay, -1);

}