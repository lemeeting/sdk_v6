//
//  lm_conf_cs_param_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSParamWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

- (int)getKeyCount;
- (int)getKeyList:(int*)keys count:(int)count;

- (int)getIntValue:(int)key;
- (unsigned int)getUIntValue:(int)key;
- (NSString *)getStringValue:(int)key;
- (CFAbsoluteTime)getTimeValue:(int)key;

- (void)setIntValue:(int)key value:(int)value;
- (void)setUIntValue:(int)key value:(unsigned int)value;
- (void)setStringValue:(int)key value:(NSString*)value;
- (void)setTimeValue:(int)key value:(CFAbsoluteTime)value;

@end
