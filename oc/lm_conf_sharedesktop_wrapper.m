//
//  lm_conf_sharedesktop_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_sharedesktop_wrapper.h"
void CALLBACK FuncShareDesktopOnDisconnectShareServerCB(int result, void* clientdata) {
    ConfShareDesktopWrapper* wrapper = (__bridge ConfShareDesktopWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfShareDesktopWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(OnDisconnectShareServer:)]) {
            [delegate OnDisconnectShareServer:result];
        }
    }
}

@implementation ConfShareDesktopWrapper
@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfShareDesktopCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnDisconnectShareServer_ = FuncShareDesktopOnDisconnectShareServerCB;
        lm_conf_sharedesktop_set_callback(c_object_, &cb);
    }
    return self;
}

- (void)addDelegate:(id<ConfShareDesktopWrapperDelegate>)desktop_delegate {
    [delegate_array_ addObject:desktop_delegate];
}

- (void)removeDelegate:(id<ConfShareDesktopWrapperDelegate>)desktop_delegate {
    [delegate_array_ removeObject:desktop_delegate];
}


@end
