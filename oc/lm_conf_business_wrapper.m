//
//  lm_conf_business_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_business_wrapper.h"
#import "lm_conf_as_attendee_wrapper.h"
#import "lm_conf_as_confattribute_wrapper.h"
#import "lm_conf_as_syncinfo_wrapper.h"
#import "lm_conf_as_realtimeconfinfo_wrapper.h"

void CALLBACK FuncBusinessOnDisconnectCB(
                                         int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onDisconnect:)]) {
            [delegate onDisconnect:result];
        }
    }
}

void CALLBACK FuncBusinessOnEntryConferenceCB(
                                              int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onEntryConference:)]) {
            [delegate onEntryConference:result];
        }
    }
}

void CALLBACK FuncBusinessOnLeaveConferenceCB(
                                              int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onLeaveConference:)]) {
            [delegate onLeaveConference:result];
        }
    }
}

void CALLBACK FuncBusinessOnGetConferenceCB(
                                            native_object_t conference_info, void* clientdata) {
    ASConfAttributeWrapper* info = [[ASConfAttributeWrapper alloc] initWithCObject:conference_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConference:)]) {
            [delegate onGetConference:info];
        }
    }
}

void CALLBACK FuncBusinessOnUpdateConerenceCB(
                                              native_object_t conference_info, void* clientdata) {
    ASConfAttributeWrapper* info = [[ASConfAttributeWrapper alloc] initWithCObject:conference_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateConerence:)]) {
            [delegate onUpdateConerence:info];
        }
    }
}

void CALLBACK FuncBusinessOnRemoveConerenceCB(
                                              int id_conference, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveConerence:)]) {
            [delegate onRemoveConerence:id_conference];
        }
    }
}

void CALLBACK FuncBusinessOnGetConferenceRealTimeInfoCB(
                                                        native_object_t real_time_conference_info, void* clientdata) {
    ASConfRealTimeInfoWrapper* info = [[ASConfRealTimeInfoWrapper alloc] initWithCObject:real_time_conference_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConferenceRealTimeInfo:)]) {
            [delegate onGetConferenceRealTimeInfo:info];
        }
    }
}

void CALLBACK FuncBusinessOnGetConferenceSyncInfoCB(
                                                    native_object_t conference_sync_info, void* clientdata) {
    ASConfSyncInfoWrapper* info = [[ASConfSyncInfoWrapper alloc] initWithCObject:conference_sync_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConferenceSyncInfo:)]) {
            [delegate onGetConferenceSyncInfo:info];
        }
    }
}

void CALLBACK FuncBusinessOnAddSelfAttendeeCB(
                                              native_object_t slef_attendee_info, void* clientdata) {
    ASConfAttendeeWrapper* info = [[ASConfAttendeeWrapper alloc] initWithCObject:slef_attendee_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddSelfAttendee:)]) {
            [delegate onAddSelfAttendee:info];
        }
    }
}

void CALLBACK FuncBusinessOnAttendeeOnlineCB(
                                             native_object_t attendee_info, void* clientdata) {
    ASConfAttendeeWrapper* info = [[ASConfAttendeeWrapper alloc] initWithCObject:attendee_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAttendeeOnline:)]) {
            [delegate onAttendeeOnline:info];
        }
    }
}

void CALLBACK FuncBusinessOnAttendeeOfflineCB(
                                              const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAttendeeOffline:)]) {
            [delegate onAttendeeOffline:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnUpdateAttendeeCB(
                                             native_object_t old_attendee_info, native_object_t new_attendee_info, void* clientdata) {
    ASConfAttendeeWrapper* old_info = [[ASConfAttendeeWrapper alloc] initWithCObject:old_attendee_info];
    ASConfAttendeeWrapper* new_info = [[ASConfAttendeeWrapper alloc] initWithCObject:new_attendee_info];
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateAttendee:attendee:)]) {
            [delegate onUpdateAttendee:old_info attendee:new_info];
        }
    }
}

void CALLBACK FuncBusinessOnRemoveAttendeeCB(
                                             const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveAttendee:)]) {
            [delegate onRemoveAttendee:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnAddConfAdminCB(
                                           const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddConfAdmin:)]) {
            [delegate onAddConfAdmin:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnRemoveConfAdminCB(
                                              const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveConfAdmin:)]) {
            [delegate onRemoveConfAdmin:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnAddConfDefaultAttendeeCB(
                                                     const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddConfDefaultAttendee:)]) {
            [delegate onAddConfDefaultAttendee:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnRemoveConfDefaultAttendeeCB(
                                                        const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveConfDefaultAttendee:)]) {
            [delegate onRemoveConfDefaultAttendee:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnApplySpeakOperCB(
                                             int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onApplySpeakOper:account:new_state:old_state:apply:)]) {
            [delegate onApplySpeakOper:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state apply:!!apply];
        }
    }
}

void CALLBACK FuncBusinessOnAccreditSpeakOperCB(
                                                int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAccreditSpeakOper:account:new_state:old_state:accredit:)]) {
            [delegate onAccreditSpeakOper:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state accredit:!!accredit];
        }
    }
}

void CALLBACK FuncBusinessOnSpeakNotifyCB(
                                          const char* account, int new_state, int old_state, int is_speak, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSpeakNotify:new_state:old_state:speak:)]) {
            [delegate onSpeakNotify:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state speak:!!is_speak];
        }
    }
}

void CALLBACK FuncBusinessOnApplyDataOperCB(
                                            int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onApplyDataOper:account:new_state:old_state:apply:)]) {
            [delegate onApplyDataOper:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state apply:!!apply];
        }
    }
}

void CALLBACK FuncBusinessOnAccreditDataOperCB(
                                               int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAccreditDataOper:account:new_state:old_state:accredit:)]) {
            [delegate onAccreditDataOper:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state accredit:!!accredit];
        }
    }
}

void CALLBACK FuncBusinessOnDataOpNotifyCB(
                                           const char* account, int new_state, int old_state, int is_data_op, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onDataOpNotify:new_state:old_state:is_data_op:)]) {
            [delegate onDataOpNotify:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state is_data_op:!!is_data_op];
        }
    }
}

void CALLBACK FuncBusinessOnApplyDataSyncCB(
                                            int result, const char* account, int new_state, int old_state, int apply, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onApplyDataSync:account:new_state:old_state:apply:)]) {
            [delegate onApplyDataSync:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state apply:!!apply];
        }
    }
}

void CALLBACK FuncBusinessOnAccreditDataSyncCB(
                                               int result, const char* account, int new_state, int old_state, int accredit, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAccreditDataSync:account:new_state:old_state:accredit:)]) {
            [delegate onAccreditDataSync:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state accredit:!!accredit];
        }
    }
}

void CALLBACK FuncBusinessOnSyncOpNotifyCB(
                                           const char* account, int new_state, int old_state, int is_sync, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSyncOpNotify:new_state:old_state:sync:)]) {
            [delegate onSyncOpNotify:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state sync:!!is_sync];
        }
    }
}

void CALLBACK FuncBusinessOnDataSyncCommandCB(
                                              int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onDataSyncCommand:)]) {
            [delegate onDataSyncCommand:result];
        }
    }
}

void CALLBACK FuncBusinessOnDataSyncCommandNotifyCB(
                                                    const char* syncer, const char* sync_data, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onDataSyncCommandNotify:data:)]) {
            [delegate onDataSyncCommandNotify:[NSString stringWithUTF8String:syncer] data:[NSString stringWithUTF8String:sync_data]];
        }
    }
}

void CALLBACK FuncBusinessOnApplyTempAdminCB(
                                             int result, const char* account,
                                             int new_state, int old_state, int apply, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onApplyTempAdmin:account:new_state:old_state:apply:)]) {
            [delegate onApplyTempAdmin:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state apply:!!apply];
        }
    }
}

void CALLBACK FuncBusinessOnAccreditTempAdminCB(
                                                int result, const char* account,
                                                int new_state, int old_state, int accredit, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAccreditTempAdmin:account:new_state:old_state:accredit:)]) {
            [delegate onAccreditTempAdmin:result account:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state accredit:!!accredit];
        }
    }
}

void CALLBACK FuncBusinessOnAuthTempAdminCB(
                                            int result, const char* admin_psw, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAuthTempAdmin:account:)]) {
            [delegate onAuthTempAdmin:result account:[NSString stringWithUTF8String:admin_psw]];
        }
    }
}

void CALLBACK FuncBusinessOnTempAdminNotifyCB(const char* account,
                                              uint32_t new_state, uint32_t old_state, int apply, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onTempAdminNotify:new_state:old_state:is_admin:)]) {
            [delegate onTempAdminNotify:[NSString stringWithUTF8String:account] new_state:new_state old_state:old_state is_admin:!!apply];
        }
    }
}

void CALLBACK FuncBusinessOnStartPreviewVideoCB(
                                                int result, const char* shower,
                                                int id_device, uint64_t context, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPreviewVideo:shower:identity:context:)]) {
            [delegate onStartPreviewVideo:result shower:[NSString stringWithUTF8String:shower] identity:id_device context:context];
        }
    }
}

void CALLBACK FuncBusinessOnStopPreviewVideoCB(
                                               int result, const char* shower,
                                               int id_device, uint64_t context, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPreviewVideo:shower:identity:context:)]) {
            [delegate onStopPreviewVideo:result shower:[NSString stringWithUTF8String:shower] identity:id_device context:context];
        }
    }
}

void CALLBACK FuncBusinessOnStartPreviewVideoNotifyCB(
                                                      const char* shower, int id_device, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPreviewVideoNotify:identity:)]) {
            [delegate onStartPreviewVideoNotify:[NSString stringWithUTF8String:shower] identity:id_device];
        }
    }
}

void CALLBACK FuncBusinessOnStopPreviewVideoNotifyCB(
                                                     const char* shower, int id_device, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPreviewVideoNotify:identity:)]) {
            [delegate onStopPreviewVideoNotify:[NSString stringWithUTF8String:shower] identity:id_device];
        }
    }
}

void CALLBACK FuncBusinessOnSwitchMainVideoCB(
                                              int result, int old_dev_id, int new_dev_id, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSwitchMainVideo:old_identity:identity:)]) {
            [delegate onSwitchMainVideo:result old_identity:old_dev_id identity:new_dev_id];
        }
    }
}

void CALLBACK FuncBusinessOnSwitchMainVideoNotifyCB(
                                                    const char* account, int old_dev_id, int new_dev_id, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSwitchMainVideoNotify:old_identity:identity:)]) {
            [delegate onSwitchMainVideoNotify:[NSString stringWithUTF8String:account] old_identity:old_dev_id identity:new_dev_id];
        }
    }
}

void CALLBACK FuncBusinessOnEnableVideoCB(
                                          int result, int enabled, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onEnableVideo:enabled:)]) {
            [delegate onEnableVideo:result enabled:!!enabled];
        }
    }
}

void CALLBACK FuncBusinessOnEnableVideoNotifyCB(
                                                const char* account, int enabled, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onEnableVideoNotify:enabled:)]) {
            [delegate onEnableVideoNotify:[NSString stringWithUTF8String:account] enabled:!!enabled];
        }
    }
}

void CALLBACK FuncBusinessOnCaptureVideoStateCB(
                                                int result, int id_device, int enabled, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onCaptureVideoState:identity:enabled:)]) {
            [delegate onCaptureVideoState:result identity:id_device enabled:!!enabled];
        }
    }
}

void CALLBACK FuncBusinessOnCaptureVideoStateNotifyCB(
                                                      const char* account, int id_device, int enabled, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onCaptureVideoStateNotify:identity:enabled:)]) {
            [delegate onCaptureVideoStateNotify:[NSString stringWithUTF8String:account] identity:id_device enabled:!!enabled];
        }
    }
}

void CALLBACK FuncBusinessOnVideoShowNameChangedNotifyCB(
                                                         const char* account, int id_device, const char* new_show_name, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onVideoShowNameChangedNotify:identity:name:)]) {
            [delegate onVideoShowNameChangedNotify:[NSString stringWithUTF8String:account] identity:id_device name:[NSString stringWithUTF8String:new_show_name]];
        }
    }
}

void CALLBACK FuncBusinessOnVideoDeviceChangedNotifyCB(
                                                       const char* account, int dev_nums, int dev_state, int dev_main_id, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onVideoDeviceChangedNotify:devnums:devstate:maindevidentity:)]) {
            [delegate onVideoDeviceChangedNotify:[NSString stringWithUTF8String:account] devnums:dev_nums devstate:dev_state maindevidentity:dev_main_id];
        }
    }
}

void CALLBACK FuncBusinessOnKickoutAttendeeCB(
                                              int result, const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onKickoutAttendee:account:)]) {
            [delegate onKickoutAttendee:result account:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnKickoutAttendeeNotifyCB(
                                                    const char* oper, const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onKickoutAttendeeNotify:account:)]) {
            [delegate onKickoutAttendeeNotify:[NSString stringWithUTF8String:oper] account:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnUpdateAttendeeNameCB(
                                                 int result, const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateAttendeeName:newname:)]) {
            [delegate onUpdateAttendeeName:result newname:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnUpdateAttendeeNameNotifyCB(
                                                       const char* account, const char* new_name, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateAttendeeNameNotify:newname:)]) {
            [delegate onUpdateAttendeeNameNotify:[NSString stringWithUTF8String:account] newname:[NSString stringWithUTF8String:new_name]];
        }
    }
}

void CALLBACK FuncBusinessOnRelayMsgToOneCB(
                                            int result, const char* receiver, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRelayMsgToOne:receiver:)]) {
            [delegate onRelayMsgToOne:result receiver:[NSString stringWithUTF8String:receiver]];
        }
    }
}

void CALLBACK FuncBusinessOnRelayMsgToOneNotifyCB(
                                                  const char* sneder, const char* receiver, const char* msg, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRelayMsgToOneNotify:receiver:msg:)]) {
            [delegate onRelayMsgToOneNotify:[NSString stringWithUTF8String:sneder] receiver:[NSString stringWithUTF8String:receiver] msg:[NSString stringWithUTF8String:msg]];
        }
    }
}

void CALLBACK FuncBusinessOnRelayMsgToAllCB(
                                            int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRelayMsgToAll:)]) {
            [delegate onRelayMsgToAll:result];
        }
    }
}

void CALLBACK FuncBusinessOnRelayMsgToAllNotifyCB(
                                                  const char* sneder, const char* msg, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRelayMsgToAllNotify:msg:)]) {
            [delegate onRelayMsgToAllNotify:[NSString stringWithUTF8String:sneder] msg:[NSString stringWithUTF8String: msg]];
        }
    }
}

void CALLBACK FuncBusinessOnAdminOperConfSettingCB(
                                                   int result, int cmd, int cmd_value, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAdminOperConfSetting:cmd:value:)]) {
            [delegate onAdminOperConfSetting:result cmd:cmd value:cmd_value];
        }
    }
}

void CALLBACK FuncBusinessOnAdminOperConfSettingNotifyCB(
                                                         const char* oper, int cmd, int cmd_value, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAdminOperConfSettingNotify:cmd:value:)]) {
            [delegate onAdminOperConfSettingNotify:[NSString stringWithUTF8String:oper] cmd:cmd value:cmd_value];
        }
    }
}

void CALLBACK FuncBusinessOnSetConfPasswordCB(int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSetConfPassword:)]) {
            [delegate onSetConfPassword:result];
        }
    }
}

void CALLBACK FuncBusinessOnSetConfPasswordNotifyCB(
                                                    const char* oper, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSetConfPasswordNotify:)]) {
            [delegate onSetConfPasswordNotify:[NSString stringWithUTF8String:oper]];
        }
    }
}

void CALLBACK FuncBusinessOnStartDesktopShareCB(
                                                int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopDesktopShare:)]) {
            [delegate onStopDesktopShare:result];
        }
    }
}

void CALLBACK FuncBusinessOnStartDesktopShareNotifyCB(
                                                      const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartDesktopShareNotify:)]) {
            [delegate onStartDesktopShareNotify:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnStopDesktopShareCB(
                                               int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopDesktopShare:)]) {
            [delegate onStopDesktopShare:result];
        }
    }
}

void CALLBACK FuncBusinessOnStopDesktopShareNotifyCB(
                                                     const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopDesktopShareNotify:)]) {
            [delegate onStopDesktopShareNotify:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnStartPreviewDesktopCB(
                                                  int result, const char* sharer, uint64_t context, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPreviewDesktop:sharer:context:)]) {
            [delegate onStartPreviewDesktop:result sharer:[NSString stringWithUTF8String:sharer] context:context];
        }
    }
}

void CALLBACK FuncBusinessOnStopPreviewDesktopCB(
                                                 int result, const char* sharer, uint64_t context, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPreviewDesktop:sharer:context:)]) {
            [delegate onStopPreviewDesktop:result sharer:[NSString stringWithUTF8String:sharer] context:context];
        }
    }
}

void CALLBACK FuncBusinessOnStartPreviewDesktopNotifyCB(
                                                        const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPreviewDesktopNotify:)]) {
            [delegate onStartPreviewDesktopNotify:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnStopPreviewDesktopNotifyCB(
                                                       const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPreviewDesktopNotify:)]) {
            [delegate onStopPreviewDesktopNotify:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnAskRemoteDesktopControlCB(
                                                      int result, const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAskRemoteDesktopControl:sharer:)]) {
            [delegate onAskRemoteDesktopControl:result sharer:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnAskRemoteDesktopControlNotifyCB(
                                                            const char* controller, const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAskRemoteDesktopControlNotify:sharer:)]) {
            [delegate onAskRemoteDesktopControlNotify:[NSString stringWithUTF8String:controller] sharer:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlCB(
                                                          int result, const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAbstainRemoteDesktopControl:sharer:)]) {
            [delegate onAbstainRemoteDesktopControl:result sharer:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnAbstainRemoteDesktopControlNotifyCB(
                                                                const char* controller, const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAbstainRemoteDesktopControlNotify:sharer:)]) {
            [delegate onAbstainRemoteDesktopControlNotify:[NSString stringWithUTF8String:controller] sharer:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlCB(
                                                         int result, const char* controller, int allow, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAnswerRemoteDesktopControl:controller:allow:)]) {
            [delegate onAnswerRemoteDesktopControl:result controller:[NSString stringWithUTF8String:controller] allow:!!allow];
        }
    }
}

void CALLBACK FuncBusinessOnAnswerRemoteDesktopControlNotifyCB(
                                                               const char* controller, const char* sharer, int allow, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAnswerRemoteDesktopControlNotify:sharer:allow:)]) {
            [delegate onAnswerRemoteDesktopControlNotify:[NSString stringWithUTF8String:controller] sharer:[NSString stringWithUTF8String:sharer] allow:!!allow];
        }
    }
}

void CALLBACK FuncBusinessOnAbortRemoteDesktopControlCB(
                                                        int result, const char* controller, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAbstainRemoteDesktopControl:sharer:)]) {
            [delegate onAbstainRemoteDesktopControl:result sharer:[NSString stringWithUTF8String:controller]];
        }
    }
}

void CALLBACK FuncBusinessOnAbortRemoteDesktopControlNotifyCB(
                                                              const char* controller, const char* sharer, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAbstainRemoteDesktopControlNotify:sharer:)]) {
            [delegate onAbstainRemoteDesktopControlNotify:[NSString stringWithUTF8String:controller] sharer:[NSString stringWithUTF8String:sharer]];
        }
    }
}

void CALLBACK FuncBusinessOnLaunchSigninCB(
                                           int result, int type, int64_t launch_or_stop_time, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onLaunchSignin:type:time:)]) {
            [delegate onLaunchSignin:result type:type time:launch_or_stop_time];
        }
    }
}

void CALLBACK FuncBusinessOnLaunchSigninNotifyCB(
                                                 const char* account, int type, int64_t launch_or_stop_time, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onLaunchSigninNotify:type:time:)]) {
            [delegate onLaunchSigninNotify:[NSString stringWithUTF8String:account] type:type time:launch_or_stop_time];
        }
    }
}

void CALLBACK FuncBusinessOnSigninCB(
                                     int result, int is_signin, int64_t launch_time,
                                     int64_t signin_or_cancel_time, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSignin:is_signin:launch_time:time:)]) {
            [delegate onSignin:result is_signin:!!is_signin launch_time:launch_time time:signin_or_cancel_time];
        }
    }
}

void CALLBACK FuncBusinessOnSigninNotifyCB(
                                           const char* account, int is_signin, int64_t launch_time,
                                           int64_t signin_or_cancel_time, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSigninNotify:is_signin:launch_time:time:)]) {
            [delegate onSigninNotify:[NSString stringWithUTF8String:account] is_signin:!!is_signin launch_time:launch_time time:signin_or_cancel_time];
        }
    }
}

void CALLBACK FuncBusinessOnOperSubGroupCB(
                                           int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onOperSubGroup:)]) {
            [delegate onOperSubGroup:result];
        }
    }
}

void CALLBACK FuncBusinessOnOperSubGroupNotifyCB(
                                                 const char* oper, int id_sub_group, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onOperSubGroupNotify:subgroup:)]) {
            [delegate onOperSubGroupNotify:[NSString stringWithUTF8String:oper] subgroup:id_sub_group];
        }
    }
}

void CALLBACK FuncBusinessOnCancelSubGroupCB(
                                             int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onCancelSubGroup:)]) {
            [delegate onCancelSubGroup:result];
        }
    }
}

void CALLBACK FuncBusinessOnCancelSubGroupNotifyCB(
                                                   const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onCancelSubGroupNotify:)]) {
            [delegate onCancelSubGroupNotify:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnOpRemoteDesktopSharedCB(
                                                    int result, const char* sharer, int is_start, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onOpRemoteDesktopShared:sharer:start:)]) {
            [delegate onOpRemoteDesktopShared:result sharer:[NSString stringWithUTF8String:sharer] start:!!is_start];
        }
    }
}

void CALLBACK FuncBusinessOnOpRemoteDesktopSharedNotifyCB(
                                                          const char* oper, const char* sharer, int is_start, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onOpRemoteDesktopSharedNotify:sharer:start:)]) {
            [delegate onOpRemoteDesktopSharedNotify:[NSString stringWithUTF8String:oper] sharer:[NSString stringWithUTF8String:sharer] start:!!is_start];
        }
    }
}

void CALLBACK FuncBusinessOnStartPlaybackCB(
                                            int result, const char* file_name, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPlayback:filenme:)]) {
            [delegate onStartPlayback:result filenme:[NSString stringWithUTF8String:file_name]];
        }
    }
}

void CALLBACK FuncBusinessOnStartPlaybackNotifyCB(
                                                  const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartPlaybackNotify:)]) {
            [delegate onStartPlaybackNotify:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnStopPlaybackCB(
                                           int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPlayback:)]) {
            [delegate onStopPlayback:result];
        }
    }
}

void CALLBACK FuncBusinessOnStopPlaybackNotifyCB(
                                                 const char* account, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopPlaybackNotify:)]) {
            [delegate onStopPlaybackNotify:[NSString stringWithUTF8String:account]];
        }
    }
}

void CALLBACK FuncBusinessOnStartMediaPlayCB(
                                             int result, const char* file_name, int media_flag, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartMediaPlay:fileName:flags:)]) {
            [delegate onStartMediaPlay:result fileName:[NSString stringWithUTF8String:file_name] flags:media_flag];
        }
    }
}

void CALLBACK FuncBusinessOnStartMediaPlayNotifyCB(
                                                   const char* oper, const char* file_name, int media_flag, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartMediaPlayNotify:fileName:flags:)]) {
            [delegate onStartMediaPlayNotify:[NSString stringWithUTF8String:oper] fileName:[NSString stringWithUTF8String:file_name] flags:media_flag];
        }
    }
}

void CALLBACK FuncBusinessOnStopMediaPlayCB(
                                            int result, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopMediaPlay:)]) {
            [delegate onStopMediaPlay:result];
        }
    }
}

void CALLBACK FuncBusinessOnStopMediaPlayNotifyCB(
                                                  const char* oper, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStopMediaPlayNotify:)]) {
            [delegate onStopMediaPlayNotify:[NSString stringWithUTF8String:oper]];
        }
    }
}

void CALLBACK FuncBusinessOnPauseMediaPlayCB(
                                             int result, int paused, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onPauseMediaPlay:paused:)]) {
            [delegate onPauseMediaPlay:result paused:!!paused];
        }
    }
}

void CALLBACK FuncBusinessOnPauseMediaPlayNotifyCB(
                                                   const char* oper, int paused, void* clientdata) {
    ConfBusinessWrapper* wrapper = (__bridge ConfBusinessWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfBusinessWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onPauseMediaPlayNotify:paused:)]) {
            [delegate onPauseMediaPlayNotify:[NSString stringWithUTF8String:oper] paused:!!paused];
        }
    }
}

@implementation ConfBusinessWrapper

@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfBusinessCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnDisconnect_ = FuncBusinessOnDisconnectCB;
        cb.OnEntryConference_ = FuncBusinessOnEntryConferenceCB;
        cb.OnLeaveConference_ = FuncBusinessOnLeaveConferenceCB;
        cb.OnGetConference_ = FuncBusinessOnGetConferenceCB;
        cb.OnUpdateConerence_ = FuncBusinessOnUpdateConerenceCB;
        cb.OnRemoveConerence_ = FuncBusinessOnRemoveConerenceCB;
        cb.OnGetConferenceRealTimeInfo_ = FuncBusinessOnGetConferenceRealTimeInfoCB;
        cb.OnGetConferenceSyncInfo_ = FuncBusinessOnGetConferenceSyncInfoCB;
        cb.OnAddSelfAttendee_ = FuncBusinessOnAddSelfAttendeeCB;
        cb.OnAttendeeOnline_ = FuncBusinessOnAttendeeOnlineCB;
        cb.OnAttendeeOffline_ = FuncBusinessOnAttendeeOfflineCB;
        cb.OnUpdateAttendee_ = FuncBusinessOnUpdateAttendeeCB;
        cb.OnRemoveAttendee_ = FuncBusinessOnRemoveAttendeeCB;
        cb.OnAddConfAdmin_ = FuncBusinessOnAddConfAdminCB;
        cb.OnRemoveConfAdmin_ = FuncBusinessOnRemoveConfAdminCB;
        cb.OnAddConfDefaultAttendee_ = FuncBusinessOnAddConfDefaultAttendeeCB;
        cb.OnRemoveConfDefaultAttendee_ = FuncBusinessOnRemoveConfDefaultAttendeeCB;
        cb.OnApplySpeakOper_ = FuncBusinessOnApplySpeakOperCB;
        cb.OnAccreditSpeakOper_ = FuncBusinessOnAccreditSpeakOperCB;
        cb.OnSpeakNotify_ = FuncBusinessOnSpeakNotifyCB;
        cb.OnApplyDataOper_ = FuncBusinessOnApplyDataOperCB;
        cb.OnAccreditDataOper_ = FuncBusinessOnAccreditDataOperCB;
        cb.OnDataOpNotify_ = FuncBusinessOnDataOpNotifyCB;
        cb.OnApplyDataSync_ = FuncBusinessOnApplyDataSyncCB;
        cb.OnAccreditDataSync_ = FuncBusinessOnAccreditDataSyncCB;
        cb.OnSyncOpNotify_ = FuncBusinessOnSyncOpNotifyCB;
        cb.OnDataSyncCommand_ = FuncBusinessOnDataSyncCommandCB;
        cb.OnDataSyncCommandNotify_ = FuncBusinessOnDataSyncCommandNotifyCB;
        cb.OnApplyTempAdmin_ = FuncBusinessOnApplyTempAdminCB;
        cb.OnAccreditTempAdmin_ = FuncBusinessOnAccreditTempAdminCB;
        cb.OnAuthTempAdmin_ = FuncBusinessOnAuthTempAdminCB;
        cb.OnTempAdminNotify_ = FuncBusinessOnTempAdminNotifyCB;
        cb.OnStartPreviewVideo_ = FuncBusinessOnStartPreviewVideoCB;
        cb.OnStopPreviewVideo_ = FuncBusinessOnStopPreviewVideoCB;
        cb.OnStartPreviewVideoNotify_ = FuncBusinessOnStartPreviewVideoNotifyCB;
        cb.OnStopPreviewVideoNotify_ = FuncBusinessOnStopPreviewVideoNotifyCB;
        cb.OnSwitchMainVideo_ = FuncBusinessOnSwitchMainVideoCB;
        cb.OnSwitchMainVideoNotify_ = FuncBusinessOnSwitchMainVideoNotifyCB;
        cb.OnEnableVideo_ = FuncBusinessOnEnableVideoCB;
        cb.OnEnableVideoNotify_ = FuncBusinessOnEnableVideoNotifyCB;
        cb.OnCaptureVideoState_ = FuncBusinessOnCaptureVideoStateCB;
        cb.OnCaptureVideoStateNotify_ = FuncBusinessOnCaptureVideoStateNotifyCB;
        cb.OnVideoDeviceChangedNotify_ = FuncBusinessOnVideoDeviceChangedNotifyCB;
        cb.OnVideoShowNameChangedNotify_ = FuncBusinessOnVideoShowNameChangedNotifyCB;
        cb.OnKickoutAttendee_ = FuncBusinessOnKickoutAttendeeCB;
        cb.OnKickoutAttendeeNotify_ = FuncBusinessOnKickoutAttendeeNotifyCB;
        cb.OnUpdateAttendeeName_ = FuncBusinessOnUpdateAttendeeNameCB;
        cb.OnUpdateAttendeeNameNotify_ = FuncBusinessOnUpdateAttendeeNameNotifyCB;
        cb.OnRelayMsgToOne_ = FuncBusinessOnRelayMsgToOneCB;
        cb.OnRelayMsgToOneNotify_ = FuncBusinessOnRelayMsgToOneNotifyCB;
        cb.OnRelayMsgToAll_ = FuncBusinessOnRelayMsgToAllCB;
        cb.OnRelayMsgToAllNotify_ = FuncBusinessOnRelayMsgToAllNotifyCB;
        cb.OnAdminOperConfSetting_ = FuncBusinessOnAdminOperConfSettingCB;
        cb.OnAdminOperConfSettingNotify_ = FuncBusinessOnAdminOperConfSettingNotifyCB;
        cb.OnSetConfPassword_ = FuncBusinessOnSetConfPasswordCB;
        cb.OnSetConfPasswordNotify_ = FuncBusinessOnSetConfPasswordNotifyCB;
        cb.OnStartDesktopShare_ = FuncBusinessOnStartDesktopShareCB;
        cb.OnStartDesktopShareNotify_ = FuncBusinessOnStartDesktopShareNotifyCB;
        cb.OnStopDesktopShare_ = FuncBusinessOnStopDesktopShareCB;
        cb.OnStopDesktopShareNotify_ = FuncBusinessOnStopDesktopShareNotifyCB;
        cb.OnStartPreviewDesktop_ = FuncBusinessOnStartPreviewDesktopCB;
        cb.OnStopPreviewDesktop_ = FuncBusinessOnStopPreviewDesktopCB;
        cb.OnStartPreviewDesktopNotify_ = FuncBusinessOnStartPreviewDesktopNotifyCB;
        cb.OnStopPreviewDesktopNotify_ = FuncBusinessOnStopPreviewDesktopNotifyCB;
        cb.OnAskRemoteDesktopControl_ = FuncBusinessOnAskRemoteDesktopControlCB;
        cb.OnAskRemoteDesktopControlNotify_ = FuncBusinessOnAskRemoteDesktopControlNotifyCB;
        cb.OnAbstainRemoteDesktopControl_ = FuncBusinessOnAbstainRemoteDesktopControlCB;
        cb.OnAbstainRemoteDesktopControlNotify_ = FuncBusinessOnAbstainRemoteDesktopControlNotifyCB;
        cb.OnAnswerRemoteDesktopControl_ = FuncBusinessOnAnswerRemoteDesktopControlCB;
        cb.OnAnswerRemoteDesktopControlNotify_ = FuncBusinessOnAnswerRemoteDesktopControlNotifyCB;
        cb.OnAbortRemoteDesktopControl_ = FuncBusinessOnAbortRemoteDesktopControlCB;
        cb.OnAbortRemoteDesktopControlNotify_ = FuncBusinessOnAbortRemoteDesktopControlNotifyCB;
        cb.OnLaunchSignin_ = FuncBusinessOnLaunchSigninCB;
        cb.OnLaunchSigninNotify_ = FuncBusinessOnLaunchSigninNotifyCB;
        cb.OnSignin_ = FuncBusinessOnSigninCB;
        cb.OnSigninNotify_ = FuncBusinessOnSigninNotifyCB;
        cb.OnOperSubGroup_ = FuncBusinessOnOperSubGroupCB;
        cb.OnOperSubGroupNotify_ = FuncBusinessOnOperSubGroupNotifyCB;
        cb.OnCancelSubGroup_ = FuncBusinessOnCancelSubGroupCB;
        cb.OnCancelSubGroupNotify_ = FuncBusinessOnCancelSubGroupNotifyCB;
        cb.OnOpRemoteDesktopShared_ = FuncBusinessOnOpRemoteDesktopSharedCB;
        cb.OnOpRemoteDesktopSharedNotify_ = FuncBusinessOnOpRemoteDesktopSharedNotifyCB;
        cb.OnStartPlayback_ = FuncBusinessOnStartPlaybackCB;
        cb.OnStartPlaybackNotify_ = FuncBusinessOnStartPlaybackNotifyCB;
        cb.OnStopPlayback_ = FuncBusinessOnStopPlaybackCB;
        cb.OnStopPlaybackNotify_ = FuncBusinessOnStopPlaybackNotifyCB;
        cb.OnStartMediaPlay_ = FuncBusinessOnStartMediaPlayCB;
        cb.OnStartMediaPlayNotify_ = FuncBusinessOnStartMediaPlayNotifyCB;
        cb.OnStopMediaPlay_ = FuncBusinessOnStopMediaPlayCB;
        cb.OnStopMediaPlayNotify_ = FuncBusinessOnStopMediaPlayNotifyCB;
        cb.OnPauseMediaPlay_ = FuncBusinessOnPauseMediaPlayCB;
        cb.OnPauseMediaPlayNotify_ = FuncBusinessOnPauseMediaPlayNotifyCB;
        
        lm_conf_business_set_observer(c_object_, &cb);
    }
    return self;
}

- (void)addDelegate:(id<ConfBusinessWrapperDelegate>)business_delegate {
    [delegate_array_ addObject:business_delegate];
}

- (void)removeDelegate:(id<ConfBusinessWrapperDelegate>)business_delegate {
    [delegate_array_ removeObject:business_delegate];
}

- (int)entryConference:(NSString *)json_address
         id_conference:(uint32_t)id_conference
              conf_tag:(uint32_t)conf_tag
              conf_psw:(NSString *)conf_psw
                is_md5:(BOOL)is_md5
               account:(NSString *)account
                id_org:(uint32_t)id_org
                  name:(NSString *)name {
    return lm_conf_business_EntryConference(c_object_,
                                            [json_address UTF8String],
                                            id_conference, conf_tag,
                                            [conf_psw UTF8String],
                                            is_md5 ? 1 : 0,
                                            [account UTF8String], id_org,
                                            [name UTF8String]);
}

- (int)leaveConference {
    return lm_conf_business_LeaveConference(c_object_);
}

- (int)applySpeak:(BOOL)apply {
    return lm_conf_business_ApplySpeak(c_object_, apply ? 1 : 0);
}

- (int)accreditSpeak:(NSString *)account
            accredit:(BOOL)accredit {
    return lm_conf_business_AccreditSpeak(c_object_, [account UTF8String], accredit ? 1 : 0);
}

- (int)applyDataOper:(BOOL)apply {
    return lm_conf_business_ApplyDataOper(c_object_, apply ? 1 : 0);
}


- (int)accreditDataOper:(NSString *)account
               accredit:(BOOL)accredit {
    return lm_conf_business_AccreditDataOper(c_object_, [account UTF8String], accredit ? 1 : 0);
}

- (int)applyDataSync:(BOOL)apply {
    return lm_conf_business_ApplyDataSync(c_object_, apply ? 1 : 0);
}

- (int)accreditDataSync:(NSString *)account
               accredit:(BOOL)accredit {
    return lm_conf_business_AccreditDataSync(c_object_, [account UTF8String], accredit ? 1 : 0);
}

- (int)dataSyncCommand:(NSString *)command {
    return lm_conf_business_DataSyncCommand(c_object_, [command UTF8String]);
}

- (int)applyTempAdmin:(BOOL)apply {
    return lm_conf_business_ApplyTempAdmin(c_object_, apply ? 1 : 0);
}

- (int)accreditTempAdmin:(NSString *)account
                accredit:(BOOL)accredit {
    return lm_conf_business_AccreditTempAdmin(c_object_, [account UTF8String], accredit ? 1 : 0);
}

- (int)authTempAdmin:(NSString *)admin_psw {
    return lm_conf_business_AuthTempAdmin(c_object_, [admin_psw UTF8String]);
}

- (int)startPreviewVideo:(NSString *)shower
                identity:(DeviceIdentity)identity
                 context:(uint64_t)context {
    return lm_conf_business_StartPreviewVideo(c_object_, [shower UTF8String], identity, context);
}

- (int)stopPreviewVideo:(NSString *)shower
               identity:(DeviceIdentity)identity
                context:(uint64_t)context {
    return lm_conf_business_StopPreviewVideo(c_object_, [shower UTF8String], identity, context);
}

- (int)switchMainVideo:(DeviceIdentity)identity {
    return lm_conf_business_SwitchMainVideo(c_object_, identity);
}

- (int)enableVideo:(BOOL)enabled {
    return lm_conf_business_EnableVideo(c_object_, enabled ? 1 : 0);
}

- (int)captureVideoState:(DeviceIdentity)identity
                 enabled:(BOOL)enabled {
    return lm_conf_business_CaptureVideoState(c_object_, identity, enabled ? 1 : 0);
}

- (int)updateVideoShowName:(DeviceIdentity)identity
                      name:(NSString *)name {
    return lm_conf_business_UpdateVideoShowName(c_object_, identity, [name UTF8String]);
}

- (int)kickOutAttendee:(NSString *)account {
    return lm_conf_business_KickOutAttendee(c_object_, [account UTF8String]);
}

- (int)updataAttendeeName:(NSString *)name {
    return lm_conf_business_UpdataAttendeeName(c_object_, [name UTF8String]);
}

- (int)relayMsgToOne:(NSString *)receiver
                 msg:(NSString *)msg {
    return lm_conf_business_RelayMsgToOne(c_object_, [receiver UTF8String], [msg UTF8String]);
}

- (int)relayMsgToAll:(NSString *)msg {
    return lm_conf_business_RelayMsgToAll(c_object_, [msg UTF8String]);
}

- (int)adminOperConfSetting:(int)cmd
                  cmd_value:(int)cmd_value {
    return lm_conf_business_AdminOperConfSetting(c_object_, cmd, cmd_value);
}

- (int)setConfPassword:(NSString *)psw {
    return lm_conf_business_SetConfPassword(c_object_, [psw UTF8String]);
}

- (int)startDesktopShare {
    return lm_conf_business_StartDesktopShare(c_object_);
}

- (int)stopDesktopShare {
    return lm_conf_business_StopDesktopShare(c_object_);
}

- (int)startPreviewDesktop:(NSString *)sharer
                   context:(uint64_t)context {
    return lm_conf_business_StartPreviewDesktop(c_object_, [sharer UTF8String], context);
}

- (int)stopPreviewDesktop:(NSString *)sharer
                  context:(uint64_t)context {
    return lm_conf_business_StopPreviewDesktop(c_object_, [sharer UTF8String], context);
}

- (int)askRemoteDesktopControl:(NSString *)sharer {
    return lm_conf_business_AskRemoteDesktopControl(c_object_, [sharer UTF8String]);
}

- (int)abstainRemoteDesktopControl:(NSString *)sharer {
    return lm_conf_business_AbstainRemoteDesktopControl(c_object_, [sharer UTF8String]);
}

- (int)answerRemoteDesktopControl:(NSString *)controller
                            allow:(BOOL)allow {
    return lm_conf_business_AnswerRemoteDesktopControl(c_object_, [controller UTF8String], allow ? 1 : 0);
}

- (int)abortRemoteDesktopControl:(NSString *)controller {
    return lm_conf_business_AbortRemoteDesktopControl(c_object_, [controller UTF8String]);
}

- (int)launchSignin:(enum LMConferenceSigninType)signin_type {
    return lm_conf_business_LaunchSignin(c_object_, signin_type);
}

- (int)signin:(BOOL)is_signin {
    return lm_conf_business_Signin(c_object_, is_signin ? 1 : 0);
}

- (int)operSubGroup:(NSArray<NSString*> *)account_vector
        id_subgroup:(uint32_t)id_subgroup {
    const char** accounts = malloc(sizeof(const char*) * [account_vector count]);
    for (int i = 0; i < [account_vector count]; i++) {
        accounts[i] = [[account_vector objectAtIndex:i] UTF8String];
    }
    int ret = lm_conf_business_OperSubGroup(c_object_, accounts, (int)[account_vector count], id_subgroup);
    free(accounts);
    return ret;
}

- (int)cancelSubGroup {
    return lm_conf_business_CancelSubGroup(c_object_);
}

- (int)opRemoteDesktopShared:(NSString *)sharer
                    is_start:(BOOL)is_start {
    return lm_conf_business_OpRemoteDesktopShared(c_object_, [sharer UTF8String], is_start ? 1 : 0);
}

- (int)startPlayback:(NSString *)file_name {
    return lm_conf_business_StartPlayback(c_object_, [file_name UTF8String]);
}

- (int)stopPlayback {
    return lm_conf_business_StopPlayback(c_object_);
}

- (int)startMediaPlay:(NSString *)file_name
                flags:(int)media_flag {
    return lm_conf_business_StartMediaPlay(c_object_, [file_name UTF8String], media_flag);
}

- (int)stopMediaPlay {
    return lm_conf_business_StopMediaPlay(c_object_);
}

- (int)pauseMediaPlay:(BOOL)paused {
    return lm_conf_business_PauseMediaPlay(c_object_, paused ? 1 : 0);
}

@end
