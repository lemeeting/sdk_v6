//
//  lm_conf_as_confattribute_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface ASConfAttributeWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

+ (ASConfAttributeWrapper*) from_conf_base;

- (id)initWithCObject:(native_object_t)c_object;

- (int)orgId;
- (int)conferenceId;
- (uint32_t)tag;
- (int)maxAttendees;
- (NSString *)name;

- (BOOL)isFree;
- (BOOL)isLock;
- (BOOL)isDisableText;
- (BOOL)isDisableRecord;
- (BOOL)isDisableBrowVideo;

@end
