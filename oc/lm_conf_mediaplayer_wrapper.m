//
//  lm_conf_mediaplayer_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_mediaplayer_wrapper.h"
void CALLBACK FuncMediaPlayOnMediaPlayOpenCB(
                                             int error, int identity, void* clientdata) {
    ConfMediaPlayerWrapper* wrapper = (__bridge ConfMediaPlayerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfMediaPlayerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onMediaPlayOpen:identity:)]) {
            [delegate onMediaPlayOpen:error identity:identity];
        }
    }
}

void CALLBACK FuncMediaPlayOnMediaPlayErrorCB(
                                              int error, int identity, void* clientdata) {
    ConfMediaPlayerWrapper* wrapper = (__bridge ConfMediaPlayerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfMediaPlayerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onMediaPlayError:identity:)]) {
            [delegate onMediaPlayError:error identity:identity];
        }
    }
}

void CALLBACK FuncMediaPlayOnMediaPlayCompleteCB(
                                                 int identity, void* clientdata) {
    ConfMediaPlayerWrapper* wrapper = (__bridge ConfMediaPlayerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfMediaPlayerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onMediaPlayComplete:)]) {
            [delegate onMediaPlayComplete:identity];
        }
    }
}

void CALLBACK FuncMediaPlayOnMediaPlayDurationChangedCB(
                                                        int identity, int64_t duration, void* clientdata) {
    ConfMediaPlayerWrapper* wrapper = (__bridge ConfMediaPlayerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfMediaPlayerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onMediaPlayDurationChanged:duration:)]) {
            [delegate onMediaPlayDurationChanged:identity duration:duration];
        }
    }
}

void CALLBACK FuncMediaPlayOnMediaPlayPositionChangedCB(
                                                        int identity, int64_t position, void* clientdata) {
    ConfMediaPlayerWrapper* wrapper = (__bridge ConfMediaPlayerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfMediaPlayerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onMediaPlayPositionChanged:position:)]) {
            [delegate onMediaPlayPositionChanged:identity position:position];
        }
    }
}

@implementation ConfMediaPlayerWrapper
@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfMediaPlayerCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnMediaPlayOpen_ = FuncMediaPlayOnMediaPlayOpenCB;
        cb.OnMediaPlayComplete_ = FuncMediaPlayOnMediaPlayCompleteCB;
        cb.OnMediaPlayDurationChanged_ = FuncMediaPlayOnMediaPlayDurationChangedCB;
        cb.OnMediaPlayError_ = FuncMediaPlayOnMediaPlayErrorCB;
        cb.OnMediaPlayPositionChanged_ = FuncMediaPlayOnMediaPlayPositionChangedCB;
        lm_conf_mediaplay_set_callback(c_object_, &cb);
    }
    return self;
}

- (void)addDelegate:(id<ConfMediaPlayerWrapperDelegate>)mediaplayer_delegate {
    [delegate_array_ addObject:mediaplayer_delegate];
}

- (void)removeDelegate:(id<ConfMediaPlayerWrapperDelegate>)mediaplayer_delegate {
    [delegate_array_ removeObject:mediaplayer_delegate];
}

- (int)openURL:(NSString*)url identity:(int*)identity {
    return lm_conf_mediaplay_Open_url(c_object_, [url UTF8String], identity);
}

- (int)open:(NSString*)filename identity:(int*)identity {
    return lm_conf_mediaplay_Open(c_object_, [filename UTF8String], identity);
}

- (int)close:(int)identity {
    return lm_conf_mediaplay_Close(c_object_, identity);
}

- (int)play:(int)identity timeout:(int)timeoutS {
    return lm_conf_mediaplay_Play(c_object_, identity, timeoutS);
}

- (int)stop:(int)identity {
    return lm_conf_mediaplay_Stop(c_object_, identity);
}

- (int)pause:(int)identity {
    return lm_conf_mediaplay_Pause(c_object_, identity);
}

- (int)playStatus:(int)identity running:(BOOL*)running {
    int v = 0;
    int ret = lm_conf_mediaplay_PlayStatus(c_object_, identity, &v);
    *running = !!v;
    return ret;
}

- (int)stopStatus:(int)identity stopped:(BOOL*)stopped {
    int v = 0;
    int ret = lm_conf_mediaplay_StopStatus(c_object_, identity, &v);
    *stopped = !!v;
    return ret;
}

- (int)pauseStatus:(int)identity paused:(BOOL*)paused {
    int v = 0;
    int ret = lm_conf_mediaplay_PauseStatus(c_object_, identity, &v);
    *paused = !!v;
    return ret;
}

- (int)getCurrentPosition:(int)identity position:(int64_t*)positionMS {
    return lm_conf_mediaplay_GetCurrentPosition(c_object_, identity, positionMS);
}

- (int)setCurrentPosition:(int)identity position:(int64_t)positionMS {
    return lm_conf_mediaplay_SetCurrentPosition(c_object_, identity, positionMS);
}

- (int)getDuration:(int)identity duration:(int64_t*)durationMS {
    return lm_conf_mediaplay_GetDuration(c_object_, identity, durationMS);
}

@end
