//
//  lm_conf_video_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "lm_conf_apple_defines.h"
#include "conf_wrapper_interface.h"

@protocol ConfVideoWrapperDelegate <NSObject>
@optional
- (void)onDisconnectVideoServer:(int)result;

- (void)onAddLocalPreview:(DeviceIdentity)id_device
                idPreview:(int)id_preview
                  context:(uint64_t)context
                    error:(int)error;

- (void)onVideoDeviceChanged:(DeviceIdentity)change_device
                       isAdd:(BOOL)is_add
                     devNums:(int)devnums
                   mainDevId:(DeviceIdentity)main_dev_id;

- (void)onLocalVideoResolutionChanged:(DeviceIdentity)id_device
                            idPreview:(int)previewindex
                                width:(int)width
                               height:(int)height;

- (void)onRemoteVideoResolutionChanged:(NSString*)account
                              DevId:(DeviceIdentity)id_device
                             idPreview:(int)previewindex
                                 width:(int)width
                                height:(int)height;

@end

@interface ConfVideoWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfVideoWrapperDelegate>)video_delegate;

- (void)removeDelegate:(id<ConfVideoWrapperDelegate>)video_delegate;

- (int)addLocalPreview:(DeviceIdentity)id_device
               windows:(PlatformView *)windows
               context:(uint64_t)context
               z_order:(unsigned int)z_order
                  left:(float)left
                   top:(float)top
                 right:(float)right
                bottom:(float)bottom
            id_preview:(int *)id_preview;


- (int)configureLocalPreview:(DeviceIdentity)id_device
                  id_preview:(int)id_preview
                     z_order:(unsigned int)z_order
                        left:(float)left
                         top:(float)top
                       right:(float)right
                      bottom:(float)bottom;

- (int)changeLocalPreviewWindow:(DeviceIdentity)id_device
                     id_preview:(int)id_preview
                        windows:(PlatformView *)windows
                        z_order:(unsigned int)z_order
                           left:(float)left
                            top:(float)top
                          right:(float)right
                         bottom:(float)bottom;

- (int)removeLocalPreview:(DeviceIdentity)id_device
               id_preview:(int)id_preview;

- (int)enableMirrorLocalPreview:(DeviceIdentity)id_device
                     id_preview:(int)id_preview
                         enable:(BOOL)enable
                   mirror_xaxis:(BOOL)mirror_xaxis
                   mirror_yaxis:(BOOL)mirror_yaxis;

- (int)mirrorLocalPreviewState:(DeviceIdentity)id_device
                               id_preview:(int)id_preview
                               enable:(BOOL*)enable
                               mirror_xaxis:(BOOL*)mirror_xaxis
                               mirror_yaxis:(BOOL*)mirror_yaxis;

- (int)rotateLocalPreview:(DeviceIdentity)id_device
               id_preview:(int)id_preview
                 rotation:(int)rotation;

- (int)rotateLocalPreviewState:(DeviceIdentity)id_device
                    id_preview:(int)id_preview
                      rotation:(int*)rotation;

- (int)getLocalPreviewResolution:(DeviceIdentity)id_device
                           width:(int*)width
                          height:(int*)height;

- (int)addRemotePreview:(NSString *)account
            deviceId:(DeviceIdentity)id_device
                windows:(PlatformView *)windows
                z_order:(unsigned int)z_order
                   left:(float)left
                    top:(float)top
                  right:(float)right
                 bottom:(float)bottom
             id_preview:(int *)id_preview;

- (int)configureRemotePreview:(NSString *)account
                     deviceId:(DeviceIdentity)id_device
                   id_preview:(int)id_preview
                      z_order:(unsigned int)z_order
                         left:(float)left
                          top:(float)top
                        right:(float)right
                       bottom:(float)bottom;

- (int)changeRemotePreviewWindow:(NSString *)account
                        deviceId:(DeviceIdentity)id_device
                      id_preview:(int)id_preview
                         windows:(PlatformView *)windows
                         z_order:(unsigned int)z_order
                            left:(float)left
                             top:(float)top
                           right:(float)right
                          bottom:(float)bottom;

- (int)removeRemotePreview:(NSString *)account
                  deviceId:(DeviceIdentity)id_device
                id_preview:(int)id_preview;

- (int)enableMirrorRemotePreview:(NSString *)account
                        deviceId:(DeviceIdentity)id_device
                      id_preview:(int)id_preview
                          enable:(BOOL)enable
                    mirror_xaxis:(BOOL)mirror_xaxis
                    mirror_yaxis:(BOOL)mirror_yaxis;

- (int)mirrorRemotePreviewState:(NSString *)account
                       deviceId:(DeviceIdentity)id_device
                     id_preview:(int)id_preview
                         enable:(BOOL*)enable
                   mirror_xaxis:(BOOL*)mirror_xaxis
                   mirror_yaxis:(BOOL*)mirror_yaxis;

- (int)rotateRemotePreview:(NSString *)account
                  deviceId:(DeviceIdentity)id_device
                id_preview:(int)id_preview
                  rotation:(int)rotation;

- (int)rotateRemotePreviewState:(NSString *)account
                       deviceId:(DeviceIdentity)id_device
                     id_preview:(int)id_preview
                       rotation:(int*)rotation;

- (int)enableRemotePreviewColorEnhancement:(NSString *)account
                                  deviceId:(DeviceIdentity)id_device
                                    enable:(BOOL)enable;

- (int)remotePreviewColorEnhancementState:(NSString *)account
                                 deviceId:(DeviceIdentity)id_device
                                   enable:(BOOL*)enable;

- (int)enableRemotePreview:(NSString *)account
                  deviceId:(DeviceIdentity)id_device
                    enable:(BOOL)enable;

- (int)getRemotePreviewState:(NSString *)account
                    deviceId:(DeviceIdentity)id_device
                      enable:(BOOL *)enable;

- (int)getRemotePreviewResolution:(NSString *)account
                         deviceId:(DeviceIdentity)id_device
                            width:(int*)width
                           height:(int*)height;

- (int)numOfDevice;

- (int)enumDevices:(DeviceIdentity*)id_devices
              nums:(int*)nums;

- (int)getDeviceName:(DeviceIdentity)id_device
                name:(char *)name name_len:(int)name_len
                guid:(char *)guid guid_len:(int)guid_len;

- (int)getDeviceIdWithGuid:(NSString *)guid
                  deviceId:(DeviceIdentity*)id_device;

- (int)getDeviceFormat:(DeviceIdentity)id_device
                pixels:(unsigned int *)pixels
                 count:(int *)count;

- (int)switchMobiledevicesCamera;

- (DeviceIdentity)getMobiledevicesActiveCameraId;

- (BOOL)isCaptureing:(DeviceIdentity)id_device;

- (int)setVideoConfig:(DeviceIdentity)id_device
          videoConfig:(const struct LMVideoConfig *)config;

- (int)getVideoConfig:(DeviceIdentity)id_device
          videoConfig:(struct LMVideoConfig *)config;

- (int)enableDeflickering:(DeviceIdentity)id_device
                   enable:(BOOL)enable;

- (int)deflickeringState:(DeviceIdentity)id_device
                  enable:(BOOL*)enable;

- (int)getCaptureDeviceSnapshot:(DeviceIdentity)id_device
                       fileName:(NSString *)file_name;

- (int)getRenderSnapshot:(NSString *)account
                deviceId:(DeviceIdentity)id_device
              id_preview:(int)id_preview
                fileName:(NSString *)file_name;

- (int)setShowNameInVideo:(BOOL)enable;

- (BOOL)getShowNameInVideoState;

- (int)enableDebugVideo:(BOOL)enable;

- (BOOL)getDebugVideoState;

- (int)getCameraType:(DeviceIdentity)id_device
          cameraType:(enum LMCameraType*)type;

- (int)addIPCamera:(const struct LMIPCameraInfo*)info
           channel:(int)channel
        mainStream:(BOOL)is_main_stream
              name:(NSString*)name;

- (int)removeIPCamera:(int)identity
              channel:(int)channel
           mainStream:(BOOL)is_main_stream;

- (int)addRAWDataCamera:(int)identity name:(NSString*)name;
- (int)removeRAWDataCamera:(int)identity;
- (int)incomingRAWData:(int)identity
               rawdata:(const void*)data
                length:(size_t)data_len
                 width:(int)width
                height:(int)height
                  type:(enum LMRawVideoType)type;

@end
