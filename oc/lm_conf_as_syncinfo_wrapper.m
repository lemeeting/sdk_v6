//
//  lm_conf_as_syncinfo_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_as_syncinfo_wrapper.h"
#import "lm_conf_base_wrapper.h"
#import "lm_conf_engine_wrapper.h"

@implementation ASConfSyncInfoWrapper

+ (ASConfSyncInfoWrapper*) from_conf_base {
    native_object_t conf_base_cobject = [[[ConfEngineWrapper conf_engine] base_wrapper] cobject];
    native_object_t cobject = NULL;
    if (lm_conf_base_get_conf_sync_info(conf_base_cobject, &cobject) != 0) {
        return nil;
    }
    return [[ASConfSyncInfoWrapper alloc] initWithCObject:cobject];
}

- (id)init {
    if ((self = [super init])) {
        owner_ = YES;
        lm_create_as_conference_sync_info_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = NO;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_as_conference_sync_info_instance(c_object_);
}

- (BOOL)isSync {
    return !!lm_as_conference_sync_info_is_sync(c_object_);
}

- (NSString*)syncer {
    return [NSString stringWithUTF8String:lm_get_as_conference_sync_info_syncer(c_object_)];
}

- (NSString*)syncData {
    return [NSString stringWithUTF8String:lm_get_as_conference_sync_info_sync_data(c_object_)];
}

@end
