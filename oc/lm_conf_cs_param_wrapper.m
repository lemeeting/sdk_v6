//
//  lm_conf_cs_param_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_param_wrapper.h"

@implementation CSParamWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_param_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_param_instance(c_object_);
}

- (int)getKeyCount {
    return lm_get_cs_param_count(c_object_);
}

- (int)getKeyList:(int*)keys count:(int)count {
    return lm_get_cs_param_keys(c_object_, keys, count);
}

- (int)getIntValue:(int)key {
    int value = 0;
    lm_get_cs_param_int_value(c_object_, key, &value);
    return value;
}

- (unsigned int)getUIntValue:(int)key {
    unsigned int value = 0;
    lm_get_cs_param_uint_value(c_object_, key, &value);
    return value;
}

- (NSString *)getStringValue:(int)key {
    char sss[512] = {0};
    lm_get_cs_param_string_value(c_object_, key, sss, 512);
    return [NSString stringWithUTF8String:sss];
}

- (CFAbsoluteTime)getTimeValue:(int)key {
    int64_t value = 0;
    lm_get_cs_param_time_value(c_object_, key, &value);
    return value;
}

- (void)setIntValue:(int)key value:(int)value {
    lm_set_cs_param_int_value(c_object_, key, value);
}

- (void)setUIntValue:(int)key value:(unsigned int)value {
    lm_set_cs_param_uint_value(c_object_, key, value);
}

- (void)setStringValue:(int)key value:(NSString*)value {
    lm_set_cs_param_string_value(c_object_, key, [value UTF8String]);
}

- (void)setTimeValue:(int)key value:(CFAbsoluteTime)value {
    lm_set_cs_param_time_value(c_object_, key, value);
}

@end
