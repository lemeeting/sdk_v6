//
//  lm_conf_business_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@class ASConfAttendeeWrapper;
@class ASConfAttributeWrapper;
@class ASConfRealTimeInfoWrapper;
@class ASConfSyncInfoWrapper;

@protocol ConfBusinessWrapperDelegate <NSObject>

@optional

- (void)onDisconnect:(int)result;

- (void)onEntryConference:(int)result;

- (void)onLeaveConference:(int)result;

- (void)onGetConference:(ASConfAttributeWrapper *)attribute;

- (void)onUpdateConerence:(ASConfAttributeWrapper *)attribute;

- (void)onRemoveConerence:(uint32_t)id_conference;

- (void)onGetConferenceRealTimeInfo:(ASConfRealTimeInfoWrapper *)info;

- (void)onGetConferenceSyncInfo:(ASConfSyncInfoWrapper *)info;

- (void)onAddSelfAttendee:(ASConfAttendeeWrapper *)attendee;

- (void)onAttendeeOnline:(ASConfAttendeeWrapper *)attendee;

- (void)onAttendeeOffline:(NSString *)account;

- (void)onUpdateAttendee:(ASConfAttendeeWrapper *)old_attendee
                attendee:(ASConfAttendeeWrapper *)attendee;

- (void)onRemoveAttendee:(NSString *)account;

- (void)onAddConfAdmin:(NSString *)admin;

- (void)onRemoveConfAdmin:(NSString *)account;

- (void)onAddConfDefaultAttendee:(NSString *)account;

- (void)onRemoveConfDefaultAttendee:(NSString *)account;

- (void)onApplySpeakOper:(int)result
                 account:(NSString *)account
               new_state:(uint32_t)new_state
               old_state:(uint32_t)old_state
                   apply:(BOOL)apply;

- (void)onAccreditSpeakOper:(int)result
                    account:(NSString *)account
                  new_state:(uint32_t)new_state
                  old_state:(uint32_t)old_state
                   accredit:(BOOL)accredit;

- (void)onSpeakNotify:(NSString *)account
            new_state:(uint32_t)new_state
            old_state:(uint32_t)old_state
                speak:(BOOL)is_speak;

- (void)onApplyDataOper:(int)result
                account:(NSString *)account
              new_state:(uint32_t)new_state
              old_state:(uint32_t)old_state
                  apply:(BOOL)apply;

- (void)onAccreditDataOper:(int)result
                   account:(NSString *)account
                 new_state:(uint32_t)new_state
                 old_state:(uint32_t)old_state
                  accredit:(BOOL)accredit;

- (void)onDataOpNotify:(NSString *)account
             new_state:(uint32_t)new_state
             old_state:(uint32_t)old_state
            is_data_op:(BOOL)is_data_op;


- (void)onApplyDataSync:(int)result
                account:(NSString *)account
              new_state:(uint32_t)new_state
              old_state:(uint32_t)old_state
                  apply:(BOOL)apply;

- (void)onAccreditDataSync:(int)result
                   account:(NSString *)account
                 new_state:(uint32_t)new_state
                 old_state:(uint32_t)old_state
                  accredit:(BOOL)accredit;

- (void)onSyncOpNotify:(NSString *)account
             new_state:(uint32_t)new_state
             old_state:(uint32_t)old_state
                  sync:(BOOL)is_sync;


- (void)onDataSyncCommand:(int)result;

- (void)onDataSyncCommandNotify:(NSString *)syncer
                           data:(NSString *)sync_data;

- (void)onApplyTempAdmin:(int)result
                 account:(NSString *)account
               new_state:(uint32_t)new_state
               old_state:(uint32_t)old_state
                   apply:(BOOL)apply;

- (void)onAccreditTempAdmin:(int)result
                    account:(NSString *)account
                  new_state:(uint32_t)new_state
                  old_state:(uint32_t)old_state
                   accredit:(BOOL)accredit;

- (void)onAuthTempAdmin:(int)result
                account:(NSString *)account;

- (void)onTempAdminNotify:(NSString *)account
                new_state:(uint32_t)new_state
                old_state:(uint32_t)old_state
                 is_admin:(BOOL)is_admin;

- (void)onStartPreviewVideo:(int)result
                     shower:(NSString *)shower
                      identity:(DeviceIdentity)identity
                    context:(uint64_t)context;

- (void)onStopPreviewVideo:(int)result
                    shower:(NSString *)shower
                  identity:(DeviceIdentity)identity
                   context:(uint64_t)context;

- (void)onStartPreviewVideoNotify:(NSString *)shower
                            identity:(DeviceIdentity)identity;

- (void)onStopPreviewVideoNotify:(NSString *)shower
                           identity:(DeviceIdentity)identity;

- (void)onSwitchMainVideo:(int)result
                old_identity:(DeviceIdentity)old_identity
                    identity:(DeviceIdentity)identity;

- (void)onSwitchMainVideoNotify:(NSString *)account
                   old_identity:(DeviceIdentity)old_identity
                       identity:(DeviceIdentity)identity;

- (void)onEnableVideo:(int)result
              enabled:(BOOL)enabled;

- (void)onEnableVideoNotify:(NSString *)account
                    enabled:(BOOL)enabled;

- (void)onCaptureVideoState:(int)result
                   identity:(DeviceIdentity)identity
                    enabled:(BOOL)enabled;

- (void)onCaptureVideoStateNotify:(NSString *)account
                         identity:(DeviceIdentity)identity
                          enabled:(BOOL)enabled;

- (void)onVideoDeviceChangedNotify:(NSString *)account
                           devnums:(uint16_t)dev_nums
                          devstate:(uint16_t)dev_state
                      maindevidentity:(DeviceIdentity)identity;

- (void)onVideoShowNameChangedNotify:(NSString *)account
                            identity:(DeviceIdentity)identity
                                name:(NSString *)name;

- (void)onKickoutAttendee:(int)result
                  account:(NSString *)account;

- (void)onKickoutAttendeeNotify:(NSString *)oper_account
                        account:(NSString *)account;

- (void)onUpdateAttendeeName:(int)result
                     newname:(NSString *)newname;

- (void)onUpdateAttendeeNameNotify:(NSString *)account
                           newname:(NSString *)newname;

- (void)onRelayMsgToOne:(int)result
               receiver:(NSString *)receiver;

- (void)onRelayMsgToOneNotify:(NSString *)sender
                     receiver:(NSString *)receiver
                          msg:(NSString *)msg;

- (void)onRelayMsgToAll:(int)result;

- (void)onRelayMsgToAllNotify:(NSString *)sender
                          msg:(NSString *)msg;

- (void)onAdminOperConfSetting:(int)result
                           cmd:(int)cmd
                         value:(int)cmd_value;

- (void)onAdminOperConfSettingNotify:(NSString *)account
                                 cmd:(int)cmd
                               value:(int)cmd_value;

- (void)onSetConfPassword:(int)result;

- (void)onSetConfPasswordNotify:(NSString *)account;

- (void)onStartDesktopShare:(int)result;

- (void)onStartDesktopShareNotify:(NSString *)account;

- (void)onStopDesktopShare:(int)result;

- (void)onStopDesktopShareNotify:(NSString *)account;

- (void)onStartPreviewDesktop:(int)result
                       sharer:(NSString *)sharer
                      context:(uint64_t)context;

- (void)onStopPreviewDesktop:(int)result
                      sharer:(NSString *)sharer
                     context:(uint64_t)context;

- (void)onStartPreviewDesktopNotify:(NSString *)sharer;
- (void)onStopPreviewDesktopNotify:(NSString *)sharer;

- (void)onAskRemoteDesktopControl:(int)result
                           sharer:(NSString*)sharer;
- (void)onAskRemoteDesktopControlNotify:(NSString*)controller
                                 sharer:(NSString*)sharer;

- (void)onAbstainRemoteDesktopControl:(int)result
                               sharer:(NSString*)sharer;
- (void)onAbstainRemoteDesktopControlNotify:(NSString*)controller
                                     sharer:(NSString*)sharer;

- (void)onAnswerRemoteDesktopControl:(int)result
                          controller:(NSString*)controller
                               allow:(BOOL)allow;
- (void)onAnswerRemoteDesktopControlNotify:(NSString*)controller
                                    sharer:(NSString*)sharer
                                     allow:(BOOL)allow;

- (void)onAbortRemoteDesktopControl:(int)result
                         controller:(NSString*)controller;
- (void)onAbortRemoteDesktopControlNotify:(NSString*)controller
                                   sharer:(NSString*)sharer;


- (void)onLaunchSignin:(int)result
                  type:(enum LMConferenceSigninType)signin_type
                  time:(CFAbsoluteTime)launch_or_stop_time;

- (void)onLaunchSigninNotify:(NSString *)launcher
                        type:(enum LMConferenceSigninType)signin_type
                        time:(CFAbsoluteTime)launch_or_stop_time;

- (void)onSignin:(int)result
       is_signin:(BOOL)is_signin
     launch_time:(CFAbsoluteTime)launch_time
            time:(CFAbsoluteTime)signin_or_cancel_time;


- (void)onSigninNotify:(NSString *)signiner
             is_signin:(BOOL)is_signin
           launch_time:(CFAbsoluteTime)launch_time
                  time:(CFAbsoluteTime)signin_or_cancel_time;

- (void)onOperSubGroup:(int)result;

- (void)onOperSubGroupNotify:(NSString*)oper
                    subgroup:(uint32_t)id_subgroup;

- (void)onCancelSubGroup:(int)result;

- (void)onCancelSubGroupNotify:(NSString*)oper;

- (void)onOpRemoteDesktopShared:(int)result
                         sharer:(NSString*)sharer
                          start:(BOOL)is_start;

- (void)onOpRemoteDesktopSharedNotify:(NSString*)oper
                               sharer:(NSString*)sharer
                                start:(BOOL)is_start;

- (void)onStartPlayback:(int)result
                filenme:(NSString*)file_name;

- (void)onStartPlaybackNotify:(NSString*)oper;

- (void)onStopPlayback:(int)result;

- (void)onStopPlaybackNotify:(NSString*)oper;

- (void)onStartMediaPlay:(int)result
                fileName:(NSString*)file_name
                   flags:(int)media_flag;

- (void)onStartMediaPlayNotify:(NSString*)oper
                      fileName:(NSString*)file_name
                         flags:(int)media_flag;

- (void)onStopMediaPlay:(int)result;

- (void)onStopMediaPlayNotify:(NSString*)oper;

- (void)onPauseMediaPlay:(int)result
                  paused:(BOOL)paused;

- (void)onPauseMediaPlayNotify:(NSString*)oper
                        paused:(BOOL)paused;
@end

@interface ConfBusinessWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;


- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfBusinessWrapperDelegate>)business_delegate;
- (void)removeDelegate:(id<ConfBusinessWrapperDelegate>)business_delegate;

- (int)entryConference:(NSString *)json_address
         id_conference:(uint32_t)id_conference
              conf_tag:(uint32_t)conf_tag
              conf_psw:(NSString *)conf_psw
                is_md5:(BOOL)is_md5
               account:(NSString *)account
                id_org:(uint32_t)id_org
                  name:(NSString *)name;

- (int)leaveConference;

- (int)applySpeak:(BOOL)apply;

- (int)accreditSpeak:(NSString *)account
            accredit:(BOOL)accredit;

- (int)applyDataOper:(BOOL)apply;

- (int)accreditDataOper:(NSString *)account
               accredit:(BOOL)accredit;

- (int)applyDataSync:(BOOL)apply;

- (int)accreditDataSync:(NSString *)account
               accredit:(BOOL)accredit;

- (int)dataSyncCommand:(NSString *)command;

- (int)applyTempAdmin:(BOOL)apply;

- (int)accreditTempAdmin:(NSString *)account
                accredit:(BOOL)accredit;

- (int)authTempAdmin:(NSString *)admin_psw;

- (int)startPreviewVideo:(NSString *)shower
                identity:(DeviceIdentity)identity
                 context:(uint64_t)context;

- (int)stopPreviewVideo:(NSString *)shower
               identity:(DeviceIdentity)identity
                context:(uint64_t)context;

- (int)switchMainVideo:(DeviceIdentity)identity;

- (int)enableVideo:(BOOL)enabled;

- (int)captureVideoState:(DeviceIdentity)identity
                 enabled:(BOOL)enabled;

- (int)updateVideoShowName:(DeviceIdentity)identity
                      name:(NSString *)name;

- (int)kickOutAttendee:(NSString *)account;

- (int)updataAttendeeName:(NSString *)name;

- (int)relayMsgToOne:(NSString *)receiver
                 msg:(NSString *)msg;

- (int)relayMsgToAll:(NSString *)msg;

- (int)adminOperConfSetting:(int)cmd
                  cmd_value:(int)cmd_value;

- (int)setConfPassword:(NSString *)psw;

- (int)startDesktopShare;

- (int)stopDesktopShare;

- (int)startPreviewDesktop:(NSString *)sharer
                   context:(uint64_t)context;

- (int)stopPreviewDesktop:(NSString *)sharer
                  context:(uint64_t)context;

- (int)askRemoteDesktopControl:(NSString *)sharer;
- (int)abstainRemoteDesktopControl:(NSString *)sharer;

- (int)answerRemoteDesktopControl:(NSString *)controller
                            allow:(BOOL)allow;
- (int)abortRemoteDesktopControl:(NSString *)controller;

- (int)launchSignin:(enum LMConferenceSigninType)signin_type;

- (int)signin:(BOOL)is_signin;

- (int)operSubGroup:(NSArray<NSString*> *)account_vector
        id_subgroup:(uint32_t)id_subgroup;

- (int)cancelSubGroup;

- (int)opRemoteDesktopShared:(NSString *)sharer
                    is_start:(BOOL)is_start;

- (int)startPlayback:(NSString *)file_name;

- (int)stopPlayback;

- (int)startMediaPlay:(NSString *)file_name
                flags:(int)media_flag;
- (int)stopMediaPlay;
- (int)pauseMediaPlay:(BOOL)paused;

@end
