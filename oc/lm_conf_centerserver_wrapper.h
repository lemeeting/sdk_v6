//
//  lm_conf_centerserver_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@class CSParamWrapper;
@class CSApplyMsgWrapper;
@class CSConfCodeWrapper;
@class CSConfRoomWrapper;
@class CSOrgInfoWrapper;
@class CSOrgUserWrapper;
@class CSPushMsgWrapper;
@class CSUserBindWrapper;
@class CSUserInfoWrapper;
@class CSRealTimeConfWrapper;

@protocol ConfCenterServerWrapperDelegate <NSObject>

@optional

- (void)onDisconnectCenter:(int)result;

- (void)onGetAuthCode:(int)result
                param:(CSParamWrapper *)param
                 code:(NSString *)strCode;

- (void)onRegUserInfo:(int)result
                param:(CSParamWrapper *)param
                 info:(CSUserInfoWrapper *)info;

- (void)onLogin:(int)result
          param:(CSParamWrapper *)param
           info:(CSUserInfoWrapper *)userInfo;

- (void)onLogout:(int)result
           param:(CSParamWrapper *)param;

- (void)onPrepareLoginConf:(int)result
                     param:(CSParamWrapper *)param
                    confid:(uint32_t)id_conf
                   address:(NSString *)jsonAddress;

- (void)onGetRealConf:(int)result
                param:(CSParamWrapper *)param
             infoList:(NSArray<CSRealTimeConfWrapper *> *)info_list;

- (void)onGetUserInfo:(int)result
                param:(CSParamWrapper *)param
                 info:(CSUserInfoWrapper *)info;

- (void)onUpdateUserInfo:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSUserInfoWrapper *)info;

- (void)onRemoveUserInfo:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSUserInfoWrapper *)info;

- (void)onGetUserInfoList:(int)result
                    param:(CSParamWrapper *)param
                     info:(CSUserInfoWrapper *)info
                 infoList:(NSArray<CSUserInfoWrapper *> *)info_list;

- (void)onGetOrgInfo:(int)result
               param:(CSParamWrapper *)param
                info:(CSOrgInfoWrapper *)info;

- (void)onUpdateOrgInfo:(int)result
                  param:(CSParamWrapper *)param
                   info:(CSOrgInfoWrapper *)info;

- (void)onGetOrgInfoList:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSOrgInfoWrapper *)info
                infoList:(NSArray<CSOrgInfoWrapper *> *)info_list;

- (void)onGetUserBind:(int)result
                param:(CSParamWrapper *)param
                 info:(CSUserBindWrapper *)info;

- (void)onAddUserBind:(int)result
                param:(CSParamWrapper *)param
                 info:(CSUserBindWrapper *)info;

- (void)onUpdateUserBind:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSUserBindWrapper *)info;

- (void)onRemoveUserBind:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSUserBindWrapper *)info;

- (void)onGetUserBindList:(int)result
                    param:(CSParamWrapper *)param
                     info:(CSUserBindWrapper *)info
                 infoList:(NSArray<CSUserBindWrapper *> *)info_list;

- (void)onGetOrgUser:(int)result
               param:(CSParamWrapper *)param
                info:(CSOrgUserWrapper *)info;

- (void)onAddOrgUser:(int)result
               param:(CSParamWrapper *)param
                info:(CSOrgUserWrapper *)info;

- (void)onUpdateOrgUser:(int)result
                  param:(CSParamWrapper *)param
                   info:(CSOrgUserWrapper *)info;

- (void)onRemoveOrgUser:(int)result
                  param:(CSParamWrapper *)param
                   info:(CSOrgUserWrapper *)info;

- (void)onGetOrgUserList:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSOrgUserWrapper *)info
                infoList:(NSArray<CSOrgUserWrapper *> *)info_list;

- (void)onGetConfRoom:(int)result
                param:(CSParamWrapper *)param
                 info:(CSConfRoomWrapper *)info;

- (void)onAddConfRoom:(int)result
                param:(CSParamWrapper *)param
                 info:(CSConfRoomWrapper *)info;

- (void)onUpdateConfRoom:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSConfRoomWrapper *)info;

- (void)onRemoveConfRoom:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSConfRoomWrapper *)info;

- (void)onGetConfRoomList:(int)result
                    param:(CSParamWrapper *)param
                     info:(CSConfRoomWrapper *)info
                 infoList:(NSArray<CSConfRoomWrapper *> *)info_list;

- (void)onGetConfCode:(int)result
                param:(CSParamWrapper *)param
                 info:(CSConfCodeWrapper *)info;

- (void)onAddConfCode:(int)result
                param:(CSParamWrapper *)param
                 info:(CSConfCodeWrapper *)info;

- (void)onUpdateConfCode:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSConfCodeWrapper *)info;

- (void)onRemoveConfCode:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSConfCodeWrapper *)info;

- (void)onGetConfCodeList:(int)result
                    param:(CSParamWrapper *)param
                     info:(CSConfCodeWrapper *)info
                 infoList:(NSArray<CSConfCodeWrapper *> *)info_list;

- (void)onGetApplyMsg:(int)result
                param:(CSParamWrapper *)param
                 info:(CSApplyMsgWrapper *)info;

- (void)onAddApplyMsg:(int)result
                param:(CSParamWrapper *)param
                 info:(CSApplyMsgWrapper *)info;

- (void)onUpdateApplyMsg:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSApplyMsgWrapper *)info;

- (void)onRemoveApplyMsg:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSApplyMsgWrapper *)info;

- (void)onGetApplyMsgList:(int)result
                    param:(CSParamWrapper *)param
                     info:(CSApplyMsgWrapper *)info
                 infoList:(NSArray<CSApplyMsgWrapper *> *)info_list;

- (void)onGetPushMsg:(int)result
               param:(CSParamWrapper *)param
                info:(CSPushMsgWrapper *)info;

- (void)onAddPushMsg:(int)result
               param:(CSParamWrapper *)param
                info:(CSPushMsgWrapper *)info;

- (void)onUpdatePushMsg:(int)result
                  param:(CSParamWrapper *)param
                   info:(CSPushMsgWrapper *)info;

- (void)onRemovePushMsg:(int)result
                  param:(CSParamWrapper *)param
                   info:(CSPushMsgWrapper *)info;

- (void)onGetPushMsgList:(int)result
                   param:(CSParamWrapper *)param
                    info:(CSPushMsgWrapper *)info
                infoList:(NSArray<CSPushMsgWrapper *> *)info_list;

- (void)onNoticePushMsg:(CSParamWrapper *)param
                   info:(CSPushMsgWrapper *)info;


@end

@interface ConfCenterServerWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfCenterServerWrapperDelegate>)to_center_delegate;
- (void)removeDelegate:(id<ConfCenterServerWrapperDelegate>)to_center_delegate;

- (int)setCenterAddress:(NSString *)address;
- (NSString *)getCenterAddress;

- (int)getAuthCode:(CSParamWrapper *)param;
- (int)regUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info;

- (int)login:(CSParamWrapper *)param id_org:(uint32_t)id_org account:(NSString *)account psw:(NSString *)psw;
- (int)logout:(CSParamWrapper *)param;

- (int)prepareLoginConf:(CSParamWrapper *)param id_conf:(uint32_t)id_conf;
- (int)getRealConf:(CSParamWrapper *)param int_list:(NSArray<NSNumber*> *)listConfID;

- (int)getUserInfo:(CSParamWrapper *)param account:(NSString *)account;
- (int)updateUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info;
- (int)removeUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info;
- (int)getUserInfoList:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info;

- (int)getOrgInfo:(CSParamWrapper *)param id_org:(uint32_t)id_org;
- (int)updateOrgInfo:(CSParamWrapper *)param info:(CSOrgInfoWrapper *)info;
- (int)getOrgInfoList:(CSParamWrapper *)param info:(CSOrgInfoWrapper *)info;

- (int)getUserBind:(CSParamWrapper *)param name:(NSString *)name;
- (int)addUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info;
- (int)updateUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info;
- (int)removeUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info;
- (int)getUserBindList:(CSParamWrapper *)param info:(CSUserBindWrapper *)info;

- (int)getOrgUser:(CSParamWrapper *)param id_org:(uint32_t)id_org account:(NSString *)account;
- (int)addOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info;
- (int)updateOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info;
- (int)removeOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info;
- (int)getOrgUserList:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info;

- (int)getConfRoom:(CSParamWrapper *)param id_conf:(uint32_t)id_conf;
- (int)addConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info;
- (int)updateConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info;
- (int)removeConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info;
- (int)getConfRoomList:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info;

- (int)getConfCode:(CSParamWrapper *)param code:(NSString *)codeid;
- (int)addConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info;
- (int)updateConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info;
- (int)removeConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info;
- (int)getConfCodeList:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info;

- (int)getApplyMsg:(CSParamWrapper *)param id_req:(uint32_t)id_req;
- (int)addApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info;
- (int)updateApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info;
- (int)removeApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info;
- (int)getApplyMsgList:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info;

- (int)getPushMsg:(CSParamWrapper *)param msg_id:(uint32_t)msg_id;
- (int)addPushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info;
- (int)updatePushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info;
- (int)removePushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info;
- (int)getPushMsgList:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info;

- (int)getConfCode:(CSParamWrapper *)param  idConf:(int)id_conf;

@end
