//
//  lm_conf_playback_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@protocol ConfPlaybackWrapperDelegate <NSObject>
@optional

- (void)onPlaybackOpen:(int)error;
- (void)onPlaybackPlay:(int)error;
- (void)onPlaybackPause:(int)error;
- (void)onPlaybackContinue:(int)error;
- (void)onPlaybackComplete;

- (void)onRepairBegin:(int)error;
- (void)onRepairEnd:(int)error;
- (void)onRepairProgress:(int64_t)currpos duration:(int64_t)duration;

@end

@interface ConfPlaybackWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfPlaybackWrapperDelegate>)playback_delegate;

- (void)removeDelegate:(id<ConfPlaybackWrapperDelegate>)playback_delegate;

- (int)startPlayback:(NSString *)file_name;
- (int)playPlayback:(NSArray<NSString *> *)playaccounts;
- (int)pausePlayback;
- (int)continuePlayback;
- (int)stopPlayback;

- (int)startRepair:(NSString *)file_name;
- (int)stopRepair;

- (BOOL)isPlaybacking;
- (BOOL)isRepairing;

- (int)getRecSessionCount:(enum LMRecSessionType)type;
- (int)enumRecSessionInfo:(enum LMRecSessionType)type info:(struct LMRecSessionInfo*)info count:(int*)count;


@end
