//
//  lm_conf_as_realtimeconfinfo_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface ASConfRealTimeInfoWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

+ (ASConfRealTimeInfoWrapper*) from_conf_base;

- (id)initWithCObject:(native_object_t)c_object;

- (uint32_t)flags;
- (int)conferenceId;
- (int)onlineAttendee;
- (BOOL)isLaunchSignin;
- (BOOL)isStopSignin;

- (enum LMConferenceSigninType)signinType;
- (CFAbsoluteTime)lanunchSigninTime;
- (CFAbsoluteTime)stopSigninTime;

- (BOOL)isFree;
- (BOOL)isLock;
- (BOOL)isOpen;
- (BOOL)isHide;
- (BOOL)isForceSync;
- (BOOL)isDisableText;
- (BOOL)isDisableRecord;
- (BOOL)isDisableBrowVideo;
- (BOOL)isPlayback;
- (BOOL)isDisableAllMicrophone;
- (BOOL)isAllMuted;
- (BOOL)isLockScreen;
- (BOOL)isMediaPlay;
- (BOOL)isMediaPause;

@end
