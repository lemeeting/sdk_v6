//
//  lm_conf_as_realtimeconfinfo_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_as_realtimeconfinfo_wrapper.h"
#import "lm_conf_base_wrapper.h"
#import "lm_conf_engine_wrapper.h"

@implementation ASConfRealTimeInfoWrapper

+ (ASConfRealTimeInfoWrapper*) from_conf_base {
    native_object_t conf_base_cobject = [[[ConfEngineWrapper conf_engine] base_wrapper] cobject];
    native_object_t cobject = NULL;
    if (lm_conf_base_get_real_time_conf_info(conf_base_cobject, &cobject) != 0) {
        return nil;
    }
    return [[ASConfRealTimeInfoWrapper alloc] initWithCObject:cobject];
}

- (id)init {
    if ((self = [super init])) {
        owner_ = YES;
        lm_create_cs_real_conf_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = NO;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_real_conf_instance(c_object_);
}

- (uint32_t)flags {
    return lm_get_as_real_time_conference_info_flags(c_object_);
}

- (int)conferenceId {
    return lm_get_as_real_time_conference_info_conference_id(c_object_);
}

- (int)onlineAttendee {
    return lm_get_as_real_time_conference_info_online_attendees(c_object_);
}

- (BOOL)isLaunchSignin {
    return !!lm_as_real_time_conference_info_is_lanunch_signin(c_object_);
}

- (BOOL)isStopSignin {
    return !!lm_as_real_time_conference_info_is_stop_signin(c_object_);
}

- (enum LMConferenceSigninType)signinType {
    return (enum LMConferenceSigninType)(lm_get_as_real_time_conference_info_signin_type(c_object_));
}

- (CFAbsoluteTime)lanunchSigninTime {
    return lm_get_as_real_time_conference_info_lanunch_signin_time(c_object_);
}

- (CFAbsoluteTime)stopSigninTime {
    return lm_get_as_real_time_conference_info_stop_signin_time(c_object_);
}

- (BOOL)isFree {
    return LM_CHECK_OPTION([self flags], kConferenceFree);
}

- (BOOL)isLock {
    return LM_CHECK_OPTION([self flags], kConferenceLock);
}

- (BOOL)isOpen {
    return LM_CHECK_OPTION([self flags], kConferenceOpen);
}

- (BOOL)isHide {
    return LM_CHECK_OPTION([self flags], kConferenceHide);
}

- (BOOL)isForceSync {
    return LM_CHECK_OPTION([self flags], kConferenceForceSync);
}

- (BOOL)isDisableText {
    return LM_CHECK_OPTION([self flags], kConferenceDisableText);
}

- (BOOL)isDisableRecord {
    return LM_CHECK_OPTION([self flags], kConferenceDisableRecord);
}

- (BOOL)isDisableBrowVideo {
    return LM_CHECK_OPTION([self flags], kConferenceDisableBrowVideo);
}

- (BOOL)isPlayback {
    return LM_CHECK_OPTION([self flags], kConferencePlayback);
}

- (BOOL)isDisableAllMicrophone {
    return LM_CHECK_OPTION([self flags], kConferenceDisableMicrophone);
}

- (BOOL)isAllMuted {
    return LM_CHECK_OPTION([self flags], kConferenceMuted);
}

- (BOOL)isLockScreen {
    return LM_CHECK_OPTION([self flags], kConferenceLockScreen);
}

- (BOOL)isMediaPlay {
    return LM_CHECK_OPTION([self flags], kConferenceMediaPlay);
}

- (BOOL)isMediaPause {
    return LM_CHECK_OPTION([self flags], kConferenceMediaPause);
}


@end
