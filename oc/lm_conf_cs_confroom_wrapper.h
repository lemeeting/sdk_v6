//
//  lm_conf_cs_confroom_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSConfRoomWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

- (int)getVersionid;
- (int)getDeleteflag;
- (int)getConfid;
- (int)getConftype;
- (int)getWorldid;
- (int)getGroupid;
- (int)getServerid;

- (NSString *)getConfname;
- (NSString *)getConfpassword;
- (NSString *)getManagepassword;

- (CFAbsoluteTime)getStarttime;
- (CFAbsoluteTime)getEndtime;

- (int)getMaxusercount;
- (int)getMaxspeakercount;

- (NSString *)getCreator;
- (NSString *)getMender;

- (CFAbsoluteTime)getCreatetime;
- (CFAbsoluteTime)getModifytime;

- (int)getConfflag;
- (NSString *)getSettingjson;
- (NSString *)getExtendjson;


- (void)setConfid:(int)value;
- (void)setConftype:(int)value;
- (void)setWorldid:(int)value;
- (void)setGroupid:(int)value;
- (void)setServerid:(int)value;

- (void)setConfname:(NSString*)value;
- (void)setConfpassword:(NSString*)value;
- (void)setManagepassword:(NSString*)value;

- (void)setStarttime:(CFAbsoluteTime)value;
- (void)setEndtime:(CFAbsoluteTime)value;

- (void)setMaxusercount:(int)value;
- (void)setMaxspeakercount:(int)value;
- (void)setConfflag:(int)value;
- (void)setExtendjson:(NSString*)value;

@end
