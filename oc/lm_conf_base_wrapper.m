//
//  lm_conf_base_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_base_wrapper.h"

@implementation ConfBaseWrapper
@synthesize cobject = c_object_;

+ (NSString*)errorToString:(int)error {
    char serror[256] = { 0 };
    lm_get_error_string(error, serror, 256);
    return [NSString stringWithUTF8String:serror];    
}

+ (void)defaultInitOptions:(struct LMInitOptions*)options {
    lm_get_default_init_options(options);
}

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
    }
    return self;
}

- (BOOL)initializ {
    return lm_conf_base_init(c_object_) == 0;
}

- (BOOL)initializWithOptions:(const struct LMInitOptions*)options {
    return lm_conf_base_init_with_options(c_object_, options) == 0;
}

- (int)terminate {
    lm_conf_base_terminate(c_object_);
    return 0;
}

- (void)setUseTcp:(BOOL)is_use_tcp {
    lm_conf_base_set_use_tcp(c_object_, is_use_tcp ? 1 : 0);
}

- (BOOL)isUseTcp {
    int v = 0;
    lm_conf_base_is_use_tcp(c_object_, &v);
    return !!v;
}

- (void)setProxy:(const struct LMProxyOptions *)options {
    lm_conf_base_set_proxy(c_object_, options);
}

- (void)getProxy:(struct LMProxyOptions *)options {
    lm_conf_base_get_proxy(c_object_, options);
}

- (void)setScreenCopyFps:(int)fps {
    lm_conf_base_set_screen_copy_fps(c_object_, fps);
}

- (int)getScreenCopyFps {
    return lm_conf_base_get_screen_copy_fps(c_object_);
}

- (int)getPerfMon:(struct LMPerMonitor *)perfmon {
    return lm_conf_base_get_per_monitor(c_object_, perfmon);
}

- (PlatformView *)createVideoPreviewWindows:(CGRect)frameRect {
    return (__bridge PlatformView*)lm_conf_base_CreateVideoPreviewWindows(c_object_,
                                                  frameRect.origin.x, frameRect.origin.y, frameRect.size.width, frameRect.size.height);
}

- (void)deleteVideoPreviewWindow:(PlatformView *)windows {
    lm_conf_base_DeleteVideoPreviewWindows(c_object_, (__bridge void*)windows);
}

@end
