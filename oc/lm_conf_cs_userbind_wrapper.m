//
//  lm_conf_cs_userbind_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_userbind_wrapper.h"

@implementation CSUserBindWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_user_bind_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_user_bind_instance(c_object_);
}

@end
