//
//  lm_conf_cs_orguser_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSOrgUserWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

- (int)getVersionid;
- (int)getDeleteflag;
- (int)getOrgid;
- (NSString *)getUseraccount;
- (int)getUsertype;
- (int)getUserstatus;
- (int)getBinduser;

- (NSString *)getUsername;
- (NSString *)getPassword;
- (NSString *)getPhone;
- (NSString *)getEmail;

- (CFAbsoluteTime)getBirthday;
- (CFAbsoluteTime)getCreatetime;

- (NSString *)getSettingjson;
- (NSString *)getExtendjson;

- (int64_t)getUserperm;


- (void)setOrgid:(int)value;
- (void)setUseraccount:(NSString *)value;
- (void)setUsertype:(int)value;
- (void)setUserstatus:(int)value;
- (void)setBinduser:(int)value;

- (void)setUsername:(NSString *)value;
- (void)setPassword:(NSString *)value;
- (void)setPhone:(NSString *)value;
- (void)setEmail:(NSString *)value;

- (void)setBirthday:(CFAbsoluteTime)value;
- (void)setCreatetime:(CFAbsoluteTime)value;

- (void)setSettingjson:(NSString *)value;
- (void)setExtendjson:(NSString *)value;

- (void)setUserperm:(int64_t)value;

@end
