//
//  lm_conf_ipcamera_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_ipcamera_wrapper.h"
void CALLBACK FuncIPCameraOnSearchWithLANCB(
                                            int error, const struct LMIPCameraAddress* results, int count, void* clientdata) {
    ConfIPCameraWrapper* wrapper = (__bridge ConfIPCameraWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfIPCameraWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onSearchWithLAN:results:count:)]) {
            [delegate onSearchWithLAN:error results:results count:count];
        }
    }
}

void CALLBACK FuncIPCameraOnAddIPCameraCB(
                                          int error, int identity, const struct LMIPCameraInfo* info, void* clientdata) {
    ConfIPCameraWrapper* wrapper = (__bridge ConfIPCameraWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfIPCameraWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddIPCamera:identity:info:)]) {
            [delegate onAddIPCamera:error identity:identity info:info];
        }
    }
}

@implementation ConfIPCameraWrapper
@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfIPCameraCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnAddIPCamera_ = FuncIPCameraOnAddIPCameraCB;
        cb.OnSearchWithLAN_ = FuncIPCameraOnSearchWithLANCB;
        lm_conf_ipcamera_set_callback(c_object_, &cb);        
    }
    return self;
}

- (void)addDelegate:(id<ConfIPCameraWrapperDelegate>)ipcamera_delegate {
    [delegate_array_ addObject:ipcamera_delegate];
}

- (void)removeDelegate:(id<ConfIPCameraWrapperDelegate>)ipcamera_delegate {
    [delegate_array_ removeObject:ipcamera_delegate];
}

- (int)searchWithLAN:(NSString* )multicast_address {
    return lm_conf_ipcamera_SearchWithLAN(c_object_, [multicast_address UTF8String]);
    
}

- (int)addIPCamera:(const struct LMIPCameraAddress* )address
              auth:(const struct LMIPCameraAuthInfo*)auth
          identity:(int*)identity {
    return lm_conf_ipcamera_AddIPCamera(c_object_, address, auth, identity);
    
}

- (int)removeIPCamera:(int)identity {
    return lm_conf_ipcamera_RemoveIPCamera(c_object_, identity);
}

@end
