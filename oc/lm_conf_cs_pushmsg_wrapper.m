//
//  lm_conf_cs_pushmsg_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_pushmsg_wrapper.h"

@implementation CSPushMsgWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_push_msg_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_push_msg_instance(c_object_);
}

- (int)getVersionid {
    return lm_get_cs_push_msg_version_id(c_object_);    
}

- (int)getDeleteflag {
    return lm_get_cs_push_msg_delete_flag(c_object_);
}

- (int)getMsgid {
    return lm_get_cs_push_msg_id(c_object_);
}

- (int)getMsgtype {
    return lm_get_cs_push_msg_type(c_object_);
}

- (int)getMsgstate {
    return lm_get_cs_push_msg_state(c_object_);
}

- (int)getMsgresult {
    return lm_get_cs_push_msg_result(c_object_);
}

- (int)getSendorgid {
    return lm_get_cs_push_msg_send_org_id(c_object_);
}

- (NSString *)getSendaccount {
    return [NSString stringWithUTF8String:lm_get_cs_push_msg_send_account(c_object_)];
}

- (CFAbsoluteTime)getSendtime {
    return lm_get_cs_push_msg_send_time(c_object_);
}

- (CFAbsoluteTime)getEndtime {
    return lm_get_cs_push_msg_end_time(c_object_);
}

- (NSString *)getMsgtopic {
    return [NSString stringWithUTF8String:lm_get_cs_push_msg_topic(c_object_)];
}

- (NSString *)getMsgdata {
    return [NSString stringWithUTF8String:lm_get_cs_push_msg_data(c_object_)];
}

- (NSString *)getMsgreceiver {
    return [NSString stringWithUTF8String:lm_get_cs_push_msg_receiver(c_object_)];
}

- (NSString *)getExtendjson {
    return [NSString stringWithUTF8String:lm_get_cs_push_msg_extend_json(c_object_)];
}

- (void)setMsgid:(int)value {
    lm_set_cs_push_msg_id(c_object_, value);
}

- (void)setMsgtype:(int)value {
    lm_set_cs_push_msg_type(c_object_, value);
}

- (void)setMsgstate:(int)value {
    lm_set_cs_push_msg_state(c_object_, value);
}

- (void)setMsgresult:(int)value {
    lm_set_cs_push_msg_result(c_object_, value);
}

- (void)setSendorgid:(int)value {
    lm_set_cs_push_msg_send_org_id(c_object_, value);
}

- (void)setSendaccount:(NSString *)value {
    lm_set_cs_push_msg_send_account(c_object_, [value UTF8String]);
}

- (void)setSendtime:(CFAbsoluteTime)value {
    lm_set_cs_push_msg_send_time(c_object_, value);
}

- (void)setEndtime:(CFAbsoluteTime)value {
    lm_set_cs_push_msg_end_time(c_object_, value);
}

- (void)setMsgtopic:(NSString *)value {
    lm_set_cs_push_msg_topic(c_object_, [value UTF8String]);
}

- (void)setMsgdata:(NSString *)value {
    lm_set_cs_push_msg_data(c_object_, [value UTF8String]);
}

- (void)setMsgreceiver:(NSString *)value {
    lm_set_cs_push_msg_receiver(c_object_, [value UTF8String]);
}

- (void)setExtendjson:(NSString *)value {
    lm_set_cs_push_msg_extend_json(c_object_, [value UTF8String]);
}


@end
