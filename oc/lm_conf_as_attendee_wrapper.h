//
//  lm_conf_as_attendee_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface ASConfAttendeeWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

+ (ASConfAttendeeWrapper*) from_conf_base:(NSString*)account;
+ (ASConfAttendeeWrapper*) self_from_conf_base;

- (id)initWithCObject:(native_object_t)c_object;

- (int)type;
- (enum LMConferenceClientType)loginType;
- (uint32_t)opsStatus;
- (enum LMConferenceAdminModes)adminPrivilege;
- (int)numOfVoiceDevices;
- (int)numOfVideoDevices;
- (void)enumVideoDevices:(DeviceIdentity*)id_devices nums:(int*)nums;
- (BOOL)isEnableVideoDevice:(DeviceIdentity)id_device;
- (enum LMCameraType)getVideoDeviceType:(DeviceIdentity)id_device;
- (enum LMCameraSubType)getVideoDeviceSubType:(DeviceIdentity)id_device;
- (NSString *)getVideoDeviceShowName:(DeviceIdentity)id_device;
- (DeviceIdentity)mainVideoDeviceId;
- (BOOL)isForbiddenVideoDevice;
- (NSString *)account;
- (NSString *)name;
- (int)enterpriseId;
- (int)accessServerId;
- (int)conferenceId;
- (NSString *)macAddress;
- (CFAbsoluteTime)signinTime;

- (BOOL)isValidVideoDevice:(DeviceIdentity)id_device;

- (BOOL)isAdmin;

- (BOOL)applySpeakOp;
- (BOOL)speakOp;
- (BOOL)applyDataOp;
- (BOOL)dataOp;
- (BOOL)applySyncOp;
- (BOOL)syncOp;
- (BOOL)applyAdminOp;
- (BOOL)sharedDesktopOp;
- (BOOL)playbackOp;
- (BOOL)mediaPlayOp;
- (BOOL)askRemoteControlDesktopStatus;
- (BOOL)inRemoteControlDesktopStatus;

@end
