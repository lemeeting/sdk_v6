//
//  lm_conf_dataserver_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_dataserver_wrapper.h"

void CALLBACK FuncDataServerOnPingResultCB(
                                           uint32_t task_id, uint64_t context, int error,
                                           const struct LMPingResult* result, void* clientdata) {
    ConfDataServerWrapper* wrapper = (__bridge ConfDataServerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfDataServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onPingResult:context:error:result:)]) {
            [delegate onPingResult:task_id context:context error:error result:result];
        }
    }
}

@implementation ConfDataServerWrapper
@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfDataServerCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnPingResult_ = FuncDataServerOnPingResultCB;
        lm_conf_dataserver_set_callback(c_object_, &cb);
        
    }
    return self;
}

- (void)addDelegate:(id<ConfDataServerWrapperDelegate>)dataserver_delegate {
    [delegate_array_ addObject:dataserver_delegate];
}

- (void)removeDelegate:(id<ConfDataServerWrapperDelegate>)dataserver_delegate {
    [delegate_array_ removeObject:dataserver_delegate];
}

- (int)getDataServerCount {
    int count = 0;
    lm_conf_dataserver_get_count(c_object_, &count);
    return count;
}

- (int)enumDataServers:(struct LMDataServerInfo*)data_Servers count:(int*)count {
    return lm_conf_dataserver_get_dataserves(c_object_, data_Servers, count);
}

- (int)getCurrentConnectDataServer:(enum LMDataServerModule)module
                        dataServer:(struct LMDataServerInfo*)data_server {
    return lm_conf_dataserver_GetCurrentConnectDataServer(c_object_, module, data_server);
}

- (int)pingDataServer:(NSString *)address
                 port:(uint16_t)port
        callback_time:(int64_t)callback_time
               useTcp:(BOOL)is_use_udp
              context:(uint64_t)context
               taskId:(uint32_t*)task_id {
    return lm_conf_dataserver_PingDataServer(c_object_, [address UTF8String], port,
                                             callback_time, is_use_udp ? 1 : 0,
                                             context, (int*)(task_id));
}

- (int)stopPingDataServer:(uint32_t)task_id {
    return lm_conf_dataserver_StopPingDataServer(c_object_, task_id);
}

- (int)switchDataServer:(enum LMDataServerModule)module address:(NSString *)address {
    return lm_conf_dataserver_SwitchDataServer(c_object_, module, [address UTF8String]);
}


@end
