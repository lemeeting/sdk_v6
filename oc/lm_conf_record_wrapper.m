//
//  lm_conf_record_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_record_wrapper.h"

void CALLBACK FuncRecordOnStartLocalRecordReadyCB(void* clientdata) {
    ConfRecordWrapper* wrapper = (__bridge ConfRecordWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfRecordWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onStartLocalRecordReady)]) {
            [delegate onStartLocalRecordReady];
        }
    }
}

@implementation ConfRecordWrapper
@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfRecordCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnStartLocalRecordReady_ = FuncRecordOnStartLocalRecordReadyCB;
        lm_conf_record_set_callback(c_object_, &cb);
    }
    return self;
}

- (void)addDelegate:(id<ConfRecordWrapperDelegate>)record_delegate {
    [delegate_array_ addObject:record_delegate];
}

- (void)removeDelegate:(id<ConfRecordWrapperDelegate>)record_delegate {
    [delegate_array_ removeObject:record_delegate];
}

- (int)startLocalRecord:(NSString *)file_name
         recordaccounts:(NSArray<NSString *> *)recordaccounts
                  flags:(int)flags {
    const char** ss = malloc(sizeof(const char*) * [recordaccounts count]);
    int count = 0;
    for(NSString* obj in recordaccounts) {
        ss[count++] = [obj UTF8String];
    }

    int ret = lm_conf_record_StartLocalRecord(c_object_, [file_name UTF8String], ss, count, flags);
    free(ss);
    return ret;
}

- (int)stopLocalRecord {
    return lm_conf_record_StopLocalRecord(c_object_);
}

- (int)startScreenRecord:(NSString *)file_name
                  format:(enum LMScreenRecordFormats)format
                    zoom:(enum LMScreenRecordZoom)zoom
              frame_rate:(float)frame_rate
                      QP:(enum LMScreenRecordQP)qp
                   flags:(int)flags {
    return lm_conf_record_StartScreenRecord(c_object_, [file_name UTF8String], format, zoom, frame_rate, qp, flags);
}

- (int)stopScreenRecord {
    return lm_conf_record_StopScreenRecord(c_object_);
}

- (int)stopRecord {
    return lm_conf_record_StopRecord(c_object_);
}

- (enum LMConfRecordType)currentRecordType {
    return (enum LMConfRecordType)(lm_conf_record_CurrentRecordType(c_object_));
}

@end
