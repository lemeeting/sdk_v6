//
//  lm_conf_mediaplayer_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@protocol ConfMediaPlayerWrapperDelegate <NSObject>
@optional

- (void)onMediaPlayOpen:(int)error identity:(int)identity;
- (void)onMediaPlayError:(int)error identity:(int)identity;
- (void)onMediaPlayComplete:(int)identity;
- (void)onMediaPlayDurationChanged:(int)identity duration:(int64_t)durationMS;
- (void)onMediaPlayPositionChanged:(int)identity position:(int64_t)positionMS;

@end

@interface ConfMediaPlayerWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfMediaPlayerWrapperDelegate>)mediaplayer_delegate;

- (void)removeDelegate:(id<ConfMediaPlayerWrapperDelegate>)mediaplayer_delegate;

- (int)openURL:(NSString*)url identity:(int*)identity;
- (int)open:(NSString*)filename identity:(int*)identity;
- (int)close:(int)identity;

- (int)play:(int)identity timeout:(int)timeoutS;
- (int)stop:(int)identity;
- (int)pause:(int)identity;
- (int)playStatus:(int)identity running:(BOOL*)running;
- (int)stopStatus:(int)identity stopped:(BOOL*)stopped;
- (int)pauseStatus:(int)identity paused:(BOOL*)paused;

- (int)getCurrentPosition:(int)identity position:(int64_t*)positionMS;
- (int)setCurrentPosition:(int)identity position:(int64_t)positionMS;
- (int)getDuration:(int)identity duration:(int64_t*)durationMS;

@end
