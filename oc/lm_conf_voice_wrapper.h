//
//  lm_conf_voice_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@protocol ConfVoiceWrapperDelegate <NSObject>
@optional

- (void)onDisconnectVoiceServer:(int)error;
- (void)onDeviceChange;

@end

@interface ConfVoiceWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfVoiceWrapperDelegate>)voice_delegate;

- (void)removeDelegate:(id<ConfVoiceWrapperDelegate>)voice_delegate;

- (int)setCodecType:(enum LMVoiceCodecModes)codec_type
          delayMode:(enum LMVoiceDelayModes)delay_mode;

- (int)getCodecType:(enum LMVoiceCodecModes*)codec_type
          delayMode:(enum LMVoiceDelayModes*)delay_mode;

- (int)setCodecTypeWithInputDevice:(enum LMVoiceDeviceType)input_type
                         codecType:(enum LMVoiceCodecModes)codec_type
                         delayMode:(enum LMVoiceDelayModes)delay_mode
                              rate:(int)rate;

- (int)getCodecTypeWithInputDevice:(enum LMVoiceDeviceType)input_type
                         codecType:(enum LMVoiceCodecModes*)codec_type
                         delayMode:(enum LMVoiceDelayModes*)delay_mode
                              rate:(int*)rate;

- (BOOL)isValidDelayMode:(enum LMVoiceCodecModes)codec_type
               delayMode:(enum LMVoiceDelayModes)delay_mode;

- (int)getNumOfInputDevices:(int *)nums;

- (int)getNumOfOutputDevices:(int *)nums;

- (int)getInputDeviceName:(int)index
                     name:(char *)name
                 name_len:(int)name_len
                     guid:(char *)guid
                 guid_len:(int)guid_len;

- (int)getOutputDeviceName:(int)index
                      name:(char *)name
                  name_len:(int)name_len
                      guid:(char *)guid
                  guid_len:(int)guid_len;

- (int)getDefaultInputDeviceName:(char *)name
                        name_len:(int)name_len
                            guid:(char *)guid
                        guid_len:(int)guid_len;

- (int)getDefaultOutputDeviceName:(char *)name
                         name_len:(int)name_len
                             guid:(char *)guid
                         guid_len:(int)guid_len;

- (int)getCurrentInputDeviceName:(char *)name
                        name_len:(int)name_len
                            guid:(char *)guid
                        guid_len:(int)guid_len;

- (int)getCurrentOutputDeviceName:(char *)name
                         name_len:(int)name_len
                             guid:(char *)guid
                         guid_len:(int)guid_len;

- (int)getDefaultInputDeviceIndex:(int* )index;
- (int)getDefaultOutputDeviceIndex:(int* )index;
- (int)getCurrentInputDeviceIndex:(int* )index;
- (int)getCurrentOutputDeviceIndex:(int* )index;

- (int)setInputDevice:(int)index;

- (int)setOutputDevice:(int)index;

- (int)setInputVolume:(unsigned int)volume;

- (int)getInputVolume:(unsigned int *)volume;

- (int)setOutputVolume:(unsigned int)volume;

- (int)getOutputVolume:(unsigned int *)volume;

- (int)getInputVoiceLevel:(unsigned int *)level;

- (int)getOutputVoiceLevelForAccount:(NSString *)account
                               level:(unsigned int *)level;
- (int)getOutputVoiceLevel:(int)node level:(unsigned int *)level;

- (int)setLoudspeakerStatus:(BOOL)enabled;

- (int)setEcStatus:(BOOL)enabled
              mode:(enum LMEcModes)mode;

- (int)setNsStatus:(BOOL)enabled
              mode:(enum LMNsModes)mode;

- (int)setAgcStatus:(BOOL)enabled
               mode:(enum LMAgcModes)mode;

- (int)setAecmMode:(enum LMAecmModes)mode
         enableCNG:(BOOL)enableCNG;

- (int)setVADStatus:(BOOL)enabled
               mode:(enum LMVadModes)mode
         disableDTX:(BOOL)disableDTX;

- (int)getLoudspeakerStatus:(BOOL *)enabled;
- (int)getEcStatus:(BOOL *)enabled
              mode:(enum LMEcModes *)mode;

- (int)getNsStatus:(BOOL *)enabled
              mode:(enum LMNsModes *)mode;

- (int)getAgcStatus:(BOOL *)enabled
               mode:(enum LMAgcModes *)mode;

- (int)getAecmMode:(enum LMAecmModes *)mode
        enabledCNG:(BOOL *)enabledCNG;

- (int)getVADStatus:(BOOL *)enabled
               mode:(enum LMVadModes *)mode
        disabledDTX:(BOOL *)disabledDTX;

- (int)enableDriftCompensation:(BOOL)enabled;

- (int)driftCompensationEnabled:(BOOL *)enabled;

- (int)setDelayOffsetMs:(int)offset;

- (int)delayOffsetMs:(int *)offset;

- (int)enableHighPassFilter:(BOOL)enabled;

- (int)isHighPassFilterEnabled:(BOOL *)enabled;

- (int)enableInput:(BOOL)enabled;
- (int)enableOutput:(BOOL)enabled;

- (int)getInputState:(BOOL *)enabled;
- (int)getOutputState:(BOOL *)enabled;

- (int)disablePlaybackInput:(BOOL)disabled;
- (int)disableIPCameraInput:(BOOL)disabled;
- (int)disableMediaPlayerInput:(BOOL)disabled;
- (int)disableExternalDeviceInput:(BOOL)disabled;

- (int)disablePlaybackInputLocalOutput:(BOOL)disabled;
- (int)disableIPCameraInputLocalOutput:(BOOL)disabled;
- (int)disableMediaPlayerInputLocalOutput:(BOOL)disabled;
- (int)disableExternalDeviceInputLocalOutput:(BOOL)disabled;

- (int)getDisablePlaybackInputState:(BOOL *)disabled;
- (int)getDisableIPCameraInputState:(BOOL *)disabled;
- (int)getDisableMediaPlayerInputState:(BOOL *)disabled;
- (int)getDisableExternalDeviceInputState:(BOOL *)disabled;

- (int)getDisablePlaybackInputLocalOutputState:(BOOL *)disabled;
- (int)getDisableIPCameraInputLocalOutputState:(BOOL *)disabled;
- (int)getDisableMediaPlayerInputLocalOutputState:(BOOL *)disabled;
- (int)getDisableExternalDeviceInputLocalOutputState:(BOOL *)disabled;


// Playout file locally
- (int)startPlayingFileLocally:(int *)node
                      fileName:(NSString *)fileName
                          loop:(BOOL)loop
                        format:(enum LMVoiceFileFormats)format
                 volumeScaling:(float)volumeScaling
                  startPointMs:(int)startPointMs
                   stopPointMs:(int)stopPointMs;

- (int)startPlayingFileLocally:(int *)node
                        stream:(const void *)stream
                        length:(int)stream_len
                        format:(enum LMVoiceFileFormats)format
                 volumeScaling:(float)volumeScaling
                  startPointMs:(int)startPointMs
                   stopPointMs:(int)stopPointMs;

- (int)stopPlayingFileLocally:(int)node;

- (int)isPlayingFileLocally:(int)node;

// Use file as microphone input
- (int)startPlayingFileAsMicrophone:(NSString *)fileName
                               loop:(BOOL)loop
                  mixWithMicrophone:(BOOL)mixWithMicrophone
                             format:(enum LMVoiceFileFormats)format
                      volumeScaling:(float)volumeScaling;

- (int)startPlayingFileAsMicrophone:(const void *)stream
                             length:(int)stream_len
                  mixWithMicrophone:(BOOL)mixWithMicrophone
                             format:(enum LMVoiceFileFormats)format
                      volumeScaling:(float)volumeScaling;

- (int)stopPlayingFileAsMicrophone;

- (int)isPlayingFileAsMicrophone;

// Record speaker signal to file
- (int)startRecordingPlayout:(NSString *)account
                    fileName:(NSString *)fileName
                       codec:(enum LMVoiceCodecModes)codec
                maxSizeBytes:(int)maxSizeBytes;

- (int)startRecordingPlayout:(NSString *)account
                      stream:(void *)stream
                      length:(int)stream_len
                       codec:(enum LMVoiceCodecModes)codec;

- (int)stopRecordingPlayout:(NSString *)account;

- (int)startRecordingPlayout:(NSString *)fileName
                       codec:(enum LMVoiceCodecModes)codec
                maxSizeBytes:(int)maxSizeBytes;

- (int)startRecordingPlayout:(void *)stream
                      length:(int)stream_len
                       codec:(enum LMVoiceCodecModes)codec;

- (int)stopRecordingPlayout;

// Record microphone signal to file
- (int)startRecordingMicrophone:(NSString *)fileName
                          codec:(enum LMVoiceCodecModes)codec
                   maxSizeBytes:(int)maxSizeBytes;

- (int)startRecordingMicrophone:(void *)stream
                         length:(int)stream_len
                          codec:(enum LMVoiceCodecModes)codec;

- (int)stopRecordingMicrophone;

- (int)addIPCamera:(const struct LMIPCameraInfo*)info
           channel:(int)camera_channel;

- (int)removeIPCamera:(int)identity
              channel:(int)camera_channel;

- (int)addExternalRAWDataInputDevice:(int)sampleRate
                             channel:(int)channel
                            identity:(int)identity;

- (int)removeExternalRAWDataInputDevice:(int)identity;

- (int)incomingExternalInputRAWData:(int)identity
                               data:(const void*)data
                            datalen:(int)data_len;

- (int)addExternalRTPDataInputDevice:(int)identity;

- (int)removeExternalRTPDataInputDevice:(int)identity;

- (int)incomingExternalInputRTPData:(int)identity
                               data:(const void*)data
                            datalen:(int)data_len
                          delayMode:(enum LMVoiceDelayModes)delay_mode;


//	for udp default kVoiceAdaptiveIO, tcp return -1
- (int)setVoiceIOMode:(enum LMVoiceIOModes)mode;
- (int)getVoiceIOMode:(enum LMVoiceIOModes *)mode;

@end
