//
//  lm_conf_sharedesktop_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "lm_conf_apple_defines.h"
#include "conf_wrapper_interface.h"

@protocol ConfShareDesktopWrapperDelegate <NSObject>
@optional
- (void)OnDisconnectShareServer:(int)error;
@end

@interface ConfShareDesktopWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfShareDesktopWrapperDelegate>)desktop_delegate;

- (void)removeDelegate:(id<ConfShareDesktopWrapperDelegate>)desktop_delegate;

@end
