//
//  lm_conf_engine_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@class ConfBaseWrapper;
@class ConfBusinessWrapper;
@class ConfVideoWrapper;
@class ConfVoiceWrapper;
@class ConfShareDesktopWrapper;
@class ConfRecordWrapper;
@class ConfPlaybackWrapper;
@class ConfMediaPlayerWrapper;
@class ConfDataServerWrapper;
@class ConfCenterServerWrapper;
@class ConfIPCameraWrapper;

@interface ConfEngineWrapper : NSObject {
@private
    native_object_t native_conf_engine_;
    native_object_t native_conf_base_;
    native_object_t native_conf_business_;
    native_object_t native_conf_video_;
    native_object_t native_conf_voice_;
    native_object_t native_conf_sharedesktop_;
    native_object_t native_conf_record_;
    native_object_t native_conf_playback_;
    native_object_t native_conf_mediaplayer_;
    native_object_t native_conf_ipcamera_;
    native_object_t native_conf_dataserver_;
    native_object_t native_conf_centerserver_;
    
    ConfBaseWrapper* base_wrapper_;
    ConfBusinessWrapper* business_wrapper_;
    ConfVideoWrapper* video_wrapper_;
    ConfVoiceWrapper* voice_wrapper_;
    ConfShareDesktopWrapper* sharedeskop_wrapper_;
    ConfRecordWrapper* record_wrapper_;
    ConfPlaybackWrapper* playback_wrapper_;
    ConfMediaPlayerWrapper* mediaplayer_wrapper_;
    ConfDataServerWrapper* dataserver_wrapper_;
    ConfCenterServerWrapper* centerserver_wrapper_;
    ConfIPCameraWrapper* ipcamera_wrapper_;
}

@property(nonatomic, readonly) ConfBaseWrapper* base_wrapper;
@property(nonatomic, readonly) ConfBusinessWrapper* business_wrapper;
@property(nonatomic, readonly) ConfVideoWrapper* video_wrapper;
@property(nonatomic, readonly) ConfVoiceWrapper* voice_wrapper_;
@property(nonatomic, readonly) ConfShareDesktopWrapper* sharedeskop_wrapper;
@property(nonatomic, readonly) ConfRecordWrapper* record_wrapper;
@property(nonatomic, readonly) ConfPlaybackWrapper* playback_wrapper;
@property(nonatomic, readonly) ConfMediaPlayerWrapper* mediaplayer_wrapper;
@property(nonatomic, readonly) ConfDataServerWrapper* dataserver_wrapper;
@property(nonatomic, readonly) ConfCenterServerWrapper* centerserver_wrapper;
@property(nonatomic, readonly) ConfIPCameraWrapper* ipcamera_wrapper;

+ (ConfEngineWrapper*)conf_engine;

@end
