//
//  lm_conf_base_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "lm_conf_apple_defines.h"
#include "conf_wrapper_interface.h"

@interface ConfBaseWrapper : NSObject {
@private
    native_object_t c_object_;
}

@property native_object_t cobject;

+ (NSString*)errorToString:(int)error;

+ (void)defaultInitOptions:(struct LMInitOptions*)options;

- (id)initWithCObject:(native_object_t)c_object;

- (BOOL)initializ;
- (BOOL)initializWithOptions:(const struct LMInitOptions*)options;
- (int)terminate;

- (void)setUseTcp:(BOOL)is_use_tcp;
- (BOOL)isUseTcp;

- (void)setProxy:(const struct LMProxyOptions *)options;
- (void)getProxy:(struct LMProxyOptions *)options;

- (void)setScreenCopyFps:(int)fps;
- (int)getScreenCopyFps;

- (int)getPerfMon:(struct LMPerMonitor *)perfmon;

- (PlatformView *)createVideoPreviewWindows:(CGRect)frameRect;

- (void)deleteVideoPreviewWindow:(PlatformView *)windows;

@end
