//
//  lm_conf_centerserver_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_centerserver_wrapper.h"
#import "lm_conf_cs_param_wrapper.h"
#import "lm_conf_cs_orginfo_wrapper.h"
#import "lm_conf_cs_orguser_wrapper.h"
#import "lm_conf_cs_pushmsg_wrapper.h"
#import "lm_conf_cs_applymsg_wrapper.h"
#import "lm_conf_cs_confcode_wrapper.h"
#import "lm_conf_cs_userbind_wrapper.h"
#import "lm_conf_cs_confroom_wrapper.h"
#import "lm_conf_cs_userinfo_wrapper.h"
#import "lm_conf_cs_realtime_confinfo_wrapper.h"

void CALLBACK FuncToCenterOnDisconnectCenterCB(int result, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onDisconnectCenter:)]) {
            [delegate onDisconnectCenter:result];
        }
    }
}

void CALLBACK FuncToCenterOnGetAuthCodeCB(int result, native_object_t param, const char* code, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetAuthCode:param:code:)]) {
            [delegate onGetAuthCode:result param:cs_param code:[NSString stringWithUTF8String:code]];
        }
    }
}

void CALLBACK FuncToCenterOnRegUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRegUserInfo:param:info:)]) {
            [delegate onRegUserInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnLoginCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onLogin:param:info:)]) {
            [delegate onLogin:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnLogoutCB(int result, native_object_t param, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onLogout:param:)]) {
            [delegate onLogout:result param:cs_param];
        }
    }
}

void CALLBACK FuncToCenterOnPrepareLoginConfCB(int result, native_object_t param,
                                               int id_conf, const char* json_address, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onPrepareLoginConf:param:confid:address:)]) {
            [delegate onPrepareLoginConf:result param:cs_param confid:id_conf address:[NSString stringWithUTF8String:json_address]];
        }
    }
}

void CALLBACK FuncToCenterOnGetRealConfCB(int result, native_object_t param, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSRealTimeConfWrapper* cs_info = [[CSRealTimeConfWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetRealConf:param:infoList:)]) {
            [delegate onGetRealConf:result param:cs_param infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetUserInfo:param:info:)]) {
            [delegate onGetUserInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateUserInfo:param:info:)]) {
            [delegate onUpdateUserInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveUserInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveUserInfo:param:info:)]) {
            [delegate onRemoveUserInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetUserInfoListCB(int result, native_object_t param,
                                              native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserInfoWrapper* cs_info = [[CSUserInfoWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSUserInfoWrapper* cs_info_temp = [[CSUserInfoWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetUserInfoList:param:info:infoList:)]) {
            [delegate onGetUserInfoList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgInfoWrapper* cs_info = [[CSOrgInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetOrgInfo:param:info:)]) {
            [delegate onGetOrgInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateOrgInfoCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgInfoWrapper* cs_info = [[CSOrgInfoWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateOrgInfo:param:info:)]) {
            [delegate onUpdateOrgInfo:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetOrgInfoListCB(int result, native_object_t param,
                                             native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgInfoWrapper* cs_info = [[CSOrgInfoWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSOrgInfoWrapper* cs_info_temp = [[CSOrgInfoWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetOrgInfoList:param:info:infoList:)]) {
            [delegate onGetOrgInfoList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserBindWrapper* cs_info = [[CSUserBindWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetUserBind:param:info:)]) {
            [delegate onGetUserBind:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserBindWrapper* cs_info = [[CSUserBindWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddUserBind:param:info:)]) {
            [delegate onAddUserBind:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserBindWrapper* cs_info = [[CSUserBindWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateUserBind:param:info:)]) {
            [delegate onUpdateUserBind:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveUserBindCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserBindWrapper* cs_info = [[CSUserBindWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveUserBind:param:info:)]) {
            [delegate onRemoveUserBind:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetUserBindListCB(int result, native_object_t param,
                                              native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSUserBindWrapper* cs_info = [[CSUserBindWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSUserBindWrapper* cs_info_temp = [[CSUserBindWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetUserBindList:param:info:infoList:)]) {
            [delegate onGetUserBindList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgUserWrapper* cs_info = [[CSOrgUserWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetOrgUser:param:info:)]) {
            [delegate onGetOrgUser:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgUserWrapper* cs_info = [[CSOrgUserWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddOrgUser:param:info:)]) {
            [delegate onAddOrgUser:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgUserWrapper* cs_info = [[CSOrgUserWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateOrgUser:param:info:)]) {
            [delegate onUpdateOrgUser:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveOrgUserCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgUserWrapper* cs_info = [[CSOrgUserWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveOrgUser:param:info:)]) {
            [delegate onRemoveOrgUser:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetOrgUserListCB(int result, native_object_t param,
                                             native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSOrgUserWrapper* cs_info = [[CSOrgUserWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSOrgUserWrapper* cs_info_temp = [[CSOrgUserWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetOrgUserList:param:info:infoList:)]) {
            [delegate onGetOrgUserList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfRoomWrapper* cs_info = [[CSConfRoomWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConfRoom:param:info:)]) {
            [delegate onGetConfRoom:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfRoomWrapper* cs_info = [[CSConfRoomWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddConfRoom:param:info:)]) {
            [delegate onAddConfRoom:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfRoomWrapper* cs_info = [[CSConfRoomWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateConfRoom:param:info:)]) {
            [delegate onUpdateConfRoom:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveConfRoomCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfRoomWrapper* cs_info = [[CSConfRoomWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveConfRoom:param:info:)]) {
            [delegate onRemoveConfRoom:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetConfRoomListCB(int result, native_object_t param,
                                              native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfRoomWrapper* cs_info = [[CSConfRoomWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSConfRoomWrapper* cs_info_temp = [[CSConfRoomWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConfRoomList:param:info:infoList:)]) {
            [delegate onGetConfRoomList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfCodeWrapper* cs_info = [[CSConfCodeWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConfCode:param:info:)]) {
            [delegate onGetConfCode:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfCodeWrapper* cs_info = [[CSConfCodeWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddConfCode:param:info:)]) {
            [delegate onAddConfCode:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfCodeWrapper* cs_info = [[CSConfCodeWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateConfCode:param:info:)]) {
            [delegate onUpdateConfCode:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveConfCodeCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfCodeWrapper* cs_info = [[CSConfCodeWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveConfCode:param:info:)]) {
            [delegate onRemoveConfCode:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetConfCodeListCB(int result, native_object_t param,
                                              native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSConfCodeWrapper* cs_info = [[CSConfCodeWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSConfCodeWrapper* cs_info_temp = [[CSConfCodeWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetConfCodeList:param:info:infoList:)]) {
            [delegate onGetConfCodeList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSApplyMsgWrapper* cs_info = [[CSApplyMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetApplyMsg:param:info:)]) {
            [delegate onGetApplyMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSApplyMsgWrapper* cs_info = [[CSApplyMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddApplyMsg:param:info:)]) {
            [delegate onAddApplyMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdateApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSApplyMsgWrapper* cs_info = [[CSApplyMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdateApplyMsg:param:info:)]) {
            [delegate onUpdateApplyMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemoveApplyMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSApplyMsgWrapper* cs_info = [[CSApplyMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemoveApplyMsg:param:info:)]) {
            [delegate onRemoveApplyMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetApplyMsgListCB(int result, native_object_t param,
                                              native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSApplyMsgWrapper* cs_info = [[CSApplyMsgWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSApplyMsgWrapper* cs_info_temp = [[CSApplyMsgWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetApplyMsgList:param:info:infoList:)]) {
            [delegate onGetApplyMsgList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnGetPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetPushMsg:param:info:)]) {
            [delegate onGetPushMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnAddPushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onAddPushMsg:param:info:)]) {
            [delegate onAddPushMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnUpdatePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onUpdatePushMsg:param:info:)]) {
            [delegate onUpdatePushMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnRemovePushMsgCB(int result, native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onRemovePushMsg:param:info:)]) {
            [delegate onRemovePushMsg:result param:cs_param info:cs_info];
        }
    }
}

void CALLBACK FuncToCenterOnGetPushMsgListCB(int result, native_object_t param,
                                             native_object_t info, native_object_t* infos, int count, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    NSMutableArray* ns_array = [[NSMutableArray alloc] init];
    for(int i = 0; i < count; i++) {
        CSPushMsgWrapper* cs_info_temp = [[CSPushMsgWrapper alloc] initWithCObject:infos[i]];
        [ns_array addObject:cs_info_temp];
    }
    
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onGetPushMsgList:param:info:infoList:)]) {
            [delegate onGetPushMsgList:result param:cs_param info:cs_info infoList:ns_array];
        }
    }
}

void CALLBACK FuncToCenterOnNoticePushMsgCB(native_object_t param, native_object_t info, void* clientdata) {
    ConfCenterServerWrapper* wrapper = (__bridge ConfCenterServerWrapper*)(clientdata);
    CSParamWrapper* cs_param = [[CSParamWrapper alloc] initWithCObject:param];
    CSPushMsgWrapper* cs_info = [[CSPushMsgWrapper alloc] initWithCObject:info];
    for(id obj in [wrapper delegate_array]) {
        id<ConfCenterServerWrapperDelegate> delegate = obj;
        if([delegate respondsToSelector:@selector(onNoticePushMsg:info:)]) {
            [delegate onNoticePushMsg:cs_param info:cs_info];
        }
    }
}

@implementation ConfCenterServerWrapper

@synthesize delegate_array = delegate_array_;

- (id)init {
    if ((self = [super init])) {
        c_object_ = NULL;
        delegate_array_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        c_object_ = c_object;
        delegate_array_ = [[NSMutableArray alloc] init];
        
        struct ConfToCenterCallback cb;
        memset(&cb, 0, sizeof(cb));
        cb.client_data_ = (__bridge void*)self;
        cb.OnAddApplyMsg_ = FuncToCenterOnAddApplyMsgCB;
        cb.OnAddConfCode_ = FuncToCenterOnAddConfCodeCB;
        cb.OnAddConfRoom_ = FuncToCenterOnAddConfRoomCB;
        cb.OnAddOrgUser_ = FuncToCenterOnAddOrgUserCB;
        cb.OnAddPushMsg_ = FuncToCenterOnAddPushMsgCB;
        cb.OnAddUserBind_ = FuncToCenterOnAddUserBindCB;
        cb.OnDisconnectCenter_ = FuncToCenterOnDisconnectCenterCB;
        cb.OnGetApplyMsg_ = FuncToCenterOnGetApplyMsgCB;
        cb.OnGetApplyMsgList_ = FuncToCenterOnGetApplyMsgListCB;
        cb.OnGetAuthCode_ = FuncToCenterOnGetAuthCodeCB;
        cb.OnGetConfCode_ = FuncToCenterOnGetConfCodeCB;
        cb.OnGetConfCodeList_ = FuncToCenterOnGetConfCodeListCB;
        cb.OnGetConfRoom_ = FuncToCenterOnGetConfRoomCB;
        cb.OnGetConfRoomList_ = FuncToCenterOnGetConfRoomListCB;
        cb.OnGetOrgInfo_ = FuncToCenterOnGetOrgInfoCB;
        cb.OnGetOrgInfoList_ = FuncToCenterOnGetOrgInfoListCB;
        cb.OnGetOrgUser_ = FuncToCenterOnGetOrgUserCB;
        cb.OnGetOrgUserList_ = FuncToCenterOnGetOrgUserListCB;
        cb.OnGetPushMsg_ = FuncToCenterOnGetPushMsgCB;
        cb.OnGetPushMsgList_ = FuncToCenterOnGetPushMsgListCB;
        cb.OnGetRealConf_ = FuncToCenterOnGetRealConfCB;
        cb.OnGetUserBind_ = FuncToCenterOnGetUserBindCB;
        cb.OnGetUserBindList_ = FuncToCenterOnGetUserBindListCB;
        cb.OnGetUserInfo_ = FuncToCenterOnGetUserInfoCB;
        cb.OnGetUserInfoList_ = FuncToCenterOnGetUserInfoListCB;
        cb.OnLogin_ = FuncToCenterOnLoginCB;
        cb.OnLogout_ = FuncToCenterOnLogoutCB;
        cb.OnNoticePushMsg_ = FuncToCenterOnNoticePushMsgCB;
        cb.OnPrepareLoginConf_ = FuncToCenterOnPrepareLoginConfCB;
        cb.OnRegUserInfo_ = FuncToCenterOnRegUserInfoCB;
        cb.OnRemoveApplyMsg_ = FuncToCenterOnRemoveApplyMsgCB;
        cb.OnRemoveConfCode_ = FuncToCenterOnRemoveConfCodeCB;
        cb.OnRemoveConfRoom_ = FuncToCenterOnRemoveConfRoomCB;
        cb.OnRemoveOrgUser_ = FuncToCenterOnRemoveOrgUserCB;
        cb.OnRemovePushMsg_ = FuncToCenterOnRemovePushMsgCB;
        cb.OnRemoveUserBind_ = FuncToCenterOnRemoveUserBindCB;
        cb.OnRemoveUserInfo_ = FuncToCenterOnRemoveUserInfoCB;
        cb.OnUpdateApplyMsg_ = FuncToCenterOnUpdateApplyMsgCB;
        cb.OnUpdateConfCode_ = FuncToCenterOnUpdateConfCodeCB;
        cb.OnUpdateConfRoom_ = FuncToCenterOnUpdateConfRoomCB;
        cb.OnUpdateOrgInfo_ = FuncToCenterOnUpdateOrgInfoCB;
        cb.OnUpdateOrgUser_ = FuncToCenterOnUpdateOrgUserCB;
        cb.OnUpdatePushMsg_ = FuncToCenterOnUpdatePushMsgCB;
        cb.OnUpdateUserBind_ = FuncToCenterOnUpdateUserBindCB;
        cb.OnUpdateUserInfo_ = FuncToCenterOnUpdateUserInfoCB;
        
        lm_conf_centerserver_set_callback(c_object_, &cb);
    }
    return self;
}

- (void)addDelegate:(id<ConfCenterServerWrapperDelegate>)to_center_delegate {
    [delegate_array_ addObject:to_center_delegate];
}

- (void)removeDelegate:(id<ConfCenterServerWrapperDelegate>)to_center_delegate {
    [delegate_array_ removeObject:to_center_delegate];
}

- (int)setCenterAddress:(NSString *)address {
    return lm_conf_centerserver_SetCenterAddress(c_object_, [address UTF8String]);
}

- (NSString *)getCenterAddress {
    char s[256] = {0};
    lm_conf_centerserver_GetCenterAddress(c_object_, s, 256);
    return [NSString stringWithUTF8String:s];
}

- (int)getAuthCode:(CSParamWrapper *)param {
    return lm_conf_centerserver_GetAuthCode(c_object_, [param cobject]);
}

- (int)regUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info {
    return lm_conf_centerserver_RegUserInfo(c_object_, [param cobject], [info cobject]);
}

- (int)login:(CSParamWrapper *)param id_org:(uint32_t)id_org account:(NSString *)account psw:(NSString *)psw {
    return lm_conf_centerserver_Login(c_object_, [param cobject], id_org, [account UTF8String], [psw UTF8String]);
}

- (int)logout:(CSParamWrapper *)param {
    return lm_conf_centerserver_Logout(c_object_, [param cobject]);
}

- (int)prepareLoginConf:(CSParamWrapper *)param id_conf:(uint32_t)id_conf {
    return lm_conf_centerserver_PrepareLoginConf(c_object_, [param cobject], id_conf);
}

- (int)getRealConf:(CSParamWrapper *)param int_list:(NSArray<NSNumber*> *)listConfID {
    int* id_confs = malloc(sizeof(int) * [listConfID count]);
    int count = 0;
    for (NSNumber* element in listConfID) {
        id_confs[count++] = [element intValue];
    }
    int ret = lm_conf_centerserver_GetRealConf(c_object_, [param cobject], id_confs, count);
    free(id_confs);
    return ret;
}


- (int)getUserInfo:(CSParamWrapper *)param account:(NSString *)account {
    return lm_conf_centerserver_GetUserInfo(c_object_, [param cobject], [account UTF8String]);
}

- (int)updateUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info {
    return lm_conf_centerserver_UpdateUserInfo(c_object_, [param cobject], [info cobject]);
}

- (int)removeUserInfo:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info {
    return lm_conf_centerserver_RemoveUserInfo(c_object_, [param cobject], [info cobject]);
}

- (int)getUserInfoList:(CSParamWrapper *)param info:(CSUserInfoWrapper *)info {
    return lm_conf_centerserver_GetUserInfoList(c_object_, [param cobject], [info cobject]);
}

- (int)getOrgInfo:(CSParamWrapper *)param id_org:(uint32_t)id_org {
    return lm_conf_centerserver_GetOrgInfo(c_object_, [param cobject], id_org);
}

- (int)updateOrgInfo:(CSParamWrapper *)param info:(CSOrgInfoWrapper *)info {
    return lm_conf_centerserver_UpdateOrgInfo(c_object_, [param cobject], [info cobject]);
}

- (int)getOrgInfoList:(CSParamWrapper *)param info:(CSOrgInfoWrapper *)info {
    return lm_conf_centerserver_UpdateOrgInfo(c_object_, [param cobject], [info cobject]);
}

- (int)getUserBind:(CSParamWrapper *)param name:(NSString *)name {
    return lm_conf_centerserver_GetUserBind(c_object_, [param cobject], [name UTF8String]);
}

- (int)addUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info {
    return lm_conf_centerserver_AddUserBind(c_object_, [param cobject], [info cobject]);
}

- (int)updateUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info {
    return lm_conf_centerserver_UpdateUserBind(c_object_, [param cobject], [info cobject]);
}

- (int)removeUserBind:(CSParamWrapper *)param info:(CSUserBindWrapper *)info {
    return lm_conf_centerserver_RemoveUserBind(c_object_, [param cobject], [info cobject]);
}

- (int)getUserBindList:(CSParamWrapper *)param info:(CSUserBindWrapper *)info {
    return lm_conf_centerserver_GetUserBindList(c_object_, [param cobject], [info cobject]);
}

- (int)getOrgUser:(CSParamWrapper *)param id_org:(uint32_t)id_org account:(NSString *)account {
    return lm_conf_centerserver_GetOrgUser(c_object_, [param cobject], id_org, [account UTF8String]);
}

- (int)addOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info {
    return lm_conf_centerserver_AddOrgUser(c_object_, [param cobject], [info cobject]);
}

- (int)updateOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info {
    return lm_conf_centerserver_UpdateOrgUser(c_object_, [param cobject], [info cobject]);
}

- (int)removeOrgUser:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info {
    return lm_conf_centerserver_RemoveOrgUser(c_object_, [param cobject], [info cobject]);
}

- (int)getOrgUserList:(CSParamWrapper *)param info:(CSOrgUserWrapper *)info {
    return lm_conf_centerserver_GetOrgUserList(c_object_, [param cobject], [info cobject]);
}

- (int)getConfRoom:(CSParamWrapper *)param id_conf:(uint32_t)id_conf {
    return lm_conf_centerserver_GetConfRoom(c_object_, [param cobject], id_conf);
}

- (int)addConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info {
    return lm_conf_centerserver_AddConfRoom(c_object_, [param cobject], [info cobject]);
}

- (int)updateConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info {
    return lm_conf_centerserver_UpdateConfRoom(c_object_, [param cobject], [info cobject]);
}

- (int)removeConfRoom:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info {
    return lm_conf_centerserver_RemoveConfRoom(c_object_, [param cobject], [info cobject]);
}

- (int)getConfRoomList:(CSParamWrapper *)param info:(CSConfRoomWrapper *)info {
    return lm_conf_centerserver_GetConfRoomList(c_object_, [param cobject], [info cobject]);
}

- (int)getConfCode:(CSParamWrapper *)param code:(NSString *)codeid {
    return lm_conf_centerserver_GetConfCode(c_object_, [param cobject], [codeid UTF8String]);
}

- (int)addConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info {
    return lm_conf_centerserver_AddConfCode(c_object_, [param cobject], [info cobject]);
}

- (int)updateConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info {
    return lm_conf_centerserver_UpdateConfCode(c_object_, [param cobject], [info cobject]);
}

- (int)removeConfCode:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info {
    return lm_conf_centerserver_RemoveConfCode(c_object_, [param cobject], [info cobject]);
}

- (int)getConfCodeList:(CSParamWrapper *)param info:(CSConfCodeWrapper *)info {
    return lm_conf_centerserver_GetConfCodeList(c_object_, [param cobject], [info cobject]);
}

- (int)getApplyMsg:(CSParamWrapper *)param id_req:(uint32_t)id_req {
    return lm_conf_centerserver_GetApplyMsg(c_object_, [param cobject], id_req);
}

- (int)addApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info {
    return lm_conf_centerserver_AddApplyMsg(c_object_, [param cobject], [info cobject]);
}

- (int)updateApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info {
    return lm_conf_centerserver_UpdateApplyMsg(c_object_, [param cobject], [info cobject]);
}

- (int)removeApplyMsg:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info {
    return lm_conf_centerserver_RemoveApplyMsg(c_object_, [param cobject], [info cobject]);
}

- (int)getApplyMsgList:(CSParamWrapper *)param info:(CSApplyMsgWrapper *)info {
    return lm_conf_centerserver_GetApplyMsgList(c_object_, [param cobject], [info cobject]);
}

- (int)getPushMsg:(CSParamWrapper *)param msg_id:(uint32_t)msg_id {
    return lm_conf_centerserver_GetPushMsg(c_object_, [param cobject], msg_id);
}

- (int)addPushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info {
    return lm_conf_centerserver_AddPushMsg(c_object_, [param cobject], [info cobject]);
}

- (int)updatePushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info {
    return lm_conf_centerserver_UpdatePushMsg(c_object_, [param cobject], [info cobject]);
}

- (int)removePushMsg:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info {
    return lm_conf_centerserver_RemovePushMsg(c_object_, [param cobject], [info cobject]);
}

- (int)getPushMsgList:(CSParamWrapper *)param info:(CSPushMsgWrapper *)info {
    return lm_conf_centerserver_GetPushMsgList(c_object_, [param cobject], [info cobject]);
}

- (int)getConfCode:(CSParamWrapper *)param  idConf:(int)id_conf {
    return lm_conf_centerserver_GetConfCode_with_id(c_object_, [param cobject], id_conf);
}

@end
