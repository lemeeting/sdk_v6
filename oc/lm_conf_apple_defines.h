//
//  lm_conf_apple_defines.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/20.
//  Copyright © 2016 lh. All rights reserved.
//

#ifndef lm_conf_apple_defines_h
#define lm_conf_apple_defines_h

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
typedef UIView PlatformView;
#else
#import <AppKit/AppKit.h>
typedef NSView PlatformView;
#endif


#endif /* lm_conf_apple_defines_h */
