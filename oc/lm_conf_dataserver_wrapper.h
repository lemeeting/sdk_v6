//
//  lm_conf_dataserver_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@protocol ConfDataServerWrapperDelegate <NSObject>

@optional
- (void)onPingResult:(uint32_t)task_id
             context:(uint64_t)context
               error:(int)error
              result:(const struct LMPingResult*)result;
@end

@interface ConfDataServerWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfDataServerWrapperDelegate>)dataserver_delegate;

- (void)removeDelegate:(id<ConfDataServerWrapperDelegate>)dataserver_delegate;

- (int)getDataServerCount;

- (int)enumDataServers:(struct LMDataServerInfo*)data_Servers count:(int*)count;

- (int)getCurrentConnectDataServer:(enum LMDataServerModule)module
                        dataServer:(struct LMDataServerInfo*)data_server;

- (int)pingDataServer:(NSString *)address
                 port:(uint16_t)port
        callback_time:(int64_t)callback_time
               useTcp:(BOOL)is_use_udp
              context:(uint64_t)context
               taskId:(uint32_t*)task_id;

- (int)stopPingDataServer:(uint32_t)task_id;

- (int)switchDataServer:(enum LMDataServerModule)module address:(NSString *)address;

@end
