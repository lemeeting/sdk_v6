//
//  lm_conf_cs_realtime_confinfo_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSRealTimeConfWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

@end
