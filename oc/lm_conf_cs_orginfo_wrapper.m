//
//  lm_conf_cs_orginfo_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_orginfo_wrapper.h"

@implementation CSOrgInfoWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_org_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_org_instance(c_object_);
}

- (int)getVersionid {
    return lm_get_cs_org_version_id(c_object_);
    
}

- (int)getDeleteflag {
    return lm_get_cs_org_delete_flag(c_object_);
    
}

- (int)getOrgid {
    return lm_get_cs_org_world_id(c_object_);
    
}

- (int)getOrgtype {
    return lm_get_cs_org_type(c_object_);
    
}

- (int)getOrgstatus {
    return lm_get_cs_org_status(c_object_);
    
}

- (int)getGroupid {
    return lm_get_cs_org_group_id(c_object_);
    
}

- (int)getMaxusercount {
    return lm_get_cs_org_max_user_nums(c_object_);
    
}

- (NSString *)getOrgname {
    return [NSString stringWithUTF8String:lm_get_cs_org_name(c_object_)];
    
}

- (NSString *)getShowname {
    return [NSString stringWithUTF8String:lm_get_cs_org_show_name(c_object_)];
    
}

- (NSString *)getAddress {
    return [NSString stringWithUTF8String:lm_get_cs_org_address(c_object_)];
    
}

- (NSString *)getContacter {
    return [NSString stringWithUTF8String:lm_get_cs_org_contacter(c_object_)];
    
}

- (NSString *)getPhone {
    return [NSString stringWithUTF8String:lm_get_cs_org_phone(c_object_)];
    
}

- (NSString *)getEmail {
    return [NSString stringWithUTF8String:lm_get_cs_org_email(c_object_)];
    
}

- (NSString *)getWebsite {
    return [NSString stringWithUTF8String:lm_get_cs_org_website(c_object_)];
    
}

- (CFAbsoluteTime)getBirthday {
    return lm_get_cs_org_birthday(c_object_);
    
}

- (CFAbsoluteTime)getCreatetime {
    return lm_get_cs_org_create_time(c_object_);
    
}

- (CFAbsoluteTime)getModifytime {
    return lm_get_cs_org_modify_time(c_object_);
    
}

- (NSString *)getCreator {
    return [NSString stringWithUTF8String:lm_get_cs_org_creator(c_object_)];
    
}

- (NSString *)getMender {
    return [NSString stringWithUTF8String:lm_get_cs_org_mender(c_object_)];
    
}

- (int)getOrgflag {
    return lm_get_cs_org_flag(c_object_);
    
}

- (NSString *)getSettingjson {
    return [NSString stringWithUTF8String:lm_get_cs_org_setting_json(c_object_)];
    
}

- (NSString *)getExtendjson {
    return [NSString stringWithUTF8String:lm_get_cs_org_extend_json(c_object_)];
   
}

- (void)setOrgid:(int)value {
    lm_set_cs_org_world_id(c_object_, value);
    
}

- (void)setOrgtype:(int)value {
    lm_set_cs_org_type(c_object_, value);
    
}

- (void)setOrgstatus:(int)value {
    lm_set_cs_org_status(c_object_, value);
    
}

- (void)setGroupid:(int)value {
    lm_set_cs_org_group_id(c_object_, value);
    
}

- (void)setMaxusercount:(int)value {
    lm_set_cs_org_max_user_nums(c_object_, value);
    
}

- (void)setOrgname:(NSString *)value {
    lm_set_cs_org_name(c_object_, [value UTF8String]);
    
}

- (void)setShowname:(NSString *)value {
    lm_set_cs_org_show_name(c_object_, [value UTF8String]);
    
}

- (void)setAddress:(NSString *)value {
    lm_set_cs_org_address(c_object_, [value UTF8String]);
    
}

- (void)setContacter:(NSString *)value {
    lm_set_cs_org_contacter(c_object_, [value UTF8String]);
    
}

- (void)setPhone:(NSString *)value {
    lm_set_cs_org_phone(c_object_, [value UTF8String]);
    
}

- (void)setEmail:(NSString *)value {
    lm_set_cs_org_email(c_object_, [value UTF8String]);
    
}

- (void)setWebsite:(NSString *)value {
    lm_set_cs_org_website(c_object_, [value UTF8String]);
    
}

- (void)setBirthday:(CFAbsoluteTime)value {
    lm_set_cs_org_birthday(c_object_, value);
    
}

- (void)setOrgflag:(int)value {
    lm_set_cs_org_flag(c_object_, value);
    
}

- (void)setExtendjson:(NSString *)value {
    lm_set_cs_org_extend_json(c_object_, [value UTF8String]);
    
}


@end
