//
//  lm_conf_engine_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_engine_wrapper.h"
#import "lm_conf_base_wrapper.h"
#import "lm_conf_video_wrapper.h"
#import "lm_conf_voice_wrapper.h"
#import "lm_conf_record_wrapper.h"
#import "lm_conf_business_wrapper.h"
#import "lm_conf_record_wrapper.h"
#import "lm_conf_playback_wrapper.h"
#import "lm_conf_mediaplayer_wrapper.h"
#import "lm_conf_ipcamera_wrapper.h"
#import "lm_conf_dataserver_wrapper.h"
#import "lm_conf_centerserver_wrapper.h"
#import "lm_conf_sharedesktop_wrapper.h"

@implementation ConfEngineWrapper

@synthesize base_wrapper = base_wrapper_;
@synthesize business_wrapper = business_wrapper_;
@synthesize video_wrapper = video_wrapper_;
@synthesize voice_wrapper_ = voice_wrapper_;
@synthesize sharedeskop_wrapper = sharedeskop_wrapper_;
@synthesize record_wrapper = record_wrapper_;
@synthesize playback_wrapper = playback_wrapper_;
@synthesize mediaplayer_wrapper = mediaplayer_wrapper_;
@synthesize dataserver_wrapper =dataserver_wrapper_;
@synthesize centerserver_wrapper =centerserver_wrapper_;
@synthesize ipcamera_wrapper = ipcamera_wrapper_;

+ (ConfEngineWrapper*)conf_engine {
    static ConfEngineWrapper *obj;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        obj = [[self alloc] init];
    });
    return obj;
}

- (id)init {
    if ((self = [super init])) {
        lm_create_conf_engine_instance(&native_conf_engine_);
        if (native_conf_engine_) {
            lm_get_conf_base_instance_with_engine(native_conf_engine_, &native_conf_base_);
            lm_get_conf_business_instance_with_engine(native_conf_engine_, &native_conf_business_);
            lm_get_conf_center_server_instance_with_engine(native_conf_engine_, &native_conf_centerserver_);
            lm_get_conf_data_server_instance_with_engine(native_conf_engine_, &native_conf_dataserver_);
            lm_get_conf_record_instance_with_engine(native_conf_engine_, &native_conf_record_);
            lm_get_conf_playback_instance_with_engine(native_conf_engine_, &native_conf_playback_);
            lm_get_conf_mediaplayer_instance_with_engine(native_conf_engine_, &native_conf_mediaplayer_);
            lm_get_conf_video_instance_with_engine(native_conf_engine_, &native_conf_video_);
            lm_get_conf_voice_instance_with_engine(native_conf_engine_, &native_conf_voice_);
            lm_get_conf_sharedesktop_instance_with_engine(native_conf_engine_, &native_conf_sharedesktop_);
            lm_get_conf_ipcamera_instance_with_engine(native_conf_engine_, &native_conf_ipcamera_);
            
        }
        
        if (native_conf_base_)
            base_wrapper_ = [[ConfBaseWrapper alloc] initWithCObject:native_conf_base_];
        if (native_conf_business_)
            business_wrapper_ = [[ConfBusinessWrapper alloc] initWithCObject:native_conf_business_];
        if (native_conf_centerserver_)
            centerserver_wrapper_ = [[ConfCenterServerWrapper alloc] initWithCObject:native_conf_centerserver_];
        if (native_conf_dataserver_)
            dataserver_wrapper_ = [[ConfDataServerWrapper alloc] initWithCObject:native_conf_dataserver_];
        if (native_conf_ipcamera_)
            ipcamera_wrapper_ = [[ConfIPCameraWrapper alloc] initWithCObject:native_conf_ipcamera_];
        if (native_conf_mediaplayer_)
            mediaplayer_wrapper_ = [[ConfMediaPlayerWrapper alloc] initWithCObject:native_conf_mediaplayer_];
        if (native_conf_playback_)
            playback_wrapper_ = [[ConfPlaybackWrapper alloc] initWithCObject:native_conf_playback_];
        if (native_conf_record_)
            record_wrapper_ = [[ConfRecordWrapper alloc] initWithCObject:native_conf_record_];
        if (native_conf_sharedesktop_)
            sharedeskop_wrapper_ = [[ConfShareDesktopWrapper alloc] initWithCObject:native_conf_sharedesktop_];
        if (native_conf_video_)
            video_wrapper_ = [[ConfVideoWrapper alloc] initWithCObject:native_conf_video_];
        if (native_conf_voice_)
            voice_wrapper_ = [[ConfVoiceWrapper alloc] initWithCObject:native_conf_voice_];
    }
    return self;
}

- (void)dealloc {
    if (native_conf_engine_)
        lm_destroy_conf_engine_instance(native_conf_engine_);
}


@end
