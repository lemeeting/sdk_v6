//
//  lm_conf_cs_userinfo_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_userinfo_wrapper.h"

@implementation CSUserInfoWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_user_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_user_instance(c_object_);
}

- (int)getVersionid {
    return lm_get_cs_user_version_id(c_object_);    
}

- (int)getDeleteflag {
    return lm_get_cs_user_delete_flag(c_object_);
}

- (int)getOrgid {
    return lm_get_cs_user_world_id(c_object_);
}

- (NSString *)getUseraccount {
    return [NSString stringWithUTF8String:lm_get_cs_user_account(c_object_)];
}

- (int)getUsertype {
    return lm_get_cs_user_type(c_object_);
}

- (int)getUserstatus {
    return lm_get_cs_user_status(c_object_);
}

- (NSString *)getUsername {
    return [NSString stringWithUTF8String:lm_get_cs_user_name(c_object_)];
}

- (NSString *)getPassword {
    return [NSString stringWithUTF8String:lm_get_cs_user_password(c_object_)];
}

- (NSString *)getPhone {
    return [NSString stringWithUTF8String:lm_get_cs_user_phone(c_object_)];
}

- (NSString *)getEmail {
    return [NSString stringWithUTF8String:lm_get_cs_user_email(c_object_)];
}

- (CFAbsoluteTime)getBirthday {
    return lm_get_cs_user_birthday(c_object_);
}

- (CFAbsoluteTime)getCreatetime {
    return lm_get_cs_user_create_time(c_object_);
}

- (NSString *)getSettingjson {
    return [NSString stringWithUTF8String:lm_get_cs_user_setting_json(c_object_)];
}

- (NSString *)getExtendjson {
    return [NSString stringWithUTF8String:lm_get_cs_user_extend_json(c_object_)];
}

- (void)setOrgid:(int)value {
    lm_set_cs_org_user_org_id(c_object_, value);
}

- (void)setUseraccount:(NSString *)value {
    lm_set_cs_org_user_account(c_object_, [value UTF8String]);
}

- (void)setUsertype:(int)value {
    lm_set_cs_org_user_type(c_object_, value);
}

- (void)setUserstatus:(int)value {
    lm_set_cs_org_user_status(c_object_, value);
}

- (void)setUsername:(NSString *)value {
    lm_set_cs_org_user_name(c_object_, [value UTF8String]);
}

- (void)setPassword:(NSString *)value {
    lm_set_cs_org_user_password(c_object_, [value UTF8String]);
}

- (void)setPhone:(NSString *)value {
    lm_set_cs_org_user_phone(c_object_, [value UTF8String]);
}

- (void)setEmail:(NSString *)value {
    lm_set_cs_org_user_email(c_object_, [value UTF8String]);
}

- (void)setBirthday:(CFAbsoluteTime)value {
    lm_set_cs_org_user_birthday(c_object_, value);
}

- (void)setCreatetime:(CFAbsoluteTime)value {
    lm_set_cs_org_user_create_time(c_object_, value);
}

- (void)setSettingjson:(NSString *)value {
    lm_set_cs_org_user_setting_json(c_object_, [value UTF8String]);
}

- (void)setExtendjson:(NSString *)value {
    lm_set_cs_org_user_extend_json(c_object_, [value UTF8String]);
}

@end
