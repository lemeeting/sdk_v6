//
//  lm_conf_cs_orginfo_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSOrgInfoWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

- (int)getVersionid;
- (int)getDeleteflag;
- (int)getOrgid;
- (int)getOrgtype;
- (int)getOrgstatus;
- (int)getGroupid;
- (int)getMaxusercount;

- (NSString *)getOrgname;
- (NSString *)getShowname;
- (NSString *)getAddress;
- (NSString *)getContacter;
- (NSString *)getPhone;
- (NSString *)getEmail;
- (NSString *)getWebsite;

- (CFAbsoluteTime)getBirthday;
- (CFAbsoluteTime)getCreatetime;
- (CFAbsoluteTime)getModifytime;

- (NSString *)getCreator;
- (NSString *)getMender;

- (int)getOrgflag;
- (NSString *)getSettingjson;
- (NSString *)getExtendjson;


- (void)setOrgid:(int)value;
- (void)setOrgtype:(int)value;
- (void)setOrgstatus:(int)value;
- (void)setGroupid:(int)value;
- (void)setMaxusercount:(int)value;

- (void)setOrgname:(NSString *)value;
- (void)setShowname:(NSString *)value;
- (void)setAddress:(NSString *)value;
- (void)setContacter:(NSString *)value;
- (void)setPhone:(NSString *)value;
- (void)setEmail:(NSString *)value;
- (void)setWebsite:(NSString *)value;

- (void)setBirthday:(CFAbsoluteTime)value;
- (void)setOrgflag:(int)value;
- (void)setExtendjson:(NSString *)value;

@end
