//
//  lm_conf_record_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@protocol ConfRecordWrapperDelegate <NSObject>
@optional
- (void)onStartLocalRecordReady;
@end

@interface ConfRecordWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfRecordWrapperDelegate>)record_delegate;

- (void)removeDelegate:(id<ConfRecordWrapperDelegate>)record_delegate;


- (int)startLocalRecord:(NSString *)file_name
         recordaccounts:(NSArray<NSString *> *)recordaccounts
                  flags:(int)flags;

- (int)stopLocalRecord;

- (int)startScreenRecord:(NSString *)file_name
                  format:(enum LMScreenRecordFormats)format
                    zoom:(enum LMScreenRecordZoom)zoom
              frame_rate:(float)frame_rate
                      QP:(enum LMScreenRecordQP)qp
                   flags:(int)flags;

- (int)stopScreenRecord;

- (int)stopRecord;

- (enum LMConfRecordType)currentRecordType;

@end
