//
//  lm_conf_as_confattribute_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_as_confattribute_wrapper.h"
#import "lm_conf_base_wrapper.h"
#import "lm_conf_engine_wrapper.h"

@implementation ASConfAttributeWrapper

+ (ASConfAttributeWrapper*) from_conf_base {
    native_object_t conf_base_cobject = [[[ConfEngineWrapper conf_engine] base_wrapper] cobject];
    native_object_t cobject = NULL;
    if (lm_conf_base_get_conf_attribute(conf_base_cobject, &cobject) != 0) {
        return nil;
    }
    return [[ASConfAttributeWrapper alloc] initWithCObject:cobject];
}

- (id)init {
    if ((self = [super init])) {
        owner_ = YES;
        lm_create_as_conference_attribute_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = NO;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_as_conference_attribute_instance(c_object_);
}

- (int)orgId {
    return lm_get_as_conference_attribute_enterprise_id(c_object_);
}

- (int)conferenceId {
    return lm_get_as_conference_attribute_conference_id(c_object_);
}

- (uint32_t)tag {
    return lm_get_as_conference_attribute_tag(c_object_);
}

- (int)maxAttendees {
    return lm_get_as_conference_attribute_max_attendees(c_object_);
}

- (NSString *)name {
    return [NSString stringWithUTF8String:lm_get_as_conference_attribute_name(c_object_)];
}

- (BOOL)isFree {
    return !!lm_as_conference_attribute_is_free(c_object_);
}

- (BOOL)isLock {
    return !!lm_as_conference_attribute_is_lock(c_object_);
}

- (BOOL)isDisableText {
    return !!lm_as_conference_attribute_is_disable_text(c_object_);
}

- (BOOL)isDisableRecord {
    return !!lm_as_conference_attribute_is_disable_record(c_object_);
}

- (BOOL)isDisableBrowVideo {
    return !!lm_as_conference_attribute_is_disable_brower_video(c_object_);
}


@end
