//
//  lm_conf_as_attendee_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_as_attendee_wrapper.h"
#import "lm_conf_base_wrapper.h"
#import "lm_conf_engine_wrapper.h"

@implementation ASConfAttendeeWrapper

+ (ASConfAttendeeWrapper*) from_conf_base:(NSString*)account {
    native_object_t conf_base_cobject = [[[ConfEngineWrapper conf_engine] base_wrapper] cobject];
    native_object_t cobject = NULL;
    if (lm_conf_base_get_attendee_info(conf_base_cobject, [account UTF8String], &cobject) != 0) {
        return nil;
    }
    
    return [[ASConfAttendeeWrapper alloc] initWithCObject:cobject];
 }

+ (ASConfAttendeeWrapper*) self_from_conf_base {
    native_object_t conf_base_cobject = [[[ConfEngineWrapper conf_engine] base_wrapper] cobject];
    native_object_t cobject = NULL;
    if (lm_conf_base_get_self_attendee_info(conf_base_cobject, &cobject) != 0) {
        return nil;
    }
    
    return [[ASConfAttendeeWrapper alloc] initWithCObject:cobject];
}

- (id)init {
    if ((self = [super init])) {
        owner_ = YES;
        lm_create_as_attendee_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = NO;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_as_attendee_instance(c_object_);
}

- (int)type {
    return lm_get_as_attendee_type(c_object_);
}

- (enum LMConferenceClientType)loginType {
    return (enum LMConferenceClientType)(lm_get_as_attendee_login_type(c_object_));
}

- (uint32_t)opsStatus {
    return lm_get_as_attendee_op_status(c_object_);
}

- (enum LMConferenceAdminModes)adminPrivilege {
    return (enum LMConferenceAdminModes)(lm_get_as_attendee_admin_privilege(c_object_));
}

- (int)numOfVoiceDevices {
    return lm_get_as_attendee_audio_device_nums(c_object_);
}

- (int)numOfVideoDevices {
    return lm_get_as_attendee_video_device_nums(c_object_);
}

- (void)enumVideoDevices:(DeviceIdentity*)id_devices nums:(int*)nums {
    lm_get_as_attendee_enum_video_devices(c_object_, (int*)(id_devices), nums);
}

- (BOOL)isEnableVideoDevice:(DeviceIdentity)id_device {
    return !!lm_get_as_attendee_video_device_state(c_object_, id_device);
}

- (enum LMCameraType)getVideoDeviceType:(DeviceIdentity)id_device {
    return (enum LMCameraType)(lm_get_as_attendee_video_device_type(c_object_, id_device));
}

- (enum LMCameraSubType)getVideoDeviceSubType:(DeviceIdentity)id_device {
    return (enum LMCameraSubType)(lm_get_as_attendee_video_device_sub_type(c_object_, id_device));
}

- (NSString *)getVideoDeviceShowName:(DeviceIdentity)id_device {
    return [NSString stringWithUTF8String:lm_get_as_attendee_video_device_show_name(c_object_, id_device)];
}

- (DeviceIdentity)mainVideoDeviceId {
    return lm_get_as_attendee_main_video_device_id(c_object_);
}

- (BOOL)isForbiddenVideoDevice {
    return !!lm_as_attendee_is_forbidden_video_device(c_object_);
}

- (NSString *)account {
    return [NSString stringWithUTF8String:lm_get_as_attendee_account(c_object_)];
}

- (NSString *)name {
    return [NSString stringWithUTF8String:lm_get_as_attendee_name(c_object_)];
}

- (int)enterpriseId {
    return lm_get_as_attendee_enterprise_id(c_object_);
}

- (int)accessServerId {
    return lm_get_as_attendee_server_id(c_object_);
}

- (int)conferenceId {
    return lm_get_as_attendee_conference_id(c_object_);
}

- (NSString *)macAddress {
    return [NSString stringWithUTF8String:lm_get_as_attendee_mac_address(c_object_)];
}

- (CFAbsoluteTime)signinTime {
    return lm_get_as_attendee_signin_time(c_object_);
}

- (BOOL)isValidVideoDevice:(DeviceIdentity)id_device {
    return !!lm_as_attendee_is_valid_video_device(c_object_, id_device);
}

- (BOOL)isAdmin {
  return [self adminPrivilege] != kConferenceNonAdmin;
}

- (BOOL)applySpeakOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsApplySpeak);
}

- (BOOL)speakOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsSpeak);
}

- (BOOL)applyDataOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsApplyOperData);
}

- (BOOL)dataOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsOperData);
}

- (BOOL)applySyncOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsApplySync);
}

- (BOOL)syncOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsSync);
}

- (BOOL)applyAdminOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsApplyAdmin);
}

- (BOOL)sharedDesktopOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsSharedDesktop);
}

- (BOOL)playbackOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsPlayback);
}

- (BOOL)mediaPlayOp {
    return LM_CHECK_OPTION([self opsStatus], kOpsMediaPlay);
}

- (BOOL)askRemoteControlDesktopStatus {
    return LM_CHECK_OPTION([self opsStatus], kOpsAskRemoteControlDesktop);
}

- (BOOL)inRemoteControlDesktopStatus {
    return LM_CHECK_OPTION([self opsStatus], kOpsInRemoteControlDesktop);
}

@end
