//
//  lm_conf_as_syncinfo_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface ASConfSyncInfoWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

+ (ASConfSyncInfoWrapper*) from_conf_base;

- (id)initWithCObject:(native_object_t)c_object;

- (BOOL)isSync;
- (NSString*)syncer;
- (NSString*)syncData;

@end
