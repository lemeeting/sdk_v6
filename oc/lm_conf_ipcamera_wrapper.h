//
//  lm_conf_ipcamera_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"
#include "conf_wrapper_interface.h"

@protocol ConfIPCameraWrapperDelegate <NSObject>
@optional

- (void)onSearchWithLAN:(int)error results:(const struct LMIPCameraAddress*)results count:(int)count;

- (void)onAddIPCamera:(int)error identity:(int)identity info:(const struct LMIPCameraInfo*)info;

@end

@interface ConfIPCameraWrapper : NSObject {
@private
    native_object_t c_object_;
    NSMutableArray* delegate_array_;
}

@property(strong, nonatomic) NSMutableArray* delegate_array;

- (id)initWithCObject:(native_object_t)c_object;

- (void)addDelegate:(id<ConfIPCameraWrapperDelegate>)ipcamera_delegate;

- (void)removeDelegate:(id<ConfIPCameraWrapperDelegate>)ipcamera_delegate;

- (int)searchWithLAN:(NSString* )multicast_address;

- (int)addIPCamera:(const struct LMIPCameraAddress* )address
              auth:(const struct LMIPCameraAuthInfo*)auth
          identity:(int*)identity;

- (int)removeIPCamera:(int)identity;


@end
