//
//  lm_conf_cs_confroom_wrapper.m
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import "lm_conf_cs_confroom_wrapper.h"

@implementation CSConfRoomWrapper
@synthesize cobject = c_object_;

- (id)init {
    if ((self = [super init])) {
        owner_ = true;
        lm_create_cs_room_instance(&c_object_);
    }
    return self;
}

- (id)initWithCObject:(native_object_t)c_object {
    if ((self = [super init])) {
        owner_ = false;
        c_object_ = c_object;
    }
    return self;
}

- (void)dealloc {
    if(owner_)
        lm_destroy_cs_room_instance(c_object_);
}

- (int)getVersionid {
    return lm_get_cs_room_version_id(c_object_);
}

- (int)getDeleteflag {
    return lm_get_cs_room_delete_flag(c_object_);
}

- (int)getConfid {
    return lm_get_cs_room_id(c_object_);
}

- (int)getConftype {
    return lm_get_cs_room_type(c_object_);
}

- (int)getWorldid {
    return lm_get_cs_room_world_id(c_object_);
}

- (int)getGroupid {
    return lm_get_cs_room_group_id(c_object_);
}

- (int)getServerid {
    return lm_get_cs_room_server_id(c_object_);
}

- (NSString *)getConfname {
    return [NSString stringWithUTF8String:lm_get_cs_room_name(c_object_)];
}

- (NSString *)getConfpassword {
    return [NSString stringWithUTF8String:lm_get_cs_room_conf_password(c_object_)];
}

- (NSString *)getManagepassword {
    return [NSString stringWithUTF8String:lm_get_cs_room_manager_password(c_object_)];
}

- (CFAbsoluteTime)getStarttime {
    return lm_get_cs_room_start_time(c_object_);
}

- (CFAbsoluteTime)getEndtime {
    return lm_get_cs_room_end_time(c_object_);
}

- (int)getMaxusercount {
    return lm_get_cs_room_max_user_nums(c_object_);
}

- (int)getMaxspeakercount {
    return lm_get_cs_room_max_speaker_nums(c_object_);
}

- (NSString *)getCreator {
    return [NSString stringWithUTF8String:lm_get_cs_room_creator(c_object_)];
}

- (NSString *)getMender {
    return [NSString stringWithUTF8String:lm_get_cs_room_mender(c_object_)];
}

- (CFAbsoluteTime)getCreatetime {
    return lm_get_cs_room_create_time(c_object_);
}

- (CFAbsoluteTime)getModifytime {
    return lm_get_cs_room_modify_time(c_object_);
}

- (int)getConfflag {
    return lm_get_cs_room_flag(c_object_);
}

- (NSString *)getSettingjson {
    return [NSString stringWithUTF8String:lm_get_cs_room_setting_json(c_object_)];
}

- (NSString *)getExtendjson {
    return [NSString stringWithUTF8String:lm_get_cs_room_extend_json(c_object_)];
}

- (void)setConfid:(int)value {
    lm_set_cs_room_id(c_object_, value);
}

- (void)setConftype:(int)value {
    lm_set_cs_room_type(c_object_, value);
}

- (void)setWorldid:(int)value {
    lm_set_cs_room_world_id(c_object_, value);
}

- (void)setGroupid:(int)value {
    lm_set_cs_room_group_id(c_object_, value);
}

- (void)setServerid:(int)value {
    lm_set_cs_room_server_id(c_object_, value);
}

- (void)setConfname:(NSString*)value {
    lm_set_cs_room_name(c_object_, [value UTF8String]);
}

- (void)setConfpassword:(NSString*)value {
    lm_set_cs_room_conf_password(c_object_, [value UTF8String]);
}

- (void)setManagepassword:(NSString*)value {
    lm_set_cs_room_manager_password(c_object_, [value UTF8String]);
}

- (void)setStarttime:(CFAbsoluteTime)value {
    lm_set_cs_room_start_time(c_object_, value);
}

- (void)setEndtime:(CFAbsoluteTime)value {
    lm_set_cs_room_end_time(c_object_, value);
}

- (void)setMaxusercount:(int)value {
    lm_set_cs_room_max_user_nums(c_object_, value);
}

- (void)setMaxspeakercount:(int)value {
    lm_set_cs_room_max_speaker_nums(c_object_, value);
}

- (void)setConfflag:(int)value {
    lm_set_cs_room_flag(c_object_, value);
}

- (void)setExtendjson:(NSString*)value {
    lm_set_cs_room_extend_json(c_object_, [value UTF8String]);
}


@end
