//
//  lm_conf_cs_pushmsg_wrapper.h
//  LMConf_ios
//
//  Created by lh Authors on 16/6/19.
//  Copyright © 2016 lh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "conf_wrapper_interface.h"

@interface CSPushMsgWrapper : NSObject {
@private
    BOOL owner_;
    native_object_t c_object_;
}

@property native_object_t cobject;

- (id)initWithCObject:(native_object_t)c_object;

- (int)getVersionid;
- (int)getDeleteflag;
- (int)getMsgid;
- (int)getMsgtype;
- (int)getMsgstate;
- (int)getMsgresult;
- (int)getSendorgid;

- (NSString *)getSendaccount;

- (CFAbsoluteTime)getSendtime;
- (CFAbsoluteTime)getEndtime;

- (NSString *)getMsgtopic;
- (NSString *)getMsgdata;
- (NSString *)getMsgreceiver;
- (NSString *)getExtendjson;

- (void)setMsgid:(int)value;
- (void)setMsgtype:(int)value;
- (void)setMsgstate:(int)value;
- (void)setMsgresult:(int)value;
- (void)setSendorgid:(int)value;

- (void)setSendaccount:(NSString *)value;

- (void)setSendtime:(CFAbsoluteTime)value;
- (void)setEndtime:(CFAbsoluteTime)value;

- (void)setMsgtopic:(NSString *)value;
- (void)setMsgdata:(NSString *)value;
- (void)setMsgreceiver:(NSString *)value;
- (void)setExtendjson:(NSString *)value;

@end
