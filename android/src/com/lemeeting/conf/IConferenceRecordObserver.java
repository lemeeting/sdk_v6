package com.lemeeting.conf;

public interface IConferenceRecordObserver {
	void onStartLocalRecordReady();
}
