package com.lemeeting.conf;


import android.view.SurfaceView;
import android.view.ViewGroup;

import com.lemeeting.conf.defines.QzVideoConfig;

public interface IConferenceVideo {

	int addObserver(IConferenceVideoObserver observer);

	int removeObserver(IConferenceVideoObserver observer);
	
	SurfaceView createPreview();

	int addLocalPreview(int id_device, ViewGroup window, long context,
			int z_order, float left, float top, float right, float bottom);

	int removeLocalPreview(int id_device, int id_preview);

	int configureLocalPreview(int id_device, int id_preview,
			int z_order, float left, float top,	float right, float bottom);

	int changeLocalPreviewWindow(int id_device,
			int id_preview, ViewGroup window,
			int z_order, float left, float top, float right, float bottom);
	
	int addLocalPreview2(int id_device, SurfaceView window, long context,
			int z_order, float left, float top, float right, float bottom);

	int removeLocalPreview2(int id_device, int id_preview);

	int configureLocalPreview2(int id_device, int id_preview,
			int z_order, float left, float top,	float right, float bottom);
	
	int changeLocalPreviewWindow2(int id_device,
			int id_preview, SurfaceView window,
			int z_order, float left, float top, float right, float bottom);

	int enableMirrorLocalPreview(int id_device, int id_preview,
			boolean enable, boolean mirror_xaxis, boolean mirror_yaxis);
	
	
	//(ret >> 16 & 0xff)=enable, ((ret & 0xff00) >> 8) = mirror_xaxis, (ret & 0xff)=mirror_yaxis, -1=error
	int mirrorLocalPreviewState(int id_device, int id_preview); 

	int rotateLocalPreview(int id_device, 
			int id_preview, int rotation);

	int rotateLocalPreviewState(int id_device, 
			int id_preview);

	int addRemotePreview(String account, int id_device, ViewGroup window,
			int z_order, float left, float top, float right, float bottom);

	int removeRemotePreview(String account, int id_device, int id_preview);

	int configureRemotePreview(String account, int id_device, int id_preview,
			int z_order, float left, float top,	float right, float bottom);

	int changeRemotePreviewWindow(String account, int id_device,
			int id_preview, ViewGroup window,
			int z_order, float left, float top, float right, float bottom);
	
	int addRemotePreview2(String account, int id_device, SurfaceView window,
			int z_order, float left, float top, float right, float bottom);

	int removeRemotePreview2(String account, int id_device, int id_preview);

	int configureRemotePreview2(String account, int id_device, int id_preview,
			int z_order, float left, float top,	float right, float bottom);

	int changeRemotePreviewWindow2(String account, int id_device,
			int id_preview, SurfaceView window,
			int z_order, float left, float top, float right, float bottom);
	
	int enableMirrorRemotePreview(String account, int id_device, int id_preview,
			boolean enable, boolean mirror_xaxis, boolean mirror_yaxis);

	//(ret >> 16 & 0xff)=enable, ((ret & 0xff00) >> 8) = mirror_xaxis, (ret & 0xff)=mirror_yaxis, -1=error
	int mirrorRemotePreviewState(String account, int id_device, int id_preview); 

	int rotateRemotePreview(String account, int id_device, int id_preview, int rotation);

	int rotateRemotePreviewState(String account, int id_device, int id_preview);

	int enableRemotePreviewColorEnhancement(String account, int id_device, boolean enable);
		
	boolean remotePreviewColorEnhancementState(String account, int id_device);

	int enableRemotePreview(String account, int id_device, boolean enable);

	boolean getRemotePreviewState(String account, int id_device);

	int numOfDevice();

	String getDeviceName(int id_device);

	int[] getDeviceFormat(int id_device);

	int isFrontCamera(int index);

	int switchMobiledevicesCamera();

	int getMobiledevicesActiveCameraId();

	boolean isCaptureing(int id_device);

	int setVideoConfig(int id_device, QzVideoConfig config);

	QzVideoConfig getVideoConfig(int id_device);
	
	int enableDeflickering(int id_device, boolean enable);
	boolean deflickeringState(int id_device);

	int getCaptureDeviceSnapshot(int id_device, String file_name);

	int getRenderSnapshot(String account, int id_device, int id_preview,
			String file_name);

	int setShowNameInVideo(boolean enable);

	boolean getShowNameInVideoState();

	int enableDebugVideo(boolean enable);

	boolean getDebugVideoState();
	
}
