package com.lemeeting.conf;

import com.lemeeting.conf.defines.ACApplyMsg;
import com.lemeeting.conf.defines.ACConfCode;
import com.lemeeting.conf.defines.ACConfRoom;
import com.lemeeting.conf.defines.ACOrgInfo;
import com.lemeeting.conf.defines.ACOrgUser;
import com.lemeeting.conf.defines.ACPushMsg;
import com.lemeeting.conf.defines.ACUserBind;
import com.lemeeting.conf.defines.ACUserInfo;
import com.lemeeting.conf.defines.QzParamSetValue;
import com.lemeeting.conf.defines.QzRealConfInfo;

public interface IConferenceToCenterServerObserver {
	
	void onDisconnectCenter(int result);

	void onGetAuthCode(int result, QzParamSetValue[] param, String strCode);

	void onRegUserInfo(int result, QzParamSetValue[] param, ACUserInfo info);

	void onLogin(int result, QzParamSetValue[] param, ACUserInfo userInfo);

	void onLogout(int result, QzParamSetValue[] param);

	void onPrepareLoginConf(int result, QzParamSetValue[] param, int id_conf, String jsonAddress);

	void onGetRealConf(int result, QzParamSetValue[] param, QzRealConfInfo[] mapRealConf);

	void onGetUserInfo(int result, QzParamSetValue[] param, ACUserInfo info);

	void onUpdateUserInfo(int result, QzParamSetValue[] param, ACUserInfo info);

	void onRemoveUserInfo(int result, QzParamSetValue[] param, ACUserInfo info);

	void onGetUserInfoList(int result, QzParamSetValue[] param, ACUserInfo info, ACUserInfo[] listInfo);

	void onGetOrgInfo(int result, QzParamSetValue[] param, ACOrgInfo info);
	
	void onUpdateOrgInfo(int result, QzParamSetValue[] param, ACOrgInfo info);

	void onGetOrgInfoList(int result, QzParamSetValue[] param, ACOrgInfo info, ACOrgInfo[] listInfo);

	void onGetUserBind(int result, QzParamSetValue[] param, ACUserBind info);

	void onAddUserBind(int result, QzParamSetValue[] param, ACUserBind info);

	void onUpdateUserBind(int result, QzParamSetValue[] param, ACUserBind info);

	void onRemoveUserBind(int result, QzParamSetValue[] param, ACUserBind info);

	void onGetUserBindList(int result, QzParamSetValue[] param, ACUserBind info, ACUserBind[] listInfo);

	void onGetOrgUser(int result, QzParamSetValue[] param, ACOrgUser info);

	void onAddOrgUser(int result, QzParamSetValue[] param, ACOrgUser info);

	void onUpdateOrgUser(int result, QzParamSetValue[] param, ACOrgUser info);

	void onRemoveOrgUser(int result, QzParamSetValue[] param, ACOrgUser info);

	void onGetOrgUserList(int result, QzParamSetValue[] param, ACOrgUser info, ACOrgUser[] listInfo);

	void onGetConfRoom(int result, QzParamSetValue[] param, ACConfRoom info);

	void onAddConfRoom(int result, QzParamSetValue[] param, ACConfRoom info);

	void onUpdateConfRoom(int result, QzParamSetValue[] param, ACConfRoom info);

	void onRemoveConfRoom(int result, QzParamSetValue[] param, ACConfRoom info);

	void onGetConfRoomList(int result, QzParamSetValue[] param, ACConfRoom info, ACConfRoom[] listInfo);

	void onGetConfCode(int result, QzParamSetValue[] param, ACConfCode info);

	void onAddConfCode(int result, QzParamSetValue[] param, ACConfCode info);

	void onUpdateConfCode(int result, QzParamSetValue[] param, ACConfCode info);

	void onRemoveConfCode(int result, QzParamSetValue[] param, ACConfCode info);

	void onGetConfCodeList(int result, QzParamSetValue[] param, ACConfCode info, ACConfCode[] listInfo);

	void onGetApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info);

	void onAddApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info);

	void onUpdateApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info);

	void onRemoveApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info);

	void onGetApplyMsgList(int result, QzParamSetValue[] param, ACApplyMsg info, ACApplyMsg[] listInfo);
	
	void onGetPushMsg(int result, QzParamSetValue[] param, ACPushMsg info);

	void onAddPushMsg(int result, QzParamSetValue[] param, ACPushMsg info);

	void onUpdatePushMsg(int result, QzParamSetValue[] param, ACPushMsg info);

	void onRemovePushMsg(int result, QzParamSetValue[] param, ACPushMsg info);

	void onGetPushMsgList(int result, QzParamSetValue[] param, ACPushMsg info, ACPushMsg[] listInfo);

	void onNoticePushMsg(QzParamSetValue[] param, ACPushMsg info);

}
