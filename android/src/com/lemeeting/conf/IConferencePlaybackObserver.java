package com.lemeeting.conf;

public interface IConferencePlaybackObserver {

	public static final int kRec = 0;
	public static final int kRecVoice = 1;
	public static final int kRecVideo = 2;
	public static final int kRecWhiteboard = 3;
	public static final int kRecDesktopShare = 4;
	public static final int kRecTxt = 5;

	void onPlaybackOpen(int error);

	void onPlaybackPlay(int error);

	void onPlaybackPause(int error);

	void onPlaybackContinue(int error);

	void onPlaybackComplete();
}
