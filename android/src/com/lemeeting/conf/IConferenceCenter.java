package com.lemeeting.conf;

public interface IConferenceCenter {
	boolean start();

	void stop();

	IConferenceBase confBase();

	IConferenceBusiness confBusiness();

	IConferenceRecord confRecord();

	IConferencePlayback confPlayback();
	
	IConferenceShareDesktop confShareDesktop();

	IConferenceSwitchDataServer confSwitchDataServer();

	IConferenceToCenterServer confToCenterServer();

	IConferenceVideo confVideo();

	IConferenceVoice confVoice();

	IConferenceWhiteboard confWhiteboard();
	
	IConferenceBox confBox();
}
