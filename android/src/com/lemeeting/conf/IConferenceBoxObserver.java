package com.lemeeting.conf;

public interface IConferenceBoxObserver {

	void onCommandFromController(String cmd);

	void onControllerOnline(int nums);

	void onControllerOffline(int nums);

}
