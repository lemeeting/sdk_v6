package com.lemeeting.conf;

import com.lemeeting.conf.defines.ACApplyMsg;
import com.lemeeting.conf.defines.ACConfCode;
import com.lemeeting.conf.defines.ACConfRoom;
import com.lemeeting.conf.defines.ACOrgInfo;
import com.lemeeting.conf.defines.ACOrgUser;
import com.lemeeting.conf.defines.ACPushMsg;
import com.lemeeting.conf.defines.ACUserBind;
import com.lemeeting.conf.defines.ACUserInfo;
import com.lemeeting.conf.defines.QzParamSetValue;

public interface IConferenceToCenterServer {

	int addObserver(IConferenceToCenterServerObserver observer);

	int removeObserver(IConferenceToCenterServerObserver observer);

	int setCenterAddress(String address);
	String getCenterAddress();

	int getAuthCode(QzParamSetValue[] param);
	int regUserInfo(QzParamSetValue[] param, ACUserInfo info);

	int login(QzParamSetValue[] param, int id_org, String account, String psw);
	int logout(QzParamSetValue[] param);

	int prepareLoginConf(QzParamSetValue[] param, int id_conf);
	int getRealConf(QzParamSetValue[] param, Integer[] listConfID);

	int getUserInfo(QzParamSetValue[] param, String strAccount);
	int updateUserInfo(QzParamSetValue[] param, ACUserInfo info);
	int removeUserInfo(QzParamSetValue[] param, ACUserInfo info);
	int getUserInfoList(QzParamSetValue[] param, ACUserInfo info);

	int getOrgInfo(QzParamSetValue[] param, int id_org);
	int updateOrgInfo(QzParamSetValue[] param, ACOrgInfo info);
	int getOrgInfoList(QzParamSetValue[] param, ACOrgInfo info);

	int getUserBind(QzParamSetValue[] param, String strName);
	int addUserBind(QzParamSetValue[] param, ACUserBind info);
	int updateUserBind(QzParamSetValue[] param, ACUserBind info);
	int removeUserBind(QzParamSetValue[] param, ACUserBind info);
	int getUserBindList(QzParamSetValue[] param, ACUserBind info);

	int getOrgUser(QzParamSetValue[] param, int id_org, String strAccount);
	int addOrgUser(QzParamSetValue[] param, ACOrgUser info);
	int updateOrgUser(QzParamSetValue[] param, ACOrgUser info);
	int removeOrgUser(QzParamSetValue[] param, ACOrgUser info);
	int getOrgUserList(QzParamSetValue[] param, ACOrgUser info);

	int getConfRoom(QzParamSetValue[] param, int id_conf);
	int addConfRoom(QzParamSetValue[] param, ACConfRoom info);
	int updateConfRoom(QzParamSetValue[] param, ACConfRoom info);
	int removeConfRoom(QzParamSetValue[] param, ACConfRoom info);
	int getConfRoomList(QzParamSetValue[] param, ACConfRoom info);

	int getConfCode(QzParamSetValue[] param, String strCodeid);
	int addConfCode(QzParamSetValue[] param, ACConfCode info);
	int updateConfCode(QzParamSetValue[] param, ACConfCode info);
	int removeConfCode(QzParamSetValue[] param, ACConfCode info);
	int getConfCodeList(QzParamSetValue[] param, ACConfCode info);

	int getApplyMsg(QzParamSetValue[] param, int id_req);
	int addApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	int updateApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	int removeApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	int getApplyMsgList(QzParamSetValue[] param, ACApplyMsg info);
	
	int getPushMsg(QzParamSetValue[] param, int msg_id);
	int addPushMsg(QzParamSetValue[] param, ACPushMsg info);
	int updatePushMsg(QzParamSetValue[] param, ACPushMsg info);
	int removePushMsg(QzParamSetValue[] param, ACPushMsg info);
	int getPushMsgList(QzParamSetValue[] param, ACPushMsg info);

}
