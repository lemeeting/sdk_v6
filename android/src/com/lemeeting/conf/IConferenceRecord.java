package com.lemeeting.conf;

public interface IConferenceRecord {

	public static final int kRecordVoice = 1 << 0;
	public static final int kRecordVideo = 1 << 1;
	public static final int kRecordWbd = 1 << 2;
	public static final int kRecordAppShare = 1 << 3;
	public static final int kRecordText = 1 << 4;

	int addObserver(IConferenceRecordObserver observer);

	int removeObserver(IConferenceRecordObserver observer);

	int startLocalRecord(String file_name, int flag);

	int stopLocalRecord();

}
