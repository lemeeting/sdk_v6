package com.lemeeting.conf;

import com.lemeeting.conf.defines.QzPingResult;

public interface IConferenceSwitchDataServerObserver {
	void onPingResult(int task_id, QzPingResult result, long context, int error);
}
