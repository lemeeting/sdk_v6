package com.lemeeting.conf;

public interface IConferenceVideoObserver {
	void onAddLocalPreview(int id_device, int id_preview, long context, int error);

	void onVideoDeviceChanged(int id_changed, boolean is_add, int dev_nums, int id_main_dev);

	void onLocalVideoResolutionChanged(int id_device, int previewindex, int width, int height);

	void onRemoteVideoResolutionChanged(String account, int id_device, int previewindex, int width, int height);
}
