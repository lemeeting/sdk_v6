package com.lemeeting.conf;


import com.lemeeting.conf.defines.QzDataServerInfo;

public interface IConferenceSwitchDataServer {

	public static final int kVideoDataModule = 0;
	public static final int kVoiceDataModule = 1;
	public static final int kWhiteboardDataModule = 2;
	public static final int kDesktopSharedDataModule = 3;

	int addObserver(IConferenceSwitchDataServerObserver observer);

	int removeObserver(IConferenceSwitchDataServerObserver observer);

	QzDataServerInfo[] getClusterGroupDataServer();

	QzDataServerInfo getCurrentConnectDataServer(int module);

	int pingDataServer(String address, int port, long callback_time,
			boolean is_use_udp, long context);

	int stopPingDataServer(int task_id);

	int switchDataServer(int module, String address);
}
