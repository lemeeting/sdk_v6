package com.lemeeting.conf.defines;

public class QzParamSetValue {
	public static final int CK_START = 1;
	public static final int CK_COUNT = 2;
	public static final int CK_TOTAL = 3;
	public static final int CK_OPERATE_TYPE = 4;
	public static final int CK_SERVER_ID = 5;
	public static final int CK_TAG = 6;
	public static final int CK_TYPE = 7;
	public static final int CK_WORLD_ID = 8;
	public static final int CK_CONNECT_TYPE = 9;
	public static final int CK_CONNECT_ID = 10;
	public static final int CK_CONNECT_ACCOUNT = 11;
	public static final int CK_HTTP_TYPE = 12;
	public static final int CK_CONF_CODE =13;
	public static final int CK_SOURCE_VERSION = 14;
	public static final int CK_CLIENT_TYPE = 15;
	public static final int CK_CLIENT_VERSION = 16;
	public static final int CK_CLIENT_MIN_VERSION = 17;
	public static final int CK_CLIENT_URL = 18;

	public static final int CK_CONDITION = 20;
	public static final int CK_ORDERBY = 21;
	public static final int CK_ACCOUNT = 22;
	public static final int CK_NAME = 23;
	public static final int CK_PARAM = 24;
	public static final int CK_MULTI_ACCOUNT = 25;
	public static final int CK_CONF_PASSWORD = 26;
	public static final int CK_IS_CREATE_CODE = 27;
	public static final int CK_CONF_GUEST_USER = 28;
	public static final int CK_SESSION_ID = 29;
	public static final int CK_CONF_NAME = 30;
	public static final int CK_QUERY_TIME = 31;
	public static final int CK_USER_PERM = 32;
	public static final int CK_SUCCESS_COUNT = 33;
	public static final int CK_ORG_ACCOUNT = 34;
	public static final int CK_TOKEN = 35;
	public static final int CK_VERSION_NAME = 36;
	public static final int CK_VERSION_INFO = 37;
	public static final int CK_MAC_ADDRESS = 38;


	public int key;
	public String value;
	
	public void setValue(int key, int value) {
		this.key = key;
		this.value = String.valueOf(value);
	}
	
	public void setValue(int key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public void setTimeValue(int key, long time) {
		this.key = key;
		this.value = String.valueOf(value);
	}
	
	static String getKeyName(int key) {
		switch (key)
		{
			case CK_START:
				return "start";
			case CK_COUNT:
				return "count";
			case CK_TOTAL:
				return "total";
			case CK_WORLD_ID:
				return "world_id";
			case CK_ACCOUNT:
				return "account";
			default:
				break;
		}
		return Integer.toString(key);
	}
	
	static boolean getKeyName(int key, String strName) {
		switch (key)
		{
			case CK_START:
				strName = "start";
				break;
			case CK_COUNT:
				strName = "count";
				break;
			case CK_TOTAL:
				strName = "total";
				break;
			case CK_WORLD_ID:
				strName = "world_id";
				break;
			case CK_ACCOUNT:
				strName = "account";
				break;
			default:
				return false;
		}
		return true;
	}
}
