package com.lemeeting.conf.defines;

public class QzDataServerInfo {
	
	public static final int	kVideoServer = 2;
	public static final int	kVoiceServer = 3;
	public static final int	kWhiteboardServer = 4;
	public static final int	kShareDesktopServer = 5;
	
	public int getType_() {
		return type_;
	}
	public void setType_(int type_) {
		this.type_ = type_;
	}
	public int getId_cluster_group_() {
		return id_cluster_group_;
	}
	public void setId_cluster_group_(int id_cluster_group_) {
		this.id_cluster_group_ = id_cluster_group_;
	}
	public int getId_access_server_() {
		return id_access_server_;
	}
	public void setId_access_server_(int id_access_server_) {
		this.id_access_server_ = id_access_server_;
	}
	public int getId_server_() {
		return id_server_;
	}
	public void setId_server_(int id_server_) {
		this.id_server_ = id_server_;
	}
	public String getName_() {
		return name_;
	}
	public void setName_(String name_) {
		this.name_ = name_;
	}
	public String getExpansion_() {
		return expansion_;
	}
	public void setExpansion_(String expansion_) {
		this.expansion_ = expansion_;
	}
	public String getAddress_() {
		return address_;
	}
	public void setAddress_(String address_) {
		this.address_ = address_;
	}
	public int getPort_() {
		return port_;
	}
	public void setPort_(int port_) {
		this.port_ = port_;
	}
	public int getState_() {
		return state_;
	}
	public void setState_(int state_) {
		this.state_ = state_;
	}
	public int getMax_attendee_nums_() {
		return max_attendee_nums_;
	}
	public void setMax_attendee_nums_(int max_attendee_nums_) {
		this.max_attendee_nums_ = max_attendee_nums_;
	}
	public int getOnline_attendee_nums_() {
		return online_attendee_nums_;
	}
	public void setOnline_attendee_nums_(int online_attendee_nums_) {
		this.online_attendee_nums_ = online_attendee_nums_;
	}
	
	int type_;
	int id_cluster_group_;
	int id_access_server_;
	int id_server_;
	String name_;
	String expansion_;
	String address_;
	int port_;
	int state_;
	int max_attendee_nums_;
	int online_attendee_nums_;
}
