package com.lemeeting.conf.defines;

public class QzRealConfInfo {
	public int m_iversionid;
	public int m_iconfid;
	public int m_iworldid;
	public int m_igroupid;
	public int m_iserverid;
	public int m_ionlinenumber;
	public long m_dtmodifytime;
	public int m_iflag;
	public String m_strextendjson;
}
