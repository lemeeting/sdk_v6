package com.lemeeting.conf.defines;

public class QzVideoConfig {
	public static final int kNoRescaling = 0;
	public static final int kFastRescaling = 1;
	public static final int kBiLinear = 2;
	public static final int kBox = 3;
	
	int capture_width = 640;
	int capture_height = 480;
	int encode_width = 352;
	int encode_height = 288;
	int rotation = 0;
	int capture_fps = 30;
	int encode_fps = 15;
	int encode_bitrate = 200;	//150-1200
	int encode_qp = 30;	//26-51
	int frame_interval = 100;
	int resampling = kFastRescaling;
	
	
	public int getCapture_width() {
		return capture_width;
	}
	public void setCapture_width(int capture_width) {
		this.capture_width = capture_width;
	}
	public int getCapture_height() {
		return capture_height;
	}
	public void setCapture_height(int capture_height) {
		this.capture_height = capture_height;
	}
	public int getEncode_width() {
		return encode_width;
	}
	public void setEncode_width(int encode_width) {
		this.encode_width = encode_width;
	}
	public int getEncode_height() {
		return encode_height;
	}
	public void setEncode_height(int encode_height) {
		this.encode_height = encode_height;
	}
	public int getRotation() {
		return rotation;
	}
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}
	public int getCapture_fps() {
		return capture_fps;
	}
	public void setCapture_fps(int capture_fps) {
		this.capture_fps = capture_fps;
	}
	public int getEncode_fps() {
		return encode_fps;
	}
	public void setEncode_fps(int encode_fps) {
		this.encode_fps = encode_fps;
	}
	public int getEncode_bitrate() {
		return encode_bitrate;
	}
	public void setEncode_bitrate(int encode_bitrate) {
		this.encode_bitrate = encode_bitrate;
	}
	public int getEncode_qp() {
		return encode_qp;
	}
	public void setEncode_qp(int encode_qp) {
		this.encode_qp = encode_qp;
	}
	public int getFrame_interval() {
		return frame_interval;
	}
	public void setFrame_interval(int frame_interval) {
		this.frame_interval = frame_interval;
	}
	public int getResampling() {
		return resampling;
	}
	public void setResampling(int resampling) {
		this.resampling = resampling;
	}
	
}
