package com.lemeeting.conf.defines;

public class ACConfRoom {
	
	static final int PMNULL = 0;
	static final int PM_VERSION_ID = 1 << 0;
	static final int PM_DELETE_FLAG = 1 << 1;
	static final int PM_CONF_ID = 1 << 2;
	static final int PM_CONF_TYPE = 1 << 3;
	static final int PM_WORLD_ID = 1 << 4;
	static final int PM_GROUP_ID = 1 << 5;
	
	static final int PM_SERVER_ID = 1 << 6;
	static final int PM_CONF_NAME = 1 << 7;
	static final int PM_CONF_PASSWORD = 1 << 8;
	static final int PM_MANAGE_PASSWORD = 1 << 9;
	static final int PM_START_TIME = 1 << 10;
	static final int PM_END_TIME = 1 << 11;
	static final int PM_MAX_USER_COUNT = 1 << 12;
	static final int PM_MAX_SPEAKER_COUNT = 1 << 13;
	static final int PM_CREATOR = 1 << 14;
	static final int PM_MENDER = 1 << 15;
	static final int PM_CREATE_TIME = 1 << 16;
	static final int PM_MODIFY_TIME = 1 << 17;
	static final int PM_CONF_FLAG = 1 << 18;
	static final int PM_SETTING_JSON = 1 << 19;
	static final int PM_EXTEND_JSON = 1 << 20;
	static final int PMALL = 0xffffffff;
	static final int PMCOUNT = 21;
	
	public int getM_uiversionid() {
		return m_uiversionid;
	}
	public void setM_uiversionid(int m_uiversionid) {
		this.m_uiversionid = m_uiversionid;
		m_uiMarker |= PM_VERSION_ID;
	}
	public int getM_ideleteflag() {
		return m_ideleteflag;
	}
	public void setM_ideleteflag(int m_ideleteflag) {
		this.m_ideleteflag = m_ideleteflag;
		m_uiMarker |= PM_DELETE_FLAG;
	}
	public int getM_uiconfid() {
		return m_uiconfid;
	}
	public void setM_uiconfid(int m_uiconfid) {
		this.m_uiconfid = m_uiconfid;
		m_uiMarker |= PM_CONF_ID;
	}
	public int getM_uiconftype() {
		return m_uiconftype;
	}
	public void setM_uiconftype(int m_uiconftype) {
		this.m_uiconftype = m_uiconftype;
		m_uiMarker |= PM_CONF_TYPE;
	}
	public int getM_uiworldid() {
		return m_uiworldid;
	}
	public void setM_uiworldid(int m_uiworldid) {
		this.m_uiworldid = m_uiworldid;
		m_uiMarker |= PM_WORLD_ID;
	}
	public int getM_uigroupid() {
		return m_uigroupid;
	}
	public void setM_uigroupid(int m_uigroupid) {
		this.m_uigroupid = m_uigroupid;
		m_uiMarker |= PM_GROUP_ID;
	}
	public int getM_uiserverid() {
		return m_uiserverid;
	}
	public void setM_uiserverid(int m_uiserverid) {
		this.m_uiserverid = m_uiserverid;
		m_uiMarker |= PM_SERVER_ID;
	}
	
	public String getM_strconfname() {
		return m_strconfname;
	}
	
	public void setM_strconfname(String m_strconfname) {
		this.m_strconfname = m_strconfname;
		m_uiMarker |= PM_CONF_NAME;
	}
	public String getM_strconfpassword() {
		return m_strconfpassword;
	}
	public void setM_strconfpassword(String m_strconfpassword) {
		this.m_strconfpassword = m_strconfpassword;
		m_uiMarker |= PM_CONF_PASSWORD;
	}
	public String getM_strmanagepassword() {
		return m_strmanagepassword;
	}
	public void setM_strmanagepassword(String m_strmanagepassword) {
		this.m_strmanagepassword = m_strmanagepassword;
		m_uiMarker |= PM_MANAGE_PASSWORD;
	}
	public long getM_dtstarttime() {
		return m_dtstarttime;
	}
	public void setM_dtstarttime(long m_dtstarttime) {
		this.m_dtstarttime = m_dtstarttime;
		m_uiMarker |= PM_START_TIME;
	}
	public long getM_dtendtime() {
		return m_dtendtime;
	}
	
	public void setM_dtendtime(long m_dtendtime) {
		this.m_dtendtime = m_dtendtime;
		m_uiMarker |= PM_END_TIME;
	}
	public int getM_uimaxusercount() {
		return m_uimaxusercount;
	}
	public void setM_uimaxusercount(int m_uimaxusercount) {
		this.m_uimaxusercount = m_uimaxusercount;
		m_uiMarker |= PM_MAX_USER_COUNT;
	}
	public int getM_uimaxspeakercount() {
		return m_uimaxspeakercount;
	}
	public void setM_uimaxspeakercount(int m_uimaxspeakercount) {
		this.m_uimaxspeakercount = m_uimaxspeakercount;
		m_uiMarker |= PM_MAX_SPEAKER_COUNT;
	}
	public String getM_strcreator() {
		return m_strcreator;
	}
	public void setM_strcreator(String m_strcreator) {
		this.m_strcreator = m_strcreator;
		m_uiMarker |= PM_CREATOR;
	}
	public String getM_strmender() {
		return m_strmender;
	}
	public void setM_strmender(String m_strmender) {
		this.m_strmender = m_strmender;
		m_uiMarker |= PM_MENDER;
	}
	public long getM_dtcreatetime() {
		return m_dtcreatetime;
	}
	public void setM_dtcreatetime(long m_dtcreatetime) {
		this.m_dtcreatetime = m_dtcreatetime;
		m_uiMarker |= PM_CREATE_TIME;
	}
	public long getM_dtmodifytime() {
		return m_dtmodifytime;
	}
	public void setM_dtmodifytime(long m_dtmodifytime) {
		this.m_dtmodifytime = m_dtmodifytime;
		m_uiMarker |= PM_MODIFY_TIME;
	}
	public int getM_uiconfflag() {
		return m_uiconfflag;
	}
	public void setM_uiconfflag(int m_uiconfflag) {
		this.m_uiconfflag = m_uiconfflag;
		m_uiMarker |= PM_CONF_FLAG;
	}
	public String getM_strsettingjson() {
		return m_strsettingjson;
	}
	public void setM_strsettingjson(String m_strsettingjson) {
		this.m_strsettingjson = m_strsettingjson;
		m_uiMarker |= PM_SETTING_JSON;
	}
	public String getM_strextendjson() {
		return m_strextendjson;
	}
	public void setM_strextendjson(String m_strextendjson) {
		this.m_strextendjson = m_strextendjson;
		m_uiMarker |= PM_EXTEND_JSON;
	}
	
	public int getM_uiMarker() {
		return m_uiMarker;
	}
	public void setM_uiMarker(int m_uiMarker) {
		this.m_uiMarker = m_uiMarker;
	}
	
	public ACConfRoom() {
		m_uiMarker = 0;
		m_strconfname = "";
		m_strconfpassword = "";
		m_strmanagepassword = "";
		m_strcreator = "";
		m_strmender = "";
		m_strsettingjson = "";
		m_strextendjson = "";
	}
	

	int m_uiMarker;
	int m_uiversionid;
	int m_ideleteflag;
	int m_uiconfid;
	int m_uiconftype;
	int m_uiworldid;
	int m_uigroupid;
	int m_uiserverid;
	String m_strconfname;
	String m_strconfpassword;
	String m_strmanagepassword;
	long m_dtstarttime;
	long m_dtendtime;
	int m_uimaxusercount;
	int m_uimaxspeakercount;
	String m_strcreator;
	String m_strmender;
	long m_dtcreatetime;
	long m_dtmodifytime;
	int m_uiconfflag;
	String m_strsettingjson;
	String m_strextendjson;
}
