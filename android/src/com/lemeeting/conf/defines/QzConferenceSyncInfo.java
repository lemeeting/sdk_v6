package com.lemeeting.conf.defines;

public class QzConferenceSyncInfo {
	public boolean isIs_sync_() {
		return is_sync_;
	}
	public void setIs_sync_(boolean is_sync_) {
		this.is_sync_ = is_sync_;
	}
	public String getSyncer_() {
		return syncer_;
	}
	public void setSyncer_(String syncer_) {
		this.syncer_ = syncer_;
	}
	public String getSync_data_() {
		return sync_data_;
	}
	public void setSync_data_(String sync_data_) {
		this.sync_data_ = sync_data_;
	}
	
	boolean is_sync_;
	String syncer_;
	String sync_data_;
}
