package com.lemeeting.conf.defines;

public class ACUserInfo {
	public int m_uiversionid;
	public int m_uideleteflag;
	public int m_uiworldid;
	public String m_struseraccount;
	public int m_uiusertype;
	public int m_iuserstatus;
	public String m_strusername;
	public String m_strpassword;
	public String m_strphone;
	public String m_stremail;
	public long m_dtbirthday;
	public long m_dtcreatetime;
	public String m_strsettingjson;
	public String m_strextendjson;
}
