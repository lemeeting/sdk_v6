package com.lemeeting.conf.defines;

public class ACApplyMsg {
	public int m_uiversionid;
	public int m_uideleteflag;
	public int m_uireqid;
	public int m_ireqtype;
	public int m_ireqstate;
	public int m_ireqresult;
	public long m_dtreqtime;
	public long m_dtrsptime;
	public String m_strreqmsg;
	public String m_strrspmsg;
	public String m_strreqdata;
}
