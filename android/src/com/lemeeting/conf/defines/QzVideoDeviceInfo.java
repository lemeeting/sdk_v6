package com.lemeeting.conf.defines;

public class QzVideoDeviceInfo {
	public static final int kUnknowCamera = -1;
	public static final int kLocalCamera = 0;
	public static final int kDesktopCamera = 1;
	public static final int kIPCamera = 2;
	public static final int kPlayBackCamera = 3;
	public static final int kMediaFileCamera = 4;
	public static final int kMaxCameraType = 5;

	public int getType_() {
		return type_;
	}
	public int getId_() {
		return id_;
	}
	public boolean isEnable_() {
		return enable_;
	}
	public String getShow_name_() {
		return show_name_;
	}

	int type_ = kLocalCamera;
	int id_ = -1;
	boolean enable_ = true;
	String show_name_ = "";
}
