package com.lemeeting.conf.defines;

public class ACPushMsg {
	public int m_uiversionid;
	public int m_uideleteflag;
	public int m_uimsgid;
	public int m_imsgtype;
	public int m_imsgstate;
	public int m_imsgresult;
	public int m_isendorgid;
	public String m_strsendaccount;
	public long m_dtsendtime;
	public long m_dtendtime;
	public String m_strmsgtopic;
	public String m_strmsgdata;
	public String m_strmsgreceiver;
	public String m_strextendjson;
}
