package com.lemeeting.conf.defines;

public class QzProxyOptions {
	public static final int kPTInvalid = 0;
	public static final int kPTSocks4 = 1;
	public static final int kPTSocks5 = 2;
	public static final int kPTHttpConnect = 3;
	public static final int kPTHttpTunnel = 4;

	public int getProxyType() {
		return proxyType;
	}
	public void setProxyType(int proxyType) {
		this.proxyType = proxyType;
	}
	public int getProxyPort() {
		return proxyPort;
	}
	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}
	public String getsProxyAddr() {
		return sProxyAddr;
	}
	public void setsProxyAddr(String sProxyAddr) {
		this.sProxyAddr = sProxyAddr;
	}
	public String getsProxyUserName() {
		return sProxyUserName;
	}
	public void setsProxyUserName(String sProxyUserName) {
		this.sProxyUserName = sProxyUserName;
	}
	public String getsProxyPwd() {
		return sProxyPwd;
	}
	public void setsProxyPwd(String sProxyPwd) {
		this.sProxyPwd = sProxyPwd;
	}
	
	int proxyType = kPTInvalid;
	int proxyPort = 0;
	String sProxyAddr = "";
	String sProxyUserName = "";
	String sProxyPwd = "";
}
