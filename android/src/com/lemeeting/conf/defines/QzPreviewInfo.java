package com.lemeeting.conf.defines;

import android.view.SurfaceView;

public class QzPreviewInfo {
	public String account;
	public int index_or_id_device;	
	public SurfaceView window;
	public int z_order;
	public float left;
	public float top;
	public float right;
	public float bottom;
	public long context;
}
