package com.lemeeting.conf.defines;

public class QzRealTimeConferenceInfo {
	public static final int kConferenceFree = 1 << 0;
	public static final int kConferenceLock = 1 << 1;
	public static final int kConferenceOpen = 1 << 2;
	public static final int kConferenceHide = 1 << 3;
	public static final int kConferenceForceSync = 1 << 4;
	public static final int kConferenceDisableText = 1 << 5;
	public static final int kConferenceDisableRecord = 1 << 6;
	public static final int kConferenceDisableBrowVideo = 1 << 7;
	public static final int kConferencePlayback = 1 << 8;
	public static final int kConferenceDisableMicrophone = 1 << 9;
	public static final int kConferenceMuted = 1 << 10;
	public static final int kConferenceLockScreen = 1 << 11;

	public static final int	kNoSignin = 0;
	public static final int	kImmediatelySignin = 0;
	public static final int	kTimingSignin =1;

	public boolean isFree() { return ((flags_ >>> 0) & 0x01) != 0; }
	public boolean isLock() { return ((flags_ >>> 1) & 0x01) != 0; }
	public boolean isOpen() { return ((flags_ >>> 2) & 0x01) != 0; }
	public boolean isHide() { return ((flags_ >>> 3) & 0x01) != 0; }
	public boolean isForceSync() { return ((flags_ >>> 4) & 0x01) != 0; }
	public boolean isDisableText() { return ((flags_ >>> 5) & 0x01) != 0; }
	public boolean isDisableRecord()  { return ((flags_ >>> 6) & 0x01) != 0; }
	public boolean isDisableBrowVideo() { return ((flags_ >>> 7) & 0x01) != 0; }
	public boolean isPlayback() { return ((flags_ >>> 8) & 0x01) != 0; }
	public boolean isDisableAllMicrophone() { return ((flags_ >>> 9) & 0x01) != 0; }
	public boolean isAllMuted() { return ((flags_ >>> 10) & 0x01) != 0; }
	public boolean isLockScreen() { return ((flags_ >> 11) & 0x01) != 0; }

	public void setFree(boolean v) { if (v) flags_ |= kConferenceFree; else flags_ &= ~kConferenceFree; }
	public void setLock(boolean v) { if (v) flags_ |= kConferenceLock; else flags_ &= ~kConferenceLock; }
	public void setOpen(boolean v) { if (v) flags_ |= kConferenceOpen; else flags_ &= ~kConferenceOpen; }
	public void setHide(boolean v) { if (v) flags_ |= kConferenceHide; else flags_ &= ~kConferenceHide; }
	public void setForceSync(boolean v) { if (v) flags_ |= kConferenceForceSync; else flags_ &= ~kConferenceForceSync; }
	public void setDisableText(boolean v) { if (v) flags_ |= kConferenceDisableText; else flags_ &= ~kConferenceDisableText; }
	public void setDisableRecord(boolean v) { if (v) flags_ |= kConferenceDisableRecord; else flags_ &= ~kConferenceDisableRecord; }
	public void setDisableBrowVideo(boolean v) { if (v) flags_ |= kConferenceDisableBrowVideo; else flags_ &= ~kConferenceDisableBrowVideo; }
	public void setPlayback(boolean v) { if (v) flags_ |= kConferencePlayback; else flags_ &= ~kConferencePlayback; }
	public void setDisableAllMicrophone(boolean v) { if (v) flags_ |= kConferenceDisableMicrophone; else flags_ &= ~kConferenceDisableMicrophone; }
	public void setAllMuted(boolean v) { if (v) flags_ |= kConferenceMuted; else flags_ &= ~kConferenceMuted; }
	public void SetLockScreen(boolean v) { if (v) flags_ |= kConferenceLockScreen; else flags_ &= ~kConferenceLockScreen; }

	QzRealTimeConferenceInfo() {
		id_conference_ = 0;
		flags_ = 0;
		online_attendees_ = 0;
		conf_password_ = "";
		admin_password_ = "";	
		signin_type_ = kNoSignin;
		launch_signin_time_ = 0;
		stop_signin_time_ = 0;
	}
	
	public int getId_conference_() {
		return id_conference_;
	}
	
	public void setId_conference_(int id_conference_) {
		this.id_conference_ = id_conference_;
	}
	
	public int getFlags_() {
		return flags_;
	}
	
	public void setFlags_(int flags_) {
		this.flags_ = flags_;
	}
	
	public int getOnline_attendees_() {
		return online_attendees_;
	}
	
	public void setOnline_attendees_(int online_attendees_) {
		this.online_attendees_ = online_attendees_;
	}
	
	public String getConf_password_() {
		return conf_password_;
	}
	
	public void setConf_password_(String conf_password_) {
		this.conf_password_ = conf_password_;
	}
	
	public String getAdmin_password_() {
		return admin_password_;
	}
	
	public void setAdmin_password_(String admin_password_) {
		this.admin_password_ = admin_password_;
	}

	public String getExtend_json_() {
		return extend_json_;
	}
	public void setExtend_json_(String extend_json_) {
		this.extend_json_ = extend_json_;
	}
	
	public boolean isIs_launch_signin_() {
		return launch_signin_time_ > 0;
	}
	
	public boolean isIs_Stop_signin_() {
		return  stop_signin_time_ < Long.MAX_VALUE;
	}
	
	public long getLaunch_signin_time_() {
		return launch_signin_time_;
	}
	
	public void setLaunch_signin_time_(long launch_signin_time_) {
		this.launch_signin_time_ = launch_signin_time_;
	}
	
	public long getStop_signin_time_() {
		return stop_signin_time_;
	}
	public void setStop_signin_time_(long stop_signin_time_) {
		this.stop_signin_time_ = stop_signin_time_;
	}
	
	public int getSignin_type_() {
		return signin_type_;
	}
	
	public void setSignin_type_(int signin_type_) {
		this.signin_type_ = signin_type_;
	}

	int id_conference_;
	int flags_;
	int online_attendees_;
	String conf_password_;
	String admin_password_;
	String extend_json_;
	long launch_signin_time_;
	long stop_signin_time_;
	int signin_type_;
}
