package com.lemeeting.conf.defines;

public class QzPingResult {
	public int getAll_packet() {
		return all_packet;
	}
	public void setAll_packet(int all_packet) {
		this.all_packet = all_packet;
	}
	public int getLost_packet() {
		return lost_packet;
	}
	public void setLost_packet(int lost_packet) {
		this.lost_packet = lost_packet;
	}
	public double getMax_delaytime() {
		return max_delaytime;
	}
	public void setMax_delaytime(double max_delaytime) {
		this.max_delaytime = max_delaytime;
	}
	public double getMin_delaytime() {
		return min_delaytime;
	}
	public void setMin_delaytime(double min_delaytime) {
		this.min_delaytime = min_delaytime;
	}
	public double getAvg_delaytime() {
		return avg_delaytime;
	}
	public void setAvg_delaytime(double avg_delaytime) {
		this.avg_delaytime = avg_delaytime;
	}
	public double getEval_value() {
		return eval_value;
	}
	public void setEval_value(double eval_value) {
		this.eval_value = eval_value;
	}
	
	int all_packet;
	int lost_packet;
	double max_delaytime;
	double min_delaytime;
	double avg_delaytime;
	double eval_value;
}
