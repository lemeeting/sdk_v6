package com.lemeeting.conf.defines;

public class QzAttendee {
	public static final int	kOpsNormal = 0;
	public static final int	kOpsApplySpeak = 1 << 0;
	public static final int	kOpsSpeak = 1 << 1;
	public static final int	kOpsApplyOperData = 1 << 2;
	public static final int	kOpsOperData = 1 << 3;
	public static final int	kOpsApplySync = 1 << 4;
	public static final int	kOpsSync = 1 << 5;
	public static final int	kOpsApplyAdmin = 1 << 6;
	public static final int	kOpsRecord = 1 << 7;
	public static final int	kOpsRecordPause = 1 << 8;
	public static final int	kOpsMediaPlay = 1 << 9;
	public static final int	kOpsMediaPlayPause = 1 << 10;
	public static final int	kOpsSharedDesktop = 1 << 11;
	public static final int	kOpsOperWhiteboard = 1 << 12;
	public static final int	kOpsPlayback = 1 << 13;
	public static final int	kOpsAskRemoteControlDesktop = 1 << 14;
	public static final int	kOpsInRemoteControlDesktop = 1 << 15;

	public static final int	kUnknow = -1;
	public static final int	kPC = 0;
	public static final int	kMAC = 1;
	public static final int	kLinux = 2;
	public static final int	kAndroid = 3;
	public static final int	kIPhone = 4;
	public static final int	kIPad = 5;
	public static final int	kWinPhone = 6;
	public static final int	kH323 = 7;
	public static final int	kBox = 8;
	
	public static final int	kConferenceNonAdmin = 0;
	public static final int	kConferenceSystemAdmin = 1;
	public static final int	kConferenceTimelessAdmin = 2;
	public static final int	kConferenceTempAdmin = 3;
	public static final int	kConferenceGroupAdmin = 4;
	
		
	public void setApplySpeakOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsApplySpeak;
		}
		else {
			ops_status_ &= ~kOpsApplySpeak;
		}		
	}
	
	public void setSpeakOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsSpeak;
		}
		else {
			ops_status_ &= ~kOpsSpeak;
		}		
	}
	
	public void setApplyDataOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsApplyOperData;
		}
		else {
			ops_status_ &= ~kOpsApplyOperData;
		} 
	}
	
	public void setDataOp(boolean v) {		
		if(v) {
			ops_status_ |= kOpsOperData;
		}
		else {
			ops_status_ &= ~kOpsOperData;
		} 
	}
	
	public void setApplySyncOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsApplySync;
		}
		else {
			ops_status_ &= ~kOpsApplySync;
		} 
	}
	
	public void setSyncOp(boolean v) { 
		if(v) {
			ops_status_ |= kOpsSync;
		}
		else {
			ops_status_ &= ~kOpsSync;
		}
	}
	
	public void setApplyAdminOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsApplyAdmin;
		}
		else {
			ops_status_ &= ~kOpsApplyAdmin;
		}
	}
	
	public void setSharedDesktopOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsSharedDesktop;
		}
		else {
			ops_status_ &= ~kOpsSharedDesktop;
		}
	}
	
	public void setPlaybackOp(boolean v) {
		if(v) {
			ops_status_ |= kOpsPlayback;
		}
		else {
			ops_status_ &= ~kOpsPlayback;
		}
	}
	
	public void SetAskRemoteControlDesktopStatus(boolean v) {
		if(v) {
			ops_status_ |= kOpsAskRemoteControlDesktop;
		}
		else {
			ops_status_ &= ~kOpsAskRemoteControlDesktop;
		}
	}
	
	public void SetInRemoteControlDesktopStatus(boolean v) { 
		if(v) {
			ops_status_ |= kOpsInRemoteControlDesktop;
		}
		else {
			ops_status_ &= ~kOpsInRemoteControlDesktop;
		}
	}


	public boolean isMuteStatus(){
		return ((ops_status_ & kOpsApplySpeak) == 0 && (ops_status_ & kOpsSpeak) == 0);
	}
	public boolean isApplySpeakOp() {
		return (ops_status_ & kOpsApplySpeak) != 0;
	}
	
	public boolean isSpeakOp() {
		return (ops_status_ & kOpsSpeak) != 0;
	}
	
	public boolean isApplyDataOp() {
		return (ops_status_ & kOpsApplyOperData) != 0;
	}
	
	public boolean isDataOp() {
		return (ops_status_ & kOpsOperData) != 0;
	}
	
	public boolean isApplySyncOp() {
		return (ops_status_ & kOpsApplySync) != 0;
	}
	
	public boolean isSyncOp() {
		return (ops_status_ & kOpsSync) != 0;
	}
	
	public boolean isApplyAdminOp() {
		return (ops_status_ & kOpsApplyAdmin) != 0;
	}
	
	public boolean isSharedDesktopOp() {
		return (ops_status_ & kOpsSharedDesktop) != 0;
	}

	public boolean isPlaybackOp() {
		return (ops_status_ & kOpsPlayback) != 0;
	}
	
	public boolean askRemoteControlDesktopStatus() {
		return (ops_status_ & kOpsAskRemoteControlDesktop) != 0;
	}
	
	public boolean inRemoteControlDesktopStatus() { 
		return (ops_status_ & kOpsInRemoteControlDesktop) != 0;
	}

	public boolean videoCaptureState(int devindex) {
		return ((video_state_ >>> devindex) & 0x01) != 0;
	}

	public boolean isAdmin()  {
		return adminprivilege_ != kConferenceNonAdmin; 
	}
	
	public int getType_() {
		return type_;
	}

	public void setType_(int type_) {
		this.type_ = type_;
	}

	public int getLogin_type_() {
		return login_type_;
	}

	public void setLogin_type_(int login_type_) {
		this.login_type_ = login_type_;
	}

	public int getOps_status_() {
		return ops_status_;
	}

	public void setOps_status_(int ops_status_) {
		this.ops_status_ = ops_status_;
	}

	public int getAdminprivilege_() {
		return adminprivilege_;
	}

	public void setAdminprivilege_(int adminprivilege_) {
		this.adminprivilege_ = adminprivilege_;
	}

	public int getVideo_nums_() {
		return video_nums_;
	}

	public void setVideo_nums_(int video_nums_) {
		this.video_nums_ = video_nums_;
	}

	public int getVoice_nums_() {
		return voice_nums_;
	}

	public void setVoice_nums_(int voice_nums_) {
		this.voice_nums_ = voice_nums_;
	}

	public long getVideo_state_() {
		return video_state_;
	}

	public void setVideo_state_(long video_state_) {
		this.video_state_ = video_state_;
	}

	public boolean isForbiddenvideo_() {
		return forbiddenvideo_;
	}

	public void setForbiddenvideo_(boolean forbiddenvideo_) {
		this.forbiddenvideo_ = forbiddenvideo_;
	}

	public int getMain_video_id_() {
		return main_video_id_;
	}

	public void setMain_video_id_(int main_video_id) {
		this.main_video_id_ = main_video_id;
	}

	public int getId_enterprise_() {
		return id_enterprise_;
	}

	public void setId_enterprise_(int id_enterprise_) {
		this.id_enterprise_ = id_enterprise_;
	}

	public String getName_() {
		return name_;
	}

	public void setName_(String name_) {
		this.name_ = name_;
	}

	public String getMac_address_() {
		return mac_address_;
	}

	public void setMac_address_(String mac_address_) {
		this.mac_address_ = mac_address_;
	}

	public String getGuid_() {
		return guid_;
	}

	public void setGuid_(String guid_) {
		this.guid_ = guid_;
	}

	public String getJson_() {
		return json_;
	}

	public void setJson_(String json_) {
		this.json_ = json_;
	}

	public int getId_version_() {
		return id_version_;
	}

	public void setId_version_(int id_version_) {
		this.id_version_ = id_version_;
	}

	public int getId_subgroup_() {
		return id_subgroup_;
	}

	public void setId_subgroup_(int id_subgroup_) {
		this.id_subgroup_ = id_subgroup_;
	}

	public int getId_access_server_() {
		return id_access_server_;
	}

	public void setId_access_server_(int id_access_server_) {
		this.id_access_server_ = id_access_server_;
	}

	public int getId_conference_() {
		return id_conference_;
	}

	public void setId_conference_(int id_conference_) {
		this.id_conference_ = id_conference_;
	}

	public String getAccount_() {
		return account_;
	}

	public void setAccount_(String account_) {
		this.account_ = account_;
	}
	
	public long getSignin_time_() {
		return signin_time_;
	}

	public void setSignin_time_(long signin_time_) {
		this.signin_time_ = signin_time_;
	}
	
	public QzVideoDeviceInfo[] getVideo_devices_() {
		return video_devices_;
	}
	
	public void setVideo_devices_(QzVideoDeviceInfo[] video_devices_) {
		this.video_devices_ = video_devices_;
	}
	
	public void resetValue(QzAttendee newValue) {
		this.type_ = newValue.type_;
		this.login_type_ = newValue.login_type_;
		this.ops_status_ = newValue.ops_status_;
		this.adminprivilege_ = newValue.adminprivilege_;
		this.video_nums_ = newValue.video_nums_;
		this.voice_nums_ = newValue.voice_nums_;
		this.video_state_ = newValue.video_state_;
		this.forbiddenvideo_ = newValue.forbiddenvideo_;
		this.main_video_id_ = newValue.main_video_id_;
		this.id_enterprise_ = newValue.id_enterprise_;
		
		this.name_ = newValue.name_;
		this.mac_address_ = newValue.mac_address_;
		this.guid_ = newValue.guid_;
		this.json_ = newValue.json_;
		this.id_version_ = newValue.id_version_;
		this.id_subgroup_ = newValue.id_subgroup_;
		this.id_access_server_ = newValue.id_access_server_;
		this.id_conference_ = newValue.id_conference_;
		this.account_ = newValue.account_;
		this.signin_time_ = newValue.signin_time_;
		this.video_devices_ = newValue.video_devices_;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getName_();
	}
	
	public boolean isSameAttendee(QzAttendee attendee){
		boolean result = false;
		if (this.account_.equalsIgnoreCase(attendee.getAccount_())) {
			result = true;
		}
		return result;
	}
	int type_;
	int login_type_;
	int ops_status_;
	int adminprivilege_;
	int video_nums_;
	int voice_nums_;
	long video_state_;
	boolean forbiddenvideo_;
	int main_video_id_;

	int id_enterprise_;

	String name_;
	String mac_address_;
	String guid_;
	String json_;

	int id_version_;
	int id_subgroup_;
	int id_access_server_;
	int id_conference_;
	String account_;
	
	long signin_time_;
	
	QzVideoDeviceInfo[] video_devices_ = null;
	
	int videoDeviceType(int id_device) {
		if(video_devices_ == null)
			return QzVideoDeviceInfo.kUnknowCamera;
		
		for(int i = 0; i < video_devices_.length; i++) {
			if(video_devices_[i].id_ == id_device) 
				return video_devices_[i].type_;
		}
		return QzVideoDeviceInfo.kUnknowCamera;
	}
	
	boolean isValidVideoDevice(int id_device) {
		if(video_devices_ == null)
			return false;
		for(int i = 0; i < video_devices_.length; i++) {
			if(video_devices_[i].id_ == id_device) 
				return true;
		}
		return false;	
	}
	
	boolean isEnabledVideoDevice(int id_device) {
		if(video_devices_ == null)
			return false;
		for(int i = 0; i < video_devices_.length; i++) {
			if(video_devices_[i].id_ == id_device) 
				return video_devices_[i].enable_;
		}
		return false;			
	}
	
	String videoDeviceName(int id_device) {
		if(video_devices_ == null)
			return "";
		for(int i = 0; i < video_devices_.length; i++) {
			if(video_devices_[i].id_ == id_device) 
				return video_devices_[i].show_name_;
		}
		return "";
	}

}
