package com.lemeeting.conf.defines;

public final class QzError {
	
	public static final int ERR_OK = 0;
	
	public static final int ERR_FAILED = -2000;

	public static final int ERR_CLUSTER_GROUP_ID = -2001;

	public static final int ERR_CLUSTER_ACCESS_SERVER_ID = -2002;

	public static final int ERR_USER_NAME = -2003;

	public static final int ERR_PASSWORD = -2004;

	public static final int ERR_WAIT = -2005;

	public static final int ERR_ACCESS_SERVER_TO_CENTER_SERVER_NO_READY = -2006;

	public static final int ERR_CLUSTER_DATA_SERVER_ID = -2007;

	public static final int ERR_ENTRY_CONFERENCE_STATE = -2008;

	public static final int ERR_NET = -2009;

	public static final int ERR_CLUSTER_GROUP_INFO = -2010;

	public static final int ERR_NO_INIT = -2011;

	public static final int ERR_OPER_TIMEOUT = -2012;

	public static final int ERR_ROOM_NO_EXIST = -2013;

	public static final int ERR_VIDEO_SERVER_TO_ACCESS_SERVER_NO_READY = -2014;

	public static final int ERR_VOICE_SERVER_TO_ACCESS_SERVER_NO_READY = -2015;

	public static final int ERR_WHITEBOARD_SERVER_TO_ACCESS_SERVER_NO_READY = -2016;

	public static final int ERR_DESKTOPSHARE_SERVER_TO_ACCESS_SERVER_NO_READY = -2017;

	public static final int ERR_CONFERENCE_TAG = -2018;

	public static final int ERR_USER_HAS_EXIST = -2019;

	public static final int ERR_USER_NO_EXIST = -2020;

	public static final int ERR_ROOM_FULL = -2021;

	public static final int ERR_SPEAK_STATE = -2022;

	public static final int ERR_DATA_OP_STATE = -2023;

	public static final int ERR_SYNC_OP_STATE = -2024;

	public static final int ERR_ADMIN_OP_STATE = -2025;

	public static final int ERR_SYNCER_HAS_EXIST = -2026;

	public static final int ERR_ADMIN_PRIVILEGE = -2027;

	public static final int ERR_REG_STATE = -2028;

	public static final int ERR_SHOW_STATE = -2029;

	public static final int ERR_SPEAKER_FULL = -2030;

	public static final int ERR_ROOM_STATE = -2031;

	public static final int ERR_DESKTOPSHARE_STATE = -2032;

	public static final int ERR_EXIST_DESKTOPSHARER = -2033;

	public static final int ERR_CONF_PROTOCOL_VERSION = -2034;
	
	public static final int ERR_PLAYBACK_STATE = -2035;
	
	public static final int ERR_ACTIVE_DISCONNECT = -2036;

	public static final int ERR_DESKTOPCONTROL_STATE = -2037;

	public static final int ERR_ROOM_LOCK = -2038;



	 ////////////////////////////////////////////////////////////////////////
	 public static final int HR_ERROR_BASE	= -3000;
	 public static final int HR_SUCCESS		= 0;
	 
	 public static final int ERR_HR_FAILD = HR_ERROR_BASE + 1;
	 public static final int ERR_HR_UNKNOWN = HR_ERROR_BASE + 2;
	 public static final int ERR_HR_NOT_HANDLE = HR_ERROR_BASE + 3;
	 public static final int ERR_HR_SERVICE_TYPE = HR_ERROR_BASE + 4;
	 public static final int ERR_HR_CLIENT_TYPE = HR_ERROR_BASE + 5;
	 public static final int ERR_HR_SERVICE_VERSION = HR_ERROR_BASE + 6;
	 public static final int ERR_HR_CLIENT_VERSION = HR_ERROR_BASE + 7;
	 public static final int ERR_HR_EXIST_CONNECT = HR_ERROR_BASE + 8;
	 public static final int ERR_HR_FIND_CONNECT = HR_ERROR_BASE + 9;
	 public static final int ERR_HR_NOT_AUTH = HR_ERROR_BASE + 10;
	 public static final int ERR_HR_SESSION_INFO = HR_ERROR_BASE + 11;
	 public static final int ERR_HR_READ_AUTH = HR_ERROR_BASE + 12;
	 public static final int ERR_HR_AUTH_INVALID = HR_ERROR_BASE + 13;
	 public static final int ERR_HR_NOT_EXIST = HR_ERROR_BASE + 14;
	 public static final int ERR_HR_RELAY_MAIN = HR_ERROR_BASE + 15;
	 public static final int ERR_HR_OFFLINE = HR_ERROR_BASE + 16;
	 public static final int ERR_HR_NOT_FINISH = HR_ERROR_BASE + 17;
	 public static final int ERR_HR_SERVER_ID = HR_ERROR_BASE + 18;

	 ////////////////////////////////////////////////////////////////////////
	 public static final int DB_ERROR_BASE	= -4000;
	 public static final int DB_SUCCESS		= 0;
	 
	 public static final int ERR_DB_FAILED = DB_ERROR_BASE + 1;
	 public static final int ERR_DB_NULL_CONNECT = DB_ERROR_BASE + 2;
	 public static final int ERR_DB_DATA = DB_ERROR_BASE + 3;
	 public static final int ERR_DB_NOTHING = DB_ERROR_BASE + 4;
	 public static final int ERR_DB_NOT_CHANGED = DB_ERROR_BASE + 5;
	 public static final int ERR_DB_ALLOC_ID = DB_ERROR_BASE + 6;
	 public static final int ERR_DB_ORG_INFO = DB_ERROR_BASE + 7;
	 public static final int ERR_DB_OVER_USER_COUNT = DB_ERROR_BASE + 8;
	 public static final int ERR_DB_SYNC_FINISH = DB_ERROR_BASE + 9;
	 public static final int ERR_DB_KEY_DUPLICATE = DB_ERROR_BASE + 10;

	 public static final int BS_ERROR_BASE	= -4200;
	 public static final int ERR_BS_ORG_USER_COUNT = BS_ERROR_BASE + 1;
	 public static final int ERR_BS_ORG_ROOM_COUNT = BS_ERROR_BASE + 2;
	 public static final int ERR_BS_ROOM_USER_COUNT = BS_ERROR_BASE + 3;
	 public static final int ERR_BS_ROOM_SPEAKER_COUNT = BS_ERROR_BASE + 4;
	 public static final int ERR_BS_WORLD_ID = BS_ERROR_BASE + 5;
	 public static final int ERR_BS_NOT_PERM = BS_ERROR_BASE + 6;
	 public static final int ERR_BS_ACCOUNT = BS_ERROR_BASE + 7;
	 public static final int ERR_BS_CLIENT_TYPE = BS_ERROR_BASE + 8;
	 public static final int ERR_BS_ORG_CONF_USER_OVERFULL = BS_ERROR_BASE + 9;
	 public static final int ERR_BS_ORG_ACCOUNT = BS_ERROR_BASE + 10;
	 public static final int ERR_BS_FIND_ORG_INFO = BS_ERROR_BASE + 11;
	 public static final int ERR_BS_ACCOUNT_EMPTY = BS_ERROR_BASE + 12;
	 public static final int ERR_BS_AUTH_SERVER_ID = BS_ERROR_BASE + 13;
	 public static final int ERR_BS_AUTH_INFO = BS_ERROR_BASE + 14;
	 public static final int ERR_BS_AUTH_EXPIRE = BS_ERROR_BASE + 15;
	 public static final int ERR_BS_AUTH_FORBID = BS_ERROR_BASE + 16;
	 public static final int ERR_BS_AUTH_OVER_COUNT = BS_ERROR_BASE + 17;
	 public static final int ERR_BS_TIME = BS_ERROR_BASE + 18;
	 public static final int ERR_BS_OVER_COUNT = BS_ERROR_BASE + 19;
	 public static final int ERR_BS_NOT_VALID_BUSINESS = BS_ERROR_BASE + 20;
	 public static final int ERR_BS_INVALID_CONFID = BS_ERROR_BASE + 21;
	 public static final int ERR_BS_OVER_REG_USER_COUNT = BS_ERROR_BASE + 22;

	 
	 public static final int WEB_ERROR_BASE	= -4300;
	 public static final int ERR_WEB_XML_DATA = WEB_ERROR_BASE + 1;
	 public static final int ERR_WEB_XML_USER = WEB_ERROR_BASE + 2;
	 public static final int ERR_WEB_XML_PASSWORD = WEB_ERROR_BASE + 3;
	 public static final int ERR_WEB_TOKEN = WEB_ERROR_BASE + 4;
	 public static final int ERR_WEB_XML_CONF_ID = WEB_ERROR_BASE + 5;

	 
	 public static final int CB_ERROR_BASE	= -4500;
	 public static final int ERR_CB_SERVER_OFFLINE = CB_ERROR_BASE + 1;
	 public static final int ERR_CB_QUERY_GROUP = CB_ERROR_BASE + 2;
	 public static final int ERR_CB_QUERY_SERVER = CB_ERROR_BASE + 3;
	 public static final int ERR_CB_INIT_JSON_ADDRESS = CB_ERROR_BASE + 4;


	 public static final int ERR_HAS_EXIST_RECORD_TASK = -5000;
	 public static final int ERR_CREATE_RECORD_TASK = -5001;
	 public static final int ERR_RECORD_FILE = -5002;
	 public static final int ERR_DAMAGE_RECORD_FILE = -5003;
	 public static final int ERR_RECORD_FAIL = -5004;
	 public static final int ERR_HAS_EXIST_PLAY_TASK = -5005;
	 public static final int ERR_PLAY_STATE = -5006;
	 public static final int ERR_RECORD_FILE_NO_NEED_REPAIR = -5007;
	 public static final int ERR_HAS_EXIST_REPAIR_TASK = -5008;


	 public static final int ERR_DOWNLOAD_FILE = -5500;
	 public static final int ERR_NO_EXIST_DOWNLOAD_TASK = -5501;
	 public static final int ERR_HAS_EXIST_DOWNLOAD_TASK = -5502;
	 public static final int ERR_DOWNLOAD_FILE_START_POS_VERY_LONG = -5503;
	 public static final int ERR_DOWNLOAD_FILE_CHANNEL_NO_EXIST = -5504;
	 public static final int ERR_DOWNLOAD_FILE_CHANNEL_STATE = -5505;



}
