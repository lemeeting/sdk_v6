package com.lemeeting.conf.defines;

public class QzConferenceAttribute {
	public int getId_version_() {
		return id_version_;
	}
	public void setId_version_(int id_version_) {
		this.id_version_ = id_version_;
	}
	public int getId_conference_() {
		return id_conference_;
	}
	public void setId_conference_(int id_conference_) {
		this.id_conference_ = id_conference_;
	}
	public int getId_org_() {
		return id_org_;
	}
	public void setId_org_(int id_org_) {
		this.id_org_ = id_org_;
	}
	public int getId_cluster_group_() {
		return id_cluster_group_;
	}
	public void setId_cluster_group_(int id_cluster_group_) {
		this.id_cluster_group_ = id_cluster_group_;
	}
	public int getType_() {
		return type_;
	}
	public void setType_(int type_) {
		this.type_ = type_;
	}
	public int getMax_attendees_() {
		return max_attendees_;
	}
	public void setMax_attendees_(int max_attendees_) {
		this.max_attendees_ = max_attendees_;
	}
	public int getMax_speakers_() {
		return max_speakers_;
	}
	public void setMax_speakers_(int max_speakers_) {
		this.max_speakers_ = max_speakers_;
	}
	public int getTag_() {
		return tag_;
	}
	public void setTag_(int tag_) {
		this.tag_ = tag_;
	}
	public long getStart_time_() {
		return start_time_;
	}
	public void setStart_time_(long start_time_) {
		this.start_time_ = start_time_;
	}
	public long getEnd_time_() {
		return end_time_;
	}
	public void setEnd_time_(long end_time_) {
		this.end_time_ = end_time_;
	}
	public long getCreate_time_() {
		return create_time_;
	}
	public void setCreate_time_(long create_time_) {
		this.create_time_ = create_time_;
	}
	public long getModify_time_() {
		return modify_time_;
	}
	public void setModify_time_(long modify_time_) {
		this.modify_time_ = modify_time_;
	}
	public String getName_() {
		return name_;
	}
	public void setName_(String name_) {
		this.name_ = name_;
	}
	public String getConference_password_() {
		return conference_password_;
	}
	public void setConference_password_(String conference_password_) {
		this.conference_password_ = conference_password_;
	}
	public String getManager_password_() {
		return manager_password_;
	}
	public void setManager_password_(String manager_password_) {
		this.manager_password_ = manager_password_;
	}
	public String getCreator_() {
		return creator_;
	}
	public void setCreator_(String creator_) {
		this.creator_ = creator_;
	}
	public String getMender_() {
		return mender_;
	}
	public void setMender_(String mender_) {
		this.mender_ = mender_;
	}
	public String getDesc_() {
		return desc_;
	}
	public void setDesc_(String desc_) {
		this.desc_ = desc_;
	}
	public String getSetting_() {
		return setting_;
	}
	public void setSetting_(String setting_) {
		this.setting_ = setting_;
	}
	public String getExtend_() {
		return extend_;
	}
	public void setExtend_(String extend_) {
		this.extend_ = extend_;
	}
	
	int id_version_;
	int id_conference_;
	int id_org_;
	int id_cluster_group_;
	int type_;
	int max_attendees_;
	int max_speakers_;
	int tag_;
	long start_time_;
	long end_time_;
	long create_time_;
	long modify_time_;
	String name_;
	String conference_password_;
	String manager_password_;
	String creator_;
	String mender_;
	String desc_;
	String setting_;
	String extend_;
}
