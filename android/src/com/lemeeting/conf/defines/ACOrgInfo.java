package com.lemeeting.conf.defines;

public class ACOrgInfo {	
	public int m_uiversionid;
	public int m_uideleteflag;
	public int m_uiworldid;
	public int m_uiorgtype;
	public int m_iorgstatus;
	public int m_uigroupid;
	public int m_uimaxusercount;
	public String m_strorgname;
	public String m_strshowname;
	public String m_straddress;
	public String m_strcontacter;
	public String m_strphone;
	public String m_stremail;
	public String m_strwebsite;
	public long m_dtbirthday;
	public long m_dtcreatetime;
	public long m_dtmodifytime;
	public String m_strcreator;
	public String m_strmender;
	public int m_uiorgflag;
	public String m_strsettingjson;
	public String m_strextendjson;
}
