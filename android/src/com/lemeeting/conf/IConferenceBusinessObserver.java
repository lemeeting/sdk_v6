package com.lemeeting.conf;

import com.lemeeting.conf.defines.QzAttendee;
import com.lemeeting.conf.defines.QzConferenceAttribute;
import com.lemeeting.conf.defines.QzConferenceSyncInfo;
import com.lemeeting.conf.defines.QzRealTimeConferenceInfo;

public interface IConferenceBusinessObserver {
	void onDisconnect(int result);

	void onEntryConference(int result);

	void onLeaveConference(int result);

	void onGetConference(QzConferenceAttribute attribute);

	void onUpdateConerence(QzConferenceAttribute attribute);

	void onRemoveConerence(int id_conference);

	void onGetConferenceRealTimeInfo(QzRealTimeConferenceInfo info);

	void onGetConferenceSyncInfo(QzConferenceSyncInfo info);

	void onAddSelfAttendee(QzAttendee attendee);

	void onAttendeeOnline(QzAttendee attendee);

	void onAttendeeOffline(String account);

	void onUpdateAttendee(QzAttendee old_attendee, QzAttendee attendee);

	void onRemoveAttendee(String account);

	void onAddConfAdmin(String admin);

	void onRemoveConfAdmin(String account);

	void onAddConfDefaultAttendee(String account);

	void onRemoveConfDefaultAttendee(String account);

	void onApplySpeakOper(int result, String account, int new_state,
			int old_state, boolean apply);

	void onAccreditSpeakOper(int result, String account, int new_state,
			int old_state, boolean accredit);

	void onSpeakNotify(String account, int new_state, int old_state,
			boolean is_speak);

	void onApplyDataOper(int result, String account, int new_state,
			int old_state, boolean apply);

	void onAccreditDataOper(int result, String account, int new_state,
			int old_state, boolean accredit);

	void onDataOpNotify(String account, int new_state, int old_state,
			boolean is_data_op);

	void onApplyDataSync(int result, String account, int new_state,
			int old_state, boolean apply);

	void onAccreditDataSync(int result, String account, int new_state,
			int old_state, boolean accredit);

	void onSyncOpNotify(String account, int new_state, int old_state,
			boolean is_sync);

	void onDataSyncCommand(int result);

	void onDataSyncCommandNotify(String syncer, String sync_data);

	void onApplyTempAdmin(int result, String account, int new_state,
			int old_state, boolean apply);

	void onAccreditTempAdmin(int result, String account, int new_state,
			int old_state, boolean accredit);

	void onAuthTempAdmin(int result, String account);

	void onTempAdminNotify(String account, int new_state, int old_state,
			boolean is_admin);

	void onStartPreviewVideo(int result, String shower, int id_device, long context);

	void onStopPreviewVideo(int result, String shower, int id_device, long context);

	void onStartPreviewVideoNotify(String shower, int id_device);

	void onStopPreviewVideoNotify(String shower, int id_device);

	void onSwitchMainVideo(int result, int old_id_device, int id_device);

	void onSwitchMainVideoNotify(String account, int old_id_device, int id_device);

	void onEnableVideo(int result, boolean enabled);

	void onEnableVideoNotify(String account, boolean enabled);

	void onCaptureVideoState(int result, int id_device, boolean enabled);

	void onCaptureVideoStateNotify(String account, int id_device, boolean enabled);

	void onVideoDeviceChangedNotify(String account, int dev_nums,
			int dev_state, int id_main_device);
	
	void onVideoShowNameChangedNotify(String account, int id_device, String name);

	void onKickoutAttendee(int result, String account);

	void onKickoutAttendeeNotify(String oper_account, String account);

	void onUpdateAttendeeName(int result, String new_name);

	void onUpdateAttendeeNameNotify(String account, String new_name);

	void onRelayMsgToOne(int result, String receiver);

	void onRelayMsgToOneNotify(String sender, String receiver, String msg);

	void onRelayMsgToAll(int result);

	void onRelayMsgToAllNotify(String sender, String msg);

	void onAdminOperConfSetting(int result, int cmd, int cmd_value);

	void onAdminOperConfSettingNotify(String account, int cmd, int cmd_value);

	void onSetConfPassword(int result);

	void onSetConfPasswordNotify(String account);

	void onStartDesktopShare(int result);

	void onStartDesktopShareNotify(String account);

	void onStopDesktopShare(int result);

	void onStopDesktopShareNotify(String account);

	void onStartPreviewDesktop(int result, String sharer, long context);

	void onStopPreviewDesktop(int result, String sharer, long context);

	void onStartPreviewDesktopNotify(String sharer);

	void onStopPreviewDesktopNotify(String sharer);
	
	void onOperAllMic(int result, boolean is_up_mic);
	void onOperAllMicNotify(String oper, boolean is_up_mic);

	void onOperAllMuted(int result, boolean is_muted);
	void onOperAllMutedNotify(String oper, boolean is_muted);

	void onLaunchSignin(int result, int signin_type, long launch_or_stop_time);
	void onLaunchSigninNotify(String launcher, int signin_type, long launch_or_stop_time);

	void onSignin(int result, boolean is_signin, long launch_time, long signin_or_cancel_time);
	void onSigninNotify(String signiner, boolean is_signin, long launch_time, long signin_or_cancel_time);

	void onOperSubGroup(int result);
	void onOperSubGroupNotify(String oper, int id_subgroup);
	void onCancelSubGroup(int result);
	void onCancelSubGroupNotify(String oper);

	void onOpRemoteDesktopShared(int result, String sharer, boolean is_start);
	
	void onOpRemoteDesktopSharedNotify(String oper, String sharer, boolean is_start);
	
	void onStartPlayback(int result, String file_name);
	void onStartPlaybackNotify(String oper);
	void onStopPlayback(int result);
	void onStopPlaybackNotify(String oper);

}
