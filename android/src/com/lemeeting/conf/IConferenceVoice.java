package com.lemeeting.conf;

public interface IConferenceVoice {
	public static final int kInvalidCodec = -1;
	public static final int kOpus = 0;
	public static final int kISAC = 1;
	public static final int kSpeex = 2;
	public static final int kILBC = 3;
	public static final int kG7211 = 4;

	public static final int kNsUnchanged = 0; // previously set mode
	public static final int kNsDefault = 1; // platform default
	public static final int kNsConference = 2; // conferencing default
	public static final int kNsLowSuppression = 3; // lowest suppression
	public static final int kNsModerateSuppression = 4;
	public static final int kNsHighSuppression = 5;
	public static final int kNsVeryHighSuppression = 6;// highest suppression

	public static final int kAgcUnchanged = 0; // previously set mode
	public static final int kAgcDefault = 1; // platform default
	// adaptive mode for use when analog volume control exists (e.g. for
	// PC softphone)
	public static final int kAgcAdaptiveAnalog = 2;
	// scaling takes place in the digital domain (e.g. for conference servers
	// and embedded devices)
	public static final int kAgcAdaptiveDigital = 3;
	// can be used on embedded devices where the capture signal level
	// is predictable
	public static final int kAgcFixedDigital = 4;

	public static final int kAecmQuietEarpieceOrHeadset = 0;
	// Quiet earpiece or headset use
	public static final int kAecmEarpiece = 1; // most earpiece use
	public static final int kAecmLoudEarpiece = 2; // Loud earpiece or quiet
											// speakerphone use
	public static final int kAecmSpeakerphone = 3; // most speakerphone use (default)
	public static final int kAecmLoudSpeakerphone = 4; // Loud speakerphone

	public static final int kVadConventional = 0; // lowest reduction
	public static final int kVadAggressiveLow = 1;
	public static final int kVadAggressiveMid = 2;
	public static final int kVadAggressiveHigh = 3; // highest reduction

	public static final int kVoiceDefaultIO = 0;
	public static final int kVoiceAdaptiveIO = 1;
	public static final int kVoiceReliableIO = 2;
	
	int setCodecType(int codec_type);

	int getCodecType();

	int getNumOfInputDevices();

	int getNumOfOutputDevices();

	String getInputDeviceName(int index);

	String getOutputDeviceName(int index);

	int setInputDevice(int index);

	int setOutputDevice(int index);

	int setInputVolume(int volume);

	int getInputVolume();

	int setOutputVolume(int volume);

	int getOutputVolume();

	int getInputVoiceLevel();

	int getOutputVoiceLevel(String account);
	
	int getOutputVoiceLevelForNode(int node);

	int setLoudspeakerStatus(boolean enabled);

	int setNsStatus(boolean enabled, int mode);

	int setAgcStatus(boolean enabled, int mode);

	int setAecmMode(int mode, boolean enableCNG);

	int setVADStatus(boolean enable, int mode, boolean disableDTX);

	boolean getLoudspeakerStatus();

	int getNsStatus(); //hight=enable, low=mode, -1=erroe

	int getAgcStatus(); //hight=enable, low=mode, -1=erroe

	int getAecmMode(); //hight=enable, low=mode, -1=erroe

	int getVADStatus(); //hight=enable, mid=mode, low=disableDTX, -1=erroe

	int setDelayOffsetMs(int offset);

	int delayOffsetMs();

	int enableHighPassFilter(boolean enable);

	boolean isHighPassFilterEnabled();

	int enableInput(boolean enabled);

	int enableOutput(boolean enabled);

	boolean getInputState();

	boolean getOutputState();

	int setVoiceIOMode(int mode);
	
	int getVoiceIOMode();

}
