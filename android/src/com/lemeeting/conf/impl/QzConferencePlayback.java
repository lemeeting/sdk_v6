package com.lemeeting.conf.impl;

import java.util.HashMap;

import org.chromium.base.ObserverList;

import android.view.SurfaceView;
import android.view.ViewGroup;

import com.lemeeting.conf.IConferencePlayback;
import com.lemeeting.conf.IConferencePlaybackObserver;
import com.lemeeting.conf.defines.QzPreviewInfo;

public class QzConferencePlayback implements IConferencePlayback {	
	private final ObserverList<IConferencePlaybackObserver> observer_list_ = new ObserverList<IConferencePlaybackObserver>();
	long nativeConfPlayback_ = 0;

	QzConferencePlayback(QzConferenceCenter center) {
		
	}

	@Override
	public int addObserver(IConferencePlaybackObserver observer) {
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;	
	}

	@Override
	public int removeObserver(IConferencePlaybackObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public int startPlayback(String file_name) {
		return jnistartPlayback(file_name);
	}

	@Override
	public int pausePlayback() {
		return jnipausePlayback();
	}

	@Override
	public int continuePlayback() {
		return jnicontinuePlayback();
	}

	@Override
	public int stopPlayback() {
		return jnistopPlayback();
	}


	void onPlaybackOpen(int error) {
		for (IConferencePlaybackObserver obeserver : observer_list_)
			obeserver.onPlaybackOpen(error);				
	}

	void onPlaybackPlay(int error) {
		for (IConferencePlaybackObserver obeserver : observer_list_)
			obeserver.onPlaybackPlay(error);				
	}

	void onPlaybackPause(int error) {
		for (IConferencePlaybackObserver obeserver : observer_list_)
			obeserver.onPlaybackPause(error);				
	}

	void onPlaybackContinue(int error) {
		for (IConferencePlaybackObserver obeserver : observer_list_)
			obeserver.onPlaybackContinue(error);				
	}

	void onPlaybackComplete() {
		for (IConferencePlaybackObserver obeserver : observer_list_)
			obeserver.onPlaybackComplete();				
	}

	private native int jnistartPlayback(String file_name);

	private native int jnipausePlayback();

	private native int jnicontinuePlayback();

	private native int jnistopPlayback();

}
