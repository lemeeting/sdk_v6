package com.lemeeting.conf.impl;

import org.chromium.base.ObserverList;

import com.lemeeting.conf.IConferenceBox;
import com.lemeeting.conf.IConferenceBoxObserver;

public class QzConferenceBox implements IConferenceBox {
    private final ObserverList<IConferenceBoxObserver> observer_list_ = new ObserverList<IConferenceBoxObserver>();

	long nativeConfBox_ = 0;
	QzConferenceBox(QzConferenceCenter center) {
		
	}

	@Override
	public int addObserver(IConferenceBoxObserver observer) {		
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;
	}

	@Override
	public int removeObserver(IConferenceBoxObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public int setCharacteristicCode(String code) {
		return jnisetCharacteristicCode(code);
	}

	@Override
	public int toBoxControllerCommand(String cmd) {
		return jnitoBoxControllerCommand(cmd);
	}
	
	void onCommandFromController(String cmd) {
		for (IConferenceBoxObserver obeserver : observer_list_)
			obeserver.onCommandFromController(cmd);				
	}

	void onControllerOnline(int nums) {
		for (IConferenceBoxObserver obeserver : observer_list_)
			obeserver.onControllerOnline(nums);				
	}

	void onControllerOffline(int nums) {
	      for (IConferenceBoxObserver obs : observer_list_)
	            obs.onControllerOffline(nums);
	}

	private native int jnisetCharacteristicCode(String code);

	private native int jnitoBoxControllerCommand(String cmd);

}
