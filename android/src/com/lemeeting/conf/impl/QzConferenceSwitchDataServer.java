package com.lemeeting.conf.impl;

import org.chromium.base.ObserverList;

import com.lemeeting.conf.IConferenceSwitchDataServer;
import com.lemeeting.conf.IConferenceSwitchDataServerObserver;
import com.lemeeting.conf.defines.QzDataServerInfo;
import com.lemeeting.conf.defines.QzPingResult;

public class QzConferenceSwitchDataServer implements IConferenceSwitchDataServer {
	private final ObserverList<IConferenceSwitchDataServerObserver> observer_list_ = new ObserverList<IConferenceSwitchDataServerObserver>();
	long nativeConfSwitchServer_ = 0;

	QzConferenceSwitchDataServer(QzConferenceCenter center) {
		
	}
	
	@Override
	public int addObserver(IConferenceSwitchDataServerObserver observer) {
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;	
	}

	@Override
	public int removeObserver(IConferenceSwitchDataServerObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public QzDataServerInfo[] getClusterGroupDataServer() {
		return jnigetClusterGroupDataServer();
	}

	@Override
	public QzDataServerInfo getCurrentConnectDataServer(int module) {
		return jnigetCurrentConnectDataServer(module);
	}

	@Override
	public int pingDataServer(String address, int port, long callback_time,
			boolean is_use_udp, long context) {
		return jnipingDataServer(address, port, callback_time, is_use_udp, context);
	}

	@Override
	public int stopPingDataServer(int task_id) {
		return jnistopPingDataServer(task_id);
	}

	@Override
	public int switchDataServer(int module, String address) {
		return jniswitchDataServer(module, address);
	}

	void onPingResult(int task_id, QzPingResult result, long context, int error) {
		for (IConferenceSwitchDataServerObserver obeserver : observer_list_)
			obeserver.onPingResult(task_id, result, context, error);		
	}

	private native QzDataServerInfo[] jnigetClusterGroupDataServer();

	private native QzDataServerInfo jnigetCurrentConnectDataServer(int module);

	private native int jnipingDataServer(String address, int port, long callback_time,
			boolean is_use_udp, long context);

	private native int jnistopPingDataServer(int task_id);

	private native int jniswitchDataServer(int module, String address);
}
