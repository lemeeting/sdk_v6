package com.lemeeting.conf.impl;


import com.lemeeting.conf.IConferenceBase;
import com.lemeeting.conf.defines.QzAttendee;
import com.lemeeting.conf.defines.QzConferenceAttribute;
import com.lemeeting.conf.defines.QzProxyOptions;
import com.lemeeting.conf.defines.QzRealTimeConferenceInfo;

public class QzConferenceBase implements IConferenceBase {
	
	long nativeConfBase_ = 0;
	QzConferenceBase(QzConferenceCenter center) {
		
	}
	
	@Override
	public String getStringOfDateTime(long dt) {
		return jnigetStringOfDateTime(dt);
	}

	@Override
	public int setUseTcp(boolean is_use_tcp) {
		return jnisetUseTcp(is_use_tcp);
	}

	@Override
	public boolean isUseTcp() {
		return jniisUseTcp();
	}

	@Override
	public int setProxy(QzProxyOptions options) {
		return jnisetProxy(options);
	}

	@Override
	public QzProxyOptions getProxy() {
		return jnigetProxy();
	}


	@Override
	public QzConferenceAttribute conferenceAttribute() {
		return jniconferenceAttribute();
	}

	@Override
	public QzRealTimeConferenceInfo realtimeConferenceInfo() {
		return jnirealtimeConferenceInfo();
	}

	@Override
	public QzAttendee getAttendee(String account) {
		return jnigetAttendee(account);
	}

	@Override
	public QzAttendee getSelfAttendee() {
		return jnigetSelfAttendee();
	}
	
	private native String jnigetStringOfDateTime(long dt);
	
	private native int jnisetUseTcp(boolean is_use_tcp);

	private native boolean jniisUseTcp();

	private native int jnisetProxy(QzProxyOptions options);

	private native QzProxyOptions jnigetProxy();

	private native QzConferenceAttribute jniconferenceAttribute();

	private native QzRealTimeConferenceInfo jnirealtimeConferenceInfo();

	private native QzAttendee jnigetAttendee(String account);

	private native QzAttendee jnigetSelfAttendee();


}
