package com.lemeeting.conf.impl;


import org.chromium.base.ObserverList;

import com.lemeeting.conf.IConferenceToCenterServer;
import com.lemeeting.conf.IConferenceToCenterServerObserver;
import com.lemeeting.conf.defines.ACApplyMsg;
import com.lemeeting.conf.defines.ACConfCode;
import com.lemeeting.conf.defines.ACConfRoom;
import com.lemeeting.conf.defines.ACOrgInfo;
import com.lemeeting.conf.defines.ACOrgUser;
import com.lemeeting.conf.defines.ACPushMsg;
import com.lemeeting.conf.defines.ACUserBind;
import com.lemeeting.conf.defines.ACUserInfo;
import com.lemeeting.conf.defines.QzParamSetValue;
import com.lemeeting.conf.defines.QzRealConfInfo;

public class QzConferenceToCenterServer implements IConferenceToCenterServer {
	private final ObserverList<IConferenceToCenterServerObserver> observer_list_ = new ObserverList<IConferenceToCenterServerObserver>();
	long nativeConfToCenterServer_;

	QzConferenceToCenterServer(QzConferenceCenter center) {
		
	}
	
	@Override
	public int addObserver(IConferenceToCenterServerObserver observer) {
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;	
	}

	@Override
	public int removeObserver(IConferenceToCenterServerObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public int setCenterAddress(String address) {
		return jnisetCenterAddress(address);
	}

	@Override
	public String getCenterAddress() {
		return jnigetCenterAddress();
	}

	//�����лص�
	@Override
	public int getAuthCode(QzParamSetValue[] param) {
		return jnigetAuthCode(param);		
	}
	
	@Override
	public int regUserInfo(QzParamSetValue[] param, ACUserInfo info) {
		return jniregUserInfo(param, info);	
	}

	@Override
	public int login(QzParamSetValue[] param, int id_org, String account, String psw) {
		return jnilogin(param, id_org, account, psw);	
	}
	
	@Override
	public int logout(QzParamSetValue[] param) {
		return jnilogout(param);			
	}

	@Override
	public int prepareLoginConf(QzParamSetValue[] param, int id_conf) {
		return jniprepareLoginConf(param, id_conf);					
	}
	
	@Override
	public int getRealConf(QzParamSetValue[] param, Integer[] listConfID) {
		return jnigetRealConf(param, listConfID);							
	}

	@Override
	public int getUserInfo(QzParamSetValue[] param, String strAccount) {
		return jnigetUserInfo(param, strAccount);									
	}
	
	@Override
	public int updateUserInfo(QzParamSetValue[] param, ACUserInfo info) {
		return jniupdateUserInfo(param, info);											
	}
	
	@Override
	public int removeUserInfo(QzParamSetValue[] param, ACUserInfo info) {
		return jniremoveUserInfo(param, info);													
	}
	
	@Override
	public int getUserInfoList(QzParamSetValue[] param, ACUserInfo info) {
		return jnigetUserInfoList(param, info);															
	}

	@Override
	public int getOrgInfo(QzParamSetValue[] param, int id_org) {
		return jnigetOrgInfo(param, id_org);																	
	}
	
	@Override
	public int updateOrgInfo(QzParamSetValue[] param, ACOrgInfo info) {
		return jniupdateOrgInfo(param, info);																			
	}
	
	@Override
	public int getOrgInfoList(QzParamSetValue[] param, ACOrgInfo info) {
		return jnigetOrgInfoList(param, info);																					
	}

	@Override
	public int getUserBind(QzParamSetValue[] param, String strName) {
		return jnigetUserBind(param, strName);																							
	}
	
	@Override
	public int addUserBind(QzParamSetValue[] param, ACUserBind info) {
		return jniaddUserBind(param, info);																									
	}
	
	@Override
	public int updateUserBind(QzParamSetValue[] param, ACUserBind info) {
		return jniupdateUserBind(param, info);																											
	}
	
	@Override
	public int removeUserBind(QzParamSetValue[] param, ACUserBind info) {
		return jniremoveUserBind(param, info);																													
	}
	
	@Override
	public int getUserBindList(QzParamSetValue[] param, ACUserBind info) {
		return jnigetUserBindList(param, info);																															
	}

	@Override
	public int getOrgUser(QzParamSetValue[] param, int id_org, String strAccount) {
		return jnigetOrgUser(param, id_org, strAccount);																																	
	}
	
	@Override
	public int addOrgUser(QzParamSetValue[] param, ACOrgUser info) {
		return jniaddOrgUser(param, info);																																			
	}
	
	@Override
	public int updateOrgUser(QzParamSetValue[] param, ACOrgUser info) {
		return jniupdateOrgUser(param, info);																																					
	}
	
	@Override
	public int removeOrgUser(QzParamSetValue[] param, ACOrgUser info) {
		return jniremoveOrgUser(param, info);																																							
	}
	
	@Override
	public int getOrgUserList(QzParamSetValue[] param, ACOrgUser info) {
		return jnigetOrgUserList(param, info);																																									
	}

	//��������Ϣ����
	@Override
	public int getConfRoom(QzParamSetValue[] param, int id_conf) {
		return jnigetConfRoom(param, id_conf);																																											
	}
	
	@Override
	public int addConfRoom(QzParamSetValue[] param, ACConfRoom info) {
		return jniaddConfRoom(param, info);																																													
	}
	
	@Override
	public int updateConfRoom(QzParamSetValue[] param, ACConfRoom info) {
		return jniupdateConfRoom(param, info);
	}
	
	@Override
	public int removeConfRoom(QzParamSetValue[] param, ACConfRoom info) {
		return jniremoveConfRoom(param, info);
	}
	
	@Override
	public int getConfRoomList(QzParamSetValue[] param, ACConfRoom info) {
		return jnigetConfRoomList(param, info);
	}

	//��������֤���¼
	@Override
	public int getConfCode(QzParamSetValue[] param, String strCodeid) {
		return jnigetConfCode(param, strCodeid);
	}
	
	@Override
	public int addConfCode(QzParamSetValue[] param, ACConfCode info) {
		return jniaddConfCode(param, info);
	}
	
	@Override
	public int updateConfCode(QzParamSetValue[] param, ACConfCode info) {
		return jniupdateConfCode(param, info);
	}
	
	@Override
	public int removeConfCode(QzParamSetValue[] param, ACConfCode info) {
		return jniremoveConfCode(param, info);
	}
	
	@Override
	public int getConfCodeList(QzParamSetValue[] param, ACConfCode info) {
		return jnigetConfCodeList(param, info);
	}

	//������Ϣ���?����Ӻ��ѵȣ�
	@Override
	public int getApplyMsg(QzParamSetValue[] param, int id_req) {
		return jnigetApplyMsg(param, id_req);
	}
	
	@Override
	public int addApplyMsg(QzParamSetValue[] param, ACApplyMsg info) {
		return jniaddApplyMsg(param, info);
	}
	
	@Override
	public int updateApplyMsg(QzParamSetValue[] param, ACApplyMsg info) {
		return jniupdateApplyMsg(param, info);
	}
	
	@Override
	public int removeApplyMsg(QzParamSetValue[] param, ACApplyMsg info) {
		return jniremoveApplyMsg(param, info);
	}
	
	@Override
	public int getApplyMsgList(QzParamSetValue[] param, ACApplyMsg info) {
		return jnigetApplyMsgList(param, info);
	}

	@Override
	public int getPushMsg(QzParamSetValue[] param, int msg_id) {
		return jnigetPushMsg(param, msg_id);
	}
	
	@Override
	public int addPushMsg(QzParamSetValue[] param, ACPushMsg info) {
		return jniaddPushMsg(param, info);
	}
	
	@Override
	public int updatePushMsg(QzParamSetValue[] param, ACPushMsg info) {
		return jniupdatePushMsg(param, info);
	}
	
	@Override
	public int removePushMsg(QzParamSetValue[] param, ACPushMsg info) {
		return jniremovePushMsg(param, info);
	}
	
	@Override
	public int getPushMsgList(QzParamSetValue[] param, ACPushMsg info) {
		return jnigetPushMsgList(param, info);
	}

	void onDisconnectCenter(int result) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onDisconnectCenter(result);																					
	}

	void onGetAuthCode(int result, QzParamSetValue[] param, String strCode) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetAuthCode(result, param, strCode);																					
	}

	void onRegUserInfo(int result, QzParamSetValue[] param, ACUserInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRegUserInfo(result, param, info);																					
	}

	void onLogin(int result, QzParamSetValue[] param, ACUserInfo userInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onLogin(result, param, userInfo);																					
	}

	void onLogout(int result, QzParamSetValue[] param) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onLogout(result, param);																					
	}

	void onPrepareLoginConf(int result, QzParamSetValue[] param, int id_conf, String jsonAddress) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onPrepareLoginConf(result, param, id_conf, jsonAddress);																					
	}

	void onGetRealConf(int result, QzParamSetValue[] param, QzRealConfInfo[] mapRealConf) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetRealConf(result, param, mapRealConf);																					
	}

	void onGetUserInfo(int result, QzParamSetValue[] param, ACUserInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetUserInfo(result, param, info);																					
	}

	void onUpdateUserInfo(int result, QzParamSetValue[] param, ACUserInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateUserInfo(result, param, info);																					
	}

	void onRemoveUserInfo(int result, QzParamSetValue[] param, ACUserInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveUserInfo(result, param, info);																					
	}

	void onGetUserInfoList(int result, QzParamSetValue[] param, ACUserInfo info, ACUserInfo[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetUserInfoList(result, param, info, listInfo);																					
	}

	void onGetOrgInfo(int result, QzParamSetValue[] param, ACOrgInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetOrgInfo(result, param, info);																					
	}

	void onUpdateOrgInfo(int result, QzParamSetValue[] param, ACOrgInfo info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateOrgInfo(result, param, info);																					
	}

	void onGetOrgInfoList(int result, QzParamSetValue[] param, ACOrgInfo info, ACOrgInfo[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetOrgInfoList(result, param, info, listInfo);																					
	}

	void onGetUserBind(int result, QzParamSetValue[] param, ACUserBind info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetUserBind(result, param, info);																					
	}

	void onAddUserBind(int result, QzParamSetValue[] param, ACUserBind info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddUserBind(result, param, info);																					
	}

	void onUpdateUserBind(int result, QzParamSetValue[] param, ACUserBind info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateUserBind(result, param, info);																					
	}

	void onRemoveUserBind(int result, QzParamSetValue[] param, ACUserBind info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveUserBind(result, param, info);																					
	}

	void onGetUserBindList(int result, QzParamSetValue[] param, ACUserBind info, ACUserBind[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetUserBindList(result, param, info, listInfo);																					
	}

	void onGetOrgUser(int result, QzParamSetValue[] param, ACOrgUser info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetOrgUser(result, param, info);																					
	}

	void onAddOrgUser(int result, QzParamSetValue[] param, ACOrgUser info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddOrgUser(result, param, info);																					
	}

	void onUpdateOrgUser(int result, QzParamSetValue[] param, ACOrgUser info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateOrgUser(result, param, info);																					
	}

	void onRemoveOrgUser(int result, QzParamSetValue[] param, ACOrgUser info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveOrgUser(result, param, info);																					
	}

	void onGetOrgUserList(int result, QzParamSetValue[] param, ACOrgUser info, ACOrgUser[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetOrgUserList(result, param, info, listInfo);																					
	}

	void onGetConfRoom(int result, QzParamSetValue[] param, ACConfRoom info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetConfRoom(result, param, info);																					
	}

	void onAddConfRoom(int result, QzParamSetValue[] param, ACConfRoom info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddConfRoom(result, param, info);																					
	}

	void onUpdateConfRoom(int result, QzParamSetValue[] param, ACConfRoom info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateConfRoom(result, param, info);																					
	}

	void onRemoveConfRoom(int result, QzParamSetValue[] param, ACConfRoom info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveConfRoom(result, param, info);																					
	}

	void onGetConfRoomList(int result, QzParamSetValue[] param, ACConfRoom info, ACConfRoom[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetConfRoomList(result, param, info, listInfo);																					
	}

	void onGetConfCode(int result, QzParamSetValue[] param, ACConfCode info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetConfCode(result, param, info);																					
	}

	void onAddConfCode(int result, QzParamSetValue[] param, ACConfCode info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddConfCode(result, param, info);																					
	}

	void onUpdateConfCode(int result, QzParamSetValue[] param, ACConfCode info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateConfCode(result, param, info);																					
	}

	void onRemoveConfCode(int result, QzParamSetValue[] param, ACConfCode info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveConfCode(result, param, info);																					
	}

	void onGetConfCodeList(int result, QzParamSetValue[] param, ACConfCode info, ACConfCode[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetConfCodeList(result, param, info, listInfo);																					
	}

	void onGetApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetApplyMsg(result, param, info);																					
	}

	void onAddApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddApplyMsg(result, param, info);																					
	}

	void onUpdateApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdateApplyMsg(result, param, info);																					
	}

	void onRemoveApplyMsg(int result, QzParamSetValue[] param, ACApplyMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemoveApplyMsg(result, param, info);																					
	}

	void onGetApplyMsgList(int result, QzParamSetValue[] param, ACApplyMsg info, ACApplyMsg[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetApplyMsgList(result, param, info, listInfo);																					
	}
	
	void onGetPushMsg(int result, QzParamSetValue[] param, ACPushMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetPushMsg(result, param, info);																					
	}

	void onAddPushMsg(int result, QzParamSetValue[] param, ACPushMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onAddPushMsg(result, param, info);																					
	}

	void onUpdatePushMsg(int result, QzParamSetValue[] param, ACPushMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onUpdatePushMsg(result, param, info);																							
	}

	void onRemovePushMsg(int result, QzParamSetValue[] param, ACPushMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onRemovePushMsg(result, param, info);																							
	}

	void onGetPushMsgList(int result, QzParamSetValue[] param, ACPushMsg info, ACPushMsg[] listInfo) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onGetPushMsgList(result, param, info, listInfo);																					
	}

	void onNoticePushMsg(QzParamSetValue[] param, ACPushMsg info) {
		for (IConferenceToCenterServerObserver obeserver : observer_list_)
			obeserver.onNoticePushMsg(param, info);																							
	}

	private native int jnisetCenterAddress(String address);
	private native String jnigetCenterAddress();
	
	private native int jnigetAuthCode(QzParamSetValue[] param);
	private native int jniregUserInfo(QzParamSetValue[] param, ACUserInfo info);

	private native int jnilogin(QzParamSetValue[] param, int id_org, String account, String psw);
	private native int jnilogout(QzParamSetValue[] param);

	private native int jniprepareLoginConf(QzParamSetValue[] param, int id_conf);
	private native int jnigetRealConf(QzParamSetValue[] param, Integer[] listConfID);

	private native int jnigetUserInfo(QzParamSetValue[] param, String strAccount);
	private native int jniupdateUserInfo(QzParamSetValue[] param, ACUserInfo info);
	private native int jniremoveUserInfo(QzParamSetValue[] param, ACUserInfo info);
	private native int jnigetUserInfoList(QzParamSetValue[] param, ACUserInfo info);

	private native int jnigetOrgInfo(QzParamSetValue[] param, int id_org);
	private native int jniupdateOrgInfo(QzParamSetValue[] param, ACOrgInfo info);
	private native int jnigetOrgInfoList(QzParamSetValue[] param, ACOrgInfo info);

	private native int jnigetUserBind(QzParamSetValue[] param, String strName);
	private native int jniaddUserBind(QzParamSetValue[] param, ACUserBind info);
	private native int jniupdateUserBind(QzParamSetValue[] param, ACUserBind info);
	private native int jniremoveUserBind(QzParamSetValue[] param, ACUserBind info);
	private native int jnigetUserBindList(QzParamSetValue[] param, ACUserBind info);

	private native int jnigetOrgUser(QzParamSetValue[] param, int id_org, String strAccount);
	private native int jniaddOrgUser(QzParamSetValue[] param, ACOrgUser info);
	private native int jniupdateOrgUser(QzParamSetValue[] param, ACOrgUser info);
	private native int jniremoveOrgUser(QzParamSetValue[] param, ACOrgUser info);
	private native int jnigetOrgUserList(QzParamSetValue[] param, ACOrgUser info);

	private native int jnigetConfRoom(QzParamSetValue[] param, int id_conf);
	private native int jniaddConfRoom(QzParamSetValue[] param, ACConfRoom info);
	private native int jniupdateConfRoom(QzParamSetValue[] param, ACConfRoom info);
	private native int jniremoveConfRoom(QzParamSetValue[] param, ACConfRoom info);
	private native int jnigetConfRoomList(QzParamSetValue[] param, ACConfRoom info);

	private native int jnigetConfCode(QzParamSetValue[] param, String strCodeid);
	private native int jniaddConfCode(QzParamSetValue[] param, ACConfCode info);
	private native int jniupdateConfCode(QzParamSetValue[] param, ACConfCode info);
	private native int jniremoveConfCode(QzParamSetValue[] param, ACConfCode info);
	private native int jnigetConfCodeList(QzParamSetValue[] param, ACConfCode info);

	private native int jnigetApplyMsg(QzParamSetValue[] param, int id_req);
	private native int jniaddApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	private native int jniupdateApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	private native int jniremoveApplyMsg(QzParamSetValue[] param, ACApplyMsg info);
	private native int jnigetApplyMsgList(QzParamSetValue[] param, ACApplyMsg info);
	
	private native int jnigetPushMsg(QzParamSetValue[] param, int id_msg);
	private native int jniaddPushMsg(QzParamSetValue[] param, ACPushMsg info);
	private native int jniupdatePushMsg(QzParamSetValue[] param, ACPushMsg info);
	private native int jniremovePushMsg(QzParamSetValue[] param, ACPushMsg info);
	private native int jnigetPushMsgList(QzParamSetValue[] param, ACPushMsg info);


}
