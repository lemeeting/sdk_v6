package com.lemeeting.conf.impl;

import com.lemeeting.conf.IConferenceVoice;

public class QzConferenceVoice implements IConferenceVoice {

	long nativeConfVoice_ = 0;
	
	QzConferenceVoice(QzConferenceCenter center) {
		
	}
	
	@Override
	public int setCodecType(int codec_type) {
		return jnisetCodecType(codec_type);
	}

	@Override
	public int getCodecType() {
		return jnigetCodecType();
	}

	@Override
	public int getNumOfInputDevices() {
		return jnigetNumOfInputDevices();
	}

	@Override
	public int getNumOfOutputDevices() {
		return jnigetNumOfOutputDevices();
	}

	@Override
	public String getInputDeviceName(int index) {
		return jnigetInputDeviceName(index);
	}

	@Override
	public String getOutputDeviceName(int index) {
		return jnigetOutputDeviceName(index);
	}

	@Override
	public int setInputDevice(int index) {
		return jnisetInputDevice(index);
	}

	@Override
	public int setOutputDevice(int index) {
		return jnisetOutputDevice(index);
	}

	@Override
	public int setInputVolume(int volume) {
		return jnisetInputVolume(volume);
	}

	@Override
	public int getInputVolume() {
		return jnigetInputVolume();
	}

	@Override
	public int setOutputVolume(int volume) {
		return jnisetOutputVolume(volume);
	}

	@Override
	public int getOutputVolume() {
		return jnigetOutputVolume();
	}

	@Override
	public int getInputVoiceLevel() {
		return jnigetInputVoiceLevel();
	}

	@Override
	public int getOutputVoiceLevel(String account) {
		return jnigetOutputVoiceLevel(account);
	}

	@Override
	public int getOutputVoiceLevelForNode(int node) {
		return jnigetOutputVoiceLevelForNode(node);		
	}

	@Override
	public int setLoudspeakerStatus(boolean enabled) {
		return jnisetLoudspeakerStatus(enabled);
	}

	@Override
	public int setNsStatus(boolean enabled, int mode) {
		return jnisetNsStatus(enabled, mode);
	}

	@Override
	public int setAgcStatus(boolean enabled, int mode) {
		return jnisetAgcStatus(enabled, mode);
	}

	@Override
	public int setAecmMode(int mode, boolean enableCNG) {
		return jnisetAecmMode(mode, enableCNG);
	}

	@Override
	public int setVADStatus(boolean enable, int mode, boolean disableDTX) {
		return jnisetVADStatus(enable, mode, disableDTX);
	}

	@Override
	public boolean getLoudspeakerStatus() {
		return jnigetLoudspeakerStatus();
	}

	@Override
	public int getNsStatus() {
		return jnigetNsStatus();
	}

	@Override
	public int getAgcStatus() {
		return jnigetAgcStatus();
	}

	@Override
	public int getAecmMode() {
		return jnigetAecmMode();
	}

	@Override
	public int getVADStatus() {
		return jnigetVADStatus();
	}

	@Override
	public int setDelayOffsetMs(int offset) {
		return jnisetDelayOffsetMs(offset);
	}

	@Override
	public int delayOffsetMs() {
		return jnidelayOffsetMs();
	}

	@Override
	public int enableHighPassFilter(boolean enable) {
		return jnienableHighPassFilter(enable);
	}

	@Override
	public boolean isHighPassFilterEnabled() {
		return jniisHighPassFilterEnabled();
	}

	@Override
	public int enableInput(boolean enabled) {
		return jnienableInput(enabled);
	}

	@Override
	public int enableOutput(boolean enabled) {
		return jnienableOutput(enabled);
	}

	@Override
	public boolean getInputState() {
		return jnigetInputState();
	}

	@Override
	public boolean getOutputState() {
		return jnigetOutputState();
	}

	@Override
	public int setVoiceIOMode(int mode) {
		return jnisetVoiceIOMode(mode);
	}
	
	@Override
	public int getVoiceIOMode() {
		return jnigetVoiceIOMode();	
	}

	private native int jnisetCodecType(int codec_type);

	private native int jnigetCodecType();

	private native int jnigetNumOfInputDevices();

	private native int jnigetNumOfOutputDevices();

	private native String jnigetInputDeviceName(int index);

	private native String jnigetOutputDeviceName(int index);

	private native int jnisetInputDevice(int index);

	private native int jnisetOutputDevice(int index);

	private native int jnisetInputVolume(int volume);

	private native int jnigetInputVolume();

	private native int jnisetOutputVolume(int volume);

	private native int jnigetOutputVolume();

	private native int jnigetInputVoiceLevel();

	private native int jnigetOutputVoiceLevel(String account);
	
	private native int jnigetOutputVoiceLevelForNode(int node);

	private native int jnisetLoudspeakerStatus(boolean enabled);

	private native int jnisetNsStatus(boolean enabled, int mode);

	private native int jnisetAgcStatus(boolean enabled, int mode);

	private native int jnisetAecmMode(int mode, boolean enableCNG);

	private native int jnisetVADStatus(boolean enable, int mode, boolean disableDTX);

	private native boolean jnigetLoudspeakerStatus();

	private native int jnigetNsStatus();

	private native int jnigetAgcStatus();

	private native int jnigetAecmMode();

	private native int jnigetVADStatus();

	private native int jnisetDelayOffsetMs(int offset);

	private native int jnidelayOffsetMs();

	private native int jnienableHighPassFilter(boolean enable);

	private native boolean jniisHighPassFilterEnabled();

	private native int jnienableInput(boolean enabled);

	private native int jnienableOutput(boolean enabled);

	private native boolean jnigetInputState();

	private native boolean jnigetOutputState();
	
	private native int jnisetVoiceIOMode(int mode);
	
	private native int jnigetVoiceIOMode() ;

}
