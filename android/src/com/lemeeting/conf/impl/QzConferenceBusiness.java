package com.lemeeting.conf.impl;


import org.chromium.base.ObserverList;

import com.lemeeting.conf.IConferenceBusiness;
import com.lemeeting.conf.IConferenceBusinessObserver;
import com.lemeeting.conf.defines.QzAttendee;
import com.lemeeting.conf.defines.QzConferenceAttribute;
import com.lemeeting.conf.defines.QzConferenceSyncInfo;
import com.lemeeting.conf.defines.QzRealTimeConferenceInfo;

public class QzConferenceBusiness implements IConferenceBusiness {
	private final ObserverList<IConferenceBusinessObserver> observer_list_ = new ObserverList<IConferenceBusinessObserver>();
	long nativeConfBusiness_ = 0;
	final QzConferenceCenter center_;

	QzConferenceBusiness(QzConferenceCenter center) {
		center_ = center;
	}
	
	@Override
	public int addObserver(IConferenceBusinessObserver observer) {
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;
	}

	@Override
	public int removeObserver(IConferenceBusinessObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public int entryConference(String json_address, int id_conference,
			int conf_tag, String conf_psw, boolean is_md5, String account,
			int id_org, String name) {
		return jnientryConference(json_address, id_conference, conf_tag,
				conf_psw, is_md5, account, id_org, name);
	}

	@Override
	public int leaveConference() {
		return jnileaveConference();
	}

	@Override
	public int applySpeak(boolean apply) {
		return jniapplySpeak(apply);
	}

	@Override
	public int accreditSpeak(String account, boolean accredit) {
		return jniaccreditSpeak(account, accredit);
	}

	@Override
	public int applyDataOper(boolean apply) {
		return jniapplyDataOper(apply);
	}

	@Override
	public int accreditDataOper(String account, boolean accredit) {
		return jniaccreditDataOper(account, accredit);
	}

	@Override
	public int applyDataSync(boolean apply) {
		return jniapplyDataSync(apply);
	}

	@Override
	public int accreditDataSync(String account, boolean accredit) {
		return jniaccreditDataSync(account, accredit);
	}

	@Override
	public int dataSyncCommand(String command) {
		return jnidataSyncCommand(command);
	}

	@Override
	public int applyTempAdmin(boolean apply) {
		return jniapplyTempAdmin(apply);
	}

	@Override
	public int accreditTempAdmin(String account, boolean accredit) {
		return jniaccreditTempAdmin(account, accredit);
	}

	@Override
	public int authTempAdmin(String admin_psw) {
		return jniauthTempAdmin(admin_psw);
	}

	@Override
	public int startPreviewVideo(String shower, int id_device, long context) {
		return jnistartPreviewVideo(shower, id_device, context);
	}

	@Override
	public int stopPreviewVideo(String shower, int id_device, long context) {
		return jnistopPreviewVideo(shower, id_device, context);
	}

	@Override
	public int enableVideo(boolean enabled) {
		return jnienableVideo(enabled);
	}

	@Override
	public int captureVideoState(int id_device, boolean enabled) {
		return jnicaptureVideoState(id_device, enabled);
	}

	
	@Override
	public int UpdateVideoShowName(int id_device, String name) {
		return jniupdateVideoShowName(id_device, name);
	}

	@Override
	public int kickOutAttendee(String account) {
		return jnikickOutAttendee(account);
	}

	@Override
	public int updataAttendeeName(String name) {
		return jniupdataAttendeeName(name);
	}

	@Override
	public int relayMsgToOne(String receiver, String msg) {
		return jnirelayMsgToOne(receiver, msg);
	}

	@Override
	public int relayMsgToAll(String msg) {
		return jnirelayMsgToAll(msg);
	}

	@Override
	public int adminOperConfSetting(int cmd, int cmd_value) {
		return jniadminOperConfSetting(cmd, cmd_value);
	}

	@Override
	public int setConfPassword(String psw) {
		return jnisetConfPassword(psw);
	}

	@Override
	public int startDesktopShare() {
		return jnistartDesktopShare();
	}

	@Override
	public int stopDesktopShare() {
		return jnistopDesktopShare();
	}

	@Override
	public int startPreviewDesktop(String sharer, long context) {
		return jnistartPreviewDesktop(sharer, context);
	}

	@Override
	public int stopPreviewDesktop(String sharer, long context) {
		return jnistopPreviewDesktop(sharer, context);
	}

	@Override
	public int launchSignin(int signin_type) {
		return jnilaunchSignin(signin_type);
	}
	
	@Override
	public int signin(boolean is_signin) {
		return jnisignin(is_signin);
	}

	@Override
	public int operSubGroup(String[] account_vector, int id_subgroup) {
		return jnioperSubGroup(account_vector, id_subgroup);
	}
	
	@Override
	public int cancelSubGroup() {
		return jnicancelSubGroup();		
	}
	
	@Override
	public int opRemoteDesktopShared(String sharer, boolean is_start) {
		return jniopRemoteDesktopShared(sharer, is_start);		
	}

	@Override
	public int startPlayback(String file_name) {
		return jnistartPlayback(file_name);				
	}
	
	@Override
	public int stopPlayback() {
		return jnistopPlayback();				
	}

	void onDisconnect(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onDisconnect(result);																					
	}
	
	void onEntryConference(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onEntryConference(result);																					
	}

	void onLeaveConference(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onLeaveConference(result);																					
	}

	void onGetConference(QzConferenceAttribute attribute) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onGetConference(attribute);																					
	}

	void onUpdateConerence(QzConferenceAttribute attribute) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onUpdateConerence(attribute);																					
	}

	void onRemoveConerence(int id_conference) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRemoveConerence(id_conference);																					
	}

	void onGetConferenceRealTimeInfo(QzRealTimeConferenceInfo info) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onGetConferenceRealTimeInfo(info);																					
	}

	void onGetConferenceSyncInfo(QzConferenceSyncInfo info) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onGetConferenceSyncInfo(info);																					
	}

	void onAddSelfAttendee(QzAttendee attendee) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAddSelfAttendee(attendee);																					
	}

	void onAttendeeOnline(QzAttendee attendee) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAttendeeOnline(attendee);																					
	}

	void onAttendeeOffline(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAttendeeOffline(account);																					
	}

	void onUpdateAttendee(QzAttendee old_attendee, QzAttendee attendee) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onUpdateAttendee(old_attendee, attendee);																					
	}

	void onRemoveAttendee(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRemoveAttendee(account);																					
	}

	void onAddConfAdmin(String admin) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAddConfAdmin(admin);																					
	}

	void onRemoveConfAdmin(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRemoveConfAdmin(account);																					
	}

	void onAddConfDefaultAttendee(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAddConfDefaultAttendee(account);																					
	}

	void onRemoveConfDefaultAttendee(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRemoveConfDefaultAttendee(account);																					
	}

	void onApplySpeakOper(int result, String account, int new_state,
			int old_state, boolean apply) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onApplySpeakOper(result, account, new_state, old_state, apply);																					
	}

	void onAccreditSpeakOper(int result, String account, int new_state,
			int old_state, boolean accredit) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAccreditSpeakOper(result, account, new_state, old_state, accredit);																					
	}

	void onSpeakNotify(String account, int new_state, int old_state,
			boolean is_speak) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSpeakNotify(account, new_state, old_state, is_speak);																					
	}

	void onApplyDataOper(int result, String account, int new_state,
			int old_state, boolean apply) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onApplyDataOper(result, account, new_state, old_state, apply);																					
	}

	void onAccreditDataOper(int result, String account, int new_state,
			int old_state, boolean accredit) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAccreditDataOper(result, account, new_state, old_state, accredit);																					
	}

	void onDataOpNotify(String account, int new_state, int old_state,
			boolean is_data_op) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onDataOpNotify(account, new_state, old_state, is_data_op);																					
	}

	void onApplyDataSync(int result, String account, int new_state,
			int old_state, boolean apply) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onApplyDataSync(result, account, new_state, old_state, apply);																					
	}

	void onAccreditDataSync(int result, String account, int new_state,
			int old_state, boolean accredit) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAccreditDataSync(result, account, new_state, old_state, accredit);																					
	}

	void onSyncOpNotify(String account, int new_state, int old_state,
			boolean is_sync) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSyncOpNotify(account, new_state, old_state, is_sync);																					
	}

	void onDataSyncCommand(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onDataSyncCommand(result);																					
	}

	void onDataSyncCommandNotify(String syncer, String sync_data) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onDataSyncCommandNotify(syncer, sync_data);																					
	}

	void onApplyTempAdmin(int result, String account, int new_state,
			int old_state, boolean apply) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onApplyTempAdmin(result, account, new_state, old_state, apply);																					
	}

	void onAccreditTempAdmin(int result, String account, int new_state,
			int old_state, boolean accredit) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAccreditTempAdmin(result, account, new_state, old_state, accredit);																					
	}

	void onAuthTempAdmin(int result, String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAuthTempAdmin(result, account);																					
	}

	void onTempAdminNotify(String account, int new_state, int old_state,
			boolean is_admin) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onTempAdminNotify(account, new_state, old_state, is_admin);																					
	}

	void onStartPreviewVideo(int result, String shower, int id_device, long context) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPreviewVideo(result, shower, id_device, context);																					
	}

	void onStopPreviewVideo(int result, String shower, int id_device, long context) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPreviewVideo(result, shower, id_device, context);																					
	}

	void onStartPreviewVideoNotify(String shower, int id_device) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPreviewVideoNotify(shower, id_device);																					
	}

	void onStopPreviewVideoNotify(String shower, int id_device) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPreviewVideoNotify(shower, id_device);																					
	}

	void onSwitchMainVideo(int result, int old_index, int id_device) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSwitchMainVideo(result, old_index, id_device);																			
	}

	void onSwitchMainVideoNotify(String account, int old_id_device, int id_device) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSwitchMainVideoNotify(account, old_id_device, id_device);																			
	}

	void onEnableVideo(int result, boolean enabled) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onEnableVideo(result, enabled);																			
	}

	void onEnableVideoNotify(String account, boolean enabled) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onEnableVideoNotify(account, enabled);																		
	}

	void onCaptureVideoState(int result, int id_device, boolean enabled) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onCaptureVideoState(result, id_device, enabled);																		
	}

	void onCaptureVideoStateNotify(String account, int id_device, boolean enabled) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onCaptureVideoStateNotify(account, id_device, enabled);																
	}

	void onVideoDeviceChangedNotify(String account, int dev_nums,
			int dev_state, int id_main_device) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onVideoDeviceChangedNotify(account, dev_nums, dev_state, id_main_device);																
	}
	
	void onVideoShowNameChangedNotify(String account, int id_device, String name) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onVideoShowNameChangedNotify(account, id_device, name);																
	}

	void onKickoutAttendee(int result, String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onKickoutAttendee(result, account);												
	}

	void onKickoutAttendeeNotify(String oper_account, String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onKickoutAttendeeNotify(oper_account, account);												
	}

	void onUpdateAttendeeName(int result, String new_name) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onUpdateAttendeeName(result, new_name);												
	}

	void onUpdateAttendeeNameNotify(String account, String new_name) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onUpdateAttendeeNameNotify(account, new_name);										
	}

	void onRelayMsgToOne(int result, String receiver) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRelayMsgToOne(result, receiver);										
	}

	void onRelayMsgToOneNotify(String sender, String receiver, String msg) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRelayMsgToOneNotify(sender, receiver, msg);										
	}

	void onRelayMsgToAll(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRelayMsgToAll(result);										
	}

	void onRelayMsgToAllNotify(String sender, String msg) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onRelayMsgToAllNotify(sender, msg);										
	}

	void onAdminOperConfSetting(int result, int cmd, int cmd_value) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAdminOperConfSetting(result, cmd, cmd_value);								
	}

	void onAdminOperConfSettingNotify(String account, int cmd, int cmd_value) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onAdminOperConfSettingNotify(account, cmd, cmd_value);								
	}

	void onSetConfPassword(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSetConfPassword(result);								
	}

	void onSetConfPasswordNotify(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSetConfPasswordNotify(account);								
	}

	void onStartDesktopShare(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartDesktopShare(result);								
	}

	void onStartDesktopShareNotify(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartDesktopShareNotify(account);						
	}

	void onStopDesktopShare(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopDesktopShare(result);						
	}

	void onStopDesktopShareNotify(String account) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopDesktopShareNotify(account);						
	}

	void onStartPreviewDesktop(int result, String sharer, long context) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPreviewDesktop(result, sharer, context);				
	}

	void onStopPreviewDesktop(int result, String sharer, long context) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPreviewDesktop(result, sharer, context);		
	}

	void onStartPreviewDesktopNotify(String sharer) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPreviewDesktopNotify(sharer);
	}

	void onStopPreviewDesktopNotify(String sharer) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPreviewDesktopNotify(sharer);		
	}
	
	void onLaunchSignin(int result, int signin_type, long launch_or_stop_time) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onLaunchSignin(result, signin_type, launch_or_stop_time);		
	}
	
	void onLaunchSigninNotify(String launcher, int signin_type, long launch_or_stop_time) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onLaunchSigninNotify(launcher, signin_type, launch_or_stop_time);		
	}

	void onSignin(int result, boolean is_signin, long launch_time, long signin_or_cancel_time) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSignin(result, is_signin, launch_time, launch_time);		
	}
	
	void onSigninNotify(String signiner, boolean is_signin, long launch_time, long signin_or_cancel_time) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onSigninNotify(signiner, is_signin, launch_time, signin_or_cancel_time);		
	}
	
	void onOperSubGroup(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onOperSubGroup(result);				
	}
	
	void onOperSubGroupNotify(String oper, int id_subgroup) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onOperSubGroupNotify(oper, id_subgroup);				
	}
	
	void onCancelSubGroup(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onCancelSubGroup(result);				
	}
	
	void onCancelSubGroupNotify(String oper) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onCancelSubGroupNotify(oper);						
	}

	void onOpRemoteDesktopShared(int result, String sharer, boolean is_start) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onOpRemoteDesktopShared(result, sharer, is_start);							
	}
	
	void onOpRemoteDesktopSharedNotify(String oper, String sharer, boolean is_start) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onOpRemoteDesktopSharedNotify(oper, sharer, is_start);								
	}

	void onStartPlayback(int result, String file_name) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPlayback(result, file_name);										
	}
	
	void onStartPlaybackNotify(String oper) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStartPlaybackNotify(oper);											
	}
	
	void onStopPlayback(int result) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPlayback(result);												
	}
	
	void onStopPlaybackNotify(String oper) {
		for (IConferenceBusinessObserver obeserver : observer_list_)
			obeserver.onStopPlaybackNotify(oper);												
	}

	private native int jnientryConference(String json_address,
			int id_conference, int conf_tag, String conf_psw, boolean is_md5,
			String account, int id_org, String name);

	private native int jnileaveConference();

	private native int jniapplySpeak(boolean apply);

	private native int jniaccreditSpeak(String account, boolean accredit);

	private native int jniapplyDataOper(boolean apply);

	private native int jniaccreditDataOper(String account, boolean accredit);

	private native int jniapplyDataSync(boolean apply);

	private native int jniaccreditDataSync(String account, boolean accredit);

	private native int jnidataSyncCommand(String command);

	private native int jniapplyTempAdmin(boolean apply);

	private native int jniaccreditTempAdmin(String account, boolean accredit);

	private native int jniauthTempAdmin(String admin_psw);

	private native int jnistartPreviewVideo(String shower, int id_device,
			long context);

	private native int jnistopPreviewVideo(String shower, int id_device,
			long context);
	
	private native int jnienableVideo(boolean enabled);

	private native int jnicaptureVideoState(int id_device, boolean enabled);
	
	private native int jniupdateVideoShowName(int id_device, String name);

	private native int jnikickOutAttendee(String account);

	private native int jniupdataAttendeeName(String name);

	private native int jnirelayMsgToOne(String receiver, String msg);

	private native int jnirelayMsgToAll(String msg);

	private native int jniadminOperConfSetting(int cmd, int cmd_value);

	private native int jnisetConfPassword(String psw);

	private native int jnistartDesktopShare();

	private native int jnistopDesktopShare();

	private native int jnistartPreviewDesktop(String sharer, long context);

	private native int jnistopPreviewDesktop(String sharer, long context);

	private native int jnilaunchSignin(int signin_type);
	private native int jnisignin(boolean is_signin);

	private native int jnioperSubGroup(String[] account_vector, int id_subgroup);
	private native int jnicancelSubGroup();
	
	private native int jniopRemoteDesktopShared(String sharer, boolean is_start);
	
	private native int jnistartPlayback(String file_name);	
	private native int jnistopPlayback();
}
