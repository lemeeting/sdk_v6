package com.lemeeting.conf.impl;

import android.content.Context;

public class NativeContextRegistry {
	static public native void register(Context context);
	static public native void unRegister();

}
