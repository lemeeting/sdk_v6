package com.lemeeting.conf.impl;


import org.chromium.base.ObserverList;

import com.lemeeting.conf.IConferenceRecord;
import com.lemeeting.conf.IConferenceRecordObserver;

public class QzConferenceRecord implements IConferenceRecord {
	private final ObserverList<IConferenceRecordObserver> observer_list_ = new ObserverList<IConferenceRecordObserver>();
	long nativeConfRecord_ = 0;

	QzConferenceRecord(QzConferenceCenter center) {
		
	}
	
	@Override
	public int addObserver(IConferenceRecordObserver observer) {
		if (observer_list_.hasObserver(observer))
			return -1;
		observer_list_.addObserver(observer);
		return 0;	
	}

	@Override
	public int removeObserver(IConferenceRecordObserver observer) {
		observer_list_.removeObserver(observer);
		return 0;
	}

	@Override
	public int startLocalRecord(String file_name, int flag) {
		return jnistartLocalRecord(file_name, flag);
	}

	@Override
	public int stopLocalRecord() {
		return jnistopLocalRecord();
	}
	
	void onStartLocalRecordReady() {
		for (IConferenceRecordObserver obeserver : observer_list_)
			obeserver.onStartLocalRecordReady();		
	}

	
	private native int jnistartLocalRecord(String file_name, int flag);

	private native int jnistopLocalRecord();


}
