package com.lemeeting.conf.impl;

import org.chromium.base.library_loader.LibraryLoader;
import org.chromium.base.library_loader.NativeLibraries;
import org.chromium.base.library_loader.ProcessInitException;

import android.content.Context;

import com.lemeeting.conf.IConfenerceCenterFactory;
import com.lemeeting.conf.IConferenceBase;
import com.lemeeting.conf.IConferenceBox;
import com.lemeeting.conf.IConferenceBusiness;
import com.lemeeting.conf.IConferenceCenter;
import com.lemeeting.conf.IConferencePlayback;
import com.lemeeting.conf.IConferenceRecord;
import com.lemeeting.conf.IConferenceShareDesktop;
import com.lemeeting.conf.IConferenceSwitchDataServer;
import com.lemeeting.conf.IConferenceToCenterServer;
import com.lemeeting.conf.IConferenceVideo;
import com.lemeeting.conf.IConferenceVoice;
import com.lemeeting.conf.IConferenceWhiteboard;

public class QzConferenceCenter implements IConferenceCenter {

	public static IConfenerceCenterFactory factory = new IConfenerceCenterFactory() {
		public IConferenceCenter getConfCenter() {
			return new QzConferenceCenter();
		}
	};

	final QzConferenceToCenterServer to_center_server_manager_;
	final QzConferenceBase base_manager_;
	final QzConferenceVoice voice_manager_;
	final QzConferenceVideo video_manager_;
	final QzConferenceBusiness business_manager_;
	final QzConferenceShareDesktop sd_manager_;
	final QzConferenceWhiteboard wb_manager_;
	final QzConferenceRecord record_manager_;
	final QzConferencePlayback playback_manager_;
	final QzConferenceSwitchDataServer switch_manager_;
	final QzConferenceBox box_manager_;

	long nativeConfCnter_ = 0;
	
	static Context context_;

	public static void ensureInitialized(Context context) {
		context_ = context;
		
		NativeLibraries.LIBRARIES = new String[1];
		NativeLibraries.LIBRARIES[0] = "confcenter";
		
		try {
			LibraryLoader.ensureInitialized();
		} catch (ProcessInitException e) {
			e.printStackTrace();
		}

		NativeContextRegistry.register(context);
	}
	
	public static void ensureUninitialized() {
		NativeContextRegistry.unRegister();
	}
	
	public static Context getContext() {
		return context_;
	}

	QzConferenceCenter() {
		to_center_server_manager_ = new QzConferenceToCenterServer(this);
		base_manager_ = new QzConferenceBase(this);
		voice_manager_ = new QzConferenceVoice(this);
		video_manager_ = new QzConferenceVideo(this);
		business_manager_ = new QzConferenceBusiness(this);
		sd_manager_ = new QzConferenceShareDesktop(this);
		wb_manager_ = new QzConferenceWhiteboard(this);
		record_manager_ = new QzConferenceRecord(this);
		playback_manager_ = new QzConferencePlayback(this);
		switch_manager_ = new QzConferenceSwitchDataServer(this);
		box_manager_ = new QzConferenceBox(this);
	}


	@Override
	public boolean start() {
		if (nativeConfCnter_ != 0)
			return false;

		nativeConfCnter_ = jnicreate();
		if (jnistart()) {
			return true;
		} else {
			jnidispose();
			nativeConfCnter_ = 0;
			return false;
		}
	}

	@Override
	public void stop() {
		if (nativeConfCnter_ == 0)
			return;
		jnistop();
		jnidispose();
		nativeConfCnter_ = 0;
	}

	@Override
	public IConferenceBase confBase() {
		return base_manager_;
	}

	@Override
	public IConferenceBusiness confBusiness() {
		return business_manager_;
	}

	@Override
	public IConferenceRecord confRecord() {
		return record_manager_;
	}

	@Override
	public IConferencePlayback confPlayback() {
		return playback_manager_;
	}
	
	@Override
	public IConferenceShareDesktop confShareDesktop() {
		return sd_manager_;
	}

	@Override
	public IConferenceSwitchDataServer confSwitchDataServer() {
		return switch_manager_;
	}

	@Override
	public IConferenceToCenterServer confToCenterServer() {
		return to_center_server_manager_;
	}

	@Override
	public IConferenceVideo confVideo() {
		return video_manager_;
	}

	@Override
	public IConferenceVoice confVoice() {
		return voice_manager_;
	}

	@Override
	public IConferenceWhiteboard confWhiteboard() {
		return wb_manager_;
	}
	
	@Override
	public IConferenceBox confBox() {
		return box_manager_;
	}

	private native long jnicreate();

	private native void jnidispose();

	private native boolean jnistart();

	private native void jnistop();

}
