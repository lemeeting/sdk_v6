package com.lemeeting.conf;

import com.lemeeting.conf.defines.QzAttendee;
import com.lemeeting.conf.defines.QzConferenceAttribute;
import com.lemeeting.conf.defines.QzProxyOptions;
import com.lemeeting.conf.defines.QzRealTimeConferenceInfo;

public interface IConferenceBase {
	
	String getStringOfDateTime(long dt);
	
	int setUseTcp(boolean is_use_tcp);

	boolean isUseTcp();

	int setProxy(QzProxyOptions options);

	QzProxyOptions getProxy();
	
	QzConferenceAttribute conferenceAttribute();

	QzRealTimeConferenceInfo realtimeConferenceInfo();

	QzAttendee getAttendee(String account);

	QzAttendee getSelfAttendee();


}
