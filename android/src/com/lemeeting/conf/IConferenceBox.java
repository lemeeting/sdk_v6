package com.lemeeting.conf;

public interface IConferenceBox {
	int addObserver(IConferenceBoxObserver observer);

	int removeObserver(IConferenceBoxObserver observer);

	int setCharacteristicCode(String code);

	int toBoxControllerCommand(String cmd);

}
