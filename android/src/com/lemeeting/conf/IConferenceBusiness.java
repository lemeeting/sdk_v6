package com.lemeeting.conf;

public interface IConferenceBusiness {

	public static final int kNoSignin = -1;
	public static final int kImmediatelySignin = 0;
	public static final int kTimingSignin = 1;
	
	int addObserver(IConferenceBusinessObserver observer);

	int removeObserver(IConferenceBusinessObserver observer);

	int entryConference(String json_address, int id_conference, int conf_tag,
			String conf_psw, boolean is_md5, String account, int id_org,
			String name);

	int leaveConference();

	int applySpeak(boolean apply);

	int accreditSpeak(String account, boolean accredit);

	int applyDataOper(boolean apply);

	int accreditDataOper(String account, boolean accredit);

	int applyDataSync(boolean apply);

	int accreditDataSync(String account, boolean accredit);

	int dataSyncCommand(String command);

	int applyTempAdmin(boolean apply);

	int accreditTempAdmin(String account, boolean accredit);

	int authTempAdmin(String admin_psw);

	int startPreviewVideo(String shower, int id_device, long context);

	int stopPreviewVideo(String shower, int id_device, long context);

	int enableVideo(boolean enabled);

	int captureVideoState(int id_device, boolean enabled);

	int UpdateVideoShowName(int id_device, String name);
	
	int kickOutAttendee(String account);

	int updataAttendeeName(String name);

	int relayMsgToOne(String receiver, String msg);

	int relayMsgToAll(String msg);

	int adminOperConfSetting(int cmd, int cmd_value);

	int setConfPassword(String psw);

	int startDesktopShare();

	int stopDesktopShare();

	int startPreviewDesktop(String sharer, long context);

	int stopPreviewDesktop(String sharer, long context);
	
	int launchSignin(int signin_type);
	int signin(boolean is_signin);

	int operSubGroup(String[] account_vector, int id_subgroup);
	int cancelSubGroup();

	int opRemoteDesktopShared(String sharer, boolean is_start);

	int startPlayback(String file_name);	
	int stopPlayback();

}
