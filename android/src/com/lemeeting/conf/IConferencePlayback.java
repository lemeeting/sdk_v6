package com.lemeeting.conf;

public interface IConferencePlayback {
	int addObserver(IConferencePlaybackObserver observer);

	int removeObserver(IConferencePlaybackObserver observer);

	int startPlayback(String file_name);

	int pausePlayback();

	int continuePlayback();

	int stopPlayback();
}
