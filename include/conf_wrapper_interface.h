#ifndef __conf_wrapper_interface_h__
#define __conf_wrapper_interface_h__

#include "conf_defines.h"
#include "conf_wrapper_defines.h"
#include "conf_wrapper_to_access_struct.h"
#include "conf_wrapper_to_center_struct.h"
#include "conf_wrapper_engine.h"
#include "conf_wrapper_base.h"
#include "conf_wrapper_business.h"
#include "conf_wrapper_voice.h"
#include "conf_wrapper_video.h"
#include "conf_wrapper_sharedesktop.h"
#include "conf_wrapper_record.h"
#include "conf_wrapper_playback.h"
#include "conf_wrapper_mediaplayer.h"
#include "conf_wrapper_ipcamera.h"
#include "conf_wrapper_dataserver.h"
#include "conf_wrapper_centerserver.h"

#endif //__conf_wrapper_interface_h__