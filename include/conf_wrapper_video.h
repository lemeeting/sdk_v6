#ifndef __conf_wrapper_video_h__
#define __conf_wrapper_video_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 视频模块回调接口															*/
/************************************************************************/

/*
*	到视频服务器连接通道断开,底层会自动重连
*	param result 错误结果
*	param clientdata 应用层私有参数,通过ConfVideoCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVideoOnDisconnectVideoServer)(int result, void* clientdata);

/*
*	预览本地视频异步回调. @see lm_conf_video_AddLocalPreview
*	param id_device 视频设备ID
*	param id_preview 预览ID,一路视频可以在多个窗口显示
*	param context 上下文,由lm_conf_video_AddLocalPreview传入
*	param error 0-预览成功,-1失败,一般情况是被别的应用占用.
*	param clientdata 应用层私有参数,通过ConfVideoCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVideoOnAddLocalPreview)(
	int id_device, int id_preview, uint64_t context, int error, void* clientdata);

/*
*	视频设备改变回调(热插拔)通过以下接口可以获取视频设备详细信息
*	lm_get_as_attendee_video_device_nums
*	lm_get_as_attendee_enum_video_devices
*	lm_get_as_attendee_video_device_type
*	lm_get_as_attendee_video_device_state
*	lm_get_as_attendee_video_device_show_name
*	lm_get_as_attendee_main_video_device_id

*	param change_device 改变的设备ID
*	param is_add 1-增加设备,0-减少设备
*	param devnums 当前的设备数
*	param main_dev_id 改变后的主设备(有可能会改变)
*	param clientdata 应用层私有参数,通过ConfVideoCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVideoOnVideoDeviceChanged)(
	int change_device, int is_add, int devnums, int main_dev_id, void* clientdata);

/*
*	本地视频渲染分辨率发生改变通知
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param width 宽度
*	param height 高度
*	param clientdata 应用层私有参数,通过ConfVideoCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVideoOnLocalVideoResolutionChanged)(
	int id_device, int id_preview, int width, int height, void* clientdata);

/*
*	远程视频渲染分辨率发生改变通知
*	param account 与会者
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param width 宽度
*	param height 高度
*	param clientdata 应用层私有参数,通过ConfVideoCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVideoOnRemoteVideoResolutionChanged)(
	const char* account, int id_device, int id_preview, int width, int height, void* clientdata);

struct ConfVideoCallback {
	void* client_data_;
	FuncVideoOnDisconnectVideoServer OnDisconnectVideoServer_;
	FuncVideoOnAddLocalPreview OnAddLocalPreview_;
	FuncVideoOnVideoDeviceChanged OnVideoDeviceChanged_;
	FuncVideoOnLocalVideoResolutionChanged OnLocalVideoResolutionChanged_;
	FuncVideoOnRemoteVideoResolutionChanged OnRemoteVideoResolutionChanged_;
};

/*
*	设置远程视频数据没到之前的图片
*	param jpg_file 
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_SetStartImage(
	const char* jpg_file);

/*
*	设置远程视频超时时的的图片
*	param jpg_file
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_SetTimeoutImage(
	const char* jpg_file);

/*
*	设置远程视频禁止时的的图片
*	param jpg_file
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_SetRemoteDisableRemoteVideoImage(
	const char* jpg_file);

/*
*	设置本地禁止远程视频时的的图片
*	param jpg_file
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_SetLocalDisableRemoteVideoImage(
	const char* jpg_file);

/*
*	设置本地虚拟桌面摄像头显示的图片
*	param jpg_file
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_SetLocalDesktopCamearImage(
	const char* jpg_file);


/*
*	设置回调函数
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_video_set_callback(
	native_object_t instance,
	struct ConfVideoCallback* callback);

/*
*	预览本地视频, 一路视频可以多次显示,在同一窗口或在多窗口.最多可以显示16个预览
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param windows 对于window:HWND, 对于Android:SufeceView, 对于ios,osx由lm_conf_base_CreateVideoPreviewWindows产生
*	param context 上下文 @see FuncVideoOnAddLocalPreview
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例  如(1, 0.0, 0.0, 1.0, 1.0)与(0, 0.0, 0.0, 0.5, 0.5),同一窗口会显示一路满屏和一路1/4屏.
*	param id_preview 产生的一个预览ID
*	return 0或-2005 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_AddLocalPreview(
	native_object_t instance,
	int id_device,
	void* windows,
	uint64_t context,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom,
	int* id_preview);

/*
*	调整本地视频的显示位置.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_ConfigureLocalPreview(
	native_object_t instance,
	int id_device,
	int id_preview,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom);

/*
*	更改本地视频的显示窗口及位置.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param windows 显示窗口
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_ChangeLocalPreviewWindow(
	native_object_t instance, int id_device, int id_preview, void* window,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom);

/*
*	移除本地视频预览
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RemoveLocalPreview(
	native_object_t instance, int id_device, int id_preview);

/*
*	镜像本地视频
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param enable 1-镜像, 0-禁止
*	param mirror_xaxis 上下
*	param mirror_yaxis 左右
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableMirrorLocalPreview(
	native_object_t instance, int id_device, int id_preview,
	int enable, int mirror_xaxis, int mirror_yaxis);

/*
*	镜像本地视频状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param enable 1-镜像, 0-禁止	[IN/OUT] (must not be NULL)
*	param mirror_xaxis 上下		[IN/OUT] (must not be NULL)
*	param mirror_yaxis 左右		[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_MirrorLocalPreviewState(
	native_object_t instance, int id_device, int id_preview,
	int* enable, int* mirror_xaxis, int* mirror_yaxis);

/*
*	旋转预览本地视频,与本地视频旋转采集不同 @see lm_conf_video_SetVideoConfig
*	旋转采集会影响到远程视频,旋转预览本地视频只是本地行为,不会远程观看效果.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param rotation 角度 0,90,180, 270有效
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RotateLocalPreview(
	native_object_t instance, int id_device,
	int id_preview, int rotation);

/*
*	旋转预览本地视频状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param rotation 角度 0,90,180, 270有效 [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RotateLocalPreviewState(
	native_object_t instance, int id_device,
	int id_preview, int* rotation);

/*
*	得到本地视频分辨率
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 视频设备ID,当旋转采集时高度与宽度互换
*	param width		[IN/OUT] (must not be NULL)
*	param height	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetLocalPreviewResolution(
	native_object_t instance, int id_device, int* width, int* height);

/*
*	预览远程视频,@see FuncBusinessOnStartPreviewVideo
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param windows 显示窗口
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例 
*	param id_preview 产生的一个预览ID
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_AddRemotePreview(
	native_object_t instance, 
	const char* account,
	int id_device,
	void* windows,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom,
	int* id_preview);

/*
*	调整远程视频的显示位置.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_ConfigureRemotePreview(
	native_object_t instance, 
	const char* account,
	int id_device,
	int id_preview,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom);

/*
*	更改本地视频的显示窗口及位置.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	param windows 显示窗口
*	param z_order 单窗口可以显示多路视频
*	param left 左边比例
*	param top 上边比例
*	param right 右边比例
*	param botton 下边比例
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_ChangeRemotePreviewWindow(
	native_object_t instance, const char* account, 
	int id_device, int id_preview, void* window,
	unsigned int z_order,
	float left,
	float top,
	float right,
	float bottom);

/*
*	移除远程视频预览
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param id_preview 预览ID
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RemoveRemotePreview(
	native_object_t instance, const char* account,
	int id_device, int id_preview);

/*
*	镜像远程视频
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param enable 1-镜像, 0-禁止
*	param mirror_xaxis 上下
*	param mirror_yaxis 左右
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableMirrorRemotePreview(
	native_object_t instance, const char* account,
	int id_device, int id_preview,
	int enable, int mirror_xaxis, int mirror_yaxis);

/*
*	镜像远程视频状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param enable 1-镜像, 0-禁止	[IN/OUT] (must not be NULL)
*	param mirror_xaxis 上下		[IN/OUT] (must not be NULL)
*	param mirror_yaxis 左右		[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_MirrorRemotePreviewState(
	native_object_t instance, const char* account,
	int id_device, int id_preview,
	int* enable, int* mirror_xaxis, int* mirror_yaxis);

/*
*	旋转预览远程视频
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param rotation 角度 0,90,180, 270有效
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RotateRemotePreview(
	native_object_t instance, const char* account,
	int id_device, int id_preview, int rotation);

/*
*	旋转预览远程视频状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param id_preview 预览ID -1影响所有预览此视频窗口
*	param rotation 角度 0,90,180, 270有效 [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RotateRemotePreviewState(
	native_object_t instance, const char* account,
	int id_device, int id_preview, int* rotation);

/*
*	远程视频颜色加强
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param enable
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableRemotePreviewColorEnhancement(
	native_object_t instance, const char* account,
	int id_device, int enable);

/*
*	远程视频颜色加强状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID
*	param enable [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RemotePreviewColorEnhancementState(
	native_object_t instance, const char* account,
	int id_device, int* enable);

/*
*	打开或关闭远程视频预览,本地行为,当关闭时服务器还是会发送数据到本地.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param enable 1-打开,0-关闭
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableRemotePreview(
	native_object_t instance, const char* account, 
	int id_device, int enable);

/*
*	得到远程视频预览状态.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 共享视频者
*	param id_device 视频设备ID
*	param enable [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetRemotePreviewState(
	native_object_t instance, const char* account,
	int id_device, int* enable);

/*
*	得到远程视频分辨率
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device 视频设备ID,当旋转采集时高度与宽度互换
*	param width		[IN/OUT] (must not be NULL)
*	param height	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetRemotePreviewResolution(
	native_object_t instance, const char* account,
	int id_device, int* width, int* height);

/*
*	得到本地视频设备数量.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param nums [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_NumOfDevice(
	native_object_t instance, int* nums);

/*
*	得到本地所有视频设备ID.
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_devices [IN/OUT] (must not be NULL)
*	param nums [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnumDevices(
	native_object_t instance, int* id_devices, int* nums);

/*
*	得到视频设备名称
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 设备ID
*	param name			[IN/OUT] (must not be NULL)
*	param name_size		[IN/OUT] (must not be NULL)
*	param guid			[IN/OUT] (must not be NULL)
*	param guid_size		[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetDeviceName(
	native_object_t instance, int id_device,
	char* name, int name_size, char* guid, int guid_size);

/*
*	得到视频设备ID根据设备GUID
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param guid			[IN/OUT] (must not be NULL)
*	param id_device		[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetDeviceIdWithGuid(
	native_object_t instance, const char* guid, int* id_device);

/*
*	得到视频设备所有采集分辨率
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param pixels with | heigth << 16	[IN/OUT] (must not be NULL)
*	parma count pixels size				[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetDeviceFormat(
	native_object_t instance, int id_device,
	unsigned int *pixels, int* count);

/*
*	打开视频设备属性设置对话框(windows有效)
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param window HWND Mybe NULL
*	parma title Mybe NULL
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_OpenPropertypage(
	native_object_t instance, int id_device,
	void* window, const char* title);

/*
*	切换手机摄像头,对远程来说,手机只有一个摄像头
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_SwitchMobiledevicesCamera(
	native_object_t instance);

/*
*	得到手机摄像头的设备ID
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetMobiledevicesActiveCamera(
	native_object_t instance, int* id_device);

/*
*	视频摄像头是否正在采集中
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device 
*	param value 1-yes, 0-no [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_IsCaptureing(
	native_object_t instance, int id_device, int* value);

/*
*	设置视频设备采集与编码参数
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param config
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_SetVideoConfig(
	native_object_t instance, int id_device, const struct LMVideoConfig* config);

/*
*	设置视频设备当前采集与编码参数
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param config	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetVideoConfig(
	native_object_t instance, int id_device, struct LMVideoConfig* config);

/*
*	去闪烁
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param enable
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableDeflickering(
	native_object_t instance, int id_device, int enable);

/*
*	去闪烁状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param enable	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_DeflickeringState(
	native_object_t instance, int id_device, int* enable);

/*
*	捕获本地视频并存为jpg文件
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param file_name
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetCaptureDeviceSnapshot(
	native_object_t instance, int id_device, const char* file_name);

/*
*	捕获远程视频并存为jpg文件
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param id_device
*	param file_name
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetRenderSnapshot(
	native_object_t instance, const char* account, 
	int id_device, int id_preview, const char* file_name);

/*
*	在视频上禁止或显示预设的名称
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param enable
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_SetShowNameInVideo(
	native_object_t instance, int enable);

/*
*	在视频上禁止或显示预设的名称状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param enable	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetShowNameInVideoState(
	native_object_t instance, int* enable);


/*
*	视频上禁止或显示流量信息
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param enable
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_EnableDebugVideo(
	native_object_t instance, int enable);

/*
*	视频上禁止或显示流量信息状态
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param enable	[IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetDebugVideoState(
	native_object_t instance, int* enable);

/*
*	得到本地视频设备类型
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param id_device
*	param type	@see LMCameraType [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_GetCameraType(
	native_object_t instance, int id_device, int* type);

/*
*	加入IPCamera摄像头
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param info
*	param channel
*	param is_main_stream 主码流或付码流,虚拟为二个摄像头
*	param name 应用层可以为摄像头定义新的名字
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_AddIPCamera(
	native_object_t instance, const struct LMIPCameraInfo* info,
	int channel, int is_main_stream, const char* name);

/*
*	移除IPCamera摄像头
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param identity 0-65535
*	param channel
*	param is_main_stream
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RemoveIPCamera(
	native_object_t instance, int identity,
	int channel, int is_main_stream);

/*
*	加入原始视频数据摄像头
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param identity	0-65535
*	param name 应用层可以为摄像头定义新的名字
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_AddRAWDataCamera(
	native_object_t instance, int identity, const char* name);

/*
*	移除原始视频数据摄像头
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param identity 0-65535
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_RemoveRAWDataCamera(
	native_object_t instance, int identity);

/*
*	原始视频数据
*	param instance 通过lm_get_conf_video_instance_with_engine产生 (must not be NULL)
*	param identity 0-65535
*	param data bytes
*	param data_len data length
*	param width
*	param height
*	param type @see LMRawVideoType
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_video_IncomingRAWData(
	native_object_t instance, int identity, const void* data, int data_len,
	int width, int height, int type);

#endif //__conf_wrapper_video_h__