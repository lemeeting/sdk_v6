#ifndef __conf_wrapper_to_center_struct_h__
#define __conf_wrapper_to_center_struct_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"


/****************************************************************************/
/*								申请消息（废弃未用到）						*/
/****************************************************************************/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_apply_msg_instance(native_object_t* instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_apply_msg_instance(native_object_t instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_apply_msg_instance(native_object_t srcinstance, native_object_t dstinstance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_version_id(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_delete_flag(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_req_id(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_req_type(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_req_state(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_apply_msg_req_result(native_object_t instance);
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_apply_msg_req_time(native_object_t instance);
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_apply_msg_rsp_time(native_object_t instance);
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_apply_msg_req_msg(native_object_t instance);
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_apply_msg_rsp_msg(native_object_t instance);
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_apply_msg_req_data(native_object_t instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_version_id(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_delete_flag(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_id(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_type(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_state(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_result(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_time(native_object_t instance, int64_t value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_rsp_time(native_object_t instance, int64_t value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_msg(native_object_t instance, const char* value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_rsp_msg(native_object_t instance, const char* value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_apply_msg_req_data(native_object_t instance, const char* value);


/****************************************************************************/
/*								会议室验证码信息							*/
/****************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_code_instance(native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_code_instance(native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_code_instance(native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取验证码信息版本号
*	param instance (must not be NULL)
*	return 版本号
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_code_version_id(native_object_t instance);

/*
*	获取验证码删除标志
*	param instance (must not be NULL)
*	return 删除标志
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_code_delete_flag(native_object_t instance);

/*
*	获取验证码类型
*	param instance (must not be NULL)
*	return 验证码类型
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_code_type(native_object_t instance);

/*
*	获取验证码对应会议室ID
*	param instance (must not be NULL)
*	return 会议室ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_code_conference_id(native_object_t instance);

/*
*	获取验证码删除标志
*	param instance (must not be NULL)
*	return 删除标志
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_code_reserve(native_object_t instance);

/*
*	获取验证码ID
*	param instance (must not be NULL)
*	return 验证码ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_code_id(native_object_t instanc);

/*
*	设置验证码版本ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_version_id(native_object_t instance, int value);

/*
*	设置验证码删除标志
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_delete_flag(native_object_t instance, int value);

/*
*	设置验证码类型
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_type(native_object_t instance, int value);

/*
*	设置验证码对应会议室ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_conference_id(native_object_t instance, int value);

/*
*	设置验证码备注
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_reserve(native_object_t instance, int value);

/*
*	设置验证码ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_code_id(native_object_t instance, const char* value);



/****************************************************************************/
/*								注册企业信息  							    */
/****************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_org_instance(native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_org_instance(native_object_t instance);

/*
*	拷贝实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_org_instance(native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取企业信息版本号
*	param instance (must not be NULL)
*	return 版本号
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_version_id(native_object_t instance);

/*
*	获取企业信息删除标志
*	param instance (must not be NULL)
*	return 删除标志
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_delete_flag(native_object_t instance);

/*
*	获取企业ID
*	param instance (must not be NULL)
*	return 企业ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_world_id(native_object_t instance);

/*
*	获取企业信息类型
*	param instance (must not be NULL)
*	return 企业信息类型
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_type(native_object_t instance);

/*
*	获取企业标志
*	param instance (must not be NULL)
*	return 企业标志
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_flag(native_object_t instance);

/*
*	获取企业状态
*	param instance (must not be NULL)
*	return 企业状态
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_status(native_object_t instance);

/*
*	获取企业组ID
*	param instance (must not be NULL)
*	return 企业组ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_group_id(native_object_t instance);

/*
*	获取企业最大人数
*	param instance (must not be NULL)
*	return 企业最大人数
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_max_user_nums(native_object_t instance);

/*
*	获取企业名称
*	param instance (must not be NULL)
*	return 企业名称
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_name(native_object_t instance);

/*
*	获取企业显示名称
*	param instance (must not be NULL)
*	return 企业显示名称
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_show_name(native_object_t instance);

/*
*	获取企业地址
*	param instance (must not be NULL)
*	return 企业地址
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_address(native_object_t instance);

/*
*	获取企业联系人
*	param instance (must not be NULL)
*	return 企业联系人
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_contacter(native_object_t instance);

/*
*	获取企业联系电话
*	param instance (must not be NULL)
*	return 企业联系电话
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_phone(native_object_t instance);

/*
*	获取企业电子信箱
*	param instance (must not be NULL)
*	return 企业电子信箱
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_email(native_object_t instance);

/*
*	获取企业网站
*	param instance (must not be NULL)
*	return 企业网站
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_website(native_object_t instance);

/*
*	获取企业创建日期
*	param instance (must not be NULL)
*	return 企业创建日期
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_birthday(native_object_t instance);

/*
*	获取信息创建时间
*	param instance (must not be NULL)
*	return 创建时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_create_time(native_object_t instance);

/*
*	获取信息修改时间
*	param instance (must not be NULL)
*	return 修改时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_modify_time(native_object_t instance);

/*
*	获取信息创建者
*	param instance (must not be NULL)
*	return 创建者
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_creator(native_object_t instance);

/*
*	获取信息修改者
*	param instance (must not be NULL)
*	return 修改者
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_mender(native_object_t instance);

/*
*	获取企业设置JSON
*	param instance (must not be NULL)
*	return 企业设置JSON
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_setting_json(native_object_t instance);

/*
*	获取企业扩展信息JSON
*	param instance (must not be NULL)
*	return 企业扩展信息JSON
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_extend_json(native_object_t instance);

/*
*	设置信息版本号
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_version_id(native_object_t instance, int value);

/*
*	设置信息删除标志
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_delete_flag(native_object_t instance, int value);

/*
*	设置企业ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_world_id(native_object_t instance, int value);

/*
*	设置企业类型
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_type(native_object_t instance, int value);

/*
*	设置企业标志
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_flag(native_object_t instance, int value);

/*
*	设置企业状态
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_status(native_object_t instance, int value);

/*
*	设置企业组ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_group_id(native_object_t instance, int value);

/*
*	设置企业最大人数
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_max_user_nums(native_object_t instance, int value);

/*
*	设置企业名称
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_name(native_object_t instance, const char* value);

/*
*	设置显示名称
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_show_name(native_object_t instance, const char* value);

/*
*	设置企业地址
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_address(native_object_t instance, const char* value);

/*
*	设置企业联系人
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_contacter(native_object_t instance, const char* value);

/*
*	设置企业联系电话
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_phone(native_object_t instance, const char* value);

/*
*	设置企业电子邮箱
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_email(native_object_t instance, const char* value);

/*
*	设置企业网站
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_website(native_object_t instance, const char* value);

/*
*	设置企业创建时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_birthday(native_object_t instance, int64_t value);

/*
*	设置信息创建时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_create_time(native_object_t instance, int64_t value);

/*
*	设置信息修改时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_modify_time(native_object_t instance, int64_t value);

/*
*	设置信息创建者
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_creator(native_object_t instance, const char* value);

/*
*	设置信息修改者
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_mender(native_object_t instance, const char* value);

/*
*	设置企业设置JSON
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_setting_json(native_object_t instance, const char* value);

/*
*	设置企业扩展JSON
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_extend_json(native_object_t instance, const char* value);


/****************************************************************************/
/*								 企业用户信息	   						    */
/****************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_org_user_instance(native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_org_user_instance(native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_org_user_instance(native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取用户信息版本号
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_version_id(native_object_t instance);

/*
*	获取用户信息删除标志
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_delete_flag(native_object_t instance);

/*
*	获取用户信息企业ID
*	param instance (must not be NULL)
*	return > 0
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_org_id(native_object_t instance);

/*
*	获取用户信息类型
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_type(native_object_t instance);

/*
*	获取用户信息状态
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_status(native_object_t instance);

/*
*	获取获取用户绑定的个人账号ID
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_org_user_bind_user(native_object_t instance);

/*
*	获取用户生日
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_user_birthday(native_object_t instance);

/*
*	获取信息创建时间
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_user_create_time(native_object_t instance);

/*
*	获取企业用户权限
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_org_user_perm(native_object_t instance);

/*
*	获取企业用户账号
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_account(native_object_t instance);

/*
*	获取企业用户名称
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_name(native_object_t instance);

/*
*	获取企业用户密码
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_password(native_object_t instance);

/*
*	获取企业用户电话
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_phone(native_object_t instance);

/*
*	获取企业用户EMAIL
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_email(native_object_t instance);

/*
*	获取企业用户电话设置信息
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_setting_json(native_object_t instance);

/*
*	获取企业用户扩展信息
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_org_user_extend_json(native_object_t instance);


/*
*	设置企业用户版本ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_version_id(native_object_t instance, int value);

/*
*	设置企业用户删除标志
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_delete_flag(native_object_t instance, int value);

/*
*	设置用户的企业ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_org_id(native_object_t instance, int value);

/*
*	设置用户类型
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_type(native_object_t instance, int value);

/*
*	设置用户状态
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_status(native_object_t instance, int value);

/*
*	设置用户绑定个人用户ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_bind_user(native_object_t instance, int value);

/*
*	设置企业用户生日
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_birthday(native_object_t instance, int64_t value);

/*
*	设置用户信息创建时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_create_time(native_object_t instance, int64_t value);

/*
*	设置用户权限
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_perm(native_object_t instance, int64_t value);

/*
*	设置企业用户账号
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_account(native_object_t instance, const char* value);

/*
*	设置企业用户名称
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_name(native_object_t instance, const char* value);

/*
*	设置企业用户密码
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_password(native_object_t instance, const char* value);

/*
*	设置企业用户电话
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_phone(native_object_t instance, const char* value);

/*
*	设置企业用户EMAIL
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_email(native_object_t instance, const char* value);

/*
*	设置企业用户设置
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_setting_json(native_object_t instance, const char* value);

/*
*	设置企业用户扩展
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_org_user_extend_json(native_object_t instance, const char* value);


/************************************************************************/
/*				会议参数集合操作                                         */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_param_instance(native_object_t* instance);
/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_param_instance(native_object_t instance);
/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_param_instance(native_object_t srcinstance, native_object_t dstinstance);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_count(native_object_t instance);
/*
*	获取key对应的int值
*	param instance (must not be NULL)
*	return int value
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_contains_key(native_object_t instance, int key);
/*
*	获取key列表
*	param instance (must not be NULL)
*	return 
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_keys(native_object_t instance, int* keys, int count);
/*
*	获取key的int值
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_int_value(native_object_t instance, int key, int* value);
/*
*	获取key的unsigned int值
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_uint_value(native_object_t instance, int key, unsigned int* value);
/*
*	获取key的字符串值
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_string_value(native_object_t instance, int key, char* value, int size);
/*
*	获取key的int64值
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_param_time_value(native_object_t instance, int key, int64_t* value);

/*
*	设置key的unsigned int值
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_param_uint_value(native_object_t instance, int key, unsigned int value);
/*
*	设置key的int值
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_param_int_value(native_object_t instance, int key, int value);
/*
*	设置key的字符串值
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_param_string_value(native_object_t instance, int key, const char* value);
/*
*	设置key的int64值
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_param_time_value(native_object_t instance, int key, int64_t value);


/************************************************************************/
/*				推送消息                                         */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_push_msg_instance(native_object_t* instance);
/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_push_msg_instance(native_object_t instance);
/*
*	Copy实例
*	param srcinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_push_msg_instance(native_object_t srcinstance, native_object_t dstinstance);
/*
*	获取消息版本ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_version_id(native_object_t instance);
/*
*	获取消息删除标志
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_delete_flag(native_object_t instance);
/*
*	获取消息ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_id(native_object_t instance);
/*
*	获取消息类型
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_type(native_object_t instance);
/*
*	获取消息状态
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_state(native_object_t instance);
/*
*	获取消息通知范围（1=全部通知，0=指定人员通知）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_result(native_object_t instance);
/*
*	获取消息企业ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_push_msg_send_org_id(native_object_t instance);
/*
*	获取消息发送时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_push_msg_send_time(native_object_t instance);
/*
*	获取消息结束时间（此处为 会议室开始时间）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_push_msg_end_time(native_object_t instance);
/*
*	获取消息主题
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_push_msg_topic(native_object_t instance);
/*
*	获取消息发送者账号
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_push_msg_send_account(native_object_t instance);
/*
*	获取消息数据（存储消息内容等的JSON字符串）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_push_msg_data(native_object_t instance);
/*
*	获取消息接收者（存储接收者信息的JSON字符串）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_push_msg_receiver(native_object_t instance);
/*
*	获取消息扩展信息（存储扩展信息的JSON字符串）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_push_msg_extend_json(native_object_t instance);

/*
*	设置消息版本号（服务器处理，客户端无需设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_version_id(native_object_t instance, int value);
/*
*	设置消息删除标志（服务器处理，客户端无需设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_delete_flag(native_object_t instance, int value);
/*
*	设置消息ID（服务器处理，客户端无需设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_id(native_object_t instance, int value);
/*
*	设置消息类型
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_type(native_object_t instance, int value);
/*
*	设置消息状态
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_state(native_object_t instance, int value);
/*
*	设置消息范围（指定人员或全体推送）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_result(native_object_t instance, int value);
/*
*	设置消息企业ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_send_org_id(native_object_t instance, int value);
/*
*	设置消息发送时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_send_time(native_object_t instance, int64_t value);
/*
*	设置消息结束时间（会议开始时间）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_end_time(native_object_t instance, int64_t value);
/*
*	设置消息主题
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_topic(native_object_t instance, const char* value);
/*
*	设置消息发送者账号
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_send_account(native_object_t instance, const char* value);
/*
*	设置消息数据（JSON字符串）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_data(native_object_t instance, const char* value);
/*
*	设置消息接收者列表（JSON字符串）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_receiver(native_object_t instance, const char* value);
/*
*	设置消息扩展信息（JSON字符串）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_push_msg_extend_json(native_object_t instance, const char* value);


/************************************************************************/
/*				真实会议信息                                         */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_real_conf_instance(native_object_t* instance);
/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_real_conf_instance(native_object_t instance);
/*
*	Copy实例
*	param srcinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_real_conf_instance(native_object_t srcinstance, native_object_t dstinstance);
/*
*	获取真实会议版本ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_version_id(native_object_t instance);
/*
*	获取真实会议会议室ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_conference_id(native_object_t instance);
/*
*	获取真实会议企业ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_world_id(native_object_t instance);
/*
*	获取真实会议集群组ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_group_id(native_object_t instance);
/*
*	获取真实会议集群组服务器ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_server_id(native_object_t instance);
/*
*	获取真实会议在线人数
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_online_users(native_object_t instance);
/*
*	获取真实会议标志（保留字段）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_real_conf_flag(native_object_t instance);
/*
*	获取真实会议修改时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_real_conf_modify_time(native_object_t instance);
/*
*	获取真实会议扩展JSON信息
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_real_conf_extend_json(native_object_t instance);
/*
*	设置真实会议版本ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_version_id(native_object_t instance, int value);
/*
*	设置真实会议会议室ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_conference_id(native_object_t instance, int value);
/*
*	设置真实会议企业ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_world_id(native_object_t instance, int value);
/*
*	设置真实会议集群组ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_group_id(native_object_t instance, int value);
/*
*	设置真实会议集群服务器ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_server_id(native_object_t instance, int value);
/*
*	设置真实会议在线人数（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_online_users(native_object_t instance, int value);
/*
*	设置真实会议会议标志（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_flag(native_object_t instance, int value);
/*
*	设置真实会议修改时间（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_modify_time(native_object_t instance, int64_t value);
/*
*	设置真实会议扩展JSON（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_real_conf_extend_json(native_object_t instance, const char* value);


/************************************************************************/
/*				会议室信息                                         */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_room_instance(native_object_t* instance);
/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_room_instance(native_object_t instance);
/*
*	Copy实例
*	param srcinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_room_instance(native_object_t srcinstance, native_object_t dstinstance);
/*
*	获取会议室版本ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_version_id(native_object_t instance);
/*
*	获取会议室删除标志
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_delete_flag(native_object_t instance);
/*
*	获取会议室ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_id(native_object_t instance);
/*
*	获取会议室类型
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_type(native_object_t instance);
/*
*	获取会议室所属企业ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_world_id(native_object_t instance);
/*
*	获取会议室所属集群组ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_group_id(native_object_t instance);
/*
*	获取会议室所属服务器ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_server_id(native_object_t instance);
/*
*	获取会议室最大人数
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_max_user_nums(native_object_t instance);
/*
*	获取会议室最大发言人数
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_max_speaker_nums(native_object_t instance);
/*
*	获取会议室标志
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_room_flag(native_object_t instance);
/*
*	获取会议室开始时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_room_start_time(native_object_t instance);
/*
*	获取会议室结束时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_room_end_time(native_object_t instance);
/*
*	获取会议室创建时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_room_create_time(native_object_t instance);
/*
*	获取会议室修改时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_room_modify_time(native_object_t instance);
/*
*	获取会议室名称
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_name(native_object_t instance);
/*
*	获取会议室密码
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_conf_password(native_object_t instance);
/*
*	获取会议室管理密码
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_manager_password(native_object_t instance);
/*
*	获取会议室创建者
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_creator(native_object_t instance);
/*
*	获取会议室修改者
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_mender(native_object_t instance);
/*
*	获取会议室设置JSON
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_setting_json(native_object_t instance);
/*
*	获取会议室扩展JSON
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_room_extend_json(native_object_t instance);
/*
*	设置会议室版本ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_version_id(native_object_t instance, int value);
/*
*	设置会议室删除标志（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_delete_flag(native_object_t instance, int value);
/*
*	设置会议室ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_id(native_object_t instance, int value);
/*
*	设置会议室类型
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_type(native_object_t instance, int value);
/*
*	设置会议室所属企业ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_world_id(native_object_t instance, int value);
/*
*	设置会议室所属集全组ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_group_id(native_object_t instance, int value);
/*
*	设置会议室所属服务器ID
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_server_id(native_object_t instance, int value);
/*
*	设置会议室最大人数
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_max_user_nums(native_object_t instance, int value);
/*
*	设置会议室最大发言人数
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_max_speaker_nums(native_object_t instance, int value);
/*
*	设置会议室标志
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_flag(native_object_t instance, int value);
/*
*	设置会议室开始时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_start_time(native_object_t instance, int64_t value);
/*
*	设置会议室结束时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_end_time(native_object_t instance, int64_t value);
/*
*	设置会议室创建时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_create_time(native_object_t instance, int64_t value);
/*
*	设置会议室修改时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_modify_time(native_object_t instance, int64_t value);
/*
*	设置会议室名称
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_name(native_object_t instance, const char* value);
/*
*	设置会议室会议密码
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_conf_password(native_object_t instance, const char* value);
/*
*	设置会议室管理密码
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_manager_password(native_object_t instance, const char* value);
/*
*	设置会议室创建者
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_creator(native_object_t instance, const char* value);
/*
*	设置会议室修改者
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_mender(native_object_t instance, const char* value);
/*
*	设置会议室设置JSON
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_setting_json(native_object_t instance, const char* value);
/*
*	设置会议室扩展JSON
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_room_extend_json(native_object_t instance, const char* value);


/************************************************************************/
/*				用户绑定信息（服务器端设置）                             */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_user_bind_instance(native_object_t* instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_user_bind_instance(native_object_t instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_user_bind_instance(native_object_t srcinstance, native_object_t dstinstance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_bind_version_id(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_bind_delete_flag(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_bind_world_id(native_object_t instance);
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_bind_type(native_object_t instance);
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_user_bind_create_time(native_object_t instance);
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_bind_name(native_object_t instance);
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_bind_extend_json(native_object_t instance);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_version_id(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_delete_flag(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_world_id(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_type(native_object_t instance, int value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_create_time(native_object_t instance, int64_t value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_name(native_object_t instance, const char* value);
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_bind_extend_json(native_object_t instance, const char* value);


/************************************************************************/
/*				用户信息					                             */
/************************************************************************/
/**	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_cs_user_instance(native_object_t* instance);
/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_cs_user_instance(native_object_t instance);
/*
*	Copy实例
*	param srcinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_cs_user_instance(native_object_t srcinstance, native_object_t dstinstance);
/*
*	获取用户版本ID（服务器端设置）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_version_id(native_object_t instance);
/*
*	获取用户删除标志（服务器端设置）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_delete_flag(native_object_t instance);
/*
*	获取用户所属企业ID
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_world_id(native_object_t instance);
/*
*	获取用户类型
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_type(native_object_t instance);
/*
*	获取用户状态
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_cs_user_status(native_object_t instance);
/*
*	获取用户生日
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_user_birthday(native_object_t instance);
/*
*	获取用户创建时间
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_cs_user_create_time(native_object_t instance);
/*
*	获取用户账号
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_account(native_object_t instance);
/*
*	获取用户名称
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_name(native_object_t instance);
/*
*	获取用户密码
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_password(native_object_t instance);
/*
*	获取用户电话
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_phone(native_object_t instance);
/*
*	获取用户EMAIL
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_email(native_object_t instance);
/*
*	获取设置JSON（服务器端设置）
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_setting_json(native_object_t instance);
/*
*	获取用户扩展JSON设置
*	param instance (must not be NULL)
*	return
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_cs_user_extend_json(native_object_t instance);

/*
*	设置用户版本ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_version_id(native_object_t instance, int value);
/*
*	设置用户删除标志（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_delete_flag(native_object_t instance, int value);
/*
*	设置用户所属企业ID（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_world_id(native_object_t instance, int value);
/*
*	设置用户类型（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_type(native_object_t instance, int value);
/*
*	设置用户状态（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_status(native_object_t instance, int value);
/*
*	设置用户生日
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_birthday(native_object_t instance, int64_t value);
/*
*	设置用户创建时间
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_create_time(native_object_t instance, int64_t value);
/*
*	设置用户账号
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_account(native_object_t instance, const char* value);
/*
*	设置用户名称
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_name(native_object_t instance, const char* value);
/*
*	设置用户密码
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_password(native_object_t instance, const char* value);
/*
*	设置用户电话
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_phone(native_object_t instance, const char* value);
/*
*	设置用户EMAIL
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_email(native_object_t instance, const char* value);
/*
*	设置用户JSON信息（服务器端设置）
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_setting_json(native_object_t instance, const char* value);
/*
*	设置用户扩展JSON信息
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_set_cs_user_extend_json(native_object_t instance, const char* value);


#endif //__conf_wrapper_to_center_struct_h__