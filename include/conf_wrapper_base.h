#ifndef __conf_wrapper_base_h__
#define __conf_wrapper_base_h__

#include "conf_defines.h"
#include "conf_wrapper_defines.h"

EXTERN_C CONFWRAPPER_API void* CALLBACK conf_malloc_and_zero(int size);
EXTERN_C CONFWRAPPER_API void CALLBACK conf_free(void* p);

/*
*	基本操作接口
*	native_object_t instance 由lm_get_conf_base_instance_with_engine产生
*/

EXTERN_C CONFWRAPPER_API void CALLBACK lm_get_version(
	char* version, int size);

/**
*	得到默认会议中间层初始化参数
*	param options (must not be NULL)
*	return void
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_get_default_init_options(
	struct LMInitOptions* options);

/**
*	得到错误描述
*	param serror (must not be NULL)
*	param serror bytes size
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_get_error_string(
	int error, char* serror, int len);


/**
*	根据默认值初始化会议中间层
*	param instance (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_init(
	native_object_t instance);

/**
*	根据设定值初始化会议中间层
*	param instance (must not be NULL)
*	param options (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_init_with_options(
	native_object_t instance, const struct LMInitOptions* options);

/**
*	结束会议中间层
*	param instance (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_terminate(
	native_object_t instance);

/**
*	设置使用TCP传输
*	param instance (must not be NULL)
*	param is_use_tcp 
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_set_use_tcp(
	native_object_t instance, int is_use_tcp);

/**
*	当前是否使用TCP传输
*	param instance (must not be NULL)
*	param is_use_tcp (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_is_use_tcp(
	native_object_t instance, int* is_use_tcp);

/**
*	设置代理
*	param instance (must not be NULL)
*	param proxy_options (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_set_proxy(
	native_object_t instance, const struct LMProxyOptions* proxy_options);

/**
*	获取代理属性
*	param instance (must not be NULL)
*	param proxy_options (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_get_proxy(
	native_object_t instance, struct LMProxyOptions* proxy_options);

/**
*	设置桌面copy的帧率(当前只支持windows平台)
*	param instance (must not be NULL)
*	param fps (max value 10)
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_set_screen_copy_fps(
	native_object_t instance, int fps);

/**
*	获取桌面copy的帧率
*	param instance (must not be NULL)
*	return fps
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_screen_copy_fps(
	native_object_t instance);

/**
*	设置桌面copy时显否copy分层窗口,当copy分层窗口,cpu占用上升(当前只支持windows平台)
*	param instance (must not be NULL)
*	param enable
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_enable_screen_copy_layer_window(
	native_object_t instance, int enable);

/**
*	获取桌面copy时显否copy分层窗口
*	param instance (must not be NULL)
*	return 1-copy 0-no copy
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_is_enable_screen_copy_layer_window(
	native_object_t instance);

/**
*	设置是否开启虚拟桌面摄像头(当前只支持windows平台)
*	param instance (must not be NULL)
*	param enable
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_enable_virtual_desktop_camera(
	native_object_t instance, int enable);

/**
*	获取是否开启虚拟桌面摄像头
*	param instance (must not be NULL)
*	return 1-开启 0-不开启
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_is_enable_virtual_desktop_camera(
	native_object_t instance);

/**
*	根据保存配置设置主视频设备
*	param instance (must not be NULL)
*	param unique_id 主视频设备guid
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_set_main_video_device(
	native_object_t instance, const char* unique_id);


/**
*	得到当前上传下载流量
*	param instance (must not be NULL)
*	param per_mon (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_per_monitor(
	native_object_t instance, struct LMPerMonitor* per_mon);

/**
*	产生视频预览窗口 ios, osx
*	param instance (must not be NULL)
*	param originX 
*	param originY
*	param width
*	param height
*	return ios UIView, osx NSOpenGLView null failure
*/
EXTERN_C CONFWRAPPER_API void* CALLBACK lm_conf_base_CreateVideoPreviewWindows(
	native_object_t instance, float originX, float originY, float width, float height);

/**
*	销毁视频预览窗口 ios, osx
*	param instance (must not be NULL)
*	param windows @see lm_conf_base_CreateVideoPreviewWindows
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_base_DeleteVideoPreviewWindows(
	native_object_t instance, void* windows);

/**
*	得到当前会议信息
*	param instance (must not be NULL)
*	param attribute (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_conf_attribute(
	native_object_t instance, native_object_t* attribute);

/**
*	得到当前会议实时信息
*	param instance (must not be NULL)
*	param info (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_real_time_conf_info(
	native_object_t instance, native_object_t* info);

/**
*	得到当前会议同步信息
*	param instance (must not be NULL)
*	param info (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_conf_sync_info(
	native_object_t instance, native_object_t* info);

/**
*	得到与会者本人信息
*	param instance (must not be NULL)
*	param info (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_self_attendee_info(
	native_object_t instance, native_object_t* info);

/**
*	得到其他与会者信息
*	param instance (must not be NULL)
*	param account (must not be NULL)
*	param info (must not be NULL)
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_base_get_attendee_info(
	native_object_t instance, const char* account, native_object_t* info);

#endif //__conf_wrapper_base_h__