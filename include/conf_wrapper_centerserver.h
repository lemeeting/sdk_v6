#ifndef __conf_wrapper_centerserver_h__
#define __conf_wrapper_centerserver_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 中心服务器管理模块回调接口                                           */
/************************************************************************/

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnDisconnectCenter
// 函数状态: 使用中
// 函数功能：断开中心服务器回调
// 触发条件: 当调用lm_conf_centerserver_Login()函数后，网络断线回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnDisconnectCenter)(int result, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetAuthCode
// 函数状态: 未启用
// 函数功能：获取认证码回调
// 触发条件: 当调用lm_conf_centerserver_GetAuthCode()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetAuthCode)(int result, native_object_t, const char*, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnRegUserInfo
// 函数状态: 使用中
// 函数功能：注册用户信息回调
// 触发条件: 当调用lm_conf_centerserver_RegUserInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t  为返回的注册用户的信息，（参见 lm_create_cs_user_instance）
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRegUserInfo)(int result, native_object_t, native_object_t, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnLogin
// 函数状态: 使用中
// 函数功能：登录系统回调
// 触发条件: 当调用lm_conf_centerserver_Login()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t  为返回的注册用户的信息，（参见 lm_create_cs_user_instance）
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnLogin)(int result, native_object_t, native_object_t, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnLogout
// 函数状态: 使用中
// 函数功能：退出系统回调
// 触发条件: 当调用lm_conf_centerserver_Logout()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnLogout)(int result, native_object_t, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnPrepareLoginConf
// 函数状态: 使用中
// 函数功能：回调准备入会信息
// 触发条件: 当调用lm_conf_centerserver_PrepareLoginConf()函数后，回调此函数。
// 参数说明: (1) int result ,返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t ,为参数集合（包括调用时设置的和服务器带回的）
//			 (3) int ,准备登录的会议室ID
//			 (4) const char* ,准备登录的会议室所在服务器地址，此地址为JSON串,
//				格式为：{"Address":"103.56.62.174;222.186.136.174","Port":2811}
//			 (5) clientdata 应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnPrepareLoginConf)(int result, native_object_t, int, const char*, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetRealConf
// 函数状态: 使用中
// 函数功能：获取实时会议列表
// 触发条件: 当调用lm_conf_centerserver_GetRealConf()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为真实会议室集合（包括实时人数等）
//			 (4) native_object_t  为真实会议室集合元素个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetRealConf)(int result, native_object_t, native_object_t*, int, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetUserInfo
// 函数状态: 使用中
// 函数功能：获取用户信息
// 触发条件: 当调用lm_conf_centerserver_GetUserInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t  为用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetUserInfo)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateUserInfo
// 函数状态: 使用中
// 函数功能：更新用户信息回调
// 触发条件: 当调用lm_conf_centerserver_UpdateUserInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t  为用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdateUserInfo)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateUserInfo
// 函数状态: 使用中
// 函数功能：删除用户信息回调
// 触发条件: 当调用lm_conf_centerserver_RemoveUserInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t  为用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRemoveUserInfo)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetUserInfoList
// 函数状态: 使用中
// 函数功能：获取用户信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetUserInfoList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t  为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为用户信息实例列表
//			 (4) int  为用户信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetUserInfoList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetOrgInfo
// 函数状态: 使用中
// 函数功能：获取企业信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetOrgInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetOrgInfo)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateOrgInfo
// 函数状态: 使用中
// 函数功能：更新企业信息列表回调
// 触发条件: 当调用lm_conf_centerserver_UpdateOrgInfo()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdateOrgInfo)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetOrgInfoList
// 函数状态: 未用到
// 函数功能：更新企业信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetOrgInfoList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为企业信息实例列表
//			 (4) int  为企业信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetOrgInfoList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetUserBind
// 函数状态: 客户端暂时未用到
// 函数功能：获取用户绑定信息
// 触发条件: 当调用lm_conf_centerserver_GetUserBind()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetUserBind)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnAddUserBind)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnUpdateUserBind)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnRemoveUserBind)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnGetUserBindList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetOrgUser
// 函数状态: 使用中
// 函数功能：获取企业用户信息
// 触发条件: 当调用lm_conf_centerserver_GetOrgUser()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetOrgUser)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnAddOrgUser
// 函数状态: 使用中
// 函数功能：添加企业用户信息
// 触发条件: 当调用lm_conf_centerserver_AddOrgUser()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnAddOrgUser)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateOrgUser
// 函数状态: 使用中
// 函数功能：修改企业用户信息
// 触发条件: 当调用lm_conf_centerserver_UpdateOrgUser()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdateOrgUser)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnRemoveOrgUser
// 函数状态: 使用中
// 函数功能：删除企业用户信息
// 触发条件: 当调用lm_conf_centerserver_RemoveOrgUser()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为企业用户信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRemoveOrgUser)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetOrgUserList
// 函数状态: 使用中
// 函数功能：更新企业用户信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetOrgUserList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为企业用户信息实例列表
//			 (4) int  为企业用户信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetOrgUserList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetConfRoom
// 函数状态: 使用中
// 函数功能：获取会议室信息
// 触发条件: 当调用lm_conf_centerserver_GetConfRoom()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为会议室信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetConfRoom)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnAddConfRoom
// 函数状态: 使用中
// 函数功能：添加会议室信息
// 触发条件: 当调用lm_conf_centerserver_AddConfRoom()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为会议室信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnAddConfRoom)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateConfRoom
// 函数状态: 使用中
// 函数功能：修改会议室信息
// 触发条件: 当调用lm_conf_centerserver_UpdateConfRoom()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为会议室信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdateConfRoom)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnRemoveConfRoom
// 函数状态: 使用中
// 函数功能：删除会议室信息
// 触发条件: 当调用lm_conf_centerserver_RemoveConfRoom()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为会议室信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRemoveConfRoom)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetConfRoomList
// 函数状态: 使用中
// 函数功能：获取会议室信息列表
// 触发条件: 当调用lm_conf_centerserver_GetConfRoomList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为会议室信息实例列表
//			 (4) int  为会议室信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetConfRoomList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetConfCode
// 函数状态: 使用中
// 函数功能：获取会议室验证码信息
// 触发条件: 当调用lm_conf_centerserver_GetConfCode()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为验证码信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetConfCode)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnAddConfCode
// 函数状态: 使用中
// 函数功能：获取会议室验证码信息
// 触发条件: 当调用lm_conf_centerserver_AddConfCode()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为验证码信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnAddConfCode)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdateConfCode
// 函数状态: 使用中
// 函数功能：修改会议室验证码信息
// 触发条件: 当调用lm_conf_centerserver_UpdateConfCode()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为验证码信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdateConfCode)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnRemoveConfCode
// 函数状态: 使用中
// 函数功能：删除会议室验证码信息
// 触发条件: 当调用lm_conf_centerserver_RemoveConfCode()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为验证码信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRemoveConfCode)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetConfCodeList
// 函数状态: 使用中
// 函数功能：获取验证码信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetConfCodeList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为验证码信息实例列表
//			 (4) int  为验证码信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetConfCodeList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);


//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetConfCode
// 函数状态: 客户端暂时未用到
// 函数功能：以下申请消息相关5个函数未启用
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetApplyMsg)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnAddApplyMsg)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnUpdateApplyMsg)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnRemoveApplyMsg)(int result, native_object_t, native_object_t, void* clientdata);
typedef void(CALLBACK *FuncToCenterOnGetApplyMsgList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetPushMsg
// 函数状态: 使用中
// 函数功能：获取推送消息
// 触发条件: 当调用lm_conf_centerserver_GetPushMsg()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为推送消息信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetPushMsg)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnAddPushMsg
// 函数状态: 使用中
// 函数功能：添加推送消息
// 触发条件: 当调用lm_conf_centerserver_AddPushMsg()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为推送消息信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnAddPushMsg)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnUpdatePushMsg
// 函数状态: 使用中
// 函数功能：修改推送消息
// 触发条件: 当调用lm_conf_centerserver_UpdatePushMsg()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为推送消息信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnUpdatePushMsg)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnRemovePushMsg
// 函数状态: 使用中
// 函数功能：添加推送消息
// 触发条件: 当调用lm_conf_centerserver_RemovePushMsg()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为推送消息信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnRemovePushMsg)(int result, native_object_t, native_object_t, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnGetPushMsgList
// 函数状态: 使用中
// 函数功能：获取验证码信息列表回调
// 触发条件: 当调用lm_conf_centerserver_GetPushMsgList()函数后，回调此函数。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t* 为推送消息信息实例列表
//			 (4) int  为推送消息信息实例个数
//			 (5) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnGetPushMsgList)(int result, native_object_t, native_object_t, native_object_t*, int, void* clientdata);

//-------------------------------------------------------------------
// 函数名称: FuncToCenterOnNoticePushMsg
// 函数状态: 使用中
// 函数功能：添加推送消息
// 触发条件: 别人添加是，服务器主动通知此回调。
// 参数说明: (1) int result  返回值0为成功，其它错误码参见call lm_get_error_lm_get_error_string说明
//			 (2) native_object_t 为参数集合（包括调用时设置的和服务器带回的）
//			 (3) native_object_t 为推送消息信息实例
//			 (4) clientdata  应用层私有参数,通过ConfToCenterCallback结构传入 [IN]
//-------------------------------------------------------------------
typedef void(CALLBACK *FuncToCenterOnNoticePushMsg)(native_object_t, native_object_t, void* clientdata);


struct ConfToCenterCallback {
	void* client_data_;
	FuncToCenterOnDisconnectCenter OnDisconnectCenter_;
	FuncToCenterOnGetAuthCode OnGetAuthCode_;
	FuncToCenterOnRegUserInfo OnRegUserInfo_;
	FuncToCenterOnLogin OnLogin_;
	FuncToCenterOnLogout OnLogout_;
	FuncToCenterOnPrepareLoginConf OnPrepareLoginConf_;
	FuncToCenterOnGetRealConf OnGetRealConf_;
	FuncToCenterOnGetUserInfo OnGetUserInfo_;
	FuncToCenterOnUpdateUserInfo OnUpdateUserInfo_;
	FuncToCenterOnRemoveUserInfo OnRemoveUserInfo_;
	FuncToCenterOnGetUserInfoList OnGetUserInfoList_;
	FuncToCenterOnGetOrgInfo OnGetOrgInfo_;
	FuncToCenterOnUpdateOrgInfo OnUpdateOrgInfo_;
	FuncToCenterOnGetOrgInfoList OnGetOrgInfoList_;
	FuncToCenterOnGetUserBind OnGetUserBind_;
	FuncToCenterOnAddUserBind OnAddUserBind_;
	FuncToCenterOnUpdateUserBind OnUpdateUserBind_;
	FuncToCenterOnRemoveUserBind OnRemoveUserBind_;
	FuncToCenterOnGetUserBindList OnGetUserBindList_;
	FuncToCenterOnGetOrgUser OnGetOrgUser_;
	FuncToCenterOnAddOrgUser OnAddOrgUser_;
	FuncToCenterOnUpdateOrgUser OnUpdateOrgUser_;
	FuncToCenterOnRemoveOrgUser OnRemoveOrgUser_;
	FuncToCenterOnGetOrgUserList OnGetOrgUserList_;
	FuncToCenterOnGetConfRoom OnGetConfRoom_;
	FuncToCenterOnAddConfRoom OnAddConfRoom_;
	FuncToCenterOnUpdateConfRoom OnUpdateConfRoom_;
	FuncToCenterOnRemoveConfRoom OnRemoveConfRoom_;
	FuncToCenterOnGetConfRoomList OnGetConfRoomList_;
	FuncToCenterOnGetConfCode OnGetConfCode_;
	FuncToCenterOnAddConfCode OnAddConfCode_;
	FuncToCenterOnUpdateConfCode OnUpdateConfCode_;
	FuncToCenterOnRemoveConfCode OnRemoveConfCode_;
	FuncToCenterOnGetConfCodeList OnGetConfCodeList_;
	FuncToCenterOnGetApplyMsg OnGetApplyMsg_;
	FuncToCenterOnAddApplyMsg OnAddApplyMsg_;
	FuncToCenterOnUpdateApplyMsg OnUpdateApplyMsg_;
	FuncToCenterOnRemoveApplyMsg OnRemoveApplyMsg_;
	FuncToCenterOnGetApplyMsgList OnGetApplyMsgList_;
	FuncToCenterOnGetPushMsg OnGetPushMsg_;
	FuncToCenterOnAddPushMsg OnAddPushMsg_;
	FuncToCenterOnUpdatePushMsg OnUpdatePushMsg_;
	FuncToCenterOnRemovePushMsg OnRemovePushMsg_;
	FuncToCenterOnGetPushMsgList OnGetPushMsgList_;
	FuncToCenterOnNoticePushMsg OnNoticePushMsg_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_centerserver_set_callback(
	native_object_t instance,
	struct ConfToCenterCallback* callback);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_SetCenterAddress
// 函数状态: 使用中
// 函数功能：设置中心服务器连接地址
// 触发条件: 主动设置没有回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) const char* address 中心服务器IP地址列表和端口（格式为：192.168.11.201:2803;192.168.11.202:2803）
//				地址为IP+":"+端口 多地址之间分号分隔
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_SetCenterAddress(
	native_object_t instance, 
	const char* address);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetCenterAddress
// 函数状态: 使用中
// 函数功能：获取中心服务器连接地址
// 触发条件: 主动设置没有回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) const char* address 中心服务器IP地址列表和端口（格式为：192.168.11.201:2803;192.168.11.202:2803）
//			 (3) int size 为字符串长度
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetCenterAddress(
	native_object_t instance, 
	char* address, int size);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetAuthCode
// 函数状态: 未启用
// 函数功能：获取认证码
// 触发条件: 存在FuncToCenterOnGetAuthCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetAuthCode(
	native_object_t instance,
	native_object_t param);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RegUserInfo
// 函数状态: 使用中
// 函数功能：注册用户信息
// 触发条件: 存在FuncToCenterOnRegUserInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 用户信息实例（参见lm_create_cs_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RegUserInfo(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_Login
// 函数状态: 使用中
// 函数功能：登录中心服务器
// 触发条件: 存在FuncToCenterOnLogin回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int id_org 企业信息ID
//			 (4) const char* account 企业用户账号
//			 (5) const char* psw 企业用户密码
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_Login(
	native_object_t instance, 
	native_object_t param, 
	int id_org, 
	const char* account,
	const char* psw);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_Logout
// 函数状态: 使用中
// 函数功能：退出中心连接
// 触发条件: 存在FuncToCenterOnLogout回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_Logout(
	native_object_t instance, 
	native_object_t param);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_PrepareLoginConf
// 函数状态: 使用中
// 函数功能：准备登录会议，获取登录参数
// 触发条件: 存在FuncToCenterOnPrepareLoginConf回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int id_conf 会议室ID
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_PrepareLoginConf(
	native_object_t instance, 
	native_object_t param,
	int id_conf);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetRealConf
// 函数状态: 使用中
// 函数功能：获取指定会议室实时信息（如会议室人数）
// 触发条件: 存在FuncToCenterOnGetRealConf回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int* listConfID 获取实时会议信息的会议室ID数组
//			 (4) int count 获取实时会议信息的会议室ID个数
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetRealConf(
	native_object_t instance, 
	native_object_t param, 
	int* listConfID, int count);

//-------------------------------------------------------------------
// 特别说明：一下用户信息是个人用户信息，企业用户信息是org_user的接口
//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetUserInfo
// 函数状态: 使用中
// 函数功能：获取用户信息
// 触发条件: 存在FuncToCenterOnGetUserInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) const char* strAccount 用户账号
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetUserInfo(
	native_object_t instance, 
	native_object_t param, 
	const char* strAccount);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdateUserInfo
// 函数状态: 使用中
// 函数功能：修改用户信息
// 触发条件: 存在FuncToCenterOnUpdateUserInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 用户信息（参见lm_create_cs_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateUserInfo(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RemoveUserInfo
// 函数状态: 使用中
// 函数功能：删除用户信息
// 触发条件: 存在FuncToCenterOnRemoveUserInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 用户信息（参见lm_create_cs_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveUserInfo(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetUserInfoList
// 函数状态: 使用中
// 函数功能：获取用户信息列表
// 触发条件: 存在FuncToCenterOnGetUserInfoList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 用户信息作为条件，不是设置任何信息就是查询所有（参见lm_create_cs_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetUserInfoList(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetOrgInfo
// 函数状态: 使用中
// 函数功能：获取用户信息列表
// 触发条件: 存在FuncToCenterOnGetOrgInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int id_org 要获取企业信息的企业ID
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetOrgInfo(
	native_object_t instance, 
	native_object_t param, 
	int id_org);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdateOrgInfo
// 函数状态: 使用中
// 函数功能：修改用户信息列表
// 触发条件: 存在FuncToCenterOnUpdateOrgInfo回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业信息实例（参加lm_create_cs_org_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateOrgInfo(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetOrgInfoList
// 函数状态: 使用中
// 函数功能：获取企业信息列表
// 触发条件: 存在FuncToCenterOnGetOrgInfoList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业信息实例（参加lm_create_cs_org_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetOrgInfoList(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetUserBind
// 函数状态: 暂时用不到
// 函数功能：获取用户绑定信息
// 触发条件: 存在FuncToCenterOnGetUserBind回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业信息实例（参加lm_create_cs_user_bind_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetUserBind(
	native_object_t instance, 
	native_object_t param, 
	const char* strName);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddUserBind(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateUserBind(
	native_object_t instance,
	native_object_t param,
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveUserBind(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetUserBindList(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetOrgUser
// 函数状态: 使用中
// 函数功能：获取企业用户信息
// 触发条件: 存在FuncToCenterOnGetOrgUser回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业用户信息实例（参加lm_create_cs_org_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetOrgUser(
	native_object_t instance, 
	native_object_t param, 
	int id_org, 
	const char* strAccount);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_AddOrgUser
// 函数状态: 使用中
// 函数功能：添加企业用户信息
// 触发条件: 存在FuncToCenterOnAddOrgUser回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业用户信息实例（参加lm_create_cs_org_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddOrgUser(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdateOrgUser
// 函数状态: 使用中
// 函数功能：修改企业用户信息
// 触发条件: 存在FuncToCenterOnUpdateOrgUser回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业用户信息实例（参加lm_create_cs_org_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateOrgUser(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RemoveOrgUser
// 函数状态: 使用中
// 函数功能：删除企业用户信息
// 触发条件: 存在FuncToCenterOnRemoveOrgUser回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业用户信息实例（参加lm_create_cs_org_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveOrgUser(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetOrgUserList
// 函数状态: 使用中
// 函数功能：获取企业用户信息列表
// 触发条件: 存在FuncToCenterOnGetOrgUserList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 企业用户信息条件，不设置为获取所有（参加lm_create_cs_org_user_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetOrgUserList(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetConfRoom
// 函数状态: 使用中
// 函数功能：获取会议室信息
// 触发条件: 存在FuncToCenterOnGetConfRoom回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int id_conf 会议室信息ID（参加lm_create_cs_room_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetConfRoom(
	native_object_t instance, 
	native_object_t param,
	int id_conf);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_AddConfRoom
// 函数状态: 使用中
// 函数功能：添加会议室信息
// 触发条件: 存在FuncToCenterOnAddConfRoom回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议室信息（参加lm_create_cs_room_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddConfRoom(
	native_object_t instance,
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdateConfRoom
// 函数状态: 使用中
// 函数功能：修改会议室信息
// 触发条件: 存在FuncToCenterOnUpdateConfRoom回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议室信息（参加lm_create_cs_room_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateConfRoom(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RemoveConfRoom
// 函数状态: 使用中
// 函数功能：删除会议室信息
// 触发条件: 存在FuncToCenterOnRemoveConfRoom回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议室信息（参加lm_create_cs_room_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveConfRoom(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetConfRoomList
// 函数状态: 使用中
// 函数功能：获取会议室信息列表
// 触发条件: 存在FuncToCenterOnGetConfRoomList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议室信息条件，不设置获取所有会议室（参加lm_create_cs_room_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetConfRoomList(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetConfCode
// 函数状态: 使用中
// 函数功能：获取会议室验证码
// 触发条件: 存在FuncToCenterOnGetConfCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) const char* strCodeid 会议验证码ID（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetConfCode(
	native_object_t instance, 
	native_object_t param, 
	const char* strCodeid);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_AddConfCode
// 函数状态: 使用中
// 函数功能：添加会议室验证码
// 触发条件: 存在FuncToCenterOnAddConfCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议验证码（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddConfCode(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdateConfCode
// 函数状态: 使用中
// 函数功能：修改会议室验证码
// 触发条件: 存在FuncToCenterOnUpdateConfCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议验证码，设置要修改的验证码ID（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateConfCode(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RemoveConfCode
// 函数状态: 使用中
// 函数功能：删除会议室验证码
// 触发条件: 存在FuncToCenterOnRemoveConfCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议验证码信息，设置要删除的验证码ID（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveConfCode(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetConfCodeList
// 函数状态: 使用中
// 函数功能：获取会议室验证码列表
// 触发条件: 存在FuncToCenterOnGetConfCodeList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 会议验证码信息，设置获取条件，不设置获取所有（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetConfCodeList(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetApplyMsg 等ApplyMsg相关5个函数废弃
// 函数状态: 废弃
// 函数功能：获取会议室验证码列表
// 触发条件: 存在FuncToCenterOnGetApplyMsg回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetApplyMsg(
	native_object_t instance, 
	native_object_t param, 
	int id_req);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddApplyMsg(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdateApplyMsg(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemoveApplyMsg(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);

EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetApplyMsgList(
	native_object_t instance,
	native_object_t param, 
	native_object_t info);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetPushMsg
// 函数状态: 使用中
// 函数功能：获取推送消息
// 触发条件: 存在FuncToCenterOnGetPushMsg回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) int msg_id 推送消息ID（参加lm_create_cs_push_msg_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetPushMsg(
	native_object_t instance, 
	native_object_t param, 
	int msg_id);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_AddPushMsg
// 函数状态: 使用中
// 函数功能：添加推送消息
// 触发条件: 存在FuncToCenterOnAddPushMsg回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 推送消息（参加lm_create_cs_push_msg_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_AddPushMsg(
	native_object_t instance,
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_UpdatePushMsg
// 函数状态: 使用中
// 函数功能：修改推送消息
// 触发条件: 存在FuncToCenterOnUpdatePushMsg回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 修改的推送消息（参加lm_create_cs_push_msg_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_UpdatePushMsg(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_RemovePushMsg
// 函数状态: 使用中
// 函数功能：删除推送消息
// 触发条件: 存在FuncToCenterOnRemovePushMsg回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 删除的推送消息，设置推送消息ID即可（参加lm_create_cs_push_msg_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_RemovePushMsg(
	native_object_t instance, 
	native_object_t param, 
	native_object_t info);

//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetPushMsgList
// 函数状态: 使用中
// 函数功能：获取推送消息列表
// 触发条件: 存在FuncToCenterOnGetPushMsgList回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) native_object_t info 获取条件，不设置为获取所有推送消息（参加lm_create_cs_push_msg_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetPushMsgList(
	native_object_t instance, 
	native_object_t param,
	native_object_t info);


//-------------------------------------------------------------------
// 函数名称: lm_conf_centerserver_GetConfCode_with_id
// 函数状态: 使用中
// 函数功能：获取会议室验证码
// 触发条件: 存在FuncToCenterOnGetConfCode回调
// 参数说明: (1) native_object_t instance 通过lm_get_conf_center_server_instance_with_engine产生 (must not be NULL)
//			 (2) native_object_t param 参数集合实例（参见lm_create_cs_param_set_instance）
//			 (3) const char* strCodeid 会议验证码ID（参加lm_create_cs_code_instance）
//-------------------------------------------------------------------
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_centerserver_GetConfCode_with_id(
	native_object_t instance,
	native_object_t param,
	int id_conf);

#endif //__conf_wrapper_centerserver_h__