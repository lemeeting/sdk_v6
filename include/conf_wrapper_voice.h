#ifndef  __conf_wrapper_voice_h__
#define __conf_wrapper_voice_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"


/************************************************************************/
/* 音频模块回调接口															*/
/************************************************************************/

/*
*	到音频服务器连接通道断开,底层会自动重连
*	param result 错误结果
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVoiceOnDisconnectVoiceServer)(
	int result, void* clientdata);

/*
*	音频设备改变回调(热插拔)
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncVoiceOnDeviceChange)(void* clientdata);

struct ConfVoiceCallback {
	void* client_data_;
	FuncVoiceOnDisconnectVoiceServer OnDisconnectVoiceServer_;
	FuncVoiceOnDeviceChange OnDeviceChange_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_voice_set_callback(
	native_object_t instance, struct ConfVoiceCallback* callback);

/*
*	设置本地音频设备编码参数
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param codec_type @see LMVoiceCodecModes
*	param delay_mode @see LMVoiceDelayModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetCodecType(
	native_object_t instance, int codec_type, int delay_mode);

/*
*	得到本地音频设备当前编码参数
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param codec_type @see LMVoiceCodecModes	[IN/OUT](must not be NULL)
*	param delay_mode @see LMVoiceDelayModes	[IN/OUT](must not be NULL)		
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCodecType(
	native_object_t instance, int* codec_type, int* delay_mode);

/*
*	设置编码参数根据输入设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param input_type @see LMVoiceDeviceType
*	param codec_type @see LMVoiceCodecModes
*	param delay_mode @see LMVoiceDelayModes
*	param rate 48k,32k,16k,8k, -1=default
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetCodecTypeWithInputDevice(
	native_object_t instance, int input_type,
	int codec_type, int delay_mode, int rate); 

/*
*	得到当前编码参数根据输入设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param input_type @see LMVoiceDeviceType
*	param codec_type @see LMVoiceCodecModes	[IN/OUT](must not be NULL)
*	param delay_mode @see LMVoiceDelayModes	[IN/OUT](must not be NULL)
*	param rate 48k,32k,16k,8k, -1=default	[IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCodecTypeWithInputDevice(
	native_object_t instance, int input_type,
	int* codec_type, int* delay_mode, int* rate);

/*
*	是否是正确的延时包根据编码类型
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param codec_type @see LMVoiceCodecModes
*	param delay_mode @see LMVoiceDelayModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IsValidDelayMode(
	native_object_t instance, int codec_type, int delay_mode);

/*
*	得到本地音频输入设备的数量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param nums [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetNumOfInputDevices(
	native_object_t instance, int* nums);

/*
*	得到本地音频输出设备的数量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param nums [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetNumOfOutputDevices(
	native_object_t instance, int* nums);

/*
*	得到本地音频输入设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index 设备索引
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetInputDeviceName(
	native_object_t instance, int index, char* name, int name_size, char* guid, int guid_size);

/*
*	得到本地音频输出设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index 设备索引
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetOutputDeviceName(
	native_object_t instance, int index, char* name, int name_size, char* guid, int guid_size);


/*
*	得到本地默认音频输入设备索引
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDefaultInputDeviceIndex(
	native_object_t instance, int* index);

/*
*	得到本地默认音频输出设备索引
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDefaultOutputDeviceIndex(
	native_object_t instance, int* index);

/*
*	得到本地当前选择音频输入设备索引
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCurrentInputDeviceIndex(
	native_object_t instance, int* index);

/*
*	得到本地当前选择音频输出设备索引
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCurrentOutputDeviceIndex(
	native_object_t instance, int* index);

/*
*	得到本地默认音频输入设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDefaultInputDeviceName(
	native_object_t instance, char* name, int name_size, char* guid, int guid_size);

/*
*	得到本地默认音频输出设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDefaultOutputDeviceName(
	native_object_t instance, char* name, int name_size, char* guid, int guid_size);

/*
*	得到本地当前选择音频输入设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCurrentInputDeviceName(
	native_object_t instance, char* name, int name_size, char* guid, int guid_size);

/*
*	得到本地当前选择音频输出设备的名称
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param name [IN/OUT](must not be NULL)
*	param name_size name大小
*	param guid [IN/OUT](must not be NULL)
*	param guid_size guid大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetCurrentOutputDeviceName(
	native_object_t instance, char* name, int name_size, char* guid, int guid_size);

/*
*	选择本地音频输入设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index 设备索引
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetInputDevice(
	native_object_t instance, int index);

/*
*	选择本地音频输出设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param index 设备索引
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetOutputDevice(
	native_object_t instance, int index);

/*
*	设置本地当前选择音频输入音量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param volume 0-255
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetInputVolume(
	native_object_t instance, unsigned int volume);

/*
*	得到本地当前选择音频输入音量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param volume 0-255 [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetInputVolume(
	native_object_t instance, unsigned int* volume);

/*
*	设置本地当前选择音频输出音量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param volume 0-255
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetOutputVolume(
	native_object_t instance, unsigned int volume);

/*
*	得到本地当前选择音频输出音量
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param volume 0-255 [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetOutputVolume(
	native_object_t instance, unsigned int* volume);

/*
*	得到本地参会者音频输入信号
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param level 0-9 [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetInputVoiceLevel(
	native_object_t instance, unsigned int* level);

/*
*	得到远程与会者音频输出信号
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param level 0-9 [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetOutputVoiceLevel(
	native_object_t instance, const char* account, unsigned int* level);

/*
*	得到扩展节点音频输入信号,如播放文件或外部音频流
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param 扩展节点
*	param level 0-9 [IN/OUT](must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetOutputVoiceLevelWithNode(
	native_object_t instance, int node, unsigned int* level);

/*
*	对于移动设备,内置话筒与外置话筒切换
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-外置,0-内置
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetLoudspeakerStatus(
	native_object_t instance, int enabled);

/*
*	设置回音消除模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止
*	param mode @see LMEcModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetEcStatus(
	native_object_t instance, int enabled, int mode);

/*
*	设置音频降噪模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止
*	param mode @see LMNsModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetNsStatus(
	native_object_t instance, int enabled, int mode);

/*
*	设置音频自动增益模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止
*	param mode @see LMAgcModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetAgcStatus(
	native_object_t instance, int enabled, int mode);

/*
*	设置移动设备回音消除级别
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param mode @see LMAecmModes
*	param enableCNG 1-使用CNG,0-禁止 comfort noise generator,舒适噪音生成
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetAecmMode(
	native_object_t instance, int mode, int enableCNG);

/*
*	设置音频静音检测模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止
*	param mode @see LMVadModes
*	param disableDTX 禁止连续发送
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetVADStatus(
	native_object_t instance, int enable, int mode, int disableDTX);

/*
*	得到移动设备话筒状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-外置,0-内置
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetLoudspeakerStatus(
	native_object_t instance, int* enabled);
/*
*	得到回音消除状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	param mode @see LMEcModes	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetEcStatus(
	native_object_t instance, int* enabled, int* mode);
/*
*	得到音频降噪状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	param mode @see LMNsModes	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetNsStatus(
	native_object_t instance, int* enabled, int* mode);

/*
*	得到音频自动增益状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	param mode @see LMAgcModes	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetAgcStatus(
	native_object_t instance, int* enabled, int* mode);

/*
*	得到移动设备回音消除状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param mode @see LMAecmModes		[IN/OUT] (must not be NULL)
*	param enableCNG 1-使用CNG,0-禁止	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetAecmMode(
	native_object_t instance, int* mode, int* enabledCNG);

/*
*	得到音频静音检测状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	param mode @see LMVadModes	[IN/OUT] (must not be NULL)
*	param disableDTX 禁止连续发送	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetVADStatus(
	native_object_t instance, int* enabled, int* mode, int* disabledDTX);

/*
*	设置音频时钟补偿
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_EnableDriftCompensation(
	native_object_t instance, int enable);
/*
*	得到音频时钟补偿状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enabled 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DriftCompensationEnabled(
	native_object_t instance, int *enable);

/*
*	回音消除延时参数
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param offset 0-200ms
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetDelayOffsetMs(
	native_object_t instance, int offset);

/*
*	得到回音消除延时参数
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param offset [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DelayOffsetMs(
	native_object_t instance, int *offset);

/*
*	设置高通滤波
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_EnableHighPassFilter(
	native_object_t instance, int enable);
/*
*	得到高通滤波状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止	[IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IsHighPassFilterEnabled(
	native_object_t instance, int *enable);

/*
*	禁止或打开所有输入设备,包括本地与扩展设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_EnableInput(
	native_object_t instance, int enabled);

/*
*	禁止或打开所有输出设备,包括本地与扩展设备
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_EnableOutput(
	native_object_t instance, int enabled);

/*
*	得到音频输入设备状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetInputState(
	native_object_t instance, int* enabled);

/*
*	得到音频输出设备状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetOutputState(
	native_object_t instance, int* enabled);

/*
*	禁止或打开录制回放音频输入
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisablePlaybackInput(
	native_object_t instance, int disabled);

/*
*	禁止或打开IPCamera音频输入
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableIPCameraInput(
	native_object_t instance, int disabled);

/*
*	禁止或打开媒体播放音频输入
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableMediaPlayerInput(
	native_object_t instance, int disabled);

/*
*	禁止或打开外部音频输入
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableExternalDeviceInput(
	native_object_t instance, int disabled);

/*
*	禁止或打开录制回放音频本地播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisablePlaybackInputLocalOutput(
	native_object_t instance, int disabled);

/*
*	禁止或打开IPCamera音频本地播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableIPCameraInputLocalOutput(
	native_object_t instance, int disabled);

/*
*	禁止或打开媒体播放音频本地播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableMediaPlayerInputLocalOutput(
	native_object_t instance, int disabled);

/*
*	禁止或打开外部扩展音频本地播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_DisableExternalDeviceInputLocalOutput(
	native_object_t instance, int disabled);


/*
*	得到录制回放音频输入状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisablePlaybackInputState(
	native_object_t instance, int* disabled);

/*
*	得到录制IPCamera音频输入状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableIPCameraInputState(
	native_object_t instance, int* disabled);

/*
*	得到录制媒体播放音频输入状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableMediaPlayerInputState(
	native_object_t instance, int* disabled);

/*
*	得到录制外部扩展音频输入状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableExternalDeviceInputState(
	native_object_t instance, int* disabled);

/*
*	得到录制回放音频本地播放状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisablePlaybackInputLocalOutputState(
	native_object_t instance, int* disabled);

/*
*	得到IPCamera音频本地播放状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableIPCameraInputLocalOutputState(
	native_object_t instance, int* disabled);

/*
*	得到媒体播放音频本地播放状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableMediaPlayerInputLocalOutputState(
	native_object_t instance, int* disabled);

/*
*	得到外部扩展音频本地播放状态
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param enable 1-使用,0-禁止 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetDisableExternalDeviceInputLocalOutputState(
	native_object_t instance, int* disabled);

/*
*	播放本地音频文件
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param fileName 文件
*	param loop 
*	param format @see LMVideoFileFormats
*	param volumeScaling 放大或缩小音频信号
*	param startPointMs 开始位置
*	param startPointMs 结束位置
*	param 产生的播放节点 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartPlayingFileLocallyFromFile(
	native_object_t instance, 
	const char* fileName,
	int loop,
	int format,
	float volumeScaling,
	int startPointMs,
	int stopPointMs,
	int* node);

/*
*	播放音频流
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param stream 音频流
*	param stream_len bytes of stream
*	param format @see LMVideoFileFormats
*	param volumeScaling 放大或缩小音频信号
*	param startPointMs 开始位置
*	param startPointMs 结束位置
*	param 产生的播放节点 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartPlayingFileLocallyFromStream(
	native_object_t instance, 
	const void* stream, int stream_len,
	int format,
	float volumeScaling,
	int startPointMs, int stopPointMs, int* node);

/*
*	停止播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param 播放节点
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StopPlayingFileLocallyWithNode(
	native_object_t instance, int node);
/*
*	节点是否在播放
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param 播放节点
*	return 0-No, 1-Yes
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IsPlayingFileLocallyWithNode(
	native_object_t instance, int node);

/*
*	把音频文件引入MIC
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param fileName 文件
*	param loop
*	param mixWithMicrophone 是否同MIC输入混音 1-Yes, 0-No
*	param format @see LMVideoFileFormats
*	param volumeScaling 放大或缩小音频信号
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartPlayingFileAsMicrophoneFromFile(
	native_object_t instance, 
	const char* fileName,
	int loop ,
	int mixWithMicrophone,
	int format,
	float volumeScaling);

/*
*	把音频流引入MIC
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param stream 音频流
*	param stream_len bytes of stream
*	param mixWithMicrophone 是否同MIC输入混音 1-Yes, 0-No
*	param format @see LMVideoFileFormats
*	param volumeScaling 放大或缩小音频信号
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartPlayingFileAsMicrophoneFromStream(
	native_object_t instance, 
	const void* stream, int stream_len,
	int mixWithMicrophone,
	int format,
	float volumeScaling);

/*
*	停止音频流引入MIC
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StopPlayingFileAsMicrophone(
	native_object_t instance);

/*
*	是否有外部音频引入MIC
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	return 0-No, 1-Yes
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IsPlayingFileAsMicrophone(
	native_object_t instance);

/*
*	录制与会者音频到文件
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param fileName 保存文件名
*	param codecint @see LMVoiceCodecModes
*	param maxSizeBytes 最大值 -1无限
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingAccountPlayoutToFile(
	native_object_t instance, 
	const char* account,
	const char* fileName,
	int codecint,
	int maxSizeBytes);

/*
*	录制与会者音频到缓冲
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	param stream 缓冲区
*	param stream_len 缓冲区大小 
*	param codecint @see LMVoiceCodecModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingAccountPlayoutToStream(
	native_object_t instance, 
	const char* account,
	void* stream, int stream_len,
	int codecint);

/*
*	停止录制与会者音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param account 与会者
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StopRecordingAccountPlayout(
	native_object_t instance, const char* account);

/*
*	录制与会者音频到文件
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param fileName 保存文件名
*	param codecint @see LMVoiceCodecModes
*	param maxSizeBytes 最大值 -1无限
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingAllPlayoutToFile(
	native_object_t instance, 
	const char* fileName,
	int codec,
	int maxSizeBytes);

/*
*	录制所有与会者音频到缓冲
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param stream 缓冲区
*	param stream_len 缓冲区大小
*	param codecint @see LMVoiceCodecModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingAllPlayoutToStream(
	native_object_t instance, 
	void* stream, int stream_len, int codec);

/*
*	停止录制所有与会者音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StopRecordingAllPlayout(
	native_object_t instance);

/*
*	录制MIC到文件
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param fileName 保存文件名
*	param codecint @see LMVoiceCodecModes
*	param maxSizeBytes 最大值 -1无限
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingMicrophoneToFile(
	native_object_t instance, const char* fileName, int codec, int maxSizeBytes);

/*
*	录制MIC到缓冲
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param stream 缓冲区  可以为空缓冲
*	param stream_len 缓冲区大小
*	param codecint @see LMVoiceCodecModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StartRecordingMicrophoneToStream(
	native_object_t instance, void* stream, int stream_len, int codec);

/*
*	停止录制MIC
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_StopRecordingMicrophone(
	native_object_t instance);

/*
*	加入IPCamera音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param info @see LMIPCameraInfo
*	param camera_channel 通道
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_AddIPCamera(
	native_object_t instance, const struct LMIPCameraInfo* info, int camera_channel);

/*
*	移除IPCamera音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	param camera_channel 通道
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_RemoveIPCamera(
	native_object_t instance, int identity, int camera_channel);

/*
*	加入一路扩展原始音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param sampleRate 采样率
*	param channel 通道数
*	param identity 标识 0-65535
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_AddExternalRAWDataInputDevice(
	native_object_t instance, int sampleRate, int channel, int identity);
/*
*	移除一路扩展原始音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_RemoveExternalRAWDataInputDevice(
	native_object_t instance, int identity);

/*
*	处理扩展原始音频数据
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	param data 音频数据
*	param data_len bytes of data
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IncomingExternalInputRAWData(
	native_object_t instance, int identity, const void* data, int data_len);

/*
*	加入一路RTP包音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_AddExternalRTPDataInputDevice(
	native_object_t instance, int identity);
/*
*	移除一路RTP包音频
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_RemoveExternalRTPDataInputDevice(
	native_object_t instance, int identity);

/*
*	处理RTP音频数据
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param identity 标识 0-65535
*	param data 音频数据
*	param data_len bytes of data
*	paran delay_mode @see LMVoiceDelayModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_IncomingExternalInputRTPData(
	native_object_t instance, int identity, const void* data, int data_len, int delay_mode);

/*
*	设置音频包网络接收模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param mode @see LMVoiceIOModes
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_SetVoiceIOMode(
	native_object_t instance, int mode);

/*
*	得到音频包网络接收模式
*	param instance 通过lm_get_conf_voice_instance_with_engine产生 (must not be NULL)
*	param mode @see LMVoiceIOModes [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_voice_GetVoiceIOMode(
	native_object_t instance, int* mode);


#endif //__conf_wrapper_voice_h__