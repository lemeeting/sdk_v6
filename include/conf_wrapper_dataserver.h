#ifndef __conf_wrapper_dataserver_h__
#define __conf_wrapper_dataserver_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 数据服务器模块回调接口                                                    */
/************************************************************************/
/*
*	ping 数据服务器 @see lm_conf_dataserver_PingDataServer
*	param task_id 由lm_conf_dataserver_PingDataServer产生的任务ID
*	param context 由lm_conf_dataserver_PingDataServer传入
*	param error -1错误,0正确
*	param result ping结果
*	param clientdata 应用层私有参数,通过ConfDataServerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncDataServerOnPingResult)(
	uint32_t task_id, uint64_t context, int error, 
	const struct LMPingResult* result, void* clientdata);

struct ConfDataServerCallback {
	void* client_data_;
	FuncDataServerOnPingResult OnPingResult_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_dataserver_set_callback(
	native_object_t instance,
	struct ConfDataServerCallback* callback);

/*
*	得到数据服务器的个数
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param count [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_get_count(
	native_object_t instance,
	int* count);

/*
*	得到所有数据服务器信息
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param data_servers @see lm_create_as_data_server_info_instance [IN/OUT] (must not be NULL)
*	param count IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_get_dataserves(
	native_object_t instance,
	struct LMDataServerInfo* data_servers, 
	int* count);

/*
*	得到当前所连接的数据服务器
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param module @see LMDataServerModule
*	param data_servers @see lm_create_as_data_server_info_instance [IN/OUT] (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_GetCurrentConnectDataServer(
	native_object_t instance, 
	int module, 
	struct LMDataServerInfo* data_server);

/*
*	PING数据服务器
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param address 服务器地址(must not be NULL)
*	param port 服务器端口
*	param 结果回调时间
*	param is_use_udp 1-udp, 0-tcp
*	param context 上下文
*	parma task_id 任务ID [IN/OUT](must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_PingDataServer(
	native_object_t instance, 
	const char* address,
	int port, 
	int64_t callback_time,
	int is_use_udp,
	uint64_t context, 
	int* task_id);

/*
*	停止PING数据服务器
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	parma task_id 由lm_conf_dataserver_PingDataServer产生
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_StopPingDataServer(
	native_object_t instance, 
	int task_id);

/*
*	切换数据服务器,可以根据ping结果,手动切换服务器
*	param instance 通过lm_get_conf_data_server_instance_with_engine产生 (must not be NULL)
*	param module @see LMDataServerModule
*	param address 新的服务器地址(must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_dataserver_SwitchDataServer(
	native_object_t instance, 
	int module, 
	const char* address);

#endif //__conf_wrapper_dataserver_h__