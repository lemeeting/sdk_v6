#ifndef __CONF_WRAPPER_DEFINES_H__
#define __CONF_WRAPPER_DEFINES_H__

#ifdef WIN32
#define CONFWRAPPER_API __declspec(dllexport)
#ifndef CALLBACK
#define CALLBACK __stdcall
#endif
#else
#define CONFWRAPPER_API
#ifndef CALLBACK
#define CALLBACK
#endif
#endif

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C extern
#endif
#endif

typedef void* native_object_t;


#endif //__CONF_WRAPPER_DEFINES_H__