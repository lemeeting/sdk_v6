#ifndef __conf_wrapper_record_h__
#define __conf_wrapper_record_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 录制模块回调接口	,暂时只支持windows系统  									*/
/************************************************************************/

/**
*	调用lm_conf_record_StartLocalRecord后,由于底层需要做费时的初时化工作,因此
*	lm_conf_record_StartLocalRecord是异步调用,在这段时间内,UI层应保持禁止操作状态,
*	如:可以用模式对话框等待控制.
*	param clientdata 应用层私有参数,通过ConfRecordCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncRecordOnStartLocalRecordReady)(void* clientdata);

struct ConfRecordCallback {
	void* client_data_;
	FuncRecordOnStartLocalRecordReady OnStartLocalRecordReady_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_record_set_callback(
	native_object_t instance,
	struct ConfRecordCallback* callback);

/*
*	开始会议模块录制
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	param file_name 保存文件名,emv格式
*	param accounts 录制的与会者,如果count=0,录制所有与会者.
*	param count accounts的数量
*	param flags @see LMConferenceRecordFlags
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_StartLocalRecord(
	native_object_t instance,
	const char* file_name,
	const char** accounts,
	int count,
	int flags);

/*
*	停止会议模块录制
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_StopLocalRecord(
	native_object_t instance);

/*
*	开始桌面录制
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	param file_name 保存文件名,e.g flv, mkv, mp4, avi
*	param format @see LMScreenRecordFormats
*	param zoom @see LMScreenRecordZoom
*	param frame_rate 屏幕采集的帧数 @see lm_conf_base_set_screen_copy_fps
*	param qp 屏幕编码质量 @see LMScreenRecordQP
*	param flags @see LMScreenRecordVoiceFlags
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_StartScreenRecord(
	native_object_t instance,
	const char* file_name, 
	int format,
	int zoom,
	float frame_rate,
	int qp,
	int flags);

/*
*	停止屏幕录制
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_StopScreenRecord(
	native_object_t instance);

/*
*	停止录制
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_StopRecord(
	native_object_t instance);

/*
*	得到当前录制类型
*	param instance 通过lm_get_conf_record_instance_with_engine产生 (must not be NULL)
*	return @see LMConfRecordType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_record_CurrentRecordType(
	native_object_t instance);

#endif //__conf_wrapper_record_h__