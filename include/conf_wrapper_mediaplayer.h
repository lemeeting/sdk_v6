#ifndef __conf_wrapper_mediaplayer_h__
#define __conf_wrapper_mediaplayer_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 媒体播放模块回调接口,暂时只支持windows系统                                 */
/************************************************************************/

/**
*	调用lm_conf_mediaplay_Open的回调
*	param error 0-success other failure
*	param identity 内部产生的标识0-65535
*	param clientdata 应用层私有参数,通过ConfMediaPlayerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncMediaPlayOnMediaPlayOpen)(
	int error, int identity, void* clientdata);


/**
*	play发生错误
*	param error 0-success other failure
*	param identity 内部产生的标识0-65535
*	param clientdata 应用层私有参数,通过ConfMediaPlayerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncMediaPlayOnMediaPlayError)(
	int error, int identity, void* clientdata);

/**
*	播放结束的回调
*	param identity 内部产生的标识0-65535
*	param clientdata 应用层私有参数,通过ConfMediaPlayerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncMediaPlayOnMediaPlayComplete)(
	int identity, void* clientdata);

/**
*	播放媒体的总时间发生变化的回调,网络流媒体
*	param identity 内部产生的标识0-65535
*	param duration 变化后的总时间
*	param clientdata 应用层私有参数,通过ConfMediaPlayerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncMediaPlayOnMediaPlayDurationChanged)(
	int identity, int64_t duration, void* clientdata);

/**
*	播放时间发生变化的回调
*	param identity 内部产生的标识0-65535
*	param 当前播放位置
*	param clientdata 应用层私有参数,通过ConfMediaPlayerCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncMediaPlayOnMediaPlayPositionChanged)(
	int identity, int64_t position, void* clientdata);

struct ConfMediaPlayerCallback {
	void* client_data_;
	FuncMediaPlayOnMediaPlayOpen OnMediaPlayOpen_;
	FuncMediaPlayOnMediaPlayError OnMediaPlayError_;
	FuncMediaPlayOnMediaPlayComplete OnMediaPlayComplete_;
	FuncMediaPlayOnMediaPlayDurationChanged OnMediaPlayDurationChanged_;
	FuncMediaPlayOnMediaPlayPositionChanged OnMediaPlayPositionChanged_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_mediaplay_set_callback(
	native_object_t instance,
	struct ConfMediaPlayerCallback* callback);

/*
*	打开一个本地媒体文件
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param filename 媒体文件 e.g mp4 format (must not be NULL)
*	param identity 内部产生的标识 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Open(
	native_object_t instance, const char* filename, int* identity);

/*
*	打开网络流媒体
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param url  (must not be NULL)
*	param identity 内部产生的标识 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Open_url(
	native_object_t instance, const char* url, int* identity);

/*
*	关闭已经打开的媒体文件
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Close(
	native_object_t instance, int identity);

/*
*	播放已经打开或暂停播放的媒体文件
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Play(
	native_object_t instance, int identity, int timeoutS);

/*
*	停止正在播放媒体文件
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Stop(
	native_object_t instance, int identity);

/*
*	暂停正在播放媒体文件
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_Pause(
	native_object_t instance, int identity);

/*
*	是否播放中
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param running [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_PlayStatus(
	native_object_t instance, int identity, int* running);

/*
*	是否停止播放中
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param stopped [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_StopStatus(
	native_object_t instance, int identity, int* stopped);

/*
*	是否暂停播放中
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param paused [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_PauseStatus(
	native_object_t instance, int identity, int* paused);

/*
*	得到当前播放位置
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param positionMS [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_GetCurrentPosition(
	native_object_t instance, int identity, int64_t* positionMS);

/*
*	跳转
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param positionMS
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_SetCurrentPosition(
	native_object_t instance, int identity, int64_t positionMS);

/*
*	得到播放总长度
*	param instance 通过lm_get_conf_mediaplayer_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	param durationMS [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_mediaplay_GetDuration(
	native_object_t instance, int identity, int64_t* durationMS);

#endif //__conf_wrapper_mediaplayer_h__