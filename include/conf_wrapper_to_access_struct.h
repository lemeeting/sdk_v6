#ifndef __conf_wrapper_to_access_struct_h__
#define __conf_wrapper_to_access_struct_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"


/****************************************************************************/
/*								会议人员信息									*/
/****************************************************************************/
/*
*	创建实例
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_as_attendee_instance(
	native_object_t* instance);

/*
*	删除实例
*	param instance
*	
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_as_attendee_instance(
	native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL)
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_as_attendee_instance(
	native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取类型
*	param instance (must not be NULL)
*	return int (暂时无定义)
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_type(
	native_object_t instance);

/*
*	获取login类型
*	param instance (must not be NULL)
*	return @see enum LMConferenceClientType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_login_type(
	native_object_t instance);

/*
*	获取状态
*	param instance (must not be NULL)
*	return @see enum LMAttendeeOpsFlags
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_op_status(
	native_object_t instance);

/*
*	获取管理员信息
*	param instance (must not be NULL)
*	return @see enum LMConferenceAdminModes
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_admin_privilege(
	native_object_t instance);

/*
*	获取视频设备数量,视频设备包括本地摄像头,网络摄像头,桌面虚拟摄像头,回放虚拟摄像头,媒体播放虚拟摄像头,
*	@see enum LMCameraType
*	param instance (must not be NULL)
*	return 视频设备数量
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_video_device_nums(
	native_object_t instance);

/*
*	获取所有视频设备ID
*	param instance (must not be NULL)
*	param id_dvices 由应用层分配空间 [IN/OUT] (must not be NULL)
*	param nums 空间大小 [IN/OUT] (must not be NULL)
*	return -1错误,0正确
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_enum_video_devices(
	native_object_t instance, int* id_dvices, int* nums);

/*
*	获取指定视频设备类型
*	param instance (must not be NULL)
*	param id_device 设备ID
*	return @see enum LMCameraType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_video_device_type(
	native_object_t instance, int id_device);

/*
*	获取指定视频设备视频类型
*	param instance (must not be NULL)
*	param id_device 设备ID
*	return @see enum LMCameraSubType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_video_device_sub_type(
	native_object_t instance, int id_device);

/*
*	获取指定视频设备状态
*	param instance (must not be NULL)
*	param id_device 设备ID
*	return 1 = enabled, 0 = disabled
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_video_device_state(
	native_object_t instance, int id_device);

/*
*	获取指定视频设备可以用来显示的名称
*	param instance (must not be NULL)
*	param id_device 设备ID
*	return 名称
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_attendee_video_device_show_name(
	native_object_t instance, int id_device);

/*
*	获取视频设备是否有效
*	param instance (must not be NULL)
*	param id_device 设备ID
*	return 1 = yes, 0 = no
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_attendee_is_valid_video_device(
	native_object_t instance, int id_device);

/*
*	获取主视频设备
*	param instance (must not be NULL)
*	return -1为无效值,一般是没有视频设备,否则为主视频设备ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_main_video_device_id(
	native_object_t instance);

/*
*	是否禁止所有视频
*	param instance (must not be NULL)
*	return 1禁止所有视频,0共享所有视频
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_attendee_is_forbidden_video_device(
	native_object_t instance);

/*
*	获取音频设备数量
*	param instance (must not be NULL)
*	return 音频设备数量
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_audio_device_nums(
	native_object_t instance);

/*
*	获取用户账号
*	param instance (must not be NULL)
*	return 用户账号
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_attendee_account(
	native_object_t instance);

/*
*	获取用户昵称
*	param instance (must not be NULL)
*	return 用户昵称
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_attendee_name(
	native_object_t instance);

/*
*	获取企业ID
*	param instance (must not be NULL)
*	return id
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_enterprise_id(
	native_object_t instance);

/*
*	获取集群组接入服务器id
*	param instance (must not be NULL)
*	return id
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_server_id(
	native_object_t instance);

/*
*	获取所参加会议室id
*	param instance (must not be NULL)
*	return id
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_attendee_conference_id(
	native_object_t instance);

/*
*	获取用户运行系统的网卡地址
*	param instance (must not be NULL)
*	return 地址
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_attendee_mac_address(
	native_object_t instance);

/*
*	获取用户签到时间
*	param instance (must not be NULL)
*	return 时间(windows FileTime us) 
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_attendee_signin_time(
	native_object_t instance);



/****************************************************************************/
/*								会议室信息									*/
/****************************************************************************/
/**	创建实例
*	param instance 
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_as_conference_attribute_instance(
	native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_as_conference_attribute_instance(
	native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL) 
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_as_conference_attribute_instance(
	native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取会议室ID
*	param instance (must not be NULL)
*	return 会议室ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_conference_id(
	native_object_t instance);

/*
*	获取企业ID
*	param instance (must not be NULL)
*	return 企业ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_enterprise_id(
	native_object_t instance);

/*
*	获取会议室类型
*	param instance (must not be NULL)
*	return @see enum LMConferenceType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_type(
	native_object_t instance);

/*
*	获取会议室最大人数
*	param instance (must not be NULL)
*	return 人数
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_max_attendees(
	native_object_t instance);

/*
*	获取会议室最大主席数
*	param instance (must not be NULL)
*	return 主席数
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_max_speakers(
	native_object_t instance);

/*
*	获取会议室开始时间　
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_conference_attribute_start_time(
	native_object_t instance);

/*
*	获取会议室结束时间　
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_conference_attribute_end_time(
	native_object_t instance);

/*
*	获取会议室创建时间　
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_conference_attribute_create_time(
	native_object_t instance);

/*
*	获取会议室编辑时间　
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_conference_attribute_modify_time(
	native_object_t instance);

/*
*	获取会议室名称
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_name(
	native_object_t instance);

/*
*	获取会议室密码
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_conference_password(
	native_object_t instance);

/*
*	获取会议室管理员认证密码
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_manager_password(
	native_object_t instance);

/*
*	获取会议室创建者
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_creator(
	native_object_t instance);

/*
*	获取会议室信息最后一次修改者
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_mender(
	native_object_t instance);

/*
*	获取会议室扩展信息
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_extend(
	native_object_t instance);

/*
*	获取会议室描述
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_desc(
	native_object_t instance);

/*
*	获取会议室认证码
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_attribute_code_id(
	native_object_t instance);

/*
*	获取会议室TAG
*	param instance (must not be NULL)
*	return int
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_conference_attribute_tag(
	native_object_t instance);

/*
*	会议室模式
*	param instance (must not be NULL)
*	return 1-自由发言模式, 0-集中管理模式
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_free(
	native_object_t instance);

/*
*	会议室锁定
*	param instance (must not be NULL)
*	return 1-锁定, 0-无锁定
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_lock(
	native_object_t instance);

/*
*	会议室文字消息
*	param instance (must not be NULL)
*	return 1-禁止, 0-不禁止
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_disable_text(
	native_object_t instance);

/*
*	会议室录制
*	param instance (must not be NULL)
*	return 1-禁止, 0-不禁止
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_disable_record(
	native_object_t instance);

/*
*	会议室共享视频
*	param instance (must not be NULL)
*	return 1-禁止, 0-不禁止
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_disable_brower_video(
	native_object_t instance);

/*
*	是否为管理员
*	param instance (must not be NULL)
*	param account 用户账号(must not be NULL)
*	return 1-管理员, 0-非管理员
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_default_admin(
	native_object_t instance, const char* account);

/*
*	是否为默认与会者
*	param instance (must not be NULL)
*	param account 用户账号(must not be NULL)
*	return 1-默认, 0-非默认
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_default_attendee(
	native_object_t instance, const char* account);

/*
*	会议室是否需要密码进入
*	param instance (must not be NULL)
*	return 1-需要, 0-不需要
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_attribute_is_need_password(
	native_object_t instance);

/************************************************************************/
/*			会议实时信息                                                  */
/************************************************************************/
/**	创建实例
*	param instance 
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_as_real_time_conference_info_instance(
	native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_as_real_time_conference_info_instance(
	native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL) 
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_as_real_time_conference_info_instance(
	native_object_t srcinstance, native_object_t dstinstance);

/*
*	实时会议信息标记
*	param instance (must not be NULL)
*	return @see enum LMConerenceFlags
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_real_time_conference_info_flags(
	native_object_t instance);

/*
*	会议室ID
*	param instance (must not be NULL)
*	return ID
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_real_time_conference_info_conference_id(
	native_object_t instance);

/*
*	会议室当前人数
*	param instance (must not be NULL)
*	return 人数
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_real_time_conference_info_online_attendees(
	native_object_t instance);

/*
*	会议室签到类型
*	param instance (must not be NULL)
*	return @see enum LMConferenceSigninType
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_as_real_time_conference_info_signin_type(
	native_object_t instance);

/*
*	会议室签到发起时间
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_real_time_conference_info_lanunch_signin_time(
	native_object_t instance);

/*
*	会议室签到停止时间
*	param instance (must not be NULL)
*	return 时间
*/
EXTERN_C CONFWRAPPER_API int64_t CALLBACK lm_get_as_real_time_conference_info_stop_signin_time(
	native_object_t instance);

/*
*	会议室是否处于签到状态
*	param instance (must not be NULL)
*	return 1-签到状态, 0-非签到状态
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_real_time_conference_info_is_lanunch_signin(
	native_object_t instance);

/*
*	会议室是否已经停止签到
*	param instance (must not be NULL)
*	return 1-停止, 0-没停止
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_real_time_conference_info_is_stop_signin(
	native_object_t instance);


/************************************************************************/
/*				会议室同步信息                                            */
/************************************************************************/
/**	创建实例
*	param instance 
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_as_conference_sync_info_instance(
	native_object_t* instance);

/*
*	删除实例
*	param instance
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_as_conference_sync_info_instance(
	native_object_t instance);

/*
*	Copy实例
*	param srcinstance (must not be NULL)
*	param dstinstance (must not be NULL) 
*
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_copy_as_conference_sync_info_instance(
	native_object_t srcinstance, native_object_t dstinstance);

/*
*	获取会议室当前同步者
*	param instance (must not be NULL)
*	return ""意为没有同步者
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_sync_info_syncer(
	native_object_t instance);
/*
*	获取会议室同步数据,由应用层自定议格式
*	param instance (must not be NULL)
*	return 字符串
*/
EXTERN_C CONFWRAPPER_API const char* CALLBACK lm_get_as_conference_sync_info_sync_data(
	native_object_t instance);

/*
*	会议室是否处于同步状态
*	param instance (must not be NULL)
*	return 1-同步,0-非同步
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_as_conference_sync_info_is_sync(
	native_object_t instance);


#endif //__conf_wrapper_to_access_struct_h__