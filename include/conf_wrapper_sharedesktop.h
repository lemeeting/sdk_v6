#ifndef __conf_wrapper_sharedesktop_h__
#define __conf_wrapper_sharedesktop_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"


/************************************************************************/
/* 屏幕共享模块回调接口,暂时只支持windows系统                                  */
/************************************************************************/
/*
*	到屏幕共享服务器连接通道断开,底层会自动重连
*	param result 错误结果
*	param clientdata 应用层私有参数,通过ConfShareDesktopCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncShareDesktopOnDisconnectShareServer)(int result, void* clientdata);

struct ConfShareDesktopCallback {
	void* client_data_;
	FuncShareDesktopOnDisconnectShareServer OnDisconnectShareServer_; 
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param callback 
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_sharedesktop_set_callback(
	native_object_t instance, struct ConfShareDesktopCallback* callback);

/*
*	本地开始共享
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_SelfStartShare(
	native_object_t instance);

/*
*	本地停止共享
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_SelfStopShare(
	native_object_t instance);


/*
*	产生预览远程桌面窗口
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param strSharer 桌面共享者
*	param hParent 窗口句柄(window HWND)
*	crBkg 窗口底色 (window COLORREF)
*	hWnd 子窗口句柄(window HWND)	[IN/OUT]
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_CreateView(
	native_object_t instance, const char* strSharer,
	void* hParent, int crBkg, void** hWnd);

/*
*	销毁预览远程桌面窗口
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param strSharer 桌面共享者
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_DestroyView(
	native_object_t instance, const char* strSharer);

/*
*	设置预览远程桌面窗口显示模式
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param strSharer 桌面共享者
*	param nMode @see LMShareDesktopDisplayModes
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_SetDisplayMode(
	native_object_t instance, const char* strSharer, int nMode);

/*
*	得到本地共享桌面的分辨率
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param nWidth 宽度 [IN/OUT](must not be NULL)
*	param nHeight 高度 [IN/OUT](must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_GetScreenResolution(
	native_object_t instance, int* nWidth, int* nHeight);

/*
*	得到远程共享桌面的分辨率
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param nWidth 宽度 [IN/OUT](must not be NULL)
*	param nHeight 高度 [IN/OUT](must not be NULL)
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_GetRemoteResolution(
	native_object_t instance, int* nWidth, int* nHeight);

/*
*	设置共享桌面发送的分辨率
*	param instance 通过lm_get_conf_sharedesktop_instance_with_engine产生 (must not be NULL)
*	param nWidth 宽度
*	param nHeight 高度
*	return 0 success, other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_sharedesktop_SetRemoteResolution(
	native_object_t instance, int nWidth, int nHeight);

#endif //__conf_wrapper_sharedesktop_h__