#ifndef __conf_wrapper_business_h__
#define __conf_wrapper_business_h__

#include "conf_defines.h"
#include "conf_wrapper_defines.h"
/************************************************************************/
/* 业务逻辑层回调接口                                                      */
/************************************************************************/

/*
*	同接入服务器连接断开回调接口,断开后中间层会根据LMInitOptions的设定是否自动重连
*	param result 错误号 call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnDisconnect)(
	int result, void* clientdata);

/*
*	进入会议室回调接口,第一次或重连成功后会调用此接口
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnEntryConference)(
	int result, void* clientdata);

/*
*	退出会议室回调接口, 调用lm_conf_business_LeaveConference后需要等此回调后才能关闭中间层.
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnLeaveConference)(
	int result, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前会有此回调.服务器会主动发送此信息给客户端
*	param conference_info 当前会议室基本信息, 最重要的一个信息是:会议室是否需要密码进入
*							@see lm_is_as_conference_attribute_need_password
*
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnGetConference)(
	native_object_t conference_info, void* clientdata);

/*
*	进入会议室后会有此回调.当会议基本信息有变化时,会回调此接口
*
*	param conference_info 当前会议室基本信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnUpdateConerence)(
	native_object_t conference_info, void* clientdata);

/*
*	删除会议室后会有此回调.如果是下在参加的会议室被删除,需要调用lm_conf_business_LeaveConference,其他会议室不需要处理.
*
*	param id_conference 会议室ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRemoveConerence)(
	int id_conference, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前会有此回调.服务器会主动发送此信息给客户端,
*	以后会议实时信息有变化也会通知到客户端,
*	应用层应对变化做相应的逻辑处理,应调用lm_get_as_real_time_conference_info_status,
*	得到的值与以前的值做比较,
*	如:会议室由自由管理模式到自由模式的转换,根据应用层具体需求,做或不做变化.
*
*	param real_time_conference_info 当前会议室实时信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnGetConferenceRealTimeInfo)(
	native_object_t real_time_conference_info, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前会有此回调.服务器会主动发送此信息给客户端,
*	以后会议同步信息有变化也会通知到客户端,
*	同步信息具体由应用层决定,中间层不参与逻辑处理.如:会议界面的同步,自定义String消息
*	param conference_sync_info 当前会议室同步信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnGetConferenceSyncInfo)(
	native_object_t conference_sync_info, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前会有此回调.
*	param slef_attendee_info 参会者信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAddSelfAttendee)(
	native_object_t slef_attendee_info, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前或以后有人员上线时会有此回调
*	param slef_attendee_info 参会者信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAttendeeOnline)(
	native_object_t attendee_info, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前或以后有人员下线时会有此回调
*	param account 参会者账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAttendeeOffline)(
	const char* account, void* clientdata);

/*
*	FuncBusinessOnEntryConference之前或以后有人员信息变化时会有此回调,
*	FuncBusinessOnEntryConference之前有回调,主要原因是,在掉线过程中,其他
*	与会者信息发生变化,如由主席变为非主席,客户端应根据需求做或不做逻辑处理.
*	param old_attendee_info 参会者变化前信息
*	param new_attendee_info 参会者变化后信息
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnUpdateAttendee)(
	native_object_t old_attendee_info, native_object_t new_attendee_info, void* clientdata);

/*
*	有人员被从企业删除时会有此回调,如果是与会者本人被删除,需要做退会操作.
*	param account 参会者账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRemoveAttendee)(
	const char* account, void* clientdata);

/*
*	会议室增加管理员时会有此回调.
*	param account 管理员账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAddConfAdmin)(
	const char* account, void* clientdata);

/*
*	会议室减少管理员时会有此回调.
*	param account 管理员账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRemoveConfAdmin)(
	const char* account, void* clientdata);

/*
*	会议室增加默认与会者会有此回调.
*	param account 默认与会者账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAddConfDefaultAttendee)(
	const char* account, void* clientdata);

/*
*	会议室减少默认与会者会有此回调.
*	param account 默认与会者账号
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRemoveConfDefaultAttendee)(
	const char* account, void* clientdata);

/*
*	申请发言回应.如果是集中管理模式,需要管理员授权,方可发言,如果是自由模式,没有达到会议室
*	最大发言人数,就可发言. @see lm_conf_business_ApplySpeak
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param apply 1-申请发言, 0-取消发言
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnApplySpeakOper)(
	int result, const char* account, int new_state, int old_state, int apply, void* clientdata);

/*
*	需要管理员权限,可授权与会者为发言状态或取消与会者的发言状态.
*	@see lm_conf_business_AccreditSpeak
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param accredit 1-授权, 0-取消授权
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAccreditSpeakOper)(
	int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);

/*
*	与会者变为发言者或取消发言后的广播通知消息.由与会者申请或由管理员授权.
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param is_speak 1-发言状态, 0-非发言状态
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSpeakNotify)(
	const char* account, int new_state, int old_state, int is_speak, void* clientdata);

/*
*	申请数据操作权限回应.如果是集中管理模式,需要管理员授权,方可,如果是自由模式,
*	则直接申请成功. @see lm_conf_business_ApplyDataOper
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param apply 1-申请数据权限, 0-取消数据权限
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnApplyDataOper)(
	int result, const char* account, int new_state, int old_state, int apply, void* clientdata);

/*
*	需要管理员权限,可授权与会者有数据操作权限或取消数据操作权限.
*	@see lm_conf_business_AccreditDataOper
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param accredit 1-授权, 0-取消授权
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAccreditDataOper)(
	int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);

/*
*	与会者变为有数据操作权限或取消数据操作权限后的广播通知消息.由与会者申请或由管理员授权.
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param is_data_op 1-有数据操作权限, 0-无数据操作权限
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnDataOpNotify)(
	const char* account, int new_state, int old_state, int is_data_op, void* clientdata);

/*
*	在有数据操作权限的前提下可申请数据同步操作.
*	申请数据同步回应.如果是集中管理模式,需要管理员授权,方可,如果是自由模式,
*	则直接申请成功. @see lm_conf_business_ApplyDataSync
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param apply 1-申请同步权限, 0-取消同步权限
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnApplyDataSync)(
	int result, const char* account, int new_state, int old_state, int apply, void* clientdata);

/*
*	需要管理员权限,可授权与会者有同步权限或取消同步权限.
*	@see lm_conf_business_AccreditDataSync
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param accredit 1-授权, 0-取消授权
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAccreditDataSync)(
	int result, const char* account, int new_state, int old_state, int accredit, void* clientdata);

/*
*	与会者变为有同步权限或取消同步权限后的广播通知消息.由与会者申请或由管理员授权.
*	param account 用户账号
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param is_sync 1-有同步权限, 0-无同步权限
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSyncOpNotify)(
	const char* account, int new_state, int old_state, int is_sync, void* clientdata);

/*
*	与会者发送同步命令回应. @see lm_conf_business_DataSyncCommand
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnDataSyncCommand)(
	int result, void* clientdata);

/*
*	与会者发送同步命令通知. @see lm_conf_business_DataSyncCommand
*	param syncer 做同步者
*	param sync_data 同步数据(由应用层自定义)
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnDataSyncCommandNotify)(
	const char* syncer, const char* sync_data, void* clientdata);

/*
*	与会者申请临时管理员回应或通知. @see lm_conf_business_ApplyTempAdmin
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 申请者
*	param new_state 新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 老的用户状态 @see LMAttendeeOpsFlags
*   param apply 1-申请管理员, 0-取消管理员
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnApplyTempAdmin)(
	int result, const char* account,
	int new_state, int old_state, int apply, void* clientdata);

/*
*	与会者授权临时管理员回应或通知. @see lm_conf_business_AccreditTempAdmin
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 被授权者
*	param new_state 被授权者新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 被授权者老的用户状态 @see LMAttendeeOpsFlags
*   param apply 1-授权管理员, 0-取消管理员
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAccreditTempAdmin)(
	int result, const char* account,
	int new_state, int old_state, int accredit, void* clientdata);

/*
*	与会者密码认证申请临时管理员回应. @see lm_conf_business_AuthTempAdmin
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param admin_psw 会议室管理员密码
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAuthTempAdmin)(
	int result, const char* admin_psw, void* clientdata);

/*
*	与会者密码认证申请临时管理员通知. @see lm_conf_business_AuthTempAdmin
*	param admin_psw 会议室管理员申请者
*	param new_state 申请者新的用户状态 @see LMAttendeeOpsFlags
*	param old_state 申请者老的用户状态 @see LMAttendeeOpsFlags
*	param apply 1-申请,0-取消
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnTempAdminNotify)(const char* account,
	uint32_t new_state, uint32_t old_state, int apply, void* clientdata);

/*
*	申请预览与会者视频回应. @see lm_conf_business_StartPreviewVideo
*	预览者接收到此消息且result=0,应准备显示窗口用于中间层接收的视频数据用于显示
*	@see conf_wrapper_video.h中lm_conf_video_AddRemotePreview
*
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param shower 被预览者
*	param id_device 被预览者视频设备ID
*	param context 上下文 @see lm_conf_business_StartPreviewVideo 参数uint64_t　context
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPreviewVideo)(
	int result, const char* shower,
	int id_device, uint64_t context, void* clientdata);

/*
*	停止预览与会者视频回应. @see lm_conf_business_StopPreviewVideo
*	应用层可以对此接口不做处理
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param shower 被预览者
*	param id_device 被预览者视频设备ID
*	param context 上下文 @see lm_conf_business_StopPreviewVideo 参数uint64_t　context
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPreviewVideo)(
	int result, const char* shower,
	int id_device, uint64_t context, void* clientdata);

/*
*	申请预览与会者视频通知. @see lm_conf_business_StartPreviewVideo
*	如果被预览者接收到此消息，中间层会自动采集并编码视频数据至视频服务器.应用层可以对此接口不做处理
*	param shower 被预览者
*	param id_device 被预览者视频设备ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPreviewVideoNotify)(
	const char* shower, int id_device, void* clientdata);

/*
*	停止预览与会者视频通知. @see lm_conf_business_StopPreviewVideo
*	被预览者接收到此通知中间层会停止视频编码或采集.应用层可以对此接口不做处理
*	param shower 被预览者
*	param id_device 被预览者视频设备ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPreviewVideoNotify)(
	const char* shower, int id_device, void* clientdata);

/*
*	与会者切换主视频设备回应. @see lm_conf_business_SwitchMainVideo
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param old_dev_id 老的主视频设备ID
*	param new_dev_id 新的主视频设备ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSwitchMainVideo)(
	int result, int old_dev_id, int new_dev_id, void* clientdata);

/*
*	与会者切换主视频设备通知. @see lm_conf_business_SwitchMainVideo
*	接收到此通知，如果预览了此与会者的主视频,
*	应先调用lm_conf_business_StopPreviewVideo再调用lm_conf_business_StartPreviewVideo
*	param account 切换主视频设备者
*	param old_dev_id 老的主视频设备ID
*	param new_dev_id 新的主视频设备ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSwitchMainVideoNotify)(
	const char* account, int old_dev_id, int new_dev_id, void* clientdata);

/*
*	与会者禁止或共享视频设备回应. @see lm_conf_business_EnableVideo
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param enabled 1-共享所有视频设备,0-禁止所有视频设备
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnEnableVideo)(
	int result, int enabled, void* clientdata);

/*
*	与会者禁止或共享视频设备通知. @see lm_conf_business_EnableVideo
*	当接收到此消息并预览了此用户的视频设备应关闭此用户已经预览的视频,
*	调用lm_conf_business_StopPreviewVideo与lm_conf_video_RemoveRemotePreview
*	param account 禁止或共享视频设备者
*	param enabled 1-共享所有视频设备,0-禁止所有视频设备
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnEnableVideoNotify)(
	const char* account, int enabled, void* clientdata);

/*
*	与会者禁止或共享一路视频设备回应. @see lm_conf_business_CaptureVideoState
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param id_device 视频设备ID
*	param enabled 1-共享一路视频设备,0-禁止一路视频设备
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnCaptureVideoState)(
	int result, int id_device, int enabled, void* clientdata);

/*
*	与会者禁止或共享一路视频视频设备通知. @see lm_conf_business_EnableVideo
*	当接收到此消息并预览了此用户的此路视频设备应关闭此用户此路视频,
*	调用lm_conf_business_StopPreviewVideo与lm_conf_video_RemoveRemotePreview
*	param account 禁止或共享一路视频设备者
*	param id_device 视频设备ID
*	param enabled 1-共享一路视频设备,0-禁止一路视频设备
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnCaptureVideoStateNotify)(
	const char* account, int id_device, int enabled, void* clientdata);

/*
*	与会者视频设备显示名称发生改变通知. @see lm_conf_business_UpdateVideoShowName
*	param account 与会者
*	param id_device 视频设备ID
*	param new_show_name 新的显示名称
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnVideoShowNameChangedNotify)(
	const char* account, int id_device, const char* new_show_name, void* clientdata);

/*
*	与会者视频设备发生改变通知. 由中间层发起,如视频设备的热插拔
*	dev_nums, dev_state, dev_main_id只是改变后概括性的值，具体细节通过人员信息实例接口获取
*	lm_get_as_attendee_video_device_nums
*	lm_get_as_attendee_enum_video_devices
*	lm_get_as_attendee_video_device_type
*	lm_get_as_attendee_video_device_state
*	lm_get_as_attendee_video_device_show_name
*	lm_get_as_attendee_main_video_device_id
*	param account 发生改变与会者
*	param dev_nums 改变后的设备个数
*	param dev_state 改变后所有设备状态,
*	param dev_main_id 新的主视频设备ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnVideoDeviceChangedNotify)(
	const char* account, int dev_nums, int dev_state, int dev_main_id, void* clientdata);

/*
*	与会者踢人回应. @see lm_conf_business_KickOutAttendee
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 被踢与会者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnKickoutAttendee)(
	int result, const char* account, void* clientdata);

/*
*	与会者踢人通知. @see lm_conf_business_KickOutAttendee
*	如果被踢者是自己应调用lm_conf_business_LeaveConference
*	param oper 踢人者
*	param account 被踢与会者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnKickoutAttendeeNotify)(
	const char* oper, const char* account, void* clientdata);

/*
*	与会者更改昵称回应. @see lm_conf_business_UpdataAttendeeName
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param account 修改与会者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnUpdateAttendeeName)(
	int result, const char* account, void* clientdata);

/*
*	与会者更改昵称通知. @see lm_conf_business_KickOutAttendee
*	应用层修改此与会者的显示信息
*	param account 更改昵称者
*	param new_name 更改后的昵称
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnUpdateAttendeeNameNotify)(
	const char* account, const char* new_name, void* clientdata);

/*
*	与会者发送消息给一个与会者回应. @see lm_conf_business_RelayMsgToOne
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	应用层可以不处理此回调
*	param receiver 接收者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRelayMsgToOne)(
	int result, const char* receiver, void* clientdata);

/*
*	与会者发送消息给一个与会者通知. @see lm_conf_business_RelayMsgToOne
*	如果接收者是自己,应用层根据具体需求做操作,中间层不处理此业务,服务器只是负责转发
*	param sneder 发送者
*	param receiver 接收者
*	param msg 字符串由应用层自定义
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRelayMsgToOneNotify)(
	const char* sneder, const char* receiver, const char* msg, void* clientdata);

/*
*	与会者发送消息给所有与会者回应. @see lm_conf_business_RelayMsgToAll
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	应用层可以不处理此回调
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRelayMsgToAll)(
	int result, void* clientdata);

/*
*	与会者发送消息给所有与会者通知. @see lm_conf_business_RelayMsgToAll
*	应用层根据具体需求做操作,中间层不处理此业务,服务器只是负责转发
*	param sneder 发送者
*	param msg 字符串由应用层自定义
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnRelayMsgToAllNotify)(
	const char* sneder, const char* msg, void* clientdata);

/*
*	管理员操作会议室状态回应. @see lm_conf_business_AdminOperConfSetting
*	应用层应根据相对应的状态做变化,如集中管理模式与自由发言模式的变换等.
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param cmd @see LMConferenceCommandModes
*	param cmd_value 0-为取消状态,1-为置状态
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAdminOperConfSetting)(
	int result, int cmd, int cmd_value, void* clientdata);

/*
*	管理员操作会议室状态通知. @see lm_conf_business_AdminOperConfSetting
*	应用层应根据相对应的状态做变化,如集中管理模式与自由发言模式的变换等.
*	param oper 操作者
*	param cmd @see LMConferenceCommandModes
*	param cmd_value 0-为取消状态,1-为置状态
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAdminOperConfSettingNotify)(
	const char* oper, int cmd, int cmd_value, void* clientdata);

/*
*	修改会议室密码回应,需要管理员权限. @see lm_conf_business_SetConfPassword
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSetConfPassword)(int result, void* clientdata);

/*
*	修改会议室密码通知. @see lm_conf_business_SetConfPassword
*	应用层可以不处理此回调
*	param oper 操作者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSetConfPasswordNotify)(
	const char* oper, void* clientdata);

/*
*	与会者开始桌面共享回应. @see lm_conf_business_StartDesktopShare
*	整个会议室只允许一路桌面共享
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartDesktopShare)(
	int result, void* clientdata);

/*
*	与会者开始桌面共享通知. @see lm_conf_business_StartDesktopShare
*	整个会议室只允许一路桌面共享,应用层接收到此通知后应调用lm_conf_business_StartPreviewDesktop
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartDesktopShareNotify)(
	const char* sharer, void* clientdata);

/*
*	与会者停止桌面共享回应. @see lm_conf_business_StopDesktopShare
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopDesktopShare)(
	int result, void* clientdata);

/*
*	与会者停止桌面共享通知. @see lm_conf_business_StopDesktopShare
*	应用层应调用lm_conf_business_StopPreviewDesktop并lm_conf_sharedesktop_DestroyView,用于停止预览共享桌面
*	param sharer 桌面共享停止者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopDesktopShareNotify)(
	const char* sharer, void* clientdata);

/*
*	与会者开始预览共享桌面回应. @see lm_conf_business_StartPreviewDesktop
*	result=0, 需要调用lm_conf_sharedesktop_CreateView用于从桌面共享服务器传输的数据显示.
*	param sharer 桌面共享者
*	param context 上下文,由lm_conf_business_StartPreviewDesktop传入
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPreviewDesktop)(
	int result, const char* sharer, uint64_t context, void* clientdata);

/*
*	与会者停止预览共享桌面回应. @see lm_conf_business_StopPreviewDesktop
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param sharer 桌面共享者
*	param context 上下文,由lm_conf_business_StopPreviewDesktop传入
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPreviewDesktop)(
	int result, const char* sharer, uint64_t context, void* clientdata);

/*
*	与会者开始预览共享桌面通知. @see lm_conf_business_StartPreviewDesktop
*	如果与会者是桌面共享者.,中间层接收到此回应后会开始桌面采集并编码发送至桌面共享服务器
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPreviewDesktopNotify)(
	const char* sharer, void* clientdata);

/*
*	与会者停止预览共享桌面通知. @see lm_conf_business_StopPreviewDesktop
*	如果与会者是桌面共享者.中间层接收到此回应后会停止桌面采集并编码
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPreviewDesktopNotify)(
	const char* sharer, void* clientdata);

/*
*	请求远程桌面控制回应. @see lm_conf_business_AskRemoteDesktopControl
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAskRemoteDesktopControl)(
	int result, const char* sharer, void* clientdata);

/*
*	请求远程桌面控制通知. @see lm_conf_business_AskRemoteDesktopControl
*	param controller 控制者
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAskRemoteDesktopControlNotify)(
	const char* controller, const char* sharer, void* clientdata);

/*
*	放弃远程桌面控制回应. @see lm_conf_business_AbstainRemoteDesktopControl
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAbstainRemoteDesktopControl)(
	int result, const char* sharer, void* clientdata);

/*
*	许诺远程桌面控制通知. @see lm_conf_business_AskRemoteDesktopControl
*	param controller 控制者
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAbstainRemoteDesktopControlNotify)(
	const char* controller, const char* sharer, void* clientdata);

/*
*	应答远程桌面控制回应. @see lm_conf_business_AnswerRemoteDesktopControl
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param controller 桌面控制者
*	int allow 1-允许控制,0-不允许控制
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAnswerRemoteDesktopControl)(
	int result, const char* controller, int allow, void* clientdata);

/*
*	应答远程桌面控制. @see lm_conf_business_AnswerRemoteDesktopControl
*	param controller 桌面控制者
*	param sharer 桌面共享者
*	int allow 1-允许控制,0-不允许控制
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAnswerRemoteDesktopControlNotify)(
	const char* controller, const char* sharer, int allow, void* clientdata);

/*
*	中止与会者远程桌面控制回应. @see lm_conf_business_AbortRemoteDesktopControl
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param controller 桌面控制者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAbortRemoteDesktopControl)(
	int result, const char* controller, void* clientdata);

/*
*	中止与会者远程桌面控制通知. @see lm_conf_business_AbortRemoteDesktopControl
*	param controller 桌面控制者
*	param sharer 桌面共享者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnAbortRemoteDesktopControlNotify)(
	const char* controller, const char* sharer, void* clientdata);

/*
*	与会者发启签到回应. @see lm_conf_business_LaunchSignin
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param type @see LMConferenceSigninType
*	param launch_or_stop_time 发起或停止时间
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnLaunchSignin)(
	int result, int type, int64_t launch_or_stop_time, void* clientdata);

/*
*	与会者发启签到通知. @see lm_conf_business_LaunchSignin
*	param account 发起者
*	param type @see LMConferenceSigninType
*	param launch_or_stop_time 发起或停止时间
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnLaunchSigninNotify)(
	const char* account, int type, int64_t launch_or_stop_time, void* clientdata);

/*
*	与会者签到回应. @see lm_conf_business_Signin
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param is_signin 1-签到,0-取消签到
*	param launch_time发起时间 @see lm_conf_business_LaunchSignin
*	param signin_or_cancel_time 签到或取消签到时间
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSignin)(
	int result, int is_signin, int64_t launch_time,
	int64_t signin_or_cancel_time, void* clientdata);

/*
*	与会者签到通知. @see lm_conf_business_Signin
*	param account 签到者
*	param is_signin 1-签到,0-取消签到
*	param launch_time发起时间 @see lm_conf_business_LaunchSignin
*	param signin_or_cancel_time 签到或取消签到时间
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnSigninNotify)(
	const char* account, int is_signin, int64_t launch_time,
	int64_t signin_or_cancel_time, void* clientdata);

/*
*	分组回应(暂不支持). @see lm_conf_business_OperSubGroup
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnOperSubGroup)(
	int result, void* clientdata);

/*
*	分组通知(暂不支持). @see lm_conf_business_OperSubGroup
*	param oper 分组者
*	param id_sub_group 得到此通知者所属分组ID
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnOperSubGroupNotify)(
	const char* oper, int id_sub_group, void* clientdata);

/*
*	取消分组回应(暂不支持). @see lm_conf_business_CancelSubGroup
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnCancelSubGroup)(
	int result, void* clientdata);

/*
*	取消分组通知(暂不支持). @see lm_conf_business_CancelSubGroup
*	param oper 取消分组分组者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnCancelSubGroupNotify)(
	const char* oper, void* clientdata);

/*
*	开启或停止远程与会者桌面共享回应. @see lm_conf_business_OpRemoteDesktopShared
*	应用层可以不处理此回调
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param sharer 需要开启或停止桌面共享者
*	param is_start 1-开启, 0-停止
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnOpRemoteDesktopShared)(
	int result, const char* sharer, int is_start, void* clientdata);

/*
*	开启或停止远程与会者桌面共享通知. @see lm_conf_business_OpRemoteDesktopShared
*	如果通知的与会者是自己,启动或停止桌面流程
*	param oper 启动者
*	param sharer 需要开启或停止桌面共享者
*	param is_start 1-开启, 0-停止
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnOpRemoteDesktopSharedNotify)(
	const char* oper, const char* sharer, int is_start, void* clientdata);

/*
*	开始回放回应. @see lm_conf_business_StartPlayback
*	result=0 调用lm_conf_playback_StartPlayback
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param file_name 回放的录制文件名emv格式
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPlayback)(
	int result, const char* file_name, void* clientdata);

/*
*	开始回放通知. @see lm_conf_business_StartPlayback
*	param account 回放者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartPlaybackNotify)(
	const char* account, void* clientdata);

/*
*	停止回放回应. @see lm_conf_business_StopPlayback
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPlayback)(
	int result, void* clientdata);

/*
*	停止回放通知. @see lm_conf_business_StopPlayback
*	param account 回放者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopPlaybackNotify)(
	const char* account, void* clientdata);

/*
*	媒体播放回应. @see lm_conf_business_StartMediaPlay
*	result=0 调用lm_conf_mediaplay_StartPlayback
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param file_name 媒体文件名
*	parma media_flag 应用层自定义
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartMediaPlay)(
	int result, const char* file_name, int media_flag, void* clientdata);

/*
*	媒体播放通知. @see lm_conf_business_StartMediaPlay
*	param file_name 媒体文件名
*	parma media_flag 应用层自定义
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStartMediaPlayNotify)(
	const char* oper, const char* file_name, int media_flag, void* clientdata);

/*
*	停止媒体播放回应. @see lm_conf_business_StopMediaPlay
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopMediaPlay)(
	int result, void* clientdata);

/*
*	停止媒体播放通知. @see lm_conf_business_StopMediaPlay
*	param oper 播放者
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnStopMediaPlayNotify)(
	const char* oper, void* clientdata);

/*
*	暂停或继续媒体播放回应. @see lm_conf_business_StopMediaPlay
*	param result 0表示成功,其他失败, call lm_get_error_lm_get_error_string
*	param paused 1-暂停,0-继续
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnPauseMediaPlay)(
	int result, int paused, void* clientdata);

/*
*	暂停或继续媒体播放通知. @see lm_conf_business_StopMediaPlay
*	param oper 播放者
*	param paused 1-暂停,0-继续
*	param clientdata 应用层私有参数,通过ConfBusinessCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncBusinessOnPauseMediaPlayNotify)(
	const char* oper, int paused, void* clientdata);

struct ConfBusinessCallback {
	void* client_data_;
	FuncBusinessOnDisconnect OnDisconnect_;
	FuncBusinessOnEntryConference OnEntryConference_;
	FuncBusinessOnLeaveConference OnLeaveConference_;
	FuncBusinessOnGetConference OnGetConference_;
	FuncBusinessOnUpdateConerence OnUpdateConerence_;
	FuncBusinessOnRemoveConerence OnRemoveConerence_;
	FuncBusinessOnGetConferenceRealTimeInfo OnGetConferenceRealTimeInfo_;
	FuncBusinessOnGetConferenceSyncInfo OnGetConferenceSyncInfo_;
	FuncBusinessOnAddSelfAttendee OnAddSelfAttendee_;
	FuncBusinessOnAttendeeOnline OnAttendeeOnline_;
	FuncBusinessOnAttendeeOffline OnAttendeeOffline_;
	FuncBusinessOnUpdateAttendee OnUpdateAttendee_;
	FuncBusinessOnRemoveAttendee OnRemoveAttendee_;
	FuncBusinessOnAddConfAdmin OnAddConfAdmin_;
	FuncBusinessOnRemoveConfAdmin OnRemoveConfAdmin_;
	FuncBusinessOnAddConfDefaultAttendee OnAddConfDefaultAttendee_;
	FuncBusinessOnRemoveConfDefaultAttendee OnRemoveConfDefaultAttendee_;
	FuncBusinessOnApplySpeakOper OnApplySpeakOper_;
	FuncBusinessOnAccreditSpeakOper OnAccreditSpeakOper_;
	FuncBusinessOnSpeakNotify OnSpeakNotify_;
	FuncBusinessOnApplyDataOper OnApplyDataOper_;
	FuncBusinessOnAccreditDataOper OnAccreditDataOper_;
	FuncBusinessOnDataOpNotify OnDataOpNotify_;
	FuncBusinessOnApplyDataSync OnApplyDataSync_;
	FuncBusinessOnAccreditDataSync OnAccreditDataSync_;
	FuncBusinessOnSyncOpNotify OnSyncOpNotify_;
	FuncBusinessOnDataSyncCommand OnDataSyncCommand_;
	FuncBusinessOnDataSyncCommandNotify OnDataSyncCommandNotify_;
	FuncBusinessOnApplyTempAdmin OnApplyTempAdmin_;
	FuncBusinessOnAccreditTempAdmin OnAccreditTempAdmin_;
	FuncBusinessOnAuthTempAdmin OnAuthTempAdmin_;
	FuncBusinessOnTempAdminNotify OnTempAdminNotify_;
	FuncBusinessOnStartPreviewVideo OnStartPreviewVideo_;
	FuncBusinessOnStopPreviewVideo OnStopPreviewVideo_;
	FuncBusinessOnStartPreviewVideoNotify OnStartPreviewVideoNotify_;
	FuncBusinessOnStopPreviewVideoNotify OnStopPreviewVideoNotify_;
	FuncBusinessOnSwitchMainVideo OnSwitchMainVideo_;
	FuncBusinessOnSwitchMainVideoNotify OnSwitchMainVideoNotify_;
	FuncBusinessOnEnableVideo OnEnableVideo_;
	FuncBusinessOnEnableVideoNotify OnEnableVideoNotify_;
	FuncBusinessOnCaptureVideoState OnCaptureVideoState_;
	FuncBusinessOnCaptureVideoStateNotify OnCaptureVideoStateNotify_;
	FuncBusinessOnVideoDeviceChangedNotify OnVideoDeviceChangedNotify_;
	FuncBusinessOnVideoShowNameChangedNotify OnVideoShowNameChangedNotify_;
	FuncBusinessOnKickoutAttendee OnKickoutAttendee_;
	FuncBusinessOnKickoutAttendeeNotify OnKickoutAttendeeNotify_;
	FuncBusinessOnUpdateAttendeeName OnUpdateAttendeeName_;
	FuncBusinessOnUpdateAttendeeNameNotify OnUpdateAttendeeNameNotify_;
	FuncBusinessOnRelayMsgToOne OnRelayMsgToOne_;
	FuncBusinessOnRelayMsgToOneNotify OnRelayMsgToOneNotify_;
	FuncBusinessOnRelayMsgToAll OnRelayMsgToAll_;
	FuncBusinessOnRelayMsgToAllNotify OnRelayMsgToAllNotify_;
	FuncBusinessOnAdminOperConfSetting OnAdminOperConfSetting_;
	FuncBusinessOnAdminOperConfSettingNotify OnAdminOperConfSettingNotify_;
	FuncBusinessOnSetConfPassword OnSetConfPassword_;
	FuncBusinessOnSetConfPasswordNotify OnSetConfPasswordNotify_;
	FuncBusinessOnStartDesktopShare OnStartDesktopShare_;
	FuncBusinessOnStartDesktopShareNotify OnStartDesktopShareNotify_;
	FuncBusinessOnStopDesktopShare OnStopDesktopShare_;
	FuncBusinessOnStopDesktopShareNotify OnStopDesktopShareNotify_;
	FuncBusinessOnStartPreviewDesktop OnStartPreviewDesktop_;
	FuncBusinessOnStopPreviewDesktop OnStopPreviewDesktop_;
	FuncBusinessOnStartPreviewDesktopNotify OnStartPreviewDesktopNotify_;
	FuncBusinessOnStopPreviewDesktopNotify OnStopPreviewDesktopNotify_;
	FuncBusinessOnAskRemoteDesktopControl OnAskRemoteDesktopControl_;
	FuncBusinessOnAskRemoteDesktopControlNotify OnAskRemoteDesktopControlNotify_;
	FuncBusinessOnAbstainRemoteDesktopControl OnAbstainRemoteDesktopControl_;
	FuncBusinessOnAbstainRemoteDesktopControlNotify OnAbstainRemoteDesktopControlNotify_;
	FuncBusinessOnAnswerRemoteDesktopControl OnAnswerRemoteDesktopControl_;
	FuncBusinessOnAnswerRemoteDesktopControlNotify OnAnswerRemoteDesktopControlNotify_;
	FuncBusinessOnAbortRemoteDesktopControl OnAbortRemoteDesktopControl_;
	FuncBusinessOnAbortRemoteDesktopControlNotify OnAbortRemoteDesktopControlNotify_;
	FuncBusinessOnLaunchSignin OnLaunchSignin_;
	FuncBusinessOnLaunchSigninNotify OnLaunchSigninNotify_;
	FuncBusinessOnSignin OnSignin_;
	FuncBusinessOnSigninNotify OnSigninNotify_;
	FuncBusinessOnOperSubGroup OnOperSubGroup_;
	FuncBusinessOnOperSubGroupNotify OnOperSubGroupNotify_;
	FuncBusinessOnCancelSubGroup OnCancelSubGroup_;
	FuncBusinessOnCancelSubGroupNotify OnCancelSubGroupNotify_;
	FuncBusinessOnOpRemoteDesktopShared OnOpRemoteDesktopShared_;
	FuncBusinessOnOpRemoteDesktopSharedNotify OnOpRemoteDesktopSharedNotify_;
	FuncBusinessOnStartPlayback OnStartPlayback_;
	FuncBusinessOnStartPlaybackNotify OnStartPlaybackNotify_;
	FuncBusinessOnStopPlayback OnStopPlayback_;
	FuncBusinessOnStopPlaybackNotify OnStopPlaybackNotify_;
	FuncBusinessOnStartMediaPlay OnStartMediaPlay_;
	FuncBusinessOnStartMediaPlayNotify OnStartMediaPlayNotify_;
	FuncBusinessOnStopMediaPlay OnStopMediaPlay_;
	FuncBusinessOnStopMediaPlayNotify OnStopMediaPlayNotify_;
	FuncBusinessOnPauseMediaPlay OnPauseMediaPlay_;
	FuncBusinessOnPauseMediaPlayNotify OnPauseMediaPlayNotify_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_business_set_observer(
	native_object_t instance, struct ConfBusinessCallback* callback);

/*
*	1.通过中心得到的接入服务器地址和应用层的认证信息开始进入会议室
*	2.当同接入服务器断开后,中间层会自动调用此接口,进行重连
*	3.当成功进入会议室会依次调用以下回调:
*	FuncBusinessOnAttendeeOnline
*	FuncBusinessOnAttendeeOffline(不是断开重连,不会有此回调)
*	FuncBusinessOnUpdateAttendee(不是断开重连,不会有此回调)
*	FuncBusinessOnAddSelfAttendee
*	FuncBusinessOnGetConference
*	FuncBusinessOnGetConferenceRealTimeInfo
*	FuncBusinessOnGetConferenceSyncInfo
*	FuncBusinessOnEntryConference
*	4.当失败会调用:
*	FuncBusinessOnEntryConference
*	5.对于其他与会者会调用:
*	FuncBusinessOnAttendeeOnline
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param json_address 接入服务器地址
*	param id_conference 会议室ID
*	param conf_tag 一个变化的会议室tag值,参与认证
*	param conf_psw 会议室密码(如果没有设为空字符串)
*	param is_md5 输入的conf_psw是否是md5值,如果是明文,中间层会做md5变换
*	param account 用户账号
*	param id_org 企业ID
*	param name 用户昵称
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_EntryConference(
	native_object_t instance, const char* json_address,
	int id_conference, int conf_tag, const char* conf_psw, int is_md5,
	const char* account, int id_org, const char* name);

/*
*	1.离开会议室调用此接口,需要等待FuncBusinessOnLeaveConference回调,
*		才能销毁会议中间层引擎,但当返回值不为0时,可以销毁会议中间层引擎,终止程序.
*	2.对于其他与会者会响应
*		FuncBusinessOnAttendeeOffline
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_LeaveConference(
	native_object_t instance);

/*
*	申请或取消发言.
*	1.当为自由发言模式时会议室没有超过最大发言人数时,申请发言会返回发言状态.
*	2.当为集中管理模式时,需要管理员授权,但如为管理员申请,不受此限制
*	3.对于申请者会响应FuncBusinessOnApplySpeakOper
*	4.对于其他与会者会响应FuncBusinessOnApplySpeakOper
*	5.对于申请者或其他与会者都应对响应根据状态做出反应.
*	6.当最终变为发言状态,会有回调:
*		FuncBusinessOnSpeakNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param apply 1-申请, 0-取消
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_ApplySpeak(
	native_object_t instance, int apply);

/*
*	对其他与会者授权或取消发言.
*	需要管理员权限.
*	@see lm_conf_business_ApplySpeak
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param account 被授权者
*	param accredit 1-授权发言, 0-取消发言
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AccreditSpeak(
	native_object_t instance, const char* account, int accredit);

/*
*	申请或取消数据操作权限.@see FuncBusinessOnApplyDataOper
*	有关回调:
*	FuncBusinessOnApplyDataOper
*	FuncBusinessOnDataOpNotify
*	FuncBusinessOnAccreditDataOper
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param apply 1-申请, 0-取消
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_ApplyDataOper(
	native_object_t instance, int apply);

/*
*	对其他与会者授权或取消数据操作权限.@see FuncBusinessOnAccreditDataOper
*	需要管理员权限.
*	有关回调:
*	FuncBusinessOnApplyDataOper
*	FuncBusinessOnDataOpNotify
*	FuncBusinessOnAccreditDataOper
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param account 被授权者
*	param accredit 1-授权数据操作, 0-取消数据操作
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AccreditDataOper(
	native_object_t instance, const char* account, int accredit);

/*
*	申请或取消数据同步. @see FuncBusinessOnApplyDataSync
*	有关回调:
*	FuncBusinessOnApplyDataSync
*	FuncBusinessOnAccreditDataSync
*	FuncBusinessOnSyncOpNotify

*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param apply 1-申请, 0-取消
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_ApplyDataSync(
	native_object_t instance, int apply);

/*
*	对其他与会者授权或取消数据同步. @see FuncBusinessOnAccreditDataSync
*	需要管理员权限.
*	有关回调:
*	FuncBusinessOnApplyDataSync
*	FuncBusinessOnAccreditDataSync
*	FuncBusinessOnSyncOpNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param account 被授权者
*	param accredit 1-授权数据操作, 0-取消数据操作
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AccreditDataSync(
	native_object_t instance, const char* account, int accredit);

/*
*	同步操作命令.
*	需要管理员权限.且有数据操作权限
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param command 应用层自定义
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_DataSyncCommand(
	native_object_t instance, const char* command);

/*
*	申请或取消临时管理员.
*	有关回调:
*	FuncBusinessOnApplyTempAdmin
*	FuncBusinessOnAccreditTempAdmin
*	FuncBusinessOnTempAdminNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param apply 1-申请, 0-取消
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_ApplyTempAdmin(
	native_object_t instance, int apply);

/*
*	对与会者进行管理员授权或取消临时管理员.
*	需要管理员权限.
*	有关回调:
*	FuncBusinessOnApplyTempAdmin
*	FuncBusinessOnAccreditTempAdmin
*	FuncBusinessOnTempAdminNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param account 被授权者
*	param accredit 1-授权, 0-取消
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AccreditTempAdmin(
	native_object_t instance, const char* account, int accredit);

/*
*	密码谁临时管理员.
*	有关回调:
*	FuncBusinessOnAuthTempAdmin
*	FuncBusinessOnTempAdminNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param admin_psw 会议室密码(明文)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AuthTempAdmin(
	native_object_t instance, const char* admin_psw);

/*
*	预览与会者视频.
*	有关回调:
*	FuncBusinessOnStartPreviewVideo
*	FuncBusinessOnStartPreviewVideoNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param shower 与会者
*	param id_device 与会者视频设备ID,如果有多设备会有多个ID,@see人员信息视频设备相关
*	param context 上下文,会通过回调返回给应用层,不能使用对象指针.
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StartPreviewVideo(
	native_object_t instance, const char* shower, int id_device, uint64_t context);

/*
*	停止预览与会者视频.
*	有关回调:
*	FuncBusinessOnStopPreviewVideo
*	FuncBusinessOnStopPreviewVideoNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param shower 与会者
*	param id_device
*	param context 上下文,会通过回调返回给应用层,不能使用对象指针.
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StopPreviewVideo(
	native_object_t instance, const char* shower, int id_device, uint64_t context);

/*
*	切换本地主视频设备.如果其他与会者正在预览本与会者的主视频,
*	先调用lm_conf_business_StopPreviewVideo老的主视频,
*	而后调用lm_conf_business_StartPreviewVideo新的主视频
*	有关回调:
*	FuncBusinessOnSwitchMainVideo
*	FuncBusinessOnSwitchMainVideoNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param id_device  新的主视频设备ID
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_SwitchMainVideo(
	native_object_t instance, int id_device);

/*
*	共享或禁止本地所有视频设备.
*	禁止状态下,如果其他与会者正在预览本与会者的视频,	调用lm_conf_business_StopPreviewVideo视频,
*	不能预览禁止的视频设备
*	有关回调:
*	FuncBusinessOnEnableVideo
*	FuncBusinessOnEnableVideoNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param enabled  0-禁止,1-共享
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_EnableVideo(
	native_object_t instance, int enabled);

/*
*	共享或禁止本地某路视频设备.
*	禁止状态下,如果其他与会者正在预览本与会者的此路视频, 调用lm_conf_business_StopPreviewVideo视频,
*	不能预览禁止的视频设备
*	有关回调:
*	FuncBusinessOnCaptureVideoState
*	FuncBusinessOnCaptureVideoStateNotify
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param id_device  视频设备ID
*	param enabled  0-禁止,1-共享
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_CaptureVideoState(
	native_object_t instance, int id_device, int enabled);

/*
*	修改某路视频设备的显示名称.
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param id_device  视频设备ID
*	param name_utf8  新的名称
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_UpdateVideoShowName(
	native_object_t instance, int id_device, const char* name_utf8);

/*
*	踢人操作.需要管理员权限
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param account  被踢者
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_KickOutAttendee(
	native_object_t instance, const char* account);

/*
*	修改昵称
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param name  新的昵称
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_UpdataAttendeeName(
	native_object_t instance, const char* name);

/*
*	转发消息到某个与会者
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param receiver  与会者
*	param msg 应用层自定义
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_RelayMsgToOne(
	native_object_t instance, const char* receiver, const char* msg);

/*
*	转发消息到所有与会者
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param msg 应用层自定义
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_RelayMsgToAll(
	native_object_t instance, const char* msg);

/*
*	设置会议室状态
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param cmd @see LMConferenceCommandModes
*	param cmd_value 0-为取消状态,1-为置状态
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AdminOperConfSetting(
	native_object_t instance, int cmd, int cmd_value);

/*
*	设置会议室密码
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param psw 新的会议室密码(明文)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_SetConfPassword(
	native_object_t instance, const char* psw);

/*
*	开始桌面共享
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StartDesktopShare(
	native_object_t instance);

/*
*	停止桌面共享
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StopDesktopShare(
	native_object_t instance);

/*
*	开始预览与会者的桌面
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param sharer 桌面共享者
*	param context 上下文
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StartPreviewDesktop(
	native_object_t instance, const char* sharer, uint64_t context);

/*
*	停止预览与会者的桌面
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param sharer 桌面共享者
*	param context 上下文
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StopPreviewDesktop(
	native_object_t instance, const char* sharer, uint64_t context);

/*
*	请求控制与会者的桌面
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param sharer 桌面共享者
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AskRemoteDesktopControl(
	native_object_t instance, const char* sharer);

/*
*	放弃控制与会者的桌面
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param sharer 桌面共享者
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AbstainRemoteDesktopControl(
	native_object_t instance, const char* sharer);

/*
*	应答远程桌面控制者
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param controller 远程桌面控制者
*	param allow 1-同意, 0-不同意
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AnswerRemoteDesktopControl(
	native_object_t instance, const char* controller, int allow);

/*
*	中断控制者控制
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param controller 远程桌面控制者
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_AbortRemoteDesktopControl(
	native_object_t instance, const char* controller);

/*
*	发起签到
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param signin_type @see LMConferenceSigninType
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_LaunchSignin(
	native_object_t instance, int signin_type);

/*
*	与会者签到或取消签到
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param is_signin 1-签到, 0-取消签到
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_Signin(
	native_object_t instance, int is_signin);

/*
*	分组,需要管理员权限(暂不支持)
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param accounts 与会者
*	param count accounts size
*	param id_subgroup 所分组的组ID
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_OperSubGroup(
	native_object_t instance, const char** accounts, int count, int id_subgroup);

/*
*	取消分组,需要管理员权限(暂不支持)
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_CancelSubGroup(
	native_object_t instance);

/*
*	打开或关闭与会者的桌面共享,需要管理员权限
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param sharer 共享者
*	param is_start 1-打开, 0-关闭
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_OpRemoteDesktopShared(
	native_object_t instance, const char* sharer, int is_start);

/*
*	开始回放, 会议室只能有一个回放者
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param file_name 模块录制的文件名
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StartPlayback(
	native_object_t instance, const char* file_name);

/*
*	停止回放
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StopPlayback(
	native_object_t instance);

/*
*	开始媒体播放, 会议室只能有一个播放者
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	param file_name 媒体文件
*	param media_flag 应用层自定义.
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StartMediaPlay(
	native_object_t instance, const char* file_name, int media_flag);

/*
*	停止媒体播放
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_business_StopMediaPlay(
	native_object_t instance);

/*
*	暂停或继续媒体播放
*
*	param instance 通过lm_get_conf_business_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int lm_conf_business_PauseMediaPlay(
	native_object_t instance, int paused);

#endif //__conf_wrapper_business_h__