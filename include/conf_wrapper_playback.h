#ifndef __conf_wrapper_playback_h__
#define __conf_wrapper_playback_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 回放模块回调接口	,暂时只支持windows系统										*/
/************************************************************************/
/**
*	调用lm_conf_playback_StartPlayback的回调
*	param result 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnPlaybackOpen)(
	int result, void* clientdata);

/**
*	调用lm_conf_playback_PlayPlayback的回调
*	param result 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnPlaybackPlay)(
	int result, void* clientdata);

/**
*	调用lm_conf_playback_PausePlayback的回调
*	param result 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnPlaybackPause)(
	int result, void* clientdata);

/**
*	调用lm_conf_playback_ContinuePlayback的回调
*	param result 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnPlaybackContinue)(
	int result, void* clientdata);

/**
*	播放结束的回调
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnPlaybackComplete)(
	void* clientdata);

/**
*	开始修复录制文件的回调 lm_conf_playback_StartRepair
*	param error 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnRepairBegin)(
	int error, void* clientdata);

/**
*	结束修复录制文件的回调 lm_conf_playback_StopRepair
*	param error 0-success other failure
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnRepairEnd)(
	int error, void* clientdata);

/**
*	修复进度
*	param currpos 当前修复位置
*	param duration 总的修复位置
*	param clientdata 应用层私有参数,通过ConfPlaybackCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncPlaybackOnRepairProgress)(
	int64_t currpos, int64_t duration, void* clientdata);

struct ConfPlaybackCallback {
	void* client_data_;
	FuncPlaybackOnPlaybackOpen OnPlaybackOpen_;
	FuncPlaybackOnPlaybackPlay OnPlaybackPlay_;
	FuncPlaybackOnPlaybackPause OnPlaybackPause_;
	FuncPlaybackOnPlaybackContinue OnPlaybackContinue_;
	FuncPlaybackOnPlaybackComplete OnPlaybackComplete_;
	FuncPlaybackOnRepairBegin OnRepairBegin_;
	FuncPlaybackOnRepairEnd OnRepairEnd_;
	FuncPlaybackOnRepairProgress OnRepairProgress_;
};

/**
*	录制文件是否是一个有效的文件，如果有效，是否需要进行修复,
*	回放前必须进行修复,调用lm_conf_playback_StartRepair
*	param file_name 录制文件 (must not be NULL)
*	param needRepairIfValid 如果是个有效有文件是否需要修复 [IN/OUT] (must not be NULL)
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_IsVaildPlaybackFile(
	const char* file_name, int* needRepairIfValid);

/*
*	设置回调函数
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_playback_set_callback(
	native_object_t instance,
	struct ConfPlaybackCallback* callback);

/*
*	开始一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param file_name 录制文件 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_StartPlayback(
	native_object_t instance,
	const char* file_name);

/*
*	Play一个录制文件, FuncPlaybackOnPlaybackOpen回调成功后,调用此接口
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param playaccounts 回放人数,如nums为0,回放所有录制内容
*	param nums playaccounts的大小
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_PlayPlayback(
	native_object_t instance, const char** playaccounts, int nums);

/*
*	Pause一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_PausePlayback(
	native_object_t instance);

/*
*	Continue一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_ContinuePlayback(
	native_object_t instance);

/*
*	Stop一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_StopPlayback(
	native_object_t instance);

/*
*	修复一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param file_name 录制文件 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_StartRepair(
	native_object_t instance, const char* file_name);

/*
*	停止修复一个录制文件
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_StopRepair(
	native_object_t instance);

/*
*	得到录制文件总共录制的路数
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param type @see LMRecSessionType
*	return -1 failure >=0 为路数
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_GetRecSessionCount(
	native_object_t instance, int type);

/*
*	得到录制文件录制的路数信息,根据此结果可以控制具体回放路数,@see lm_conf_playback_PlayPlayback
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	param type @see LMRecSessionType
*	param info [IN/OUT] (must not be NULL)
*	param count [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_EnumRecSessionInfo(
	native_object_t instance, int type, struct LMRecSessionInfo* info, int* count);

/*
*	当前是否正在回放中
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0-没有回放 1-回放中
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_IsPlaybacking(
	native_object_t instance);

/*
*	当前是否正在修复中
*	param instance 通过lm_get_conf_playback_instance_with_engine产生 (must not be NULL)
*	return 0-没有修复 1-修复中
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_playback_IsRepairing(
	native_object_t instance);

#endif //__conf_wrapper_playback_h__