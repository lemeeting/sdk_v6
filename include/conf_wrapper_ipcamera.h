#ifndef __conf_wrapper_ipcamera_h__
#define __conf_wrapper_ipcamera_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

/************************************************************************/
/* 网络摄像头管理模块回调接口                                                */
/************************************************************************/

/**
*	调用lm_conf_ipcamera_SearchWithLAN的回调
*	param error 0-success other failure
*	param results @see LMIPCameraAddress
*	param count results的个数
*	param clientdata 应用层私有参数,通过ConfIPCameraCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncIPCameraOnSearchWithLAN)(
	int error, const struct LMIPCameraAddress* results, int count, void* clientdata);

/**
*	调用lm_conf_ipcamera_AddIPCamera的回调
*	param error 0-success other failure
*	param identity 内部产生的标识0-65535
*	param info @see LMIPCameraAddress
*	param clientdata 应用层私有参数,通过ConfIPCameraCallback结构传入 [IN]
*/
typedef void(CALLBACK *FuncIPCameraOnAddIPCamera)(
	int error, int identity, const struct LMIPCameraInfo* info, void* clientdata);

struct ConfIPCameraCallback {
	void* client_data_;
	FuncIPCameraOnSearchWithLAN OnSearchWithLAN_;
	FuncIPCameraOnAddIPCamera OnAddIPCamera_;
};

/*
*	设置回调函数
*	param instance 通过lm_get_conf_ipcamera_instance_with_engine产生 (must not be NULL)
*	param callback
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_conf_ipcamera_set_callback(
	native_object_t instance,
	struct ConfIPCameraCallback* callback);

/*
*	设置局域网自动查找的组播地址
*	param instance 通过lm_get_conf_ipcamera_instance_with_engine产生 (must not be NULL)
*	param multicast_address (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_ipcamera_SearchWithLAN(
	native_object_t instance, const char* multicast_address/* = "239.255.255.250:3702"*/);

/*
*	加入一个网络摄像头,或通过局域网自动查找结果,或手动加入已知的局域网或公网网络摄像头
*	param instance 通过lm_get_conf_ipcamera_instance_with_engine产生 (must not be NULL)
*	param address 地址信息(must not be NULL)
*	param auth 认证信息
*	param identity 内部产生的标识 [IN/OUT] (must not be NULL)
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_ipcamera_AddIPCamera(
	native_object_t instance,
	const struct LMIPCameraAddress* address,
	const struct LMIPCameraAuthInfo* auth,
	int* identity);

/*
*	移除一个已加入的网络摄像头
*	param instance 通过lm_get_conf_ipcamera_instance_with_engine产生 (must not be NULL)
*	param identity 标识
*	return 0 success other failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_conf_ipcamera_RemoveIPCamera(
	native_object_t instance, int identity);


#endif //__conf_wrapper_ipcamera_h__