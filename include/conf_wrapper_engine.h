#ifndef __conf_wrapper_engine_h__
#define __conf_wrapper_engine_h__
#include "conf_defines.h"
#include "conf_wrapper_defines.h"

#if defined(__APPLE__)
#ifndef TARGET_OS_IPHONE
int LMRunMessageLoop(int argc, const char *argv[]);
#endif
#endif  // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE

/**
*	创建会议中间层引擎
*	param instance
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_create_conf_engine_instance(
	native_object_t* instance);

/**
*	销毁会议中间层引擎
*	param instance lm_create_conf_engine_instance结果
*/
EXTERN_C CONFWRAPPER_API void CALLBACK lm_destroy_conf_engine_instance(
	native_object_t instance);

/**
*	根据引擎得到基本操作管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_base_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得到业务逻辑处理管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_business_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得到视频管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_video_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得到音频管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_voice_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得到中心服务器管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_center_server_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得数据服务器管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_data_server_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得录制管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_record_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得回放管理实例
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_playback_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得桌面共享管理实例(当前只支持windows平台)
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_sharedesktop_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得媒体播放管理实例(当前只支持windows平台)
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_mediaplayer_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);

/**
*	根据引擎得IPCamera管理实例(当前只支持windows平台)
*	param conf_engine lm_create_conf_engine_instance结果
*	param instance 管理实例
*	return 0 success, -1 failure
*/
EXTERN_C CONFWRAPPER_API int CALLBACK lm_get_conf_ipcamera_instance_with_engine(
	native_object_t conf_engine, native_object_t* instance);


#endif //__conf_wrapper_engine_h__